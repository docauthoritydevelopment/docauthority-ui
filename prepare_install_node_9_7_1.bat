@echo off

REM echo === install angular-cli
REM cd  client-angular
REM npm install -g @angular/cli@7.3.7

echo === Prepare V2 files for installer
cd client-angular
call npm --no-color install
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

REM echo === List npm components installed versions
REM call npm --no-color list --offline
REM if NOT ERRORLEVEL 0 (
REM	echo Error %ERRORLEVEL%
REM	exit /b %ERRORLEVEL%
REM )

echo === V2 buildDev
call npm --no-color run buildDev
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === V2 buildProd
call npm --no-color run buildProd
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === V2 zipClientDev
call npm --no-color run zipClientDev
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === V2 zipClientPro
call npm --no-color run zipClientPro
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

REM echo === V2 zipClientNode
REM call npm --no-color run zipClientNode
REM if NOT ERRORLEVEL 0 (
REM 	echo Error %ERRORLEVEL%
REM 	exit /b %ERRORLEVEL%
REM )
REM
cd ..
echo === V2 Prepare files done

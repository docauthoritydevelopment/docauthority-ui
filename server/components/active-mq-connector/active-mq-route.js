const http = require('http')
const btoa = require('btoa');
const getLocalConfig = require('../daUtils/local-config')

const assembleServerResponse = require('../socket/assemble-server-response')
const localConfig  = getLocalConfig();
const activeMqConfig = localConfig.activeMQ || { user: 'admin', password: 'admin', host: 'localhost', consolePort: 8161 };

const activeMqProxy = function(req, res,  next) {

  const options = {
      // http://52.25.187.121:8161/
    host: activeMqConfig.host,
    path: req.path.replace('/active-mq-url', ''),
    port: activeMqConfig.consolePort,
    headers: {'Authorization': `Basic ${btoa(`${activeMqConfig.user}:${activeMqConfig.password}`)}`, Accept: '*'}
  };
  const promise = new Promise(function(resolve, reject){
    console.log("About to Request activeMq console: " ,  JSON.stringify(options));
    http.get(options, function(resp) {
        console.log("Request activeMq success");
       const responsePromise = new Promise((responseResolver) => {
          assembleServerResponse(resp, responseResolver)
        });
        // Resolve with both response header and content
        responsePromise.then(responseContent => resolve({response: resp, content: responseContent}))
      }
     // resolve(resp);
    ).on('error', function(e) {
      console.error("Got activeMq response: " ,  e);
      reject(e);
    });
  });
  promise.then((amqResponseAndContent) => {
      let content = amqResponseAndContent.content;
    if(content)    {
        const titleRow = `<head>`;
        const baseHrefrow = `<base href="${req.protocol}://${req.host}:${localConfig.port}/active-mq-url/">`;
        if (typeof content === 'string') {
          content = content.replace(titleRow, titleRow + baseHrefrow);
          content = content.replace(/url\(\//g, 'url(').replace(/href=\"\//g, 'href="')
          content = content.replace(/url\('\//g, 'url(\'')
          content = content.replace(/src='\//g, 'src=\'')
           // content = content.replace(/url\(\"\.\.\//g, 'url("')
        }



          res.setHeader('content-type',   amqResponseAndContent.response.headers ? amqResponseAndContent.response.headers['content-type'] : 'text/html')
      return res.send(200,content); //.end();
    }
    else{
      console.error("Got response2: " + amqResponseAndContent.errorCode);
      res.send("sds ");
      res.status(444);
      res.end();
    }
  }).catch(function(resp)
  {
      console.error("Got response3: " , resp);
    res.status( 500);
    res.end();
  });

};

module.exports = activeMqProxy

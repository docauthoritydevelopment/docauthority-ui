function getLocalConfig() {
    let localConfig = null;
    try {
      localConfig = require('../../config/local.env');
      console.info('local.env file found ' + JSON.stringify(localConfig));
    } catch (e) {
      localConfig = {};
      console.info('local.env CONFIG file NOT found ', e);
    }

    return localConfig;
}

module.exports = getLocalConfig
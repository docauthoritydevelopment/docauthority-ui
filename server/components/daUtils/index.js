/**
 * Main DocAuthority utils
 */

'use strict';

var path = require('path');
var chalk = require('chalk');

module.exports.highlight = function (text) {
  return chalk.green.bgBlue.bold(text);
};

module.exports.getModuleName = function (file) {
  var sp = file.split(path.sep);
  var name = sp[sp.length-1];
  if (name === 'index.js') {
    name = sp[sp.length-2];
  } else {
    name = name.replace('.js','');
  }
  return name;
};

module.exports.hasSuffix = function(urlStr) {
  if (urlStr.length <5) {
    return false;
  }
  return urlStr.indexOf(".", urlStr.length - 5) !== -1;
}


module.exports.getLogger = function (file) {
  var _logger = require('log4js').getLogger(module.exports.getModuleName(file));
  _logger.highlight = function(text) {
    return module.exports.highlight(text);
  }
  return _logger;
};



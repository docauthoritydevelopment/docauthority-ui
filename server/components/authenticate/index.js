/**
 * Main application routes
 */

'use strict';

var util = require('util');
var querystring = require('querystring');
var http = require('http');
var httpProxy = require('http-proxy');
var proxy = httpProxy.createProxyServer({});

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
//var BasicStrategy = require('passport-http').BasicStrategy;
var flash = require('connect-flash');
var url = require('url');
const buildRequestOptions = require("../socket/request-option-builder");


var serverUrl;
var serverPort;
var serverHost;

var loginPage = '/login.html';
var serverLoginPath = '/login';

var localConfig;

//var logger = require('log4js').getLogger(module.id.substring(__dirname.length+1));
const daUtils = require('../daUtils');
var logger = daUtils.getLogger(module.id);
const hasSuffix = daUtils.hasSuffix;
var suppress_xsrf;
var sessionCookieName;
var envConfig,localConfig;
// DB of sessions per user
var users = {};

var configurationList = null;


proxy.on('error', function (err, req, res) {

  var res_status = res.statusCode;
  res.writeHead(502, {
    'Content-Type': 'text/plain'
  });

  logger.error('Proxy communication error. url=' + req.url + ' res-status=' + res.statusCode + ' err=' + err);
  // logger.error('Proxy communication error. url=' + req.url + ' req-status=' + req_status + ' res-status=' + res_status + ' err=' + err);
  res.end('Proxy communication error.');
});

proxy.on('proxyRes', function (proxyRes, req, res) {
  var res_status = proxyRes.statusCode;

  if (proxyRes.statusCode === 200) {
  } else {
    logger.info('proxyRes status:' + proxyRes.statusCode + " for url: " + req.url + ' res-status=' + res_status);
  }
  if (res_status == 401) //Status currently is always 200 due to known bug in the proxy lib
  {
    logger.info('Proxy error: Token has expired. Remove it for users list' + " for url: " + req.url + ' res-status=' + res_status);
    removeUserLocally(req, res);
  }

});

/**
 * Basic login using passport.js - user server login to authenticate
 */
var doLogin = function (username, pass, clientIP, loginDoneCallback) {

  var post_data = querystring.stringify({
    'username': username,
    'password': pass
  });

  // An object of options to indicate where to post to
  var postOptions = {
    hostname: serverHost,
    port: serverPort,
    path: serverLoginPath,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': post_data.length,
      'X-Forwarded-For': clientIP
    }
  };

  var retVal = null;
  // Set up the request
  var postReq = http.request(postOptions, function (res) {
    res.setEncoding('utf8');
    if (res.statusCode !== 200) {
      logger.info('login failure. res.statusCode=' + res.statusCode + ' username:' + username + ' res: ' + JSON.stringify(res.message));
      var msg = 'authentication failure';

      return loginDoneCallback({statusCode: res.statusCode}, false, {message: msg});
    }
    else {
      var server_session_cookie_key = localConfig.server_session_cookie_key || envConfig.server_session_cookie_key;
      var sessionToken = getCookieValue(res.headers['set-cookie'], server_session_cookie_key);
      logger.trace('login ok. res.statusCode=' + res.statusCode + ' session=' + sessionToken);
      if (sessionToken) {
        return getUserDetails(sessionToken, loginDoneCallback);
      }
      else {
        logger.error('Bad user record - missing session. user=' + sessionToken ? JSON.stringify(sessionToken) : 'undefined');
        return loginDoneCallback({statusCode: 403}, false, {message: 'Bad user record - missing session'});
      }

    }
  }).on('error', function (err) {
    var msg = 'authentication connection failure';
    logger.error('Login connection failed. err=' + JSON.stringify(util.inspect(err)));
    return loginDoneCallback({statusCode: 503}, false, {message: msg});
  });
  // post the data
  postReq.write(post_data);
  postReq.end();
};

var getUserDetails = function (sessionToken, loginDoneCallback) {
  var server_session_cookie_key = localConfig.server_session_cookie_key || envConfig.server_session_cookie_key;
  // An object of options to indicate where to post to
  var getOptions = {
    hostname: serverHost,
    port: serverPort,
    path: '/api/user/current',
    method: 'GET',
    headers: {
      //'Authorization': user.token
      Cookie: server_session_cookie_key + '=' + sessionToken
    }
  };
  logger.debug('Server request headers: ' + JSON.stringify(getOptions.headers));

  var body = '';
  // Set up the request
  var getReq = http.request(getOptions, function (res) {
    res.setEncoding('utf8');
    if (res.statusCode != 200) {
      logger.error('Get user info error. res.statusCode=' + res.statusCode);
      return loginDoneCallback({status: res.statusCode}, false, {message: 'Could not get user info'});
    } else {
      logger.debug('User info. res.statusCode=' + res.statusCode);
    }
    res.on('data', function (data) { //The 'data' event is emitted whenever the stream is relinquishing ownership of a chunk of data to a consumer.
      body += data;
    }).on('end', function () { //The 'end' event is emitted when there is no more data to be consumed from the stream.
      var value = JSON.parse(body);
      logger.debug('Server response: ' + JSON.stringify(value));
      var user = {};
      user.session = sessionToken;
      user.id = value.id;
      user.name = value.name;
      user.username = value.username,
        user.bizRoleDtos = value.bizRoleDtos,
        user.funcRoleDtos = value.funcRoleDtos,
        user.passwordMustBeChanged = value.passwordMustBeChanged,
        user.systemRoleDtos = value.systemRoleDtos,
        user.userType = value.userType;

      user.loginLicenseExpired = (typeof value.loginLicenseExpired == "undefined") ? false : value.loginLicenseExpired;

      user.highlightFileContentTextSupport = localConfig.highlightFileContentTextSupport || envConfig.highlightFileContentTextSupport;
      user.dashboards= localConfig.dashboards ||  {  ITSysRole:'RunScans',   AdminSysRole:'ViewContent' };
      user.isDemoInstallation =  !!localConfig.isDemoInstallation;

      user.loginTime = new Date();
      logger.debug('User got all info. ' + user.id + ',' + user.name);
      users[user.session] = user;
      loginDoneCallback(null, user);
    });
  }).on('error', function (e) {
    logger.error('Get user info error. res.statusCode=' + JSON.stringify(util.inspect(e)));
    loginDoneCallback({errorCode: 503}, false, {message: 'Error getting user info'});
  });
  getReq.end();
};

var getURLParameter = function (url, param) {
  var result = (new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20') || null


  // console.log('parameters from url', result);

  return result;
}

var getCookieValue = function (cookies, sessionCookieKey) {

//Set-Cookie: JSESSIONID=14v29op7z0ixk184xzcg1508qh;Path=/
  var sessionCookie;
  var start;
  if (!cookies) {
    return undefined;
  }
  for (var i = 0; i < cookies.length; ++i) {
    sessionCookie = cookies[i];
    start = sessionCookie.indexOf(sessionCookieKey);
    if (start >= 0) {
      break;
    }
  }
  var session;
  var end;
  if (start >= 0) {
    start += sessionCookieKey.length + 1; //+1 for the equal sign
    end = sessionCookie.indexOf(';', start);
    if (end < 0) end = sessionCookie.length;
    session = sessionCookie.substring(start, end);
  }

  return session;
};
var startsWith = function (str1, str2) {
  return str1.substring(0, str2.length) === str2;
};

module.exports.initialize = function (app, localConfig1, envConfig1) {

  envConfig = envConfig1;
  localConfig = localConfig1;
  sessionCookieName = localConfig.client_session_cookie_key || envConfig.client_session_cookie_key;
  suppress_xsrf = (typeof localConfig.suppress_xsrf == "undefined") ? envConfig.suppress_xsrf : localConfig.suppress_xsrf;
  if (suppress_xsrf) {
    logger.info('******  XSRF is suppressed ***********');
  }

  serverUrl = app.set('login_server_url') || app.set('server_url');
  serverHost = url.parse(serverUrl).hostname;
  serverPort = url.parse(serverUrl).port;
  logger.trace('Authenticate init start');

  passport.use(
    new LocalStrategy(
      {passReqToCallback: true},
      function (req, username, password, done) {
        var clientIP =
          (req.headers && req.headers['x-forwarded-for']) ? req.headers['x-forwarded-for'] :
            ((req.connection && req.connection.remoteAddress) ? req.connection.remoteAddress :
              ((req.socket && req.socket.remoteAddress) ? req.socket.remoteAddress :
                ((req.connection && req.connection.socket && req.connection.socket.remoteAddress) ? req.connection.socket.remoteAddress : "")));
        var cl_ip_log = "" +
          ((req.headers && req.headers['x-forwarded-for']) ? req.headers['x-forwarded-for'] : "") + " | " +
          ((req.connection && req.connection.remoteAddress) ? req.connection.remoteAddress : "") + " | " +
          ((req.socket && req.socket.remoteAddress) ? req.socket.remoteAddress : "") + " | " +
          ((req.connection && req.connection.socket && req.connection.socket.remoteAddress) ? req.connection.socket.remoteAddress : "");
        logger.info("Client ip: " + clientIP + "(" + cl_ip_log + ")");
        process.nextTick(function () {
          doLogin(username, password, clientIP, done);
        });
      }
    ));

  passport.serializeUser(function (user, done) {
    //console.log('serializing user='+JSON.stringify(user));
    done(null, user.username);
  });

  passport.deserializeUser(function (username, done) {
    logger.warn('Unexpected callL De-serializing user=' + JSON.stringify(username));
    /*
        var user = users[username];
        if(user){
          done(null, user);
        }else{
          done('invalid sesssion');
        }
    */
    done('invalid call');
  });

  app.use(passport.initialize());
  //app.use(passport.session());
  app.use(flash());
  // app.get('/logout', module.exports.logout);
  app.get('/userInfo', module.exports.userInfo);

  /**
   * Login - using local strategy
   */
  /*
    app.post('/login',
        passport.authenticate('local', { successRedirect: '/',
          failureRedirect: loginPage,
          failureFlash: true })

    );
  */
  app.post('/logout', function (req, res, next) {
    removeUser(req, res);
    req.logout();
    return res.send(200);
  });

  app.post('/login', function (req, res, next) {
    logger.debug('login response ', res)
    passport.authenticate('local', function (err, user, info) {
      if (!user) {
        var reqBody = JSON.stringify(req.body);
        var reqBodySafe = reqBody;
        if (req.body.password) {
          var passwd = req.body.password;
          req.body.password = "*masked*";
          reqBodySafe = JSON.stringify(req.body);
          req.body.password = passwd;
        }
        logger.debug('Failed to authenticate user. Request:' + reqBodySafe);
        var statusCode = err && err.statusCode ? err.statusCode : 401;
        logger.debug('Failed Response to browser:  ' + statusCode + ' ' + (info ? info.message : 'None'));
        logger.debug('Failed error ' + JSON.stringify(info));

        return res.send(statusCode, {success: false, message: info ? info.message : ''});
      }
      logger.debug('authenticating user ' + user.username);
      req.logIn(user, function (err) {

        if (err) {
          logger.error('error in req.logIn. ' + JSON.stringify(err));
          return next(err);
        }
        res.cookie(sessionCookieName, user.session, {path: '/', httpOnly: true, secure: localConfig.ssl});
        return res.send(200, {success: true, path: '/'});
        ////if (user.redirect) {
        ////  var redirectUrl = (req.body.daUrl &&
        ////      req.body.daUrl !=='' &&
        ////      !startsWith(req.body.daUrl,'/api/'))?
        ////        req.body.daUrl:'/';
        ////  logger.info('Redirecting after successful login. redirect='+redirectUrl);
        ////  return res.redirect(redirectUrl);
        //}
        //else {
        //  logger.info('Re-authentication successful. params=' + JSON.stringify(req.body));
        //  return res.status(200);
        //}
      });
    })(req, res, next);
  });
  logger.trace('Authentication init done user ');
};

var isAuthenticatedByCookie = function (req) {
  return (req.cookies && req.cookies[sessionCookieName] && users && users[req.cookies[sessionCookieName]]);
}
module.exports.isAuthenticatedByCookie = function (req) {
  return isAuthenticatedByCookie(req);

}
var getCurrentUserDetails = function (req) {
  var user = users ? users[req.cookies[sessionCookieName]] : null;
  return user;
}

module.exports.getCurrentUserDetails = function (req) {
  return getCurrentUserDetails(req);
}

module.exports.getRedirectUrlAfterlogin = function (req) {
  var returnUrl = getURLParameter(`${req.url}`, '_u')
  if (returnUrl) {
    // console.log('returned url', returnUrl)
    return decodeURIComponent(returnUrl);
  }
  return '/';
}

module.exports.getUserByToekn = function(token) {
  return users[token];
}

module.exports.ensureAuthenticated = function (req, res, next) {
  // if((res && res.error == 404) )
  // {
  //   return next();
  // }

  //The following three lines are for reverse proxies that works directly with webpack dev server (port 4200)
  if (req.url.indexOf(loginPage) >= 0) {
    return next();
  }
  if (isAuthenticatedByCookie(req)) {
    var user = users[req.cookies[sessionCookieName]];
    logger.info(req.url + ' authenticated passed');
    return next();  // pass control to the next handler
  }
  // }
  logger.info('401 ensureAuthenticated failed for url ' + req.url);
//  the redirect here  was removed since we need to let the app redirect intself to login page with the address url.
// the ajax request does not have the address and also every request gets redirect causing multiple redirects and override the return url
  logger.info('redirecting to ' + loginPage );
  res.redirect(loginPage + '?_u=' + encodeURIComponent(req.url));
  //return res.send(401);
};

module.exports.apiProxy = function (req, res, next) {
  var target = serverUrl;
  var opt = {
    target: serverUrl
    //ssl: {
    //key: fs.readFileSync('valid-ssl-key.pem', 'utf8'),
    //cert: fs.readFileSync('valid-ssl-cert.pem', 'utf8')
    //key: fs.readFileSync('Keys/keys/userA.key', 'utf8'),
    //cert: fs.readFileSync('Keys/certs/userA.crt', 'utf8')
    //},
    //secure: true
  };
  proxy.web(req, res, opt, function (err) {
    logger.info('Proxy error ', JSON.stringify(err));
  });
};

const freeUrlsForXSRF = ['api/files/content','api/logs/fetch' ];

module.exports.setProxy = function (pattern) {
  var ret = function (req, res, next) {
    var pat = new RegExp(pattern, '');
    if (!pat.test(req.url)) {
      next();
    } else  {
      //console.log('Cookeis='+JSON.stringify(req.cookies));
      if (!isAuthenticatedByCookie(req)) {
        logger.error('Session data not found. url=' + req.url);
        res.status(401);
        res.send('Session user data not found');
      }  else {

        var user = users[req.cookies[sessionCookieName]];
        if (!user.session) {
          logger.error('user token missing');
        }
        const passFree = !!freeUrlsForXSRF.find(url => req.url.indexOf(url) !== -1)
        if (passFree) {
          logger.debug('XSRF Pass free for url: ' + req.url);
        }
        if (!suppress_xsrf && !passFree) {
          logger.debug('XSRF Start validation for url: ' + req.url);
          var antiForgeryToken = req.headers[envConfig.csrf_token_header_key.toLowerCase()];

          if (!antiForgeryToken) {
            logger.error(' XSRF Header ' + envConfig.csrf_token_header_key + ' is missing. User data not yet reached client' + req.url);
            res.status(403);
            res.send('Forbidden - missing header');
            return;
          }
          else if (antiForgeryToken !== user.username && antiForgeryToken !== envConfig.csrf_token_header_freepass_value) //TODO: change to token
          {
            logger.error(`UNAUTHORIZED- Session belongs to another user: Not ${antiForgeryToken} but ${user.username}`);
            res.status(401);
            res.send('UNAUTHORIZED - Session belongs to another user');
            return;
          }
          //    logger.debug('XSRF Validation passed ' );
        }
        //     req.headers.Authorization = user.token;
        //   delete req.cookies[sessionCookieName];
        proxy.web(req, res, {target: serverUrl});
      }
    }

  };
  return ret;
};
var removeUser = function (req, res) {
  if (req.cookies && req.cookies[sessionCookieName] && users[req.cookies[sessionCookieName]]) {
    var session = req.cookies[sessionCookieName];
    var user = users[session];
    logger.info("LOGGING OUT " + user.username);

    var logoutOptions = {
      hostname: serverHost,
      port: serverPort,
      path: '/logout',
      method: 'GET',
      headers: {
        Cookie: sessionCookieName + '=' + session
      }
    };

    var logoutReq = http.request(logoutOptions, function () {
    }, function (err) {
      logger.error(err);
    });
    logoutReq.end();
    delete users[session];
  }
}
var removeUserLocally = function (req, res) {
  if (req.cookies && req.cookies[sessionCookieName] && users[req.cookies[sessionCookieName]]) {
    var session = req.cookies[sessionCookieName];
    var user = users[session];
    logger.info("LOGGING OUT " + user.username);

    delete users[session];
  }
}
// module.exports.logout = function (req, res) {
//
//   removeUser(req, res);
//   req.logout();
//   res.redirect(loginPage);
// };

var roleName = function (roles) {
  var res = '';
  var first = true;
  roles.forEach(function (r) {
//    res += (first?'':',') + (r.description?r.description:'');
    res += (first ? '' : ';') + (r.name ? r.name : '');
    first = false;
  });
  return res;
};

module.exports.userInfo = function (req, res) {
  var info = {};
  var user ;
  if (req.cookies && req.cookies[sessionCookieName] && users[req.cookies[sessionCookieName]]) {
    var sessionToken = req.cookies[sessionCookieName];
    if (configurationList === null) {
        var reqOptions = {
          hostname: serverHost,
          port: serverPort,
          path: '/api/system/settings/prefix?prefix=',
          headers: {
            'Content-Type': 'application/json',
            Cookie: `JSESSIONID=${sessionToken}`
          }
        };
        const request = http.get(reqOptions, resp => {
          resp.on("error", err => {
            console.error(`Error getting system configuratioon `, err);
          });

          let data = '';
          resp.on('data', (chunk) => {
            data += chunk;
          });


          resp.on('end', () => {
            configurationList = JSON.parse(data);
            user = users[sessionToken];
            user.configurationList = configurationList;
            res.send(user);
          });
        });
        request.end();
    }
    else
      {
        user = users[sessionToken];
        user.configurationList = configurationList;
        //logger.debug('get user info for ' + user.username);
        info = {
          username: user.username,
          name: user.name,
          roleName: user.roleName,
          bizRoleDtos: user.bizRoleDtos,
          passwordMustBeChanged: user.passwordMustBeChanged,
          systemRoleDtos: user.systemRoleDtos,
          userType: user.userType,
          loginTime: user.loginTime.getTime(),
          highlightFileContentTextSupport: localConfig.highlightFileContentTextSupport || envConfig.highlightFileContentTextSupport,
          dashboards: localConfig.dashboards || {ITSysRole: 'RunScans', AdminSysRole: 'ViewContent'},
          isDemoInstallation: !!localConfig.isDemoInstallation
        };
        res.send(user);
      }
  }
};

const pollingProxy = require('./server-push-notifications')
const config = require('../../config/local.env')
const authentication = require('../authenticate');
const URL = require('url');
const buildRequestOptions = require('./request-option-builder')
const assembleResponse = require('./assemble-server-response')

const authorizationFn = (conn) => {
  if (!conn.cookies && conn.cookies && conn.cookies.JSESSIONID) {
    return false;
  }
  const user =  authentication.getUserByToekn(conn.cookies.JSESSIONID)
  if (!user || !user.bizRoleDtos || !user.bizRoleDtos[0].template) {
    return false;
  }
  const roles = user.bizRoleDtos[0].template.systemRoleDtos;
  return roles.find(role => role.name === 'GeneralAdmin')
}



const pollServer = (subjectName, path, interval) => {
  pollingProxy.connectionAdded.subscribe((connection) => {
    pollingProxy.sendCachedData('errors', conn => authorizationFn(conn), connection)
    pollingProxy.sendCachedData('fatals', conn => authorizationFn(conn), connection)

  })
  pollingProxy.poll(subjectName, conn => authorizationFn(conn), (jSessionId) => {
    const http = require('http')
    const url = new URL.parse(config.server_url);
    return new Promise((resolve, reject) => {
      http.get({...buildRequestOptions(jSessionId), path: path}, resp => assembleResponse(resp, resolve)
      ).on('error', err => {
        console.error(`Error getting ${subjectName}`, err, url)
        reject(err)
      })

    })
  }, interval)
}


const pollFatals = () => {
  pollServer('fatals',`/api/run/logmon/fatal`, 5000 )
}





const pollErrors = () => {
  pollServer('errors',`/api/run/logmon/error`, 5000 )
}

module.exports = {pollFatals, pollErrors}


const connections = [];
const pubSubInstance = require("./client-messages-pubsub");
const addDocTypeItem = require('./add-remove-doctypes');
const {Subject} = require('rxjs');
const sockjs = require("sockjs");
const serverSocket = sockjs.createServer({ jsessionid: true });

// Whenever a connection is established, use its cookies for authentication
serverSocket.on("connection", conn => {
  connections.push(conn);

  conn.cookies = getConnectionCookies(conn);
  conn.on("data", message => {
    const { subject, args } = JSON.parse(message);
    console.info(" message", message);
    pubSubInstance.publish(conn, subject, args);
    // conn.write(message)
  });

  conn.on("close", () => {
    connections.splice(connections.indexOf(conn), 1);
  });
});

const getConnectionCookies = function(conn) {
  const cookieHeader =
    conn._session.recv.ws._stream._readableState.pipes._driver._request.headers
      .cookie;
  const cookies = {};
  cookieHeader.split(";").forEach(cookie => {
    const parts = cookie.split("=");
    cookies[parts.shift().trim()] = decodeURI(parts.join("="));
  });

  return cookies;
};



addDocTypeItem();

class PollingpProxy {
  constructor(connections, serverPushSocket ) {
    this.connections = connections;
    this.serverPushSocket = serverPushSocket;
    this.cachedData = {};
    this.connectionAdded = new Subject();
    this.serverPushSocket.on('connection', conn => {
        this.connectionAdded.next(conn);
    })
  }

  getAuthorizedConnections(authorizationFn) {
    if (typeof authorizationFn !== "function") {
      console.error("autheticatorFn is not a function", authorizationFn);
      return [];
    }
    const authConnections = this.connections.filter(conn =>
      authorizationFn(conn)
    );
    return authConnections
  }

  sendCachedData(subject, authorizationFn, conn){
    console.log('send cache data')
    if (!this.cachedData[subject] || !authorizationFn(conn)) {
      return;
    }
     conn.write(  JSON.stringify({ subject, result: JSON.stringify(this.cachedData[subject]) }))

  }

  poll(subject, authorizationFn, executionFn, interval) {
    if (typeof authorizationFn !== "function") {
      console.error("autheticatorFn is not a function", authorizationFn);

    }
    const authConnections =  this.getAuthorizedConnections(authorizationFn);
    if (!authConnections[0]) {
      setTimeout(
        () => this.poll(subject, authorizationFn, executionFn, interval),
        interval
      );
      return;
    }
    executionFn(authConnections[0].cookies.JSESSIONID).then(
      // Send result to all authorized connections
      result => {
        authConnections.forEach(conn => {
          this.cachedData[subject] = result;
          conn.write(
            JSON.stringify({ subject, result: JSON.stringify(result) })
          );
        });
        setTimeout(
          () => this.poll(subject, authorizationFn, executionFn, interval),
          interval
        );
      },
      err => {
        console.error("error", err);
        authConnections.forEach(conn => {
          conn.write(JSON.stringify({ subject, result: JSON.stringify(err) }));
        });
        setTimeout(
          () => this.poll(subject, authorizationFn, executionFn, interval),
          interval
        );
      }
    );
  }
}
const pollingProxy = new PollingpProxy(connections, serverSocket)


module.exports = pollingProxy;

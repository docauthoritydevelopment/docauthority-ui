class ClientMessagesPubsub {
    
    constructor() {
        this.subjects = [];
    }
    getSubjectById(subjectId) {
        return this.subjects.find(subject => subject.id === subjectId );
    }
    subscribe(subjectId, callbackFn) {
        if (!callbackFn) {
            return;
        }
        if (typeof callbackFn !== 'function') {
            throw({message: 'Callback fn is not a function', data: callbackFn})
        }
        let subject = this.getSubjectById(subjectId);
        if (!subject) {
            subject = {id: subjectId, callbacks: []};
            this.subjects.push(subject);
        }

        subject.callbacks.push(callbackFn);
    }

    publish(conn, subjectId, data) {
        console.info('Publishing', conn, subjectId, data) 
        const subject = this.getSubjectById(subjectId);
        if (!subject || !subject.callbacks) {
            return;
        }
        subject.callbacks.forEach(callbackFn => {
            callbackFn(conn, ...data);
        });
    }
}

module.exports = new ClientMessagesPubsub();
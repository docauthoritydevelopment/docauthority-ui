const Stream = require('stream').Transform
const assembleStream = (response, resolve) => {
    const data = new Stream();
    response.on('data', function(chunk) {                                       
        data.push(chunk);                                                         
      });   
      response.on('end', function() {                                             
       resolve(data.read())            
      });        
}

module.exports = assembleStream;
const config = require('../../config/local.env')
const URL = require('url');


const buildRequestOptions = (sessionId) => {
    const url = new URL.parse(config.server_url);
    return {
      protocol: url.protocol,
      host: url.hostname,
      port: url.port,
      headers: {
        Cookie: `JSESSIONID=${sessionId}`,
  
      }
    }
  }

  module.exports = buildRequestOptions;

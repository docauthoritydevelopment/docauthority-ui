const assembleStream = require('./assemple-stream-response');

const assembleResponse = (resp, resolve) => {
    if (resp.headers['content-type'].indexOf('image') >= 0) {
         assembleStream(resp, resolve);
      return;
    }
    let data = '';
  //  resp.setEncoding('binary')
    // resp.headers['content-type'] = 'image/png'
    resp.on('data', (chunk) => {
      
      data += chunk;
    });
    resp.on('end', () => {
      try {
        resolve(JSON.parse(data));
      } catch (err) {
        // Resolve without parsing
        console.log('data is not json, returnd without parsing', data)
        resolve(data)
      }
    });
  
  }

  module.exports = assembleResponse;

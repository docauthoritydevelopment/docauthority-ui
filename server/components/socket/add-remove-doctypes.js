const assembleResponse = require("./assemble-server-response");
const buildRequestOptions = require("./request-option-builder");
const pubSubInstance = require("./client-messages-pubsub");

const geturlByDisplayType = (displayType, docTypeId, fileId) => {
  switch (displayType.value) {
    case "groups": {
      return `/api/doctype/${docTypeId}/bid/${fileId}`;
    }
    case "files": {
      return `/api/doctype/${docTypeId}/file/${fileId}`;
    }
    case "folders": {
      return `/api/doctype/${docTypeId}/folder/${fileId}`;
    }
    default: {
      throw { message: `displayType ${displayType.value} is not supported` };
    }
  }
};

const addDocTypeItem = () => {
  pubSubInstance.subscribe("addDocTypeToItems", (conn, data) => {
    const { docTypeId, fileIds, displayType } = data;
    const promises = [];
    const requestOptions = buildRequestOptions(conn.cookies.JSESSIONID);
    requestOptions.method = "PUT";
    fileIds.forEach(fileId => {
      const http = require("http");
      const path = geturlByDisplayType(displayType, docTypeId, fileId);
      // const url = new URL.parse(config.server_url);
      const promise = new Promise((resolve, reject) => {
        requestOptions.path = path;
        const payload = JSON.stringify({
          docTypeId: docTypeId,
          groupId: fileId
        });
        requestOptions.headers["Content-Length"] = Buffer.byteLength(payload);
        requestOptions.headers["Content-Type"] = "application/json";
        const request = http.request(requestOptions, resp => {
          assembleResponse(resp, resolve);
          resp.on("error", err => {
            console.error(`Error adding doctype`, err);
            reject(err);
          });
        });

        request.write(payload);
        request.end();
      });
      promises.push(promise);
    });
    Promise.all(promises).then(
      values => {
        console.log("add toctypes result", values);
        conn.write(
          JSON.stringify({ subject: "addDocTypeToItems", result: "complete" })
        );
      },
      err => {
        console.error("error adding doctypes", data, err, requestOptions);
        conn.write(
          JSON.stringify({
            subject: "addDocTypeToItems",
            result: { error: err }
          })
        );
      }
    );
  });
}


module.exports = addDocTypeItem;

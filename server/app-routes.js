/**
 * Express configuration
 */

'use strict';

var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var envConfig = require('./config/environment');
var versionFile = '../version.txt';

var fs = require('fs');
var logFolder = './log';
if (!fs.existsSync(logFolder)){
  fs.mkdirSync(logFolder);
}
const getLocalConfig = require('./components/daUtils/local-config')

var version = null;
if (fs.existsSync(versionFile)) {
  version = fs.readFileSync(versionFile, 'utf8');
}
var log4js = require('log4js');

var httpProxy = require('http-proxy');
var angularDevServer = 'http://localhost:4200';


var apiProxy = httpProxy.createProxyServer();

var isStaticFileRequest = function (urlStr) {
  if (urlStr.length < 6) {
    return false;
  }
  return urlStr.indexOf(".", urlStr.length - 6) !== -1;
}

process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' , err);
});
let localConfig = getLocalConfig();
log4js.configure({
  appenders: [
    { type: 'console' },
    {
      "type": "file",
      "filename": "log/node.log",
      "maxLogSize":localConfig.maxLogSize|| 70240,
      "backups": localConfig.maxLogFiles||10
    }
  ],
  replaceConsole: true
});
log4js.setGlobalLogLevel('DEBUG');


// Custom Morgan formatting
morgan.token('timeP', function(){
  var d=new Date();
  return ''+ d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()+'.'+d.getMilliseconds();
});
morgan.format('tinyP', ':timeP :method :url :status :res[content-length] - :response-time ms');
morgan.format('devP', function(tokens, req, res){
  return morgan.timeP(req)+' '+morgan.dev(tokens, req, res);
});

//var logger = log4js.getLogger(module.id.substring(__dirname.length+1));
var logger = require('./components/daUtils').getLogger(module.id);

function updatedDevProEnv(app, v1_root_pro, v2_root_pro, v1_root_dev, v2_root_dev, localConfig) {
  var devOnly = true;
  var static_root = envConfig.root;
  if ('production' === app.get('env') || ('dual' === app.get('env') && app.get('envMode') == 'production')) {
    devOnly = false;
    app.set('appPathV1', static_root + v1_root_pro);
    app.set('appPathV2', static_root + v2_root_pro);

    // app.use(favicon(path.join(app.get('appPathV2'), '/assets/favicon.ico')));
    app.use(morgan('tinyP'));
    log4js.setGlobalLogLevel(process.env.LOGLEVEL || 'INFO');
    // } else if (devOnly) {
  } else {
    app.set('appPathV1', static_root + v1_root_dev);
    app.set('appPathV2', static_root + v2_root_dev);
    // app.use(favicon(path.join(app.get('appPathV2'), '/assets/favicon.ico')));
    app.use(morgan('devP'));
    app.use(errorHandler()); // Error handler - has to be last

    var livereloadPort = localConfig.livereloadPort || 4141;
    var livereloadOptions = {
      port: livereloadPort
    };

    // This module inject a script to the end of each html 'body' section referring to the middleware listenning at the livereload port.
    if (livereloadPort > 0) {
      try {
        app.use(require('connect-livereload')(livereloadOptions));
        console.info('Livereload options: ' + JSON.stringify(livereloadOptions));
      } catch (e) {
        console.info('Livereload off (module connect-livereload not present).');
      }
    }
    else {
      console.info('Livereload off (set by local.env).');
    }
    log4js.setGlobalLogLevel(process.env.LOGLEVEL || 'DEBUG');
  }
  console.info('appPathV1=' + app.get('appPathV1') + ', appPathV2=' + app.get('appPathV2'));
  return devOnly;
}

module.exports = function(app, authenticate, appConfig) {
  var oneYear = 60 * 60 * 24 * 365;

  let localConfig = getLocalConfig();

  var env = app.get('env');
  if (localConfig.env && localConfig.env !== env) {
    env = localConfig.env;
    app.set('env', env);
    logger.info('Env forced to: ' + localConfig.env);
  }
  var server_url =  localConfig.server_url || envConfig.server_url;
  var initEnvMode = localConfig.envMode || 'production';
  version = localConfig.version || version || 'v3.0.0';
  logger.info(logger.highlight('Server url: '+server_url));
  logger.info('Version string: ' + version);
  logger.info('EnvMode: ' + initEnvMode);

  if (localConfig.logLevel) {
    log4js.setGlobalLogLevel(localConfig.logLevel);
    console.info('Setting global logLevel to '+ localConfig.logLevel);
  }

  if (localConfig.ssl) {
    logger.info('Server set for SSL. Key-file='+ localConfig.ssl_key +', certificate-file='+localConfig.ssl_certificate);
    if (!fs.existsSync(localConfig.ssl_key)) {
      logger.error("Key file not found: "+ localConfig.ssl_key);
    }
    if (!fs.existsSync(localConfig.ssl_certificate)) {
      logger.error("Certificate file not found: "+ localConfig.ssl_certificate);
    }
    app.set('ssl', true);
    app.set('ssl_key', fs.readFileSync(localConfig.ssl_key));
    app.set('ssl_cert', fs.readFileSync(localConfig.ssl_certificate));
  }

  app.set('envMode', initEnvMode);
  app.set('views', envConfig.root + '/server/views');
  app.set('server_url', server_url);
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  if (localConfig.port && localConfig.port>0 && appConfig.port != localConfig.port) {
    console.info('Setting port from localConfig to '+ localConfig.port + ' (env port=' + appConfig.port + ')');
    appConfig.port = localConfig.port;
  }

  app.use(compression());
  app.use(cookieParser());
  app.use(methodOverride());
  app.use(authenticate.setProxy('/api/.*'));  // Should be before 'bodyParser' processing as it result POST null body forwarding.
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  var v1_root_pro = localConfig.v1_root_pro || envConfig.v1_root_pro || '/client-angularjs/dist';
  var v1_root_dev = localConfig.v1_root_dev || envConfig.v1_root_dev || '/client-angularjs';
  var v2_root_pro = localConfig.v2_root_pro || envConfig.v2_root_pro || '/client-angular/public';
  var v2_root_dev = localConfig.v2_root_dev || envConfig.v2_root_dev || '/client-angular/dist';
  var demo_info_file_dev = '../mocks/demoInfo.json';
  var demo_info_file_pro = './mocks/demoInfo.json';

  //app.use(session({secret: 'superSecretDocAuthority', saveUninitialized: true, resave: true}));
  // var devOnly = ('development' ===  app.get('env') || ('dual' === env &&  app.get('envMode')!=='production' ) || 'test' ===  app.get('env'));
  var devOnly = updatedDevProEnv(app, v1_root_pro, v2_root_pro, v1_root_dev, v2_root_dev, localConfig);

  authenticate.initialize(app,localConfig,envConfig);

  const activeMqProxy = require('./components/active-mq-connector/active-mq-route');
  app.get('/unauthorized',function(req, res,  next) {
    if (authenticate.isAuthenticatedByCookie(req)){
      var redirectUrl = authenticate.getRedirectUrlAfterlogin(req);
      if(redirectUrl)
      {
        logger.info(redirectUrl);
        return res.redirect(redirectUrl);
      }
      return res.redirect('/');
    }
    next();
  });
  app.route('/active-mq-url*').get(authenticate.ensureAuthenticated, activeMqProxy)

  app.get('/login.html', function(req, res,  next) {
    if (authenticate.isAuthenticatedByCookie(req)){
      logger.info(req.url +' was requested. Redirected to ');
      var redirectUrl = authenticate.getRedirectUrlAfterlogin(req);
      if(redirectUrl)
      {
        return res.redirect(redirectUrl);
      }
      return res.redirect('/');
    }
    var sf = app.get('appPathV1') + '/login.html';
 //   console.log('sending ' + sf );
    res.sendfile(sf, null, function (err) {
     // console.log('Error sending ' + sf , err);
      res.status(400).end();
    });
  });

  app.use('/v1', authenticate.ensureAuthenticated);
  app.use('/', authenticate.ensureAuthenticated);
  if (devOnly) {

    app.use('/v1', express.static(app.get('appPathV1') + '/src'));
  }
  else {
    app.use('/v1', express.static(app.get('appPathV1')+'/public', { maxAge: oneYear }));
  }
  app.use('/v1', express.static(app.get('appPathV1'))); //maxage should be here 0 (otherwise index.html will be cached forever).

  if ('development' !==  app.get('env') ) { //Angular 5 in development is served from proxy, all files are in memory and not on disk
    var pathToStaticFiles = app.get('appPathV2');
    logger.debug('Serve V2 static files from: ', pathToStaticFiles);
    app.use(express.static(pathToStaticFiles));
  }

  logger.debug('Init route handlers');




  app.get('/sock-js*',function(req, res,  next) {
    res.status(409).end();
  });
  app.get('/sockjs-node/*',function(req, res,  next) {
    res.status(409).end();
  });


  var endsWith = function(str, suffix) {
      return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

  app.get('/poll/fatals', (req, res) => {
    const pollers = require('../components/')
    res.send(200).end();
  })

  var removeQueryString = function(url) {

    var indexOfQuestionMark = url.indexOf('?');
    if (indexOfQuestionMark > 0) {
      url = url.substring(0, indexOfQuestionMark);
    }
    return url;
  }

  app.route('/v1/*').get(authenticate.ensureAuthenticated,
      function(req, res, next) {
          var urlReq = removeQueryString(req.url);

          if (isStaticFileRequest(urlReq)) {
            //If a direct page was requested, respond with a 404 page
            // var sf =  urlReq.substring('/v1'.length);
            // res.sendfile(sf, {root: app.get('appPathV1')}, function (err) {
            //   if (err) {
            //     logger.error('Error sending v1 file ' + sf, err);
            //   }
            //   // res.status(err.status?err.status:404).end();
            // });
            logger.error('V1 file requested, move to static handling from ' + app.get('appPathV1')+ ': '+req.url);
    //       next();
          } else {
            // if ('production' ===  app.get('env') || ('dual' ===  app.get('env')  && app.get('envMode')=='production')) {
            //   logger.debug('sending app-v1-index in dual or production mode', sf);
            //   res.sendfile(app.get('appPathV1') + '/index.html', null, function (err) {
            //     console.log('Error sending V1 dual prod index.html ' + err);
            //     // res.status(err.status).end();
            //   });
            //
            // }
            // else { //dev
            var sf = app.get('appPathV1') + '/index.html';
            //used in case app is opened in a different tab (i.e. directly with v1)
            res.sendfile(sf, null, function (err) {
              if (err) {
                logger.error('Error sending app-v1-index ' + sf + '.', err);
              }
              //  res.status(err.status).end();
            });
            // }
          }
      });

  app.route('/getVersion').get(function(req, res, next) {
    res.json({version: version});
    res.status(200);
  });

  app.route('/demoInfo').get(function(req, res, next) {
    let demoInfoFile = app.get('env') ===  'production' ? demo_info_file_pro : demo_info_file_dev;
    console.log("demoInfoFile:" + demoInfoFile);

    function sendRes(data){
      res.json({data: data});
      res.status(200);
    }

    fs.exists(demoInfoFile, (exists) => {
      if(exists) {
        fs.readFile(demoInfoFile, 'utf8', function(err, data){
          if (err) {
            sendRes(null);
          }else {
            sendRes(data);
          }
        });
      } else {
        sendRes(null);
      }
    });
  });
  app.route('/setDev').get(authenticate.ensureAuthenticated,
      function(req, res) {
        logger.info('Set mode to development');
        app.set('envMode', 'development');
        devOnly = updatedDevProEnv(app, v1_root_pro, v2_root_pro, v1_root_dev, v2_root_dev, localConfig);
        res.send('Set mode to development');
        res.status(200);
      });
  app.route('/setPro').get(authenticate.ensureAuthenticated,
      function(req, res) {
        logger.info('Set mode to production');
        app.set('envMode', 'production');
        devOnly = updatedDevProEnv(app, v1_root_pro, v2_root_pro, v1_root_dev, v2_root_dev, localConfig);
        res.send('Set mode to production');
        res.status(200);
      });

  app.route('/*').get(authenticate.ensureAuthenticated,
    function(req, res,next) {
      var urlReq = req.url;
      if ('dual' === app.get('env') ||  'production' ===  app.get('env')) {
        // V2 Production
        var indexOfQuestionMark = urlReq.indexOf('?');
        if (indexOfQuestionMark > 0) {
          urlReq = urlReq.substring(0, indexOfQuestionMark);
        }
        if (isStaticFileRequest(urlReq)) { //static files are served from static files configuration
          if (endsWith(urlReq, "html") || endsWith(urlReq, "htm")) {
            // logger.info('Sending V2 from static files - status 406 for: '+ req.url);
            // res.status(406).end();
            //  res.sendfile(app.get('appPath') + '/404.html');
          }
 //         next();
        } else {
          var indexUrl = app.get('appPathV2')+'/index.html';
          logger.debug('>>> Sending V2 index.html from dist folder ' + indexUrl + ' for url: '+ req.url);
          res.sendfile(indexUrl, null, function (err) {
            if (err) {
              logger.error('Error sending V2 index.html ' + indexUrl + '.', err);
            }
          //    res.status(err.status).end();
            });
          }
      } else { //dev mode for angular 5 : all files are served from proxy
        // V2 Production
        logger.info('>>> Sending V2 req via angular-cli server ' + req.url);
        apiProxy.web(req, res, {target: angularDevServer});
      }
    });

  logger.trace('Exports set');
};

/**
 * Main application file: server/app.js
 *
 * Routing overview:
 * -----------------
 *            Itay Reved / May 2015
 *
 * /<client-dir>/*      - static route for all files that reside under the client dir ('client' for dev or 'public' in production).
 *                        The client logic is an AngularJS single page application configured at 'client/app.js'.
 * /api/*               - set as a webProxy that is forwarded to the backend server with the same URL. Session token is forwarded
 * /login, /logout      - special login/logout services. Session is authenticated with the backend server and copied to the client-node session.
 * /<node-services-url> - Specific services implemented at the middleware (e.g. /userInfo )
 * /index.html          - Application load page
 * /*                   - All other URLS are served with the application page (index.html). The actual logic depends on the URL
 *
 * All pages are checked for valid session authentication. If authentication fails, page is redirected to /login.html
 *
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var  spdy = require('spdy');
var config = require('./config/environment');
// Setup server
var app = express();

 var authenticate = require('./components/authenticate');

require('./app-routes')(app, authenticate, config);
var server = null;
var logger = require('./components/daUtils').getLogger(__filename);

//app.use(authenticate.setProxy('/api/.*'));  // moved to 'config/express'.
if (app.get('ssl')) {
  var sslOptions = {
    key: app.get('ssl_key'),
    cert: app.get('ssl_cert')
  };



  const getLocalConfig = require('./components/daUtils/local-config');
  let localConfig = getLocalConfig();
  if (localConfig.disable_http2==undefined || localConfig.disable_http2 == true) {
    server = require('https').createServer(sslOptions, app);
    console.log('Creating https 1.1 connection');
  }
  else {
    server = spdy.createServer(sslOptions, app);
    console.log('Creating http2 connection');
  }

  // logger.debug("SSL options: " + JSON.stringify(sslOptions));
} else {
  server = require('http').createServer(app);
}
// const pollingProxy = require('./components/socket/server-push-notifications');
// pollingProxy.serverPushSocket.installHandlers(server, {prefix : '/socket/server-push-notifications'});
// const pollers = require('./components/socket/pollers')
// pollers.pollFatals()
// pollers.pollErrors();
// // Start server
// logger.debug('Config ='+JSON.stringify(config));
//
server.listen(config.port, config.ip, function () {
  logger.info('Express server listening on %s, in %s mode: %s', logger.highlight(''+config.port), logger.highlight(app.get('env')), app.get('envMode'));
});


// Expose app
exports = module.exports = app;

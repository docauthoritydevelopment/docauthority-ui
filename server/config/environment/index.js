'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV || 'dual',
  envMode: 'development',
  // Root path of server
  root: path.normalize(__dirname + '/../../..'),
  // Server port
  port: process.env.PORT || 9000,
  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'cgrunt lean-secret'
  },
  ip: '0.0.0.0',
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  server_session_cookie_key:'JSESSIONID',
  client_session_cookie_key:'JSESSIONID',
  csrf_token_header_key :'DA-XSRF',
  csrf_token_header_freepass_value :'FREE-PASS',
  suppress_xsrf:true,
  highlightFileContentTextSupport: true,
//server_url: 'mock',
  server_url: process.env.SERVER_URL || 'http://localhost:8080'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});

console.info('NODE_ENV='+ process.env.NODE_ENV);
console.info('All='+ JSON.stringify(all));

Creating self signed SSL key and certificate:

( Can use Git OpenSSL toolkit at - set path=%path%;"c:\Program Files (x86)\Git\bin" )

openssl genrsa -des3 -out server1.enc.key 1024

openssl req -new -key server1.enc.key -out server1.csr -config openssl.cnf

openssl rsa -in server1.enc.key -out server1.key

openssl x509 -req -days 3650 -in server1.csr -signkey server1.key -out server1.crt

openssl x509 -in server1.crt -text -noout

@echo off
cd client-angularjs

echo === Install bower and grunt-cli globaly
call npm --no-color install -g grunt
call npm --no-color install -g grunt-cli
if NOT ERRORLEVEL 0 echo Error %ERRORLEVEL%

echo === Install bower globally
call npm --no-color install -g bower
if NOT ERRORLEVEL 0 echo Error %ERRORLEVEL%

echo === Verifying npm cache
call npm --no-color cache verify
if NOT ERRORLEVEL 0 echo Error %ERRORLEVEL%

echo === Install npm dependencies for angular js
call npm --no-color install
if NOT ERRORLEVEL 0 echo Error %ERRORLEVEL%

echo === Install server
cd ../server
echo === Install npm dependencies for angular js
call npm --no-color install
if NOT ERRORLEVEL 0 echo Error %ERRORLEVEL%

echo Clean
cd ../client-angularjs
call === grunt --no-color clean
if NOT ERRORLEVEL 0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Prune bower dependencies
call bower --no-color --config.interactive=false prune
if NOT ERRORLEVEL 0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Install bower dependencies
call bower --no-color install --config.interactive=false
if NOT ERRORLEVEL 0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === List bower components installed versions
call bower --no-color list --offline --config.interactive=false
if NOT ERRORLEVEL 0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Build
call grunt --no-color buildDev
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Pack
call grunt --no-color packDev
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Prepare files for installer - dev
call grunt --no-color compress:dev
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Prepare files for installer - dist
call grunt --no-color compress:dist
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Prepare files for installer - production node-modules
pushd ..\server
call ..\prepare_dist_node_modules.bat tmp_prod_nm
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)
popd

echo === Prepare files for installer - pack node-modules
REM cd client-angularjs
call grunt --no-color compress:node
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

echo === Prepare files for installer - pack node-modules
REM cd client-angularjs
call grunt --no-color compress:server
if NOT %ERRORLEVEL%==0 (
	echo Error %ERRORLEVEL%
	exit /b %ERRORLEVEL%
)

cd ..
echo === UI V1 Prepare done


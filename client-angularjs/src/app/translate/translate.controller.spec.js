'use strict';

describe('Controller: translateCtrl', function () {

  // load the controller's module
  beforeEach(module('daApp'));

  var TranslateCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TranslateCtrl = $controller('translateCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});

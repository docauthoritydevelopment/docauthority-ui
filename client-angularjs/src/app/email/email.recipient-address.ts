///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('email.recipient-address',[

])
  .controller('RecipientAddressCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                       propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('EmailRecipientAddressCtrl');

    var _this = this;
    $scope.elementName='recipient-address-grid';
    $scope.entityDisplayType = EntityDisplayTypes.email_recipient_address;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOMailInfo>($scope,log,configuration,eventCommunicator,userSettings,getDataUrlParams,fields,parseData,rowTemplate,$stateParams,null, null, true);
      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);
    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOMailInfo> =  Entities.DTOAggregationCountItem.Empty();
    var dTOMailInfo:Entities.DTOMailInfo =  Entities.DTOMailInfo.Empty();
    var prefixFieldAddress= propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)+'.';


    var address:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldAddress+propertiesUtils.propName(dTOMailInfo, dTOMailInfo.address),
      title: 'address',
      type: EFieldTypes.type_string,
      displayed: true,
      isPrimaryKey:true,
      editable: false,
      nullable: true,

    };
    var addressNewTemplate =    '<span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  class="title-primary" title="Recipient Address: #: '+ address.fieldName+' #">#: '+ address.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      "<li><a  ng-click='addressClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this recipient address</a></li>"+
      '  </ul></span>';

    $scope.addressClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOMailInfo>) {

      setFilterByEntity(item.item.address,item.item.address);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };




    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(address);
    fields.push(numOfFiles);

    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column' style='position:relative'><span style='font-size:13px; top: 10px;left: 9px;position: absolute;' class='"+configuration.icon_recipient_address+"'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
      addressNewTemplate+"<div></div></td>" +
      '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
      "</tr>";

    var getDataUrlParams = function()
    {
      return null;
    };

    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[address.fieldName],$scope.selectedItem[address.fieldName]);
    }

    var parseData = function (mails:Entities.DTOAggregationCountItem< Entities.DTOMailInfo>[]) {
      if(mails) {
        mails.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOMailInfo>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2 );
        });
        return mails;
      }
      return null;
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('recipientAddressGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          totalElements: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          initialLoadParams: '=',
          findId: '=',
        },
      controller: 'RecipientAddressCtrl',
      link: function (scope:any, element, attrs) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }
    }
  });

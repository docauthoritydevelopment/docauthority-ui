///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('emails',[
  'email.recipient-address',
  'email.recipient.domain',
  'email.sender-address',
  'email.sender.domain'
]);

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('email.recipient.domain',[

])
  .controller('RecipientDomainCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                       propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('RecipientDomainCtrl');

    var _this = this;
    $scope.elementName='recipient-domain-grid';
    $scope.entityDisplayType = EntityDisplayTypes.email_recipient_domains;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOMailInfo>($scope,log,configuration,eventCommunicator,userSettings,getDataUrlParams,fields,parseData,rowTemplate,$stateParams,null, null, true);
      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);
    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOMailInfo> =  Entities.DTOAggregationCountItem.Empty();
    var dTOMailInfo:Entities.DTOMailInfo =  Entities.DTOMailInfo.Empty();
    var prefixFieldDomain= propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)+'.';


    var domain:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldDomain+propertiesUtils.propName(dTOMailInfo, dTOMailInfo.domain),
      title: 'domain',
      type: EFieldTypes.type_string,
      displayed: true,
      isPrimaryKey:true,
      editable: false,
      nullable: true,

    };
    var domainNewTemplate =    '<span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  class="title-primary" title="Recipient Domain: #: '+ domain.fieldName+' #">#: '+ domain.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      "<li><a  ng-click='domainClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this recipient domain</a></li>"+
      '  </ul></span>';

    $scope.domainClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOMailInfo>) {

      setFilterByEntity(item.item.domain,item.item.domain);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var parseData = function (mails:Entities.DTOAggregationCountItem< Entities.DTOMailInfo>[]) {
      if(mails) {
        mails.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOMailInfo>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2 );
        });
        return mails;
      }
      return null;
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(domain);
    fields.push(numOfFiles);


    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column'><span class='"+configuration.icon_domain+"'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
      domainNewTemplate+"<div></div></td>" +
      '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
      "</tr>";


    var getDataUrlParams = function()
    {
      return null;
    };

    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[domain.fieldName],$scope.selectedItem[domain.fieldName]);
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('recipientDomainGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          totalElements: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          initialLoadParams: '=',
          findId: '=',
        },
      controller: 'RecipientDomainCtrl',
      link: function (scope:any, element, attrs) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

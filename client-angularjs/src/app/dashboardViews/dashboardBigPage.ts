///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('dashboards.system',[
  'directives',
  'directives.dashboardWidgets',
  'resources.dashboards'
])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.dashboardSystem, {
        url: '/dashboardSystem',
        templateUrl: '/app/dashboardViews/dashboardBigPage.tpl.html',
        controller: 'DashboardBigPageCtrl'
      })
  })
  .controller('DashboardBigPageCtrl', function ($scope,$element,$state,$stateParams,$compile,$filter,$timeout,scanService:IScanService,dialogs,
                                             Logger:ILogger,propertiesUtils, configuration:IConfig,activeScansResource:IActiveScansResource,
                                             dashboardChartResource:IDashboardChartsResource,dashboardScanResource:IDashboardScanResource,pageTitle,dashboardAdminResource:IDashboardAdminResource) {
    var log:ILog = Logger.getInstance('DashboardBigPageCtrl');



      $scope.init=function()
    {
      pageTitle.set("Summary report");
      dashboardScanResource.getScanStatistics($scope.onGetScanStatisticsCompleted,$scope.onGetScanStatisticsError);
      dashboardAdminResource.getAdminDocumentStatistics($scope.onGetAdminDocumentCompleted,$scope.onGetScanStatisticsError);

      dashboardChartResource.getFileTypeHistogram(null,true,$scope.onFileTypeHistogramCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getGroupSizeHistogram($scope.onGroupSizeHistogramCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getFilesAccessHistogram(null,true, $scope.onGetFilesAccessHistogramCompleted,$scope.onGetScanStatisticsError);

    };


    var analyzedFilesFields:any[]  = [
      {
      field : 'Documents in groups' ,
      text : 'Documents in groups'
      },
      {
        field : 'Groups' ,
        text : 'Total groups'
      },
      {
        field : 'Ungrouped documents' ,
        text : 'Ungrouped documents'
      },
      {
        field : 'Non documents' ,
        text : 'Non documents'
      }
    ];


    var systemProgressFields:any[]  = [
      {
        field : 'Documents with Doc Types' ,
        text : 'Documents with DocTypes'
      },
      {
        field : 'Groups with Doc Types' ,
        text : 'Groups with DocTypes'
      },
      {
        field : 'Documents with no Doc Types' ,
        text : 'Documents without DocTypes'
      },
      {
        field : 'Groups with no Doc Types' ,
        text : 'Groups without DocTypes'
      }
    ];

    var parseCountList = function(fieldsList:any[], counterReportDtos:Dashboards.DtoCounterReport[]) {
      let ans:Dashboards.DtoCounterReport[] = [];
      fieldsList.forEach(fieldObj=>{
        if (fieldObj.field === '') {
          ans.push({
            id: null,
            name: null,
            description: null,
            counter: null,
            extraInfo: null,
            totalCounter: null
          });
        }
        else {
          let foundItem = counterReportDtos.filter(crDto => crDto.name === fieldObj.field);
          if (foundItem.length > 0) {
            ans.push({
              id: foundItem[0].id,
              name: fieldObj.text,
              description: foundItem[0].description,
              counter: foundItem[0].counter,
              extraInfo: null,
              totalCounter: null
            });
          }
        }
      })
      return ans;
    }

    var getCounter = function(counterReportDtos:Dashboards.DtoCounterReport[], itemName:string) : Dashboards.DtoCounterReport {
      for (var i=0;i<counterReportDtos.length;++i) {
        var item:Dashboards.DtoCounterReport = counterReportDtos[i];
        if (item.name==itemName) {
          return item;
        }
      }
      return null;
    };

    $scope.scanStatisticsHTML = '<list-report report-counter-data="scanStatistics"></list-report>';

    $scope.analyedSystemStatistics = [];
    $scope.systemProgressStatistics = [];

    $scope.onGetAdminDocumentCompleted = function(counterReportDtos:Dashboards.DtoCounterReport[]) {
      $scope.analyedSystemStatistics = parseCountList(analyzedFilesFields,counterReportDtos);
      $scope.systemProgressStatistics = parseCountList(systemProgressFields,counterReportDtos);;
    }


    $scope.onGetScanStatisticsCompleted=function(counterReportDtos:Dashboards.DtoCounterReport[])
    {
      $scope.scannedFilesCounter = getCounter(counterReportDtos, "scanned files");
      $scope.analyzedFilesCounter = getCounter(counterReportDtos, "analyzed files");
      $scope.aggregatedProcessedSize = getCounter(counterReportDtos, "aggregated processed size");
      $scope.processedFilesCount = getCounter(counterReportDtos, "processed files");
      $scope.processedFilesPercentStr = ($scope.scannedFilesCounter.counter>0)?("("+(Math.floor(1000*$scope.processedFilesCount.counter/$scope.scannedFilesCounter.counter)/10)+"%)"):"";
      $scope.ingestionErrorsCount = getCounter(counterReportDtos, "files with ingestion errors");
      $scope.analysisMaxFilesCount = getCounter(counterReportDtos, "documents files with content");
      $scope.documentsIngestionErrorCount = getCounter(counterReportDtos, "documents with errors");
      var analysisPotential:number = $scope.analysisMaxFilesCount.counter - $scope.documentsIngestionErrorCount.counter;
      $scope.analyzedFilesPercentStr = (analysisPotential>0)?("("+(Math.floor(1000*$scope.analyzedFilesCounter.counter/analysisPotential)/10)+"%)"):"";

    };
    $scope.onFileTypeHistogramCompleted=function(fileTypeHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileTypeHistogram = translateDtoMultiValueList(fileTypeHistogram);
    };
    $scope.onGroupSizeHistogramCompleted=function(groupSizeHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.groupSizeHistogram = translateDtoMultiValueList(groupSizeHistogram);
    };

    $scope.onGetFilesAccessHistogramCompleted=function(fileAccessHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileAccessHistogram = translateDtoMultiValueList(fileAccessHistogram);
    };

    $scope.onGetScanStatisticsError=function(e)
    {
      log.error("error get statistics " +e);
    };

    var translateDtoMultiValueList = function(val:Dashboards.DtoMultiValueList):Dashboards.DtoMultiValueList {
      var res:Dashboards.DtoMultiValueList = <Dashboards.DtoMultiValueList>{
        name: val.name,
        type: val.type,
        data: [],
      };

      for (var i:number=0;i<val.data.length;++i) {
        var counterItem:Dashboards.DtoCounterReport = val.data[i];
        res.data.push(new Dashboards.DtoCounterReport($filter('translate')(counterItem.name),counterItem.description, counterItem.counter));
      }
      return res;
    };

    $scope.$on('$destroy', function(){
    //  scanService.forceRunStatusRefresh(false);
    });
  });

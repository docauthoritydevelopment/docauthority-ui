///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('dashboards.it',[
  'directives',
  'directives.dashboardWidgets',
  'resources.dashboards'
])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.dashboardIT, {
        url: '/dashboardIT',
        templateUrl: '/app/dashboardViews/dashboardPage.tpl.html',
        controller: 'DashboardPageCtrl'
      })
  })
  .controller('DashboardPageCtrl', function ($scope,$element,$state,$stateParams,$compile,$filter,$timeout,scanService:IScanService,dialogs,
                                             Logger:ILogger,propertiesUtils, configuration:IConfig,activeScansResource:IActiveScansResource,
                                             dashboardChartResource:IDashboardChartsResource,dashboardScanResource:IDashboardScanResource,pageTitle) {
    var log:ILog = Logger.getInstance('DashboardPageCtrl');



      $scope.init=function()
    {
      pageTitle.set("Summary report");
      dashboardScanResource.getScanStatistics($scope.onGetScanStatisticsCompleted,$scope.onGetScanStatisticsError);
      dashboardScanResource.getScanCriteria($scope.onGetScanCriteriaCompleted,$scope.onGetScanStatisticsError);
      dashboardScanResource.getScanConfiguration($scope.onGetScanConfigurationCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getFileTypeHistogram(null, false, $scope.onFileTypeHistogramCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getGroupSizeHistogram($scope.onGroupSizeHistogramCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getFilesAccessHistogram(null,false ,$scope.onGetFilesAccessHistogramCompleted,$scope.onGetScanStatisticsError);

      // $scope.$on(scanService.activeRunsChangedEvent, function (sender, value) {
      //   getRunActiveStatusCompleted(scanService.getActiveRuns());
      // });
      // scanService.forceRunStatusRefresh(true);
    };
    //
    // var getRunActiveStatusCompleted = function(activeRunsView:Operations.ActiveRunsView) {
    //
    //   log.debug('get Run activeStatus Completed: '+activeRunsView.activeRuns.length);
    //   $scope.isRunning = activeRunsView.activeRuns.length>0;
    //   $scope.statusLabelText = $scope.isRunning?'Scanning': $scope.lastRunFailed?'Failed':$scope.lastRunFinished?'Ready':'Ready';
    //   //$scope.statusLabelClass = $scope.isRunning?'label-warning': $scope.lastRunFailed?'label-error':$scope.lastRunFinished?'label-primary':'label-primary';
    //
    // };

    var getCounter = function(counterReportDtos:Dashboards.DtoCounterReport[], itemName:string) : Dashboards.DtoCounterReport {
      for (var i=0;i<counterReportDtos.length;++i) {
        var item:Dashboards.DtoCounterReport = counterReportDtos[i];
        if (item.name==itemName) {
          return item;
        }
      }
      return null;
    };


    $scope.scanStatisticsHTML = '<list-report report-counter-data="scanStatistics"></list-report>';

    $scope.onGetScanStatisticsCompleted=function(counterReportDtos:Dashboards.DtoCounterReport[])
    {
      $scope.scanStatistics = counterReportDtos;
      $scope.scanStatisticsAcls = [
        getCounter(counterReportDtos, "distinct owners"),
        getCounter(counterReportDtos, "distinct acl read"),
        getCounter(counterReportDtos, "distinct acl write")
      ];
      $scope.scannedFilesCounter = getCounter(counterReportDtos, "scanned files");
      $scope.analyzedFilesCounter = getCounter(counterReportDtos, "analyzed files");
      $scope.sensitiveFilesCounter = getCounter(counterReportDtos, "sensitive data files");
      $scope.sensitiveGroupsCounter = getCounter(counterReportDtos, "sensitive data groups");
      $scope.aggregatedProcessedSize = getCounter(counterReportDtos, "aggregated processed size");
      $scope.processedFilesCount = getCounter(counterReportDtos, "processed files");
      $scope.ingestionErrorsCount = getCounter(counterReportDtos, "files with ingestion errors");
      $scope.analysisMaxFilesCount = getCounter(counterReportDtos, "documents files with content");
      $scope.documentsIngestionErrorCount = getCounter(counterReportDtos, "documents with errors");
      var analysisPotential:number = $scope.analysisMaxFilesCount.counter - $scope.documentsIngestionErrorCount.counter;
      $scope.processedFilesPercentStr = ($scope.scannedFilesCounter.counter>0)?("("+(Math.floor(1000*$scope.processedFilesCount.counter/$scope.scannedFilesCounter.counter)/10)+"%)"):"";
      $scope.analyzedFilesPercentStr = (analysisPotential>0)?("("+(Math.floor(1000*$scope.analyzedFilesCounter.counter/analysisPotential)/10)+"%)"):"";

      var series=['Regular files','Sensitive files'];

      var sensitiveHistogram:Dashboards.DtoItemCounter<string>[] =[];
      sensitiveHistogram.push(new Dashboards.DtoItemCounter<string>(series[1],'', $scope.sensitiveFilesCounter.counter));
      sensitiveHistogram.push(new Dashboards.DtoItemCounter<string>(series[0],'', $scope.analyzedFilesCounter.counter  - $scope.sensitiveFilesCounter.counter));
      $scope.sensitiveHistogram=KendoChartDataBuilder.castToStackedData([' '],series,sensitiveHistogram,find,propertiesUtils);
      function find(sensitiveHistogram:Dashboards.DtoItemCounter<string>[],series:string,cat:string)
      {
        var fountItem = sensitiveHistogram.filter(s=>s.item == series )[0];
        if(fountItem) {
          return fountItem.count;
        }
        return 0;
      }

    };

    $scope.onGetScanCriteriaCompleted=function(scanCriterias:Dashboards.DtoMultiValueList)
    {
      $scope.scanCriteria = translateDtoMultiValueList(scanCriterias);
    };

    $scope.onGetScanConfigurationCompleted=function(stringListReportDtos:Dashboards.DtoStringListReport[],stringReportDtos:Dashboards.DtoStringReport[])
    {
      $scope.scanConfigurationList = stringListReportDtos;
      $scope.scanConfiguration = stringReportDtos;
    };
    $scope.onFileTypeHistogramCompleted=function(fileTypeHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileTypeHistogram = translateDtoMultiValueList(fileTypeHistogram);
    };
    $scope.onGroupSizeHistogramCompleted=function(groupSizeHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.groupSizeHistogram = translateDtoMultiValueList(groupSizeHistogram);
    };

    $scope.onGetFilesAccessHistogramCompleted=function(fileAccessHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileAccessHistogram = translateDtoMultiValueList(fileAccessHistogram);
    };

    $scope.onGetScanStatisticsError=function(e)
    {
      log.error("error get statistics " +e);
    };

    var translateDtoMultiValueList = function(val:Dashboards.DtoMultiValueList):Dashboards.DtoMultiValueList {
      var res:Dashboards.DtoMultiValueList = <Dashboards.DtoMultiValueList>{
        name: val.name,
        type: val.type,
        data: [],
      };

      for (var i:number=0;i<val.data.length;++i) {
        var counterItem:Dashboards.DtoCounterReport = val.data[i];
        res.data.push(new Dashboards.DtoCounterReport($filter('translate')(counterItem.name),counterItem.description, counterItem.counter));
      }
      return res;
    };

    $scope.$on('$destroy', function(){
    //  scanService.forceRunStatusRefresh(false);
    });

    var onGetError=function(e)
    {
      log.error("error get root folders" +e);

    };
  });

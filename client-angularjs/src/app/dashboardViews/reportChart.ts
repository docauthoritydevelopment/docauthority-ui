///<reference path='../../common/all.d.ts'/>

'use strict';
/* global kendo */

angular.module('reports.charts.directive',['doc2Services']).controller('ReportChartWidgetCtrl', function ($scope,$state,$window,$location,
  Logger,$timeout,$stateParams,$filter,filesResource:IFilesResource,dialogs,$element,splitViewFilterServiceFactory,propertiesUtils,configuration:IConfig,
  eventCommunicator:IEventCommunicator,chartResource:IChartsResource,filterFactory:IFilterFactory,currentUserDetailsProvider:ICurrentUserDetailsProvider,demoService:IDemoService) {
  let log:ILog = Logger.getInstance('ReportChartWidgetCtrl');
  let UNASSOCIATED_ITEM_ID = 'unassociated';
  let OTHERS_ITEM_ID = 'others';
  $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
  let splitViewFilterService:IFilterService = splitViewFilterServiceFactory.getInstance();
  $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
  $scope.showTotalCount = false;

  $scope.toggleShowTotalCount = function() {
    $scope.showTotalCount = !$scope.showTotalCount;
  };

  $scope.init = function() {
    setUserRoles();
    $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
      setUserRoles();
    });
  };

  $scope.getFilterService = function() {
    return splitViewFilterService;
  };

  var setUserRoles = function() {
    $scope.isMngReportsUser =                   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngReports]);
    $scope.isViewFuncRoleAssociationUser =      currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation]);
    $scope.isViewTagAssociationUser =           currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagAssociation],null,true);
    $scope.isViewBizListAssociationUser =       currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],null,true);
    $scope.isViewDocTypeAssociationUser =       currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeAssociation],null,true);
    $scope.isSupportAssignPublicUserViewData =  currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignPublicUserViewData]);
    $scope.isChartOwner = angular.isDefined($scope.chartInfo.owner) ? currentUserDetailsProvider.getUserData().name == $scope.chartInfo.owner.name : true;

  };

  $scope.viewPermitted = function(paneOption:EPaneOption) {
    var entityType: EntityDisplayTypes = paneOption.entityDisplayType;
    if(entityType ==EntityDisplayTypes.bizlistItem_clients_extractions||entityType ==EntityDisplayTypes.bizlistItem_consumers_extraction_rules || entityType ==EntityDisplayTypes.bizlistItems_clients) {
      return $scope.isViewBizListAssociationUser;
    }
    if(entityType ==EntityDisplayTypes.doc_types)
    {
      return $scope.isViewDocTypeAssociationUser;
    }
    if(entityType ==EntityDisplayTypes.tag_types||entityType ==EntityDisplayTypes.tags)
    {
      return $scope.isViewTagAssociationUser;
    }
    if(entityType ==EntityDisplayTypes.functional_roles)
    {
      return $scope.isViewFuncRoleAssociationUser;
    }
    return true;
  };

  $scope.$watch(function () { return $scope.chartInfo; }, function (value) {
    if($scope.chartInfo) {
      setViewConfiguration();
      setFilter();
      setShowUnassociated();
      setShowOthers();
      setRemovedValues();
      setChartType();

     if(!$scope.lazyLoad) {
        $scope.loadChartData();
      }
    }
  });

  $scope.$watch(function () { return $scope.lazyLoad; }, function (value,newValue) {

    if($scope.chartInfo&&!$scope.lazyLoad&&value!=newValue) {
        $scope.loadChartData();
    }
  });

  var setViewConfiguration = function() {
    $scope.viewConfiguration =(<ui.SavedChart>$scope.chartInfo).populationSubsetJsonFilter? $scope.chartInfo.populationSubsetJsonFilter:{};
  };

  var setFilter = function() {
    let chartInfo: ui.SavedChart = $scope.chartInfo;
    if(chartInfo.savedFilterDto) {
       splitViewFilterService.setFilterData(angular.fromJson(chartInfo.savedFilterDto.rawFilter));
      $scope.showFilterComponent = splitViewFilterService.getFilterData().hasCriteria();
    }
    else{
      $scope.showFilterComponent = false;
    }
  };

  var setChartType = function() {
    $scope.displayType = ($scope.chartInfo.chartDisplayType == ui.SavedChart.chartDisplayType_PIE ? 'pie':'bar' );

  };

  var setShowUnassociated = function() {
    $scope.showUnassociated =  (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showUnassociated;
    $scope.unassociatedText = filterFactory.getUnassociatedText($scope.chartInfo.mainEntityType);
    $scope.unassociateEnabled =  $scope.chartInfo&&!(<ui.SavedChartViewConfiguration>$scope.viewConfiguration).forceDisableUnassociated&&
        ($scope.chartInfo.mainEntityType == EntityDisplayTypes.doc_types ||
        $scope.chartInfo.mainEntityType == EntityDisplayTypes.tag_types ||
        $scope.chartInfo.mainEntityType == EntityDisplayTypes.tags ||
          //$scope.chartInfo.mainEntityType == EntityDisplayTypes.bizlistItem_clients_extractions ||
          //$scope.chartInfo.mainEntityType == EntityDisplayTypes.bizlistItem_consumers_extraction_rules ||
          //$scope.chartInfo.mainEntityType == EntityDisplayTypes.bizlistItems_clients ||
        $scope.chartInfo.mainEntityType == EntityDisplayTypes.groups  );
  };

  var setShowOthers = function() {
    $scope.showOthersEnabled=true;
    $scope.showOthersWarning =$scope.chartInfo.mainEntityType == EntityDisplayTypes.doc_types ||
        $scope.chartInfo.mainEntityType == EntityDisplayTypes.tag_types  ||
        $scope.chartInfo.mainEntityType == EntityDisplayTypes.tags;
  };

  var setRemovedValues = function() {
    (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds= (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds? (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds:[];
    $scope.removedValueIds =  (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds;

    $scope.showRemovedValues =  (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showRemovedValues;
  };

  $scope.cancelEditChartTitle = function() {
    $scope.editTitleMode=false;
    $scope.newChartTitle='';
  };

  $scope.setEditTitleMode = function(value) {
    $scope.editTitleMode=value;
    if(value) {
      $scope.newChartTitle = $scope.chartInfo.name;
    }
  };

  $scope.toggleShowUnAssociatedToChart = function() {
    (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showUnassociated = !(<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showUnassociated;
    $scope.showUnassociated =  (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showUnassociated;

    $scope.loadChartData();
    $scope.dirty = true;
  };

  $scope.toggleOthersToChart = function() {
    $scope.chartInfo.showOthers  =!  $scope.chartInfo.showOthers ;
    $scope.loadChartData();
    $scope.dirty = true;
  };

  $scope.toggleHiddenToChart = function() { //add remove from menu short cut
    $scope.chartInfo.hidden  =!  $scope.chartInfo.hidden ;
    if($scope.isSavedChart()) {
      $scope.saveChart();
    }
  };

  $scope.setPublic = function() {
    $scope.chartInfo.globalFilter = !$scope.chartInfo.globalFilter;
    if($scope.isSavedChart()) {
      $scope.saveChart();
    }
  };

  var notifyUpdateState = function() {
    eventCommunicator.fireEvent(EEventServiceEventTypes.ReportChartUserAction, {
      action: 'refreshState',
    });

  };

  $scope.editChartTitle = function() {
    var newChartTitle = $scope.newChartTitle;
    if(newChartTitle && newChartTitle.trim()!='') {
      $scope.chartInfo.name = newChartTitle.trim();
      $scope.editTitleMode=false;
      if($scope.isSavedChart()) {
        $scope.saveChart();
      }
    }
  };

  $scope.newChartTitleChange=function(newChartTitle) {
    if($scope.editChartTitleOnChangeEnabled) {
      $scope.chartInfo.name=newChartTitle;
    }
  };

  $scope.deleteSavedChart = function() {
    log.debug('delete saved chart: ' + $scope.chartInfo.id);
    var dlg:any =  dialogs.confirm('Confirmation', 'You are about to delete chart named: \''+$scope.chartInfo.name+'\'. Continue?', null);
      dlg.result.then(function(){
      chartResource.deleteChart($scope.chartInfo.id, function () {
            notifyUpdateState();
            $scope.onChartDeleted();
          },
          function () {

          });
     },function(){

    });
  };

  $scope.isChartGlobal = function() {
    return $scope.chartInfo.globalFilter;
  };

  $scope.isSavedChart = function() {
    return $scope.chartInfo&&$scope.chartInfo.id!=null;
  };

  $scope.changeChartMaxData = function(val) {
    $scope.chartInfo.maximumNumberOfEntries = val;

    $scope.loadChartData();
    $scope.dirty = true;
  };

  $scope.changeChartType = function(chartType:string) {
    $scope.chartInfo.chartDisplayType = chartType=='pie'?ui.SavedChart.chartDisplayType_PIE :ui.SavedChart.chartDisplayType_BAR_HORIZONTAL ;
    setChartType();
    $scope.selectItemRight=null;
    $scope.dirty = true;
  };

  $scope.setLazyLoad = function(val:boolean) {
    $scope.lazyLoad=val;
  };

  $scope.loadChartData = function() {
    if(currentUserDetailsProvider.isDemoInstallation()) {
      demoService.getDemoInfo(
        (demoInfo) => {
         loadChartData(demoInfo);
       }, (err) => {
       });
    }else{
      loadChartData({});
    }
  };

  $scope.enableShowTotal = false;

  var loadChartData = function(demoInfo) {
    let chartInfo:ui.SavedChart = $scope.chartInfo;
    let filter = splitViewFilterService.getFilterData().toKendoFilter();
    $scope.enableShowTotal = filter != null;
    chartResource.getChartData(chartInfo.populationJsonUrl, filter, chartInfo.maximumNumberOfEntries, chartInfo.showOthers && $scope.showOthersEnabled,
      EntityDisplayTypes.doc_types.equals(chartInfo.mainEntityType), demoInfo,function (chartData:Dashboards.DtoCounterReport[], data) {
        $scope.selectItemRight = null;

        if( $scope.showUnassociated && $scope.unassociateEnabled)
        {
            var urlParams = chartInfo.populationJsonUrl.substring(chartInfo.populationJsonUrl.indexOf('?') + 1);
            filesResource.getFilesCount(filter, urlParams, function (count:number) {
              $scope.filesCount = count;
              var unassociatedFilesCount = $scope.filesCount - data.totalAggregatedCount;
              chartData.push(new Dashboards.DtoCounterReport($scope.unassociatedText,'',unassociatedFilesCount<0?0:unassociatedFilesCount,UNASSOCIATED_ITEM_ID));
              removeSelectedValuesFromChartData(chartData);
              $scope.chartData = chartData;
              $scope.initiated = true;
              }, function () {
            });
        }
        else {
         if ($scope.onDataBind) { //for groups adding ungrouped category
            chartData = $scope.onDataBind(chartData, data);
          }
          removeSelectedValuesFromChartData(chartData);
          $scope.chartData = chartData;
          $scope.initiated = true;
        }
      }, function () {});
  };

  var removeSelectedValuesFromChartData=function(chartData) {
    if($scope.removedValueIds.length>0 &&!$scope.showRemovedValues) {
      for( var i=0 ; i<$scope.removedValueIds.length; i++) {
        var selectedItemToRemove = chartData.filter(d=> d.id == $scope.removedValueIds[i])[0];
        if(selectedItemToRemove) {
          var selectedItemToRemoveIndex = chartData.indexOf(selectedItemToRemove);
          chartData.splice(selectedItemToRemoveIndex, 1);
        }
      }
    }
  };

  $scope.copySavedChart = function() {
    log.debug('copy chart');
    var newChart =angular.copy($scope.chartInfo);
    (<ui.SavedChart>newChart).name+=' - Copy';
    (<ui.SavedChart>newChart).populationSubsetJsonFilter = $scope.viewConfiguration;
    delete newChart.id;
    $scope.saveChart(newChart);
  };

  var allTypes = currentUserDetailsProvider.isInstallationModeLegal() ? EntityDisplayTypes.allLegalInstallationMode() : EntityDisplayTypes.all();

  allTypes.splice( allTypes.indexOf(EntityDisplayTypes.files), 1 );
  allTypes.splice( allTypes.indexOf(EntityDisplayTypes.departments), 1 );
  allTypes.splice( allTypes.indexOf(EntityDisplayTypes.all_files), 1 );
  //allTypes.sort( propertiesUtils.sortAlphbeticFun('value'));

  var createFollowUpOptions = function() {
    var leftFollowUp = function(isUnassociated:boolean) {
      var filterById;
      var leftPaneType = EntityDisplayTypes.parse((<ui.SavedChart>$scope.chartInfo).mainEntityType);
      if(!isUnassociated) {
        filterById= filterFactory.createFilterByID( leftPaneType);
      }
      else {
        filterById= filterFactory.createUnassociatedFilter( leftPaneType);
      }
      return filterById;
    };

    var rightFollowUpOptions = [];
    allTypes.forEach(t=>rightFollowUpOptions.push(createPaneOption(t, followUpHandler, leftFollowUp, null)) );
    $scope.followUpOptions = rightFollowUpOptions;
  };

  $scope.removeSelectedValue = function () {
    if (!$scope.selectItemRight) {
      console.error('A ' + $scope.right + ' must be selected. Clicking on row to select one.');
      return;
    }
    let selectedItemToRemove = $scope.chartData.filter(d=> d.id==$scope.selectItemRight.id)[0];
    if(selectedItemToRemove) {
      var selectedItemToRemoveIndex = $scope.chartData.indexOf(selectedItemToRemove);
      (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds.push($scope.selectItemRight.id);
      setRemovedValues();
      $scope.dirty = true;
      $scope.loadChartData();
    }
  };

  $scope.toggleRemovedValues = function() {
    (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showRemovedValues = ! (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showRemovedValues;
    setRemovedValues();
    $scope.loadChartData();
    $scope.dirty = true;
  };

  $scope.isSetScopeDisabled = function() {
    return !$scope.chartInfo.owner || !$scope.isSupportAssignPublicUserViewData;
  };

  $scope.isRemoveSelectedValueDisabled = function() {
      return  $scope.selectItemRight==null||$scope.selectItemRight.id == OTHERS_ITEM_ID||$scope.selectItemRight.id == UNASSOCIATED_ITEM_ID;
  };

  $scope.clearRemoveSelectedValues = function() {
     (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).removedValueIds=[];
     (<ui.SavedChartViewConfiguration>$scope.viewConfiguration).showRemovedValues=false;
     setRemovedValues();
    $scope.loadChartData();
     $scope.dirty = true;
  };

  $scope.doFollowUp = function (followUp:EPaneOption) {
    if (!$scope.selectItemRight) {
      console.error('A ' + $scope.right + ' must be selected. Clicking on row to select one.');
      return;
    }
    var isUnassociated = $scope.selectItemRight.name == $scope.unassociatedText;
    var filterDataToRestore = $scope.onBeforeFollowUp(followUp,isUnassociated);
    //setIsSwitchViewsDisabled(followUp.view);
    $scope.viewOptionHandler(followUp);
    $scope.onAfterFollowUp(filterDataToRestore);
  };

  $scope.onBeforeFollowUp = function (followUp:EPaneOption,isUnassociated) {
    var filterDataToRestore =JSON.parse(angular.toJson(splitViewFilterService.getFilterData()));
    addSelectedLeftIdToFilterServiceOnFollowUp(followUp,isUnassociated);
    //   addSelectedRightIdToFilterService(followUp);
    //   filterChanged();
    return filterDataToRestore;
  };

  $scope.onAfterFollowUp = function (filterDataToRestore) {
    splitViewFilterService.setFilterData(filterDataToRestore);
  };

  var addSelectedLeftIdToFilterServiceOnFollowUp = function (followUp:EPaneOption,isUnassociated:boolean) {
    //if (splitViewFilterService.getFilterData().getPageRestrictionFilter() && splitViewFilterService.getFilterData().getPageRestrictionFilter().hasCriteria()) {
    //  splitViewFilterService.addPageFilter(splitViewFilterService.getFilterData().getPageRestrictionFilter());
    //  splitViewFilterService.clearPageRestrictionFilter();
    //}
    //else {
      var filter = followUp.leftFilterFunction(
       isUnassociated
      )($scope.selectItemRight.id, $scope.selectItemRight.name);
      splitViewFilterService.addPageFilter(filter);
    //}
  };

  $scope.viewOptionHandler = function (paneOption:EPaneOption) {
    if (paneOption.handler) {
      paneOption.handler(paneOption);
    }
  };

  $scope.selectItemRight=null;

  var setChartSeriesDisplay = function(seriesData) {
    if(!seriesData.selected) {
      seriesData.userColor = null;
      seriesData.labelUserColor = null;
      seriesData.explode = false;
    }
    else {
      seriesData.userColor = "#1ab394";
      seriesData.labelUserColor = "#1ab394";
      seriesData.explode = true;
    }
  };

  $scope.onHighchartSeriesClick = function(selectedItem) {
    if(selectedItem.selected) {//toggle selected
      $scope.selectItemRight=null;
      for (var i = 0; i < $scope.chartData.length; i++) {
        $scope.chartData[i].selected = false;
      }
    }
    else {
      $scope.selectItemRight = selectedItem ;
      for (var i = 0; i < $scope.chartData.length; i++) {
        $scope.chartData[i].selected = false;
      }
      selectedItem.selected = true;
    }
    $scope.chartData = JSON.parse(JSON.stringify($scope.chartData));
  };

  $scope.onSeriesClick = function(e,chartData) {
    if(e.dataItem.selected) {//toggle selected
      $scope.selectItemRight=null;
      for (var i = 0; i < chartData.options.series[0].data.length; i++) {
        chartData.options.series[0].data[i].selected = false;
        setChartSeriesDisplay(chartData.options.series[0].data[i]);
      }
    }
    else {
      $scope.selectItemRight = e.dataItem ;
      for (var i = 0; i < chartData.options.series[0].data.length; i++) {
        chartData.options.series[0].data[i].selected = false;
        setChartSeriesDisplay(chartData.options.series[0].data[i]);
      }
      e.dataItem.selected = true;
      setChartSeriesDisplay(e.dataItem);
    }
    $timeout(function() {
      chartData.refresh();
    });
  };

  $scope.isFollowUpOptionDisabled = function (paneOption:EPaneOption):boolean {
    return !$scope.chartInfo || (paneOption.view == (<ui.SavedChart>$scope.chartInfo).mainEntityType);
  };

  $scope.isFollowUpDisabled = function ():boolean {
       return  $scope.selectItemRight==null||$scope.selectItemRight.id == OTHERS_ITEM_ID;
  };

  $scope.getFollowUpTitle = function(selectItem) {
    if (selectItem==null) {
      return 'This feature is supported only after choosing chart slice';
    }
    else if (selectItem.id == OTHERS_ITEM_ID) {
      return "This feature is not supported for the 'Others' chart slice";
    }
    if ($scope.msie11) {
      return 'This feature is not supported in Internet Explorer 11';
    }
    else {
      return 'Open chart in new tab, drilled down by the selected chart slice';
    }
  };

  var setSavedChartFilter = function(newChart:ui.SavedChart) {
    if( splitViewFilterService.getFilterData().hasCriteria()) {
      var titleFilterPart = ' (' + ($filter('translate')('filtered')).toLowerCase() + ')';
      newChart.name += titleFilterPart;
      let savedFilter = new Entities.SavedFilterDto();
      savedFilter.name = newChart.name +(new Date()).getTime(); //filter name is unique
      savedFilter.globalFilter = false;
      savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
      savedFilter.filterDescriptor = splitViewFilterService.toKendoFilter();
      newChart.savedFilterDto = savedFilter;
    }
  };

  createFollowUpOptions();

  function followUpHandler(paneOption:EPaneOption) {
    var newChartInfo:ui.SavedChart = JSON.parse(angular.toJson($scope.chartInfo)); //deep copy

    newChartInfo.populationSubsetJsonFilter = null;
    newChartInfo.mainEntityType = paneOption.view;
    newChartInfo.name =$filter('translate')(EntityDisplayTypes.files.toString())+'s by '+$filter('translate')(newChartInfo.mainEntityType);
    setSavedChartFilter(newChartInfo);
    newChartInfo.id=null;

    var newUrl = GridBehaviorHelper.getDataUrlBuilder2(EntityDisplayTypes.parse( newChartInfo.mainEntityType)
        ,null,null,configuration,null,null,null);
    newUrl = newUrl.replace(new RegExp('{partitionId}', 'g'),'1');
    var urlParams =$scope.chartInfo.populationJsonUrl.indexOf('?')>-1? $scope.chartInfo.populationJsonUrl.substr($scope.chartInfo.populationJsonUrl.indexOf('?'),$scope.chartInfo.populationJsonUrl.length):'';
    newChartInfo.populationJsonUrl =newUrl+urlParams;

    var url = $state.href(ERouteStateName.chartView, {'chartInfo': encodeURIComponent(JSON.stringify(newChartInfo))},{ inherit: false});
    url = url.replace('/v1','');
      $window.open(url, '_blank');

  }

  function createPaneOption(opt:EntityDisplayTypes, handler:(string)=>void,
                            leftFilterFunction?:(value: boolean,fullyContainedState?:EFullyContainedFilterState) =>(value,name) => any,rightFilterFunction?:(value,name) => any) : EPaneOption {
    return new EPaneOption($filter('translate')(opt.toString()), handler, opt,leftFilterFunction,rightFilterFunction);
  }

  $scope.openInReportPage = function() {
    const urlParams = {};
    urlParams['left'] =  (<ui.SavedChart>$scope.chartInfo).mainEntityType;
    urlParams['right'] =  EntityDisplayTypes.files.toString();
    urlParams['findIdLeft']=    $scope.selectItemRight? $scope.selectItemRight.id:'';
    urlParams['activePan'] = 'left';
    urlParams['filter']=splitViewFilterService.getThinFilterCopy();

    var url = $state.href(ERouteStateName.discoverSplitView, urlParams);
    url = url.replace('/v1','');
    $window.open(url,'_blank');
  };

  $scope.saveChart = function(newChartToSave) {
    if($scope.isSavedChart() && !$scope.readOnly && !newChartToSave) {
      (<ui.SavedChart>$scope.chartInfo).populationSubsetJsonFilter = $scope.viewConfiguration;

      chartResource.updateChart(<ui.SavedChart>$scope.chartInfo, function () {
        $scope.dirty = false;
        $scope.newChartTitle='';
        notifyUpdateState();

      }, function () {
        $scope.editTitleMode=true;
        onError();
      });
    }
    else {
      var chart = newChartToSave ? newChartToSave : $scope.chartInfo;
      (<ui.SavedChart>$scope.chartInfo).populationSubsetJsonFilter = $scope.viewConfiguration;
      chartResource.addNewChart(chart, function (chart: ui.SavedChart) {
        $scope.allowSaveChart = false;
        $scope.dirty = false;
        $scope.chartInfo = chart;
        $scope.onNewChartSaved? $scope.onNewChartSaved(chart):null;
        notifyUpdateState();
      }, function () {

        onError();
      });
    }
  };

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
})
.directive('reportChartWidget', function(){
  return {
    restrict: 'EA',
    templateUrl: '/app/dashboardViews/reportChart.tpl.html',
    replace: true,
    transclude:true,
    scope:
    {
      chartInfo:'=',
      chartData:'=',
      onDataBind:'=',
      lazyLoad:'=',
      readOnly:'=',
      allowSaveChart:'=',
      editChartTitleOnChangeEnabled:'=',
      onChartDeleted:'=',
      onNewChartSaved:'=',
      disableAnimation:'=',
      onChartCreated:'=',
      initiated:'=',
      saveChart:'=',
      disableSaveButtonOnChange:'='
    },
    controller: 'ReportChartWidgetCtrl',
    link: function (scope:any, element, attrs) {
      scope.init();
    }
  }
});

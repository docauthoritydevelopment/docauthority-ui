///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports.chartPage', [
  'directives',
  'directives.dashboardWidgets',
  'resources.charts',
  'reports.charts.directive',
  'doc2Services'

])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.chartView, {
        //  abstract: true,
        url: '/charti?:chartInfo?&id?',
        templateUrl: '/app/dashboardViews/chartPage.tpl.html',
        controller: 'ChartPageCtrl'
      })

  })
  .controller('ChartPageCtrl', function ($scope, $element, $state, $stateParams, $compile, chartResource: IChartsResource, dialogs, $timeout,
                                         Logger: ILogger, propertiesUtils, configuration: IConfig, pageTitle, filterResource: IFilterResource,
                                         routerChangeService) {
    var log: ILog = Logger.getInstance('ChartPageCtrl');
    $scope.chartId = $stateParams.id && propertiesUtils.isInteger($stateParams.id) ? parseInt($stateParams.id) :null;


    $scope.init = function () {
      if($scope.chartId){
        loadSavedChart($scope.chartId);
      }
      else if ($stateParams.chartInfo) {
        $scope.chartInfo = JSON.parse(decodeURIComponent($stateParams.chartInfo));
        $scope.readOnly = $scope.chartInfo.id? true:false; //chart after drill down needs to be edited and saved
        if ($scope.chartInfo && $scope.chartInfo.name && $scope.chartInfo.populationJsonUrl) {
          pageTitle.set('Chart > ' + $scope.chartInfo.name);
        }
        else {
          pageTitle.set('Chart > info corrupted');
        }
      }

    };

    var loadSavedChart = function (chartId) {
      chartResource.getChart(chartId, function (chart: ui.SavedChart) {
        $scope.chartInfo =  chart;
          $scope.readOnly =true; //chart after drill down needs to be edited and saved
          pageTitle.set('Chart > ' + $scope.chartInfo.name);
      }, onError);

    };

    $scope.onNewChartSaved = function (chart:ui.SavedChart) {
      $scope.chartInfo = chart;
      $stateParams['id'] = $scope.chartInfo.id;
      $stateParams['chartInfo'] ='';

      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: true
        });

    }
    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }

    $scope.onChartDeleted = function () {
      $scope.chartDeleted = true;
    }

  });

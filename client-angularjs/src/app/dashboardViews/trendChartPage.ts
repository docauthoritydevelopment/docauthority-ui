///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('reports.trendChartPage',[
      'directives',
      'directives.dashboardWidgets',
      'resources.charts',
      'reports.charts.trends.directive'

    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.trendChartView, {
            //  abstract: true,
            url: '/trendcharti?:chartInfo&id?',
            templateUrl: '/app/dashboardViews/trendChartPage.tpl.html',
            controller: 'TrendChartPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngReports,Users.ESystemRoleName.ViewReports]
            }
          })

    })
    .controller('TrendChartPageCtrl', function ($scope,$element,$state,$stateParams,$compile, trendChartResource: ITrendChartsResource,dialogs,$timeout,
                                           Logger:ILogger,propertiesUtils, configuration:IConfig,pageTitle,filterResource:IFilterResource) {
      var log:ILog = Logger.getInstance('TrendChartPageCtrl');
      $scope.chartId = $stateParams.id && propertiesUtils.isInteger($stateParams.id) ? parseInt($stateParams.id) :null;

      $scope.init=function()
      {
        if($scope.chartId){
          loadSavedChartsList($scope.chartId);
        }
        else if ($stateParams.chartInfo) {
          $scope.chartInfo = JSON.parse(decodeURIComponent(atob($stateParams.chartInfo)));

          if ($scope.chartInfo && (<Dashboards.DtoTrendChart>$scope.chartInfo).name ) {
            pageTitle.set('Trend Chart > ' + $scope.chartInfo.name);
          }
          else {
            pageTitle.set('Trend Chart > info corrupted');
          }
        }

      };

      var loadSavedChartsList = function (chartId) {
        trendChartResource.getCharts(function (charts: Dashboards.DtoTrendChart[]) {
          $scope.chartInfo = charts.filter(f => f.id == chartId)[0];
          pageTitle.set('Chart > ' + $scope.chartInfo.name);
        }, function () {

        });

      };

      $scope.onChartSaved = function()
      {
        $stateParams['id'] = $scope.chartInfo.id;
        $stateParams['chartInfo'] ='';

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.onChartDeleted = function()
      {
        $scope.chartDeleted = true;
      }

    });

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports.charts',[
      'directives',
      'directives.dashboardWidgets',
      'resources.charts',
      'reports.charts.directive',
      'reports.chartPage',
      'reports.charts.tree',
      'reports.charts.trends',
      'resources.IPdfOperations'
    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.chartsManagement, {
            //  abstract: true,
            url: '/charts',
            templateUrl: '/app/dashboardViews/reportChartsPage.tpl.html',
            controller: 'ReportChartsPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngReports,Users.ESystemRoleName.ViewReports]
            }
          })


    })
    .controller('ReportChartsPageCtrl', function ($scope,$element,$state,$stateParams,$compile,chartResource:IChartsResource,dialogs,$timeout,$q,routerChangeService,$filter,
                                               Logger:ILogger,propertiesUtils, configuration:IConfig,pageTitle,filterResource:IFilterResource,pdfOperationsResource: IPdfOperations) {
      var log:ILog = Logger.getInstance('ReportChartsPageCtrl');

      $scope.init=function()
      {
        pageTitle.set("Reports: Saved charts");
        loadChartList();
        loadSavedFiltersList();

      };

      var loadSavedFiltersList = function() {
        filterResource.getFilters(function (filters:ui.SavedDiscoveredFilter[]) {
          $scope.savedFilters = filters;
        }, function () {
          onError();
        });

      };
      $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
      $scope.onNewChartSaved = function () {
            loadChartList();
      }

      $scope.onChartDeleted = function () {
        loadChartList();
      }

      var loadChartList = function()
      {
        chartResource.getCharts(function(charts:ui.SavedChart[]) {
          var lazyLoad=[];
          $scope.onChartCreated=[];  //for saving pdf after all charts have been displayed
          $scope.initiated = [];
          $scope.disableAnimation=[];
          for(var i=0; i<charts.length;i++)
          {
            lazyLoad[i]=i>=2;
          }

          $scope.lazyLoad = lazyLoad;
          $scope.savedCharts = charts;
        },function()
        {
          onError();
        });

      }

      $scope.exportToExcel = function() {
        routerChangeService.addFileToProcess('Saved_report_charts_' + $filter('date')(Date.now(), configuration.exportNameDateTimeFormat), 'user_view_data', {displayType: "CHART", byUser: true}, {}, false);
      }

      $scope.openUploadItemsDialog = function()
      {
        var dlg = dialogs.create(
          'common/directives/dialogs/uploadFileDialogByServer.tpl.html',
          'uploadFileDialogByServerCtrl',
          {uploadResourceFn: chartResource.uploadUserViewDataFromCsv,
            validateUploadResourceFn: chartResource.validateUploadUserViewDataFromCsv,
            itemsName:'user view data',createStubsOptionEnabled:true},
          {size:'md'});
        dlg.result.then(function(fileUploaded:boolean){
          if(fileUploaded)
          {
            loadChartList();
          }
        },function(){
        });
      };


      $scope.exportToPdf = function()
      {
        var loopPromises = [];
        for(var i=0; i<$scope.lazyLoad.length;i++)
        {
          if( !$scope.initiated[i]) {
            var deferred = $q.defer();
            loopPromises.push(deferred.promise);
            $scope.onChartCreated[i]=(function(index,deferred1)
            {
              return function() {
                deferred1.resolve(index);
                $scope.disableAnimation[index] = false;
              }
            })(i,deferred);
            $scope.disableAnimation[i] = true;
            $scope.lazyLoad[i] = false;
          }
        }
        $q.all(loopPromises).then(function (selectedParentIdsToUpdate) {
          pdfOperationsResource.exportToPdf('.pdf-page', pageTitle.get() + ".pdf", null);
        });

      }



      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    });

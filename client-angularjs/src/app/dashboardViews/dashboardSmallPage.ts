///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('dashboards.user',[
  'directives',
  'directives.dashboardWidgets',
  'resources.dashboards'
])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.dashboardUser, {
        url: '/dashboardUser',
        templateUrl: '/app/dashboardViews/dashboardSmallPage.tpl.html',
        controller: 'DashboardSmallPageCtrl'
      })

  })
  .controller('DashboardSmallPageCtrl', function ($scope,$element,$state,$stateParams,$compile,$filter,$timeout,scanService:IScanService,dialogs,
                                             Logger:ILogger,propertiesUtils, configuration:IConfig,activeScansResource:IActiveScansResource,
                                             dashboardChartResource:IDashboardChartsResource,dashboardScanResource:IDashboardScanResource, dashboardUserResource:IDashboardUserResource ,pageTitle) {
    var log:ILog = Logger.getInstance('DashboardSmallPageCtrl');

    var setNewNames = function(countList:any[] , fieldsList:any[]) :any[] {
      let newTextMap={};
      fieldsList.forEach((fieldItem)=> {
        newTextMap[fieldItem.field] = fieldItem.text;
      })

      countList.forEach((countItem)=> {
        if (newTextMap[countItem.name]) {
          countItem.name = newTextMap[countItem.name];
        }
      })
      return countList;
    }

    $scope.userDocumentStatistics = [];
    $scope.init=function()
    {
      pageTitle.set("Summary report");
      dashboardUserResource.getUserDocumentStatistics($scope.onGetUserDocumentStatisticsCompleted,$scope.onGetUserStatisticsError);
      dashboardChartResource.getFileTypeHistogram(null, true, $scope.onFileTypeHistogramCompleted,$scope.onGetScanStatisticsError);
      dashboardChartResource.getFilesAccessHistogram(null, true, $scope.onGetFilesAccessHistogramCompleted,$scope.onGetScanStatisticsError);
    };

    var userStatisticsFields:any[]  = [
      {
        field : 'Documents with no Doc Types' ,
        text : 'Documents without DocTypes'
      },
      {
        field : 'Documents with Doc Types' ,
        text : 'Documents with DocTypes'
      }
    ];

    $scope.onGetUserDocumentStatisticsCompleted=function(counterReportDtos:Dashboards.DtoCounterReport[])
    {
      $scope.userDocumentStatistics = setNewNames(counterReportDtos , userStatisticsFields);
    };

    $scope.onGetUserStatisticsError=function(e)
    {
      log.error("error get statistics " +e);
    };





    var translateDtoMultiValueList = function(val:Dashboards.DtoMultiValueList):Dashboards.DtoMultiValueList {
      var res:Dashboards.DtoMultiValueList = <Dashboards.DtoMultiValueList>{
        name: val.name,
        type: val.type,
        data: [],
      };

      for (var i:number=0;i<val.data.length;++i) {
        var counterItem:Dashboards.DtoCounterReport = val.data[i];
        res.data.push(new Dashboards.DtoCounterReport($filter('translate')(counterItem.name),counterItem.description, counterItem.counter));
      }
      return res;
    };




    $scope.onFileTypeHistogramCompleted=function(fileTypeHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileTypeHistogram = translateDtoMultiValueList(fileTypeHistogram);
    };

    $scope.onGetFilesAccessHistogramCompleted=function(fileAccessHistogram:Dashboards.DtoMultiValueList)
    {
      $scope.fileAccessHistogram = translateDtoMultiValueList(fileAccessHistogram);
    };


    $scope.$on('$destroy', function(){
    //  scanService.forceRunStatusRefresh(false);
    });

    var onGetError=function(e)
    {
      log.error("error get root folders" +e);

    };
  });

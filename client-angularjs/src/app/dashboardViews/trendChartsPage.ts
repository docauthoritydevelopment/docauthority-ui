///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports.charts.trends',[
      'directives',
      'directives.dashboardWidgets',
      'resources.charts',
      'reports.charts.directive',
      'reports.trendChartPage',
      'resources.IPdfOperations'
    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.trendChartsManagement, {
            //  abstract: true,
            url: '/trendCharts',
            templateUrl: '/app/dashboardViews/trendChartsPage.tpl.html',
            controller: 'ReportTrendChartsPageCtrl'
          })


    })
    .controller('ReportTrendChartsPageCtrl', function ($scope,$element,$state,$stateParams,$compile,trendChartResource:ITrendChartsResource,dialogs,$timeout,
                                                  Logger:ILogger,propertiesUtils, configuration:IConfig,pageTitle,filterResource:IFilterResource,pdfOperationsResource: IPdfOperations) {
      var log:ILog = Logger.getInstance('ReportTrendChartsPageCtrl');

      $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
      $scope.init=function()
      {
        pageTitle.set("Reports: Trend charts");
        loadChartList();
        loadSavedFiltersList();

      };

      var loadSavedFiltersList = function() {
        filterResource.getFilters(function (filters:ui.SavedDiscoveredFilter[]) {
          $scope.savedFilters = filters;
        }, function () {
          onError();
        });


      }


      $scope.onChartSaved = function () {
        loadChartList();
      }

      $scope.onChartDeleted = function () {
        loadChartList();
      }

      var loadChartList = function()
      {
        trendChartResource.getCharts(function(charts:Dashboards.DtoTrendChart[]) {
         var visibleCharts = charts.filter(c=>!(<Dashboards.DtoTrendChart>c).hidden);
          var lazyLoad=[];
          for(var i=0; i<visibleCharts.length;i++)
          {
            lazyLoad[i]=i>=2;
          }

          $scope.lazyLoad = lazyLoad;
          $scope.savedCharts = visibleCharts;

        },function()
        {
          onError();
        });

      }



      $scope.exportToPdf = function()
      {
        for(var i=0; i<$scope.lazyLoad.length;i++)
        {
          $scope.lazyLoad[i]=false;
        }
        setTimeout(function() {
          pdfOperationsResource.exportToPdf('.pdf-page', pageTitle.get() + ".pdf", null);
        },1000);
      }



      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    });

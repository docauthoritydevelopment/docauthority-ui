///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */



angular.module('dashboards.forward',[
  'directives',
])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.dashboardForwarder, {
        url: '/dashboardForwarder',
        templateUrl: '/app/dashboardViews/dashboardForwarder.tpl.html',
        controller: 'DashboardForwarderCtrl'
      })
  })
  .controller('DashboardForwarderCtrl', function ($scope,$state,currentUserDetailsProvider:ICurrentUserDetailsProvider,$location,Logger:ILogger, licenseResource: ILicenseResource) {
    var log:ILog = Logger.getInstance('DashboardForwarderCtrl');


    $scope.setDashboard = function() {
      // licenseResource.getLicenseStatus((lic: License.DtoDocAuthorityLicense) => {
      //     if (!lic.loginExpired) {
             let userData: Users.UserAuthenticationData = currentUserDetailsProvider.getUserData();
             if (currentUserDetailsProvider.isAuthorized([userData.dashboards.AdminSysRole])) {
               $state.transitionTo(ERouteStateName.dashboardSystem);
              // $location.path(ERouteStateName.dashboardSystem).replace();
             }
             else if (currentUserDetailsProvider.isAuthorized([userData.dashboards.ITSysRole])) {
               $state.transitionTo(ERouteStateName.dashboardIT);
            //   $location.path(ERouteStateName.dashboardIT).replace();
            }
             else {
               $state.transitionTo(ERouteStateName.dashboardUser);
             //  $location.path(ERouteStateName.dashboardUser).replace();
             }
      //     }
      //   }, function(){});
    }


    $scope.init=function()
    {
      if (currentUserDetailsProvider.isAuthenticated()) {
        $scope.setDashboard();
      }
      else {
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, (sender, value: Users.UserAuthenticationData) => {
          $scope.setDashboard();
        });
      }
    };



    $scope.$on('$destroy', function(){
    //  scanService.forceRunStatusRefresh(false);
    });

  });

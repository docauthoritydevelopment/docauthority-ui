///<reference path='../../common/all.d.ts'/>

'use strict';
/* global kendo */


angular.module('reports.charts.trends.directive', [
  'directives.dialogs.datesPicker', 'doc2Services'
])
  .controller('TrendChartWidgetCtrl', function ($scope, $state, $window, $location, Logger, $timeout, $stateParams, $filter, filesResource: IFilesResource, dialogs, $element, splitViewFilterServiceFactory,
                                                propertiesUtils, configuration: IConfig, eventCommunicator: IEventCommunicator, trendChartResource: ITrendChartsResource, currentUserDetailsProvider: ICurrentUserDetailsProvider, routerChangeService) {
    var log: ILog = Logger.getInstance('TrendChartWidgetCtrl');
    var resolutionsToText = {
      hours: 60 * 60 * 1000,
      days: 60 * 60 * 1000 * 24,
      weeks: 60 * 60 * 1000 * 24 * 7,
      months: 60 * 60 * 1000 * 24 * 30,
      quarters: 60 * 60 * 1000 * 24 * 364 / 4,
      years: 60 * 60 * 1000 * 24 * 365
    };
    var MAX_SAMPLE_COUNT_RETURNED = 60;
    var TOTAL_SERIES_ID = 'total';
    var OTHERS_SERIES_ID = 'others';
    var splitViewFilterService: IFilterService = splitViewFilterServiceFactory.getInstance();
    $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
    $scope.init = function () {

      splitViewFilterService.setFilterData(new FilterData());//in case there is filter from before
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData, currentUserDetailsProvider: ICurrentUserDetailsProvider) {
        setUserRoles();
      });
    };
    var setUserRoles = function () {
      $scope.isMngReportsUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngReports]);
      $scope.isChartOwner = currentUserDetailsProvider.getUserData().name == $scope.chartInfo.owner.name;
    };
    $scope.getOthersSeriesID = function () {
      return OTHERS_SERIES_ID;
    };

    $scope.getTotalSeriesID = function () {
      return TOTAL_SERIES_ID;
    };

    $scope.$watch(function () {
      return $scope.chartInfo;
    }, function (value) {
      var trendChart = <Dashboards.DtoTrendChart>$scope.chartInfo;
      if (trendChart) {
        setChartTitleForDownload();
        setViewConfiguration();
        setResolutionText();
        setShowInMenuState();
        setChartResolutionStepState();
        if (!$scope.lazyLoad) {
          $scope.loadChartData();
        }
      }
    });
    var setViewConfiguration = function () {
      const viewConfiguration = (<Dashboards.DtoTrendChart>$scope.chartInfo).viewConfiguration;
      if (viewConfiguration && typeof viewConfiguration === 'object') {
        $scope.viewConfiguration = viewConfiguration;
      } else {
        $scope.viewConfiguration = viewConfiguration ? JSON.parse(viewConfiguration) : {};

      }
    }

    var setResolutionText = function () {
      var viewResolutionMsText = null;
      if ((<Dashboards.DtoTrendChart>$scope.chartInfo).viewResolutionMs != null) {
        for (var key in resolutionsToText) {
          if ((<Dashboards.DtoTrendChart>$scope.chartInfo).viewResolutionMs == resolutionsToText[key]) {
            viewResolutionMsText = key;
          }
        }
      }
      $scope.viewResolutionMsText = viewResolutionMsText;
    }
    $scope.$watch(function () {
      return $scope.lazyLoad;
    }, function (value, newValue) {

      if ($scope.chartInfo && !$scope.lazyLoad && value != newValue) {
        $scope.loadChartData();
      }
    });

    var setShowInMenuState = function () {
      $scope.showInMenuState = (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).showInMenu;
    }

    $scope.toggleShowInMenuAsShortcut = function () //add remove from menu short cut
    {
      (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).showInMenu = !(<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).showInMenu;
      setShowInMenuState();
      $scope.saveChart(false);
    }

    $scope.toggleSeriesVisibility = function (seriesName) //add remove from menu short cut
    {
      if (seriesName == TOTAL_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesHidden = !(<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesHidden;
      }
      else if (seriesName == OTHERS_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesHidden = !(<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesHidden;
      }
      refreshChartDisplay(true);
      $scope.dirty = true;
    }
    $scope.changeSeriesType = function (seriesName, seriesType) //add remove from menu short cut
    {
      if (seriesName == TOTAL_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesType = seriesType;
      }
      if (seriesName == OTHERS_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesType = seriesType;
      }
      refreshChartDisplay(true);
      $scope.dirty = true;
    }

    $scope.hasFilter = function () {
      return $scope.jsonFilter != null;
    }
    $scope.setEditTitleMode = function (value) {
      $scope.editTitleMode = value;
      if (value) {
        $scope.newChartTitle = $scope.chartInfo.name;
      }
    }

    var notifyUpdateState = function () { //update menu item in main menu bar

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportTrendChartUserAction, {
        action: 'refreshState',
      });

    }
    $scope.cancelEditChartTitle = function () {
      $scope.editTitleMode = false;
      $scope.newChartTitle = '';
    }
    $scope.editChartTitle = function () {
      var newChartTitle = $scope.newChartTitle;
      if (newChartTitle && newChartTitle.trim() != '') {
        $scope.chartInfo.name = newChartTitle.trim();
        $scope.editTitleMode = false;
        setChartTitleForDownload();
        $scope.saveChart(false, function () {
            $scope.newChartTitle = '';
          },
          function () {
            $scope.editTitleMode = true;
          });
      }
    }


    $scope.deleteSavedChart = function () {

      log.debug('delete saved chart: ' + $scope.chartInfo.id);
      var dlg: any = dialogs.confirm('Confirmation', 'You are about to delete chart named: \''+$scope.chartInfo.name+'\'. Continue?', null);
      dlg.result.then(function () {
        trendChartResource.deleteChart($scope.chartInfo.id, function () {
            notifyUpdateState();
            $scope.onChartDeleted();
          },
          function () {

          });
      }, function () {

      });
    }
    $scope.isSavedChart = function () {
      return $scope.chartInfo && $scope.chartInfo.id != null;
    }

    $scope.changeChartMaxData = function (val) {
      (<Dashboards.DtoTrendChart> $scope.chartInfo).viewResolutionMs = null;
      setResolutionText();
      (<Dashboards.DtoTrendChart>$scope.chartInfo).viewSampleCount = val;

      $scope.loadChartData();
      $scope.dirty = true;
    }


    $scope.changeChartResolutionData = function (baseUnit) {
      (<Dashboards.DtoTrendChart> $scope.chartInfo).viewSampleCount = null;
      (<Dashboards.DtoTrendChart> $scope.chartInfo).viewResolutionMs = resolutionsToText[baseUnit];
      setResolutionText();

      $scope.loadChartData();
      $scope.dirty = true;
    }
    var setChartResolutionStepState = function () {
      $scope.viewResolutionStep = (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep && (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep > 0 ? (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep : 1;
      $scope.inputResolutionStep = $scope.viewResolutionStep;
    }

    $scope.changeChartResolutionStep = function (resolutionStep: number) {
      (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep = resolutionStep;
      setChartResolutionStepState();

      $scope.loadChartData();
      $scope.dirty = true;
    }


    $scope.changeChartType = function (chartType: Dashboards.EChartDisplayType) {
      (<Dashboards.DtoTrendChart> $scope.chartInfo).chartDisplayType = chartType;
      $scope.selectItemRight = null;
      //  refreshChartDisplay(true); TODO
      $scope.dirty = true;
    }


    $scope.setLazyLoad = function (val: boolean) {
      $scope.lazyLoad = val;

    }

    var getCalcAbsoluteDate = function (rangePoint: Entities.DTODateRangePoint): number {
      if (!rangePoint) {
        return null;
      }
      if (rangePoint.absoluteTime) {
        return rangePoint.absoluteTime;
      }

      var periodInMillisec = resolutionsToText[(new String(rangePoint.relativePeriod)).toLowerCase()];
      var nowTimeAsUtc = getNowInUtcInMillisec(rangePoint.relativePeriod);
      return nowTimeAsUtc - periodInMillisec * rangePoint.relativePeriodAmount;
    }

    var getNowInUtcInMillisec = function (relativePeriod: Entities.ETimePeriod) {
      var now = new Date(Date.now());
      var nowTimeAsUtc;
      if (relativePeriod != Entities.ETimePeriod.HOURS) {
        nowTimeAsUtc = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);

      }
      else {
        nowTimeAsUtc = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes(), now.getSeconds());
      }
      return nowTimeAsUtc.getTime();
    }
    var setChartTitleForDownload = function () {
      $scope.chartTitleForDownload = ($scope.actualViewResolutionText ? $scope.actualViewResolutionText + ' ' : '') + $scope.chartInfo.name +
        ($scope.actualStartTime ? ' - ' + $filter('date')($scope.actualStartTime, $scope.titleTimeFormat, 'UTC') + ' to ' + $filter('date')($scope.actualEndTime, $scope.titleTimeFormat, 'UTC') + '' : '');

    }
    $scope.goForwardInDates = function () {

      var viewFromAbsDate: number = $scope.actualStartTimeInUTC;
      var viewToAbsDate: number = $scope.actualEndTimeInUTC;
      //   var diff=viewToAbsDate-viewFromAbsDate;
      var diff = Math.ceil($scope.actualSampleCount / 2) * $scope.actualViewResolutionMs;

      var endTime = new Entities.DTODateRangePoint();
      var startTime = new Entities.DTODateRangePoint();
      endTime.absoluteTime = viewToAbsDate + diff;
      startTime.absoluteTime = viewFromAbsDate + diff;
      (<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom = startTime;
      (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo = endTime;

      $scope.loadChartData();
      $scope.dirty = true;
    }
    $scope.goBackwardInDates = function () {
      var viewFromAbsDate: number = $scope.actualStartTimeInUTC;
      var viewToAbsDate: number = $scope.actualEndTimeInUTC;
      //var viewFromAbsDate:number = getCalcAbsoluteDate( (<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom);
      //var viewToAbsDate:number = getCalcAbsoluteDate( (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo)?getCalcAbsoluteDate( (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo):getNowInUtcInMillisec(Entities.ETimePeriod.HOURS);
      //   var diff=viewToAbsDate-viewFromAbsDate;
      var diff = Math.ceil($scope.actualSampleCount / 2) * $scope.actualViewResolutionMs;

      var startTime = new Entities.DTODateRangePoint();
      var endTime = new Entities.DTODateRangePoint();
      startTime.absoluteTime = viewFromAbsDate - diff;
      endTime.absoluteTime = viewToAbsDate - diff;
      (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo = endTime;
      (<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom = startTime;


      $scope.loadChartData();
      $scope.dirty = true;
    }

    var getDateFormatByResolutionMs = function (viewResolutionMs, startTime: Date, endTime: Date) {
      var dateFormat;
      var nowFullYear = (new Date(Date.now()).getFullYear());
      var fullYearRequired = startTime.getFullYear() != endTime.getFullYear() || startTime.getFullYear() != nowFullYear;
      if (viewResolutionMs >= resolutionsToText['hours'] && viewResolutionMs < resolutionsToText['days']) {
        if (startTime.getDate() != endTime.getDate()) {
          dateFormat = configuration.timeDateFormat
        }
        else {
          dateFormat = configuration.timeFormat
        }
      }
      else if (viewResolutionMs >= resolutionsToText['days'] && viewResolutionMs < resolutionsToText['months']) {
        if (fullYearRequired) {
          dateFormat = configuration.dateFormat;
        }
        else {
          dateFormat = configuration.dateNoYearFormat;
        }

      }

      else if (viewResolutionMs >= resolutionsToText['months'] && viewResolutionMs < resolutionsToText['years']) {
        if (fullYearRequired) {
          dateFormat = configuration.dateNoDayFormat;
        }
        else {
          dateFormat = configuration.monthFormat;
        }

      }

      else if (viewResolutionMs >= resolutionsToText['years']) {
        dateFormat = configuration.yearFormat;
      }
      else {
        dateFormat = configuration.dateFormat;
      }
      return dateFormat;
    }

    var getDateTextFromFormat = function (dateToFormat: Date, dateFormat, viewResolutionMs) {
      var text;
      if (viewResolutionMs == resolutionsToText['quarters']) {
        var quarter = Math.ceil(dateToFormat.getMonth() / 3);
        text = $filter('date')(dateToFormat, configuration.yearFormat, 'UTC') + 'Q' + quarter;
      }
      else {
        text = $filter('date')(dateToFormat, dateFormat, 'UTC');
      }
      return text;
    }


    $scope.loadChartData = function () {
      var trendChart = <Dashboards.DtoTrendChart>$scope.chartInfo;
      var absoluteStartDate = getCalcAbsoluteDate(trendChart.viewFrom);
      var absoluteEndDate = getCalcAbsoluteDate(trendChart.viewTo) ? getCalcAbsoluteDate(trendChart.viewTo) : getNowInUtcInMillisec(Entities.ETimePeriod.HOURS);
      var resolutionSteps = (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep ? (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).viewResolutionStep : 1;
      if (((absoluteEndDate - absoluteStartDate) / (trendChart.viewResolutionMs * resolutionSteps)) > MAX_SAMPLE_COUNT_RETURNED) {
        //limit number of requested samples to max allowed
        absoluteEndDate = absoluteStartDate + (trendChart.viewResolutionMs * resolutionSteps * MAX_SAMPLE_COUNT_RETURNED);
      }

      trendChartResource.getChartData(trendChart.id, absoluteStartDate, absoluteEndDate, $scope.chartInfo.viewSampleCount, trendChart.viewResolutionMs * resolutionSteps,
        function (chartData: Dashboards.DtoTrendChartDataSummary) {

          $scope.actualStartTime = new Date(chartData.startTime);
          $scope.actualStartTimeInUTC = chartData.startTime;
          $scope.titleTimeFormat = chartData.viewResolutionMs == resolutionsToText['hours'] ? configuration.dateTimeFormat : configuration.dateFormat;
          $scope.actualEndTime = new Date(chartData.endTime);
          $scope.actualEndTimeInUTC = chartData.endTime;
          $scope.actualViewResolutionMs = chartData.viewResolutionMs;

          for (var key in resolutionsToText) {
            if ($scope.actualViewResolutionMs == resolutionsToText[key]) {
              $scope.actualViewResolutionText = $filter('translate')('TRENDCHART_Time_resolution_adverb_' + key);
            }
          }
          $scope.valueAxisType = chartData.trendChartDto.valueType;
          setChartTitleForDownload();

          var categories: ui.ICategoryChartData[] = [];  //x-axis labels

          var categoryDateFormat = getDateFormatByResolutionMs($scope.actualViewResolutionMs, $scope.actualStartTime, $scope.actualEndTime);
          if (chartData.trendChartSeriesDtoList && chartData.trendChartSeriesDtoList.length > 0) {
            $scope.actualSampleCount = Math.min(MAX_SAMPLE_COUNT_RETURNED, chartData.trendChartSeriesDtoList[0].values.length);
            for (var i = 0; i < $scope.actualSampleCount; i++) {
              var timestamp = chartData.startTime + i * $scope.actualViewResolutionMs;
              var text = getDateTextFromFormat(new Date(timestamp), categoryDateFormat, $scope.actualViewResolutionMs);
              var c = <ui.ICategoryChartData>{name: text, data: timestamp};
              categories.push(c);
            }
            ;
            var series: ui.ISeriesChartData[] = [];
            chartData.trendChartSeriesDtoList.forEach(s => {
              var aSeries = <ui.ISeriesChartData>{
                name: chartData.trendChartDto.seriesType == 'file.id' ? (<Dashboards.DtoTrendChartSeries> s).filter : (<Dashboards.DtoTrendChartSeries> s).name == 'numFound' ? TOTAL_SERIES_ID : (<Dashboards.DtoTrendChartSeries> s).name,
                data: (<Dashboards.DtoTrendChartSeries> s).values.length <= MAX_SAMPLE_COUNT_RETURNED ?
                  (<Dashboards.DtoTrendChartSeries> s).values : (<Dashboards.DtoTrendChartSeries> s).values.slice(0, $scope.actualSampleCount)
              };


              series.push(aSeries);
            });
            var othersData = [];
            var totalSeries = series.filter(s => s.name == TOTAL_SERIES_ID)[0];
            var allSeriesExTotal = series.filter(s => s.name != TOTAL_SERIES_ID);
            $scope.totalSeriesExists = totalSeries != null;
            if (totalSeries) {
              if (allSeriesExTotal.length > 0) {
                for (var i = 0; i < totalSeries.data.length; i++) {
                  var allSeriesExTotalValue = allSeriesExTotal.map(v => v.data[i]).reduce((a, b) => a + b); //sum of all values that are not in the total series for a category i
                  var othersValue = totalSeries.data[i] - allSeriesExTotalValue;
                  othersData.push(othersValue);
                }
                var othersSeries = <ui.ISeriesChartData>{name: OTHERS_SERIES_ID, data: othersData};
                series.push(othersSeries);
              }
              refreshChartDisplay(false, series);
            }
          }


          var stackedHistogramData = <ui.IStackedChartData>{categories: categories, series: series};
          $scope.histogramData = stackedHistogramData;

          $scope.initiated = true;


        }, function () {

        });


    }

    var refreshChartDisplay = function (callRefresh: boolean, series?) {
      if (!series) {
        var chartData = $scope.getChartObject();
        series = chartData.options.series;
      }
      var totalSeries = series.filter(s => s.name == TOTAL_SERIES_ID)[0];
      var othersSeries = series.filter(s => s.name == OTHERS_SERIES_ID)[0];
      if (totalSeries) {
        totalSeries['type'] = (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesType ? (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesType : 'line';
        totalSeries['visible'] = !(<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesHidden;
        totalSeries['color'] = '#26bbdd';
        totalSeries.markers = {type: 'circle'};
        totalSeries.line = {style: 'step', width: totalSeries.type == 'area' ? 0 : 2};
        totalSeries['opacity'] = totalSeries.type == 'area' ? 0.6 : 1;
      }
      if (othersSeries) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesType ? othersSeries.type = (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesType : null;
        othersSeries['visible'] = !(<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesHidden;
        othersSeries.markers = {type: 'circle'};
        othersSeries.line = {style: 'step', width: totalSeries.type == 'area' ? 0 : 2};
        othersSeries['opacity'] = othersSeries.type == 'area' ? 0.6 : 1;
        othersSeries['color'] = '#4F6990';
      }
      if (callRefresh) {
        $timeout(function () {
          var chartData = $scope.getChartObject();
          chartData.refresh();
        })
      }
    }

    $scope.changeDate = function (count, countResolution: Entities.ETimePeriod) {

      var startTime = new Entities.DTODateRangePoint();
      startTime.relativePeriod = countResolution;
      startTime.relativePeriodAmount = count;
      (<Dashboards.DtoTrendChart> $scope.chartInfo).viewFrom = startTime;
      (<Dashboards.DtoTrendChart> $scope.chartInfo).viewTo = null;


      $scope.loadChartData();
      $scope.dirty = true;
    }


    $scope.openChangeDateDialog = function () {
      var dlg = dialogs.create(
        'common/directives/dialogs/pickDatesDialog.tpl.html',
        'pickDatesDialogCtrl',
        {
          startDate: (<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom ? (new Date(getCalcAbsoluteDate((<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom))) : null,
          endDate: (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo ? (new Date(getCalcAbsoluteDate((<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo))) : null
        },
        {size: 'md'});

      dlg.result.then(function (data) {
        (<Dashboards.DtoTrendChart>$scope.chartInfo).viewFrom = <Entities.DTODateRangePoint> data.startTime;
        (<Dashboards.DtoTrendChart>$scope.chartInfo).viewTo = <Entities.DTODateRangePoint> data.endTime;
        $scope.loadChartData();
        $scope.dirty = true;

      }, function () {

      });

    }


    $scope.copySavedChart = function () {
      log.debug('copy chart');
      var newChart = angular.copy($scope.chartInfo);

      (<Dashboards.DtoTrendChart>newChart).name += ' - Copy';
      (<Dashboards.DtoTrendChart>newChart).trendChartCollectorId = newChart.id;
      (<Dashboards.DtoTrendChart>newChart).viewConfiguration = JSON.stringify($scope.viewConfiguration);
      delete newChart.id;
      $scope.saveNewChart(newChart);

    }


    $scope.selectItemRight = null;
    $scope.$watch(function () {
      return $scope.selectItemRight;
    }, function (value) {
    });

    $scope.onSeriesClick = function (e, chartData) {
      closeAllDdls();
      e.preventDefault();
      var theClickedSeries = chartData.options.series.filter(s => s == e.series)[0];
      // if(theClickedSeries.name==TOTAL_SERIES_ID)
      // {
      //   theClickedSeries.type=theClickedSeries.type=='area'?'line':theClickedSeries.type=='line'?'column':'area';
      //  theClickedSeries.type=='area'?theClickedSeries.line={style:'step'}: delete theClickedSeries.line;
      //   theClickedSeries.markers={type : "circle"};
      //   theClickedSeries.line={style:'step',width:2};
      //   theClickedSeries['opacity'] = 1;
      // }
      //else {
      if (theClickedSeries.labels.visible) {
        if (theClickedSeries.labels.rotation == 90) {
          theClickedSeries.labels.rotation = 0;
          theClickedSeries.labels.position = null;
        }
        else if (theClickedSeries.labels.rotation == 0) {
          theClickedSeries.labels.visible = false;
        }
      }
      else {
        theClickedSeries.labels.visible = true;
        theClickedSeries.labels.rotation = 90;
        theClickedSeries.labels.position = 'top';
      }
      // }
      $timeout(function () {
        chartData.refresh();
      })
      return false;
    }
    var closeAllDdls = function () {
      $('html').trigger('click'); //to close all ddls
    }
    $scope.onLegenedItemClick = function (e, chartData) {
      closeAllDdls();
      var theClickedSeries = chartData.options.series[e.seriesIndex];
      var seriesVisibilityAfterToggle = !theClickedSeries['visible'];
      if (theClickedSeries.name == TOTAL_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).totalSeriesHidden = !seriesVisibilityAfterToggle;
        $scope.dirty = true;
        $scope.$applyAsync(); //takes to long until changes are applied
      }
      else if (theClickedSeries.name == OTHERS_SERIES_ID) {
        (<Dashboards.TrendChartViewConfiguration>$scope.viewConfiguration).otherSeriesHidden = !seriesVisibilityAfterToggle;
        $scope.dirty = true;
        $scope.$applyAsync();
      }
    }


    $scope.saveChart = function (reloadList, successFunction?: () => void) {
      if ($scope.isSavedChart() && !$scope.readOnly) {
        $scope.dirty = false;
        (<Dashboards.DtoTrendChart>$scope.chartInfo).viewConfiguration = JSON.stringify($scope.viewConfiguration);
        trendChartResource.updateChart(<Dashboards.DtoTrendChart>$scope.chartInfo, function () {

          if (reloadList) {
            $scope.onChartSaved(reloadList);
          }
          notifyUpdateState();
          if (successFunction) {
            successFunction();
          }
        }, function () {
          $scope.dirty = true;
          onError();
        });
      }
    }

    $scope.saveNewChart = function (chartToSave) {
      var chart = chartToSave ? chartToSave : $scope.chartInfo;
      delete $scope.chartInfo.notSaved;
      trendChartResource.addNew(chart, function (chart: Dashboards.DtoTrendChart) {
        $scope.allowSaveChart = false;
        $scope.dirty = false;
        $scope.chartInfo = chart;
        $scope.onChartSaved();
        notifyUpdateState();
      }, function () {
        $scope.chartInfo.notSaved = true;
        onError();
      });


    }

    var onError = function () {
      dialogs.error('Error', 'Sorry, an error occurred.');
    }

  })
  .directive('trendChartWidget', function () {
    return {
      restrict: 'EA',
      templateUrl: '/app/dashboardViews/trendChart.tpl.html',
      replace: true,
      transclude: true,
      scope:
        {
          chartInfo: '=',
          lazyLoad: '=',
          readOnly: '=',
          allowSaveChart: '=',
          onChartSaved: '=',
          onChartDeleted: '=',


        },
      controller: 'TrendChartWidgetCtrl',
      link: function (scope: any, element, attrs) {
        scope.init();
      }
    }

  });


///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports.charts.tree',[
      'directives',
      'directives.dashboardWidgets',
      'resources.charts',
      'reports.charts.directive'

    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.docTypesChart, {
            //  abstract: true,
            url: '/docTypesHier',
            templateUrl: 'app/dashboardViews/treeGraphPage.tpl.html',
            controller: 'TreeGraphPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngReports,Users.ESystemRoleName.ViewReports]
            }
          })

    })
    .controller('TreeGraphPageCtrl', function ($scope,$element,$state,$q,$filter,$stateParams,$compile,chartResource:IChartsResource,dialogs,$timeout,filterResource:IFilterResource,eventCommunicator:IEventCommunicator,filterFactory:ui.IFilterFactory,
                                               $window,Logger:ILogger,propertiesUtils,filesResource:IFilesResource, configuration:IConfig,pageTitle,docTypesResource:IDocTypesResource,splitViewFilterServiceFactory,currentUserDetailsProvider:ICurrentUserDetailsProvider,demoService: IDemoService) {
      var log:ILog = Logger.getInstance('TreeGraphPageCtrl');
      var splitViewFilterService:IFilterService = splitViewFilterServiceFactory.getInstance();

      $scope.init=function()
      {
        pageTitle.set("Reports: Datamap");

        if(currentUserDetailsProvider.isDemoInstallation()) {
          demoService.getDemoInfo(
            (demoInfo) => {
              $scope.demoInfo = demoInfo || {};
              loadDocTypeList();
            }, (err) => {
            });
        }else{
          loadDocTypeList();
        }
      };
      $scope.getFilterService = function()
      {
        return splitViewFilterService;
      }

      $scope.toggleShowUnAssociatedToChart = function()
      {
          $scope.showUnAssociated = !$scope.showUnAssociated;

        refreshGraphData();

      }
      var setActiveSetFilter = function(rawFilter:FilterData)
      {
        splitViewFilterService.setFilterData(rawFilter);
        refreshGraphData();
      }
      var refreshGraphData=function()
      {
        loadDocTypeList();
        notifyUpdateFilterState();
      }

      $scope.$on('$destroy', function(){
        if(childScope) {
          childScope.$destroy();
        }
      });
      function notifyUpdateFilterState () {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsSetState, {
          action: 'reportUpdateState',
        });

      }

      /*
      var createDataForExcelEport=function(docTypes:Entities.DTOAggregationCountItem<Entities.DTODocType>[],idToNameDic)
      {
        // $scope.exportToExcelInProgress=true;
        var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODocType> =  Entities.DTOAggregationCountItem.Empty();
        var dTODocType:Entities.DTODocType =  Entities.DTODocType.Empty();
        var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

        //  $scope.exportRightToExcelInProgress = true;
        var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.id),
          title: 'ID',
          type: EFieldTypes.type_number,
          isPrimaryKey:true,
          displayed: true,

        };
        var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.name),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,

        };
        var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.parentId),
          title: 'parent Id',
          isParentId:true,
          type: EFieldTypes.type_number,
          displayed: true,
        };
        var parentName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: 'parentName',
          title: 'parent name',
          isParentId:true,
          type: EFieldTypes.type_number,
          displayed: true,
        };
        var numOfDirectFiles:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
          title: '# direct files',
          isParentId:true,
          type: EFieldTypes.type_number,
          displayed: true,
        };
        var numOfAllFiles:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2),
          title: '# all files',
          isParentId:true,
          type: EFieldTypes.type_number,
          displayed: true,
        };
        var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.description),
          title: 'Description',
          type: EFieldTypes.type_string,
          displayed: true,
        };

        var fieldsForCsv:ui.IHierarchicalFieldDisplayProperties[] =[];
        fieldsForCsv.push(id);
        fieldsForCsv.push(name);
        fieldsForCsv.push(description);
        fieldsForCsv.push(parentId);
        fieldsForCsv.push(parentName);
        fieldsForCsv.push(numOfDirectFiles);
        fieldsForCsv.push(numOfAllFiles);


        if(docTypes) {
          docTypes.forEach(function (g) {

            var docType:Entities.DTODocType = g.item;
            (<any>g).name = docType.name;
            (<any>g).id = docType.id;
            (<any>g).parentId = docType.parentId;
            (<any>g).description = docType.description;
            if(idToNameDic[docType.parentId]) {
              (<any>g).parentName = idToNameDic[docType.parentId].name;

            }
            else
            {
              (<any>g).parentName = '';
            }

          });
        }
        $scope.docTypesForExcel=docTypes;
        $scope.docTypesForExcelFields=fieldsForCsv;
      }
      */

      var childScope;
      var loadNewTreeGraph = function()
      {
        $('.tree-graph').empty();
        if(childScope) {
          childScope.$destroy();
        }

       var childScope = $scope.$new();
        var compiledDirective = $compile(' <tree-view-graph histogram-data="data" export-to-excel-data="docTypesForExcel" export-to-excel-filter="filterForExcel"  export-to-excel-data-fields="docTypesForExcelFields"' +
            '  items-to-expand="itemsToExpand"></tree-view-graph>');
        var directiveElement = compiledDirective(childScope);
       $('.tree-graph').html(directiveElement);
      }

      var filterByUngrouped = function()
      {
        var filter =  filterFactory.createFilterByUngrouped();
        splitViewFilterService.addPageFilter(filter);
        refreshGraphData();
      };
      var filterByGroupDetachedFiles = function()
      {
        var filter =  filterFactory.createFilterByGroupDetachedFiles();
        splitViewFilterService.addPageFilter(filter);
        refreshGraphData();
      };
      var removePageRestrictionFilterTag = function (filter:SingleCriteria) {
        splitViewFilterService.removePageRestrictionFilter(filter);
        refreshGraphData();

      };

      var toggleOperator = function (filter:SingleCriteria) {
        splitViewFilterService.toggleOperatorToFilterTag(filter);
        refreshGraphData();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsSetState, {
          action: 'reportUpdateState'
        });
      };

      var removePageUserFilterTag = function (filters:SingleCriteria[]) {
       filters.forEach(f=>  splitViewFilterService.removePageUserFilter(f));
        refreshGraphData();
      };

      var removePageFilterTag = function (filter:SingleCriteria) {
        splitViewFilterService.removePageFilter(filter);
        refreshGraphData();
       };

      var clearUserSearch = function () {
        splitViewFilterService.clearUserSearchFilter();
        refreshGraphData();
       };
      $scope.clearActiveSavedFilter = function()
      {
        splitViewFilterService.setFilterData(new FilterData());
        refreshGraphData();
      };
      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportFilterTagsUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {


           if(fireParam.action === 'loadSavedFilter')
            {
              setActiveSetFilter(fireParam.filterData);
            }
           else if (fireParam.action === 'filterByUngrouped') {
             filterByUngrouped();
           }
           else if (fireParam.action === 'filterByGroupDetachedFiles') {
             filterByGroupDetachedFiles();
           }
           else if (fireParam.action === 'clearUserSearch') {
             clearUserSearch();
           }
           else if (fireParam.action === 'removePageRestrictionFilterTag') {
             removePageRestrictionFilterTag(fireParam.filter);
           }
           else if (fireParam.action === 'removePageFilterTag') {
             removePageFilterTag(fireParam.filter);
           }
           else if (fireParam.action === 'removePageUserFilterTag') {
             removePageUserFilterTag(fireParam.filters);
           }
           else if (fireParam.action === 'clearAll') {
             $scope.clearActiveSavedFilter();
           }
           else if (fireParam.action === 'toggleOperator') {
             toggleOperator(fireParam.filter);
           }

          })));
      var followUpFilter = filterFactory.createSubDocTypesFilter();
      var allByFieldFilter = filterFactory.createAllByFieldFilter();


      var doFollowUp = function (docTypeId,docTypeName) {
         var filterDataToRestore = onBeforeFollowUp(docTypeId,docTypeName);
        //setIsSwitchViewsDisabled(followUp.view);
       var url = followUpHandler();
        splitViewFilterService.setFilterData(filterDataToRestore);
        return url;
      };

      var followUpHandler=function() {
        $state.params['left'] = EntityDisplayTypes.groups.toString();
        $state.params['right'] = EntityDisplayTypes.files.toString();


        var thinFilterCopy = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
          if (value == null) {
            return undefined;
          }
          return value;
        });
        $state.params['filter'] = thinFilterCopy; //remove filter from url cause filter is set trough service


        var url = $state.href('report', $state.params);
      //  $window.open(url,'_blank');
        return url;
      };

      var onBeforeFollowUp = function (docTypeId,docTypeName) {
        var filterDataToRestore =JSON.parse(JSON.stringify(splitViewFilterService.getFilterData()));
        var filter = followUpFilter(docTypeId,docTypeName);
        splitViewFilterService.addPageFilter(filter);
        return filterDataToRestore;
      };


      var getFilterDataToRestoreForUnassociate = function() {
        var filterDataToRestore =JSON.parse(JSON.stringify(splitViewFilterService.getFilterData()));
        var docTypesFilterCriteria = allByFieldFilter(configuration.subDocTypesFilterFieldName, EFilterOperators.notEquals, 'Unassociated files');
        splitViewFilterService.addPageFilter(docTypesFilterCriteria);
        return filterDataToRestore;
      };

      var getDocTypeStyleId = function(docType)
      {
        var colors = parseInt(configuration.max_docTypes_colors);
        if(docType.parents&& docType.parents.length>0)
        {
          var result = docType.parents[0]%colors+1;
          return result;
        }
        return docType.id%colors+1;
      };

      var loadDocTypeList = function() {
        setItemsToExpand();
        var loopPromises = [];
        var filter =splitViewFilterService.toKendoFilter();

        var deferredFileCount = $q.defer();
        loopPromises.push(deferredFileCount.promise);
        if( $scope.showUnAssociated ) {
          filesResource.getFilesCount(filter, null, function (count:number) {
            $scope.filesCount = count;

            deferredFileCount.resolve();


          }, function () {
          });
        }
        else {
          deferredFileCount.resolve();
        }
        var deferredDocTypes = $q.defer();
        loopPromises.push(deferredDocTypes.promise);
        docTypesResource.getAssignedDocTypes(filter,function (docTypes:Entities.DTOAggregationCountItem<Entities.DTODocType>[],totalAggregatedCount?:number) {
          var result=[];
          $scope.totalAggregatedCount= totalAggregatedCount;
          $scope.docTypesCount = docTypes?docTypes.length:0;

          var idToNameDic=[];
          docTypes.forEach(i=>{
            var docTypeItem :Entities.DTODocType=<Entities.DTODocType>i.item;
            (<any>docTypeItem).isLeaf=true;
            (<any>docTypeItem).count=i.count;
            (<any>docTypeItem).count2=i.count2;
            (<any>docTypeItem).subtreeCount=i.subtreeCount;
            (<any>docTypeItem).fileTagDtos = _.flatten(docTypeItem.fileTagTypeSettings.map(t=>t.fileTags));
            idToNameDic[docTypeItem.id]=docTypeItem;

          });

          //createDataForExcelEport(docTypes,idToNameDic);
          $scope.docTypesForExcel=docTypes;
          $scope.filterForExcel = splitViewFilterService.toKendoFilter();
          for (var i = 0; i < docTypes.length; ++i) {
            var docTypeItem :Entities.DTODocType=<Entities.DTODocType>docTypes[i].item;
            if(docTypeItem.parents.length>1)
            {
              var directParentId = docTypeItem.parents[docTypeItem.parents.length-2];
              var directParent =  idToNameDic[directParentId];
              if(directParent) {
                (<any>directParent).isLeaf = false;
              }
              else
              {
                //var newDirectParent = docTypeItem.filter(d=>d.id==directParentId)[0];
                //(<any>newDirectParent).isLeaf=false;
                //(<any>newDirectParent).count=0;
                //idToNameDic[directParentId] =newDirectParent;

              }
            }
          }
         //  var rows = idToNameDic.filter(i=>(<any>i).count>0 || (<any>i).isLeaf==true);
          var rows = idToNameDic.filter(i=> (<any>i).isLeaf==true);
          rows.forEach(row=>{
            let chartItem = createChartItem(row, idToNameDic);
            result.push(chartItem)
          });
          $scope.data = result;

          setTimeout(()=> {
            repaintCircles();
          },1000);

          deferredDocTypes.resolve();

        }, function () {
          onError();
        });

        var createChartItem = function(row, idToNameDic) {
          var chartItem = new treeChartItem();
          chartItem.fileTagDtos =row.fileTagDtos;

          chartItem.Category ='DocType';
          for(var i=0;i<8;i++) {
            if(row.parents[i]) {
              chartItem[`Level${i+1}ID`] =row.parents[i];
              chartItem[`Level${i+1}FileTagDtos`] = idToNameDic[row.parents[i]].fileTagDtos;
              chartItem[`Level${i+1}`] = idToNameDic[row.parents[i]].name;
              chartItem[`Level${i+1}Count`] = idToNameDic[row.parents[i]].count;
              chartItem[`Level${i+1}CountAgg`] = idToNameDic[row.parents[i]].subtreeCount;
              chartItem[`Level${i+1}StyleId`]= getDocTypeStyleId(idToNameDic[row.parents[i]]);

              chartItem[`Level${i+1}drillDownLink`] = (function(row) {
                return function () {
                  return doFollowUp(row.id, row.name);
                }
              })(idToNameDic[row.parents[i]]);

              if(!_.isEmpty($scope.demoInfo)) {
                chartItem[`Level${i+1}ExtraInfo`] = $scope.demoInfo[idToNameDic[row.parents[i]].name];
              }
            }
            else {
              chartItem[`Level${i+1}`] ='none';
              chartItem[`Level${i+1}Count`] =0;
              chartItem[`Level${i+1}CountAgg`] =0;
            }
          }
          return chartItem;
        };

        var repaintCircles = function() {
          let arr = [];
          let c = $('.tree-chart svg g.node circle');

          _.forEach(c, (elm) => {
            arr.push({
              size: elm['r'].baseVal.value,
              elm: $(elm).parent(),
            });
          });

          arr.sort(function (a, b) {
            return (a.size > b.size) ? 1 : ((b.size > a.size) ? -1 : 0);
          }).reverse();

          _.forEach(arr, (item) => {
            item.elm.detach();
          });

          let g = $('.tree-chart svg g');
          _.forEach(arr, (item) => {
            g.append(item.elm);
          });
        };

        $q.all(loopPromises).then(function (result) {
        //  chartData.push(new Dashboards.DtoCounterReport($scope.unassociatedText,'',unassociatedFilesCount));
          if( $scope.showUnAssociated ) {
            var unassociatedFilesCount = $scope.filesCount - $scope.totalAggregatedCount;
            var chartItem = new treeChartItem();
            chartItem.Level1ID ='Unassociated' ;
            chartItem.Level1 =$filter('translate')('Unassociated') ;
            chartItem.Level1Count = unassociatedFilesCount;
            chartItem.Level1CountAgg = unassociatedFilesCount;
            chartItem.Level2 = 'none';
            chartItem.Level3 = 'none';
            chartItem.Level4 = 'none';
            chartItem.Level5 = 'none';
            chartItem.Level1drillDownLink = function() {
              return function () {
                //var filterDataToRestore = onBeforeFollowUp(docTypeId, docTypeName);
                //setIsSwitchViewsDisabled(followUp.view);
                var filterDataToRestore = getFilterDataToRestoreForUnassociate();
                var url = followUpHandler();
                splitViewFilterService.setFilterData(filterDataToRestore);
                return url;
              }
            };
            $scope.data.push(chartItem);

          }

          loadNewTreeGraph();
        });

      };
      var setItemsToExpand = function()
      {
        if(!$scope.itemsToExpand)
        {

          var itemsToExpand={};
          $scope.itemsToExpand = itemsToExpand;
        }
      };
      $scope.getFilterDisplayedName = function(tag:SingleCriteria)
      {
        return filterFactory.getFilterReportDisplayedName(tag);
      };
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
        splitViewFilterService.setFilterData(new FilterData());
      });
      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    });
class treeChartItem
{
  Level1ID:string;
  Level2ID:string;
  Level3ID:string;
  Level4ID:string;
  Level5ID:string;
  Level6ID:string;
  Level7ID:string;
  Level8ID:string;
  Level1FileTagDtos:any;
  Level2FileTagDtos:any;
  Level3FileTagDtos:any;
  Level4FileTagDtos:any;
  Level5FileTagDtos:any;
  Level6FileTagDtos:any;
  Level7FileTagDtos:any;
  Level8FileTagDtos:any;
  Category:string;
  Level1:string;
  Level2:string;
  Level3:string;
  Level4:string;
  Level5:string;
  Level6:string;
  Level7:string;
  Level8:string;
  Level1Count:number;
  Level2Count:number;
  Level3Count:number;
  Level4Count:number;
  Level5Count:number;
  Level6Count:number;
  Level7Count:number;
  Level8Count:number;
  Level1CountAgg:number;
  Level2CountAgg:number;
  Level3CountAgg:number;
  Level4CountAgg:number;
  Level5CountAgg:number;
  Level6CountAgg:number;
  Level7CountAgg:number;
  Level8CountAgg:number;
  Level1StyleId:number;
  Level2StyleId:number;
  Level3StyleId:number;
  Level4StyleId:number;
  Level5StyleId:number;
  Level6StyleId:number;
  Level7StyleId:number;
  Level8StyleId:number;
  Level1drillDownLink:any;
  Level2drillDownLink:any;
  Level3drillDownLink:any;
  Level4drillDownLink:any;
  Level5drillDownLink:any;
  Level6drillDownLink:any;
  Level7drillDownLink:any;
  Level8drillDownLink:any;
  Level1ExtraInfo:string;
  Level2ExtraInfo:string;
  Level3ExtraInfo:string;
  Level4ExtraInfo:string;
  Level5ExtraInfo:string;
  Level6ExtraInfo:string;
  Level7ExtraInfo:string;
  Level8ExtraInfo:string;
  Federal:number;
  Total:number;
  State:number;
  Local:number;
  fileTagDtos: any[]
}

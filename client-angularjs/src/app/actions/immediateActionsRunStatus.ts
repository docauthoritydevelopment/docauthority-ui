///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('actions.immediate',[
      'resources.actions.immediate'
    ])

    .controller('ImmediateActionsRunStatusCtrl', function ($window, $scope,$element,$state,$stateParams,$compile,$filter,eventCommunicator:IEventCommunicator,
                                                   Logger:ILogger,propertiesUtils, configuration:IConfig,$timeout,dialogs ,
                                                           immediateActionsResource:IImmediateActionsResource) {
      var log:ILog = Logger.getInstance('ImmediateActionsRunStatusCtrl');
      $scope.initiated=false;

      $scope.init=function()
      {
         checkStatus();
      };

      var timeoutPromise;
      var checkStatus = function()
      {
        immediateActionsResource.immediateActionsProgress(null,immediateActionsProgressCompleted,onGetError);
      };

      var checkStatusAndReInitPooling=function()
      {
        $timeout.cancel(timeoutPromise);
        checkStatus();
      }
      $scope.stopImmediateAction = function()
      {
        var dlg = dialogs.confirm('Confirmation', 'Are you sure you want to stop file creation?', null);
        dlg.result.then(function (btn) { //user confirmed deletion
          immediateActionsResource.stopImmediateAction($scope.actionTriggerId,function(){
            $scope.errorMsg='';
            checkStatusAndReInitPooling();
            notifyChangeRunStatus(true);
          },$scope.onGetError);
        }, function (btn) {
          //  'You confirmed "No."';
        });


      };

      var immediateActionsProgressCompleted = function(status:Actions.DtoActionExecutionTaskStatus) {
        $scope.initiated = true;
        $scope.status = status;
        $scope.actionTriggerId = status.actionTriggerId;
        $scope.currentImmediateActionProgressPercentage=Math.round(status.progressMark/status.progressMaxMark*100);
        $scope.isImmediateActionRunning = status.status == Actions.DtoActionExecutionTaskStatus.status_RUNNING;
        $scope.enableStartImmediateActionButton = status.status != Actions.DtoActionExecutionTaskStatus.status_RUNNING;
        notifyChangeRunStatus( $scope.enableStartImmediateActionButton );
        timeoutPromise= $timeout(checkStatus, 28000);

      };

      $scope.$on('$destroy', function(){
        eventCommunicator.unregisterAllHandlers($scope);
        if (angular.isDefined(timeoutPromise)) {
          $timeout.cancel(timeoutPromise);
          timeoutPromise = undefined;
        }
      });

      var onGetError=function(e)
      {
        if(e&&e.userMsg) {
          var dlg =dialogs.error('Apply tags could not be started. Scan process is currently running.', null);
          ;

        }
        $timeout.cancel(timeoutPromise);
        timeoutPromise= $timeout(checkStatus, 28000);
        $scope.initiated = true;
        log.error("error get solr apply status " +e);
        $scope.enableStartApplyButton = true;
        $scope.errorMsg = 'Some difficulties occurred: Contact support'
      };


      var notifyChangeRunStatus = function (enable:boolean) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportActionsRunningTasksStatusUserAction, {
          action: 'checkStatus',
          enable:enable
        });
      };


      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportHeaderUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
            /*
             * fireParam object:
             *   { initSearch: false, value: searchText, mode: mode}
             */
            if (fireParam.action === 'checkStatus') {
              $scope.actionTriggerId = fireParam.actionTriggerId;
              checkStatusAndReInitPooling();
            }
          })));
    })
    .directive('immediateActionsStatusBar',
         function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/actions/immediateActionsRunStatus.tpl.html',
            controller: 'ImmediateActionsRunStatusCtrl',
            replace:true,
            scope:{

            },
            link: function (scope:any, element, attrs) {

              scope.init();
            }
          };
        }
    )

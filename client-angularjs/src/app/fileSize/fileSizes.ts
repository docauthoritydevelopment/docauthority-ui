///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('fileSizes',[
]);

angular.module('fileSizes')
  .config(function ($stateProvider) {
    $stateProvider
      .state('fileSizes', {
        //   abstract: true,
        url: '/fileSizes',
        templateUrl: '/app/fileSizes/fileSizes.tpl.html',
        controller: 'FileSizesPageCtrl'
      })

  })
  .controller('FileSizesPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('FileSizesPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('FileSizesCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('FileSizesCtrl');

    var _this = this;

    $scope.elementName='file-sizes-grid';
    $scope.entityDisplayType = EntityDisplayTypes.file_sizes;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null , true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOFileSizePartition> =  Entities.DTOAggregationCountItem.Empty();
    var dTOFileSize:Entities.DTOFileSizePartition =  Entities.DTOFileSizePartition.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOFileSize, dTOFileSize.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };

    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="File size: #: displayName #">#: displayName # <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='fileSizeClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this file size</a></li>"+
        '  </ul></span>';
    $scope.fileSizeClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOFileSizePartition>) {

      setFilterByEntity(item.item.id,(<any>item).displayName);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line'  href='fileSize'></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
        nameNewTemplate+"<div></div></td>" +
          //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

        '<td colspan="1" style="width:220px" >'+filterTemplate+'</td>'+
        "</tr>";


    var parseData = function (fileSizes:Entities.DTOAggregationCountItem< Entities.DTOFileSizePartition>[]) {
      if(fileSizes) {
        fileSizes.forEach(fSize=>
        {
          (<any>fSize).displayName = fSize.item.label;
          (<any>fSize).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(fSize.count , fSize.count2 );
        });
        return fileSizes;
      }
      return null;
    };
    var onSelectedItemChanged = function()
    {
       $scope.itemSelected($scope.selectedItem[id.fieldName],(<any>$scope.selectedItem).displayName);
    }
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('fileSizesGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        totalElements: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
        initialLoadParams: '=',
      },
      controller: 'FileSizesCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

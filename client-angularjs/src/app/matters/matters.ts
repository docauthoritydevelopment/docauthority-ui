///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('matters', [
  'ui.router'
]);

angular.module('matters').config(function ($stateProvider) {
  $stateProvider.state('matters', {
    url: '/matters',
    templateUrl: '/app/matters/matters.tpl.html',
    controller: 'MattersPageCtrl'
  })
})
.controller('MattersPageCtrl', function ($scope, $state, $location, Logger, pageTitle) {
  var log:ILog = Logger.getInstance('MattersPageCtrl');

  $scope.init = function () {
    pageTitle.set("Manage Matters");
  };
})
.controller('ManageMattersCtrl', function ($scope, $state, $timeout, $location, Logger, pageTitle,chartResource:IChartsResource,propertiesUtils,
                                               splitViewBuilderHelper:ui.ISplitViewBuilderHelper,$templateCache, userSettings:IUserSettingsService,fileTagsResource:IFileTagsResource,
                                               saveFilesBuilder:ISaveFileBuilder,dialogs,configuration,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
  var log:ILog = Logger.getInstance('ManageMattersCtrl');

  $scope.init = function() {
    setUserRoles();
    $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
      setUserRoles();
    });
  };

  var setUserRoles = function() {
    $scope.isMngMatterConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDepartmentConfig]);
  };

  $scope.userFilterChanged = function(userFilterText) {
    $timeout(function() {
      if(userFilterText && userFilterText.trim() != '') {
        $scope.filterData = {
          filter: { field: "name", operator: "contains", value: userFilterText},
          isClientFilter: true
        };
      }
      else {
        $scope.clearUserFilter();
      }},200);
  };

  $scope.clearUserFilter = function() {
    $scope.userFilterText = null;
    $scope.filterData = { filter: {}, isClientFilter: true };
  };

  $scope.onSelectMatterItems = function(data) {
    $scope.matterSelectItems = data;
  };

  $scope.onSelectMatterItem = function (id, name, reselectId, data) {
    $scope.matterSelectItem = data;
    $scope.onSelectedItem ? $scope.onSelectedItem(id, name, reselectId, data) : null;
  };

  $scope.onExportMattersToExcel= function () {
      $scope.exportToExcelInProgress = true;
      $scope.onExportToExcel= { fileName: 'Matters.xlsx', includeFlatData: true };
  };

  $scope.exportToExcelCompleted = function(notFullRequest:boolean) {
    if(notFullRequest) {
      dialogs.notify('Note','Excel file do not contain all data.<br/>Excel file created but records count limit reached.');
    }
    $scope.exportToExcelInProgress = false;
  };

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
})
.controller('MattersTreeCtrl', function ($scope,$compile,$state,$window,$location,Logger,$timeout,$stateParams,$element,userSettings,
                                         splitViewBuilderHelper:ui.ISplitViewBuilderHelper,propertiesUtils,configuration:IConfig,eventCommunicator:IEventCommunicator,routerChangeService,
                                         $templateCache,chartResource:IChartsResource,saveFilesBuilder:ISaveFileBuilder,dialogs) {
  var log:ILog = Logger.getInstance('MattersTreeCtrl');

  $scope.searchText = {};
  $scope.elementName='matters-tree';
  $scope.entityDisplayType = EntityDisplayTypes.matters;
  var gridOptions:ui.IGridHelper ;

  $scope.Init = function(restrictedEntityDisplayTypeName) {
    $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;
    gridOptions  = GridBehaviorHelper.createTreeList<Operations.DtoRootFolder>($scope,log,configuration,eventCommunicator,userSettings,null,
        fields,parseData,rowTemplate,$stateParams,null,getDataUrlBuilder,true,null);

    gridOptions.gridSettings.change = handleSelectionChange;
    GridBehaviorHelper.connect($scope,$timeout,$window,log,configuration,gridOptions,rowTemplate,fields,onSelectedItemChanged,$element,false,$scope.paddingBottom,
      false,$scope.showAllMatters ? createAdditionalExcelSheets : null);
  };

  var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,fullyContained,collapsedData) {
    var url;
    if($scope.showAllMatters) {
      url=  configuration.matters_list_url;
    }
    else {
      url = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType,configuration,includeSubEntityData,fullyContained,collapsedData);
    }
    return url;
  };

  $scope.updateDataItems = function(saveNew, changedMatters) {
    let treelist = $element.data("kendoTreeList");
    let dataItems = treelist.dataItems();
    let changes = {};

    for(var i=0; i<dataItems.length; i++) {
      if(_.has(changes, dataItems[i].id)) {
        _.forEach(changes[dataItems[i].id], (val, key) => {
          dataItems[i][key] = val;
        })
      }
    }
    $scope.reloadDataToGrid();
  };

  var pathToExpandAndSelect;
  var handleSelectionChange = function(e) {
    var treelist = $element.data("kendoTreeList");
    var selectedRows =   treelist.select();
    var selectedDataItems = [];
    for (var i = 0; i < selectedRows.length; i++) {
      var dataItem = treelist.dataItem(selectedRows[i]);
      selectedDataItems.push(dataItem);
    }
    $timeout(function() {
      $scope.$applyAsync(function () {
        $scope.handleSelectionChange(selectedDataItems);
      });
    });
  };

  var parseData=function(data:Entities.DTOAggregationCountItem<Entities.DTODepartment>[]) {
    data.forEach(d => {

      var dataItem:Entities.DTODepartment = (<any>d).item;
      if(dataItem && (<Entities.DTODepartment>dataItem).description == null) {
        (<Entities.DTODepartment>dataItem).description = '';
      }

      if(dataItem && dataItem.parentId) {
        (<any>d).parentId=dataItem.parentId;
      }

      (<any>d).styleId = splitViewBuilderHelper.getDepartmentStyleId((<any>d).item?(<any>d).item:d);
      (<any>d).depth = splitViewBuilderHelper.getDepartmentDepth((<any>d).item?(<any>d).item:d);
      (<any>d).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage( d.subtreeCount,d.subtreeCountUnfiltered);

    });

    $scope.totalElements = data ? data.length : 0;

    log.debug($scope.elementName + ' total elements ' + $scope.totalElements);
    return data;
  };

  var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODepartment> =  Entities.DTOAggregationCountItem.Empty();
  var dTOMatter:Entities.DTODepartment =  Entities.DTODepartment.Empty();
  var prefixFieldName = $scope.showAllMatters ? '' : propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

  var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName + propertiesUtils.propName(dTOMatter, dTOMatter.id),
    title: 'ID',
    type: EFieldTypes.type_number,
    isPrimaryKey:true,
    displayed: false,
    editable: false,
    nullable: true,
  };

  var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName + propertiesUtils.propName(dTOMatter, dTOMatter.description),
    title: 'Description',
    type: EFieldTypes.type_string,
    displayed: true,
    editable: false,
    nullable: true,
    width:'1px',
    template:' '
  };
  $scope.showAllMatters ? description.template=null : null;
  $scope.showAllMatters ? description.width=null : null;

  var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
    fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
    title: 'Number of direct filtered files',
    type: EFieldTypes.type_number,
    displayed: true,
    editable: false,
    width:'1px',
    nullable: false,
    template:' '
  };
  $scope.showAllMatters ? numOfFiles.template=null : null;
  $scope.showAllMatters ? numOfFiles.width=null : null;

  var filterTemplate = splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCountUnfiltered) ,propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCount),'filterPercentage');
  var numOfFilesNewTemplate = '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'==subtreeCount) {#<span class="title-forth" > All at this level </span>#}#' + '<span class="title-forth" style="white-space: nowrap;" >' +
    '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'!=subtreeCount) {# #: kendo.toString('+ numOfFiles.fieldName+',"n0") # #if('+propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2)+'>0) {# / #: kendo.toString('+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2) +',"n0") # #}# at this level #}# </span> ';
  var numOfAllFilteredFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
    fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCount),
    title: 'Number of all filtered files',
    type: EFieldTypes.type_number,
    displayed: true,
    editable: false,
    width:'300px',
    nullable: false,
    template:filterTemplate+'<div style="min-height: 12px;">'+numOfFilesNewTemplate+'</div>'
  };
  $scope.showAllMatters ? numOfAllFilteredFiles.template=null : null;

  $scope.isParentOfSelected = function(dataItem:Entities.DTODepartment) {
    if($scope.selectedItem) {
      var selected = (<Entities.DTODepartment>($scope.selectedItem.item));
      var s = selected.parentId == dataItem.id;
      return s;
    }
    return null;
  };

  var iconTemplate =  "<span  style='font-size: 18px;width:16px;padding: 2px 10px 0 8px;'><span class='tag tag-#: styleId #  opacity-depth-#: depth # "+configuration.icon_matter+"'></span></span>";
  var nameNewTemplate =    '<span ng-if="isParentOfSelected(dataItem)" class="notice" ng-class1="{\'k-state-selected-parent\':isParentOfSelected(dataItem)}"></span><span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
    '<a  class="title-primary break-text" title="Matter: #: '+ prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name)+' #">#: '+ prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name)+' #<i class="caret ng-hide"></i> </a></span>'+
    '<ul class="dropdown-menu"  > '+
    "<li><a  ng-click='filterByID(dataItem)'><span class='fa fa-filter'></span>  Filter by this matter</a></li>"+
    '  </ul></span>';
  $scope.filterByID = function(item:Entities.DTOAggregationCountItem<Entities.DTODepartment>) {
    setFilterByEntity(item.item.id,item.item.name);
  };
  var setFilterByEntity = function(gId,gName) {
    eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction,{
      action: 'doFilterByID',
      id: gId,
      entityDisplayType:$scope.entityDisplayType,
      filterName:gName,
      includeSubEntityDataLeft:true
    });
  };

  var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name),
    title: 'Name',
    type: EFieldTypes.type_string,
    displayed: true,
    editable: false,
    nullable: true,
    expandable:true,
    //width:'40%',
    ddlInside:true,
    template:'<span style="display: inline-block">'+iconTemplate+nameNewTemplate+'</span>'
  };

  var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.parentId),
    title: 'Parent ID',
    isParentId:true,
    type: EFieldTypes.type_number,
    displayed: false,
    editable: false,
    nullable: true,
  };

  var fields:ui.IHierarchicalFieldDisplayProperties[] =[];
  fields.push(id);
  fields.push(name);

  if(!$scope.showAllMatters) {
    fields.push(numOfFiles);
    fields.push(numOfAllFilteredFiles);
  }
  fields.push(description);
  fields.push(parentId);

  var rowTemplate=$scope.showAllMatters ? '' : "NA";

  var onSelectedItemChanged = function () {
    pathToExpandAndSelect = $scope.selectedItem[id.fieldName];
    if( $scope.selectedItem) {
      $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[name.fieldName], $scope.selectedItem[id.fieldName],$scope.selectedItem);
    }
  };

  var createAdditionalExcelSheets=function(successFunction) {
    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODepartment> =  Entities.DTOAggregationCountItem.Empty();
    var dTOMatter:Entities.DTODepartment =  Entities.DTODepartment.Empty();

    var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.id),
      title: 'ID',
      type: EFieldTypes.type_number,
      isPrimaryKey:true,
      displayed: true,
    };
    var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
    };
    var parentUniqueName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.parentName),
      title: 'Parent',
      isParentId:true,
      type: EFieldTypes.type_string,
      displayed: true,
    };
    var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.parentId),
      title: 'parentId',
      isParentId:true,
      type: EFieldTypes.type_number,
      displayed: true,
    };
    var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.description),
      title: 'Description',
      type: EFieldTypes.type_string,
      displayed: true,
    };

    var fieldsForCsv:ui.IHierarchicalFieldDisplayProperties[] =[];
    fieldsForCsv.push(id);
    fieldsForCsv.push(name);
    fieldsForCsv.push(description);
    fieldsForCsv.push(parentUniqueName);
    fieldsForCsv.push(parentId);

    var findMatterById = function(id:number, list:Entities.DTODepartment[]) : Entities.DTODepartment {
      for (var d in list) {
        if (list[d].id == id) {
          return list[d];
        }
      }
      return null;
    };

    var rightFilter:ui.IDisplayElementFilterData=$scope.getCurrentFilter();
    var populationJsonUrl = $scope.getCurrentUrl();
    chartResource.getChartData(populationJsonUrl,rightFilter ? rightFilter.filter : null,configuration.max_data_records_per_request,false,false,null,function (chartData:Dashboards.DtoCounterReport[], data) {
      var rows = saveFilesBuilder.preparSheetRows(fieldsForCsv, data.content);
      var flatSheet =  saveFilesBuilder.createSheet(fieldsForCsv, rows, 'Matters flat for csv');
      if(flatSheet) {
        successFunction(flatSheet);
      }
      else {
        dialogs.notify('Notification','Failed to create excel file.');
      }
    }, function () {
      dialogs.notify('Notification','Failed to create excel file.');
    });
  };

  $scope.$on("$destroy", function() {
    eventCommunicator.unregisterAllHandlers($scope);
  });
})
.controller('MattersGridCtrl', function ($scope, $state,$stateParams,$window,$element,$location,Logger,$timeout,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                         propertiesUtils, configuration:IConfig,userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs) {
  var log:ILog = Logger.getInstance('MattersGridCtrl');

  var _this = this;

  $scope.elementName = 'matters-grid';
  $scope.entityDisplayType = EntityDisplayTypes.matters_flat;

  $scope.Init = function (restrictedEntityDisplayTypeName) {
    $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;

    _this.gridOptions = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope, log, configuration, eventCommunicator,userSettings, null, fields, parseData, rowTemplate, $stateParams);

    GridBehaviorHelper.connect($scope, $timeout, $window, log, configuration, _this.gridOptions, null, fields, onSelectedItemChanged, $element);
  };

  var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODepartment> = Entities.DTOAggregationCountItem.Empty();
  var dTOMatter:Entities.DTODepartment = Entities.DTODepartment.Empty();
  var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

  var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName + propertiesUtils.propName(dTOMatter, dTOMatter.id),
    title: 'ID',
    type: EFieldTypes.type_number,
    isPrimaryKey:true,
    displayed: false,
    editable: false,
    nullable: true,
  };

  var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName + propertiesUtils.propName(dTOMatter, dTOMatter.description),
    title: 'Description',
    type: EFieldTypes.type_string,
    displayed: true,
    editable: false,
    nullable: true,
    //width:'1px',
    template:' '
  };
  $scope.showAllMatters ? description.template=null : null;
  //$scope.showAllDepartments ? description.width=null : null;

  var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
    fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
    title: 'Number of direct filtered files',
    type: EFieldTypes.type_number,
    displayed: true,
    editable: false,
    //width:'1px',
    nullable: false,
    template:' '
  };
  $scope.showAllMatters ? numOfFiles.template=null : null;
  //$scope.showAllDepartments ? numOfFiles.width=null : null;

  var filterTemplate = splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2),propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');
  var numOfFilesNewTemplate = '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'==count) {# All at this level #}#' + '<span class="title-forth" style="white-space: nowrap;" >' +
    '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'!=count) {# #: '+ numOfFiles.fieldName+' # at this level #}# </span> ';
  var numOfAllFilteredFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
    fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
    title: 'Number of all filtered files',
    type: EFieldTypes.type_number,
    displayed: true,
    editable: false,
    width:'300px',
    nullable: false,
    template:filterTemplate+'<div style="min-height: 12px;">'+numOfFilesNewTemplate+'</div>'
  };
  $scope.showAllMatters ? numOfAllFilteredFiles.template=null : null;

  $scope.isParentOfSelected = function(dataItem:Entities.DTODepartment) {
    if($scope.selectedItem) {
      var selected = (<Entities.DTODepartment>($scope.selectedItem.item));
      var s = selected.parentId == dataItem.id;
      return s;
    }
    return null;
  };

  var iconTemplate =  "<span  style='font-size: 18px;width:16px;padding: 2px 10px 0 8px;'><span class='tag tag-#: styleId #  opacity-depth-#: depth # "+configuration.icon_matter+"'></span></span>";
  var nameNewTemplate =    '<span ng-if="isParentOfSelected(dataItem)" class="notice" ng-class1="{\'k-state-selected-parent\':isParentOfSelected(dataItem)}"></span><span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
    '<a  class="title-primary break-text" title="Matter: #: '+ prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name)+' #">#: '+ prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name)+' #<i class="caret ng-hide"></i> </a></span>'+
    '<ul class="dropdown-menu"  > '+
    "<li><a  ng-click='filterByID(dataItem)'><span class='fa fa-filter'></span>  Filter by this matter</a></li>"+
    '  </ul><div class="title-third">#: '+ prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.fullName)+' #</div></span>';
  $scope.filterByID = function(item:Entities.DTOAggregationCountItem<Entities.DTODepartment>) {
    setFilterByEntity(item.item.id,item.item.name);
  };
  var setFilterByEntity = function(gId,gName) {
    eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction,{
      action: 'doFilterByID',
      id: gId,
      entityDisplayType:$scope.entityDisplayType,
      filterName:gName,
      includeSubEntityDataLeft:false
    });
  };

  var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: prefixFieldName+propertiesUtils.propName(dTOMatter, dTOMatter.name),
    title: 'Name',
    type: EFieldTypes.type_string,
    displayed: true,
    editable: false,
    nullable: true,
    expandable:true,
    //width:'40%',
    ddlInside:true,
    template:'<span style="display: inline-block">'+iconTemplate+nameNewTemplate+'</span>'
  };

  var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.parentId),
    title: 'Parent ID',
    isParentId:true,
    type: EFieldTypes.type_number,
    displayed: false,
    editable: false,
    nullable: true,
  };

  var fullName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
    fieldName: propertiesUtils.propName(dTOMatter, dTOMatter.fullName),
    title: 'Full Name',
    type: EFieldTypes.type_string,
    displayed: false,
    editable: false,
    nullable: true,
  };

  var fields:ui.IHierarchicalFieldDisplayProperties[] =[];
  fields.push(id);
  fields.push(name);

  if(!$scope.showAllMatters) {
    fields.push(numOfFiles);
    fields.push(numOfAllFilteredFiles);
  }
  fields.push(description);
  fields.push(parentId);
  fields.push(fullName);

  var visibleFileds = fields.filter(f=>f.displayed);
  var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
    "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'>" + iconTemplate +
    "</td>"+
    "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
    nameNewTemplate+"<div></div></td>" +
    '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
    "</tr>";

  var parseData=function(data:Entities.DTODepartment[]) {
    data.forEach(d => {
      if(d.description == null) {
        d.description = '';
      }

      var dataItem:Entities.DTODepartment = (<any>d).item;
      if(dataItem && (<Entities.DTODepartment>dataItem).description == null) {
        (<Entities.DTODepartment>dataItem).description = '';
      }

      if(dataItem && dataItem.parentId) {
        (<any>d).parentId=dataItem.parentId;
      }

      (<any>d).styleId = splitViewBuilderHelper.getDepartmentStyleId((<any>d).item?(<any>d).item:d);
      (<any>d).depth = splitViewBuilderHelper.getDepartmentDepth((<any>d).item?(<any>d).item:d);
      (<any>d).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage((<any>d).count , (<any>d).count2 );
    });

    data.sort( propertiesUtils.sortAlphbeticFun('name'));
    $scope.totalElements = data ? data.length : 0;

    log.debug($scope.elementName + ' total elements ' + $scope.totalElements);
    return data;
  };

  var onSelectedItemChanged = function () {
    $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[name.fieldName],$scope.selectedItem[id.fieldName]);
  };
})

.directive('mattersTreeList', function(){
  return {
    restrict: 'EA',
    template: '<div  class="matters-tree fill-height {{elementName}}" kendo-tree-list ng-transclude k-on-change1="handleSelectionChange(data, dataItem, columns)" ' +
    'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainTreeListOptions"></div>',
    replace: true,
    transclude:true,
    scope: {
      lazyFirstLoad: '=',
      restrictedEntityId:'=',
      itemSelected: '=',
      itemsSelected: '=',
      unselectAll: '=',
      filterData: '=',
      sortData: '=',
      exportToPdf: '=',
      exportToExcel: '=',
      templateView: '=',
      reloadDataToGrid:'=',
      refreshData:'=',
      pageChanged:'=',
      changePage:'=',
      processExportFinished:'=',
      getCurrentUrl: '=',
      getCurrentFilter: '=',
      includeSubEntityData: '=',
      showAllMatters: '=',
      totalElements: '=',
      changeSelectedItem: '=',
      findId: '=',
      paddingBottom: '=',
      isMngMatterConfigUser: '=',
      initialLoadParams: '=',
      updateDataItems: '='
    },
    controller: 'MattersTreeCtrl',
    link: function (scope:any, element, attrs) {
      scope.Init(attrs.restrictedentitydisplaytypename);
    }
  };
})
.directive('mattersGrid', function($timeout){
  return {
    restrict: 'EA',
    template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
    replace: true,
    transclude:true,
    scope: {
        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        initialLoadParams: '=',
        findId: '=',
      },
    controller: 'MattersGridCtrl',
    link: function (scope:any, element, attrs) {
      scope.Init(attrs.restrictedentitydisplaytypename);
    }

  }

})
.directive('manageMatters', function(){
  return {
    restrict: 'EA',
    templateUrl: '/app/matters/manageMatters.tpl.html',
    replace: true,
    scope: {
      matterSelectItem: '=',
      editMode: '=',
      changeMatterSelectedItemId: '=',
      onSelectedItem: '=',
    },
    controller: 'ManageMattersCtrl',
    link: function (scope:any, element, attrs) {
      scope.init();
    }
  };
});

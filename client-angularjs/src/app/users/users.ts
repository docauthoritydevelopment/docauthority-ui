///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.grid',[

    ])
    .controller('usersGridCtrl', function ($scope, $sce, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('usersGridCtrl');


      $scope.elementName = 'users-grid';

      var createGrid = function() {
        let gridOptions:ui.IGridHelper =  GridBehaviorHelper.createGrid<Users.DTOUser>($scope, log,configuration, eventCommunicator,userSettings,  null,
          fields, parseData,  rowTemplate,
          $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);
      }

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        let needToDrawGrid:boolean = false;

        if (currentUserDetailsProvider.getUserData() == null) {
          needToDrawGrid = true;
        }
        else {
          createGrid();
          setUserRoles();
        }

        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
          if (needToDrawGrid) {
            createGrid();
          }
        });
      };

      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUsers,Users.ESystemRoleName.MngUserRoles]);
      }

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
          return configuration.user_list_url;
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dTOUser:Users.DTOUser =  Users.DTOUser.Empty();
      var dTOBizRole:Users.DtoBizRole =  Users.DtoBizRole.Empty();
      var dtoSystemRole:Users.DtoSystemRole =  Users.DtoSystemRole.Empty();

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:' #: '+propertiesUtils.propName(dTOUser, dTOUser.name)+'# ',
      };
      var lock:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.accountNonLocked),
        title: 'Lock',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px',
        template:' #: '+propertiesUtils.propName(dTOUser, dTOUser.accountNonLocked)+'# ',
      };

      var userType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.userType),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span translate="{{\'Users_\'+'+'dataItem.'+propertiesUtils.propName(dTOUser, dTOUser.userType)+'}}"></span>'
      };

      var bizRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'bizRolesAsFormattedString',
        title: 'Operational Roles',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true
      };

      var systemRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'systemRolesAsFormattedString',
        title: 'System roles',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px'
      };

      var functionalRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'functionalRolesAsFormattedString',
        title: 'Data Roles',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px'
      };

      var userState:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.userState),
        title: 'State',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px',
      //  template:'<span translate="{{\'Users_state_\'+'+'dataItem.'+propertiesUtils.propName(dTOUser, dTOUser.userState)+'}}"></span>'
        template:'<span translate="{{\'Users_state_\'+'+'dataItem.'+propertiesUtils.propName(dTOUser, dTOUser.userState)+'}}"></span>'+
        '&nbsp;<span ng-show="dataItem.isActive" class="fa fa-check primary" title="User is active"></span>'

      };
      var username:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.username),
        title: 'username',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'<span class="title-secondary"> #: '+propertiesUtils.propName(dTOUser, dTOUser.username)+'# </span>'
      };

      var usernameTemplate =  '<span ng-if="isMngRolesUser"  class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<a class="title-primary">' +
          '<span title="{{dataItem.'+ username.fieldName+'}}" ng-class="{\'label-running\':dataItem.isRunning}" >#= '+username.fieldName+' #</span> ' +
          '<span ng-show="dataItem.isCurrentUser" >(YOU)</span>'+
          '<span ng-if="!dataItem.accountNonLocked" style="padding-left: 10px;padding-right: 5px;"><span class="fa fa-lock" ></span></span>'+
          '<i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          '<li ng-if="!dataItem.ldapUser"><a ng-click="openEditDialog(dataItem,\'general\')"><span class="fa fa-pencil"></span> Edit</a></li>'+
          '<li ng-if="!dataItem.ldapUser && !dataItem.isCurrentUser"><a  ng-click="openResetPasswordDialog(dataItem)">Reset password</a></li>'+
          '<li ng-if="!dataItem.ldapUser && dataItem.isCurrentUser"><a  ng-click="openCurrentUserChangePasswordDialog(dataItem)">Change password</a></li>'+
          '<li ng-if="!dataItem.accountNonLocked"><a ng-click="unlockUser(dataItem)"><span class="fa fa-unlock"></span> Unlock user</a></li>'+
          ' </ul></span> ';

      usernameTemplate=usernameTemplate+'<span ng-if="!isMngRolesUser" class="title-primary "><span title="{{dataItem.'+ username.fieldName+'}}" >#= '+username.fieldName+' #</span>' +
        '<span ng-show="dataItem.isCurrentUser" >&nbsp;(YOU)</span></span>';

      var email:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.email),
        title: 'E-mail',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'#: '+propertiesUtils.propName(dTOUser, dTOUser.email)+'# '

      };
      var passwordMustBeChanged:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.passwordMustBeChanged),
        title: 'Must change Password',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: true,
        width:'80px',
        template:'<span ng-show="{{dataItem.'+propertiesUtils.propName(dTOUser, dTOUser.passwordMustBeChanged)+'}}"><span class="fa fa-plus"></span></span>'


      };
      var ldapUser:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOUser, dTOUser.ldapUser),
        title: 'ldapUser',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: true,
        width:'80px',

      };


      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(lock);
      fields.push(username);
      fields.push(bizRoles);
   //   fields.push(systemRoles);
      fields.push(functionalRoles);
      fields.push(userState);
      fields.push(userType);
      fields.push(email);
      fields.push(passwordMustBeChanged);
      fields.push(ldapUser);

      $scope.fields =fields;

      var userActiveTemplate =splitViewBuilderHelper.getToggleButtonTemplate('isActive','User',"toggleUserState(dataItem)",'isCurrentUser');
      var emailTemplate =
          '<div class="centerText">' +
            '<div class="title-third"><span>E-Mail</span></div>' +
            '<div class="title-secondary" style="white-space:nowrap;overflow:hidden;text-overflow: ellipsis" title="{{dataItem.' + propertiesUtils.propName(dTOUser, dTOUser.email) +'}}">{{dataItem.' + propertiesUtils.propName(dTOUser, dTOUser.email) + '}}</div>' +
          '</div>';
      var bizRolesTemplate ='<div style="padding-bottom:3px; padding-top:3px; font-size:12px" ng-bind-html="buildRoleTemplate(dataItem,true)"></div>';
      var funcRolesTemplate ='<div style="font-size:12px" ng-bind-html="buildRoleTemplate(dataItem,false)"></div>';
      var visibleFileds = fields.filter(f=>f.displayed);
      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
            "<td colspan='1' class='icon-column' style='padding-top: 1px;'>" +
              "<span ng-if='!dataItem.ldapUser' ng-class='{disabled:!dataItem.isActive}' class='"+configuration.icon_user+" '></span>" +
              "<span ng-if='dataItem.ldapUser' ng-class='{disabled:!dataItem.isActive}' class='"+configuration.icon_ldap_user+" '></span>" +
            "</td>"+
            "<td colspan='1'    width='180px' class='ddl-cell break-text'>"+ usernameTemplate+"<div class='title-third'>"+name.template+"</div></td>"  +
            "<td colspan='1' width='220px'>"+emailTemplate+"</td>"+
            "<td colspan='1' width='80px'>"+userActiveTemplate+"</td>"+
            "<td colspan='"+(visibleFileds.length-2)+"' width='50%'>" +
               `<div class='title-primary' ng-click="openEditDialog(dataItem,'roles')">${bizRolesTemplate}</div>` +
               `<div class='title-primary' ng-click="openEditDialog(dataItem,'functionalRoles')">${funcRolesTemplate}</div>`+
            "</td>"+
          "</tr>";


      var parseData = function (users: Users.DTOUser[]) {
        if(users)
        {
          users.forEach(user=> {
            parseBizRoles(user);
            parseUserFunctionalRoles(user);
            (<any>user).isActive = user.userState == Users.EUserState.ACTIVE;
            (<any>user).isCurrentUser = user.username ==  currentUserDetailsProvider.getUserData().username;
          });
        }
        return users;
      }

      var parseBizRoles = function (rolableItem:Users.DTOUser) {
        if(rolableItem.bizRoleDtos) {
          rolableItem.bizRoleDtos.sort(propertiesUtils.sortAlphbeticFun('name'));


          rolableItem.bizRoleDtos.forEach(r=> {
            if(!r.description) r.description='';
          });



          var tagList = rolableItem.bizRoleDtos ? rolableItem.bizRoleDtos.map(t => t.name).join('; ') : '';
          (<any>rolableItem).bizRolesAsFormattedString = tagList;
          return rolableItem;
        }
        return null;
      };


      var parseUserFunctionalRoles = function (rolableItem:Users.DTOUser) {
        if(rolableItem.funcRoleDtos) {
          rolableItem.funcRoleDtos.sort(propertiesUtils.sortAlphbeticFun('name'));

            rolableItem.funcRoleDtos.forEach(r=> {
              if (!r.description) r.description = '';
            });

            var tagList = rolableItem.funcRoleDtos ? rolableItem.funcRoleDtos.map(t => t.name).join('; ') : '';
            (<any>rolableItem).functionalRolesAsFormattedString = tagList;
            return rolableItem;
          }

        return null;
      };
      $scope.buildRoleTemplate = function(user:Users.DTOUser, isBizRoles:boolean) {
        let tmp = '', roleClass, rolsDtos;

        if(isBizRoles) {
          roleClass = configuration.icon_bizRole;
          rolsDtos = user.bizRoleDtos
        }else {
          roleClass = configuration.icon_functionalRole+'-o';
          rolsDtos = user.funcRoleDtos
        }

        _.each(rolsDtos, function(role:Users.DtoBizRole, index, arr){
          let roleTitle = '';
          if(_.has(role, "template")) {
            roleTitle = '<div class=\'user-template-role-item-tooltip\'><i class=\'fa fa-building-o\'></i><span class=\'name\'>' + role.template.displayName + '</span></div>'
          }

          var roleStyle = isBizRoles?'':`tag-${splitViewBuilderHelper.getFunctionalRoleStyleId(role.id)}`;
          var clickableRole:boolean =   !user.ldapUser;

          tmp += `<span class="grid-list-item"><${clickableRole?'a':'span'}   class='sys-roles-item' bubble="${roleTitle}"><span class="role-icon ${roleClass} ${roleStyle}"></span>${role.name}</${clickableRole?'a':'span'}></span>`

        });

        return $sce.trustAsHtml(tmp);
      };

      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:Users.DTOUser,openWithFieldName?:string) {
        if($scope.isMngRolesUser && !dataItem.ldapUser) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridUsersUserAction, {
            action: 'openEditItemDialog',
            item: dataItem,
            openWithFieldName: openWithFieldName
          });
        }
      };
      $scope.openResetPasswordDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridUsersUserAction, {
          action: 'openResetPasswordItemDialog',
          item: dataItem,
        });
      };
      $scope.openCurrentUserChangePasswordDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridUsersUserAction, {
          action: 'openCurrentUserChangePasswordDialog',
          item: dataItem,
        });
      };

      $scope.unlockUser = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridUsersUserAction, {
          action: 'unlockUser',
          item: dataItem,
        });
      };

      $scope.toggleUserState = function(dataItem:Users.DTOUser) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridUsersUserAction, {
          action: 'toggleUserState',
          item: dataItem,

        });
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('usersGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',

          fixHeight: '@',

        },
        controller: 'usersGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

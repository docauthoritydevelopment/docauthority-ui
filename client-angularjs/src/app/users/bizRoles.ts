///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.bizRoles.grid',[

    ])
    .controller('bizRolesGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                           propertiesUtils, userBizRolesResource:IUserBizRolesResource, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('bizRolesGridCtrl');

      $scope.elementName = 'biz-roles-grid';
      var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        gridOptions  = GridBehaviorHelper.createGrid<Users.DtoBizRoleSummaryInfo>($scope, log,configuration, eventCommunicator,userSettings,  null,
          fields, parseData,  rowTemplate,
          $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }


      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
          return configuration.bizRole_summary_list_url;
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dtoBizRoleSummaryInfo:Users.DtoBizRoleSummaryInfo =  Users.DtoBizRoleSummaryInfo.Empty();
      var dTOBizRole:Users.DtoBizRole =  Users.DtoBizRole.Empty();
      // var dtoSystemRole:Users.DtoSystemRole =  Users.DtoSystemRole.Empty();
      var dtoTemplateRole:Users.DtoTemplateRole =  Users.DtoTemplateRole.Empty();
      var prefixBizRoleFieldName = propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.bizRoleDto) + '.';
      var prefixBizRoleTemplateFieldName = prefixBizRoleFieldName + propertiesUtils.propName(dTOBizRole, dTOBizRole.template) + '.';

      var dtoLdapGroup:Users.DtoLdapGroup =  Users.DtoLdapGroup.Empty();
      var prefixLdapGroupFieldName = propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos) + '.';

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:prefixBizRoleFieldName+ propertiesUtils.propName(dTOBizRole, dTOBizRole.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:prefixBizRoleFieldName+ propertiesUtils.propName(dTOBizRole, dTOBizRole.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
        '</a><span class="fa fa-user icon-column "></span><span class="title-primary"> #: '+prefixBizRoleFieldName+propertiesUtils.propName(dTOBizRole, dTOBizRole.name)+'# </span>',
      };
      var nameTemplate =  '<span class="dropdown dropdown-wrapper" ng-if="isMngRolesUser"><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<a class="title-primary">' +
          '<span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span> ' +
          '<i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          '<li><a   ng-click="openEditDialog(dataItem)"><span class="fa fa-pencil"></span> Edit</a></li>'+
          //'<li ><a   ng-click="openWhoIsAssignedDialog(dataItem)">Who is assigned?</a></li>'+

          ' </ul></span> ';

      nameTemplate=nameTemplate+'<span ng-if="!isMngRolesUser" class="title-primary "><span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span></span>';

      var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixBizRoleFieldName+propertiesUtils.propName(dTOBizRole, dTOBizRole.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span> #:'+prefixBizRoleFieldName+propertiesUtils.propName(dTOBizRole, dTOBizRole.description)+'# </span>'
      };
      var ldapGroupName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:'ldapGroupsAsFormattedString',
        title: 'LDAP group',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template: ' #if ( '+ propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos)+'&& '
        +propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos)+'[0])  {#' +
        '<div class="title-third title-main">' + 'Ldap group:' + '</div>' +
        '<div class="title-secondary">' +
        '	# for (var i=0; i < '+ propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos)+'.length; i++) { # ' +

        '<span><a ng-click="openEditDialog(dataItem,\'ldapGroup\')" title="Ldap group name #: '+propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos)+'[i].'+ propertiesUtils.propName(dtoLdapGroup, dtoLdapGroup.groupName)+' # "> #: '
        + propertiesUtils.propName(dtoBizRoleSummaryInfo, dtoBizRoleSummaryInfo.ldapGroupMappingDtos)+'[i].'+ propertiesUtils.propName(dtoLdapGroup, dtoLdapGroup.groupName)+' # </a></span>' +

        '		# } #' +
        ' </div>#}#'
      };

      var templateRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixBizRoleTemplateFieldName + propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.displayName),
        title: 'Role template',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px',
        template:
        '<div class="title-third title-main">&nbsp; &nbsp;Role template:</div>' +
        '<div class="title-secondary ellipsis">' +
          '<span class="grid-list-item">' +
          '<a ng-click="openEditDialog(dataItem,\'templateRoles\')" title="{{dataItem.systemRolesTooltip}}"> #: '+ prefixBizRoleTemplateFieldName + propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.displayName)+ '# </a>' +
          '</span>' +
        '</div>'
     };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(description);
      fields.push(ldapGroupName);
      fields.push(templateRoles);

      $scope.fields =fields;

      var rolesTemplate = '<div class="title-secondary"  >'+templateRoles.template+'</div>';
      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='4'>" +
          "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span title='{{dataItem."+name.fieldName+"|translate}}' class='"+configuration.icon_bizRole+" user-#: "+name.fieldName+"#'></span></td>"+
          "<td colspan='1'   width='180px' class='ddl-cell break-text'>"+  nameTemplate+ "<div class='title-third'>"+description.template+"</div></td>"  +
          "<td colspan='1'  width='180px' class='break-text'>"+rolesTemplate+"</td>"+
          "<td colspan='1'   width='40%' class='ddl-cell break-text'>"+  ldapGroupName.template + "</td>"  +

          "</tr>";

      var parseData = function (bizRoles: Users.DtoBizRoleSummaryInfo[]) {

        bizRoles.forEach(role=> {
          let systemRolesTooltip = buildSystemRolesTooltip(role.bizRoleDto);
          if (systemRolesTooltip) {
            (<any>role).systemRolesTooltip = systemRolesTooltip;
          }
          (<any>role).id = role.bizRoleDto.id;
          (<any>role).bizRoleDto.description = role.bizRoleDto.description || role.bizRoleDto.displayName;
          (<any>role).ldapGroupsAsFormattedString = role.ldapGroupMappingDtos.map(m => m.groupName).join(";");
        });
        return bizRoles;
      };

      var buildSystemRolesTooltip = function (bizRole:Users.ITemplateRoleContainer) {
        let systemRolesTooltip = '';

        if(bizRole.template.systemRoleDtos) {
          bizRole.template.systemRoleDtos.sort( propertiesUtils.sortAlphbeticFun('name'));
          bizRole.template.systemRoleDtos.forEach(r=> {
            systemRolesTooltip += `<div class="sys-roles-tooltip-item"><i class='fa fa-wrench icon'></i><span class='name'>${r.displayName}</span></div>`
          });
          return  `<div class="sys-roles-tooltip">${systemRolesTooltip}</div>`;
        }
        return null;
      };

      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:any,openWithFieldName?:string) {
        if($scope.isMngRolesUser) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridBizRolesUserAction, {
            action: 'openEditItemDialog',
            item: dataItem,
            openWithFieldName: openWithFieldName
          });
        }
      };
      $scope.openWhoIsAssignedDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridBizRolesUserAction, {
          action: 'whoIsAssigned',
          item: dataItem,
        });
      };

     $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('bizRolesGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',
          initialLoadParams: '=',
          fixHeight: '@',

        },
        controller: 'bizRolesGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

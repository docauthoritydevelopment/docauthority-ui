///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.users.manage.menu', [
      'resources.users',
      'directives.dialogs.users.items',

    ])
    .controller('usersMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,usersResource:IUsersResource,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                    userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs) {
      var log:ILog = Logger.getInstance('usersMenuCtrl');

      $scope.init = function () {

      };

      $scope.$watch('selectedListItemsToOperate', function () {
        $scope.isSelectedUserAdmin = _.isEmpty(_.filter($scope.selectedListItemsToOperate, function(selectedItem:Users.DTOUser){
             return selectedItem.roleName !== 'admin'
         }));
        $scope.ldapUsers = _.filter($scope.selectedListItemsToOperate, function(selectedItem:Users.DTOUser){
          return selectedItem.ldapUser
        });
        $scope.selectedItemIsLdapUser =$scope.selectedListItemsToOperate&&$scope.selectedListItemsToOperate[0]?(<Users.DTOUser>$scope.selectedListItemsToOperate[0]).ldapUser:null;
      }, true);

      $scope.openNewItemDialog = function(listId:number)
      {
        var dlg = dialogs.create(
            '/app/users/newUserDialog.tpl.html',
            'newUserItemDialogCtrl',
            {},
            {size:'md'});

        dlg.result.then(function(newItem:Users.DTOUser){
          addNewItem(newItem);
        },function(){

        });
      }

      $scope.openEditSelectedItemsDialog = function(userToEdit?:Users.DTOUser,isCurrentUser?:boolean,openWithFieldName?:string)
      {

        var item:Users.DTOUser=userToEdit?userToEdit: <Users.DTOUser>$scope.selectedListItemsToOperate[0];
        if(item.ldapUser)
        {
          return;
        }
        var dlg = dialogs.create(
            '/app/users/newUserDialog.tpl.html',
            'newUserItemDialogCtrl',
            {userItem:item,profileMode:isCurrentUser,openWithFieldName:openWithFieldName},
            {size:'md'});

        dlg.result.then(function(item:Users.DTOUser){
          if(isCurrentUser)
          {
            editCurrentUser(item);
          }
          else {
            editItem(item);
          }

        },function(){

        });
      }
      $scope.openChangePasswordDialog = function(user:Users.DTOUser)
      {
        var dlg = dialogs.create(
            '/app/users/changePasswordDialog.tpl.html',
            'changePasswordDialogCtrl',
            {userItem:user},
            {size:'md'});

        dlg.result.then(function(passwordData){
          changePassword(passwordData.oldPwd,passwordData.newPwd);
        },function(){

        });
      }
      $scope.openResetPasswordDialog = function(listItem)
      {
        var item:Users.DTOUser=listItem?listItem: $scope.selectedListItemsToOperate[0];
        var dlg = dialogs.create(
            '/app/users/newUserDialog.tpl.html',
            'newUserItemDialogCtrl',
            {userItem:item,resetPasswordMode:true,openWithFieldName:'password'},
            {size:'md'});

        dlg.result.then(function(item:Users.DTOUser){
          resetPassword(item);
        },function(){

        });
      }

      $scope.openDeleteSelectedItems = function()
      {
        var items:Users.DTOUser[]=$scope.selectedListItemsToOperate;
        if(items&& items.length>0) {
          var userMsg;
          if(items.length==1)
          {
            userMsg="You are about to delete user named: '"+items[0].username+"'. Continue?";
          }
          else if(items.length<6){
            userMsg="You are about to delete " + items.length + " users. Continue?"+'<br>'+items.map(u=>u.username).join(', ');
          }
          else {
            userMsg="You are about to delete " + items.length + " users. Continue?";
          }
          var dlg = dialogs.confirm('Confirmation', userMsg, null);
          dlg.result.then(function (btn) { //user confirmed deletion
            items.forEach(d=>  deleteItem(d));

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }

      }

      var changePassword = function (oldPassword,newPassword) {

        usersResource.updateCurrentUserPassword( oldPassword,newPassword, function (updatedPassword) {
          $scope.password = updatedPassword;
          var dlg = dialogs.notify('Note', 'Password change successfully.');
        }, function(e) {
          if (e.status == 400 && e.data.type==ERequestErrorCode.UNAUTHORIZED) {
            var dlg = dialogs.notify('Note', 'Password not changed.<br/><br/>Wrong credentials.');
            dlg.result.then(function(){
              $scope.openChangePasswordDialog();
            },function(){

            });

          }
          else {
            dialogs.error('Error', 'Failed to change password.');
          }
        });
      }
      var deleteItem = function(item:Users.DTOUser)
      {
        log.debug('delete user: ' + item.name);
        usersResource.deleteUser(item.id, function () {
          $scope.reloadData();
        }, function(e) {
          if (e.status == 400 ) {
            dialogs.error('Error', 'Cannot delete user.');
          }
          else {
            dialogs.error('Error', 'Failed to delete user.');
          }
        });
      }


      var addNewItem = function (newItem:Users.DTOUser) {
        if ( newItem.name &&  newItem.name.trim() != '') {
          log.debug('add new user to list  name: ' +  newItem.name);
          usersResource.addNewUser( newItem, function (addedItem) {
            $scope.reloadData(addedItem.id,addedItem.id);
          },  function(e) {
            if (e.status == 400 ) {
              if(e.data.type =='exists')
              {
                dialogs.error('Error', 'User already exists');
              }
              else {
                dialogs.error('Error', 'Cannot create user.');
              }
            }
            else {
              dialogs.error('Error', 'Failed to create user.');
            }
          });
        }
      }
      var editCurrentUser = function (item:Users.DTOUser) {

        item.name =  item.name?( item.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( item.name &&  item.name.trim() != '') {
          log.debug('edit user to list  name: ' +  item.name);
          usersResource.updateCurrentUserDetails( item, function (updatedUser:Users.DTOUser) {
            $scope.reloadData(updatedUser.id,updatedUser.id,updatedUser);
          }, onError);
        }
      }

      var editItem = function (item:Users.DTOUser) {
        if ( item.name &&  item.name.trim() != '') {
          log.debug('edit user to list  name: ' +  item.name);
          usersResource.updateUser( item, function () {
            $scope.reloadData(item.id,item.id);
          },  function(e) {
            if (e.status == 400 ) {
              dialogs.error('Error', 'Cannot edit user.');
            }
            else {
              dialogs.error('Error', 'Failed to edit user.');
            }
          });
        }
      }

      var resetPassword = function (item:Users.DTOUser) {
        if ( item.name &&  item.name.trim() != '') {
          log.debug('Reset user password.  name: ' +  item.name);
          usersResource.updateUserPassword( item.id,item.password, function () {
            $scope.reloadData(item.id,item.id);
          },  function(e) {
            if (e.status == 400 ) {
              dialogs.error('Error', 'Cannot edit user pwd.');
            }
            else {
              dialogs.error('Error', 'Failed to edit user pwd.');
            }
          });
        }
      }
      $scope.toggleActivateSelectedItems = function()
      {
        if( $scope.firstSelectedListItemToOperate) {
          $scope.activateSelectedItems($scope.toggleToActivateState);

        }
      }
      $scope.$watch(function () { return $scope.firstSelectedListItemToOperate; }, function (value) {
        //rebing grid
        if( $scope.firstSelectedListItemToOperate)
        {
          updateActiveToggleState();
        };
      });
      var updateActiveToggleState=function()
      {

          $scope.toggleToActivateState = nextActiveToggleState((<Users.DTOUser>$scope.firstSelectedListItemToOperate).userState);

      }

      var nextActiveToggleState=function(userState:Users.EUserState)
      {
        if(userState == Users.EUserState.ACTIVE) {
         return Users.EUserState.DISABLED;
        }
        else {
          return Users.EUserState.ACTIVE;
        }
      }

      var updateActiveState = function (user:Users.DTOUser) {
        var isCurrentUser =  currentUserDetailsProvider.getUserData().username.toLowerCase()==user.username.toLowerCase();
        if(isCurrentUser)
        {
          dialogs.notify('Note','Not possible to deactivate the logged-in user.');
        }
        else {
          var newState = nextActiveToggleState(user.userState);
          var dlg = dialogs.confirm('Confirmation', "Are you sure you want to set "+user.name+" to state " + $filter('translate')('Users_state_' + newState) + "?", null);
          dlg.result.then(function (btn) { //user confirmed deletion
            log.debug('update  Active state: ' + newState);
            var user_copy:Users.DTOUser = JSON.parse(angular.toJson(user));
            user_copy.userState = newState;
            usersResource.updateUser(user_copy, function (selectedItem:Users.DTOUser) {
              user.userState = selectedItem.userState;
              (<any>user).isActive = selectedItem.userState == Users.EUserState.ACTIVE;
              $scope.refreshDisplay();
            }, onError);
          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }
      }

      var unlockUser = function(user:Users.DTOUser){
        usersResource.unlockUser( user, function () {
          $scope.reloadData();
        },  function(e) {
          dialogs.error('Error', 'Cannot unlock user.');
        });
      }

      $scope.activateSelectedItems = function (userState:Users.EUserState) {

        if ( $scope.selectedListItemsToOperate && $scope.selectedListItemsToOperate.length > 0) {

          var loopPromises = [];
          $scope.selectedListItemsToOperate.forEach(function (selectedItem:Users.DTOUser) {
            var deferred = $q.defer();
            loopPromises.push(deferred.promise);
            selectedItem.userState = userState;
            usersResource.updateUser(selectedItem,
                function (selectedItem:Users.DTOUser) {
                  log.debug('receive DTOUser success for ' + selectedItem.id);
                  deferred.resolve(selectedItem.id);
                },
                function (error) {
                  deferred.resolve();
                  log.error('receive activateSelectedItems failure' + error);
                  onError();
                });
          });
          $q.all(loopPromises).then(function (selectedItemsIdsToUpdate) {
            updateActiveToggleState();
            $scope.reloadData(selectedItemsIdsToUpdate); //no need when auto sync is false
          });
        }
      }


      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }


      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridUsersUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

            if (fireParam.action === 'openEditItemDialog') {
              $scope.openEditSelectedItemsDialog(fireParam.item,null,fireParam.openWithFieldName);

            }
            else if (fireParam.action === 'openResetPasswordItemDialog') {
              $scope.openResetPasswordDialog(fireParam.item);

            }
            else if (fireParam.action === 'openCurrentUserChangePasswordDialog') {
              $scope.openChangePasswordDialog(fireParam.item);

            }
            else if (fireParam.action === 'toggleUserState') {
              updateActiveState(fireParam.item);
            }
            else if (fireParam.action === 'unlockUser') {
              unlockUser(fireParam.item);
            }
          })));

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportProfileUserUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

            if (fireParam.action === 'openCurrentUserChangePasswordDialog') {
              $scope.openChangePasswordDialog(fireParam.item);

            }


          })));

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('usersManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/users/manageUsersMenu.tpl.html',
            controller: 'usersMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'firstSelectedListItemToOperate':'=',
              'reloadData':'=',
              'refreshDisplay':'=',
              'gridFindId':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    )
    .directive('usersEditProfileMenu',
        function () {
          return {
            // restrict: 'E',
            template: '   <a class="btn btn-flat btn-xs" ng-class="{disabled:currentUser.ldapUser}" ng-click="openEditSelectedItemsDialog(currentUser,true)" ><i class="fa fa-pencil"> </i>&nbsp;Edit</a>',
            controller: 'usersMenuCtrl',
            replace:true,
            scope:{
              'currentUser':'=',
              'reloadData':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }
    )



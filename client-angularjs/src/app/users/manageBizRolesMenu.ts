///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.users.bizRoles.manage.menu', [
  'resources.users',
  'directives.dialogs.user.bizRole.new',
])
  .controller('userBizRolesMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,usersResource:IUsersResource,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                ldapUsersResource:ILDapUsersResource, userBizRolesResource:IUserBizRolesResource,eventCommunicator:IEventCommunicator,dialogs) {
    var log:ILog = Logger.getInstance('userBizRolesMenuCtrl');
    $scope.hideImport=true;
    $scope.init = function () {
    //  ldapUsersResource.getLDapSettings(
    //    function (settings:AppSettings.DtoLdapConnectionDetails) {
    //      isLdapSettings = settings && _.isNumber(settings.id);
    //    },
    //    function() {
    //    });
    };

    $scope.openNewItemDialog = function()
    {

      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserBizRoleDialog.tpl.html',
        'newUserBizRoleItemDialogCtrl',
        {},
        {size:'md'});

      dlg.result.then(function(addedItem:Users.DtoBizRoleSummaryInfo){
        $scope.reloadData(addedItem.bizRoleDto.id,addedItem.bizRoleDto.id);
      },function(){

      });
    }

    $scope.openEditItemsDialog = function(roleToEdit?:Users.DtoBizRoleSummaryInfo,openWithFieldName?:string)
    {
      var item:Users.DtoBizRoleSummaryInfo=roleToEdit?roleToEdit: <Users.DtoBizRoleSummaryInfo>$scope.selectedListItemsToOperate[0];
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserBizRoleDialog.tpl.html',
        'newUserBizRoleItemDialogCtrl',
        {bizRoleSummaryInfo:item, openWithFieldName:openWithFieldName},
        {size:'md'});

      dlg.result.then(function(item:Users.DtoBizRoleSummaryInfo){
        editItem(item);
      },function(){

      });
    }

    $scope.openDeleteItemsDialog = function() {
      var items:Users.DtoBizRoleSummaryInfo[]=$scope.selectedListItemsToOperate;
      if(items&& items.length>0) {
        var userMsg;
        if(items.length==1) {
          userMsg="You are about to delete role named: '"+(<Users.DtoBizRole>items[0].bizRoleDto).name+"'. Continue?";
        }
        else if(items.length<6) {
          userMsg="You are about to delete " + items.length + " roles. Continue?"+'<br>'+items.map(u=>(<Users.DtoBizRole>u.bizRoleDto).name).join(', ');
        }
        else {
          userMsg="You are about to delete " + items.length + " roles. Continue?";
        }
        var dlg = dialogs.confirm('Confirmation', userMsg, null);
        dlg.result.then(function (btn) { //user confirmed deletion
          items.forEach(d=> {
            deleteItem(d);
          });
        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });
      }
    };

    $scope.openWhoIsAssignedDialog=function(role:Users.DtoBizRole)
    {
      userBizRolesResource.getBizRoleAssignedUsers(role.id,function(users:Users.DTOUser[],totalElements)
      {
        var usersList = users&&users.length>0?users.map(u=>u.name).join(','):'';
        dialogs.notify('Who is assigned', 'Business role ' + role.name + ' is assigned to ' + totalElements + ' users.\n\n'+usersList, null);
      },onError);
    };

    var deleteItem = function(item:Users.DtoBizRoleSummaryInfo) {
      log.debug('delete user: ' + item.bizRoleDto.name);
      userBizRolesResource.deleteBizRole(item.bizRoleDto.id,function() {
        $scope.reloadData();
      }, function(e) {
        dialogs.notify('Note', e.data.message);
      });
    };


    var editItem = function (item:Users.DtoBizRoleSummaryInfo) {
      if ( item.bizRoleDto.name &&  item.bizRoleDto.name.trim() != '') {
        log.debug('edit role to list  name: ' +  item.bizRoleDto.name);
        userBizRolesResource.updateBizRole( item, function () {
          $scope.reloadData(item.bizRoleDto.id,item.bizRoleDto.id);
        },function (error) {
          if (error.status == 400 &&error.data.type =='ITEM_ALREADY_EXISTS')
          {
            dialogs.error('Error', 'Role with this name already exists. \n\nSelect another name.');
          }
          else {
            dialogs.error('Error', 'Failed to edit role.');
          }
        });
      }
    };


    var onError = function() {
      dialogs.error('Error','Sorry, an error occurred.');
    };


    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridBizRolesUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'openEditItemDialog') {
          $scope.openEditItemsDialog(fireParam.item,fireParam.openWithFieldName);

        }
        else if (fireParam.action === 'whoIsAssigned') {
          $scope.openWhoIsAssignedDialog(fireParam.item);

        }


      })));

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('bizRolesManageMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/manageMenu.tpl.html',
        controller: 'userBizRolesMenuCtrl',
        replace:true,
        scope:{
          'selectedListItemsToOperate':'=',
          'firstSelectedListItemToOperate':'=',
          'reloadData':'=',
          'refreshDisplay':'=',
          'gridFindId':'=',

        },
        link: function (scope:any, element, attrs) {
          scope.init(attrs.left);
        }
      };
    }
  )



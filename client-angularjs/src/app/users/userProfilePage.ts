///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.profile', [
  'directives.dialogs.users.updatePassword.dialog'
]);

angular.module('users')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.profile, {
            url: '/profile',
            templateUrl: '/app/users/userProfilePage.tpl.html',
            controller: 'UserProfilePageCtrl'
          })

    })
    .controller('UserProfilePageCtrl', function ($window, $scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                 Logger,propertiesUtils,$stateParams, pageTitle,bizListsResource:IBizListsResource,configuration:IConfig,
                                                 dialogs,usersResource:IUsersResource, splitViewBuilderHelper:ui.ISplitViewBuilderHelper,$sce, saveFilesBuilder:ISaveFileBuilder) {

      var log:ILog = Logger.getInstance('UserProfilePageCtrl');

      $scope.dateFormat = configuration.dateFormat;
      $scope.init = function () {
        pageTitle.set("User profile");
        initUserProfile();
      };


      var initUserProfile = function()
      {
        usersResource.getCurrentUserDetails(function(user:Users.DTOUser){
          $scope.user = user;
          $scope.bizRoles = $scope.buildRoleTemplate(true);
          $scope.funcRoles = $scope.buildRoleTemplate(false);
        },onError)
      }

      $scope.reloadUserData = function(id,name,updatedUser)
      {
        $scope.user = updatedUser;
      }

      $scope.openCurrentUserChangePasswordDialog = function() {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportProfileUserUserAction, {
          action: 'openCurrentUserChangePasswordDialog',
          item: $scope.user,
        });
      };

      $scope.getRoleIcon=function(roleType)
      {
        switch(roleType) {
          case 'biz':
            return configuration.icon_bizRole;
          case 'func':
            return configuration.icon_functionalRole;
          case 'sys':
            return configuration.icon_sysRole;
        }
      }

      $scope.getFuncRoleStyleId = function(role:Users.DtoFunctionalRole)
      {
        var styleId = splitViewBuilderHelper.getFunctionalRoleStyleId(role.id);
        return styleId;
      }

      $scope.buildRoleTemplate = function(isBizRoles:boolean) {
        let tmp = '', roleClass, rolsDtos;

        if(isBizRoles) {
          roleClass = configuration.icon_bizRole;
          rolsDtos = $scope.user.bizRoleDtos
        }else {
          roleClass = configuration.icon_functionalRole+'-o';
          rolsDtos = $scope.user.funcRoleDtos
        }

        _.each(rolsDtos, function(role:Users.DtoBizRole, index, arr){
          let roleTitle = `<div class='.profile-template-tooltip'>
            <div>
              <i class='fa fa-building-o'></i>
              <span class='name'>${role.template.displayName}</span>
            </div>          
            <div class='desc'>${role.template.description || role.template.displayName}</div>
          </div>`;

          if(!isBizRoles) {
            var styleId = splitViewBuilderHelper.getFunctionalRoleStyleId(role.id);
            tmp += '<span style="display:block; padding-bottom:7px;"><a  class=\'sys-roles-item\' bubble="' + roleTitle + '">' + '<span class="role-icon ' + roleClass + ' tag-' + styleId + '"></span><span style="padding-left:5px;">' + role.name+'</span></a></span>'
          }
          else {
            tmp += '<span style="display:block;padding-bottom:7px;"><a  class=\'sys-roles-item\' bubble="' + roleTitle + '">' + '<span class="role-icon ' + roleClass +'"></span><span style="padding-left:5px;">' + role.name+'</span></a></span>'
          }
        });

        return $sce.trustAsHtml(tmp);
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });


      var onError=function(e)
      {
        dialogs.error('Error','Sorry, an error occurred.');
      };

      function addToTxt(txtLines:any[],rows,lineTitle) {
        if (angular.isArray(rows)) {
          txtLines.push(lineTitle+':');
          for (var i = 0 ; i < rows.length ; i++) {
            txtLines.push(rows[i]);
          }
        }
        else {
          txtLines.push(lineTitle+': '+rows);
        }
      }

      $scope.exportToTextInProgress = false;
      $scope.exportToText = function() {
        $scope.exportToTextInProgress = true;
        usersResource.getCurrentUserDetailsWithTokens(function(user:Users.DTOUser){
          var txtLines = [];
          addToTxt(txtLines, user.name, "Username");
          addToTxt(txtLines, user.email, "Email");
          addToTxt(txtLines, user.roleName, "Roles");
          //add user.accessTokens to txtLines here
          addToTxt(txtLines, user.accessTokens, "Tokens");
          let txt = txtLines.join('\r\n');
          saveFilesBuilder.saveAsTxt([txt], 'Access tokens for user ' + user.name);
          $scope.exportToTextInProgress = false;
        },function(e) {
          $scope.exportToTextInProgress = false;
          onError(e);
          });
      };
    });

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.bizRoles', [
  'users.bizRoles.grid',
  'directives.users.bizRoles.manage.menu'

]);

angular.module('users.bizRoles')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.bizRolesManagement, {
            parent:'users',
            url: '/operationalRoles/manage?&selectedId?&page?',
            templateUrl: '/app/users/manageBizRolesPage.tpl.html',
            controller: 'ManageBizRolesPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngUsers,Users.ESystemRoleName.MngRoles,Users.ESystemRoleName.MngUserRoles]
            }
          })

    })
    .controller('ManageBizRolesPageCtrl', function ($window, $scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,routerChangeService,$filter,
                                                 Logger,propertiesUtils,$stateParams, pageTitle,bizListsResource:IBizListsResource,configuration:IConfig,userSettings:IUserSettingsService,
                                                 dialogs) {
      var log:ILog = Logger.getInstance('ManageBizRolesPageCtrl');

      $scope.changePage= $stateParams.page && propertiesUtils.isInteger( $stateParams.page) ? parseInt( $stateParams.page ) : null;
      $scope.changeSelectedItem=$stateParams.selectedId;



      $scope.init = function () {
        pageTitle.set("Manage Operational Roles");

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });

      };
      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }

      $scope.onBizRolesSelectItem = function (id,name,item) {
        $scope.bizRoleSelectedItemId = id;
        $scope.bizRoleSelectedItemName = name;
        $scope.bizRoleSelectedItem = item;

        $stateParams['selectedId'] = $scope.bizRoleSelectedItemId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });
      };

      $scope.pageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.onBizRoleSelectItems = function (data) {
        $scope.bizRoleSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess('Operational_roles_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'operational_roles' ,{},{},gotALotOfRecords);
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var GetError=function(e)
      {

        dialogs.error('Error','Sorry, an error occurred.');
      };


    })

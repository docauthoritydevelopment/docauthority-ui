///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.templateRoles.grid',[

    ])
    .controller('templateRolesGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                           propertiesUtils, templateRolesResource:IUserTemplateRolesResource, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('templateRolesGridCtrl');

      $scope.elementName = 'template-roles-grid';
      var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        gridOptions  = GridBehaviorHelper.createGrid<Users.DtoTemplateRole>($scope, log,configuration, eventCommunicator,userSettings,  null,
          fields, parseData,  rowTemplate,
          $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }


      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
          return configuration.templateRole_list_url.replace(":type",Users.ETemplateType.NONE.toString());
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dtoSystemRole:Users.DtoSystemRole =  Users.DtoSystemRole.Empty();
      var dtoTemplateRole:Users.DtoTemplateRole =  Users.DtoTemplateRole.Empty();
      var prefixSystemRoleFieldName = propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos) + '.';


      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
        '</a><span class="fa fa-user icon-column "></span><span class="title-primary"> #: '+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.name) +'# </span>',
      };
      var nameTemplate =  '<span class="dropdown dropdown-wrapper" ng-if="isMngRolesUser"><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<a class="title-primary">' +
          '<span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span> ' +
          '<i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          '<li><a   ng-click="openEditDialog(dataItem)"><span class="fa fa-pencil"></span> Edit</a></li>'+
          ' </ul></span> ';

      nameTemplate=nameTemplate+'<span ng-if="!isMngRolesUser" class="title-primary "><span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span></span>';

      var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span> #:'+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.description) +'# </span>'
      };

      var templateType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.templateType),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span> #:'+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.templateType) +'# </span>'
      };

      var systemRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'systemRolesAsFormattedString',
        title: 'System roles',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px',
        template: ' #if ( '+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos)+'&& '+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos)+'[0])  {#' +
        '<div class="title-third title-main">&nbsp;&nbsp;System roles:</div>' +
        '<div >' +
          '	# for (var i=0; i <= Math.min('+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos) +'.length-1,'+configuration.displayed_items_when_more_then_max+'); i++) { # ' +
          '<span class="grid-list-item"><a ng-click="openEditDialog(dataItem,\'systemRoles\')" title=" #: '+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos)+'[i].'+ propertiesUtils.propName(dtoSystemRole, dtoSystemRole.description)+' # "> #: '+ propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.systemRoleDtos)+'[i].'+ propertiesUtils.propName(dtoSystemRole, dtoSystemRole.displayName)+' # </a></span>' +
        '		# } #' +
        ' </div>#}#'
      };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(description);
      fields.push(templateType);
      fields.push(systemRoles);

      $scope.fields =fields;

      var rolesTemplate = '<div class="title-secondary"  >'+systemRoles.template+'</div>';
      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
          "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span title='{{dataItem."+name.fieldName+"|translate}}' class='"+configuration.icon_templateRole+" user-#: "+name.fieldName+"#'></span></td>"+
          "<td colspan='1'  width='180px' class='ddl-cell break-text'>"+  nameTemplate + "<div class='title-third'>"+description.template+"</div></td>"  +
          `<td colspan='1'   width='180px' class='ddl-cell'><div class='title-third title-main'>Type:</div><div class="title-secondary " >{{dataItem.translatedType}}</span></div></td>`  +
          "<td colspan='1' colspan='"+(visibleFileds.length-3)+"' width='100%' class=' break-text'>"+rolesTemplate+"</td>"+
          "</tr>";

      var parseData = function (data: any) {
        var templateRoles: Users.DtoTemplateRole[] = data;

        _.each(templateRoles, (templateRole:Users.DtoTemplateRole) => {
          templateRole.description = templateRole.description || '';

          if(templateRole.templateType) {
            if((<any>templateRole).templateType == Users.ETemplateType.NONE) {
              (<any>templateRole).translatedType = `${$filter('translate')('BIZROLE')}, ${$filter('translate')('FUNCROLE')}`;
            } else {
              (<any>templateRole).translatedType = $filter('translate')(templateRole.templateType);
            }
          }
          parseSystemRoles(templateRole);
        });
        return templateRoles;
      };

      var parseSystemRoles = function (templateRole) {
        if(templateRole.systemRoleDtos) {
          templateRole.systemRoleDtos.sort(propertiesUtils.sortAlphbeticFun('name'));

          templateRole.systemRoleDtos.forEach(r=> {
            if(!r.description) r.description='';
          });

          var tagList = templateRole.systemRoleDtos ? templateRole.systemRoleDtos.map(t => t.name).join('; ') : '';
          (<any>templateRole).systemRolesAsFormattedString = tagList;
          return templateRole;
        }
        return null;
      };


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:any,openWithFieldName?:string) {
        if($scope.isMngRolesUser) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTemplateRolesUserAction, {
            action: 'openEditItemDialog',
            item: dataItem,
            openWithFieldName: openWithFieldName
          });
        }
      };
      $scope.openWhoIsAssignedDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTemplateRolesUserAction, {
          action: 'whoIsAssigned',
          item: dataItem,
        });
      };

     $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('templateRolesGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',

          fixHeight: '@',

        },
        controller: 'templateRolesGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

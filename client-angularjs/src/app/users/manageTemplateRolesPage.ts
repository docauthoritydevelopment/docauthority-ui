///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.templateRoles', [,
  'directives.users.templateRoles.manage.menu',
  'users.templateRoles.grid'
]);

angular.module('users.templateRoles')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.templateRolesManagement, {
            parent:'users',
            url: '/templateRoles/manage?&selectedId?&page?',
            templateUrl: '/app/users/manageTemplateRolesPage.tpl.html',
            controller: 'ManageTemplateRolesPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngUsers,Users.ESystemRoleName.MngRoles,Users.ESystemRoleName.MngUserRoles]
            }
          })

    })
    .controller('ManageTemplateRolesPageCtrl', function ($window, $scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                 Logger,propertiesUtils,$stateParams, pageTitle, templateRolesResource:IUserTemplateRolesResource,configuration:IConfig,userSettings:IUserSettingsService,routerChangeService,$filter,
                                                 dialogs) {
      var log:ILog = Logger.getInstance('ManageTemplateRolesPageCtrl');

      $scope.changePage= $stateParams.page && propertiesUtils.isInteger( $stateParams.page) ? parseInt( $stateParams.page ) : null;
      $scope.changeSelectedItem=$stateParams.selectedId;



      $scope.init = function () {
        pageTitle.set("Manage Role templates");


        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });


      };
      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }

      $scope.onTemplateRoleSelectItems = function (data) {
        $scope.templateRoleSelectedItems = data;
      };

      $scope.onTemplateRolesSelectItem = function (id,name,item) {
        $scope.templateRoleSelectedItemId = id;
        $scope.templateRoleSelectedItemName = name;
        $scope.templateRoleSelectedItem = item;

        $stateParams['selectedId'] = $scope.templateRoleSelectedItemId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });
      };

      $scope.pageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess('Role_templates_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'role_templates' ,{},{},gotALotOfRecords);
      };



      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var GetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };

    })

///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.users.templateRoles.manage.menu', [
  'resources.users',
  'directives.dialogs.user.templateRole.new',
])
  .controller('userTemplateRolesMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,usersResource:IUsersResource,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                       ldapUsersResource:ILDapUsersResource, userBizRolesResource:IUserBizRolesResource,eventCommunicator:IEventCommunicator,
                                                       templateRolesResource:IUserTemplateRolesResource, dialogs) {
    var log:ILog = Logger.getInstance('userTemplateRolesMenuCtrl');
    $scope.hideImport=true;
    $scope.init = function () {
    };

    $scope.openNewItemDialog = function()
    {
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserTemplateRoleDialog.tpl.html',
        'newUserTemplateRoleDialogCtrl',
        {},
        {size:'md'});

      dlg.result.then(function(addedItem:Users.DtoTemplateRole){
        $scope.reloadData(addedItem.id,addedItem.id);
      },function(){

      });
    }

    $scope.openEditItemsDialog = function(templateRoleToEdit?:Users.DtoTemplateRole,openWithFieldName?:string)
    {
      var item:Users.DtoTemplateRole  = templateRoleToEdit ? templateRoleToEdit : <Users.DtoTemplateRole>$scope.selectedListItemsToOperate[0];
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserTemplateRoleDialog.tpl.html',
        'newUserTemplateRoleDialogCtrl',
        {
          templateRole:item,
          openWithFieldName:openWithFieldName
        },
        {size:'md'});

      dlg.result.then(function(item:Users.DtoTemplateRole){
        editItem(item);
      },function(){
      });
    };

    $scope.openDeleteItemsDialog = function()
    {
      var items:Users.DtoTemplateRole[]=$scope.selectedListItemsToOperate;
      if(items && items.length>0) {
        if(items.length == 1 ){
          let userMsg="You are about to delete role named: '"+(<Users.DtoTemplateRole>items[0]).displayName + "'. Continue?";
          var dlg = dialogs.confirm('Confirmation', userMsg, null);
          dlg.result.then(function (btn) {
            deleteItem(items[0]);
          },function(){
            //No was clicked by user
          });
        }
      }
    };

    var deleteItem = function(item:Users.DtoTemplateRole)
    {
      templateRolesResource.deleteTemplateRole(item.id, function () {
        $scope.reloadData();
      }, function(e) {
        if (e.status == 400 ) {
          dialogs.error('Error', 'Cannot delete role '+item.displayName);
        } else {
          dialogs.error('Error', 'Failed to delete role '+item.displayName);
        }
      });
    };

    var editItem = function (item:Users.DtoTemplateRole) {
      if ( item.name &&  item.name.trim() != '') {
        log.debug('edit role to list  name: ' +  item.name);
        templateRolesResource.updateTemplateRole( item, function () {
          $scope.reloadData(item.id,item.id);
        },function (error) {
          if (error.status == 400 &&error.data.type =='ITEM_ALREADY_EXISTS')
          {
            dialogs.error('Error', 'Role with this name already exists. \n\nSelect another name.');
          }
          else {
            dialogs.error('Error', 'Failed to edit role.');
          }
        });
      }
    };

    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    };

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridTemplateRolesUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'openEditItemDialog') {
          $scope.openEditItemsDialog(fireParam.item,fireParam.openWithFieldName);

        }
        else if (fireParam.action === 'whoIsAssigned') {
          $scope.openWhoIsAssignedDialog(fireParam.item);

        }


      })));

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });
  })
  .directive('templateRolesGridManageMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/manageMenu.tpl.html',
        controller: 'userTemplateRolesMenuCtrl',
        replace:true,
        scope:{
          'selectedListItemsToOperate':'=',
          'firstSelectedListItemToOperate':'=',
          'reloadData':'=',
          'refreshDisplay':'=',
          'gridFindId':'=',
          'ldapGroups':'='
        },
        link: function (scope:any, element, attrs) {
          scope.init(attrs.left);
        }
      };
    }
  )



///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.users.functionalRoles.manage.menu', [
  'resources.users',
  'directives.dialogs.user.functionalRole.new',
])
  .controller('userFunctionalRolesMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,usersResource:IUsersResource,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                       ldapUsersResource:ILDapUsersResource, userBizRolesResource:IUserBizRolesResource,eventCommunicator:IEventCommunicator,
                                                       functionalRolesResource:IFunctionalRolesResource, dialogs) {
    var log:ILog = Logger.getInstance('userFunctionalRolesMenuCtrl');
    $scope.hideImport=true;
    $scope.init = function () {
      //ldapUsersResource.getLDapSettings(
      //  function (settings:AppSettings.DtoLdapConnectionDetails) {
      //    $scope.ldapSettings = settings;
      //    $scope.isLdapSettings = settings && _.isNumber(settings.id);
      //  },
      //  function() {
      //  });
    };



    $scope.openNewItemDialog = function()
    {

      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserFunctionalRoleDialog.tpl.html',
        'newUserFunctionalRoleItemDialogCtrl',
        {},
        {size:'md'});

      dlg.result.then(function(addedItem:Users.DtoFunctionalRoleSummaryInfo){
        $scope.reloadData(addedItem.functionalRoleDto.id,addedItem.functionalRoleDto.id);
      },function(){

      });
    }

    $scope.openEditItemsDialog = function(functionalRoleToEdit?:Users.DtoFunctionalRoleSummaryInfo,openWithFieldName?:string)
    {
      var item:Users.DtoFunctionalRoleSummaryInfo  = functionalRoleToEdit ? functionalRoleToEdit : <Users.DtoFunctionalRoleSummaryInfo>$scope.selectedListItemsToOperate[0];
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newUserFunctionalRoleDialog.tpl.html',
        'newUserFunctionalRoleItemDialogCtrl',
        {
          functionalRoleSummaryInfo:item,
          openWithFieldName:openWithFieldName
        },
        {size:'md'});

      dlg.result.then(function(item:Users.DtoFunctionalRoleSummaryInfo){
        editItem(item);
      },function(){
      });
    };


    $scope.openDeleteItemsDialog = function()
    {
      var items:Users.DtoFunctionalRoleSummaryInfo[]=$scope.selectedListItemsToOperate;
      if(items&& items.length>0) {
        var userMsg;
        if(items.length==1)
        {
          userMsg="You are about to delete role named: '"+(<Users.DtoFunctionalRole>items[0].functionalRoleDto).name+"'. Continue?";
        }
        else if(items.length<6){
          userMsg="You are about to delete " + items.length + " roles. Continue?"+'<br>'+items.map(u=>(<Users.DtoFunctionalRole>u.functionalRoleDto).name).join(', ');
        }
        else {
          userMsg="You are about to delete " + items.length + " roles. Continue?";
        }
        var dlg = dialogs.confirm('Confirmation', userMsg, null);
        dlg.result.then(function (btn) { //user confirmed deletion
          items.forEach(d=> {
            functionalRolesResource.getFunctionalRoleAssignedUsers(d.functionalRoleDto.id,function(users:Users.DTOUser[],totalElements)
            {
              if(users.length>0) {
                var dlg = dialogs.confirm('Confirmation', 'Functional role ' + d.functionalRoleDto.name + ' is already assigned to ' + totalElements + ' users.\n\nProceed anyway?', null);
                dlg.result.then(function (btn) { //user confirmed deletion
                  deleteItem(d.functionalRoleDto)
                }, function (btn) {
                  // $scope.confirmed = 'You confirmed "No."';
                });
              }
              else {
                deleteItem(d.functionalRoleDto);
              }
            },onError)
          });

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });
      }
    };

    var deleteItem = function(item:Users.DtoFunctionalRole)
    {
      functionalRolesResource.deleteFunctionalRole(item.id, function () {
        $scope.reloadData();
      }, function(e) {
        if (e.status == 400 ) {
          dialogs.error('Error', 'Cannot delete role '+item.displayName);
        } else {
          dialogs.error('Error', 'Failed to delete role '+item.displayName);
        }
      });
    };

    var editItem = function (item:Users.DtoFunctionalRoleSummaryInfo) {
      if ( item.functionalRoleDto.name &&  item.functionalRoleDto.name.trim() != '') {
        log.debug('edit role to list  name: ' +  item.functionalRoleDto.name);
        functionalRolesResource.updateFunctionalRole( item, function () {
          $scope.reloadData(item.functionalRoleDto.id,item.functionalRoleDto.id);
        },function (error) {
          if (error.status == 400 &&error.data.type =='ITEM_ALREADY_EXISTS')
          {
            dialogs.error('Error', 'Role with this name already exists. \n\nSelect another name.');
          }
          else {
            dialogs.error('Error', 'Failed to edit role.');
          }
        });
      }
    };

    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    };

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridBizRolesUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'openEditItemDialog') {
          $scope.openEditItemsDialog(fireParam.item,fireParam.openWithFieldName);

        }
        else if (fireParam.action === 'whoIsAssigned') {
          $scope.openWhoIsAssignedDialog(fireParam.item);

        }


      })));

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });
  })
  .directive('functionalRolesGridManageMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/manageMenu.tpl.html',
        controller: 'userFunctionalRolesMenuCtrl',
        replace:true,
        scope:{
          'selectedListItemsToOperate':'=',
          'firstSelectedListItemToOperate':'=',
          'reloadData':'=',
          'refreshDisplay':'=',
          'gridFindId':'=',
          'ldapGroups':'='
        },
        link: function (scope:any, element, attrs) {
          scope.init(attrs.left);
        }
      };
    }
  )



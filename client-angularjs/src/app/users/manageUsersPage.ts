///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users', [
  'users.grid',
  'directives.users.manage.menu',
  'users.profile'
]);

angular.module('users')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.usersManagement, {
            parent:'users',
            url: '/manage?&selectedId?&page?',
            templateUrl: '/app/users/manageUsersPage.tpl.html',
            controller: 'ManageUsersPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngUsers,Users.ESystemRoleName.MngRoles,Users.ESystemRoleName.ViewUsers]
            }
          })

    })
    .controller('ManageUsersPageCtrl', function ($window, $scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,routerChangeService,$filter,
                                                          Logger,propertiesUtils,$stateParams, pageTitle,bizListsResource:IBizListsResource,configuration:IConfig,userSettings:IUserSettingsService,
                                                          dialogs) {
      var log:ILog = Logger.getInstance('ManageUsersPageCtrl');

      $scope.changePage= $stateParams.page && propertiesUtils.isInteger( $stateParams.page) ? parseInt( $stateParams.page ) : null;
      $scope.changeSelectedItem=$stateParams.selectedId;



      $scope.init = function () {
        pageTitle.set("Manage users");
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isMngUsersUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUsers]);
        $scope.isMngUserRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUserRoles]);
      }


      $scope.onUserSelectItem = function (id,name,item) {
        $scope.userSelectedItemId = id;
        $scope.userSelectedItemName = name;
        $scope.userSelectedItem = item;

        $stateParams['selectedId'] = $scope.userSelectedItemId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });

      };

      $scope.pageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.onUserSelectItems = function (data) {
        $scope.userSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess('Users_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'users' ,{},{},gotALotOfRecords);
      };


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var GetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };


    })

///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.users.updatePassword.dialog', [

    ])
    .controller('changePasswordDialogCtrl', function ($scope,  $uibModalInstance,data) {


      var init=function(userItem:Users.DTOUser)
      {
        if(userItem)
        {
          $scope.editMode=true;
          $scope.title='Change password';
          $scope.userItem = userItem;
        }


      }
      init(data.userItem);

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel



      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          $uibModalInstance.close( {oldPwd:$scope.oldPwd,newPwd:$scope.newPwd});
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


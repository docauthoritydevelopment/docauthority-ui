///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.users.items', [
      'directives.dialogs.user.bizRole.new',
      'resources.users'
    ])
    .controller('newUserItemDialogCtrl', function ($window, $scope,  $uibModalInstance,data,propertiesUtils,dialogs,userBizRolesResource:IUserBizRolesResource,configuration:IConfig,
                                                   eventCommunicator:IEventCommunicator,currentUserDetailsProvider:ICurrentUserDetailsProvider, functionalRolesResource:IFunctionalRolesResource) {

      $scope.states=[Users.EUserState.ACTIVE,Users.EUserState.DISABLED];

      var init=function(userItem:Users.DTOUser,profileMode,resetPasswordMode,openWithFieldName)
      {
        $scope.resetPasswordMode = resetPasswordMode;
        $scope.profileMode = profileMode;
        if(userItem)
        {
          $scope.editMode=true;
          $scope.name =  userItem.name;
          $scope.title= $scope.resetPasswordMode?'Reset password':'Edit user profile';
          $scope.userItem = userItem;
          $scope.isCurrentUser =  currentUserDetailsProvider.getUserData().username.toLowerCase()==userItem.username.toLowerCase();
          initUserBizRoles(userItem.bizRoleDtos);
          initUserFunctionalRoles(userItem.funcRoleDtos);
        }
        else
        {
          $scope.editMode=false;
          $scope.title='Add user';
          var userItem = new Users.DTOUser();
          userItem.userState=Users.EUserState.ACTIVE;
       //   userItem.userRole=Users.EUserRole.USER;
          userItem.userType=Users.EUserType.INTERNAL;
          userItem.accountNonExpired = true;
          userItem.accountNonLocked = true;
          userItem.credentialsNonExpired = true;
          userItem.consecutiveLoginFailuresCount = 0;
          userItem.bizRoleDtos=[];
          userItem.funcRoleDtos=[];

         $scope.userItem = userItem;
        }
        $scope.setActiveTab(openWithFieldName?openWithFieldName: 'general');
        updatePermissions();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          updatePermissions();
        });


        $scope.initiated=true;
      }

      var updatePermissions = function()
      {
        var isMngUsersUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUsers]);
        $scope.tabEnabled={
          'general':!$scope.resetPasswordMode,'password':!$scope.editMode||$scope.resetPasswordMode,
          'roles':isMngUsersUser&&!$scope.resetPasswordMode&&!$scope.profileMode,
          'systemRoles':isMngUsersUser&&!$scope.resetPasswordMode&&!$scope.profileMode,
          'functionalRoles':isMngUsersUser&&!$scope.resetPasswordMode&&!$scope.profileMode};
      }
      $scope.setActiveTab=function(tabName:string) {
        $scope.activeTab=tabName;
      }
      $scope.openLock = function() {
        $scope.userItem.accountNonLocked = !$scope.userItem.accountNonLocked;
        $scope.userUnlocked = true;
      };
      $scope.createBizRoleParamOption= function(role:Users.DtoBizRole) {
        var newItem = new ParamOption();
        role = (<Users.DtoBizRoleSummaryInfo>(<any>role)).bizRoleDto?(<Users.DtoBizRoleSummaryInfo>(<any>role)).bizRoleDto:role;
        newItem.id = role.id;
        newItem.display = role.name;
        newItem.subdisplay =role.description?role.description:' ';
        newItem.title = buildSysRolesBubble(role);

        newItem.value = role;
        return newItem;
      }
      $scope.createFunctionalRoleParamOption= function(role:Users.DtoFunctionalRole) {
        var newItem = new ParamOption();
        newItem.id = role.id;
        newItem.display = role.name;
        newItem.subdisplay =role.description?role.description:' ';
        newItem.title = buildSysRolesBubble(role);
        newItem.value = role;
        return newItem;
      };
      var buildSysRolesBubble = function(role) {
        let roleTitle = '';
        if(_.has(role, "systemRoleDtos") && !_.isEmpty(role.systemRoleDtos)) {
          roleTitle = '<div class=\'sys-roles-tooltip\'>';
          _.each(role.systemRoleDtos, function(sysRole:Users.DtoSystemRole){
            roleTitle += '<div  class=\'sys-roles-tooltip-item\'><i class=\'fa fa-wrench icon\'></i><span class=\'name\'>' + sysRole.displayName + '</span></div>'
          });
          roleTitle += '</div>';
        }
        return roleTitle;
      };
      var createBizRoleNewSelectedItem= function(role:Users.DtoBizRole)
      {
        var newItem = $scope.createBizRoleParamOption(role);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      }
      var createFunctionalRoleNewSelectedItem= function(role:Users.DtoFunctionalRole)
      {
        var newItem = $scope.createFunctionalRoleParamOption(role);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      }
      var initUserBizRoles = function(roles:Users.DtoBizRole[])
      {
        var userBizRolesOptions = [];
        if(roles) {
          roles.forEach(r=> {
            userBizRolesOptions.push(createBizRoleNewSelectedItem(r));
          })
        }
        $scope.userBizRolesSavedSelectedItems  =userBizRolesOptions;

      }
      var initUserFunctionalRoles = function(roles:Users.DtoFunctionalRole[])
      {
        var userFunctionalRolesOptions = [];
        if(roles) {
          roles.forEach(r=> {
            userFunctionalRolesOptions.push(createFunctionalRoleNewSelectedItem(r));
          })
        }
        $scope.userFunctionalRolesSavedSelectedItems = userFunctionalRolesOptions;
      }

      init(data.userItem,data.profileMode,data.resetPasswordMode,data.openWithFieldName);


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.autoGeneratePassword = function()
      {
          $scope.userItem.password =autoGeneratePassword();
      }

      $scope.toggleUserActiveState = function()
      {
       if((<Users.DTOUser>$scope.userItem).userState ==Users.EUserState.ACTIVE)
       {
         (<Users.DTOUser>$scope.userItem).userState =Users.EUserState.DISABLED;
       }
        else {
         (<Users.DTOUser>$scope.userItem).userState =Users.EUserState.ACTIVE;
       }
      }

      $scope.isUserActive = function()
      {
        return (<Users.DTOUser>$scope.userItem).userState ==Users.EUserState.ACTIVE;
      }

      $scope.openNewFunctionalRoleDialog = function(successFunction,cancelFunction)
      {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/newUserFunctionalRoleDialog.tpl.html',
          'newUserFunctionalRoleItemDialogCtrl',
          {},
          {size: 'md',windowClass: 'offsetDialog'});

        dlg.result.then(function (functionalRoleSummaryInfo:Users.DtoFunctionalRoleSummaryInfo) {
          successFunction(functionalRoleSummaryInfo.functionalRoleDto);
        }, function () {
          cancelFunction?cancelFunction():null;
        });

      }
      $scope.openNewBizRoleDialog = function(successFunction,cancelFunction)
      {
        var dlg = dialogs.create(
            'common/directives/operations/dialogs/newUserBizRoleDialog.tpl.html',
            'newUserBizRoleItemDialogCtrl',
            {},
            {size: 'md',windowClass: 'offsetDialog'});

        dlg.result.then(function (bizRole:Users.DtoBizRole) {
          successFunction(bizRole);
        }, function () {
          cancelFunction?cancelFunction():null;
        });

      }
      $scope.loadBizRoleOptionItems = function(itemsOptions:any[])
      {
        userBizRolesResource.getBizRoles( function (roles:Users.DtoBizRole[]) {
              if (roles) {
                roles.forEach(role=> {
                  var newItem = $scope.createBizRoleParamOption(role);
                  itemsOptions.push(newItem);
                })
              }
            }
            , onError)
      }
      $scope.loadFunctionalRoleOptionItems = function(itemsOptions:any[]) {
        functionalRolesResource.getFunctionalRoles(
          function (functionalRoles: Users.DtoFunctionalRoleSummaryInfo[]) {
            if (functionalRoles) {
              functionalRoles.forEach(role=> {
                var newItem = $scope.createFunctionalRoleParamOption(role.functionalRoleDto);
                itemsOptions.push(newItem);
              })
            }
          })
      }
      $scope.onBizRoleItemSelected=function(userRoles:ParamActiveItem<Users.DtoSystemRole>[])
      {
        $scope.userRolesSelected  =userRoles.filter(roleItem=>(<ParamActiveItem<Users.DtoSystemRole>>roleItem).active).map(userRole=>userRole.item.value);
      }

      $scope.isBizRolesValid = function()
      {
        return  true;
      }
      $scope.save = function(){
        $scope.submitted=true;
        if((!$scope.tabEnabled.general ||($scope.tabEnabled.general && $scope.generalForm.$valid))
            &&(!$scope.tabEnabled.password ||($scope.tabEnabled.password && $scope.passwordForm.$valid))&&
            $scope.isBizRolesValid())
        {
          (<Users.DTOUser>$scope.userItem).bizRoleDtos=$scope.userRolesSelected;
          (<Users.DTOUser>$scope.userItem).funcRoleDtos=$scope.functionalRolesSelected;

          $uibModalInstance.close( $scope.userItem);
        }
      };


      $scope.onFunctionalRoleItemSelected=function(userRoles:ParamActiveItem<Users.DtoFunctionalRole>[])
      {
        $scope.functionalRolesSelected = userRoles.filter(roleItem=>(<ParamActiveItem<Users.DtoFunctionalRole>>roleItem).active).map(userRole=>userRole.item.value);
      }

      $scope.isFunctionalRolesValid = function() {
        return true;
      }

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      var autoGeneratePassword = function(){

        var specials = '!@#$%^&*()_+{}:"<>?\|[];\',./`~';
        var lowercase = 'abcdefghijklmnopqrstuvwxyz';
        var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var numbers = '0123456789';

        var all = specials + lowercase + uppercase + numbers;
       // Credit to @Christoph: http://stackoverflow.com/a/962890/464744
        var pick = function(array,min, max?) {
          var n, chars = '';

          if (typeof max === 'undefined') {
            n = min;
          } else {
            n = min + Math.floor(Math.random() * (max - min));
          }

          for (var i = 0; i < n; i++) {
            chars += array.charAt(Math.floor(Math.random() * array.length));
          }

          return chars;
        };


        var shuffle = function (array) {
          var array = array.split('');
          var tmp, current, top = array.length;

          if (top) while (--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
          }

          return array.join('');
        };

        var password = (pick(specials,1) + pick(lowercase,1) + pick(uppercase,1) + shuffle(pick(all,3, 10)));
        return password;
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
    })


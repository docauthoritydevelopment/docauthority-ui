///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.functionalRoles', [,
  'directives.users.functionalRoles.manage.menu',
  'users.functionalRoles.crud.grid',
  'users.functionalRoles.manage'
]);

angular.module('users.functionalRoles')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.functionalRolesManagement, {
            parent:'users',
            url: '/dataRoles/manage?&selectedId?&page?',
            templateUrl: '/app/users/manageFunctionalRolesPage.tpl.html',
            controller: 'ManageFunctionalRolesPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngUsers,Users.ESystemRoleName.MngRoles,Users.ESystemRoleName.MngUserRoles]
            }
          })

    })
    .controller('ManageFunctionalRolesPageCtrl', function ($window, $scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,routerChangeService,$filter,
                                                 Logger,propertiesUtils,$stateParams, pageTitle, bizListsResource:IBizListsResource,configuration:IConfig,userSettings:IUserSettingsService,
                                                 dialogs) {
      var log:ILog = Logger.getInstance('ManageFunctionalRolesPageCtrl');

      $scope.changePage= $stateParams.page && propertiesUtils.isInteger( $stateParams.page) ? parseInt( $stateParams.page ) : null;
      $scope.changeSelectedItem=$stateParams.selectedId;



      $scope.init = function () {
        pageTitle.set("Manage data roles");


        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });


      };
      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }

      $scope.onBizRolesSelectItem = function (id,name,item) {
        $scope.bizRoleSelectedItemId = id;
        $scope.bizRoleSelectedItemName = name;
        $scope.bizRoleSelectedItem = item;

        $stateParams['selectedId'] = $scope.bizRoleSelectedItemId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });
      };

      $scope.pageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.onBizRoleSelectItems = function (data) {
        $scope.bizRoleSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess('Data_roles_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'data_roles' ,{},{},gotALotOfRecords);
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var GetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };

    })

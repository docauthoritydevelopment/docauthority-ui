// declare const angular;
///<reference path='../../common/all.d.ts'/>
///<reference path='dialog-types.enum.ts'/>
const module = angular.module('doc2Services', ['dialogsApi', 'actions.immediate']);

module.service('routerChangeService', function RouterChangeService($state: any, $rootScope: any, dialogsService: any,
                                                                   eventCommunicator: IEventCommunicator, $window, $timeout, $q) {
  this.$state = $state;

  dialogsService.subscribeToDialogOpen(() => this.notifyDialogOpen());
  dialogsService.subscribeToDialogClose(() => this.notifyDialogClose());

  const anyParent = <any>window.parent;// as any;

  this.unfocus = function () {
    // hiddenDiv.click()
    document.body.click();
  }


  this.raiseEvent = function (eventName: string, args?: any) {
    if (anyParent && anyParent !== window && anyParent.raiseEvent) {
      anyParent.raiseEvent(eventName, args);
    }
  }

  $window.unfocus = () => this.unfocus();

  document.body.addEventListener('click', (evt) => {

    // If the click event was on the body itself, which is unlikely, it is not an actual click but outside focus

    if (evt.target === document.body) {
      return;
    }
    if (!anyParent.notifyInnerAppFocus) {
      console.error('Please set notifyInnerAppFocus function on main app');
      return;
    }
    anyParent.notifyInnerAppFocus();
  })


  this.reload = function () {
    try {
      $state.reload();
    } catch (e) {
      console.error('could not reload', e);
    }
  }

  this.setAngularJsRoute = function (stateName: ERouteStateName, stateParams: any) {
    console.log('chnage v1 state');
    const defer = $q.defer();
    $('body').toggleClass('embedded', stateParams && stateParams.isEmbedded || !!$state.params.isEmbedded);

    if (!stateName && $state.current) { // including empty string ""
      $state.transitionTo($state.current,
        stateParams,
        {
          notify: true, inherit: true
        });
      defer.resolve({stateName, stateParams});

    } else {
      $state.go(stateName, stateParams, {inherit: false})
         .then(() => {
         defer.resolve({stateName, stateParams});
      }, (err) => {
        console.error('Could not navigate, retry on ', err, stateName, stateParams)
        if (stateName != ERouteStateName.empty && stateName != ERouteStateName.unauthorized) {
          $timeout(() => this.setAngularJsRoute(stateName, stateParams)).then(() => {
            defer.resolve({stateName, stateParams});
          })
        }
      });
    }


    return defer.promise;

  };

  this.addFileToProcess = (fileName,type, queryParams,infoParams,useAdvnacedMode:boolean = false) => {
    if (anyParent.addFileToProcess) {
      anyParent.addFileToProcess(fileName,type, queryParams,infoParams, useAdvnacedMode);
    }
  }

  this.openAddEditChartWidgetDialog = (data:any) => {
    if (anyParent.openAddEditChartWidgetDialog) {
      anyParent.openAddEditChartWidgetDialog(data);
    }
  }
  this.updateAsyncUserOperations = (data:any) => {
    if (anyParent.updateAsyncUserOperations) {
      anyParent.updateAsyncUserOperations(data);
    }
  }


  this.refreshMenuItems = () => {
    if (anyParent.refreshMenuItems) {
      anyParent.refreshMenuItems();
    }
  }

  this.setTitle = function (title) {
    if (anyParent && anyParent.setTitle) {
      anyParent.setTitle(title);
    }
  }

  this.notifyDialogOpen = function () {
    if (anyParent && anyParent.onDialogOpened) {
      anyParent.onDialogOpened();
    }
  }

  this.changeParentUrl = function (url) {
    if (anyParent && anyParent.changeUrl) {
      anyParent.changeUrl(url);
    }
  }

  this.fireEvent = function (eventType, eventParams) {
    eventCommunicator.fireEvent(eventType, eventParams)
  }

  this.notifyDialogClose = () => this.notifyDialogclose();
  this.notifyDialogclose = function () {
    if (anyParent && anyParent.onDialogClosed) {
      anyParent.onDialogClosed();
    }
  }

  this.openDialog = function (dialogType: EDialogTypes) {
    let result;
    switch (dialogType) {

      case EDialogTypes.EMailNotificationSettings : {
        result = dialogsService.openMailSettingsDialog();
        break;
      }
      case EDialogTypes.LicenceKeyDialog: {
        result = dialogsService.openLicenseKeyDialog('');
        break;
      }
    }
    this.notifyDialogOpen();
    //result.result.then(() => this.notifyDialogclose(), () => this.notifyDialogclose());
    return result;
  }


  this.fireEvent = function (type: EEventServiceEventTypes, data: any) {
    eventCommunicator.fireEvent(type, data);
  }

  this.refreshLoginStatus = function () {
    console.debug('TODO refresh login status');
  };

  $window.setAngularJsRoute = this.setAngularJsRoute;
  $window.reload = this.reload;
  $window.openDialog = (dialogType) => {
    return this.openDialog(dialogType)
  }

  $window.fireEvent = this.fireEvent;

  // Call onpushstate whenever a state record is pushed to the history
  //This is a hack that come to solve the problem  that calling pushState does generate any event (like changing the hash did) so loading of new content cannot be detected anymore.
  //so onpushstate event is called thanks to this hack
  (function (history) {
    let pushState = history.pushState;
    history.pushState = function (state) {
      if (typeof (<any>history).onpushstate === 'function') {
        (<any>history).onpushstate({state: state});
      }
      return pushState.apply(history, arguments);
    }
  })(window.history);



  let beforeLastPlace;
  let lastPlace;

  window.onpopstate = (<any>history).onpushstate = function (e) {
    if (anyParent !== window && anyParent.stateChanged) {
      // Set timeout, to notify *after* the url has changed
      setTimeout(() => {
        if (window.location.pathname !== '/v1/unauthorized' ) {
          const url = `${window.location.pathname}${window.location.search}`;
          anyParent.stateChanged(url)
        }
        else {
          if (lastPlace > window.history.length) {
            anyParent.history.back();
          }
          else if (lastPlace < window.history.length) {
            anyParent.history.forward();
          }
          else if (beforeLastPlace && beforeLastPlace >= window.history.length) {
            anyParent.history.back();
          }
          else if (beforeLastPlace && beforeLastPlace < window.history.length) {
            anyParent.history.forward();
          }
        }

        beforeLastPlace = lastPlace;
        lastPlace = window.history.length;
      })
    }
  };


  function buildSearchObject(searchString) {
    const result: any = {};
    const searchArray = searchString.split('&');
    searchArray.forEach(arg => {
      const arr = arg.split('=');
      result[arr[0]] = arr[1];
    })
    return result;
  }

  function buildSearchAndUrl(newUrl) {
    const paramsDelimiter = newUrl.indexOf('?');
    let search = null;
    if (paramsDelimiter > 0) {
      const searchString = newUrl.substring(paramsDelimiter + 1);
      search = buildSearchObject(searchString)
      newUrl = newUrl.substring(0, paramsDelimiter);
    }
    return {url: newUrl, search: search};
  }


  // window.location.href = newUrl;

});

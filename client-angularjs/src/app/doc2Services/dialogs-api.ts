///<reference path='./doc2Services.ts'/>


angular.module('dialogsApi', []).service('dialogsService', function (dialogs, $window, $q) {


  this.dialogOpenSubscribers = [];
  this.dialogCloseSubscribers = [];
  this.promises = [];

  this.subscribeToDialogOpen = function (subscriber: () => void) {
    this.dialogOpenSubscribers.push(subscriber);
  }

  this.subscribeToDialogClose = function (subscriber: () => void) {
    this.dialogCloseSubscribers.push(subscriber);
  }

  this.runSubscriber = function (subscriber) {
    if (typeof subscriber === 'function') {
      subscriber()
    }
  }


  this.openDialog = function (originalFunc, args) {

    // notify shell app on close on setTimeout (after this execution cycle) to avoid situation when previous dialog closed
    // and this dialog hasn't opened yet
    setTimeout(() =>  this.dialogOpenSubscribers.forEach(subscriber => subscriber()));
    const dialog = originalFunc(...args);
    this.promises.push(dialog.result);
    $q.all(this.promises).then(() => this.notifyClose(), () => this.notifyClose())
    return dialog;
  }

  this.notifyClose = () => {
    this.dialogCloseSubscribers.forEach(subscriber => this.runSubscriber(subscriber))
    this.promises = [];
  }


  const originalDialogCreate = dialogs.create;
  dialogs.create = (...args) => this.openDialog(originalDialogCreate, args)

  const originalDialogConfirm = dialogs.confirm;
  dialogs.confirm = (...args) => this.openDialog(originalDialogConfirm, args);

  const originalDialogError = dialogs.error;
  dialogs.error = (...args) => this.openDialog(originalDialogError, args);
  const originalDialogNotify = dialogs.notify;
  dialogs.notify = (...args) => this.openDialog(originalDialogNotify, args);


  this.openMailSettingsDialog = function () {

    const dlg = dialogs.create(
      'common/directives/dialogs/mailSettingsDialog.tpl.html',
      'mailSettingsDialogCtrl',
      {},
      {size: 'md'});

    dlg.result.then(function () {

    }, function (btn) {
      // $scope.confirmed = 'You confirmed "No."';
    });

    return dlg;

  };


  this.openLicenseKeyDialog = function (licenseUserMessage) {
    var dlg = dialogs.create(
      'common/directives/dialogs/licenseKeyDialog.tpl.html',
      'licenseKeyDialogCtrl',
      {licenseAlertMessage: licenseUserMessage},
      {size: 'md'});
    dlg.result.then(function () {
      $window.location.reload();
    }, function () {
      // $scope.confirmed = 'You confirmed "No."';
    });
    return dlg;
  }
})
;

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('docTypes.tags', [
])
  .controller('docTypesTagsCtrl', function ($scope, $compile, $window,$element,$timeout,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                            docTypeTagsService, fileTagsResource:IFileTagsResource, fileTagResource, userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,docTypesResource:IDocTypesResource,docTypeResource) {

    var log:ILog = Logger.getInstance('docTypesTagsCtrl');
    var tagResource:IfileTagResource;
    var originalSelectedTags;
    var selectedFileTagId;

    $scope.init = function (isMaster) {
      tagResource = fileTagResource(EntityDisplayTypes.doc_types);
      selectedFileTagId = `fileTag${$scope.selectedTagTypeId}`;

      extractAllTagFiles();
      setSelectedTagFiles();
    };

    //---------------------------------------

    var extractAllTagFiles = function() {

      let tagType = _.filter($scope.tagTypes, (tagType) => {
        return tagType.fileTagTypeDto.id === $scope.selectedTagTypeId;
      })[0] || <any> {};

      setStyleId(tagType.fileTags);

      $scope.fileTagsArr = tagType.fileTags || [];
      $scope.filteredFileTags = $scope.fileTagsArr;
    };

    //------------------------------------

    var setStyleId = function(fileTags){
      _.forEach(fileTags, (fileTag) => {
        fileTag.styleId = splitViewBuilderHelper.getTagStyleId(fileTag);
      });
    };

    //------------------------------------

    var setSelectedTagFiles = function() {

      $scope.selectedFiles = {};
      $scope.selectedTagsArr = $scope.docType[selectedFileTagId] || [];

      originalSelectedTags = $scope.selectedTagsArr.slice();

      _.forEach($scope.selectedTagsArr, (tag) => {
        $scope.selectedFiles[tag.id] = true;
      });
    };

    //---------------------------------------

    var updateRecentlyUsedTagFiles = function() {
      docTypeTagsService.getRecentlyFilesUsed($scope.selectedTagTypeId, (fileTagIds) => {
        $scope.recentlyUsedTagFiles = _.filter($scope.fileTagsArr, (fileTag) => {
          return _.includes(fileTagIds, fileTag.id);
        });
      });
    }

    //---------------------------------------

    $scope.openTagsDDL = function(e,  tagTypeId) {
      if($scope.moveMode) {
        _.defer(() => {
          $('body').click();
        });
      } else{
        $scope.showTagsDropdown = true;
        $scope.searchText = {};

        updateRecentlyUsedTagFiles();
      }
    };

    //---------------------------------------

    $scope.toggleFileTag = function($event, fileTag:Entities.DTOFileTag) {
      let elm = $($event.target);

      if(elm.closest('.tag-item').find('.fa-check').length > 0) {
        removeFileTag(elm, fileTag);
      } else {
        addFileTag(elm, fileTag);
      }
      $('body').click();
    };

    //---------------------------------------

    var addFileTag = function(elm, fileTag:Entities.DTOFileTag) {
      $scope.selectedFiles[fileTag.id] = true;

      if(fileTag.type.singleValueTag) {
        if(!_.isEmpty($scope.selectedTagsArr )) {
          removeFileTag(elm, $scope.selectedTagsArr[0])
        }
      }
      $scope.selectedTagsArr.push(fileTag);
      updateTagChange(fileTag);

      docTypeTagsService.updateRecentlyUsedFileTag($scope.selectedTagTypeId, fileTag.id, () => {
        updateRecentlyUsedTagFiles();
      });
    };

    //---------------------------------------

    var removeFileTag = function(elm, fileTag:Entities.DTOFileTag) {
      $scope.selectedFiles[fileTag.id] = false;

      $scope.selectedTagsArr = _.filter($scope.selectedTagsArr, (tagItem) => {
        return tagItem.id !== fileTag.id
      });
      updateTagChange(fileTag);
    };

    //--------------------------------------

    var updateTagChange = function(fileTag) {
      $scope.docType[selectedFileTagId] = $scope.selectedTagsArr;

        $scope.onTagChanged($scope.docType.id, {
          tagTypeId: fileTag.type.id,
          tags: $scope.selectedTagsArr,
          originalTags: originalSelectedTags
        });
    };

    //---------------------------------------

    $scope.addNewTagClick = function(e) {

      showDropdownList(e, false);

      let selectedFileTags = _.filter($scope.tagTypes, (tagType) => {
        return tagType.fileTagTypeDto.id === $scope.selectedTagTypeId;
      });

      let tagType = !_.isEmpty(selectedFileTags) ? selectedFileTags[0].fileTagTypeDto : null;

      let dlg = dialogs.create(
        'common/directives/reports/dialogs/newTagDialog.tpl.html',
        'newTagDialogCtrl',
        {tagType:tagType, tagTypes:$scope.tagTypes},
        {size:'md'});

      dlg.result.then(function(data:any){
        addNewTag(data.tag);
        if(data.createAnother) {
          _.defer(function () {
            $scope.addNewTagClick(e)
          })
        } else {
          _.defer(() => {
            showDropdownList(e, true);
          }, 100);
        }
      },function(){
        _.defer(() => {
          showDropdownList(e, true);
        }, 100);
      });
    };

    //---------------------------------------

    var showDropdownList = function(e, show){

      let ddl =  $(e.target).closest('.doc-type-tags-ddl');
      if(show){
        ddl.removeClass('show-anyway');
        ddl.css('display', 'block');
      }else{
        ddl.addClass('show-anyway');
        ddl.css('display', 'none');
      }
    };

    //---------------------------------------

    var addNewTag = function (newTag:Entities.DTOFileTag) {

      newTag.name =  newTag.name?( newTag.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;

      if ( newTag.name &&  newTag.name.trim() != '') {
        fileTagsResource.addNewTag( newTag.type.id, newTag, function (added) {
          fileTagsResource.getTagListByTagType($scope.selectedTagTypeId, function (tags) {
            $scope.fileTagsArr = moveNewTagToTop(tags, newTag);
            setStyleId($scope.fileTagsArr);
            $scope.searchTextChange();
          },
          () => {
          });
        }, function (error) {
          if(error && error.userMsg) {
            dialogs.notify('Note',error.userMsg);
          } else{
            dialogs.error('Error','Failed to create new tag: '+newTag.name);
          }
        });
      }
    };

    //---------------------------------------

    var moveNewTagToTop = function(tags, newTag) {
      let _tags = [];

      _.forEach(tags, (tag) => {
        if(tag.name == newTag.name) {
          _tags.unshift(tag);
        }else {
          _tags.push(tag);
        }
      });
      return _tags;
    };

    //---------------------------------------

    $scope.searchTextChange = function(tagId) {
      let _searchText = $scope.searchText[$scope.docType.id]  || '';

      if (_.isEmpty(_searchText)) {
        $scope.filteredFileTags = $scope.fileTagsArr;
      } else {
        _searchText = _searchText.toLowerCase();
        $scope.filteredFileTags = _.filter($scope.fileTagsArr, (fileTag) => {
          return fileTag.name.toLowerCase().indexOf(_searchText) === 0;
        });
      }
    };

    //---------------------------------------

    $scope.clearSearchText = function() {
      $scope.searchText[$scope.docType.id] = '';
      $scope.filteredFileTags = $scope.fileTagsArr;
    };
  })

  .directive('docTypesTags',
    function () {
      return {
        templateUrl: '/app/docTypes/docTypesTags.tpl.html',
        controller: 'docTypesTagsCtrl',
        replace:true,
        scope:{
          'docType':'=',
          'onTagChanged': '=',
          'tagTypes': '=',
          'moveMode': '=',
          'selectedTagTypeId': '=',
          'isMngDoctypeConfigUser': '='
        },
        link: function (scope:any, element, attrs) {
          scope.init(false);
        }
      };
    }
  );

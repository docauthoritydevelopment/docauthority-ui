///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('services.docTypeTags', [

])
.factory('docTypeTagsService',  function($rootScope, localStorage:IUserStorageService){

  var service=<any>{};

  service.updateRecentlyUsedFileTag = function(tagId, fileId, callback){

    getTagsFiles((tags) => {
        if(!_.includes(tags[tagId], fileId)) {
          tags[tagId] = tags[tagId] || [];
          tags[tagId].push(fileId);

          if (tags[tagId].length > 4) {
            tags[tagId] = tags[tagId].slice(1);
          }
          localStorage.setUserData('docTypeTags', 'recentlyFilesUsed', JSON.stringify(tags), () => {}, () => {})
        }
        if (callback) {
          callback(tags);
        }
      });
  };

  service.getRecentlyFilesUsed = function(tagId, callback) {
     getTagsFiles((tags) => {
       if (callback) {
         callback(tags[tagId] || []);
       }
     })
  };

  var getTagsFiles = function(callback) {
    localStorage.getUserData('docTypeTags', 'recentlyFilesUsed',
      (sTags) => {
          callback(JSON.parse(sTags || '{}'));
      },
      () => {});
  };
  return  service;
});

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('docTypes', [
  // 'directives',
  'ui.router',
  'resources.docTypes',
  'directives.docTypes',
  'docTypes.tags'
]);

angular.module('docTypes')
    .config(function ($stateProvider) {
      $stateProvider
          .state('docTypes', {
            //   abstract: true,
            url: '/docTypes',
            templateUrl: '/app/docTypes/docTypesPage.tpl.html',
            controller: 'DocTypesPageCtrl'
          })

    })
    .controller('DocTypesPageCtrl', function ($scope, $state, $location, Logger, pageTitle,propertiesUtils) {
      var log:ILog = Logger.getInstance('DocTypesPageCtrl');



      $scope.init = function () {
        pageTitle.set("Manage DocTypes  ");
      };


    })
    .controller('ManageDocTypesCtrl', function ($scope, $state, $timeout, $location, Logger, pageTitle,chartResource:IChartsResource,propertiesUtils, splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                $templateCache, userSettings:IUserSettingsService,fileTagsResource:IFileTagsResource, saveFilesBuilder:ISaveFileBuilder,dialogs,configuration,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('ManageDocTypesCtrl');

      $scope.init=function()
      {
        setUserRoles();
        $scope.moveMode = false;
        if($scope.showTags) {
          $scope.showFlags = true;
          fileTagsResource.getAllTags(onGetAllTagsCompleted, onError);
        }
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      }

      var onGetAllTagsCompleted = function (fileTagLists:Entities.DTOFileTagTypeAndTags[]) {

        _.forEach(fileTagLists, (fileTagItem) => {
          fileTagItem.styleId = splitViewBuilderHelper.getTagStyleId(<any>{type:{id:fileTagItem.fileTagTypeDto.id}});
        });

        $scope.tagTypes = _.filter(fileTagLists, (fileTagItem) => {
          return fileTagItem.fileTagTypeDto.system === false;
        });

        if(!_.isEmpty($scope.tagTypes)) {
          userSettings.getDocTypeTagColumns((selectedColumnId)=> {
            let selectedTagTypeId;

            _.forEach($scope.tagTypes, (tagType) => {
              if(tagType.fileTagTypeDto.id === selectedColumnId) {
                selectedTagTypeId = selectedColumnId;
              }
            });
            if(_.isUndefined(selectedTagTypeId)) {
              selectedTagTypeId = $scope.tagTypes[0].fileTagTypeDto.id
            }
            $scope.selectedTagTypeId = selectedTagTypeId;
          });
        }
      };

      var setUserRoles = function()
      {
        $scope.isMngDocTypeConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDocTypeConfig]);
      }
      $scope.userFilterChanged = function(userFilterText)
      {
        $timeout(function()
        {
          if(userFilterText&& userFilterText.trim()!='') {
            $scope.filterData ={filter:
               { field: "name", operator: "contains", value: userFilterText}
              ,isClientFilter:true
            };
          }
          else {
            $scope.clearUserFilter();
          }
        },200)

      }

      $scope.saveTagsChanges = function(callback) {
        let changes = buildTagChanges();

        fileTagsResource.setDocTypeTags(changes, (res) => {
          $scope.tagsChanges = [];
          $scope.updateDataItems(true, res);

          if(_.isFunction(callback)) {
            callback();
          }
        }, () => {
        })
      };

      var buildTagChanges = function() {
        let changes = {};

        _.forEach($scope.tagsChanges, (tag) => {
          changes[tag.docTypeId] = [];

          _.forEach(tag.fileTags, (fileTag) => {
            let tagList = _.map(fileTag.tags, (tag) => {
              return {
                id:tag.id
              };
            });
            changes[tag.docTypeId].push({
              fileTagType: {
                id: fileTag.tagTypeId
              },
              fileTags: tagList
            });
          });
        });

        return changes;
      };

      $scope.resetTagsChanges = function() {
        $scope.tagsChanges = [];
        $scope.updateDataItems(false);
      };

      $scope.clearUserFilter = function()
      {
        $scope.userFilterText=null;
        $scope.filterData={filter:{},isClientFilter:true};
      }

      $scope.onSelectDocTypeItems = function (data) {
        $scope.docTypeSelectItems = data;

      };

      $scope.onSelectDocTypeItem = function (id,name,reselectId,data) {
        $scope.docTypeSelectItem = data;
        $scope.onSelectedItem?$scope.onSelectedItem(id,name,reselectId,data):null;
      };

      $scope.onExportDocTypesToExcel= function () {
          $scope.exportToExcelInProgress=true;
          $scope.onExportToExcel= {fileName: 'DocTypes.xlsx',includeFlatData:true};
      };

      $scope.exportToExcelCompleted=function(notFullRequest:boolean)
      {
        if(notFullRequest) {
          dialogs.notify('Note','Excel file do not contain all data.<br/>Excel file created but records count limit reached.');
        }
        $scope.exportToExcelInProgress = false;
      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
    })
    .controller('DocTypesCtrl', function ($scope, $compile, $state,$window, $location,Logger,$timeout,$stateParams,$element,userSettings,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                          fileTagsResource:IFileTagsResource, fileTagResource, propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,
                                          docTypeTagsService, routerChangeService, $templateCache, chartResource:IChartsResource,saveFilesBuilder:ISaveFileBuilder,dialogs) {
      var log: ILog = Logger.getInstance('DocTypesCtrl');

      $scope.searchText = {};
      $scope.elementName='doc-types-tree-list';
      $scope.entityDisplayType = EntityDisplayTypes.doc_types;
      var gridOptions:ui.IGridHelper ;
      var tagResource:IfileTagResource;

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {
        tagResource = fileTagResource(EntityDisplayTypes.doc_types);

        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        gridOptions  = GridBehaviorHelper.createTreeList<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData, rowTemplate, $stateParams, null,getDataUrlBuilder,true,null);

        gridOptions.gridSettings.change = handleSelectionChange;
        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element,false,$scope.paddingBottom,false,
            $scope.showAllDocTypes?createAdditionalExcelSheets:null);
      };

      $scope.$watch(function () { return $scope.selectedTagTypeId; }, function () {
        updateTagColumn();
      },true);

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,fullyContained,collapsedData) {
        var url;
        if($scope.showAllDocTypes)
        {
          url=  configuration.docType_list_url;
        }
        else
        {
          url = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName, restrictedEntityId, entityDisplayType, configuration, includeSubEntityData,fullyContained,collapsedData);
        }

        return url;
      };

      $scope.updateDataItems = function(saveNew, changedDocTypes) {

        let treelist = $element.data("kendoTreeList");
        let dataItems = treelist.dataItems();
        let changes = {};

        _.forEach($scope.tagsChanges, (item) => {
          changes[item.docTypeId] = {};

          _.forEach(item.fileTags, (fileTag) => {
            changes[item.docTypeId]['fileTag' + fileTag.tagTypeId] = saveNew ? fileTag.tags : fileTag.originalTags;
          });
        });

        for(var i=0; i<dataItems.length; i++) {
          if(_.has(changes, dataItems[i].id)) {
            _.forEach(changes[dataItems[i].id], (val, key) => {
              dataItems[i][key] = val;
            })
          }
          _.forEach(changedDocTypes, (val, key) => {
            if (val.id === dataItems[i].id) {
              dataItems[i].fileTagTypeSettings = val.fileTagTypeSettings;
              setAllTagFiles(dataItems[i]);
            }
          });
        }
        $scope.reloadDataToGrid();
      };

      var pathToExpandAndSelect;
      var handleSelectionChange=function(e)
      {
        var treelist = $element.data("kendoTreeList")
        var selectedRows =   treelist.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
          var dataItem = treelist.dataItem(selectedRows[i]);
          selectedDataItems.push(dataItem);
        }
        $timeout(function() {
          $scope.$applyAsync(function () {

            $scope.handleSelectionChange(selectedDataItems);
          });
        });
      };

      var getDataUrlParams = function () {
        return null;
      };

      var parseData=function(data:Entities.DTODocType[])
      {
       data.forEach(d=> {
         if(d.description==null) {
           d.description = '';
         }
         var dataItem:Entities.DTODocType =(<any>d).item;
         if(dataItem&&(<Entities.DTODocType>dataItem).description==null) {
           (<Entities.DTODocType>dataItem).description ='';
         }
         if(dataItem&&dataItem.parentId) {
           (<any>d).parentId=dataItem.parentId;
         }

         (<any>d).styleId = splitViewBuilderHelper.getDocTypeStyleId((<any>d).item?(<any>d).item:d);
         (<any>d).depth = splitViewBuilderHelper.getDocTypeDepth((<any>d).item?(<any>d).item:d);
          if(dataItem) {
             (<any>d).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage((<any>d).subtreeCount,(<any>d).subtreeCountUnfiltered);
          }
         if($scope.showTags) {
           setAllTagFiles(d);
           updateDocTypeFileTags(d);
         }

       });

        data.sort( propertiesUtils.sortAlphbeticFun('name'));
        $scope.totalElements = data?data.length:0;

        log.debug($scope.elementName+' total elements '+$scope.totalElements);
        return data;
      };

      var setAllTagFiles = function(d) {
        d.tagTypeList = [];

        _.forEach((<any>d).fileTagTypeSettings, (tagType) => {
          _.forEach(tagType.fileTags, (tagType) => {
            d.tagTypeList.push( {
              id: tagType.id,
              name: `${tagType.type.name} - ${tagType.name}`,
              styleId: splitViewBuilderHelper.getTagStyleId(tagType)
            })
          });
        });
      };

      var updateDocTypeFileTags = function(d) {
        _.forEach((<any>d).fileTagTypeSettings, (settings) => {
          _.forEach(settings.fileTags, (fileTag) => {
            d["fileTag"+ fileTag.type.id] = d["fileTag"+ fileTag.type.id] || [];
            d["fileTag"+ fileTag.type.id].push({
              id:fileTag.id,
              name:fileTag.name,
              styleId: splitViewBuilderHelper.getTagStyleId(fileTag),
              type: fileTag.type
            });
          });
        })

      };

      var updateTagColumn = function() {
        $scope.reloadDataToGrid();
      };

      var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODocType> =  Entities.DTOAggregationCountItem.Empty();
      var dTODocType:Entities.DTODocType =  Entities.DTODocType.Empty();
      var prefixFieldName = $scope.showAllDocTypes?'':propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

      var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.id),
        title: 'ID',
        type: EFieldTypes.type_number,
        isPrimaryKey:true,
        displayed: false,
        editable: false,
        nullable: true,
      };
      var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: !$scope.showTags,
        editable: false,
        nullable: true,
        width:'1px',
        template:' '
      };
      $scope.showAllDocTypes?description.template=null:null;
      $scope.showAllDocTypes?description.width=null:null;
      var singleValue:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.singleValue),
        title: 'Single value',
        type: EFieldTypes.type_boolean,
        displayed: !$scope.showTags,
        editable: false,
        nullable: true,
        centerText: true,
        width:'80px',
        template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTODocType, dTODocType.singleValue)+'" title="DocType is single value association" ><span class="fa fa-check"></span></span>'

      };

      $scope.isParentOfSelected = function(dataItem:Entities.DTODocType)
      {
        if($scope.selectedItem) {
          var selected = (<Entities.DTODocType>($scope.selectedItem.item));
          var s = selected.parents != null && selected.parents.indexOf(dataItem.id) >= 0 &&
            selected.parents.indexOf(dataItem.id) < selected.parents.length - 1;
          return s;
        }
        return null;
      }
      var iconTemplate =  "<span  style='font-size: 18px;width:16px;padding: 2px 10px 0 8px;'><span class='tag tag-#: styleId #  opacity-depth-#: depth # "+configuration.icon_docType+"'></span></span>"
      var nameNewTemplate = '<span class="title-primary">#: '+ prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.name)+' #</span>';
      var nameNewTemplate =    '<span ng-if="isParentOfSelected(dataItem)" class="notice" ng-class1="{\'k-state-selected-parent\':isParentOfSelected(dataItem)}"></span><span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
          '<a  class="title-primary" title="DocType: #: '+ prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.name)+' #">#: '+ prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.name)+' #<i class="caret ng-hide"></i> </a></span>'+
          '<ul class="dropdown-menu"  > '+
          "<li><a  ng-click='filterByID(dataItem)'><span class='fa fa-filter'></span>  Filter by this docType</a></li>"+
          '  </ul></span>';
      $scope.filterByID = function(item:Entities.DTOAggregationCountItem<Entities.DTODocType>) {

        setFilterByEntity(item.item.id,item.item.name);
      };
      var setFilterByEntity = function(gId,gName)
      {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doFilterByID',
          id: gId,
          entityDisplayType:$scope.entityDisplayType,
          filterName:gName,
          includeSubEntityDataLeft:true
        });
      };
      var uniqueName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.uniqueName),
        title: 'Unique_Name',
        type: EFieldTypes.type_string,
        displayed: false,
        editable: false
      };

      var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
        title: 'Number of direct filtered  files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        width:'1px',
        nullable: false,
        template:' '
      };
      $scope.showAllDocTypes?numOfFiles.template=null:null;
      $scope.showAllDocTypes?numOfFiles.width=null:null;

      var filterTemplate = splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCountUnfiltered),propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCount),'filterPercentage');
      var numOfFilesNewTemplate = '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'=='+propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCount)+') {#<span class="title-forth" > All at this level </span>#}#' +
          '<span class="title-forth" style="white-space: nowrap;" >' +
          '# if('+numOfFiles.fieldName+'>0&&'+numOfFiles.fieldName+'!='+propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCount)+') {# #: kendo.toString('+ numOfFiles.fieldName+',"n0") # #if('+propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2)+'>0) {# / #: kendo.toString('+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2) +',"n0") # #}# at this level #}# </span> ';
      var numOfAllFilteredFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.subtreeCountUnfiltered),
        title: 'Number of all filtered files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        width:'300px',
        nullable: false,
        template:filterTemplate+'<div style="min-height: 12px;">'+numOfFilesNewTemplate+'</div>'
      };
      $scope.showAllDocTypes?numOfAllFilteredFiles.template=null:null;

      //var numOfFilesNewTemplate = '<span class="title-third" >#: '+ numOfFiles.fieldName+' #  docTypes' +
      //    '# if('+numOfAllFilteredFiles.fieldName+'>0) {#, #: kendo.toString('+ numOfAllFilteredFiles.fieldName+'-'+numOfFiles.fieldName+',"n0") # docTypes in subtree #}#' +
      //    '</span>';


      var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        expandable:true,
        //width:'40%',
        ddlInside:true,
        template:'<span style="display: inline-block">'+iconTemplate+nameNewTemplate+'</span>'
      };

      if($scope.showAllDocTypes) {
        if($scope.showTags) {
          name.template= $templateCache.get('/app/docTypes/docTypeItem.tpl.html');
        } else {
          name.template='<span class="title-secondary"> #: '+prefixFieldName+propertiesUtils.propName(dTODocType, dTODocType.name)+' # </span>&nbsp;<span  class="'+configuration.icon_docType+' tag-#: styleId #  opacity-depth-#: depth #"></span>';
          name.width='50%';
        }
      }

      var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTODocType, dTODocType.parentId),
        title: 'Parent ID',
        isParentId:true,
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true,
      };

      var fullName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTODocType, dTODocType.fullName),
        title: 'Full Name',
        type: EFieldTypes.type_string,
        displayed: false,
        editable: false,
        nullable: true,
      };

      var fields:ui.IHierarchicalFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(uniqueName);

      if(!$scope.showAllDocTypes) {
        fields.push(numOfFiles);
        fields.push(numOfAllFilteredFiles);
      }
      else{
    //    fields.push(singleValue);
      }
      fields.push(description);
      fields.push(parentId);
      fields.push(fullName);

      var rowTemplate=$scope.showAllDocTypes?'':"NA";

      var onSelectedItemChanged = function () {
        pathToExpandAndSelect = $scope.selectedItem[id.fieldName];
        if( $scope.selectedItem) {
          $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[name.fieldName], $scope.selectedItem[id.fieldName],$scope.selectedItem);
        }
      };

      var createAdditionalExcelSheets=function(successFunction)
      {

        var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODocType> =  Entities.DTOAggregationCountItem.Empty();
        var dTODocType:Entities.DTODocType =  Entities.DTODocType.Empty();

        var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.id),
          title: 'ID',
          type: EFieldTypes.type_number,
          isPrimaryKey:true,
          displayed: true,
        };
        var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.name),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var uniqueName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.uniqueName),
          title: 'Unique_Name',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var singleValue:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.singleValue),
          title: 'singleValue',
          type: EFieldTypes.type_string,
          displayed: false,
        };
        var parentUniqueName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.parentUniqueName),
          title: 'Parent',
          isParentId:true,
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.parentId),
          title: 'parentId',
          isParentId:true,
          type: EFieldTypes.type_number,
          displayed: true,
        };
        var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTODocType, dTODocType.description),
          title: 'Description',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var risk:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: 'dummy1',
          title: 'Risk Information Name',
          type: EFieldTypes.type_string,
          displayed: true,
          nullable: true,
        };
        var entityListName:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: 'dummy2',
          title: 'Entity List Name',
          type: EFieldTypes.type_string,
          displayed: true,
          nullable: true,
        };

        var createTagColumns = function(fieldsForCsv) {
          _.forEach($scope.tagTypes, (tag) => {
            fieldsForCsv.push(
              <ui.IHierarchicalFieldDisplayProperties>{
                fieldName: 'fileTag' + tag.fileTagTypeDto.id,
                title: tag.fileTagTypeDto.name,
                type: EFieldTypes.type_string,
                displayed: true
              }
            )
          });
        };

        var fieldsForCsv:ui.IHierarchicalFieldDisplayProperties[] =[];
        fieldsForCsv.push(id);
        fieldsForCsv.push(name);
        fieldsForCsv.push(uniqueName);
        fieldsForCsv.push(singleValue);
        fieldsForCsv.push(description);
        fieldsForCsv.push(parentUniqueName);
        fieldsForCsv.push(parentId);
        fieldsForCsv.push(risk);
        fieldsForCsv.push(entityListName);

        if($scope.showTags) {
          createTagColumns(fieldsForCsv);
        }

        var findDocTypeById = function(id:number, list:Entities.DTODocType[]) : Entities.DTODocType {
          for (var d in list) {
            if (list[d].id == id) {
              return list[d];
            }
          }
          return null;
        };

        var convertIdToUniqueName = function(data:any) {
          for (var d in <Entities.DTODocType[]>data.content) {
            if (data.content[d].parentId) {
              var p = findDocTypeById(data.content[d].parentId, data.content);
              data.content[d].parentUniqueName = p.uniqueName;
            }
          }
        };

        var updateDocTypeFileTagsForReport = function(data) {
          _.forEach(data.content, (content) => {
            _.forEach(content.fileTagTypeSettings, (tagTag) => {
              _.forEach(tagTag.fileTags, (fileTag) => {
                let files = _.isEmpty(content["fileTag" + fileTag.type.id]) ? fileTag.name : `${content["fileTag" + fileTag.type.id]}, ${fileTag.name}`;
                content["fileTag" + fileTag.type.id] = files;
              });
            });
          })
        };

        var rightFilter:ui.IDisplayElementFilterData=$scope.getCurrentFilter();
        var populationJsonUrl = $scope.getCurrentUrl();
        chartResource.getChartData(populationJsonUrl, rightFilter ? rightFilter.filter : null,configuration.max_data_records_per_request,false,false, null,
            function (chartData:Dashboards.DtoCounterReport[], data) {
              if($scope.showTags) {
                updateDocTypeFileTagsForReport(data);
              }
              convertIdToUniqueName(data);
              var rows = saveFilesBuilder.preparSheetRows(fieldsForCsv, data.content);
              var flatSheet =  saveFilesBuilder.createSheet(fieldsForCsv, rows, 'DocTypes flat for csv');
              if(flatSheet) {
                successFunction(flatSheet);
              }
              else {
                dialogs.notify('Notification','Failed to create excel file.');
              }
            }, function () {
              dialogs.notify('Notification','Failed to create excel file.');
            });
      };

      $scope.onTagChanged = function(docTypeId, fileTag) {

        $scope.tagsChanges = $scope.tagsChanges || [];

        let isDiff = diffChanges(fileTag.originalTags, fileTag.tags);
        if(isDiff) {

          let docType = _.filter($scope.tagsChanges, (docType) => {
            return docType.docTypeId == docTypeId;
          })[0];

          if(_.isEmpty(docType)) {
            docType = {
              docTypeId: docTypeId,
            };
            $scope.tagsChanges.push(docType);
          }
          (<any>docType).fileTags = [];
          docType.fileTags.push(fileTag);

        } else {
          removeTagChanges(docTypeId);
        }
      };

      var diffChanges = function(originalTags, tags) {
        let oldIds = _.map(originalTags, (tag) => {
          return tag.id;
        });
        let newIds = _.map(tags, (tag) => {
          return tag.id;
        });
        return !_.isEqual(oldIds.sort(), newIds.sort())
      };

      var removeTagChanges = function(docTypeId) {
        _.remove($scope.tagsChanges, (tag) => {
          return tag.docTypeId == docTypeId
        });
      };

      $scope.openTagsDDL = function(e) {
        if($scope.moveMode) {
          _.defer(() => {
            $('body').click();
          });
        }
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
    })
    .directive('docTypesTreeList', function(){
      return {
        restrict: 'EA',
        template:  '<div  class="doc-types-tree fill-height {{elementName}}" kendo-tree-list ng-transclude k-on-change1="handleSelectionChange(data, dataItem, columns)" ' +
        'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainTreeListOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          saveTagsFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          showAllDocTypes: '=',
          tagTypes: '=',
          showTags: '=',
          showFlags: '=',
          moveMode: '=',
          selectedTagTypeId: '=',
          totalElements: '=',
          tagsChanges: '=',
          changeSelectedItem: '=',
          findId: '=',
          paddingBottom: '=',
          isMngDoctypeConfigUser: '=',
          updateDataItems: '=',
          initialLoadParams: '=',

        },
        controller: 'DocTypesCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }

      }

    })
    .directive('manageDocTypes', function(){
      return {
        restrict: 'EA',
        templateUrl: '/app/docTypes/manageDocTypes.tpl.html',
        replace: true,
        scope:
        {
          docTypeSelectItem: '=',
          editMode: '=',
          showTags: '=',
          changeDocTypeSelectedItemId: '=',
          saveTagsCompleted: '=',
          onSelectedItem: '=',
        },
        controller: 'ManageDocTypesCtrl',
        link: function (scope:any, element, attrs) {
          scope.init();
        }

      }

    })
    .directive('manageDocTypesWithTags', function(){
      return {
        restrict: 'EA',
        templateUrl: '/app/docTypes/manageDocTypesWithTags.tpl.html',
        replace: true,
        scope:
          {
            docTypeSelectItem: '=',
            editMode: '=',
            showTags: '=',
            changeDocTypeSelectedItemId: '=',
            onSelectedItem: '=',
          },
        controller: 'ManageDocTypesCtrl',
        link: function (scope:any, element, attrs) {
          scope.init();
        }

      }

  });

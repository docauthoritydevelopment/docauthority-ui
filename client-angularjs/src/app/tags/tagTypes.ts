///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('tagTypes', [


])
    .controller('ManageTagTypesCtrl', function ($scope, $state,$timeout, $location, Logger, pageTitle,chartResource:IChartsResource,propertiesUtils,routerChangeService,$filter,configuration,
                                                saveFilesBuilder:ISaveFileBuilder,dialogs,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('ManageTagTypesCtrl');

      $scope.init=function()
      {
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      }
      var setUserRoles = function()
      {
        $scope.isMngTagConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngTagConfig]);
      }

      $scope.userFilterChanged = function(userFilterText)
      {
        $timeout(function()
        {
          if(userFilterText&& userFilterText.trim()!='') {
            $scope.filterData ={filter:
            { field: "name", operator: "contains", value: userFilterText}
              ,isClientFilter:true
            };
          }
          else {
            $scope.clearUserFilter();
          }
        },200)

      }

      $scope.clearUserFilter = function()
      {
        $scope.userFilterText=null;
        $scope.filterData={filter:{},isClientFilter:true};
      }

      $scope.onSelectTagTypeItems = function (data) {
        $scope.tagTypeSelectItems = data;
      };
      $scope.onSelectTagTypeItem = function (id,name,reselectId,data) {
        $scope.tagTypeSelectItem = data;
        $scope.onSelectedItem?$scope.onSelectedItem(id,name,reselectId,data):null;
      };

      $scope.onExportTagTypesToExcel= function () {
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess("Tag_types_"+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'tag_types_settings' ,{},{},gotALotOfRecords);
      };



    })
    .controller('TagTypesCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,userSettings,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                          propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,fileTagsResource:IFileTagsResource,dialogs,
                                          chartResource:IChartsResource,saveFilesBuilder:ISaveFileBuilder,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log: ILog = Logger.getInstance('TagTypesCtrl');


      $scope.elementName='tag-types-tree-list';
      $scope.entityDisplayType = EntityDisplayTypes.tag_types;
      var gridOptions:ui.IGridHelper ;

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {

        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        gridOptions  = GridBehaviorHelper.createTreeList<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData, rowTemplate, $stateParams, null,getDataUrlBuilder,true,null);

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });

        gridOptions.gridSettings.change = handleSelectionChange;
        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element,
          true,$scope.paddingBottom,false,createAdditionalExcelSheets);

      };
      var setUserRoles = function()
      {
        $scope.isMngTagConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngTagConfig]);
      }

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,collapsedData) {

        return configuration.allFileTags_url;
      };

      var pathToExpandAndSelect;
      var handleSelectionChange=function(e)
      {
        var treelist = $element.data("kendoTreeList")
        var selectedRows =   treelist.select();
        var selectedDataItems = [];
        for (var i = 0; i < selectedRows.length; i++) {
          var dataItem = treelist.dataItem(selectedRows[i]);
          selectedDataItems.push(dataItem);
        }
        $timeout(function() {
          $scope.$applyAsync(function () {

            $scope.handleSelectionChange(selectedDataItems);
          });
        });
      };


      var getDataUrlParams = function () {
        return null;
      };


      var dTOTagType:Entities.DTOFileTagType =  Entities.DTOFileTagType.Empty();
      var dTOTag:Entities.DTOFileTag =  Entities.DTOFileTag.Empty();

      var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: 'tagId',
        title: 'ID',
        type: EFieldTypes.type_string,
        isPrimaryKey:true,
        displayed: false,
        editable: false,
        nullable: true,
      };
      var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'50%',
       // template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTOTagType, dTOTagType.description)+'" class="fa fa-info" title="#= '+ propertiesUtils.propName(dTOTagType, dTOTagType.description)+' # " ></span>'
      };

      var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        expandable:true,
        width:'50%',
        ddlInside:true,
      };
      var iconTemplate =  '<span ng-if="!isMngTagConfigUser" ><span  class="title-secondary"  title="#= '+ propertiesUtils.propName(dTOTagType, dTOTagType.description)+' # ">'+
      ' #: '+ propertiesUtils.propName(dTOTagType, dTOTagType.name)+' #<span ng-if="dataItem.tagsCount"> (#: tagsCount #)</span>&nbsp;&nbsp;' +
      '<span ng-if="!dataItem.parentId" class="'+configuration.icon_tagType+' tag-#: styleId # "></span>' +
        '<span ng-if="dataItem.parentId"  class="'+configuration.icon_tag+' tag-#: styleId # "></span></span></span>';
      var nameEditDdlTemplate =    '<span class="dropdown-wrapper dropdown" ng-if="isMngTagConfigUser" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
          '<a  class="title-secondary"  title=\'#= '+ propertiesUtils.propName(dTOTagType, dTOTagType.name)+'#  #if ('+propertiesUtils.propName(dTOTagType, dTOTagType.description)+') {#(#='+ propertiesUtils.propName(dTOTagType, dTOTagType.description)+'#)#} #\'>'+
          ' #: '+ propertiesUtils.propName(dTOTagType, dTOTagType.name)+' #<span ng-if="dataItem.tagsCount"> (#: tagsCount #)</span>&nbsp;&nbsp;' +
        '<span ng-if="!dataItem.parentId" class="'+configuration.icon_tagType+' tag-#: styleId # "></span><span ng-if="dataItem.parentId"  class="'+configuration.icon_tag+' tag-#: styleId # "></span><i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          "<li  ng-show='!dataItem.parentId'><a  ng-click='openAddDialog(dataItem)'><span class='fa fa-plus'></span>  Add tag</a></li>"+
          "<li><a  ng-click='openEditDialog(dataItem)'><span class='fa fa-pencil'></span>  Edit</a></li>"+
          "<li><a  ng-click='openDeleteDialog(dataItem)'><span class='fa fa-trash-o'></span>  Delete</a></li>"+
          '  </ul></span>';

      name.template = iconTemplate + nameEditDdlTemplate;



      var tagItemsCount:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.tagItemsCount),
        title: 'tagItemsCount',
        type: EFieldTypes.type_string,
        displayed: false,
        editable: false
      };
      var sensitive:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOTagType, dTOTagType.sensitive),
        title: 'Properties',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        width:'130px',
        nullable: false,
    //    template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTOTagType, dTOTagType.sensitive)+'" class="fa fa-lightbulb-o " style="background: bisque" title="Tag type is sensitive" ></span>'
        template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTOTagType, dTOTagType.sensitive)+'" title="Tag type is sensitive" >Sensitive</span>'
      };
      var singleValueTag:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOTagType, dTOTagType.singleValueTag),
        title: 'Single value type',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        centerText: true,
        width:'130px',
        nullable: false,
        //    template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTOTagType, dTOTagType.sensitive)+'" class="fa fa-lightbulb-o " style="background: bisque" title="Tag type is sensitive" ></span>'
        template:'<span ng-if="dataItem.'+ propertiesUtils.propName(dTOTagType, dTOTagType.singleValueTag)+'" title="Tag type is single value association" ><span class="fa fa-check"></span></span>'
      };


      var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
        fieldName: 'parentId',
        title: 'Parent ID',
        isParentId:true,
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true,

      };





      var fields:ui.IHierarchicalFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(sensitive);
      fields.push(singleValueTag);
      fields.push(description);
      fields.push(parentId);
        fields.push(tagItemsCount);



      var rowTemplate='';

      var onSelectedItemChanged = function () {
        pathToExpandAndSelect = $scope.selectedItem[id.fieldName];
        if( $scope.itemSelected) {
          $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[name.fieldName], $scope.selectedItem[id.fieldName],$scope.selectedItem);
        }
      };

      $scope.parseTagItem = function(tag:Entities.DTOFileTag)
      {
        (<any>tag).tagId = tag.type.id+'.'+tag.id; //inorder to create uniqueID since tagTypes and tags has same starting index
        (<any>tag).originalTagId = tag.id;
        (<any>tag).styleId =splitViewBuilderHelper.getTagStyleId(tag);
        (<any>tag).tagsCount =null;
        tag.description = tag.description?tag.description:'';
        (<any>tag).parentId =tag.type.id;
        (<any>tag).parents =[ tag.type.id, (<any>tag).tagId];
        return (<any>tag).tagId;
      }

      var parseData=function(data:Entities.DTOFileTagTypeAndTags[])
      {
        var parsedData=[];
        data.forEach(d=> {
          var tagType = d.fileTagTypeDto;
          (<any>tagType).styleId =splitViewBuilderHelper.getTagTypeStyleId(tagType);
          (<any>tagType).parentId =null;
          (<any>tagType).parents =[tagType.id];
          (<any>tagType).tagsCount = d.fileTags? d.fileTags.length:0;
          (<any>tagType).tagId = tagType.id;
          tagType.description = tagType.description?tagType.description:'';
           parsedData.push(tagType);
          d.fileTags.forEach(tag=> {
            $scope.parseTagItem(tag);
             parsedData.push( tag)
          });
        });

        parsedData.sort( propertiesUtils.sortAlphbeticFun('name'));

        $scope.totalElements = data?data.length:0;
        return parsedData;
      };

      $scope.openEditDialog = function(dataItem:any) {

        if(!dataItem.type) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagTypesManageUserAction, {
            action: 'openEditTagTypeItemDialog',
            tagType: dataItem,
          });
        }
        else {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagTypesManageUserAction, {
            action: 'openEditTagItemDialog',
            tagItem: dataItem,
          });
        }
      };
      $scope.openAddDialog = function(dataItem:any) {

        if(!dataItem.type) {
            eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagTypesManageUserAction, {
            action: 'openAddTagItemDialog',
            tagItem: dataItem,
          });
        }
      };
      $scope.openDeleteDialog = function(dataItem:any) {

        if(!dataItem.type) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagTypesManageUserAction, {
            action: 'openDeleteTagTypeDialog',
            tagType: dataItem,
          });
        }
        else {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagTypesManageUserAction, {
            action: 'openDeleteTagItemDialog',
            tagItem: dataItem,
          });
        }
      };

      //Name, Description, Sensitive, System, Hidden, InitialTags
      var createAdditionalExcelSheets=function(successFunction)
      {

        var dTOTagType:Entities.DTOFileTagType =  Entities.DTOFileTagType.Empty();
        var dTOTag:Entities.DTOFileTag =  Entities.DTOFileTag.Empty();

        var id:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.id),
          title: 'ID',
          type: EFieldTypes.type_number,
          isPrimaryKey:true,
          displayed: true,
        };
        var name:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.name),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var description:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.description),
          title: 'Description',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var sensitive:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.sensitive),
          title: 'Sensitive',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var singleValueTag:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.singleValueTag),
          title: 'Single value',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var system:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.system),
          title: 'System',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var hidden:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOTagType, dTOTagType.hidden),
          title: 'Hidden',
          type: EFieldTypes.type_string,
          displayed: true,
        };
        var tags:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
          fieldName: 'tagsAsFormattedString',
          title: 'InitialTags',
          type: EFieldTypes.type_string,
          displayed: true,
          nullable: true,
        };

        var fieldsForCsv:ui.IHierarchicalFieldDisplayProperties[] =[];
        fieldsForCsv.push(id);
        fieldsForCsv.push(name);
        fieldsForCsv.push(description);
        fieldsForCsv.push(sensitive);
        fieldsForCsv.push(singleValueTag);
        fieldsForCsv.push(system);
        fieldsForCsv.push(hidden);
        fieldsForCsv.push(tags);


        var convertTagsToList = function(fileTagLists:Entities.DTOFileTagTypeAndTags[]) {
           fileTagLists.forEach(f=>f.fileTagTypeDto["tagsAsFormattedString"] = f.fileTags.map(t=>t.name).join(';'))
        };

        fileTagsResource.getAllTags(
          function (fileTagLists:Entities.DTOFileTagTypeAndTags[]) {
            convertTagsToList(fileTagLists);
            var rows = saveFilesBuilder.preparSheetRows(fieldsForCsv,fileTagLists.map(f=>f.fileTagTypeDto));
            var flatSheet =  saveFilesBuilder.createSheet(fieldsForCsv, rows, 'Tag types flat for csv');
            if(flatSheet) {
              successFunction(flatSheet);
            }
            else {
              dialogs.notify('Notification','Failed to create excel file.');
            }
          }, function () {
            dialogs.notify('Notification','Failed to create excel file.');
          });


      };
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('tagTypesTreeList', function(){
      return {
        restrict: 'EA',
        template:  '<div  class="fill-height {{elementName}}" kendo-tree-list ng-transclude k-on-change1="handleSelectionChange(data, dataItem, columns)" ' +
        'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainTreeListOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          totalElements: '=',
          changeSelectedItem: '=',
          findId: '=',
          paddingBottom: '=',
          parseTagItem: '=',
          initialLoadParams: '=',
        },
        controller: 'TagTypesCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }

      }

    })
    .directive('manageTagTypes', function(){
      return {
        restrict: 'EA',
        templateUrl: '/app/tags/manageTagTypes.tpl.html',
        replace: true,
        scope:
        {
          onSelectedItem: '=',
          editMode: '=',
          changeTagTypeSelectedItem: '=',

        },
        controller: 'ManageTagTypesCtrl',
        link: function (scope:any, element, attrs) {
          scope.init();
        }
      }

    });

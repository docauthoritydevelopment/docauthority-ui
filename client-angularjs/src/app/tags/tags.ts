///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('tags',[

])

    .controller('TagsCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                        propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
      var log: ILog = Logger.getInstance('TagsCtrl');

      var _this = this;

      $scope.elementName='tags-grid';
      $scope.entityDisplayType = EntityDisplayTypes.tags;

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null, true);

        GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);
      //  $scope.mainGridOptions = _this.gridOptions.gridSettings;

      };

      var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOFileTag> =  Entities.DTOAggregationCountItem.Empty();
      var dTOTag:Entities.DTOFileTag =  Entities.DTOFileTag.Empty();
      var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTOTag, dTOTag.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true
      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTOTag, dTOTag.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template: ' #= '+ prefixFieldName + propertiesUtils.propName(dTOTag, dTOTag.name)+' # <span class="tag tag-#: styleId #"> <span class="fa fa-flag"></span></span>'
      };
      var nameNewTemplate =    '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
          '<a  class="title-secondary" title="Tag: #: '+ name.fieldName+' #">#: '+ name.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          "<li><a  ng-click='tagItemClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this tag</a></li>"+
          '  </ul></span>';



      $scope.tagItemClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOFileTag>) {

        setFilterByEntity(item.item.id,item.item.name,$scope.entityDisplayType);
      };
      $scope.tagTypeClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOFileTag>) {

        setFilterByEntity(item.item.type.id,item.item.type.name,EntityDisplayTypes.tag_types);
      };
      var setFilterByEntity = function(gId,gName,entityDisplayType)
      {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doFilterByID',
          id: gId,
          entityDisplayType:entityDisplayType,
          filterName:gName
        });
      };
      var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dTOTag, dTOTag.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false
      };
      var tagType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:   prefixFieldName+'type.'+propertiesUtils.propName(dTOTag, dTOTag.name),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        template:'<span class="title-primary" title="#: '+description.fieldName+' #">#: '+ prefixFieldName +'type.'+ propertiesUtils.propName(dTOTag, dTOTag.name)+' #</span>'
      };

      var  tagTypeTemplate= '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
          '<a  class="title-primary" title="Tag type: #: '+ tagType.fieldName+' #">#: '+ tagType.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          "<li><a  ng-click='tagTypeClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this tag type</a></li>"+
          '  </ul></span>';

      var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
        title: '# files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false
      };



      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(name);
      fields.push(tagType);
      fields.push(numOfFiles);
      fields.push(description);



      var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

      var visibleFileds = fields.filter(f=>f.displayed);
      var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
          "<td colspan='1' class='icon-column'><span class='tag tag-#: styleId # "+configuration.icon_tag+"'></span></td>"+
          "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
          nameNewTemplate+"<div>"+tagTypeTemplate+"</div></td>" +
            //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+
          '<td colspan="1" style="width:220px" >'+filterTemplate+'</td>'+
          "</tr>";

      var parseData = function (tags:Entities.DTOAggregationCountItem< Entities.DTOFileTag>[]) {
        if(tags) {
          tags.forEach(g=>
          {
            g.item.description=g.item.description?g.item.description:'No description';

            (<any>g).styleId = splitViewBuilderHelper.getTagStyleId((<any>g).item?(<any>g).item:g);
            (<any>g).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(g.count , g.count2);

          });

          return tags;
        }
        return null;
      };
      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)][propertiesUtils.propName(dTOTag, dTOTag.id)],
            $scope.selectedItem[name.fieldName]);
      }
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('tagsGrid', function($timeout){
      return {
        restrict: 'EA',
        template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          totalElements: '=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          initialLoadParams: '=',
          changeSelectedItem: '=',
          findId: '=',
        },
        controller: 'TagsCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }

      }

    });

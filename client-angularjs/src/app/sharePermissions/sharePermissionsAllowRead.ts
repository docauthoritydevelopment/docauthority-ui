///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('sharePermissionsAllowRead',[

]);

angular.module('sharePermissionsAllowRead')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sharePermissions', {
        //   abstract: true,
        url: '/sharePermissions',
        templateUrl: '/app/sharePermissions/sharePermissions.tpl.html',
        controller: 'SharePermissionsAllowReadPageCtrl'
      })

  })
  .controller('SharePermissionsAllowReadPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('sharePermissionsPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('SharePermissionAllowReadCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('SharePermissionAllowReadCtrl');

    var _this = this;

    $scope.elementName='share_permissions_allow_read-grid';
    $scope.entityDisplayType = EntityDisplayTypes.share_permissions_allow_read;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOFolderSharePermissionDto>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null, true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };


    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOFolderSharePermissionDto> =  Entities.DTOAggregationCountItem.Empty();
    var dTOFolderSharePermissionDto:Entities.DTOFolderSharePermissionDto =  Entities.DTOFolderSharePermissionDto.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOFolderSharePermissionDto, dTOFolderSharePermissionDto.id),
      title: 'Id',
      type: EFieldTypes.type_string,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderSharePermissionDto, dTOFolderSharePermissionDto.user),
      title: 'User',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,


   };
    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="Share permission : #: item.user #">#: item.user # <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='filterClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this share permission</a></li>"+
        '  </ul></span>';
    $scope.filterClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOFolderSharePermissionDto>) {

      setFilterByEntity(item.item.id,item.item.user);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line' href='sharePermissions' ></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
        nameNewTemplate+"<div></div></td>" +
        "<td colspan='1' style='width:200px' class='ellipsis'>"+filterTemplate+"</td>"+
        "</tr>";


    var parseData = function (sPermissions:Entities.DTOAggregationCountItem< Entities.DTOFolderSharePermissionDto>[]) {
      if(sPermissions) {
        sPermissions.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOFolderSharePermissionDto>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2);
        });
        return sPermissions;
      }
      return null;
    };


    var onSelectedItemChanged = function()
    {
      $scope.itemSelected(Number($scope.selectedItem[id.fieldName]),$scope.selectedItem[name.fieldName]);
    }


    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('sharePermissionsAllowReadGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        entityTypeId: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        totalElements: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        initialLoadParams: '=',
        findId: '=',
      },
      controller: 'SharePermissionAllowReadCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

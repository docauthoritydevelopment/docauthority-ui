///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('settings',[
    'operations.schedules.manage',
    'operations.mediaTypeConnection.manage'
    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.settings, {
            url: '/settings/:activeTab?selectedItem&?selectedScheduleGroup&?selectedCustomerDataCenter&?selectedMediaConnection&?selectedLdap',
            templateUrl: '/app/settings/settingsPage.tpl.html',
            controller: 'SettingsPageCtrl'
          })
    })
    .controller('SettingsPageCtrl', function ($scope,$element,routerChangeService,$state,$stateParams,$compile,$filter,eventCommunicator,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                  Logger:ILogger,propertiesUtils, configuration:IConfig,$timeout,$window,pageTitle,dialogs) {
      var log:ILog = Logger.getInstance('SettingsPageCtrl');
      $scope.ESettingsTabType = ESettingsTabType;

      $scope.tabEnabled={};

      var changeActiveTab = $stateParams.activeTab &&  $scope.tabEnabled[$stateParams.activeTab]!== 'undefined'  ? $stateParams.activeTab : ESettingsTabType.tagTypes;

      $scope.changeSelectedItemId = $stateParams.selectedItem && propertiesUtils.isInteger($stateParams.selectedItem) ? parseInt($stateParams.selectedItem) :null;
      $scope.changeSelectedMediaconnectionId = $stateParams.selectedMediaConnection && propertiesUtils.isInteger($stateParams.selectedMediaConnection) ? parseInt($stateParams.selectedMediaConnection) : null;
      $scope.changeSelectedLdapId = $stateParams.selectedLdap && propertiesUtils.isInteger($stateParams.selectedLdap) ? parseInt($stateParams.selectedLdap) : null;
      $scope.changeSelectedCustomerDataCenterId = $stateParams.selectedCustomerDataCenter && propertiesUtils.isInteger($stateParams.selectedCustomerDataCenter) ? parseInt($stateParams.selectedCustomerDataCenter) : null;


      let listenToScrollChange = ()=> {
        angular.element(document.querySelector('.sidebar-nav')).bind('scroll', function () {
          sessionStorage.setItem('settingScrollTop', '' + document.querySelector('.sidebar-nav').scrollTop);
        });
      }

      $scope.init=function()
      {
        pageTitle.set("Settings");
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        if (sessionStorage.getItem('settingScrollTop') != null) {
          $( ".sidebar-nav" ).scrollTop(Number(sessionStorage.getItem('settingScrollTop')));
          setTimeout(()=>{
            $( ".sidebar-nav" ).scrollTop(Number(sessionStorage.getItem('settingScrollTop')));
            listenToScrollChange();
          });
        }
        else {
          listenToScrollChange();
        }
      };
      var setUserRoles = function()
      {
        $scope.isLoginLicenseExpired =  currentUserDetailsProvider.isLoginLicenseExpired();
        $scope.isViewTagTypesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagTypes]);
        $scope.isViewDocTypeTreeUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeTree]);
        $scope.isScanUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig,Users.ESystemRoleName.RunScans]);
        $scope.isMngUserAndRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUserRoles,Users.ESystemRoleName.MngRoles,Users.ESystemRoleName.MngUsers]);
        $scope.isGeneralAdminUser =  currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.GeneralAdmin,Users.ESystemRoleName.TechSupport]);
        $scope.isViewSearchPatterns = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ManageSearchPatterns]);
        $scope.isViewPatternCategories = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ActivatePatternCategories]);
        $scope.isViewRegulations = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ActivateRegulations]);
        $scope.isTechSupportUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.TechSupport]);
        $scope.isViewDepartmentUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentTree]) && currentUserDetailsProvider.isInstallationModeStandard();
        $scope.isViewMatterUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentTree]) && currentUserDetailsProvider.isInstallationModeLegal();
        $scope.isViewDatePartitionUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.None]);
        $scope.isViewFilterUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.None]);
        $scope.isViewChartUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewReports]);
        $scope.isViewDashboardUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.None]);

        if(currentUserDetailsProvider.isAuthenticated()) { //userInfo may not yet arrived
            updateTabEnabled();
          $scope.setActiveTab(changeActiveTab);
        }
      }

      var updateTabEnabled = function()
      {
        $scope.tabEnabled[ESettingsTabType.docTypes] = $scope.isViewDocTypeTreeUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.tagTypes] = $scope.isViewTagTypesUser && !$scope.isLoginLicenseExpired;
        $scope.tabEnabled[ESettingsTabType.dateFilters] = $scope.isViewDatePartitionUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.searchPatterns] = $scope.isViewSearchPatterns && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.patternCategories] = $scope.isViewPatternCategories && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.regulations] = $scope.isViewRegulations && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.ldapConnections] =($scope.isMngUserAndRolesUser || $scope.isGeneralAdminUser) && !$scope.isLoginLicenseExpired;
        $scope.tabEnabled[ESettingsTabType.licenseKey] = $scope.isGeneralAdminUser;
        $scope.tabEnabled[ESettingsTabType.mailSettings] = $scope.isGeneralAdminUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.customerDataCenters] = $scope.isScanUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.mediaConnections] = $scope.isScanUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.scheduleGroups] = !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.settingsDepartments] = $scope.isViewDepartmentUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.settingsMatters] = $scope.isViewMatterUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.savedFilters] = $scope.isViewFilterUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.savedViews] = $scope.isViewFilterUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.savedDashboards] = $scope.isViewDashboardUser && !$scope.isLoginLicenseExpired ;
        $scope.tabEnabled[ESettingsTabType.savedWidgets] = $scope.isViewDashboardUser && !$scope.isLoginLicenseExpired ;
      };
      var updateTitle = function()
      {
        pageTitle.set('Settings > '+$scope.activeTab);
      };

      $scope.setActiveTab=function(tabName:ESettingsTabType)
      {
        if($scope.isLoginLicenseExpired){
          tabName = ESettingsTabType.licenseKey;
        }

        if(tabName == ESettingsTabType.docTypes) {
          routerChangeService.changeParentUrl('/settings2/DocTypes');
        }
        else if(tabName == ESettingsTabType.dateFilters) {
          routerChangeService.changeParentUrl('/settings2/DateFilters');
        }
        else if(tabName == ESettingsTabType.searchPatterns) {
          routerChangeService.changeParentUrl('/settings2/SearchPatterns');
        }
        else if(tabName == ESettingsTabType.scheduleGroups) {
          routerChangeService.changeParentUrl('/settings2/ScheduleGroups');
        }
        else if(tabName == ESettingsTabType.settingsDepartments) {
          routerChangeService.changeParentUrl('/settings2/Departments');
        }
        else if(tabName == ESettingsTabType.settingsMatters) {
          routerChangeService.changeParentUrl('/settings2/Matters');
        }
        else if(tabName == ESettingsTabType.regulations) {
          routerChangeService.changeParentUrl('/settings2/Regulations');
        }
        else if(tabName == ESettingsTabType.patternCategories) {
          routerChangeService.changeParentUrl('/settings2/PatternCategories');
        }
        else if(tabName == ESettingsTabType.logLevels) {
          routerChangeService.changeParentUrl('/settings2/LogLevels');
        }
        else if(tabName == ESettingsTabType.savedFilters) {
          routerChangeService.changeParentUrl('/settings2/SavedFilters');
        }
        else if(tabName == ESettingsTabType.savedViews) {
          routerChangeService.changeParentUrl('/settings2/SavedViews');
        }
        else if(tabName == ESettingsTabType.savedDashboards) {
          routerChangeService.changeParentUrl('/settings2/SavedDashboards');
        }
        else if(tabName == ESettingsTabType.savedWidgets) {
          routerChangeService.changeParentUrl('/settings2/SavedWidgets');
        }
        else {
          if(tabName == ESettingsTabType.licenseKey) {
            $scope.openLicenseKeyDialog();
            if(!$scope.isLoginLicenseExpired) {
              tabName = ESettingsTabType.tagTypes;
            }
          }
          else if(tabName == ESettingsTabType.mailSettings) {
            $scope.openMailSettingsDialog();
            tabName =ESettingsTabType.tagTypes;
          }

          if( $scope.tabEnabled[tabName] ) {
            $scope.activeTab = tabName;
            $scope.onActiveTabChanged($scope.activeTab);
          }
          else {
            let found = false;
            for (var propt in $scope.tabEnabled) { //find first enabled tab to active it
              if ($scope.tabEnabled[propt] &&
                tabName != ESettingsTabType.licenseKey &&
                tabName != ESettingsTabType.mailSettings) {
                  $scope.setActiveTab(propt);
                  found = true;
                  break;
              }
            }
            if(!found) {
              $scope.activeTab = null;
            }
          }

        //  updateTitle();
        }
      }


      $scope.onActiveTabChanged=function(activeTab)
      {
        if($scope.activeTab == ESettingsTabType.customerDataCenters) {
          $scope.changeSelectedCustomerDataCenterId = $scope.selectedCustomerDataCenter;
        }
        else if($scope.activeTab == ESettingsTabType.mediaConnections) {
          $scope.changeSelectedMediaConnectionId = $scope.selectedMediaConnection;
        }
        else if($scope.activeTab == ESettingsTabType.ldapConnections) {
          $scope.changeSelectedLdapId = $scope.selectedLdap;
        }
        $stateParams['activeTab']=activeTab;
        $stateParams['selectedItem']='';
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

       $scope.onSelectedItem = function(id, name, data) {

         $scope.selectedItem = id;
         if ($scope.activeTab  === ESettingsTabType.customerDataCenters) {
           $scope.selectedCustomerDataCenter = (<Operations.DtoCustomerDataCenterSummaryInfo>data)?
             (<Operations.DtoCustomerDataCenterSummaryInfo>data).customerDataCenterDto:null;
           $stateParams['selectedCustomerDataCenter'] = id;
         }
         else if ($scope.activeTab  === ESettingsTabType.mediaConnections) {
           $stateParams['selectedMediaConnection'] = id;
         }
         else if ($scope.activeTab  === ESettingsTabType.ldapConnections) {
           $stateParams['selectedLdap'] = id;
         }
         else{
           $stateParams['selectedItem'] = id;
         }

         $state.transitionTo($state.current,
           $stateParams,
           {
             notify: false, inherit: true
           });
       }

      $scope.openLicenseKeyDialog=function()
      {

        var dlg = dialogs.create(
          'common/directives/dialogs/licenseKeyDialog.tpl.html',
          'licenseKeyDialogCtrl',
          {licenseAlertMessage: $scope.licenseUserMessage},
          {size: 'md'});

        dlg.result.then(function () {
        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      };

      $scope.openMailSettingsDialog=function()
      {

        var dlg = dialogs.create(
          'common/directives/dialogs/mailSettingsDialog.tpl.html',
          'mailSettingsDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function () {

        }, function (btn) {
        });

      };

      $scope.openLDapSettingsDialog=function()
      {

        var dlg = dialogs.create(
          'common/directives/dialogs/ldapSettingsDialog.tpl.html',
          'ldapSettingsDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function () {
        }, function (btn) {
        });

      };

    });

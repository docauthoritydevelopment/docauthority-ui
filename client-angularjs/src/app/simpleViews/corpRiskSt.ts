///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('simplePages',[
  'directives',
])
.config(function ($stateProvider) {

    $stateProvider
      .state('corpRiskSt1', {
        url: '/analysis/corpRiskSt1',
        templateUrl: '/app/simpleViews/corpRiskSt1.tpl.html',
        controller: 'corpRiskCtrl'
      })

  })
  .controller('corpRiskCtrl', function ($scope,$element, $state, Logger:ILogger,
                                        pageTitle:IPageTitleService, eventCommunicator:IEventCommunicator) {
    var log:ILog = Logger.getInstance('corpRiskCtrl');
    var _this = this;

    var title = 'Corporate Risk Analysis';


    $scope.init = function()
    {
      log.info("Init");
      pageTitle.set(title);
    };


    });

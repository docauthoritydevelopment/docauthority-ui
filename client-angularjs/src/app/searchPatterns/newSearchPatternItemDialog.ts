///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.searchPatterns.items', [

    ])
    .controller('newSearchPatternItemDialogCtrl', function ($scope,  $uibModalInstance,data) {

      $scope.states=[{text:'Active',value:true},{text:'Not active',value:false}];
      var init=function(searchPatternItem:Operations.DTOSearchPattern)
      {
        if(searchPatternItem)
        {
          $scope.editMode=true;
          $scope.name =  searchPatternItem.name;
          $scope.title='Edit search pattern';
          $scope.searchPatternItem = searchPatternItem;
        }
        else
        {
          $scope.editMode=false;
          $scope.title='Add search pattern';
          $scope.searchPatternItem = new Operations.DTOSearchPattern();
          $scope.searchPatternItem.patternType=Operations.ESearchPatternType.REGEX;
          $scope.searchPatternItem.active=true;
        }
        $scope.isPredifined= $scope.searchPatternItem.patternType==Operations.ESearchPatternType.PREDEFINED;
        $scope.initiated=true;
      }
      init(data.searchPatternItem);

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel



      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          $uibModalInstance.close( $scope.searchPatternItem);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


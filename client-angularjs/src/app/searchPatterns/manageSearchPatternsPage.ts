///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('searchPatterns', [
  'searchPatterns.grid',
  'directives.searchPatterns.manage.menu'
]);

angular.module('searchPatterns')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.searchPatternsManagement, {
            parent: 'operations.configuration',
            url: '/searchPatterns?&selectedId?',
            templateUrl: '/app/searchPatterns/manageSearchPatternsPage.tpl.html',
            controller: 'ManageSearchPatternsPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngScanConfig]
            }
          })

    })
    .controller('ManageSearchPatternsPageCtrl', function ($window, $scope, $state, $location, $element, $compile, eventCommunicator:IEventCommunicator, $timeout, scanService:IScanService,
                                                          Logger, propertiesUtils, $stateParams, pageTitle, bizListsResource:IBizListsResource, configuration:IConfig,
                                                          dialogs, bizListResource:IBizListResource, activeScansResource:IActiveScansResource, userSettings:IUserSettingsService, filterFactory:ui.IFilterFactory) {
      var log:ILog = Logger.getInstance('ManageSearchPatternsPageCtrl');

      var activeStateFilterCreator = filterFactory.createBizListItemStateFilter(Entities.DTOSimpleBizListItem.state_ACTIVE);
      $scope.changeShowOnlyActiveItems = false;

      $scope.init = function () {
        pageTitle.set("Manage search patterns ");
        $scope.setShowOnlyActiveItems(true);
        $scope.initiated = true;
      };


      $scope.setShowOnlyActiveItems = function(showOnlyActiveItems)
      {
        $scope.showOnlyActiveItems  = showOnlyActiveItems ;
        if($scope.showOnlyActiveItems ) {
          var filterData = <ui.IDisplayElementFilterData>{filter:activeStateFilterCreator().toKendoFilter(),text:null};
          $scope.filterData = filterData;
        }
        else {
          $scope.filterData =null;
        }
      }

      $scope.onSearchPatternSelectItem = function (id,name,item) {
        $scope.searchPatternSelectedItemId = id;
        $scope.searchPatternSelectedItemName = name;
        $scope.searchPatternSelectedItem = item;
      };
      $scope.onSearchPatternSelectItems = function (data) {
        $scope.searchPatternSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        $scope.exportToInProgress = true;
        log.debug('export '+' to excel');
        $scope.exportToExcel = {fileName:'Search patterns.xlsx'};
      };
      $scope.exportToExcelCompleted=function(notFullRequest:boolean)
      {
        if(notFullRequest) {
          dialogs.notify('Note','Excel file do not contain all data.<br/>Excel file created but records count limit reached.');

        }
        $scope.exportToExcelInProgress = false;
      }


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var onGetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };
      userSettings.getGridViewPreferences(function (data) {
        $scope.changeGridView = (!data) || data.gridView;
        $scope.init();
      });
    })

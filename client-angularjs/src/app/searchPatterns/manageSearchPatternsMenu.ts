///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.searchPatterns.manage.menu', [
       'resources.searchPatterns',
      'directives.dialogs.searchPatterns.items',
      'directives.dialogs.searchPatterns.upload',
    ])
    .controller('searchPatternsMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,searchPatternsResource:ISearchPatternsResource,
                                             userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,bizListResource:IBizListResource,bizListsResource:IBizListsResource) {
      var log:ILog = Logger.getInstance('searchPatternsMenuCtrl');

      $scope.init = function () {

      };


      $scope.openNewItemDialog = function(listId:number)
      {

        var dlg = dialogs.create(
            '/app/searchPatterns/newSearchPatternItemDialog.tpl.html',
            'newSearchPatternItemDialogCtrl',
            {},
            {size:'md'});

        dlg.result.then(function(newItem:Operations.DTOSearchPattern){
          addNewItem(newItem);
        },function(){

        });
      }

      $scope.openUploadItemsDialog = function()
      {

        var dlg = dialogs.create(
            '/app/searchPatterns/uploadSearchPatternItemsDialog.tpl.html',
            'uploadSearchPatternsItemsDialogCtrl',
            {},
            {size:'lg'});

        dlg.result.then(function(data:any){


          updateUploadStatus(data.importId,true);
        },function(){

        });
      }

      $scope.openEditSelectedItemsDialog = function(listItem)
      {
        var item:Operations.DTOSearchPattern=listItem?listItem: $scope.selectedListItemsToOperate[0];
        var dlg = dialogs.create(
            '/app/searchPatterns/newSearchPatternItemDialog.tpl.html',
            'newSearchPatternItemDialogCtrl',
            {searchPatternItem:item},
            {size:'md'});

        dlg.result.then(function(item:Operations.DTOSearchPattern){
          editItem(item);
        },function(){

        });
      }

      $scope.openDeleteSelectedItems = function()
      {
        var items:Operations.DTOSearchPattern[]=$scope.selectedListItemsToOperate;
        if(items&& items.length>0) {
          var userMsg;
          if(items.length==1)
          {
            userMsg="You are about to delete pattern named: '"+items[0].name+"'. Continue?";
          }
          else if(items.length<6){
            userMsg="You are about to delete " + items.length + " patterns. Continue?"+'<br>'+items.map(u=>u.name).join(', ');
          }
          else {
            userMsg="You are about to delete " + items.length + " patterns. Continue?";
          }
          var dlg = dialogs.confirm('Confirmation', userMsg, null);
          dlg.result.then(function (btn) { //user confirmed deletion
            items.forEach(d=>  deleteItem(d));

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }

      }

      var deleteItem = function(item:Operations.DTOSearchPattern)
      {
        log.debug('delete BizListItem: ' + item.name);
        searchPatternsResource.deleteSearchPattern(item.id, function () {
          $scope.reloadData();
        },onError);
      }


      var addNewItem = function (newItem:Operations.DTOSearchPattern) {
        newItem.name =  newItem.name?( newItem.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newItem.name &&  newItem.name.trim() != '') {
          log.debug('add new newItem to list  name: ' +  newItem.name);
          searchPatternsResource.addNewSearchPattern( newItem, function (addedItem) {
            $scope.reloadData(addedItem.id,addedItem.id);
          }, onError);
        }
      }

      var editItem = function (item:Operations.DTOSearchPattern) {

        item.name =  item.name?( item.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( item.name &&  item.name.trim() != '') {
          log.debug('edit BizListItem to list  name: ' +  item.name);
          searchPatternsResource.updateSearchPattern( item, function () {
            $scope.reloadData();
          }, onError);
        }
      }
      $scope.toggleActivateSelectedItems = function()
      {
        if( $scope.firstSelectedListItemToOperate) {
          $scope.activateSelectedItems($scope.toggleToActivateState);

        }
      }
      $scope.$watch(function () { return $scope.firstSelectedListItemToOperate; }, function (value) {
        //rebing grid
        if( $scope.firstSelectedListItemToOperate)
        {
          updateActiveToggleState();
        };
      });
      var updateActiveToggleState=function()
      {
        $scope.toggleToActivateState=!(<Operations.DTOSearchPattern>$scope.firstSelectedListItemToOperate).active;
      }
      $scope.activateSelectedItems = function (activate:boolean) {

        if ( $scope.selectedListItemsToOperate && $scope.selectedListItemsToOperate.length > 0) {

          var userMsg;
          if($scope.selectedListItemsToOperate.length==1)
          {
            userMsg="Are you sure you want to set "+(<Operations.DTOSearchPattern>$scope.selectedListItemsToOperate[0]).name+" search pattern to "+ (activate?'active':'not active')+" ?";
          }
          else if($scope.selectedListItemsToOperate.length<6){
            userMsg= "Are you sure you want to set "+$scope.selectedListItemsToOperate.length+" search patterns to "+ (activate?'active':'not active')+" ?"+
                '<br>'+$scope.selectedListItemsToOperate.map(u=>(<Operations.DTOSearchPattern>u).name).join(', ')
          }
          else {
            userMsg="Are you sure you want to set "+$scope.selectedListItemsToOperate.length+" search patterns to "+ (activate?'active':'not active')+" ?";
          }

          var dlg = dialogs.confirm('Confirmation',userMsg, null);
          dlg.result.then(function (btn) { //user confirmed deletion
            var loopPromises = [];
            $scope.selectedListItemsToOperate.forEach(function (selectedItem:Operations.DTOSearchPattern) {
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              selectedItem.active = activate;
              searchPatternsResource.updateSearchPattern(selectedItem,
                  function (selectedItem:Operations.DTOSearchPattern) {
                    log.debug('receive DTOSearchPattern success for ' + selectedItem.id);
                    deferred.resolve(selectedItem.id);
                  },
                  function (error) {
                    deferred.resolve();
                    log.error('receive activateSelectedItems failure' + error);
                    onError();
                  });
            });
            $q.all(loopPromises).then(function (selectedItemsIdsToUpdate) {
              updateActiveToggleState();
              $scope.reloadData(selectedItemsIdsToUpdate); //no need when auto sync is false
            });

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });

        }
      }

      var updateUploadStatus=function(importId:number,status)
      {
        searchPatternsResource.updateUploadListItemsStatus( importId,status, function () {
          $scope.reloadData();

        },onError);
      }

      var updateActiveState = function (pattern:Operations.DTOSearchPattern,active:boolean) {
        var dlg = dialogs.confirm('Confirmation', "Are you sure you want to set "+pattern.name+" search pattern to "+ (active?'active':'not active')+"?", null);
        dlg.result.then(function (btn) { //user confirmed deletion
          log.debug('update  Active state: '+active );
           searchPatternsResource.updateSearchPatternActiveState( pattern.id,active,function (updatedItem:Operations.DTOSearchPattern) {
            pattern.active= updatedItem.active;
            $scope.refreshDisplay();
          }, onError);

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }


        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridSearchPatternsUserAction,
            $scope,
            (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

              if (fireParam.action === 'openEditItemDialog') {
                $scope.openEditSelectedItemsDialog(fireParam.item);

              }
              else if (fireParam.action === 'setActiveState') {
                updateActiveState(fireParam.item,fireParam.newState);

              }

            })));

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('searchPatternsManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/searchPatterns/manageSearchPatternsMenu.tpl.html',
            controller: 'searchPatternsMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'firstSelectedListItemToOperate':'=',
              'reloadData':'=',
              'gridFindId':'=',
              'refreshDisplay':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    )



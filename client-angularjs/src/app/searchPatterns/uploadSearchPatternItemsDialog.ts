///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.searchPatterns.upload', [

    ])
    .controller('uploadSearchPatternsItemsDialogCtrl', function ($window, $scope, $compile, $uibModalInstance, data, dialogs, searchPatternsResource:ISearchPatternsResource) {

      $scope.fileLoaded=false;

      $scope.resetFile = function () {
        $scope.theFile = null;
        $scope.fileData=null;
        $scope.fileError="";
      };

      $scope.$watch(function () { return $scope.theFile; }, function (value) {
        $scope.importErrorMsg = null;
        if($scope.theFile)
        {
          $scope.fileData=null;
          $scope.fileError="";
          if(_.endsWith($scope.theFile.name.toLowerCase(),'.csv')) {

          }
          else
          {
            $scope.fileError="Only .csv file format is supported";
          }
        }
      });

      $scope.uploadFileToServer = function(){
        $scope.importErrorMsg = null;
        var fd = new FormData();
        //for( var key in $scope.theFile) {
        //  fd.append(key, $scope.theFile[key]);
        //}
        fd.append( "file", $scope.theFile);

        searchPatternsResource.uploadListItemsFromFile(   fd,
            function (importResult:Entities.DTOBizListImportResult) {
              onImportCompleted(importResult);
            },
            function () {
              dialogs.error('Error','Failed to upload file.');
            });


      }

      var loadGridHtml = function()
      {
        $scope.importErrorMsg = null;
        var gridContainerElement =  angular.element('.modal-body .grid');
        gridContainerElement.empty();

        //?baseFilterField=importId&baseFilterValue={importId}
        var html = '<generic-grid style="height:100%" import-mode="true" restrictedEntityDisplayTypeName="'+$scope.bizListType+'"' +
            ' restricted-entity-id="bizListId" import-data-id="importId" item-selected="onSelectItem"  ' +
            '  items-selected="onSelectItems"></generic-grid>';
        var compiledDirective = $compile(html)($scope);
        gridContainerElement.html(compiledDirective);
      }

      var onImportCompleted=function(importResult:Entities.DTOBizListImportResult)
      {
        $scope.importedRowCount = importResult.importedRowCount - importResult.duplicateRowCount;
        $scope.duplicateRowCount = importResult.duplicateRowCount;
        $scope.errorsRowCount = importResult.errorsRowCount;
        $scope.importId = importResult.importId;
        $scope.fileLoaded=true;
        if($scope.importId) {
          loadGridHtml();
        }
        else {
          $scope.importErrorMsg = importResult.resultDescription;
        }
      }


      $scope.cancel = function(){
        if($scope.importId)
        {
          searchPatternsResource.cancelUploadListItemsFromFile(  $scope.importId);

        }
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $uibModalInstance.close( {importId:$scope.importId} );
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('searchPatterns.grid',[

    ])
    .controller('searchPatternsGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
      var log:ILog = Logger.getInstance('searchPatternsGridCtrl');


      $scope.elementName = 'search-patterns-grid';
      var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData,  rowTemplate,
            $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        var doNotSelectFirstRowByDefault = false;

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,
            doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

      };

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
        return configuration.searchPattern_list_url;
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dTOSearchPattern:Operations.DTOSearchPattern =  Operations.DTOSearchPattern.Empty();

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
            '</a> #: '+propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.name)+'#',
      };
      var nameTemplate =  '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<span class="title-primary">' +
          '<a title="{{dataItem.'+ name.fieldName+'}}" ng-class="{\'label-running\':dataItem.isRunning}" >#= '+name.fieldName+' # <i class="caret ng-hide"></i></a> ' +
          '<a ng-show="dataItem.isRunning" class="fa fa-cog fa-spin " ><i class="caret ng-hide"></i></a>'+
          '</span></span>'+
          '<ul class="dropdown-menu"  > '+
          '<li><a   ng-click="openEditDialog(dataItem)"><span class="fa fa-pencil"></span> Edit</a></li>'+
          ' </ul></span> ';

      var patternType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.patternType),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span translate="{{\'SearchPatterns_\'+'+'dataItem.'+propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.patternType)+'}}"></span>'
      };
      var pattern:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.pattern),
        title: 'Pattern',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.pattern)+' #'
      };
      var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.description)+' #'
      };

      var activeTemplate =splitViewBuilderHelper.getToggleButtonTemplate(propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.active),'Use in rescan',"toggleActiveState(dataItem)", undefined, "Search pattern");

      var active:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.active),
        title: 'Active',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: true,
        width:'100px',
        //template: '<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
        //'</a> <span ng-show="dataItem.'+propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.active)+'"  title="Patterns is active for next rescan"><span class="fa fa-plus ng-hide"></span> On</span>' +
        //'<span ng-show="!dataItem.'+propertiesUtils.propName(dTOSearchPattern, dTOSearchPattern.active)+'"  title="Patterns is not active for next rescan"><span class="fa fa-plus ng-hide"></span> Off</span>'
        template:activeTemplate
      };


      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(description);
      fields.push(active);
      fields.push(patternType);
      fields.push(pattern);
      $scope.fields =fields;


      var userActiveTemplate =splitViewBuilderHelper.getToggleButtonTemplate(active.fieldName,'active',"toggleActiveState(dataItem)", undefined, "Search pattern");
      var typeTemplate = splitViewBuilderHelper.getCountTemplate(patternType.fieldName,'Type:',null,'\'SearchPatterns_\'+');

      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
          "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span  class='"+configuration.icon_searchPattern+" '></span></td>"+
          "<td colspan='1'  style='width: 40%;word-wrap:break-word' class='ddl-cell'>"+
          nameTemplate+"<div class='title-third'>"+description.template+"</div></td>"  +


          "<td colspan='1' width='40px'>"+userActiveTemplate+"</td>"+
          "<td colspan='1' width='180px'>"+typeTemplate+"</td>"+
          "<td colspan='"+(visibleFileds.length-3)+"' width='100%'>"+pattern.template+"</td>"+

          "</tr>";


      var parseData = function (schedule: Operations.DTOSearchPattern[]) {
        if(schedule)
        {
          schedule.forEach((sItem:Operations.DTOSearchPattern)=> {
            if (!sItem.description ) {
              sItem.description = '';
            }
          });
        }
        return schedule;
      }


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridSearchPatternsUserAction, {
          action: 'openEditItemDialog',
          item: dataItem,
        });
      };
      $scope.toggleActiveState = function(dataItem:Operations.DTOSearchPattern) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridSearchPatternsUserAction, {
          action: 'setActiveState',
          item: dataItem,
          newState: !dataItem.active,
        });
      };



      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('searchPatternsGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          initialLoadParams: '=',
          findId: '=',

          fixHeight: '@',

        },
        controller: 'searchPatternsGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

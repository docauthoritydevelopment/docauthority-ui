///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('owners',[

]);

angular.module('owners')
  .config(function ($stateProvider) {
    $stateProvider
      .state('owners', {
        //   abstract: true,
        url: '/owners',
        templateUrl: '/app/groups/owners.tpl.html',
        controller: 'OwnersPageCtrl'
      })

  })
  .controller('OwnersPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('OwnersPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('OwnersCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('OwnersCtrl');

    var _this = this;

    $scope.elementName='owners-grid';
    $scope.entityDisplayType = EntityDisplayTypes.owners;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null, true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOOwner> =  Entities.DTOAggregationCountItem.Empty();
    var dTOOwner:Entities.DTOOwner =  Entities.DTOOwner.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOOwner, dTOOwner.user),
      title: 'Id',
      type: EFieldTypes.type_string,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'userDisplayedName',
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,


   };
    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="Owner: #: '+ name.fieldName+' #">#: '+ name.fieldName+' # <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this owner</a></li>"+
        '  </ul></span>';
    $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOOwner>) {

      setFilterByEntity(item.item.user,item.item.user);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(name);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line' href='owner' ></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
        nameNewTemplate+"<div></div></td>" +
          //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

        '<td colspan="1" style="width:220px" >'+filterTemplate+'</td>'+
        "</tr>";


    var parseData = function (owners:Entities.DTOAggregationCountItem< Entities.DTOOwner>[]) {
      if(owners) {
        owners.forEach(g=>
        {
          var userName = g.item.user;
          (<any>g).userDisplayedName =  userName.replace(/"/g,'');
          (<any>g).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(g.count , g.count2)
        });

        return owners;
      }
      return null;
    };
    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[name.fieldName],$scope.selectedItem[name.fieldName]);
    }
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('ownersGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        totalElements: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
        initialLoadParams: '=',
      },
      controller: 'OwnersCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

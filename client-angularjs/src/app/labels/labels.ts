///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('labels',[

]);

angular.module('labels')
  .config(function ($stateProvider) {
    $stateProvider
      .state('labels', {
        //   abstract: true,
        url: '/labels',
        templateUrl: '/app/labels/labels.tpl.html',
        controller: 'LabelsPageCtrl'
      })

  })
  .controller('LabelsPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('LabelsPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('LabelsCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('LabelsCtrl');

    var _this = this;

    $scope.elementName='labels-grid';
    $scope.entityDisplayType = EntityDisplayTypes.labels;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOLabel> =  Entities.DTOAggregationCountItem.Empty();
    var dTOLabel:Entities.DTOLabel =  Entities.DTOLabel.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOLabel, dTOLabel.id),
      title: 'Id',
      type: EFieldTypes.type_string,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOLabel, dTOLabel.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,


   };
    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="Label: #: item.name #">#: item.name # <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='filterClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this label</a></li>"+
        '  </ul></span>';
    $scope.filterClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOLabel>) {

      setFilterByEntity(item.item.id,item.item.name);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line' href='labels' ></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
        nameNewTemplate+"<div></div></td>" +
        "<td colspan='1' style='width:200px' class='ellipsis'>"+filterTemplate+"</td>"+
        "</tr>";


    var parseData = function (labels:Entities.DTOAggregationCountItem< Entities.DTOLabel>[]) {
      if(labels) {
        labels.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOLabel>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2);
        });
        return labels;
      }
      return null;
      };


    var onSelectedItemChanged = function()
    {
         $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName]);
    }


    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('labelsGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        entityTypeId: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
      },
      controller: 'LabelsCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('bizLists.bizListItems.manage', [

    ])
    .controller('BizListItemsManageCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,userSettings:IUserSettingsService,
                                                propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log: ILog = Logger.getInstance('BizListItemsManageCtrl');

      var _this = this;
      $scope.elementName='biz-list-grid';
      $scope.entityDisplayType = EntityDisplayTypes.bizlistItems_clients;
      var gridOptions:ui.IGridHelper ;

      $scope.init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData,  rowTemplate,
            $stateParams, null,$scope.importMode?getDataUrlBuilderForImport: getDataUrlBuilder,true,'312px');

        var doNotSelectFirstRowByDefault = true;

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,$scope.paddingBottom,null);

      };

      var parseData = function (data:Entities.DTOSimpleBizListItem[]) {
        if(data) {
          data.map(function (listItem:Entities.DTOSimpleBizListItem) {
            (<any>listItem).isActive = listItem.state==Entities.DTOSimpleBizListItem.state_ACTIVE;
            (<any>listItem).isPending = listItem.state==Entities.DTOSimpleBizListItem.state_PENDING;
            var aliasesList = listItem.entityAliases ? listItem.entityAliases.map(t => t.name).join(', ') : '';
            (<any>listItem).entityAliasesAsFormattedString = aliasesList;
          });

        }
        return data;
      };

      var getDataUrlBuilderForImport =function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {

          var url = configuration.bizList_items_import_url.replace('{importId}',$scope.importDataId);
          return url.replace(':id',restrictedEntityId);

      };

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {

        if(restrictedEntityId )
        {
          return configuration.bizList_items_list_url.replace(':id',restrictedEntityId);
        }
      };

      var getDataUrlParams = function () {
        return null;
      };


        var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOSimpleBizListItem> = Entities.DTOAggregationCountItem.Empty();
        var dTOSimpleBizListItem:Entities.DTOSimpleBizListItem = Entities.DTOSimpleBizListItem.Empty();
        var prefixFieldName = '';

        var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName + propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.id),
          title: 'ID',
          type: EFieldTypes.type_string,
          isPrimaryKey: true,
          displayed: false,
          editable: false,
          nullable: true,
        };

        var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName + propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.name),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,

          template: '#if ( name ) {#' +
          '<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
          '</a> #: name# #}#',
          width: "20%",
        };

      var nameTemplate =  '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<a class="title-primary" title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #<i class="caret ng-hide"></i></a> ' +
          '</span>'+
          '<ul class="dropdown-menu"  > '+
          '<li><a   ng-click="openEditDialog(dataItem)"><span class="fa fa-pencil"></span> Edit</a></li>'+
          ' </ul></span> ';

        var nameNoEdit:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName + propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.name),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,
          width: "20%",
        };


        $scope.openEditDialog = function (item:Entities.DTOSimpleBizListItem) {

          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridManageBizListUserAction, {
            action: 'editBizListItem',
            item: item,
          });
        };
        var state:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName + propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.state),
          title: 'State',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,
          width: '110px',
          template: '  <span ng-show="dataItem.isActive" style="white-space: pre;"><span class="fa fa-check label-primary"></span>&nbsp;Active</span>' +
          '<span ng-show="dataItem.isPending"> Pending </span>'
        };
      $scope.toggleActiveState = function(dataItem:Entities.DTOSimpleBizListItem) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridManageBizListUserAction, {
          action: 'setActiveState',
          item: dataItem,
          newState: dataItem.state == Entities.DTOSimpleBizListItem.state_ACTIVE?Entities.DTOSimpleBizListItem.state_PENDING:Entities.DTOSimpleBizListItem.state_ACTIVE,
        });
      };

      var aliases:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: 'entityAliasesAsFormattedString',  //for excel download
          title: 'Aliases',
          exportTitle:"ALIASES",
          type: EFieldTypes.type_other,
          displayed: true,
          editable: false,
          nullable: true,
          width: "20%",
          template: '#if (  entityAliases && entityAliases[0]) {# <div > # for (var i=0; i < entityAliases.length; i++) { # ' +
          ' <div style="white-space: nowrap;" title="#: entityAliases[i].name #"> #: entityAliases[i].name #</div># } # </div># } #'
        };
        var aliasesTemplate = '<div title="aliases"> #: entityAliasesAsFormattedString # </div>';

        var bisunessId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName + propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.businessId),
          title: 'Business Id',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,

        };

        var address:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: 'Address',
          title: 'Address',
          exportTitle:"ADDRESS",
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,

        };

        var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
          title: '# direct filtered  files',
          type: EFieldTypes.type_number,
          displayed: true,
          editable: false,
          nullable: false
        };

        var numOfAllFilteredFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2),
          title: '# all filtered files',
          type: EFieldTypes.type_number,
          displayed: true,
          editable: false,
          nullable: false
        };


        var fields:ui.IFieldDisplayProperties[] = [];
        if( $scope.restrictedEntityDisplayTypeName == Entities.DTOBizList.itemType_CONSUMERS)
        {
          fields.push(id);
          fields.push(nameNoEdit);
      //    fields.push(state);
          fields.push(bisunessId);
        }
        else
        {
          fields.push(id);
          fields.push(name);
          fields.push(aliases);
          fields.push(state);
          fields.push(bisunessId);
        }

        //fields.push(numOfFiles);
        //fields.push(numOfAllFilteredFiles);


      var userStateTemplate =$scope.editMode? splitViewBuilderHelper.getToggleButtonTemplate('isActive','state',"toggleActiveState(dataItem)"):
                          splitViewBuilderHelper.getCountTemplate(state.fieldName,'state:',null);
      var bisunessIdTemplate = splitViewBuilderHelper.getCountTemplate(bisunessId.fieldName,'Business Id:',null);
      var iconTemplate = "<span class='tag tag-{{dataItem."+ prefixFieldName+propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.bizListId)+"}} {{bizListTypeIcon(dataItem."+ prefixFieldName+propertiesUtils.propName(dTOSimpleBizListItem, dTOSimpleBizListItem.type)+")}}'></span>";

      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
          "<td colspan='1' class='icon-column' style='padding-top: 1px;'>"+iconTemplate+" </span></td>"+
          "<td colspan='1'  style='width: 40%;word-wrap:break-word' class='ddl-cell'>"+
          nameTemplate+"<div class='title-third'></div></td>"  +
          ($scope.importMode?"":"<td colspan='1' width='40px'>"+userStateTemplate+"</td>")+
          "<td colspan='1' width='220px'>"+bisunessIdTemplate+"</td>"+
          "<td colspan='"+(visibleFileds.length-3)+"' width='100%'>"+aliasesTemplate+"</td>"+

          "</tr>";

      $scope.bizListTypeIcon= function(listTypeName)
      {
        return splitViewBuilderHelper.bizListTypeIcon(listTypeName);
      }
      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName]);
      }
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
    })

    .directive('bizListItemsManageGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          totalElements: '=',
          importDataId: '=',
          importMode: '=',
          editMode: '=',
          paddingBottom: '=',
        },
        controller: 'BizListItemsManageCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }

      }

    })

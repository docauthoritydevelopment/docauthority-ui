///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.bizListUpload', [

    ])
    .controller('uploadBizListItemsDialogCtrl', function ($window, $scope, $compile, $uibModalInstance, data, dialogs, bizListResource:IBizListResource) {

      $scope.fileLoaded=false;
      $scope.changeGridView=data.changeGridView;
      $scope.bizListId = data.bizListId;
      $scope.bizListName = data.bizListName;
      $scope.bizListType = data.bizListType;
      $scope.foldersMode = data.mode=='fromFolder'?true:false;
      $scope.level=1;

      $scope.resetFile = function () {
        $scope.theFile = null;
        $scope.fileData=null;
        $scope.fileError="";
      };

      $scope.$watch(function () { return $scope.theFile; }, function (value) {
        $scope.importErrorMsg = null;
        if($scope.theFile)
        {
          $scope.fileData=null;
          $scope.fileError="";
          if(_.endsWith($scope.theFile.name.toLowerCase(),'.csv')) {
            $scope.uploadFileBrowser();
          }
          else
          {
            $scope.fileError="Only .csv file format is supported";
          }
        }
      });
      $scope.isConsumerBizList = function()
      {
        return $scope.bizListType==Entities.DTOBizList.itemType_CONSUMERS;
      }
      $scope.uploadFileBrowser = function(){
        var file =$scope.theFile;
        //var fileReader = new FileReader();
        //fileReader.onloadend = function(e){
        //  $scope.$apply(function () {
        //    $scope.fileData = (<any> e.target).result;
        //
        //  });

      //  }
        //var start = parseInt(opt_startByte) || 0;
        //var stop = parseInt(opt_stopByte) || file.size - 1;
        //fileReader.onloadend = function(evt) {
        //  if (evt.target.readyState == FileReader.DONE) { // DONE == 2
        //    document.getElementById('byte_content').textContent = evt.target.result;
        //    document.getElementById('byte_range').textContent =
        //        ['Read bytes: ', start + 1, ' - ', stop + 1,
        //          ' of ', file.size, ' byte file'].join('');
        //  }
        //};

        //var blob = file.slice(start, stop + 1);
         //fileReader.readAsBinaryString(blob);
     //   fileReader.readAsBinaryString(file);
      }


      var loadGridHtml = function()
      {
        $scope.importErrorMsg = null;
        var gridContainerElement =  angular.element('.modal-body .grid');
        gridContainerElement.empty();

        //?baseFilterField=importId&baseFilterValue={importId}
        var html = '<biz-list-items-manage-grid style="height:100%" import-mode="true" restrictedEntityDisplayTypeName="'+$scope.bizListType+'"' +
            ' restricted-entity-id="bizListId"  import-data-id="importId" item-selected="onSelectItem"  ' +
            '  items-selected="onSelectItems" template-view="!changeGridView"></biz-list-items-manage-grid>';
        var compiledDirective = $compile(html)($scope);
        gridContainerElement.html(compiledDirective);
      }

      $scope.createListItemsFromFolderNames = function()
      {
        $scope.importErrorMsg = null;
        bizListResource.createListItemsFromFolderNames(  $scope.bizListId, $scope.folderPath,$scope.level,
            function (importResult:Entities.DTOBizListImportResult) {
              onImportCompleted(importResult);
            },
            function () {
              $scope.importErrorMsg = 'Incorrect Path. Failed to process request';
            //  dialogs.error('Error','Failed to process request.');
            });
      }

      var onImportCompleted=function(importResult:Entities.DTOBizListImportResult)
      {
        $scope.importedRowCount = importResult.importedRowCount - importResult.duplicateRowCount;
        $scope.duplicateRowCount = importResult.duplicateRowCount;
        $scope.errorsRowCount = importResult.errorsRowCount;
        $scope.importId = importResult.importId;
        $scope.fileLoaded=true;
        if($scope.importId) {
          loadGridHtml();
        }
        else {
          $scope.importErrorMsg = importResult.resultDescription;
        }
      }
      $scope.uploadFileToServer = function(){
        $scope.importErrorMsg = null;
        var fd = new FormData();
        //for( var key in $scope.theFile) {
        //  fd.append(key, $scope.theFile[key]);
        //}
        fd.append( "file", $scope.theFile);

        bizListResource.uploadListItemsFromFile(  $scope.bizListId, fd,
            function (importResult:Entities.DTOBizListImportResult) {
              onImportCompleted(importResult);
            },
            function () {
              dialogs.error('Error','Failed to upload file.');
        });


      }

      $scope.cancel = function(){
        if($scope.importId)
        {
          bizListResource.cancelUploadListItemsFromFile(  $scope.bizListId, $scope.importId);

        }
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(approveImport:boolean){
        $uibModalInstance.close( {importId:$scope.importId,approveImport:approveImport} );
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


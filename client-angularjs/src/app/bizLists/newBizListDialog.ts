///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.bizLists', [
      'directives.dialogs.bizLists.items'
    ])
    .controller('newBizListDialogCtrl', function ($scope,  $uibModalInstance,data,bizListsResource:IBizListsResource) {


      $scope.bizListTypes = data.bizListTypes;
      var init=function(bizList:Entities.DTOBizList)
      {
        if(bizList)
        {
          $scope.editMode=true;
          $scope.name =  bizList.name;
          $scope.title='Edit list ';
          $scope.id=bizList.id;
          $scope.description = bizList.description;
          $scope.bizListType =  bizList.bizListItemType;
          $scope.bizLisTemplateType =  bizList.template;
          $scope.active =  bizList.active;
        }
        else
        {
          $scope.editMode=false;
          $scope.active=true;
          $scope.title='Add list';
          $scope.bizListType = data.bizListTypes.length>1?null: data.bizListTypes[0];

          loadBizListTemplateTypes();
        }
        $scope.initiated=true;
      }
      var  loadBizListTemplateTypes=function() {
        bizListsResource.getBizListConsumerTemplateTypes(function (templateTypes:Entities.DTOBizListTemplateType[]) {
               $scope.bizListTemplateTypes=templateTypes;
              $scope.bizLisTemplateType = data.bizListTemplateTypes&&data.bizListTemplateTypes.length>1?null: data.bizListTemplateTypes?data.bizListTemplateTypes[0]:null;
            },
            function (error) {

            });
      }
      init(data.bizList);

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.isConsumerBizList = function()
      {
        return $scope.bizListType==Entities.DTOBizList.itemType_CONSUMERS;
      }

      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var list = new Entities.DTOBizList();
          list.bizListItemType = $scope.bizListType;
          list.name=$scope.name;
          list.id =$scope.id;
          list.description =$scope.description;
          list.active =$scope.active;
          list.template =$scope.bizListTemplateType?$scope.bizListTemplateType.name:null;
          $uibModalInstance.close(list);
        }


      };

      $scope.onBizListTypeChange=function(bizListType)
      {
        $scope.bizListType=bizListType;
      }

      $scope.onBizListTemplateTypeChange=function(bizListTemplateType)
      {
        $scope.bizListTemplateType=bizListTemplateType;
      }

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


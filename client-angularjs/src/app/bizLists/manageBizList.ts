///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('bizLists.manage', [
  'bizLists.bizListItems.manage',
  'resources.bizLists',
  'directives.bizLists.manage',
  'bizLists.bizListExtractions',
  'bizLists.bizListExtractions.rules',
  'bizLists.bizListItems',
])
    .controller('BizListManageCtrl', function ($scope, $state, $location,$element,$compile,eventCommunicator:IEventCommunicator,$timeout,filterFactory:ui.IFilterFactory,routerChangeService,$filter,
                                                   Logger,propertiesUtils,$stateParams, pageTitle,bizListsResource:IBizListsResource,configuration,
                                                   dialogs,bizListResource:IBizListResource,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log:ILog = Logger.getInstance('BizListManageCtrl');

      var activeStateFilterCreator = filterFactory.createBizListItemStateFilter(Entities.DTOSimpleBizListItem.state_ACTIVE);

      $scope.init = function () {
        pageTitle.set("Scan > Business lists settings");
        $scope.listItemsLazyFirstLoad = true;
        $scope.setShowOnlyActiveItems(true);
      };


      $scope.setShowOnlyActiveItems = function(showOnlyActiveItems)
      {
        $scope.showOnlyActiveItems  = showOnlyActiveItems ;
        if($scope.showOnlyActiveItems ) {
         var filterData = <ui.IDisplayElementFilterData>{filter:activeStateFilterCreator().toKendoFilter(),text:null};
          $scope.filterData = filterData;
        }
        else {
          $scope.filterData =null;
        }
      }


      $scope.reloadGridData = function()
      {
        $scope.reloadDataListItems();
      }
      $scope.onSelectItem = function (id,name) {
        $scope.bizListSelectedItemId = id;
        $scope.bizListSelectedItemName = name;
      };
      $scope.onSelectItems = function (data) {
        $scope.bizListSelectedItems = data;
      };
      $scope.isConsumerBizList = function()
      {
        return $scope.bizListType==Entities.DTOBizList.itemType_CONSUMERS;
      }
      $scope.$watch(function () { return $scope.bizList; }, function (value) {
        $scope.bizListId = $scope.bizList? $scope.bizList.id:null;
        loadGridHtml();
      });

      $scope.$watch(function () { return $scope.changeBizListSelectedItem; }, function (value) {
        $scope.changeBizListSelectedItemAtGrid = $scope.changeBizListSelectedItem;
      });

      $scope.$watch(function () { return $scope.gridFindId; }, function (value) {
          $scope.gridFindId11 = $scope.gridFindId;
      });


      $scope.$watch(function () { return $scope.changeShowOnlyActiveItems; }, function (value) {
        $scope.setShowOnlyActiveItems($scope.changeShowOnlyActiveItems);
      });
      var loadGridHtml = function()
      {
        var gridContainerElement =  $element.find('.grid');
        gridContainerElement.empty();
        $scope.totalListItemElements = null;
        $scope.selectItems = null;
        var html = '<biz-list-items-manage-grid style="height:100%" restrictedEntityDisplayTypeName="'+$scope.bizListType+ '"  filter-data="filterData" find-id="gridFindId11" template-view="templateView"' +
            'restricted-entity-id="bizListId" item-selected="onSelectItem"  refresh-data="reloadDataListItems" edit-mode="editMode" change-selected-item="changeBizListSelectedItemAtGrid" reload-data-to-grid="reloadDataToBizListGrid" ' +
            'items-selected="onSelectItems" export-to-excel="exportToExcel" padding-bottom="paddingBottom" lazy-first-load="listItemsLazyFirstLoad" total-elements="totalListItemElements"></biz-list-items-manage-grid>';

        var compiledDirective = $compile(html)($scope);
        gridContainerElement.html(compiledDirective);
      }


      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        let fileName:string = _.replace($scope.bizList.name,new RegExp(" ","g"),"_")+"_";
        routerChangeService.addFileToProcess(fileName+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'biz_list_settings' ,{
          bizListId : $scope.bizList.id
        },{},gotALotOfRecords);
      };

      $scope.approveAllListItems = function()
      {

        var dlg:any = dialogs.confirm('Confirm approve all', 'Are you sure you want to approve all items imported to this list?', null);
        dlg.result.then(function(btn){ //user confirmed deletion
          bizListResource.updateAllListItemsStatus(
              $scope.bizList.id,Entities.DTOSimpleBizListItem.state_PENDING,Entities.DTOSimpleBizListItem.state_ACTIVE,
              function () {
                $scope.reloadGridData();
              }
              , onError);

        },function(btn){

        });
      };

      $scope.bizListTypeIcon=function(listType)
      {
       return splitViewBuilderHelper.bizListTypeIcon(listType);
      }

      $scope.openUploadBizListItemsDialog = function(item) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizlistUploadAction, {
          bizList: $scope.bizList,
        });
      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizListApproveAllPendingAction, $scope, (new RegisteredEvent(function (registrarScope, registrationParam, fireParam) {
        if(fireParam.bizList) {
          $scope.bizList.id = fireParam.bizList.id;
          $scope.approveAllListItems();
        }
      })));
    })
    .directive('bizListManage',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/bizLists/manageBizList.tpl.html',
            controller: 'BizListManageCtrl',
            replace:true,
            scope:{
              'bizListType':'=',
              'bizList':'=',
              'totalListItemElements':'=',
              'editMode':'=',
              'templateView':'=',
              'bizListSelectedItemId': '=',
              'bizListSelectedItemName': '=',
              'changeBizListSelectedItem': '=',
              'changeShowOnlyActiveItems': '=',
              'reloadDataToBizListGrid': '=',
              'paddingBottom': '=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(true);
            }
          };
        })


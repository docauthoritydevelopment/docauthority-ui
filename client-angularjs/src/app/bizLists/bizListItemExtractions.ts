///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('bizLists.bizListExtractions', [

    ])

    .controller('BizListItemExtractionsCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,userSettings:IUserSettingsService,eventCommunicator,
                                                        bizListsResource:IBizListsResource, propertiesUtils, configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log: ILog = Logger.getInstance('BizListItemExtractionsCtrl');

      var _this = this;
      $scope.elementName='bizlist-item-clients-extractions-grid';
      $scope.entityDisplayType = EntityDisplayTypes.bizlistItem_clients_extractions;

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

    //     $scope.mainGridOptions = _this.gridOptions.gridSettings;
        loadBizListList();
      };

      var bizListsDic={};
      var loadBizListList = function() {
        bizListsResource.getAllBizLists($scope.bizListType, function (listsByIdDic:any) {
              bizListsDic = listsByIdDic;
              _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate,null,null,getDataUrlBuilder, true);

              GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

            },
            function (error) {
            }
        );
      }
      var getDataUrlBuilder = function(restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration,includeSubEntityData,fullyContained:EFullyContainedFilterState,collapsedData) {

          var url:string = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName, restrictedEntityId, entityDisplayType, configuration, includeSubEntityData,fullyContained,collapsedData);
        if($scope.entityDisplayType==EntityDisplayTypes.bizlistItem_clients_extractions) {
          return url.replace(':bizListType', Entities.DTOBizList.itemType_CLIENTS);
        }
        return null;
      }
      var parseData = function (listItems:Entities.DTOAggregationCountItem< Entities.DTOSimpleBizListItem>[]) {

        if(listItems) {
          listItems.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOSimpleBizListItem>) {
            (<any>listItem).bizListName = bizListsDic[listItem.item.bizListId].name;
            (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2 );
          });
          return listItems;
        }
        return null;
      };


      var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOSimpleBizListItem> =  Entities.DTOAggregationCountItem.Empty();
      var dtoBizListItem:Entities.DTOSimpleBizListItem =  Entities.DTOSimpleBizListItem.Empty();
      var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true
      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template: ' #= item.name # <span class="tag-#: item.state #"> <span class="fa fa-paperclip"></span></span>'
      };
      var nameNewTemplate =    '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
          '<a  class="title-secondary" title="Extraction: #: '+ name.fieldName+' #">#: '+ name.fieldName+' #<i class="caret ng-hide"></i> </a></span>'+
          '<ul class="dropdown-menu"  > '+
          "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this extraction</a></li>"+
          '  </ul></span>';
      $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOSimpleBizListItem>) {

        setFilterByEntity(item.item.id,item.item.name);
      };
      var setFilterByEntity = function(gId,gName)
      {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doFilterByID',
          id: gId,
          entityDisplayType:$scope.entityDisplayType,
          filterName:gName
        });
      };
      var aliases:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:   prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.entityAliases),
        title: 'aliases',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        template:"# if (  "+prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.entityAliases)+" && "+prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.entityAliases)+"[0]) {#" +
        ' # for (var i=0; i < '+prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.entityAliases)+'.length; i++) { # ' +
        "  <span title='Ailases' class='title-third'>#: "+prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.entityAliases)+"[i].name #</span> # } #  # } #"
      };
      var bizListName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:   'bizListName',
        title: 'list',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        template:'<span class="title-primary" title="Business list name">#: bizListName #</span>'
      };

      var bizListType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:   prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.type),
        title: 'type',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false
      };
      var state:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:   prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.state),
        title: 'state',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        template:'<span ng-if="!isActiveState(dataItem)" translate="{{dataItem.'+prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.state)+'}}"></span>'
      };

      $scope.isActiveState=function(dataItem:Entities.DTOAggregationCountItem<Entities.DTOSimpleBizListItem>)
      {
        return dataItem.item.state==Entities.DTOSimpleBizListItem.state_ACTIVE;
      }

      var numOfFilteredFiles :ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
        title: '# filtered files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
      };


      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(name);
      fields.push(bizListType);
      fields.push(bizListName);
      fields.push(numOfFilteredFiles);


      var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

      var visibleFileds = fields.filter(f=>f.displayed);
      var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
          "<td colspan='1' class='icon-column'><span class='tag tag-{{dataItem."+ prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.bizListId)+"}} {{bizListTypeIcon(dataItem."+ prefixFieldName+propertiesUtils.propName(dtoBizListItem, dtoBizListItem.type)+")}}'></span></td>"+
          "<td colspan='"+(visibleFileds.length-3)+"' width='100%' class='ddl-cell'>" +
              nameNewTemplate+"<div>"+bizListName.template+aliases.template+"</div></td>" +
            //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+
          '<td colspan="1" style="width:100px">'+state.template+'</td>'+
          '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
          "</tr>";

      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)][propertiesUtils.propName(dtoBizListItem, dtoBizListItem.id)],
            $scope.selectedItem[name.fieldName]);
      }


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      $scope.bizListTypeIcon= function(listTypeName)
      {
        return splitViewBuilderHelper.bizListTypeIcon(listTypeName);
      }

    })
    .directive('bizlistItemClientsExtractionsGrid', function(){
      return {
        restrict: 'EA',
        template: '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
        'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions"></div>',
        replace: true,
        transclude: true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          totalElements: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          initialLoadParams: '=',
        },
        controller: 'BizListItemExtractionsCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }

      }

    })

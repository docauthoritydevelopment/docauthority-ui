///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('bizLists.bizListExtractions.rules', [

    ])

    .controller('BizListItemExtractionRulesCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,userSettings:IUserSettingsService,eventCommunicator,
                                                        bizListsResource:IBizListsResource, propertiesUtils, configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log: ILog = Logger.getInstance('BizListItemExtractionRulesCtrl');

      var _this = this;
      $scope.elementName='bizlist-item-consumers-extraction-rules-grid';
      $scope.entityDisplayType = EntityDisplayTypes.bizlistItem_consumers_extraction_rules;

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate,null,null,getDataUrlBuilder, true);

        GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

      };


      var getDataUrlBuilder = function(restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration,includeSubEntityData,fullyContained:EFullyContainedFilterState,collapsedData:boolean) {

        var url:string = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName, restrictedEntityId, entityDisplayType, configuration, includeSubEntityData,fullyContained,collapsedData);
        if($scope.entityDisplayType==EntityDisplayTypes.bizlistItem_consumers_extraction_rules) {
          return url.replace(':bizListType', Entities.DTOBizList.itemType_CONSUMERS);
        }
        return null;
      }
      var parseData = function (listItems:Entities.DTOAggregationCountItem< Entities.DTOSimpleBizListItem>[]) {

        if(listItems) {
          listItems.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOSimpleBizListItem>) {
            (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2);
          });
          return listItems;
        }
        return null;
      };


      var dTOAggregationCountItem:Entities.DTOAggregationCountItem<string> =  Entities.DTOAggregationCountItem.Empty();
     var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';


      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template: ' #= item #'
      };


      var numOfFilteredFiles :ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
        title: '# filtered files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
      };

      var count2 :ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2),
        title: '# count2',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
      };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(name);

    //  fields.push(count2);
      fields.push(numOfFilteredFiles);



      var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

      var visibleFileds = fields.filter(f=>f.displayed);
      var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
          "<td colspan='1' class='icon-column' class='ddl-cell'><span class='tag tag-{{dataItem.item}} "+configuration.icon_bizListItemExtractionRule+"'></span></td>"+
          "<td colspan='"+(visibleFileds.length-2)+"' width='100%' >" +
          name.template+"<div></div></td>" +
            //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+
          '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
          "</tr>";


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[name.fieldName],$scope.selectedItem[name.fieldName]);
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });



    })
    .directive('bizlistItemConsumersExtractionRulesGrid', function(){
      return {
        restrict: 'EA',
        template: '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
        'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions"></div>',
        replace: true,
        transclude: true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          totalElements: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          initialLoadParams: '=',
        },
        controller: 'BizListItemExtractionRulesCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }

      }

    })

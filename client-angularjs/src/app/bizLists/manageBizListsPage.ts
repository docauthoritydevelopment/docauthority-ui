///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('bizLists', [
  'bizLists.manage',
  'resources.bizLists',
  'directives.bizLists.manage',
  'bizLists.bizListExtractions',
  'bizLists.bizListExtractions.rules',
  'bizLists.bizListItems',
  'directives.dialogs.bizLists.autoClassify',
]);

angular.module('bizLists')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.bizListsManagement, {
            parent: 'operations.configuration',
            url: '/bizLists?&selectedListId?',
            templateUrl: '/app/bizLists/manageBizListsPage.tpl.html',
            controller: 'ManageBizListPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngBizLists,Users.ESystemRoleName.ViewBizListAssociation,Users.ESystemRoleName.RunScans]
            }
          })

    })
    .controller('ManageBizListPageCtrl', function ($scope, $state, $filter, $location, $element, $compile, eventCommunicator:IEventCommunicator, $timeout, scanService:IScanService,
                                                   Logger, propertiesUtils, $stateParams, pageTitle, bizListsResource:IBizListsResource, configuration:IConfig, currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                   dialogs, bizListResource:IBizListResource, userSettings:IUserSettingsService, activeScansResource:IActiveScansResource, splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log:ILog = Logger.getInstance('ManageBizListPageCtrl');

 //     $scope.changePage = $stateParams.page && propertiesUtils.isInteger($stateParams.page) ? parseInt($stateParams.page) : 1;
      var selectedNavIdFromUrl =  $stateParams.selectedListId && propertiesUtils.isInteger($stateParams.selectedListId) ? parseInt($stateParams.selectedListId) : null;


      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizlistRefreshMenu, $scope, (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        $scope.loadLists($scope.selectedNavItem);
      })));


      $scope.changeShowOnlyActiveItems = false;
      $scope.inactiveStatus="Disabled for extraction";
      $scope.inactiveIconHtml= "&nbsp;<i class='fa fa-pause notice'></i>";
      var checkStatusPromise,scanTimeoutPromise;
      $scope.init = function () {
        pageTitle.set("Scan > Business lists settings");
        $scope.listItemsLazyFirstLoad = true;

        bizListsResource.getBizListAllowedTypes(getBizListAllowedTypesCompleted,onError);
        $scope.$on(scanService.runStatusOperationStateChangedEvent, function (sender, value) {
          getRunStatusCompleted();
         });
        scanService.forceRunStatusRefresh(true);
        $timeout(function(){
          $scope.resizeSplitter();
        },0);

        checkAutoClassificationStatus();

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isRunScansUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
        $scope.isMngBizListsUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngBizLists]);
      }

      var checkAutoClassificationStatus = function()
      {
        checkStatusPromise = activeScansResource.getAutoClassifyGroupsRunStatus(getAutoClassifyGroupsRunStatusCompleted,onGetError);

      };
      var getAutoClassifyGroupsRunStatusCompleted = function(status:Entities.DtoClassificationProgress)
      {
        $scope.enableStartClassifyButton = !(status.runStatus == Operations.ERunStatus.RUNNING || status.runStatus == Operations.ERunStatus.STOPPING);
        scanTimeoutPromise= $timeout(checkAutoClassificationStatus,status.runStatus == Operations.ERunStatus.RUNNING ?configuration.pooling_interval_running:configuration.pooling_interval_idle);
      }
      var checkAutoClassifyStatusAndReInitPooling=function()
      {
         $timeout.cancel(scanTimeoutPromise);
        if(checkStatusPromise)
        {
          checkStatusPromise.abort();
          checkStatusPromise=null;
        }
        checkAutoClassificationStatus();
      }

      var getRunStatusCompleted = function() {
       var activeExtractionRun:Operations.DtoRunSummaryInfo = scanService.getActiveExtractionRunForBizList($scope.selectedNavId);
       var activeExtractJob= activeExtractionRun? activeExtractionRun.jobs.filter(j=>j.jobType == Operations.EJobType.EXTRACT && j.bizListId==$scope.selectedNavId)[0]:null;
       var activeExtractFinJob= activeExtractionRun? activeExtractionRun.jobs.filter(j=>j.jobType == Operations.EJobType.EXTRACT_FINALIZE  && j.bizListId==$scope.selectedNavId)[0]:null;
        $scope.isRunning = activeExtractJob || activeExtractFinJob;
        $scope.runningPhase =  $scope.isRunning ? $filter("translate")('job_'+activeExtractionRun.jobs.filter(j=> j.bizListId==$scope.selectedNavId)[0].jobType):null;
        $scope.lastRunStartTimeText =  $scope.isRunning? activeExtractJob.phaseStart:null;
        $scope.lastRunElapsedTimeSpan =  $scope.isRunning?activeExtractJob.phaseLastStopTime:null;
        $scope.lasRunId =  $scope.isRunning? activeExtractionRun.crawlRunDetailsDto.id:null;
        $scope.lastStatus = "Ok";
        $scope.statusIconHtml= !$scope.isRunning ? "&nbsp;<i  class='fa fa-check-square primary'></i>":"&nbsp;<i class='fa fa-spin fa-cog'></i>";
      };

      $scope.resizeSplitter=function()
      {
        $timeout(function()
        {
          ($element.find(".splitter-element")).data("kendoSplitter").resize();
        })
      }

      $scope.refreshBizListGridDisplay = function(a,b)
      {
        $scope.refreshGridDisplay(a,b);
      }



      $scope.loadLists = function(listToSelect)
      {
        bizListsResource.getAllBizLists(null,
         function(listsByIdDic:any)
           {
            var lists = $.map(listsByIdDic, function(value, index) {
              return [value];
            });
            var listPerType = propertiesUtils.unique(lists,function(x:Entities.DTOBizList){return x.bizListItemType;});
            var supportedListPerType=[];
            for(var t=0; t<listPerType.length; t++) {
              var typeList= lists.filter(l=>l.bizListItemType == listPerType[t].bizListItemType);
              typeList.sort((t1, t2)=> {
                if (t1.name > t2.name) {
                  return 1;
                }
                if (t1.name < t2.name) {
                  return -1;
                }
                return 0;
              });
              var typeIsSupprted = $scope.listAllowedTypes.filter(l=>l==listPerType[t].bizListItemType)[0]!=null;
              if(typeIsSupprted) {
                supportedListPerType.push(typeList);
              }
            }

             supportedListPerType.sort((t1:Entities.DTOBizList, t2:Entities.DTOBizList)=> {
               if (t1[0].bizListItemType > t2[0].bizListItemType) {
                 return 1;
               }
               if (t1[0].bizListItemType < t2[0].bizListItemType) {
                 return -1;
               }
               return 0;
             });
            $scope.listPerType = supportedListPerType;
            setSelectedNavItemById(listToSelect?listToSelect.id:selectedNavIdFromUrl?selectedNavIdFromUrl:null,supportedListPerType);
          }
            ,onError);
      }

      $scope.bizListTypeIcon=function(listType)
      {
        return splitViewBuilderHelper.bizListTypeIcon(listType);
      }
      $scope.changeSelectedNav=function(list:Entities.DTOBizList)
      {
        $scope.selectedNavId = list.id;
        $scope.selectedNavName = list.name;
        $scope.selectedNavItem = list;
        if($scope.selectedNavType != list.bizListItemType) {
          $scope.selectedNavType = list.bizListItemType;
        }
        $scope.selectedNavActive = list.active;
        $scope.selectedNavUpdateDate = list.lastUpdateTimeStamp;
        $scope.selectedNavCreationDate = list.creationTimeStamp;
        $scope.selectedNavLastSuccessfulRunDate = list.lastSuccessfulRunDate;
        getRunStatusCompleted();
        updateUrl();
      };
      var updateUrl = function()
      {
        $stateParams['selectedListId'] = $scope.selectedNavId ;
        log.debug('Set selectedListId to url: ' + $stateParams['selectedListId']);
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      var confirmation = function(op:string) {
        return dialogs.confirm('Confirmation', 'Please confirm: '+ op, null);
      };
      $scope.startGroupClassification=function(bizListType)
      {

          $scope.enableStartClassifyButton = false;
        var dlg = dialogs.create(
            '/app/bizLists/autoClassifyGroupsDialog.tpl.html',
            'autoClassifyGroupsDialogCtrl',
            {bizListType:bizListType},
            {size:'md'});

        dlg.result.then(function(options:any){
          bizListsResource.autoClassifyGroups(bizListType,options, function () {
                checkAutoClassifyStatusAndReInitPooling();
              },
              onGetError
          );

        },function(){

        });

      }
      $scope.startExtractionForList = function(bizListId:number,bizListName:string)
      {
        $scope.errorMsg='';
        scanService.startExtractionForList(bizListId,bizListName);
      };

      $scope.stopExtractionForList = function(bizListId:number,bizListName:string)
      {
        $scope.errorMsg='';
        if($scope.lasRunId) {
          scanService.stopRunsByID([$scope.lasRunId], [bizListName]);
        }
      };

      $scope.pauseExtractionForList = function(bizListId:number,bizListName:string)
      {
        $scope.errorMsg='';
        if($scope.lasRunId) {
          scanService.pauseRunsByID([$scope.lasRunId],[bizListName]);
        }
      };

      var getBizListAllowedTypesCompleted = function(types:string[])
      {
        $scope.listAllowedTypes=types.sort((t1, t2)=> {
          if (t1 > t2) {
            return 1;
          }
          if (t1 < t2) {
            return -1;
          }
          return 0;
        });
        $scope.loadLists();
      }

      var setSelectedNavItemById = function(listId:number,listPerType)
      {
        var selectedItem;
        if(listId&&listPerType)
        {
          for(var t=0; t<listPerType.length; t++) {

            selectedItem = listPerType[t].filter(i=>i.id==listId)[0];
            if(selectedItem)
            {
              break;;
            }
          }

        }
        if(selectedItem) {
          $scope.changeSelectedNav(selectedItem);
        }
        else if(listPerType[0][0]){
          $scope.changeSelectedNav(listPerType[0][0]);
        }
      }
      var onGetError=function(e)
      {
        log.error("error get file crawling status " +e);
        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };
      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
      var openDeleteListDialog = function(list:Entities.DTOBizList)
      {
        var dlg = dialogs.confirm('Confirmation', "You are about to delete list named: '" + list.name + "' and all its items. Continue?", null);
        dlg.result.then(function (btn) { //user confirmed deletion
          deleteBizList(list);

        }, function (btn) {

        });
      }
      var editBizList = function (newBizList:Entities.DTOBizList) {
        newBizList.name =  newBizList.name?( newBizList.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newBizList.name &&  newBizList.name.trim() != '') {
          log.debug('Edit bizList to list type: ' +  $scope.bizListType + ' name: ' + newBizList.name);
          bizListsResource.updateBizList( newBizList, function () {
            $scope.loadLists(newBizList);

          }, function () {
            dialogs.error('Error','Failed to edit BizList.');
          });
        }
      }
      var deleteBizList = function(bizList:Entities.DTOBizList)
      {
        log.debug('delete BizList: ' + bizList.name);
        bizListsResource.deleteBizList(bizList.id, function () {
          $scope.loadLists();
        }, function () {
          dialogs.error('Error','Failed to delete newBizList');
        });
      };

      $scope.autoGenerate = function(bizList:Entities.DTOBizList) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizListAutoGenerateAction, {bizList: bizList});
      };

      $scope.approveAllPending = function(bizList:Entities.DTOBizList) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizListApproveAllPendingAction, {bizList: bizList});
      };

      var openEditListDialog = function(list:Entities.DTOBizList)
      {

        var dlg = dialogs.create(
            '/app/bizLists/newBizListDialog.tpl.html',
            'newBizListDialogCtrl',
            {bizList:list,bizListTypes:$scope.listAllowedTypes},
            {size:'md'});

        dlg.result.then(function(bizList:Entities.DTOBizList){
          editBizList(bizList);
        },function(){

        });
      }

      $scope.deleteBizList = function(list:Entities.DTOBizList) {
        openDeleteListDialog(list);
      };
      $scope.editBizList = function(list:Entities.DTOBizList) {
        openEditListDialog(list);
      };

      $scope.$on("$destroy", function() {
        scanService.forceRunStatusRefresh(false);
        eventCommunicator.unregisterAllHandlers($scope);
        if (angular.isDefined(scanTimeoutPromise)) {
          $timeout.cancel(scanTimeoutPromise);
          scanTimeoutPromise = undefined;
        }
        if(checkStatusPromise)
        {
          checkStatusPromise.abort();
          checkStatusPromise=null;
        }

      });
      //must be at the end to be sure all functions that ae used in init() are already created
      userSettings.getGridViewPreferences(function (data) {
        $scope.changeGridView = (!data) || data.gridView;
        $scope.init();
      });
    })

///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.bizLists.items', [

    ])
    .controller('newBizListItemDialogCtrl', function ($scope,  $uibModalInstance,data) {

      $scope.bizListId = data.bizListId;
      $scope.bizListName = data.bizListName;
      $scope.bizListType = data.bizListType;
      $scope.aliases=[];
      $scope.states = [Entities.DTOSimpleBizListItem.state_ACTIVE,Entities.DTOSimpleBizListItem.state_PENDING];

      var init=function(bizListItem:Entities.DTOSimpleBizListItem)
      {
        if(bizListItem)
        {
          $scope.editMode=true;
          $scope.name =  bizListItem.name;
          $scope.title='Edit list item';
          $scope.id=bizListItem.id;
          $scope.aliases =bizListItem.entityAliases;
          $scope.state = bizListItem.state;
          $scope.businessId = bizListItem.businessId;
          $scope.bizListId = bizListItem.bizListId;
        }
        else
        {
          $scope.editMode=false;
          $scope.title='Add list item';
          $scope.state = Entities.DTOSimpleBizListItem.state_ACTIVE;
          $scope.bizListId =   $scope.bizListId;
        }
        $scope.initiated=true;
      }
      init(data.bizListItem);

      $scope.addAlias = function()
      {
        var newItem = new Entities.DTOBizListItemAlias();
        newItem.name =  $scope.newAlias;
        $scope.aliases.push(newItem);
        $scope.newAlias=null;
      }

      $scope.removeAilas = function(alias: Entities.DTOBizListItemAlias)
      {
          $scope.aliases= $scope.aliases.filter(a=>a!=alias);
      }


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var listItem = new Entities.DTOSimpleBizListItem();
          listItem.state = $scope.state;
          listItem.name=$scope.name;
          listItem.businessId =$scope.businessId;
          listItem.id =$scope.id;
          listItem.entityAliases =$scope.aliases;
          listItem.bizListId =$scope.bizListId;
          $uibModalInstance.close(listItem);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


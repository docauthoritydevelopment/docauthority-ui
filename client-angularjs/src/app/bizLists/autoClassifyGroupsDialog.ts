///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.dialogs.bizLists.autoClassify', [

    ])
    .controller('autoClassifyGroupsDialogCtrl', function ($scope,  $uibModalInstance,data) {


      $scope.bizListType = data.bizListType;
      $scope.group_size_minimum=10
      $scope.singleBli_dominant_percentage=0.7*100;
      $scope.singleBli_others_percentage=0.1*100;
      $scope.singleBli_others_maxCount=5;
      $scope.multiMatch_bli_minCount=5;
      $scope.multiMatch_bli_others_percentage=0.5*100;

      var init=function()
      {
              $scope.editMode=false;
          $scope.title='Analyze entity extraction';


      }
      init();


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        if($scope.myForm.$valid)
        {
          var options = <any>{};
          if($scope.group_size_minimum)
             options["group.size.minimum"]=$scope.group_size_minimum;
          if($scope.singleBli_dominant_percentage)
             options["singleBli.dominant.percentage"]=$scope.singleBli_dominant_percentage/100;
          if($scope.singleBli_others_percentage)
             options["singleBli.others.percentage"]=$scope.singleBli_others_percentage/100;
          if($scope.singleBli_others_maxCount)
             options["singleBli.others.maxCount"]=$scope.singleBli_others_maxCount;
          if($scope.multiMatch_bli_minCount)
             options["multiMatch.bli.minCount"]=$scope.multiMatch_bli_minCount;
          if($scope.multiMatch_bli_others_percentage)
             options["multiMatch.bli.others.percentage"]=$scope.multiMatch_bli_others_percentage/100;
          $uibModalInstance.close(options);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


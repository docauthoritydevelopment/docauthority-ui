///<reference path='../../common/all.d.ts'/>

'use strict';


angular.module('directives.bizLists.manage', [
      'directives.dialogs.bizLists',
      'directives.dialogs.bizListUpload',
      'resources.bizLists',
    ])
    .controller('bizListMenuCtrl', function ($scope,$element,Logger:ILogger,$q,$filter,
                                              userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,bizListResource:IBizListResource,bizListsResource:IBizListsResource) {
      var log:ILog = Logger.getInstance('bizListMenuCtrl');

      $scope.init = function () {

      };
      $scope.openNewListDialog = function()
      {

        var dlg = dialogs.create(
            '/app/bizLists/newBizListDialog.tpl.html',
            'newBizListDialogCtrl',
            {bizListType:$scope.bizListType,bizListTypes:$scope.bizListTypes},
            {size:'md'});

        dlg.result.then(function(newBizList:Entities.DTOBizList){
          addNewBizList(newBizList);
        },function(){

        });
      }


      $scope.openNewBizListItemDialog = function(listId:number)
      {

        var dlg = dialogs.create(
            '/app/bizLists/newBizListItemDialog.tpl.html',
            'newBizListItemDialogCtrl',
            {bizListId:$scope.bizListId,bizListName:$scope.bizListName,bizListType:$scope.bizListType},
            {size:'md'});

        dlg.result.then(function(newBizListItem:Entities.DTOSimpleBizListItem){
          addNewItem(newBizListItem);
        },function(){

        });
      }

      $scope.openUploadBizListItemsDialog = function()
      {

        var dlg = dialogs.create(
            '/app/bizLists/uploadBizListItemsDialog.tpl.html',
            'uploadBizListItemsDialogCtrl',
            {bizListId:$scope.bizListId,bizListName:$scope.bizListName,bizListType:$scope.bizListType,changeGridView:$scope.changeGridView},
            {size:'lg'});

        dlg.result.then(function(data:any){
          var status = data.approveImport==true?Entities.DTOSimpleBizListItem.state_ACTIVE:Entities.DTOSimpleBizListItem.state_PENDING

          updateUploadStatus(data.importId,$scope.bizListId,status);
        },function(){

        });
      }

      $scope.openUploadBizListItemsFromFoldersNameDialog = function()
      {

        var dlg = dialogs.create(
            '/app/bizLists/uploadBizListItemsDialog.tpl.html',
            'uploadBizListItemsDialogCtrl',
            {bizListId:$scope.bizListId,bizListName:$scope.bizListName,bizListType:$scope.bizListType,mode:'fromFolder',changeGridView:$scope.changeGridView},
            {size:'lg'});

        dlg.result.then(function(data:any ){
          var status = data.approveImport==true?Entities.DTOSimpleBizListItem.state_ACTIVE:Entities.DTOSimpleBizListItem.state_PENDING
          updateUploadStatus(data.importId,$scope.bizListId,status);
        },function(){

        });
      }


      var notifyChange = function() {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizlistRefreshMenu, {
        });
      }
      $scope.openEditBizListSelectedItemsDialog = function(listItem)
      {
        var bizListItem:Entities.DTODocType=listItem?listItem: $scope.selectedListItemsToOperate[0];
        var dlg = dialogs.create(
            '/app/bizLists/newBizListItemDialog.tpl.html',
            'newBizListItemDialogCtrl',
            {bizListItem:bizListItem,bizListId:$scope.bizListId,bizListName:$scope.bizListName,bizListType:$scope.bizListType},
            {size:'md'});

        dlg.result.then(function(newBizListItem:Entities.DTOSimpleBizListItem){
          editItem(newBizListItem);
        },function(){

        });
      }

      $scope.openDeleteSelectedBizListItems = function()
      {
        var bizListItems:Entities.DTOSimpleBizListItem[]=$scope.selectedListItemsToOperate;
        if(bizListItems&& bizListItems.length>0) {
          var dlg = dialogs.confirm('Confirmation', "You are about to delete " + bizListItems.length + " list items. Continue?", null);
          dlg.result.then(function (btn) { //user confirmed deletion
            bizListItems.forEach(d=>  deleteItem(d));

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }

      }

      var deleteItem = function(newBizListItem:Entities.DTOSimpleBizListItem)
      {
        log.debug('delete BizListItem: ' + newBizListItem.name);
        bizListResource.removeListItem(newBizListItem, function () {
          $scope.reloadData();
          notifyChange();
        }, function () {
          dialogs.error('Error','Failed to delete newBizListItem.');
        });
      }
      var addNewBizList = function (newBizList:Entities.DTOBizList) {
        newBizList.name =  newBizList.name?( newBizList.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newBizList.name &&  newBizList.name.trim() != '') {
          log.debug('add new newBizListItem to list type: ' +  $scope.bizListType + ' name: ' + newBizList.name);
          bizListsResource.addNewBizList( newBizList, function (addedBizList) {
            $scope.reloadData(addedBizList);

          }, function () {
            dialogs.error('Error','Failed to add newBizList.');
          });
        }
      }


      var addNewItem = function (newBizListItem:Entities.DTOSimpleBizListItem) {
        newBizListItem.name =  newBizListItem.name?( newBizListItem.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newBizListItem.name &&  newBizListItem.name.trim() != '') {
          log.debug('add new newBizListItem to list id: ' +  newBizListItem.bizListId + ' name: ' +  newBizListItem.name);
          bizListResource.addListItem( newBizListItem, function (addedItem) {
            $scope.gridFindId=addedItem.id;
            $scope.reloadData();
            notifyChange();

          }, function () {
            dialogs.error('Error','Failed to add newBizListItem.');
          });
        }
      }

      var editItem = function (newBizListItem:Entities.DTOSimpleBizListItem) {

        newBizListItem.name =  newBizListItem.name?( newBizListItem.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newBizListItem.name &&  newBizListItem.name.trim() != '') {
          log.debug('edit BizListItem to list id:: ' +   newBizListItem.bizListId + ' name: ' +  newBizListItem.name);
          bizListResource.editListItem( newBizListItem, function () {
            $scope.reloadData();
          }, function () {
            dialogs.error('Error','Failed to edit bizListItem.');
          });
        }
      }

      $scope.addApproveToSelectedItems = function () {

        if ( $scope.selectedListItemsToOperate && $scope.selectedListItemsToOperate.length > 0) {
          var docTypeToAdd = <Entities.DTODocType>$scope.activeSelectedDocTypeItem;
           var loopPromises = [];
          $scope.selectedListItemsToOperate.forEach(function (selectedItem:Entities.DTOSimpleBizListItem) {
            var deferred = $q.defer();
            loopPromises.push(deferred.promise);
            var addDocType = bizListResource.updateListItemStatus(selectedItem.bizListId, selectedItem.id, selectedItem.state, Entities.DTOSimpleBizListItem.state_ACTIVE,
                function (updatedItem) {
                  log.debug('receive addDocType success for ' + selectedItem.id);
                   deferred.resolve(selectedItem.id);
                },
                function (error) {
                  deferred.resolve();
                  log.error('receive addDocType failure' + error);
                  onError();
                });
          });
          $q.all(loopPromises).then(function (selectedItemsIdsToUpdate) {
            $scope.reloadData(selectedItemsIdsToUpdate); //no need when auto sync is false
          });
        }
      }

      var updateUploadStatus=function(importId:number,bizListId,status)
      {
        bizListResource.updateUploadListItemsStatus( bizListId,importId,status, function () {
          $scope.reloadData();
          notifyChange();

        }, function () {
          dialogs.error('Error','Failed to updateUploadStatus.');
        });
      }
      $scope.toggleApproveSelectedPendingItems = function () {

        //if ($scope.activeSelectedDocTypeItem && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
        //  var docTypeToToggle = <Entities.DTODocType>$scope.activeSelectedDocTypeItem;
        //  var firstSelectedItem = $scope.selectedDocTypesItemsToTag[0];
        //  var tags = (<any>firstSelectedItem).item ? (<any>firstSelectedItem).item.docTypeDtos : (<any>firstSelectedItem).docTypeDtos;
        //  if (isExplicitDocTypeExists(tags, docTypeToToggle)) {
        //    $scope.removeDocTypeFromSelectedItems();
        //  }
        //  else {
        //    $scope.addDocTypeToSelectedItems();
        //  }
        //}
      }


      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.removeApproveFromSelectedItems= function () {
        //if ($scope.activeSelectedDocTypeItem && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
        //  var docTypeToRemove = <Entities.DTODocType>$scope.activeSelectedDocTypeItem;
        //  var theDocTypeResource:IDocTypeResource = docTypeResource($scope.displayType);
        //  var loopPromises = [];
        //  $scope.selectedDocTypesItemsToTag.forEach(function (selectedItem) {
        //    var removeTag = theDocTypeResource.removeDocType(docTypeToRemove.id, selectedItem['id']);
        //    var deferred = $q.defer();
        //    loopPromises.push(deferred.promise);
        //    removeTag.$promise.then(function (g:any) {
        //          log.debug('receive removeDocType from selected items success for' + selectedItem['id']);
        //          updateDisplayedItemAfterRemove(selectedItem, docTypeToRemove);
        //          deferred.resolve(selectedItem);
        //        },
        //        function (error) {
        //          deferred.resolve();
        //          log.error('receive removeDocType from selected items failure' + error);
        //          onError();
        //        });
        //  })
        //  $q.all(loopPromises).then(function (selectedItemsToUpdate) {
        //    $scope.reloadData(); //no need when auto sync is false
        //  });
        //}
      }
      var updateActiveState = function (bizListItem:Entities.DTOSimpleBizListItem,state:string) {
        var dlg = dialogs.confirm('Confirmation', "Are you sure you want to set "+bizListItem.name+" to "+$filter('translate')(state)+" ?", null);
        dlg.result.then(function (btn) { //user confirmed edit state
          log.debug('update  state: '+state );

          bizListResource.updateListItemStatus(bizListItem.bizListId,bizListItem.id, bizListItem.state,state,function (selectedItem:Entities.DTOSimpleBizListItem) {
            bizListItem.state= selectedItem.state;
            bizListItem['isActive']= selectedItem.state==Entities.DTOSimpleBizListItem.state_ACTIVE;
            $scope.refreshDisplay();
          }, onError);

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      }
      $scope.isClientsBizList = function()
      {
        return $scope.bizListType==Entities.DTOBizList.itemType_CLIENTS;
      }
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      if($scope.remoteSelectedItemsManageSupport=='true') {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridManageBizListUserAction, $scope, (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
              if (fireParam.action === 'editBizListItem') {
                $scope.openEditBizListSelectedItemsDialog(fireParam.item);
              }
              else if (fireParam.action === 'setActiveState') {
                updateActiveState(fireParam.item,fireParam.newState);
              }
            })));
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizListAutoGenerateAction, $scope, (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {
          if (fireParam.bizList) {
            $scope.bizListId = fireParam.bizList.id;
            $scope.bizListName = fireParam.bizList.name;
            $scope.bizListType = fireParam.bizList.bizListItemType;

            $scope.openUploadBizListItemsFromFoldersNameDialog();
          }
        })));

        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizlistUploadAction, $scope, (new RegisteredEvent(function (registrarScope, registrationParam, fireParam) {
          if (fireParam.bizList) {
            $scope.bizListId = fireParam.bizList.id;
            $scope.bizListName = fireParam.bizList.name;
            $scope.bizListType = fireParam.bizList.bizListItemType;

            $scope.openUploadBizListItemsDialog();
          }
        })));
      }
    })
    .directive('bizListItemsManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/bizLists/manageBizListsMenu.tpl.html',
            controller: 'bizListMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'refreshDisplay':'=',
              'bizListId':'=',
              'bizListName':'=',
              'bizListType':'=',
              'reloadData':'=',
              'remoteSelectedItemsManageSupport':'@',
              'gridFindId':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    )
    .directive('bizListManageMenu',
    function () {
      return {
        // restrict: 'E',
        template:'<div> <button class="btn btn-flat btn-xs" ng-click="openNewListDialog()" style="width:50px;"><i class="fa fa-plus" style="padding-right:3px"></i>Add</button></div>',

       controller: 'bizListMenuCtrl',
       replace:true,
       scope:{
          'bizListType':'=',
          'bizListTypes':'=',
          'reloadData':'=',
       },
        link: function (scope:any, element, attrs) {
          scope.init();
        }
      };
    }
   )
    .directive('bizListSelectMenu',
        function () {
          return {
            // restrict: 'E',
            template:" <select class='form-control' ng-model='bizListSelected' ng-change='onBizListSelectedChanged();'" +
            "    ng-options='listOption as listOption.name for listOption in bizListTypeItems'" +
            "</select>",

            controller: function($scope,bizListsResource:IBizListsResource)
            {
              $scope.init=function()
              {
                $scope.bizListSelectedType = $scope.bizListType;
                bizListsResource.getAllBizLists(  $scope.bizListType,function (listsByIdDic:any) {
                  var lists = $.map(listsByIdDic, function(value, index) {
                    return [value];
                  });
                  $scope.bizListTypeItems=lists;
                  $scope.bizListSelected =  $scope.bizListTypeItems[0]? $scope.bizListTypeItems[0]:null;
                }, function () {

                });
              }

              $scope.onBizListSelectedChanged = function()
              {
                 $scope.onBizListSelected($scope.bizListSelected);
              }

            },
            replace:true,
            scope:{
              'bizListType':'=',
              'bizListSelectedType':'=',
              'onBizListSelected':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }
    )


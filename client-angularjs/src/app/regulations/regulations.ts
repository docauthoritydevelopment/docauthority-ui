///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('regulations',[

]);

angular.module('regulations')
  .config(function ($stateProvider) {
    $stateProvider
      .state('regulations', {
        //   abstract: true,
        url: '/regulations',
        templateUrl: '/app/regulations/regulations.tpl.html',
        controller: 'RegulationsPageCtrl'
      })

  })
  .controller('RegulationsPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('RegulationsPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('RegulationsCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('RegulationsCtrl');

    var _this = this;

    $scope.elementName='regulation-grid';
    $scope.entityDisplayType = EntityDisplayTypes.regulations;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null, true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTORegulation> =  Entities.DTOAggregationCountItem.Empty();
    var dTORegulation:Entities.DTORegulation =  Entities.DTORegulation.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTORegulation, dTORegulation.id),
      title: 'Id',
      type: EFieldTypes.type_string,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixFieldName+propertiesUtils.propName(dTORegulation, dTORegulation.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,


   };
    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="Regulation: #: item.name # ">#: item.name #  <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='filterClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this regulation</a></li>"+
        '  </ul></span>';
    $scope.filterClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTORegulation>) {
      setFilterByEntity(item.item.id,item.item.name);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line' href='regulations' ></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
        nameNewTemplate+"<div></div></td>" +
          //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

        '<td colspan="1" style="width:220px" >'+filterTemplate+'</td>'+
        "</tr>";


    var parseData = function (regulations:Entities.DTOAggregationCountItem< Entities.DTORegulation>[]) {
      if(regulations) {
        regulations.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOTextSearchPattern>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2);
        });
        return regulations;
      }
      return null;
    };
    var onSelectedItemChanged = function()
    {
       $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName]);
    }
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('regulationsGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        totalElements: '=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        initialLoadParams: '=',
        findId: '=',
      },
      controller: 'RegulationsCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

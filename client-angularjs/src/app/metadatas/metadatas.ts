///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('metadatas',[

]);

angular.module('metadatas')
  .config(function ($stateProvider) {
    $stateProvider
      .state('metadatas', {
        //   abstract: true,
        url: '/metadatas',
        templateUrl: '/app/metadatas/metadatas.tpl.html',
        controller: 'MetadatasPageCtrl'
      })

  })
  .controller('MetadatasPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('MetadatasPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('MetadatasCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('MetadatasCtrl');

    var _this = this;

    $scope.elementName='metadatas-grid';
    $scope.entityDisplayType = EntityDisplayTypes.metadatas;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOMetadata>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate, null, null, null, true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOMetadata> =  Entities.DTOAggregationCountItem.Empty();
    var dTOMetadata:Entities.DTOMetadata =  Entities.DTOMetadata.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var type:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOMetadata, dTOMetadata.type),
      title: 'Type',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      template: ' #= '+ prefixFieldName + propertiesUtils.propName(dTOMetadata, dTOMetadata.type)+' # <span class="tag tag-#: styleId #"> <span class="fa fa-flag"></span></span>'
    };
    var typeNewTemplate =    '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  class="title-secondary" title="File metadata type: #: '+ type.fieldName+' #">#: '+ type.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  style="max-width:250px"> '+
      "<li><a  ng-click='typeItemClicked(dataItem)'><span class='fa fa-filter'></span> Filter by this file metadata type</a></li>"+
      '  </ul></span>';

    $scope.typeItemClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOMetadata>) {
      setFilterByEntity(item.item.type,item.item.type,EntityDisplayTypes.metadatas_type);
    };
    $scope.valueClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOMetadata>) {
      setFilterByEntity(getUniqueId(item.item.type, item.item.value),item.item.value, EntityDisplayTypes.metadatas);
    };
    var setFilterByEntity = function(gId,gName,entityDisplayType)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:entityDisplayType,
        filterName:gName
      });
    };

    var mValue:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:   prefixFieldName+propertiesUtils.propName(dTOMetadata, dTOMetadata.value),
      title: 'Value',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false
    };

    var  valueTemplate= '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  class="title-primary" title="File meatdata : #: '+ mValue.fieldName+' #">#: '+ mValue.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      "<li><a  ng-click='valueClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this file metadata</a></li>"+
      '  </ul></span>';



    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(type);
    fields.push(mValue);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column'><span class='tag tag-#: styleId # "+configuration.icon_metadata+"'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
      valueTemplate +"<div>"+typeNewTemplate+"</div></td>" +
      //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+
      '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
      "</tr>";


    var parseData = function (mts:Entities.DTOAggregationCountItem< Entities.DTOMetadata>[]) {
      if(mts) {
        let typeIndexMap = {};
        let typeIndex = 1 ;
        mts.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOMetadata>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2);
          if (!typeIndexMap[listItem.item.type]) {
            typeIndexMap[listItem.item.type] = (typeIndex++)+'';
          }
          (<any>listItem).styleId = splitViewBuilderHelper.getMetadataStyleId(Number(typeIndexMap[listItem.item.type]));
        });
        return mts;
      }
      return null;
      };


    var getUniqueId = function(type, value ){
      return type + ";" + value;
    }


    var onSelectedItemChanged = function()
    {
         $scope.itemSelected(getUniqueId($scope.selectedItem[type.fieldName],$scope.selectedItem[mValue.fieldName]),$scope.selectedItem[mValue.fieldName]);
    }


    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('metadatasGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        entityTypeId: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        totalElements: '=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        initialLoadParams: '=',
        findId: '=',
      },
      controller: 'MetadatasCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('folders',[
   // 'directives',
  'ui.router',

]);

angular.module('folders')
  .config(function ($stateProvider) {
    $stateProvider
      .state('folders', {
     //   abstract: true,
        url: '/folders',
        templateUrl: '/app/folders/folders.tpl.html',
        controller: 'FoldersPageCtrl'
      })

  })

 .controller('FoldersTreeCtrl', function ($scope, $state,$stateParams,$window,$element, $location,Logger,$timeout,currentUserDetailsProvider:ICurrentUserDetailsProvider,
     eventCommunicator:IEventCommunicator,propertiesUtils, configuration:IConfig,dialogs,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,docTypeResource) {
    var log: ILog = Logger.getInstance('FoldersTreeCtrl');

    $scope.entityDisplayType = EntityDisplayTypes.folders;
    $scope.elementName='folders-tree';
    $scope.EFolderType = Entities.EFolderType;

    var elementOptions;

    $scope.InitTree = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      $scope.getFindByPathValue = function(dataItem:Entities.DTOAggregationCountFolder){
        return dataItem? (<Entities.DTOFolderInfo>dataItem.item).realPath.toLowerCase(): null;
      }
      log.debug('Init FoldersCtrl tree');
      elementOptions=
          TreeBehaviorHelper.createTree<Entities.DTOAggregationCountFolder>($scope, log,configuration,eventCommunicator, getDataUrlParams,
              fields,treeItemTemplate, parseData, hasChildren,null);

      TreeBehaviorHelper.connect($scope, $timeout, $window, log,configuration,  elementOptions, onSelectedItemChanged,$element);

      $scope.isDemoUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.demo_showcase]);
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        $scope.isDemoUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.demo_showcase]);
      });
    };

    var parseData = function (folderAggregations:Entities.DTOAggregationCountFolder[]) {
      folderAggregations.map(function (folderAggregation:Entities.DTOAggregationCountFolder) {
        var folder: Entities.DTOFolderInfo= <Entities.DTOFolderInfo>(folderAggregation.item);
        (<any>folderAggregation).inFolderPercentage = splitViewBuilderHelper.getFilesNumPercentage(folderAggregation.numOfAllFiles, folder.numOfAllFiles);
        splitViewBuilderHelper.parseTags(<Entities.ITaggable>folder);
        splitViewBuilderHelper.parseDocTypes(<Entities.IDocTypesAssignee>folder,configuration);
        splitViewBuilderHelper.parseDepartment(<Entities.IDepartmentAssignee>folder,configuration);
        splitViewBuilderHelper.parseAssociatedBizListItems(<Entities.IBizListAssociatable>folder);
        splitViewBuilderHelper.parseFunctionalRoles(<Entities.IFunctionalRolesAssignee>folder);
        splitViewBuilderHelper.parseFilesAndFilterTemplate(<Entities.IFunctionalRolesAssignee>folder);


        if (folder.depthFromRoot == 0) {
           folder.name = folder.rootFolderNickName ? folder.rootFolderNickName + folder.realPathNoPrefix : folder.realPath;
           if(folder.realPath.indexOf('/')>0) {
             folder.name = folder.name[folder.name.length - 1] == '/' ? folder.name.substr(0, folder.name.length - 1) : folder.name;
           }
           else {
             folder.name = folder.name[folder.name.length - 1] == '\\' ? folder.name.substr(0, folder.name.length - 1) : folder.name;
           }
        }

      });
      folderAggregations.sort(function(x,y)
      {
        if((<Entities.DTOFolderInfo>x.item).name.toLowerCase() > (<Entities.DTOFolderInfo>y.item).name.toLowerCase())
        {
          return 1;
        }
       else
        {
          return -1;
        }
      });
      return folderAggregations;
    };

    var hasChildren  = function(e:Entities.DTOAggregationCountFolder)
    {
      return (<Entities.DTOFolderInfo>e.item).numOfSubFolders>0
    };

    var getDataUrlParams = function()
    {
      var urlParams =  {'take':configuration.MAX_FOLDERS_IN_TREE,'page':1,'reldepth':1};
      if($scope.filterData) {
        (<any>urlParams).filter = JSON.stringify($scope.filterData.filter);
      }
      return urlParams;

    };


   var onError = function()
   {
     dialogs.error('Error','Sorry, an error occurred.');
   }


    var dTOAggregationCountFolder:Entities.DTOAggregationCountFolder =  Entities.DTOAggregationCountFolder.Empty();
    var dTOFolderInfo:Entities.DTOFolderInfo =  Entities.DTOFolderInfo.Empty();
    var prefixFieldName =  propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.item)+'.';

    var id:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.id),
      title: 'ID',
      isPrimaryKey:true,
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true,

    };
   var realPath:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
     fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.realPath),
     title: 'Path',
     type: EFieldTypes.type_string,
     displayed: false,
     editable: false,
     nullable: true,
   };

   var name:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
 //  var nameNewTemplate = '<a class="title-primary">#: '+ prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.name)+' #</a>';
   var nameNewTemplate= '<span class="dropdown dropdown-wrapper" title="#= item.'+  realPath.fieldName+' #"><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
       '<a class="title-primary">#= item.'+ prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.name)+' # <i class="caret no-visible"></i></a></span>'+
       '<ul class="dropdown-menu"  > '+
       '<li><a class="btn btn-link btn-xs " target="_blank" href="{{createDetailsPageUrl( dataItem.item.id, contextName)}}"><span class="fa fa-info"></span> View folder details</a></li>'+
       '<li><a clipboard text="dataItem.' + realPath.fieldName+' || dataItem.item.mailboxGroup" title="copy path to clipboard"><span class="fa fa-copy" ></span> Copy full path </a></li>'+
       '<li><a   ng-click="folderNameClicked(dataItem)"><span class="fa fa-filter"></span> Filter by this folder</a></li>'+
       // '<li ng-if="openFolderUrl(dataItem)"><a href="{{openFolderUrl(dataItem)}}" target="_blank">Open folder in host application</a></li>'+
       '<li ng-if="isDemoUser" class="divider"></li>'+
       '<li ng-if="isDemoUser"><a>Authorize folder tree</a></li>'+
       ' </ul></span> ';

   $scope.createDetailsPageUrl = function(groupId,context:EFilterContextNames){
     if(groupId) {
       const urlParams = {};
       urlParams['context'] = context.toString();
       urlParams['id'] = groupId;

       var url = $state.href(ERouteStateName.details, urlParams);
       url = url.replace('/v1','');
       return url;
       //  $window.open(url,'_blank');
     }
     return '';
   }
   $scope.folderNameClicked = function(item:Entities.DTOAggregationCountFolder) {
     setFilterByFolder((<Entities.DTOFolderInfo>item.item).id,(<Entities.DTOFolderInfo>item.item).realPath);
   };
   var setFilterByFolder = function(gId,gName)
   {

     eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
       action: 'doFilterByID',
       id: gId,
       entityDisplayType:EntityDisplayTypes.folders,
       filterName:gName,
       includeSubEntityDataLeft:true
     });
   };


   $scope.openFolderUrl = function(dataItem:Entities.DTOAggregationCountFolder)
   {
     if(startsWith(((<Entities.DTOFolderInfo>dataItem.item).realPath.toLowerCase()),'http'))
     {
       return  (<Entities.DTOFolderInfo>dataItem.item).realPath;
     }
     return null;
   }

   var startsWith = function (str1:string, str2:string):boolean {
     return str1.substring(0, str2.length) === str2;
   };
   var numOfDirectFiles:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
     fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfDirectFiles),
     title: '# Files ',
     type: EFieldTypes.type_number,
     displayed: true,
     editable: false,
     nullable: true,
   };
   var numOfAllFiles:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
     fieldName: prefixFieldName+propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles),
     title: '# All Files',
     type: EFieldTypes.type_number,
     displayed: true,
     editable: false,
     nullable: true,
   };

    var numOfSubFolders:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName:  prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders),
      title: 'Subfolders',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };
   var numOfSubFoldersNewTemplate = '<span class="title-third" title="Total of direct files, all subfolders ">#: '+ numOfDirectFiles.fieldName+' #  files in folder' +
     //  '# if('+ prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders)+'>0) {#, #: kendo.toString('+ numOfAllFiles.fieldName+'-'+numOfDirectFiles.fieldName+',"n0") # files in
    ', #: kendo.toString('+ prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders)+',"n0") # subfolders ' +
       '</span>';


    var filteredNumOfDirectFiles:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfDirectFiles),
      title: '# Filtered Files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };

   var inFolderPercentage:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
     fieldName: 'inFolderPercentage',
     title: '% filtered files',
     type: EFieldTypes.type_number,
     displayed: true,
     editable: false,
     nullable: false,

   };



    var filteredNumOfAllFiles:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles),
      title: '# All filtered Files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };
    var bizListAssociation:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'associatedBizListAsFormattedString', //for excel export
      title: 'Clients association',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizListAssociation,eventCommunicator,true);

    var tags:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'tagsAsFormattedString', //for excel export
      title: 'Tags',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };


    var docTypes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'docTypesAsFormattedString', //for excel export
      title: 'DocTypes',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };

   var department:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
     fieldName: prefixFieldName+'departmentAsFormattedString', //for excel export
     title: 'Department',
     type: EFieldTypes.type_other,
     displayed: true,
     editable: false,
     nullable: true
   };

   var functionalRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
     fieldName: 'functionalRolesAsFormattedString', //for excel export
     title: 'Data roles',
     type: EFieldTypes.type_other,
     displayed: true,
     editable: false,
     nullable: true,

   };

   var tagNewTemplate={};
   splitViewBuilderHelper.setTagsTemplate2($scope,tagNewTemplate,eventCommunicator,true);
   var tagsTemplate = (<any>tagNewTemplate).template;

   var dNewTemplate={};
   splitViewBuilderHelper.setDocTypeTemplate2($scope,dNewTemplate,eventCommunicator, dialogs, true, false , true);
   var docTypesTemplate = (<any>dNewTemplate).template;

   var depNewTemplate={};
   splitViewBuilderHelper.setDepartmentTemplate($scope,depNewTemplate,eventCommunicator, dialogs, true, false , true);
   var departmentTemplate = (<any>depNewTemplate).template;

   var bizNewTemplate={};
   splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizNewTemplate,eventCommunicator,true);
   var bizListAssociationTemplate = (<any>bizNewTemplate).template;

   var funcRoleTemplate={};
   splitViewBuilderHelper.setFunctionalRolesTemplate($scope,funcRoleTemplate,eventCommunicator,true);
   var functionalRolesTemplate = (<any>funcRoleTemplate).template;

   var fields:ui.IFieldDisplayProperties[] =[];
   fields.push(id);
   fields.push(name);
   fields.push(realPath);
   fields.push(bizListAssociation);
   fields.push(tags);
   fields.push(docTypes);
   fields.push(department);
   fields.push(functionalRoles);
   fields.push(numOfSubFolders);
   fields.push(numOfDirectFiles);
   fields.push(filteredNumOfDirectFiles);
   fields.push(numOfAllFiles);
   fields.push(filteredNumOfAllFiles);

   $scope.contextName = EFilterContextNames.folder;

    //var filteredDirectFilesPart ="# if (  item."+filteredNumOfDirectFiles.fieldName+" >-1 )  " +
    //    "{# #: item."+filteredNumOfDirectFiles.fieldName+" # / # } " +
    //    "# #: item."+numOfDirectFiles.fieldName+"# direct files";
    //var filteredNumOfAllFilesPart = "#if (   item."+filteredNumOfAllFiles.fieldName+"  >-1 )  " +
    //    "{#  #: item."+filteredNumOfAllFiles.fieldName+" # / # } " +
    //    "# #: item."+numOfAllFiles.fieldName+"# all files";
    //var filteredNumOfSubFoldersPart = "#if (  item."+filteredNumOfSubFolders.fieldName+"  >-1 )  " +
    //    "{# #: item."+filteredNumOfSubFolders.fieldName+" # / # } " +
    //    "#  #: item."+numOfSubFolders.fieldName+"# subfolders";


    //var tagsTemplate = tags.template.replace(/item./g,'item.item.');
    //var docTypesTemplate =docTypes.template.replace(/item./g,'item.item.');
    //var bizListAssociationTemplate =bizListAssociation.template.replace(/item./g,'item.item.');
    //
    //var folderName="# if ( item."+name.fieldName+" =='' || item."+name.fieldName+" == null )  " +
    //    "{# #: item."+path.fieldName+" #  # } else {# #: item."+name.fieldName +"# #}#";

    //var treeItemTemplate = "<div style='position:relative;' title='#: item."+path.fieldName+" # '>" +
    //    "<span class='fa fa-folder'></span><span class='title'>"+folderName+"</span>" +
    //    "&nbsp;" + tagsTemplate + "&nbsp;" + docTypesTemplate  + "&nbsp;" + bizListAssociationTemplate + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='right-position' >" +
    //    "<a class='btn btn-link btn-xs ' target='_blank' ui-sref='details({ id: dataItem.item.id, context:contextName})' ><i class='fa fa-info ng-hide'></i></a>"+
    //    "<a class='btn btn-link btn-xs' clipboard text='dataItem." + path.fieldName+"' title='copy path to clipboard'><i class='fa fa-copy ng-hide' ></i></a></span></div>" +
    //    "<div><small>"+filteredDirectFilesPart+"</small>" +
    //       "<small>"+filteredNumOfAllFilesPart+"</small>" +
    //       "<small>"+filteredNumOfSubFoldersPart+"</small>" +
    //    "</div>";

   var tagsTemplate = tagsTemplate.replace(/item./g,'item.item.');
   var docTypesTemplate =docTypesTemplate.replace(/item./g,'item.item.');
   var departmentTemplate =departmentTemplate.replace(/item\./g,'item.item.');
   var bizListAssociationTemplate =bizListAssociationTemplate.replace(/item./g,'item.item.');
   var functionalRolesTemplate =functionalRolesTemplate.replace(/item./g,'item.item.');

   var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat('item.'+numOfAllFiles.fieldName,
       'item.'+filteredNumOfAllFiles.fieldName,'item.'+inFolderPercentage.fieldName);

  // nameNewTemplate= nameNewTemplate.replace(/item./g,'item.item.');
   numOfSubFoldersNewTemplate= numOfSubFoldersNewTemplate.replace(/item./g,'item.item.');

   var treeItemTemplate="<div class='table-row' style='line-height: 20px;'>" +
       `<div class='table-cell icon-column'  style='vertical-align: top'> 
           <folder-type-html type=dataItem.item.folderType ></folder-type-html>
        </div>`+
       "<div  class='table-cell break-text' style=' width:100%;white-space: normal;vertical-align: top;'  >" +
        ` <div class="flex-container-row">
         <div class="auto1 start flex-2"> ${nameNewTemplate}</div>
           <div class="initial end right" style="flex:1;">
         <span style="align-self: flex-end;white-space: nowrap;margin-right: 9px;"> ${filterTemplate} </span>
           </div>
           </div>
           <div class="flex-container-row">
         <div class="initial start" > ${numOfSubFoldersNewTemplate}&nbsp;</div>
           <div class="auto end right" >
           ${functionalRolesTemplate+'&nbsp;&nbsp;  '+bizListAssociationTemplate+'&nbsp;&nbsp;  '+tagsTemplate}
           </div>
           </div>`+
       "</div>" +
       '<div  class="table-cell" style="vertical-align:middle;min-width:125px;" >'+'<div class="flex-container-row">'+docTypesTemplate+'</div>'+'<div class="flex-container-row">'+departmentTemplate+'</div></div>'+
       "</div>";

   var onSelectedItemChanged = function () {
     var folder = (<Entities.DTOFolderInfo>(<Entities.DTOAggregationCountFolder> $scope.selectedItem).item);
   //  var id = folder.id;
    // var path = folder.realPath;
    // prepareFolderName(folder);

     //   var pathHasNetworkLink = _.startsWith(path,'http');
     //   var delimiterSign = pathHasNetworkLink?'/':'\\';
     // var name =folder.rootFolderNickName?folder.rootFolderNickName+ delimiterSign +folder.realPath.substr(folder.rootFolderLength):folder.realPath;
     // name =  name[name.length-1]==delimiterSign? name.substr(0, name.length-1):name;
     $scope.itemSelected( folder.id,folder.name,folder.realPath);
   };


 })
  .controller('FoldersGridCtrl', function ($scope, $state,$stateParams,$window,$element, $location,Logger,$timeout,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                           propertiesUtils, configuration:IConfig,userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs, docTypeResource) {
    var log:ILog = Logger.getInstance('FoldersGridCtrl');

    var _this = this;
    $scope.elementName = 'folders-grid';
    $scope.entityDisplayType = EntityDisplayTypes.folders_flat;

    $scope.InitGrid = function (restrictedEntityDisplayTypeName) {
      $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;

      log.debug('Init FoldersCtrl grid');
      _this.gridOptions = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope, log, configuration, eventCommunicator,userSettings, getDataUrlParams, fields, parseData, rowTemplate, $stateParams, null, null , true);

      GridBehaviorHelper.connect($scope, $timeout, $window, log, configuration, _this.gridOptions, null, fields, onSelectedItemChanged, $element);
 //    $scope.mainGridOptions = _this.gridOptions.gridSettings;
    };

    var getDataUrlParams = function () {
    };

    var dTOAggregationCountFolder:Entities.DTOAggregationCountFolder = Entities.DTOAggregationCountFolder.Empty();
    var dTOFolderInfo:Entities.DTOFolderInfo = Entities.DTOFolderInfo.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.item) + '.';


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.id),
      title: 'ID',
      isPrimaryKey: true,
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true,
    };

    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true,
      template: '<span class="top-right-position" ><a class="btn btn-link btn-xs da-cond-show1 ">' +
        '<i  ng-click="detailsClicked(dataItem.item.id,dataItem.item.name)" class="fa fa-info ng-hide"></i></a></span>'+
        ' #= '+ prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.name)+' # '

    };
    $scope.contextName = EFilterContextNames.folder;
    var realPath:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.realPath),
      title: 'Path',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      width: '35%',
      //template: '<textarea  class="fill-width copy-cell" spellcheck="false" style=" border: none;outline:none;background:none;resize:none;overflow: visible;"  type="text">#= '+prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.path)+' #</textarea>',
      //template: '<input onClick="$(this).closest(\'tr\').addClass(\'k-state-selected\')" class="fill-width" style=" border: none;outline:none;background:none;" readOnly  type="text" value="#= '+prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.path)+' #" />',
      template: "#= " + prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.realPath) + " #" +
        "<span class='top-right-position' >" +
            "<a class='btn btn-link btn-xs da-cond-show1 ' target='_blank' href='{{createDetailsPageUrl( dataItem.item.id, contextName)}}'><i class='fa fa-info ng-hide'></i></a>"+
            "<a clipboard text='dataItem.item.realPath' title='copy path to clipboard'><i class='fa fa-copy btn btn-link btn-xs ng-hide ' ></i></a>" +
        "</span>"
    };

    var numOfAllFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles),
      title: '# All Files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      template: '#if ( '+ propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles)+'>0) {#' +
      ' <span ng-if="filterData&&filterData.filter"> #: '+ propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles)+'#/'+
      '</span> #}#',
    };


    var numOfDirectFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfDirectFiles),
      title: '# Direct files ',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      template:'#:'+ prefixFieldName +  propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfDirectFiles)+'#',
    };


    var numOfSubFoldersNewTemplate = '<span class="title-third" title="All files in subfolders" ng-if="dataItem.item.numOfSubFolders>0" >#: kendo.toString(numOfSubfoldersFiles,"n0") # files in '+
   '#: kendo.toString('+ prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders)+',"n0") # subfolders </span>' +
      '<span class="title-third" title="All files in subfolders" ng-if="dataItem.item.numOfSubFolders<=0">No subfolders </span>';
    //numOfSubFoldersNewTemplate= numOfSubFoldersNewTemplate.replace(/item./g,'item.item.');



    var pathNewTemplate =  '<span class="dropdown dropdown-wrapper" title="#= '+ realPath.fieldName+' #" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown" >'  +
    '<a class="title-primary" >#= '+ name.fieldName+' # <i class="caret no-visible"></i></a></span>'+
    '<ul class="dropdown-menu"  > '+
     '<li><a class="btn btn-link btn-xs " target="_blank" href="{{createDetailsPageUrl( dataItem.item.id, contextName)}}"><span class="fa fa-info"></span> View folder details</a></li>'+

    '<li><a clipboard text="dataItem.item.realPath || dataItem.item.mailboxGroup" title="copy path to clipboard"><span class="fa fa-copy" ></span> Copy full path </a></li>'+
        '<li><a   ng-click="folderNameClicked(dataItem)"><span class="fa fa-filter"></span> Filter by this folder</a></li>'+
     //   '<li ng-if="openFolderUrl(dataItem)"><a href="{{openFolderUrl(dataItem)}}" target="_blank">Open folder in host application</a></li>'+
        ' </ul></span> ';

    $scope.createDetailsPageUrl = function(groupId,context:EFilterContextNames){
      if(groupId) {
        const urlParams = {};
        urlParams['context'] = context.toString();
        urlParams['id'] = groupId;

        var url = $state.href(ERouteStateName.details, urlParams);
        url = url.replace('/v1','');
        return url;
        //  $window.open(url,'_blank');
      }
      return '';
    }
    $scope.folderNameClicked = function(item:Entities.DTOAggregationCountFolder) {
      setFilterByFolder((<Entities.DTOFolderInfo>item.item).id,(<Entities.DTOFolderInfo>item.item).realPath);
    };

    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }


    $scope.openFolderUrl = function(dataItem:Entities.DTOAggregationCountFolder)
    {
      if(startsWith(((<Entities.DTOFolderInfo>dataItem.item).realPath.toLowerCase()),'http'))
      {
        return  (<Entities.DTOFolderInfo>dataItem.item).realPath;
      }
      return null;
    }

    var startsWith = function (str1:string, str2:string):boolean {
      return str1.substring(0, str2.length) === str2;
    };
    var setFilterByFolder = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName,
        includeSubEntityDataLeft:true
      });
    };
    $scope.localDataBound = function() {
    };


    var numOfSubFolders:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders),
      title: 'Subfolders',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };

    var filteredNumOfSubFolders:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders),
      title: 'Filtered subfolders',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };

    var numOfDirectFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfDirectFiles),
      title: '# Direct files ',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      template:'#:'+ prefixFieldName +  propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfDirectFiles)+'#',
    };


    var directFilesFilterPercentage:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'directFilesFilterPercentage',
      title: '% filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template: '#if ( '+ propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfDirectFiles)+'>0 ) {#' +
      '<span>  #: kendo.toString(inFolderPercentage, "n0")#%  </span>'+
      ' #}#',
    };


    var filteredNumOfDirectFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfDirectFiles),
      title: '# Filtered Direct Files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };



    var filteredNumOfAllFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOAggregationCountFolder, dTOAggregationCountFolder.numOfAllFiles),
      title: '# All filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
    };

    var bizListAssociation:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'associatedBizListAsFormattedString', //for excel export
      title: 'Clients association',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,
    };
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizListAssociation,eventCommunicator);

    var tags:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'tagsAsFormattedString', //for excel export
      title: 'Tags',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,
    };


    var docTypes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'docTypesAsFormattedString', //for excel export
      title: 'DocTypes',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,
    };

    var department:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'departmentAsFormattedString', //for excel export
      title: 'Department',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true
    };

    var functionalRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'functionalRolesAsFormattedString', //for excel export
      title: 'Data roles',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };

    var tagNewTemplate={};
    splitViewBuilderHelper.setTagsTemplate2($scope,tagNewTemplate,eventCommunicator);
    var tagsTemplate = (<any>tagNewTemplate).template;

    var dNewTemplate={};
    splitViewBuilderHelper.setDocTypeTemplate2($scope,dNewTemplate,eventCommunicator, dialogs,  false ,false,true);
    var docTypesTemplate = (<any>dNewTemplate).template;

    var depNewTemplate={};
    splitViewBuilderHelper.setDepartmentTemplate($scope,depNewTemplate,eventCommunicator, dialogs,  false ,false,true);
    var departmentTemplate = (<any>depNewTemplate).template;

    var bizNewTemplate={};
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizNewTemplate,eventCommunicator);
    var bizListAssociationTemplate = (<any>bizNewTemplate).template;

    var funcRoleTemplate={};
    splitViewBuilderHelper.setFunctionalRolesTemplate($scope,funcRoleTemplate, eventCommunicator);
    var functionalRolesTemplate = (<any>funcRoleTemplate).template;

    var fields:ui.IFieldDisplayProperties[] = [];
    fields.push(id);
    fields.push(name);
    fields.push(realPath);
    fields.push(bizListAssociation);
    fields.push(department);
    fields.push(docTypes);
    fields.push(tags);
    fields.push(functionalRoles);

    fields.push(filteredNumOfDirectFiles);
    fields.push(numOfDirectFiles);
    fields.push(numOfAllFiles);
  //  fields.push(filteredNumOfSubFolders);
//    fields.push(numOfSubFolders);
    // fields.push(filteredNumOfAllFiles);

    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(numOfDirectFiles.fieldName,
      filteredNumOfDirectFiles.fieldName,directFilesFilterPercentage.fieldName,'Total direct files');

    var visibleFileds = fields.filter(f=>f.displayed);


    /* var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-column' style='padding-top: 1px;'><folder-type-html type=dataItem.item.folderType ></folder-type-html></td>"+
        "<td colspan='"+(visibleFileds.length-3)+"' width='100%' chref=\"folder\"lass='ddl-cell'>" +
        "<span class='pull-right' style='max-width: 46%;'>" +
        "<span class='pull-right'>"+functionalRolesTemplate+'&nbsp;&nbsp;  '+bizListAssociationTemplate+'&nbsp;&nbsp;  '+tagsTemplate+"</span>" +
        "<div style='clear:right'></div>"+
        "<span style='float: right'>" +docTypesTemplate+"</span></span>" +
     "</span>" +
         pathNewTemplate+"<div></div></td>" +
          '<td colspan="1" style="width:130px;" class="ellipsis">'+filterTemplate+'</td>'+
        "</tr>";  */

    var rowTemplate=`<tr  data-uid='#: uid #' colspan="+fields.length+" ng-class='{\"freezeRow disabled1\":dataItem.updatingIndex}'>
          <td colspan='1' class='icon-column' ><folder-type-html type=dataItem.item.folderType ></folder-type-html></td>
          <td colspan='${visibleFileds.length-3}' width='100%' class='ddl-cell'>
          <div class="flex-container-row break-text">
              <div class="auto1 start flex-2">
                   <div> ${pathNewTemplate}</div>
                   <div> ${numOfSubFoldersNewTemplate}</div>
              </div>
              <div class="initial end right flex-container-column" style="flex:1;">
                <div class="auto end right" >
                  <span style="align-self: flex-end;white-space: nowrap;margin-right: 9px;"> ${filterTemplate} </span>
                </div>  
                <div class="auto end right" >
                  ${functionalRolesTemplate+'&nbsp;&nbsp;  '+bizListAssociationTemplate+'&nbsp;&nbsp;  '+tagsTemplate}
                </div>
              </div>
           </div>
          </td>
          <td colspan="1" style="width:130px;vertical-align: middle" class="ellipsis "><div class="flex-container-row">${docTypesTemplate}</div><div class="flex-container-row">${departmentTemplate}</div></td>`;

    var parseData = function (folderAggregation:Entities.DTOAggregationCountFolder[]) {
      if(folderAggregation) {
        folderAggregation.map(function (g:Entities.DTOAggregationCountFolder) {
          var folder:Entities.DTOFolderInfo = <Entities.DTOFolderInfo>(g.item);
          (<any>g).directFilesFilterPercentage = splitViewBuilderHelper.getFilesNumPercentage(g.numOfDirectFiles , folder.numOfDirectFiles);
          splitViewBuilderHelper.parseTags(<Entities.ITaggable>folder);
          splitViewBuilderHelper.parseDocTypes(<Entities.IDocTypesAssignee>folder,configuration);
          splitViewBuilderHelper.parseDepartment(<Entities.IDepartmentAssignee>folder,configuration);
          splitViewBuilderHelper.parseAssociatedBizListItems(<Entities.IBizListAssociatable>folder);
          splitViewBuilderHelper.parseFunctionalRoles(<Entities.IFunctionalRolesAssignee>folder);
          splitViewBuilderHelper.parseFilesAndFilterTemplate(<Entities.IFunctionalRolesAssignee>folder);

          (<any>g).numOfSubfoldersFiles = folder.numOfAllFiles - g.numOfDirectFiles;
          folder.name = folder.rootFolderNickName ? folder.rootFolderNickName  + folder.realPathNoPrefix : folder.realPath;
          if(folder.realPath.indexOf('/')>0) {
            folder.name = folder.name[folder.name.length - 1] == '/' ? folder.name.substr(0, folder.name.length - 1) : folder.name;
          }
          else {
            folder.name = folder.name[folder.name.length - 1] == '\\' ? folder.name.substr(0, folder.name.length - 1) : folder.name;
          }
        });
        return folderAggregation;
      }
      return null;
    };

    var onSelectedItemChanged = function () {
      $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[name.fieldName],$scope.selectedItem[id.fieldName]);
    };
  })
  .directive('foldersTree', function(){
     return {
     restrict: 'EA',
     template: '<div class="fill-height1 folders-tree"  kendo-tree-view ng-transclude k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound(e)"  k-options="mainTreeOptions" ></div>',
     replace: true,
     transclude:true,
     scope:
     {
       itemSelected: '=',
       itemsSelected: '=',
       expandFolder: '=',
       unselectAll: '=',
       restrictedEntityId:'=',
       lazyFirstLoad: '=',
       filterData: '=',
       reloadDataToTree: '=',
       refreshData:'=',
       getCurrentUrl: '=',
       getCurrentFilter: '=',
       findByPath: '=',
       includeSubEntityData:'=',
       changeSelectedItem: '=',
       findId: '=',
       localDataBound: '=',
       filterBySearchText: '=',
       getFindByPathValue: '&'
     },
     controller: 'FoldersTreeCtrl',
     link:
         function (scope:any, element, attrs) {
           scope.InitTree(attrs.restrictedentitydisplaytypename);
         }
     }
  })
  .directive('foldersFlatGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height fill-width {{elementName}}" kendo-grid ng-transclude k-on-change="handleSelectionChange(data, dataItem, columns)"  k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope:
      {
        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        itemsSelected: '=',
        unselectAll: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        totalElements: '=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData:'=',
        changeSelectedItem: '=',
        findId: '=',
        localDataBound: '=',
        filterBySearchText: '='
     },
      controller: 'FoldersGridCtrl',
      link:
          function (scope:any, element, attrs) {
            scope.InitGrid(attrs.restrictedentitydisplaytypename);
          }
    }
  });

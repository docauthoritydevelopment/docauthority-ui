///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('folders.server',[

    ])
    .controller('ServerFoldersTreeCtrl', function ($scope, $state,$stateParams,$window,$element, $location,Logger,$timeout,
                                                   eventCommunicator:IEventCommunicator,propertiesUtils, configuration:IConfig,dialogs,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log: ILog = Logger.getInstance('ServerFoldersTreeCtrl');

      $scope.entityDisplayType = EntityDisplayTypes.folders;
      $scope.elementName='server-folders-tree';

      var elementOptions;

      $scope.InitTree = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        log.debug('Init FoldersCtrl tree');
        $scope.getFindByPathValue = function(dataItem:Operations.DtoServerResource){
          return dataItem? dataItem.fullName.toLowerCase(): null;
        }
        elementOptions=
            TreeBehaviorHelper.createTree<string>($scope, log,configuration,eventCommunicator, getDataUrlParams,
                fields,treeItemTemplate, parseData, hasChildren,getDataUrlBuilder);

        var doNotExpandAndSelectFirstByDefault=true;
        TreeBehaviorHelper.connect($scope, $timeout, $window, log,configuration,  elementOptions, onSelectedItemChanged,$element,
            doNotExpandAndSelectFirstByDefault,$scope.paddingBottom);

      };

      var parseData = function (folders: Operations.DtoServerResource[]) {
        if(folders)
        {
          folders.forEach(function (f) {
            f.hasChildren = f.hasChildren===null ?true:f.hasChildren;
            f.accessible = f.accessible===null ?true:f.accessible;
            if(!f.accessible)
            {
              f.hasChildren=false;
            }
            if($scope.restrictedEntityDisplayTypeName != Operations.EMediaType.BOX ){
              f.id = f.fullName;
            }
          });
         }
        return folders;
      };

      var hasChildren  = function(e:Operations.DtoServerResource)
      {
        return e.hasChildren;
      };

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig,includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState) {
        if($scope.restrictedEntityDisplayTypeName == Operations.EMediaType.SHARE_POINT || $scope.restrictedEntityDisplayTypeName == Operations.EMediaType.ONE_DRIVE  || $scope.restrictedEntityDisplayTypeName == Operations.EMediaType.BOX )
        {
          var url =   configuration.media_type_server_folders_by_datacenter_url;
          url =  url.replace('{connectionId}',$scope.mediaConnectionId?$scope.mediaConnectionId.toString():'');
          url =  url.replace('{customerDataCenterId}',$scope.restrictedEntityTypeId?$scope.restrictedEntityTypeId.toString():'');
          url = url.replace(':mediaType',$scope.restrictedEntityDisplayTypeName == Operations.EMediaType.SHARE_POINT?'sharepoint':$scope.restrictedEntityDisplayTypeName == Operations.EMediaType.ONE_DRIVE?'onedrive':'box');
          if($scope.restrictedEntityId) {
            return url.replace('{fullPath}',encodeURIComponent($scope.restrictedEntityId));
          }
          return url.replace('id={fullPath}','');
        }
        if($scope.restrictedEntityId) {
          return configuration.server_folders_url.replace('{fullPath}',encodeURIComponent($scope.restrictedEntityId));
        }
        return configuration.server_folders_url.replace('id={fullPath}','');
      };

      var getDataUrlParams = function()
      {
        var urlParams =  {'take':10000,'page':1,'reldepth':1};
        if($scope.filterData) {
          (<any>urlParams).filter = JSON.stringify($scope.filterData.filter);
        }
        return urlParams;

      };


      var dTOServerResource:Operations.DtoServerResource =  Operations.DtoServerResource.Empty();

      var id:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
        fieldName:   'id',
        title: 'Id',
        type: EFieldTypes.type_string,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,
      };

      var path:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
        fieldName:   propertiesUtils.propName(dTOServerResource, dTOServerResource.fullName),
        title: 'Path',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
      };


      var name:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
        fieldName:   propertiesUtils.propName(dTOServerResource, dTOServerResource.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
      };

      var accessible:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
        fieldName:   propertiesUtils.propName(dTOServerResource, dTOServerResource.accessible),
        title: 'Accessible',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: true,
      };




      var fields:ui.IFieldDisplayProperties[] =[];

      fields.push(id);
      fields.push(name);
      fields.push(path);
      fields.push(accessible);

      var accessibleIcon = '<span ng-show="!dataItem.accessible" title="Folder not accessible" class="fa fa-exclamation-triangle notice">&nbsp;</span>';
  //    var treeItemTemplate ="<span class='"+configuration.icon_folder+"'></span><span class='title'>#: item."+name.fieldName+" # &nbsp;"+accessibleIcon+"</span></span>" ;
     var treeItemTemplate="<div class='table-row' >" +
          "<div class='table-cell icon-column'><span class='"+configuration.icon_folder+"'></span></div>"+
          "<div  class='table-cell' style=' width:100%;white-space: normal;'  >#: item."+name.fieldName+" # &nbsp;"+accessibleIcon+"</div>" +
          "</div>";

      var onSelectedItemChanged = function () {
        $scope.itemSelected($scope.selectedItem[path.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem[path.fieldName]);
      };

    })
    .directive('serverFoldersTree', function(){
      return {
        restrict: 'EA',
        template: '<div class="fill-height1 folders-tree"   kendo-tree-view ng-transclude k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound(e)"  k-options="mainTreeOptions" ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          itemSelected: '=',
          itemsSelected: '=',
          expandFolder: '=',
          unselectAll: '=',
          restrictedEntityId:'=',
          lazyFirstLoad: '=',
          filterData: '=',
          reloadDataToTree: '=',
          refreshData:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          findByPath: '=',
          includeSubEntityData:'=',
          changeSelectedItem: '=',
          findId: '=',
          paddingBottom:'=',
          initialized: '=',
          mediaConnectionId:'=',
          restrictedEntityTypeId:'=',
          getFindByPathValue: '&'

        },
        controller: 'ServerFoldersTreeCtrl',
        link:
            function (scope:any, element, attrs) {
              scope.InitTree(attrs.restrictedentitydisplaytypename);
            }
      }
    })

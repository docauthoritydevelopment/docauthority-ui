///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('groups', [
  // 'directives',
  'ui.router',
  'resources.groups',
  'directives.dialogs.group.grouping',
  'directives.dialogs.debug',
  'directives.dialogs.subGroup'
]);

angular.module('groups')
  .config(function ($stateProvider) {
    $stateProvider
      .state('groups', {
        //   abstract: true,
        url: '/groups?id',
        templateUrl: '/app/groups/groups.tpl.html',
        controller: 'GroupsPageCtrl'
      })

  })
  .controller('GroupsPageCtrl', function ($scope, $state, $location, Logger, Alerts) {
    var log:ILog = Logger.getInstance('GroupsPageCtrl');


    $scope.Init = function () {

    };

    $scope.alerts = Alerts.init();
    //
    //$scope.onExpandDetails = function (groupId, name) {
    //  log.debug('onExpandDetails was called with '+name);
    //  $state.go('groupFiles', {groupId: groupId, name: name});
    //};

  })
  .controller('GroupsCtrl', function ($scope, $state, $window,$element, $location, Logger, $timeout, $stateParams,groupResource:IGroupResource,dialogs,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils,fileResource:IFileResource,routerChangeService, configuration:IConfig,currentUserDetailsProvider:ICurrentUserDetailsProvider, eventCommunicator:IEventCommunicator,groupsResource:IGroupsResource, userSettings:IUserSettingsService,docTypeResource) {
    var log:ILog = Logger.getInstance('GroupsCtrl');

    $scope.elementName = 'groups-grid';
    $scope.entityDisplayType = EntityDisplayTypes.groups;

    var gridOptions:ui.IGridHelper ;
    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      gridOptions=
        GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope, log,configuration,eventCommunicator, userSettings,  getDataUrlParams,
          fields, parseData, rowTemplate,
          $stateParams, null, null, true);


      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);
      //eventCommunicator.registerHandler('ExcelGroupsExport'
      //  ,$scope
      //  , (new RegisteredEvent(function(registrarScope:any, registrationParam?:any, fireParam?:any) {
      //        _this.excelExport();
      //  })));

      //   $scope.mainGridOptions = gridOptions.gridSettings;
      updatePermissions();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        updatePermissions();
      });
    };

    var updatePermissions=function()
    {

      $scope.isGeneralAdminUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.GeneralAdmin]);
    }

    function textEditorInitialize(container, options) {
      var element = $('<textarea class="form-control" data-bind="value:' + options.field + '" />');
      var acceptElement = $('<a class="btn btn-link "><span class="fa fa-check"></span></a>');
      var declineElement = $('<a class="btn btn-link "><span class="fa fa-times"></span></a>');
      var toolbar = $('<div style="position:absolute;right:0px;bottom:0;z-index: 999;"></div>');
      // toolbar.append(acceptElement);
      toolbar.append(declineElement);

      //  element.css({'width': container.width()});
      // element.css({'height': 'auto', 'overflow-y': 'hidden'}).height((<any>element).scrollHeight);
      //autosize(element);
      container.append(element);
      container.append(toolbar);

      var grid = $("."+$scope.elementName);
      var dataUid=$(container.closest('tr')).attr('data-uid');
      var dataItem:Entities.DTOGroup = gridOptions.getRowDataByUid(grid,dataUid).item;
      var initialVal = dataItem.groupName;

      element.focus();

      //element.bind( "blur",(function() {
      //  stopEditMode();
      //}));
      function stopEditMode()
      {
        gridOptions.closeEditCellMode(grid);
      }
      function acceptUpdate()
      {
        var groupName=element.val();

        doUpdateGroupName(dataItem.id,groupName,function(){
          dataItem.groupName = groupName;
          if($scope.markedItemForJoin &&$scope.markedItemForJoin.item.id==dataItem.id)
          {
            $scope.markedItemForJoin.name = dataItem.groupName;
            $scope.markedItemForJoin.item = dataItem;
          }
          notifyGroupNameRenamed(dataItem);
          stopEditMode();
          //      gridOptions.refreshDisplay(grid)
        });
      }

      function declineUpdate()
      {
        event.preventDefault();
        element.val(initialVal);
        element.blur();
        stopEditMode();
      }
      acceptElement.click(function(event) {
        event.preventDefault();
        acceptUpdate();

      });
      declineElement.click(function(event) {
        event.preventDefault();
        declineUpdate();
      });
      element.blur(function(event) {

        acceptUpdate();
      });
      element.keydown(function(event){

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == 13){ //enter
          event.preventDefault();
          acceptUpdate();
        }
        if(keycode == 27) { //esc
          event.preventDefault();
          declineUpdate();

        }

      });
    };

    function doUpdateGroupName(groupId,groupName,afterEdit)
    {
      groupResource.updateGroupName(groupId,groupName,afterEdit,onError)
    }

    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem< Entities.DTOGroup> = Entities.DTOAggregationCountItem.Empty();
    var dTOGroup:Entities.DTOGroup = Entities.DTOGroup.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.id),
      title: 'ID',
      type: EFieldTypes.type_string,
      isPrimaryKey: true,
      displayed: false,
      editable: false,
      nullable: true,
      template: '<a href="" ng-click="itemSelected(\'#: id#\',\'#: groupName#\')">#= id.substr(0,8) #</a>',
    };
    var groupName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.groupName),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: true,
      nullable: false,
      width: "35%",
      validation: {required: true},
      template: '<span class="top-right-position" ><a class="btn btn-link btn-xs da-cond-show1 " target="_blank" ' +
      'ui-sref="details({ id: dataItem.item.id, context:contextName,name:dataItem.item.groupName})">' +
      '<i class="fa fa-info ng-hide1 no-visible"></i></a>' +
      '<a class="btn btn-link btn-xs"><i  ng-click="nameClicked($event)" class="fa fa-pencil ng-hide1 no-visible"></i></a>' +
      "<a ng-show='selectedItems.length>1' ng-click='joinGroupClicked()' title=' Join selected groups'><i class='"+configuration.icon_group+" ng-hide1 no-visible'></i> </a>"+
      '</span> #= '+ prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.groupName)+' # ',
  //    editor: textEditorInitialize
    };



    var groupNameNewTemplate= '<span class="dropdown dropdown-wrapper" style="word-break: break-word;"><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
      '<a class="title-primary">#= '+ prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.groupName)+' # ' +
      '<i class="caret ng-hide1 no-visible"></i></a></span>'+
      '<ul ng-if="!dataItem.updatingIndex" class="dropdown-menu" > '+
      '<li ng-if="isGeneralAdminUser && dataItem.groupJoinProcessing && dataItem.originalChildGroup"><a class="btn btn-link btn-xs" ng-click="tryRecoverGroup(dataItem)"><span class="fa fa-wrench"></span>&nbsp; Force index update</a></li>' +
      '<li ng-if="dataItem.mngGroupAuthorized"><a class="btn btn-link btn-xs" ng-click="openEditNameDialog(dataItem)"><span   class="fa fa-pencil"></span> Edit group name</a></li>' +
      '<li ng-if="dataItem.mngGroupAuthorized&&(dataItem.isAnalysisGroup||dataItem.isSubGroup)"><a class="btn btn-link btn-xs" ng-click="openSubGroupDialog(dataItem)"><span class="'+configuration.icon_group+' tag-SUB_GROUP"></span>&nbsp;Sub group</a></li>' +
      '<li><a class="btn btn-link btn-xs da-cond-show1 " target="_blank" ' +
      'href="{{createDetailsPageUrl(dataItem.item.id,contextName)}}"><span class="fa fa-info"></span> View group details</a></li>'+
      "<li><a  ng-click='filterByGroupClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this group</a></li>"+
      "<li ng-if='!dataItem.marked'><a  ng-click='markItem(dataItem)'   title='Mark group for later adding files to group'> <span class='"+configuration.icon_markedItem+"'></span>&nbsp;Mark group</a></li>"+
      "<li ng-if='dataItem.marked'><a  ng-click='unmarkItem(dataItem)'   title='UnMark group for later adding files to group'><span class='"+configuration.icon_unmarkedItem+"'></span>&nbsp; Un-Mark group</a></li>"+
      "<li  ng-if='dataItem.mngGroupAuthorized&&!dataItem.isSubGroup'><a  ng-click='openJoinGroupsDialog(dataItem)'   title='Join groups'><span class='tag-USER_GROUP "+configuration.icon_group+"'></span>&nbsp; Join groups</a></li>"+

      ' </ul>' +
      '<ul ng-if="dataItem.updatingIndex && isGeneralAdminUser" class="dropdown-menu" > '+
       '<li ng-if="isGeneralAdminUser && ! dataItem.subGroupApplyIsRunning"><a class="btn btn-link btn-xs" ng-click="tryRecoverGroup(dataItem)"><span class="fa fa-wrench"></span>&nbsp; Force index update</a></li>' +
       '<li ng-if="isGeneralAdminUser && dataItem.subGroupApplyIsRunning"><a class="btn btn-link btn-xs" ng-click="forceReapplyGroup(dataItem)"><span class="fa fa-wrench"></span>&nbsp; Force re-apply rules</a></li>' +
      ' </ul>' +
      '</span> ';

    $scope.createDetailsPageUrl = function(groupId,context:EFilterContextNames){
      if(groupId) {
        const urlParams = {};
        urlParams['context'] = context.toString();
        urlParams['id'] = groupId;

        var url = $state.href(ERouteStateName.details, urlParams);
        url = url.replace('/v1','');
        return url;
      //  $window.open(url,'_blank');
      }
      return '';
    }
    $scope.openJoinGroupsDialog = function()
    {

      if ($scope.selectedItems && $scope.selectedItems.length > 0) {

        var dlg = dialogs.create(
          'common/directives/reports/dialogs/joinGroupsDialog.tpl.html',
          'joinGroupsDialogCtrl',
          {
            title: 'Join file groups',
            initialGroups:(<Entities.DTOAggregationCountItem<Entities.DTOGroup>[]>$scope.selectedItems).map(g=>g.item)
          },
          {size: 'md'});

        dlg.result.then(function (data) {
          var joindGroup = new Entities.DTONewJoinedGroupDto();
          joindGroup.childrenGroupIds=(<DTOMarkedItem[]>data.selectedGroups).map(g=>g.id);
          joindGroup.groupName=data.newGroupName;
          var userGroups =data.selectedGroups.filter(g=>g.groupType == Entities.EGroupType.USER_GROUP);
          if(userGroups.length==0)
          {
            groupsResource.createJoinedGroup(joindGroup,function(newjoindGroup:Entities.DTOGroupDetails){

              $scope.markItem(newjoindGroup.groupDto,joindGroup.childrenGroupIds);
              gridOptions.selectPrevRow( $scope.element);
              $scope.refreshScreenData();
              routerChangeService.updateAsyncUserOperations();
            }, function(error){
              if (error.status == 400 &&  error.data && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                dialogs.error('Join group', 'There is already running operation for this group.');
              }
              else{
                onError();
              }
            });
          }
          else {
            var largestUserGroup:Entities.DTOGroup = data.largestUserGroup; //the largest group out of the user groups only
            groupsResource.addGroupsToJoinedGroup(largestUserGroup.id, joindGroup,function(newjoindGroup:Entities.DTOGroupDetails){
              $scope.markItem(newjoindGroup.groupDto,joindGroup.childrenGroupIds);
              $scope.refreshScreenData();
              routerChangeService.updateAsyncUserOperations();
            }, function(error){
              if (error.status == 400&&  error.data  && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                dialogs.error('Join group', 'There is already running operation for this group.');
              }
              else{
                onError();
              }
            });
          }

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      }
    }

    $scope.refreshScreenData = function() //in order to refresh file count after dettache/attach to group
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDataChangeUserAction, {
        action: 'refreshSplitView',
      });
    };
    $scope.markItem = function(dataItem:Entities.DTOGroup,removeIds:string[])
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
        action: 'markItem',
        removeIds:removeIds,
        id: dataItem.id,
        name: dataItem.groupName,
        entityDisplayType:$scope.entityDisplayType,
      });
    };
    $scope.unmarkItem = function(dataItem:Entities.DTOGroup)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
        action: 'removeMarkItem',
        id: dataItem.id,
        entityDisplayType:$scope.entityDisplayType,
      });
    };

    var unmarkItems = function(groupIds:string[])
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
        action: 'removeMarkItems',
        ids: groupIds,
        entityDisplayType:$scope.entityDisplayType,
      });
    };


    var  notifyGroupNameRenamed = function(dataItem:Entities.DTOGroup)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
        action: 'itemNameChanged',
        id: dataItem.id,
        name: dataItem.groupName,
        entityDisplayType:$scope.entityDisplayType,
      });
    };


    $scope.filterByGroupClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOGroup>) {

      setFilterByEntity(item.item.id,item.item.groupName);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    $scope.openEditNameDialog = function(item:Entities.DTOAggregationCountItem<Entities.DTOGroup>) {

      var dlg = dialogs.create(
        'common/directives/dialogs/genericDialogs.tpl.html',
        'openSingleInputDialogCtrl',
        {bodyText:'Type new group name',title:'Edit group name',initialValue:item.item.groupName},
        {size:'md'});

      dlg.result.then(function(groupName){
        groupResource.updateGroupName(item.item.id,groupName,function(group:Entities.DTOGroup){
          item.item.groupName=group.groupName;
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDataChangeUserAction, {
            action: 'reloadData',
          });
        },onError)
      },function(){

      });
    };

    $scope.openSubGroupDialog = function(item:Entities.DTOAggregationCountItem<Entities.DTOGroup>) {

      if(item.item.groupType == Entities.EGroupType.ANALYSIS_GROUP || item.item.groupType == Entities.EGroupType.SUB_GROUP) {
        var dlg = dialogs.create(
          'common/directives/reports/dialogs/subGroupDialog.tpl.html',
          'subGroupDialogCtrl',
          {
            title: 'Sub group',
            group:item.item
          },
          {size: 'md'});

        dlg.result.then(function (tokesStr) {
          var groupId =  item.item.groupType == Entities.EGroupType.SUB_GROUP?item.item.rawAnalysisGroupId:item.item.id;
          if(tokesStr == null){
            var dlg = dialogs.confirm('Confirmation', `You are about to delete sub group cluster for group named: '${item['item.groupName']}'. Continue?`,null );
            dlg.result.then(function(btn){ //user confirmed deletion

              groupResource.deleteAllRefinementRules(groupId, function ()  {
                applyRules(groupId);
              }, function () {
                dialogs.error('Error', 'Failed to revert sub group cluster.');
              })
            },function(btn){
            });
           }
          else {
            var dlg = dialogs.confirm('Confirmation', `You are about to create sub group cluster for group named: '${item['item.groupName']}'. Continue?`,null );
            dlg.result.then(function(btn){ //user confirmed deletion

              groupResource.setRefinementRules(groupId, !tokesStr ? [] : tokesStr.split(','), function (rules) {
                applyRules(groupId);
              }, function (e) {
                if(e.status==400 && e.data.type==ERequestErrorCode.ITEM_ALREADY_EXISTS)
                {
                  dialogs.error('Input error','Same rule appears more then once!');
                }
                else {
                  dialogs.error('Error', 'Failed to set sub groups rules.');
                }

              })
            },function(btn){

            });
          }
        }, function () {
            //user cancel
        });
      }

    };
    $scope.infoClicked = function(groupId,weightData:GroupNaming.DTOFactors,saveNewName:boolean){
      if(groupId)
      {
        groupResource.groupNamingDebugData(groupId,weightData,saveNewName,function(data:GroupNaming.DTOGroupNamingDebugData){
          createGroupNamingDebugDataDialog(data);
        },function()
        {

        })
      }
    };
    $scope.tryRecoverGroup = function(group:Entities.DTOAggregationCountItem<Entities.DTOGroup>){
      let groupId = (<any>group).originalChildGroup?  (<any>group).originalChildGroup.id:group.item.id;
        groupResource.recoverGroupFromFreeze(groupId,function(data){
            $scope.refreshData();
        },function()
        {
          $scope.refreshData();
        })

    };
    $scope.forceReapplyGroup = function(item:Entities.DTOAggregationCountItem<Entities.DTOGroup>){
      var groupId =  item.item.groupType == Entities.EGroupType.SUB_GROUP?item.item.rawAnalysisGroupId:item.item.id;
      applyRules(groupId);
    };
    var applyRules = function(groupId: string) {
      groupResource.applyRefinementRules(groupId, function () {
        $scope.refreshData();
        routerChangeService.updateAsyncUserOperations();
      }, function (e) {
        if(e.status==400 && e.data.type==ERequestErrorCode.GROUP_LOCKED)
        {
          dialogs.notify('Note','Sub grouping is already running for this group.');
        }
        else {
          dialogs.error('Error', 'Failed to apply sub groups rules.');
        }
      })
    }

    var createGroupNamingDebugDataDialog = function(data:GroupNaming.DTOGroupNamingDebugData)
    {

      var dlg = dialogs.create(
        'common/directives/dialogs/reports/dialogs/groupNamingDialog.tpl.html',
        'groupNamingDialogCtrl',
        data,
        {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

      dlg.result.then(function(data){
        if(data && data.restart)
        {
          $scope.infoClicked(data.groupId,data.weightData,data.saveNewName);
        }

      },function(){

      });
    };

    $scope.nameClicked = function(event) {
      var grid = angular.element('.'+$scope.elementName);
      if(grid[0]) {
        gridOptions.editCellMode(grid, event.currentTarget);
      }

    };





    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.numOfFiles),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template: '<small class="title-third1"> Size: #= kendo.toString(' + prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.numOfFiles)+',"n0") # files </small>'

    };


    var firstMemberName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.firstMemberName),
      title: 'first member',
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true
    };
    var firstMemberId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName + propertiesUtils.propName(dTOGroup, dTOGroup.firstMemberId),
      title: 'first member id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true
    };
    var firstMemberFullPath:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'firstMemberFullPath', //for excel export
      title: 'Sample member',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      ellipsis:true,
      template: '<span title="#: ' + firstMemberName.fieldName + '#">#: ' + firstMemberName.fieldName + '#</span>'+
      "<span class='top-right-position' >" +
      "<a ng-if='dataItem.openFileAuthorized' class=' btn btn-link btn-xs' title='Open file' href='" + configuration.file_download_url
        .replace('{id}', '#: ' + firstMemberId.fieldName + ' #') +"' target='_top'><i class='fa fa-download ng-hide1 no-visible' > @@@Resolve@@@</i></a>" +
      "<a class=' btn btn-link btn-xs' title='Open file' title='Open file' href='{{openFileUrl(dataItem)}}' target='{{openFileUrlTarget(dataItem)}}'><i class='fa fa-download ng-hide1 no-visible' ></i> @@@Resolve@@@</a>" +
      "</span>"
    };
    var firstMemberFullPathTemplate = '<span ng-if="dataItem.'+firstMemberId.fieldName+'==-1"></span>' +
      '<span ng-if="dataItem.openFileAuthorized && dataItem.'+firstMemberId.fieldName+'!=-1" class="dropdown dropdown-wrapper " ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
      '<a class="title-third" title="Sample file : #: firstMemberFullPath #">#:' + firstMemberName.fieldName + ' # <i class="caret ng-hide1 no-visible"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      "<li><a  ng-if='dataItem.openFileAuthorized && !dataItem.openFileCheckRequired ' title='Open file' href='{{openFileUrl(dataItem)}}' target='_top'><span class='fa fa-download' > </span> Open file</a></li>" +
      "<li><a  ng-if='dataItem.openFileAuthorized && dataItem.openFileCheckRequired ' title='Open file' ng-click='isFileContentAuthorized(dataItem)'><span class='fa fa-download' > </span> Open file</a></li>" +
      "<li><a  ng-if='dataItem.openFileAuthorized&&dataItem.fileHasNetWorkLink&&false' title='Open file' href='#: firstMemberFullPath #' target='_blank'><span class='fa fa-download' > </span> Open file in host application</a></li>" +

      ' </ul></span> ' +
      '<span ng-if="!dataItem.openFileAuthorized" class="title-third" title="Sample file : #: firstMemberFullPath #">#:' + firstMemberName.fieldName + ' #</span>';

    $scope.isFileContentAuthorized = function(dataItem:Entities.DTOAggregationCountItem<Entities.DTOGroup>){
      fileResource.isFileContentAuthorized(dataItem.item.firstMemberId,function(isAuthorized:boolean){
        if(isAuthorized){
          $window.open($scope.openFileUrl(dataItem), "_top");
        }
        else{
          dialogs.notify("Operation canceled","Sorry, you do not have permissions to view this file content.")
        }
      },onError);
    }

    $scope.openFileUrl = function(dataItem:Entities.DTOAggregationCountItem<Entities.DTOGroup>){

      return configuration.file_download_url.replace('{id}',dataItem.item.firstMemberId.toString());
    }

    var startsWith = function (str1:string, str2:string):boolean {
      return str1.substring(0, str2.length) === str2;
    };

    var numOfFilteredFiles :ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template: '<span class="filter-data" > #: '+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count)+'# </span>'
    };

    var numOfFilteredFilesNewTemplate ='#if ( '+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count)+'>0) {#' +
      ' <span ng-if="(filterData&&filterData.filter)||restrictedEntityId"> #: '+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count)+'#/'+
      '</span> #}#';


    var filterPercentage:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'filterPercentage',
      title: '% filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template: '<span style="white-space: nowrap" ng-if="(filterData&&filterData.filter)||restrictedEntityId">#: kendo.toString(filterPercentage, "n0")# %</span>',

    };

    var bizListAssociation:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'associatedBizListAsFormattedString', //for excel export
      title: 'Clients association',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizListAssociation,eventCommunicator);


    var tags:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'tagsAsFormattedString', //for excel export
      title: 'Tags',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };

    var docTypes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+'docTypesAsFormattedString', //for excel export
      title: 'DocTypes',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,
    };


    var functionalRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName + 'functionalRolesAsFormattedString', //for excel export
      title: 'Data roles',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };

    var tagNewTemplate={};
    splitViewBuilderHelper.setTagsTemplate2($scope,tagNewTemplate,eventCommunicator);
    var tagsTemplate = (<any>tagNewTemplate).template;

    var dNewTemplate={};
    splitViewBuilderHelper.setDocTypeTemplate2($scope,dNewTemplate,eventCommunicator, dialogs,false,false,true);
    var docTypesTemplate = (<any>dNewTemplate).template;

    var bizNewTemplate={};
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizNewTemplate,eventCommunicator);
    var bizListAssociationTemplate = (<any>bizNewTemplate).template;

    var funcRoleTemplate={};
    splitViewBuilderHelper.setFunctionalRolesTemplate($scope,funcRoleTemplate,eventCommunicator);
    var functionalRolesTemplate = (<any>funcRoleTemplate).template;

    var fields:ui.IFieldDisplayProperties[] = [];
    fields.push(id);
    fields.push(groupName);
    fields.push(numOfFiles);
    fields.push(numOfFilteredFiles);
    fields.push(filterPercentage);
    fields.push(bizListAssociation);
    fields.push(docTypes);
    fields.push(functionalRoles);
    fields.push(tags);
    fields.push(firstMemberFullPath);
    fields.push(firstMemberName);
    fields.push(firstMemberId);
    //    fields.push(firstMemberPath);
    //    fields.push(numOfHitEntities);
    //    fields.push(numOfHitFiles);


    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(
      propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),filterPercentage.fieldName);

    var groupJoiningIcon=  ' <span ng-if="dataItem.updatingIndex || dataItem.groupJoinProcessing"> (updating index)</span>';
    var markedItemIcon=  splitViewBuilderHelper.getMarkedItemTemplate($scope,eventCommunicator);

    var visibleFileds = fields.filter(f=>f.displayed);
    // var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+" ng-class='{\"freezeRow disabled1\":dataItem.updatingIndex}'>" +
    //   "<td colspan='1' class='icon-column' >"+
    //   "<span title='{{getIconTooltip(dataItem)|translate}}' class='"+configuration.icon_group+" tag-{{getGroupType(dataItem)}}'></span></td>"+
    //   //"<td colspan='1' class='icon-wrap-2-lines' style='padding: 3px;vertical-align: middle;'>"+
    //   //       " <da-icon class='icon-wrap icon-wrap-2-lines' href='group'></da-icon>" +
    //   //  "</td>"+
    //   "<td colspan='"+(visibleFileds.length-3)+"' width='100%' class='ddl-cell'>" +
    //   "<span class='pull-right' style='max-width: 46%;'>" +
    //   "<span class='pull-right'>"+functionalRolesTemplate+'&nbsp;&nbsp;  '+bizListAssociationTemplate+'&nbsp;&nbsp;  '+tagsTemplate+"</span>" +
    //   "<div style='clear:right'></div>"+
    //   "</span>" +
    //   "</span>" +
    //   groupNameNewTemplate+markedItemIcon+groupJoiningIcon+"<div>"+firstMemberFullPathTemplate+"</div></td>" +
    //   //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+
    //   '<td colspan="1" style="width:250px" class="ellipsis">'+filterTemplate+
    //   '<span class="choose-doc-type-area1">' +docTypesTemplate+'</span>' +
    //   '</td>'+
    //   "</tr>";
    var rowTemplate=`<tr  data-uid='#: uid #' colspan="+fields.length+" ng-class='{\"freezeRow disabled1\":dataItem.updatingIndex}'>
          <td colspan='1' class='icon-column' >
               <span title='{{getIconTooltip(dataItem)|translate}}' class='${configuration.icon_group} tag-{{getGroupType(dataItem)}}'></span>
          </td>
          <td colspan='${visibleFileds.length-3}' width='100%' class='ddl-cell'>
          <div class="flex-container-row break-text">
              <div class="auto1 start flex-2"> ${groupNameNewTemplate+markedItemIcon+groupJoiningIcon}</div>
              <div class="initial end right" style="flex:1;">
                <span style="align-self: flex-end;white-space: nowrap;margin-right: 9px;"> ${filterTemplate} </span>
              </div>
           </div>
             <div class="flex-container-row">
              <div class="initial start" >${firstMemberFullPathTemplate}</div>
              <div class="auto end right" >
                ${functionalRolesTemplate+'&nbsp;&nbsp;  '+bizListAssociationTemplate+'&nbsp;&nbsp;  '+tagsTemplate}
              </div>
           </div>

          </td>
          <td colspan="1" style="width:110px;vertical-align: middle" class="ellipsis ">
          ${docTypesTemplate}
          </td>
      `;



    $scope.getGroupType = function(item:Entities.DTOAggregationCountItem<Entities.DTOGroup>)
    {

      return item.item.groupType;
    }
    $scope.getIconTooltip = function(dataItem:Entities.DTOAggregationCountItem<Entities.DTOGroup>)
    {
      if((<any>dataItem).updatingIndex) {
        return "Merged into: "+ dataItem.item.parentGroupName + ". Updating index";
      }

      return $scope.getGroupType(dataItem)
    }
    var parseData = function (groups:Entities.DTOAggregationCountItem< Entities.DTOGroup>[]) {

      if(groups) {
        groups.forEach(function (g) {

          var group:Entities.DTOGroup = <Entities.DTOGroup>(<Entities.DTOGroup>(<any>g).item);
          if(group.parentGroup){ //replace between group and parent group
            var originalChildGroup = group;
            group = group.parentGroup;
            (<any>g).groupJoinProcessing =  true;
            (<any>g).item = group;
            (<any>g).originalChildGroup = originalChildGroup;
          }
          else if(group.groupProcessing && !group.hasSubgroups && group.groupType == Entities.EGroupType.USER_GROUP){
            (<any>g).groupJoinProcessing =  true;  //joining of joined group to joined group
          }
          else{
            (<any>g).updatingIndex =group.deleted ||  group.parentGroupName || group.hasSubgroups || group.groupProcessing ; //freeze row while processed
          }

          group.groupName==null?group.groupName="&ltname to be set&gt ("+group.id.substr(group.id.length-5,5)+")":null;

          if(group.numOfFiles>-1) {
            (<any>g).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage((<any>g).count , (<any>g).count2 );
          }
          else {
            (<any>group).numOfFiles='?';
            (<any>g).count = (<any>group).numOfFiles;
          }
          if(group.firstMemberId && group.firstMemberName) {
            (<any>g).firstMemberFullPath = group.firstMemberPath + group.firstMemberName;
          }
          else
          {
            group.firstMemberId=-1;
            group.firstMemberName="";
            (<any>g).firstMemberFullPath = "TBD";
          }
          //NOTICE!!: all authorization operations must be done before parseFunctionalRoles, since if user is not allowed, the funcRole list is eareased
          (<any>g).mngGroupAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngGroups],group.associatedFunctionalRoles.map(f=>f.id));
          (<any>g).openFileCheckRequired = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewPermittedContent],group.associatedFunctionalRoles.map(f=>f.id))
            && !currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent],group.associatedFunctionalRoles.map(f=>f.id))
            && currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewFiles],group.associatedFunctionalRoles.map(f=>f.id));

          (<any>g).openFileAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent,Users.ESystemRoleName.ViewPermittedContent],group.associatedFunctionalRoles.map(f=>f.id));

          splitViewBuilderHelper.parseTags(<Entities.ITaggable>g.item);
          splitViewBuilderHelper.parseDocTypes(<Entities.IDocTypesAssignee>g.item,configuration);
          splitViewBuilderHelper.parseAssociatedBizListItems(<Entities.IBizListAssociatable>g.item);
          splitViewBuilderHelper.parseFunctionalRoles(<Entities.IFunctionalRolesAssignee>g.item);
          splitViewBuilderHelper.parseFilesAndFilterTemplate(<Entities.IFunctionalRolesAssignee>g.item);
          (<any>g).isSubGroup = g.item.groupType ==Entities.EGroupType.SUB_GROUP;
          (<any>g).isAnalysisGroup = g.item.groupType ==Entities.EGroupType.ANALYSIS_GROUP;
          (<any>g).fileHasNetWorkLink =  (<any>g).firstMemberFullPath? startsWith(( (<any>g).firstMemberFullPath.toLowerCase()),'http'):'';

        });

        $timeout(function(){ splitViewBuilderHelper.parseMarkedItems(groups,$scope.entityDisplayType, $scope.element,gridOptions);},0);
        return groups;
      }
      return null;
    };

    var getDataUrlParams = function () {
      return null;
    };

    var onSelectedItemChanged = function () {
      $scope.itemSelected($scope.selectedItem[id.fieldName], $scope.selectedItem[groupName.fieldName]);
    }
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('groupsGrid', function () {
    return {
      restrict: 'EA',
      template: '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-change="handleSelectionChange(data, dataItem, kendoEvent)" ' +
      'k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions"></div>',
      replace: true,
      transclude: true,
      scope://false,
        {
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          totalElements: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          fullyContainedFilterState:'=',
          changeSelectedItem: '=',
          findId: '=',
          onLocalDataBound: '=',
          initialLoadParams: '=',

        },
      controller: 'GroupsCtrl',
      link: function (scope:any, element, attrs:any) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

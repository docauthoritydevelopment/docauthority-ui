///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('contents',[

]);

angular.module('contents')
  .config(function ($stateProvider) {
    $stateProvider
      .state('contents', {
        //   abstract: true,
        url: '/contents',
        templateUrl: '/app/groups/contents.tpl.html',
        controller: 'ContentsPageCtrl'
      })

  })
  .controller('ContentsPageCtrl', function ($scope,$state, $location,Logger,Alerts) {
    var log: ILog = Logger.getInstance('ContentsPageCtrl');


    $scope.Init = function()
    {

    };

    $scope.alerts = Alerts.init();

  })
  .controller('ContentsCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                      propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('ContentsCtrl');

    var _this = this;

    $scope.elementName='contents-grid';
    $scope.entityDisplayType = EntityDisplayTypes.contents;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,null,fields,parseData,rowTemplate);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

 //     $scope.mainGridOptions = _this.gridOptions.gridSettings;

    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOContent> =  Entities.DTOAggregationCountItem.Empty();
    var dTOContent:Entities.DTOContent =  Entities.DTOContent.Empty();
    var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'id',
      title: 'Id',
      type: EFieldTypes.type_string,
      displayed: false,
      isPrimaryKey:true,
      editable: false,
      nullable: true,


    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'contentName',
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,


   };
    var nameNewTemplate =    '<span class="btn-group1  dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="File name: #: '+ name.fieldName+' #">#: '+ name.fieldName+' # <i class="caret ng-hide"></i> </a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this file content</a></li>"+
        '  </ul></span>';
    $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOContent>) {

      setFilterByEntity(item.item.id,item.item.name);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(name);
    fields.push(numOfFiles);



    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-wrap-1-line' style='overflow: visible'> \
               <da-icon class='icon-wrap icon-wrap-1-line' href='duplication' ></da-icon> \
        </td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
        nameNewTemplate+"<div></div></td>" +
          //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

        '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
        "</tr>";


    var parseData = function (contents:Entities.DTOAggregationCountItem< Entities.DTOContent>[]) {
      if(contents) {
        contents.forEach(g=>
        {
          var contentName = g.item.name;
          var id = g.item.id;
          (<any>g).contentName =  contentName.replace(/"/g,'');
          (<any>g).id =  id;
          (<any>g).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(g.count , g.count2 );
        });

        return contents;
      }
      return null;
    };
    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName]);
    }
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('contentsGrid', function($timeout){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        unselectAll: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
        initialLoadParams: '=',
      },
      controller: 'ContentsCtrl',
      link: function (scope:any, element, attrs) {
         scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

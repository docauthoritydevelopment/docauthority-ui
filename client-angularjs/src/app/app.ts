///<reference path='../common/all.d.ts'/>
'use strict';
declare var _;

(function () {
  if (window.parent === window && location.search.toLowerCase().indexOf('usev1') === -1) {
    window.location.href = window.location.href.replace('/v1/', '/');
    return;
  }


  angular.module('daApp', [
    'ny.logger',
    'appTemplates',
    'config',
    'groups',
    'owners',
    'labels',
    'sharePermissionsAllowRead',
    'regulations',
    'patterns',
    'patternsMulti',
    'metadatas',
    'extensions',
    'contents',
    'files',
    'files.all',
    'discover',
    'reports',
    'reports.charts',
    'reports.charts.tree',
    'folders',
    'tagTypes',
    'docTypes',
    'departments',
    'matters',
    'users',
    'fileTypeCategories',
    'fileSizes',
    'users.bizRoles',
    'users.functionalRoles.grid',
    'users.functionalRoles',
    'users.templateRoles',
    'tags',
    'settings',
    'bizLists',
    'searchPatterns',
    'bizLists.manage',
    'operations',
    'dashboards.it',
    'dashboards.system',
    'dashboards.user',
    'dashboards.forward',
    'actions.immediate',
    'resources.fileTags',
    'resources.license',
    'resources.notifications',
    'ui.builder.filters',
    'ui.builder.html.splitView',
    'kendo.directives',
    'services.auth',
    'resources.users',
    'translations',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap',
    'angular-clipboard',
    'angularResizable',
    'services.breadcrumbs',
    'services.events',
    'LocalStorageModule',
    'services.pageTitle',
    'services.localStorage',
    'services.scan',
    'services.filter',
    'services.utils',
    'services.userSettings',
    'services.docTypeTags',
    'dialogs.main',
    'simplePages',
    'resources.operations',
    'directives.dialogs.license',
    'directives.dialogs.mailSettings',
    'directives.dialogs.ldapSettings',
    'services.i18nNotifications',
   // 'angular-click-outside',
    'doc2Services',
    'emails'
//  'services.exceptionHandler',
  ]);

  angular.module('daApp').config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.unauthorized, {
        url: '/unauthorized',
        templateUrl: '/app/unauthorized.tpl.html',
      })
      .state('empty', {
        url: '/empty',
        templateUrl: '/app/empty.tpl.html',
       // controller: 'UserProfilePageCtrl'
      })
      .state(ERouteStateName.discover, {
        //  templateUrl: '/app/GDPR/manageErasurePage1.tpl.html',
        template: '<div bbbb ui-view ng-style="height(-70)"></div>',
        url: '/report',
        abstract: true,

        // resolve: {
        //   getUserAuthData: function(userService){
        //     return userService.getUserData();
        //   }
        // }
      })
      .state('users', {
        template: '<div bbbb ui-view ng-style="height(-70)"></div>',
        url: '/users',
        abstract: true,
      })
      .state(ERouteStateName.policy, {
        template: '<div bbbb ui-view ng-style="height(-150)"></div>',
        url: '/policy',
        abstract: true,
      })


  });

  angular.module('daApp').config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/unauthorized');
    $locationProvider.html5Mode(true);
  });
  angular.module('daApp')
    .factory('timeoutHttpIntercept', function ( configuration:IConfig,$timeout) {
      return {
        request: function(config) {
          config.timeout = configuration.http_request_timeout; //360000 //in milliseconds
          return config;
        }
      };
    }).config(function($httpProvider) {
    $httpProvider.interceptors.push('timeoutHttpIntercept');
  });

  angular.module('daApp')
    .config(function (LoggerProvider) {
      // We don't want the Logger service to be enabled in production
      var isProduction = false;
      LoggerProvider.enabled(!isProduction);
    });

  angular.module('daApp').constant("appSettings", {
    "VERSION": "1.0.0",
  });


  angular.module('daApp').config(function (localStorageServiceProvider, appSettings) {
    localStorageServiceProvider.setPrefix('da' + appSettings.VERSION);
  });

  angular.module('daApp').factory('baseUrlInterceptor', function () {
    return {
      request: function (config) {
        if (!(/\.html?$/i.test(config.url))) {
          const url = config.url.indexOf('/') === 0 ? config.url : `/${config.url}`;
          const newConfig: any = {};//  angular.copy(config);
          //Shallowcopy
          for (const key in config) {

            newConfig[key] = config[key];
          }
          newConfig.url = url;
          // Use te data as is
         //  newConfig.data = config.data
          return newConfig;
          //  return  {...config, url: url};
        } else {
          return config;
        }
      }
    };
  });

  angular.module('daApp').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('baseUrlInterceptor');
  }]);

  angular.module('daApp').config(function (dialogsProvider, $translateProvider) {
    dialogsProvider.useBackdrop('static');
    dialogsProvider.useEscClose(true);
    dialogsProvider.useCopy(true);
    dialogsProvider.setSize('sm');
  });
  angular.module('daApp').controller('AppCtrl',
    function ($scope, Logger: ILogger, $rootScope, $state, $location, $timeout, $stateParams, loginService: ILoginService, $window, currentUserDetailsProvider: ICurrentUserDetailsProvider,
              localStorage: IUserStorageService, userSettings: IUserSettingsService, dialogs, licenseResource: ILicenseResource, i18nNotifications: II18nNotifications,
              routerChangeService: any, dialogsService: any) {
      var log: ILog = Logger.getInstance('AppCtrl');
      var _this = this;


      let highchartInitialColors :any[] = ["#EB742D","#954111","#5A5A5A","#243D6F","#906A00","#3D5F28","#73A7D9","#5292D0","#235588",'#f6e58d','#ffbe76','#ff7979','#badc58','#dff9fb','#f9ca24','#f0932b','#eb4d4b','#6ab04c','#c7ecee','#7ed6df','#e056fd','#686de0','#30336b','#95afc0','#22a6b3','#be2edd','#4834d4','#130f40','#535c68'];


      let isIe =  (document['documentMode'] || /Edge/.test(navigator.userAgent));
      if(isIe){
        var bodyElement = angular.element(document.querySelector('body'));
        bodyElement.addClass('isIE');
      }
      Highcharts.setOptions({
        lang: {
          thousandsSep: ''
        },
        colors: isIe ? highchartInitialColors :  Highcharts.map(highchartInitialColors, function (color) {
          return {
            radialGradient: {
              cx: 0.5,
              cy: 0.3,
              r: 0.7
            },
            stops: [
              [0, color],
              [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
          };
        })
      });

      if (isIe) {
        // Override to always force basic local export
        Highcharts.Chart.prototype.exportChartLocal = function (exportingOptions, chartOptions) {
          var chart = this,
            options = Highcharts.merge(chart.options.exporting, exportingOptions);

          chart.getSVGForLocalExport(
            options,
            chartOptions,
            function () {
              console.error("Something went wrong");
            },
            function (svg) {
              Highcharts.downloadSVGLocal(
                svg,
                options,
                function () {
                  console.error("Something went wrong");
                }
              );
            }
          );
        };
      }


    //  $window.notifyError = routerChangeService.notifyError;
        $window.unfocus = routerChangeService.unfocus;

      $scope.notifications = i18nNotifications;

      $scope.showHeader = window.parent === window;


      $scope.removeNotification = function (notification) {
        i18nNotifications.remove(notification);
      };


      $scope.Init = function () {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;


        $scope.myVersion = 'v?';
        loginService.getVersion(_this.OnGetVersion, function (err) {
        });
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          if (value) {
          }
          else {
            dialogs.error('Error', 'Failed to get user data');

          }

        });
        // Init class settings based on user pref.


      };
      userSettings.getGridViewPreferences(function (gridViewPref) {
        gridViewPref.gridView ? $('body').removeClass('template-view') : $('body').addClass('template-view');
        gridViewPref.newLook ? $('body').addClass('newLook') : $('body').removeClass('newLook');
        gridViewPref.expandTags ? $('body').addClass('hide-tag-icons') : $('body').removeClass('hide-tag-icons');
        gridViewPref.expandDocTypes ? $('body').addClass('hide-docTypes-icons') : $('body').removeClass('hide-docTypes-icons');
      });
      //// expiration alert
      var checkLicense = function () {
        licenseResource.getLicenseStatus(
          function (lic: License.DtoDocAuthorityLicense) {
            var currentTs = Date.now();
            var crawlerExpiry = parseInt(lic.restrictions['crawlerExpiry']);
            var loginExpiry = parseInt(lic.restrictions['loginExpiry']);
            var daysToCe = Math.floor((crawlerExpiry - currentTs) / (1000 * 24 * 3600));
            var daysToLe = Math.floor((loginExpiry - currentTs) / (1000 * 24 * 3600));
            log.debug("currentTs=" + currentTs + " crawlerExpiry=" + crawlerExpiry + " loginExpiry=" + loginExpiry + " daysToCe=" + daysToCe + " daysToLe=" + daysToLe);
            var expirationMsg = "";
            $scope.isScanLicenseExpired = false;
            if (currentTs > crawlerExpiry) {
              expirationMsg += "Scan license expired<br/>";
              $scope.isScanLicenseExpired = true;
            }
            else if (daysToCe == 0) {
              expirationMsg += "Scan license will expire today<br/>";
            }
            else if (daysToCe < 5) {
              expirationMsg += "Scan license will expire in " + daysToCe + " days<br/>";
            }
            if (lic.loginExpired) {
              expirationMsg += "User login license expired";
              $('body').addClass("loginexpired");
              log.info("Login expired");
              // $state.go('unauthorized', {
              //   reason: 'User login license expired',
              //   _u: encodeURIComponent($state.current.name)
              // });
            }
            else if (daysToLe < 5) {
              expirationMsg += "User login license will expire in " + daysToLe + " days";
            }
            // $scope.licenseUserMessage = 'Scan expires in ' + daysToCe + ' days<br/>Login expires in ' + daysToLe + ' days';
            $scope.licenseUserMessage = (expirationMsg.length > 0) ? expirationMsg : null;
            $scope.isLoginExpired = lic.loginExpired;
          },
          function () {
          });
      };
      $timeout(checkLicense, 1500);   // Wait a bit before license is verified


      $rootScope.$on(loginService.loginExpiredEvent, function (sender, value: Users.UserAuthenticationData) {
        $window.location.reload();
      });
      var closestScrolledContainer = function (element) {
        while (element && (element.css('overflow-y') != 'auto' && element.css('overflow-y') != 'scroll' && element.css('overflow-y') != 'hidden')) {
          element = element.parent();
        }
        return element;
      };

      var userAgent = $window.navigator.userAgent;
      var condShow = false;


      $('body').on('click', function(e) {
        let target = $(e.target);

        if(target.closest('.ddl-like').length > 0) {
          if(target.closest('.btn').length > 0) {
            target.closest('.ddl-like').find('.dropdown-menu').first().addClass('show');
          }
        } else {
          $('.ddl-like .dropdown-menu').removeClass('show');
        }
      });


      $('body').on('mousedown', function (e) {
        if (e.ctrlKey) {
          $('body').addClass("ctrlKey");
        }
        else {
          $('body').removeClass("ctrlKey");
        }
      });

      $(document).keypress(function (e) {
        if ((e.which == 100 && e.ctrlKey && e.altKey) ||  // ALT+CTRL+d
          (e.which == 1 && e.ctrlKey) || (e.which == 65 && e.ctrlKey)) {  // CTRL+shift-A
          if (condShow) {
            $(this.body).addClass("da-cond-hidden");
          }
          else {
            $(this.body).removeClass("da-cond-hidden");
          }
          condShow = !condShow;
        }
      });

      _this.OnGetVersion = function (ver: string) {
        $scope.myVersion = ver;
      };

    });

  angular.module('daApp').controller('HtmlHeadCtrl',
    function ($scope, $state) {
    });

  angular.module('daApp').controller('HeaderCtrl',
    function ($scope, $http, $timeout, $window, currentUserDetailsProvider: ICurrentUserDetailsProvider, dialogs, dialogsService, loginService: ILoginService,
              $state, $stateParams, breadcrumbs: IBreadcrumbsService, Logger: ILogger, filterResource: IFilterResource,
              chartResource: IChartsResource, trendChartResource: ITrendChartsResource, operationsResource: IOperationsResource,routerChangeService,
              eventCommunicator: IEventCommunicator, configuration: IConfig, userSettings: IUserSettingsService, healthNotificationResource: IHealthNotificationResource, notificationSettingsResource: INotificationSettingsResource, $filter) {

      var log: ILog = Logger.getInstance('HeaderCtrl');
      $scope.isCollapsed = false;
      $scope.breadcrumbs = breadcrumbs;
      $scope.$state = $state;

      $scope.user = {};
      $scope.user.alerts = 0;     // prepare for alerts notification in topnavbar
      $scope.user.fatals = 0;     // prepare for alerts notification in topnavbar

      var init = function () {
        initEventHandlers();
        if($scope.showHeader) {
          getRunErrors();
          updatePermissions();
          $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
            updatePermissions();
          });
          $timeout(function () //load only after page has loaded
          {
            loadSavedFiltersList();
            loadSavedChartsList();
            loadTrendChartsList();
          });
        }

      };
      var updatePermissions = function () {
        $scope.userName = currentUserDetailsProvider.getUserData() ? currentUserDetailsProvider.getUserData().username : '';
        $scope.isDemoUser = currentUserDetailsProvider.isDemoUser();
        $scope.isOperatorUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans, Users.ESystemRoleName.MngScanConfig]);
        $scope.isGeneralAdminUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.GeneralAdmin, Users.ESystemRoleName.TechSupport]);
        $scope.isViewUsersUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewUsers]);
        $scope.isMngUserAndRolesUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUserRoles, Users.ESystemRoleName.MngRoles, Users.ESystemRoleName.MngUsers]);
        $scope.isRunScansUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
        $scope.isMngScanConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
        $scope.isViewReportsUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewReports, Users.ESystemRoleName.MngReports]);
        $scope.isMngReportsUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngReports]);
        $scope.isViewAuditTrailUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewAuditTrail]);
        $scope.isMngBizListsUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngBizLists, Users.ESystemRoleName.ViewBizListAssociation, Users.ESystemRoleName.RunScans]);
      };

      $scope.isNavbarActive = function (navBarPath) {
        return navBarPath === breadcrumbs.getFirst().name;
      };

      $scope.getRoute = function (name) {
        if (name == "activeScans") {
          return ERouteStateName.activeScans;
        }
        else if (name == "scanHistory") {
          return ERouteStateName.scanHistory;
        }
      };
      $scope.toggleLabelingSet = function () {
        //
        var dlg = dialogs.confirm('Confirmation', "DocTypes will switch to its alternate labeling set. Current labeling can later be restored with another switch. Continue?", null);
        dlg.result.then(function (btn) { //user confirmed deletion
          operationsResource.toggleLabelingSet(function (st: any) {
              log.debug("toggleLabelingSet success. status=" + (st ? JSON.stringify(st) : "null"));
            },
            function (st: any) {
              log.debug("toggleLabelingSet error. status=" + (st ? JSON.stringify(st) : "null"));
            });

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });
      };

      $scope.toggleNewLook = function () {
        $('body').toggleClass('newLook');
        userSettings.toggleNewLook();
      };
      $scope.toggleTagIcons = function () {
        $('body').toggleClass('hide-tag-icons');
        userSettings.toggleTagIcons();
      };
      $scope.toggleDocTypeIcons = function () {
        $('body').toggleClass('hide-docTypes-icons');
        userSettings.toggleDocTypeIcons();
      };

      $scope.clearRunErrors = function () {
        var request = $http.get(configuration.clearRunErrors);
        return request.then(function (response) {
          if (response.status == 200) {
            log.debug('clearRunErrors response: ' + response.data);
            $scope.user.alerts = 0;
          }
          else {
            log.error('clearRunErrors returned status: ' + response.status);
          }
        });
      };
      $scope.clearFatalRunErrors = function () {
        var request = $http.get(configuration.clearFatalRunErrors);
        return request.then(function (response) {
          if (response.status == 200) {
            log.debug('clearFatalRunErrors response: ' + response.data);
            $scope.user.fatals = 0;
          }
          else {
            log.error('clearFatalRunErrors returned status: ' + response.status);
          }
        });
      };

      $scope.logout = function () {
        loginService.logout();
      };
      var getRunErrors = function () {
        getServerRunErrors();
        getServerRunFatals();
      };

      var setNextGetErrors = function (): void {
        setTimeout(getRunErrors, 110000);
      };
      var getServerRunErrors = function () {
        var request = $http.get(configuration.getRunErrors);
        return request.then(function (response) {
          if (response.status == 200) {
            log.debug('getRunErrors response: ' + response.data);
            $scope.user.alerts = response.data;
            setNextGetErrors();
          }
          else {
            log.error('getRunErrors returned status: ' + response.status);
          }
        });
      };

      var getServerRunFatals = function () {
        var request = $http.get(configuration.getRunErrorsFatal);
        return request.then(function (response) {
          if (response.status == 200) {
            log.debug('getRunErrorsFatal response: ' + response.data);
            $scope.user.fatals = response.data;
          }
          else {
            log.error('getRunErrorsFatal returned status: ' + response.status);
          }
        });
      };


      var loadSavedChartsList = function () {
        chartResource.getCharts(function (charts: ui.SavedChart[]) {
          $scope.savedChartsForTopMenu = charts.filter(f => !f.hidden);
        }, function () {

        });

      };
      var loadTrendChartsList = function () {
        trendChartResource.getCharts(function (charts: Dashboards.DtoTrendChart[]) {
          $scope.trendChartsForTopMenu = charts.filter(f => {
            if (f.hidden || !(<Dashboards.DtoTrendChart>f).viewConfiguration) {
              return false;
            }
            var viewConfiguration: Dashboards.TrendChartViewConfiguration = JSON.parse((<Dashboards.DtoTrendChart>f).viewConfiguration);
            return viewConfiguration.showInMenu;
          });
        }, function () {

        });

      };
      var loadSavedFiltersList = function () {
        filterResource.getViews(null,function (filters: ui.SavedDiscoveredFilter[]) {
          $scope.savedViews = filters;
        }, function () {

        });
      };
      $scope.loadSavedFilter = function (savedFilter: ui.SavedDiscoveredFilter) {
        $stateParams['left'] = savedFilter.leftEntityDisplayTypeName;
        $stateParams['right'] = savedFilter.rightEntityDisplayTypeName;
        $stateParams['filter'] = JSON.stringify(savedFilter.savedFilterDto.rawFilter);
        $stateParams['pageLeft'] = 1;
        $stateParams['pageRight'] = 1;
        log.debug('Set filter to url: ' + $stateParams['filter']);
        $state.transitionTo(ERouteStateName.discoverSplitView,
          $stateParams,
          {
            notify: true, inherit: false
          });
      };


      $scope.loadSavedChart = function (savedChart: ui.SavedChart) {
        $stateParams['id'] = savedChart.id;
        $state.transitionTo(ERouteStateName.chartView,
          $stateParams,
          {
            notify: true, inherit: false
          });
      };

      $scope.loadTrendChart = function (trendChart: Dashboards.DtoTrendChart) {
        $stateParams['id'] = trendChart.id;
        $state.transitionTo(ERouteStateName.trendChartView,
          $stateParams,
          {
            notify: true, inherit: false
          });
      };


      $scope.openLicenseKeyDialog = function () {


        var dlg = dialogs.create(
          'common/directives/dialogs/licenseKeyDialog.tpl.html',
          'licenseKeyDialogCtrl',
          {licenseAlertMessage: $scope.licenseUserMessage},
          {size: 'md'});

        dlg.result.then(function () {
          $window.location.reload();
        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      };
      $scope.sendHealthReportNow = function () {

        notificationSettingsResource.getMailNotificationSettings(function (settings: AppSettings.DtoMailNotificationConfiguration) {
            if (!settings.enabled) {
              dialogs.notify('Note', 'Mail was not sent.\n\nMail notifications is disabled.\nTo enable mails, go to Settings > Mail settings> Mail Addresses of notification receivers');
            }
            else if (!settings.addresses) {
              dialogs.notify('Note', 'Mail was not sent.\n\nMissing destination address configuration.\nGo to Settings > Mail settings> Mail Addresses of notification receivers');
            }
            else {
              healthNotificationResource.sendMailNotificationNow(function () {
                  dialogs.notify('Note', 'Scan Progress Report was sent as configured');
                }
                , onError)
            }
          }
          , onError);

      };
      $scope.viewCurrentHealthNotificationDialog = function () {

        healthNotificationResource.getHtmlNotification(function (html) {

            var currentTime = $filter('date')(Date.now(), configuration.dateTimeFormat);
            dialogs.notify('App Report for ' + currentTime, html, {size: 'lg'});
          }
          , onError)

      };
      $scope.openMailSettingsDialog = function () {
        return dialogsService.openMailSettingsDialog();
      };
      $scope.openLDapSettingsDialog = function () {

        var dlg = dialogs.create(
          'common/directives/dialogs/ldapSettingsDialog.tpl.html',
          'ldapSettingsDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function () {

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });

      };

      $scope.startApplyToSolr = () => {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderUserAction, {
          action: 'startApplyToSolr',
        });
      };

      $scope.startResyncSolr = function () {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderUserAction, {
          action: 'startResyncSolr',
        });
      };

      $scope.startApplyTagsToFiles = () => {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderUserAction, {
          action: 'startApplyPolicyTagsToFiles',
        });
      };

      var initEventHandlers = function() {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportChartUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {

            if (fireParam.action === 'refreshState') {
              if(!$scope.showHeader) {
                routerChangeService.refreshMenuItems();
              }
              else {
                loadSavedChartsList();
              }
            }

          })));

        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportTrendChartUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {

            if (fireParam.action === 'refreshState') {
              if(!$scope.showHeader) {
                routerChangeService.refreshMenuItems();
              }
              else {
                loadTrendChartsList();
              }
            }

          })));

        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportHeaderSetState,
          $scope,
          (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {
            /*
             * fireParam object:
             *   { initSearch: false, value: searchText, mode: mode}
             */
            if (fireParam.action === 'reportState') {
              if(!$scope.showHeader) {
                routerChangeService.refreshMenuItems();
              }
              else {
                loadSavedFiltersList();
              }

            }
          })));
      };
      $scope.$on("$destroy", function () {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function () {
        dialogs.error('Error', 'Sorry, an error occurred.');
      };
      init();

    })
      .run(function (routerChangeService) {
        routerChangeService.raiseEvent('loaded');
    });

  angular.bootstrap(document, ['daApp']);
}());



///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('discover',[
  'directives',
  'directives.searchBox',
  'resources.filters',
  'directives.splitViewFilter',
  'directives.splitView.navigateByMenu'

]) .config(function ($stateProvider) {

  $stateProvider
    .state(ERouteStateName.search, {
      parent:ERouteStateName.discover,
      url: '/search',
      resolve: {
        userAuth: function($stateParams, currentUserDetailsProvider:ICurrentUserDetailsProvider) {
          return currentUserDetailsProvider.checkAuthReady();
        }
      },
      templateUrl: '/app/splitViews/discover.tpl.html',
      controller: 'DiscoverSearchPageCtrl'
    })

})
  .controller('DiscoverSearchPageCtrl', function ($window,$filter, $scope,$element,$state,$stateParams,$compile, Logger:ILogger,
                                                  filterResource:IFilterResource, splitViewFilterService:IFilterService,   filesResource:IFilesResource,splitViewFilterServiceFactory,filterFactory:ui.IFilterFactory, eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService, currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('DiscoverSearchPageCtrl');
    //let splitViewFilterService:IFilterService = splitViewFilterServiceFactory.getInstance();
    $scope.userSearchModes = EUserSearchMode.all();

    $scope.searchModes = {};
    $scope.searchModes[EUserSearchMode.content.toString()] = filterFactory.createContentSearch();
    $scope.searchModes[EUserSearchMode.properties.toString()] =filterFactory.createFilterByProperties();


    var init = function() {
      var allTypes;
      if (currentUserDetailsProvider.isInstallationModeLegal()) {
        allTypes = EntityDisplayTypes.allLegalInstallationMode();
      }
      else {
        allTypes = EntityDisplayTypes.all();
      }

      if (!currentUserDetailsProvider.isLabelingEnabled()){
        allTypes= allTypes.filter(t=> !t.equals(EntityDisplayTypes.labels));
      }

      if (!currentUserDetailsProvider.isMetadataEnabled()){
        allTypes= allTypes.filter(t=> !t.equals(EntityDisplayTypes.metadatas));
      }


      $scope.viewLeftOptions =allTypes.filter(t=> !t.equals(EntityDisplayTypes.files)).map(n=> {
                     return {title: $filter('translate')(n.toString()), type:n , tooltip:$filter('translate')('DESC_'+n.toString())}
        });
      $scope.viewRightOptions = allTypes.filter(t=> !t.equals(EntityDisplayTypes.all_files)).map(n=> {
        return {title: $filter('translate')(n.toString()), type:n , tooltip:$filter('translate')('DESC_'+n.toString())}
      });
      setRecentlyUsed();
      getViews();

    };

    var setUserRoles = function() {
      $scope.isViewFuncRoleAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation]);
      $scope.isViewTagAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagAssociation],null,true);
      $scope.isViewBizListAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],null,true);
      $scope.isViewDocTypeAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeAssociation],null,true);
      $scope.isViewDepartmentAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true);
      $scope.isViewMatterAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true);
    };
    setUserRoles();
    $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
      setUserRoles();
    });

    $scope.viewPermitted = function(op) {
      let entityType:EntityDisplayTypes = <EntityDisplayTypes>op.type;
      if(entityType ==EntityDisplayTypes.bizlistItem_clients_extractions||entityType ==EntityDisplayTypes.bizlistItem_consumers_extraction_rules || entityType ==EntityDisplayTypes.bizlistItems_clients) {
        return $scope.isViewBizListAssociationUser;
      }
      if(entityType ==EntityDisplayTypes.doc_types)
      {
        return $scope.isViewDocTypeAssociationUser;
      }
      if(entityType ==EntityDisplayTypes.tag_types||entityType ==EntityDisplayTypes.tags)
      {
        return $scope.isViewTagAssociationUser;
      }
      if(entityType ==EntityDisplayTypes.departments || entityType ==EntityDisplayTypes.departments_flat)
      {
        return $scope.isViewDepartmentAssociationUser;
      }
      if(entityType ==EntityDisplayTypes.matters || entityType ==EntityDisplayTypes.matters_flat)
      {
        return $scope.isViewMatterAssociationUser;
      }
      if(entityType ==EntityDisplayTypes.functional_roles)
      {
        return $scope.isViewFuncRoleAssociationUser;
      }
      return true;
    }

    $scope.viewLeftChanged = function(op){
      $scope.leftPaneType = op.type;
    }
    $scope.viewRightChanged = function(op){
      $scope.rightPaneType = op.type;


    }
    var getViews=function(){
      const params: any ={sort: JSON.stringify([{"dir":"asc","field":"name"}])};
      filterResource.getViews(params,function(views:ui.SavedDiscoveredFilter[]){
        $scope.views = views;
      },function(){})
    }
    $scope.saveRecentlyUsed = function(){
      if($scope.leftPaneType && $scope.rightPaneType) {
        userSettings.setDiscoverSearch($scope.leftPaneType.value + ';' + $scope.rightPaneType.value);
      }
    }
    var setRecentlyUsed = function(){
      userSettings.getDiscoverSearch(function(val){
        if(val){
          let vals = val.split(';');
          $scope.viewLeftChanged( $scope.viewLeftOptions.filter(l=>l.type.value == vals[0])[0]);
          $scope.viewRightChanged($scope.viewRightOptions.filter(l=>l.type.value == vals[1])[0]);
        }
      });
    }
    $scope.onAfterSearch =function(){
      goDiscover();
    }
    $scope.onSetFilter=function(){

    }

    var getFilter = function(){
      if(splitViewFilterService.getFilterData().hasCriteria()) {
        var thinFilterCopy = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
          if (value == null) {
            return undefined;
          }
          return value;
        });
        return thinFilterCopy;
      }
      return 'none';
    }

    $scope.viewRightDisabled = function (op):boolean {
      if( !$scope.leftPaneType)
      {
        return true;
      }
     let  leftEntityTypeName:string = $scope.leftPaneType.value;
      let rightEntityTypeName:string = op.type.value;

      if(rightEntityTypeName==leftEntityTypeName){
        return true;
      }
      var leftPane:string = leftEntityTypeName;
      //  if (startsWith(leftPane, "folders")) {
      //     leftPane = "folders";
      //  }
       if (startsWith(leftPane, "patterns")) {
         leftPane = "patterns";
         return startsWith(rightEntityTypeName, leftPane);
       }
      if (startsWith(leftPane, "acl_")) {
        leftPane = "acl_";
        return startsWith(rightEntityTypeName, leftPane);
      }
      // var result= startsWith(rightPaneOption.view, leftPane);

      return false;
    };

    $scope.go = function(){
      if($scope.searchTerm) {
        $scope.doUserSearch($scope.searchTerm); //the search is async, goDiscover will be called as callback
      }
      else {
        goDiscover();
      }
    }
    var goDiscover=function(){
      $scope.saveRecentlyUsed();
      let params = {};
      params['left'] =$scope.leftPaneType;
      params['right'] =$scope.rightPaneType;
      params['filter'] =getFilter();;
      $state.transitionTo(ERouteStateName.discoverSplitView,
        params,
        {
          notify: true, inherit: false,reload:true
        });
     }


    var startsWith = function (str1:string, str2:string):boolean {
      return str1.substring(0, str2.length) === str2;
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    init();



  });

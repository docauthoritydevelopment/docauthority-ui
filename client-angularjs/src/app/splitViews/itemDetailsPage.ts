///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports.details',[
      'directives',
      'directives.dashboardWidgets',
      'resources.charts',
      'reports.charts.directive',
      'resources.folders',
      'resources.groups',
      'resources.files',
      'dialogs.main',
      'directives.dialogs',
      'directives.dialogs.charts',
      'directives.highlights',
      'resources.IPdfOperations',
      'directives.dialogs.recipient'
])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.details, {
            //  abstract: true,
            url: '/details/:context/:id',
            templateUrl: '/app/splitViews/itemDetailsPage.tpl.html',
            controller: 'ItemDetailsPageCtrl'
          })

    })
    .controller('ItemDetailsPageCtrl', function ($window, $scope,$element,$state,$stateParams,$compile,dialogs,dashboardChartResource:IDashboardChartsResource,$filter,filterFactory:ui.IFilterFactory,
                                               Logger:ILogger,propertiesUtils, configuration:IConfig,pageTitle,folderResource:IFolderResource,fileResource:IFileResource,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                 routerChangeService, groupResource:IGroupResource, currentUserDetailsProvider:ICurrentUserDetailsProvider, groupsResource:IGroupsResource,chartResource:IChartsResource,eventCommunicator:IEventCommunicator,bizListsResource:IBizListsResource, pdfOperationsResource: IPdfOperations) {
      var log:ILog = Logger.getInstance('ItemDetailsPageCtrl');
      var context:EFilterContextNames = EFilterContextNames.parse( $stateParams.context);
      $scope.contextName= context ?context : EFilterContextNames.group;
      $scope.id = $stateParams.id ? $stateParams.id.toLowerCase() : null;
      $scope.name = $stateParams.name ? $stateParams.name : null;
      $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
      $scope.chartsData =[];

      var availableContexts:EFilterContextNames[] =[];

      $scope.init=function() {
        $scope.contextTitleName = $filter('translate')($scope.contextName.toString());
        setTitle();
        setAvailableContexts();
        setUserRoles();

        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setAvailableContexts();
          setUserRoles();
        });
      };

      $scope.viewPermitted = function(context:EFilterContextNames)
      {
        var entityType: EntityDisplayTypes =EntityDisplayTypes.convertEFilterContextNames(context);
        if(entityType ==EntityDisplayTypes.bizlistItem_clients_extractions||entityType ==EntityDisplayTypes.bizlistItem_consumers_extraction_rules || entityType ==EntityDisplayTypes.bizlistItems_clients) {
          return $scope.isViewBizListAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.doc_types)
        {
          return $scope.isViewDocTypeAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.tag_types||entityType ==EntityDisplayTypes.tags)
        {
          return $scope.isViewTagAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.functional_roles)
        {
          return $scope.isViewFuncRoleAssociationUser;
        }
        return true;
      }

      var setAvailableContexts = function() {
        availableContexts = [
          $scope.isFolderReport()? EFilterContextNames.group:EFilterContextNames.root_folder,
          EFilterContextNames.tag_type,
          EFilterContextNames.acl_read,
          EFilterContextNames.acl_write,
          EFilterContextNames.owner,
          EFilterContextNames.functional_role,
          EFilterContextNames.doc_type,
          currentUserDetailsProvider.isInstallationModeLegal()?EFilterContextNames.matter:EFilterContextNames.department,
          EFilterContextNames.bizList_item_clients,
          EFilterContextNames.bizList_item_clients_extraction,
          EFilterContextNames.bizList_item_consumers_extraction_rule
        ];
      };

      var setUserRoles = function() {
        $scope.showSharePermissions = currentUserDetailsProvider.isSharePermissionEnabled();
        $scope.isRunScansUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
        $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
        $scope.isTechSupportUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.TechSupport]);

        $scope.isViewFuncRoleAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation]);
        $scope.isViewTagAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagAssociation],null,true);
        $scope.isViewBizListAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],null,true);
        $scope.isViewDocTypeAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeAssociation],null,true);
        $scope.isViewDepartmentAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true) && currentUserDetailsProvider.isInstallationModeStandard();
        $scope.isViewMatterAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true) && currentUserDetailsProvider.isInstallationModeLegal();
        if ($scope.id) {
          initItemData();
        }
        else
        {
          //$state.transitionTo('dashboard',
          //    $stateParams,
          //    {
          //      notify: true, inherit: true
          //    });
        }
        createEntitiesCharts();
      };

      var arrangeDom = function() {
        let chartWrappers = $('.chart-rows-wrapper').find('.chart-wrapper');

        for(let i=0; i<chartWrappers.length; i+=2) {
          let chartRow = $('<div class="file-detail-row row panels-mini print-page-break pdf-page"></div>');
          chartRow.append(chartWrappers[i]);

          if(i+1 < chartWrappers.length) {
            chartRow.append(chartWrappers[i+1]);
          }
          chartRow.appendTo('.chart-rows-wrapper');
        }
      };

      var initItemData = function() {
        if ($scope.isGroupReport()) {
          groupResource.getGroupById($scope.id, function (groupDetails:Entities.DTOGroupDetails) {
            $scope.entityIcon = configuration.icon_group;
            $scope.name = groupDetails.groupDto.groupName==null?"<name to be set> ("+groupDetails.groupDto.id.substr(groupDetails.groupDto.id.length-6,5)+")":groupDetails.groupDto.groupName;
            $scope.id = groupDetails.groupDto.id;
            $scope.filesCount = groupDetails.groupDto.numOfFiles;
            $scope.type=groupDetails.groupDto.groupType;
            $scope.groupProcessing=groupDetails.groupDto.groupProcessing;
            $scope.isUserGroup = groupDetails.groupDto.groupType == Entities.EGroupType.USER_GROUP;
            $scope.isSubGroup = groupDetails.groupDto.groupType == Entities.EGroupType.SUB_GROUP;
            $scope.joinedGroupChilds=  groupDetails.childrenGroups;
            $scope.subGroupMembers=  groupDetails.siblingGroups;
            $scope.foldersCount = 5;
            $scope.displayedName =$scope.name;
            setTitle();
            initFileHistogramData();

            $scope.mngGroupAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngGroups],groupDetails.groupDto.associatedFunctionalRoles.map(f=>f.id));
            $scope.removeChildGroupFromJoindGroup = function(groupIdToRemove) {
              var dlg:any = dialogs.confirm('Confirm deletion', 'Are you sure you want to remove this group?', null);
              dlg.result.then(function(btn) { //user confirmed deletion
                groupsResource.removeChildGroupFromJoinedGroup($scope.id, groupIdToRemove,function() {
                      groupResource.getGroupById($scope.id, initItemData, onError); //refresh display
                    },
                    onError)
              }, function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });
            };
            $scope.deleteUserGroup = function() {
              var dlg:any = dialogs.confirm('Confirm deletion', 'You are about to delete this joined-group. Continue?', null);
              dlg.result.then(function(btn){ //user confirmed deletion
                groupsResource.deleteJoinedGroup($scope.id,function(){
                    $scope.itemDeleted=true;
                  routerChangeService.updateAsyncUserOperations();
                },onError)
              },function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });
            };

            $scope.initiated = true;
          }, onError);
        }
        else if ($scope.isFolderReport()) {
          folderResource.getFolderById($scope.id, function (folder:Entities.DTOFolderInfo) {

            $scope.entityIcon = configuration.icon_folder;
            $scope.name = folder.name;
            $scope.filesCount = folder.numOfAllFiles;
            $scope.foldersCount = folder.numOfSubFolders;
            $scope.folderPath = folder.path;
            $scope.displayedName =folder.realPath;
            $scope.sharePermissionsData = folder.rootFolderSharePermissions;
            setTitle();
            initFileHistogramData();
            createEntitiesCharts();
            $scope.initiated = true;
          }, onError);
        }
        else if ($scope.isFileReport()) {
          fileResource.getFileById($scope.id, function (file:Entities.DTOFileDetails) {

            $scope.showDupGrid=true;
            $scope.showMailGrid=true;

            if(file.fileDto.itemSubject || file.fileDto.itemSubject=='') {
              $scope.name = _.isEmpty(file.fileDto.itemSubject) ? '(no subject)' : file.fileDto.itemSubject;
            }
            else{
              $scope.name = file.fileDto.baseName;
            }

            $scope.creationDate = file.fileDto.creationDate;
            $scope.lastModifiedDate = file.fileDto.fsLastModified;
            $scope.lastAccessDate = file.fileDto.fsLastAccessAsLong ? file.fileDto.fsLastAccessAsLong : file.fileDto.fsLastAccess;
            $scope.fileType =file.fileDto.type;
            $scope.filePath = file.fileDto.fileName;
            $scope.aclReadData = file.aclDataDto.aclReadData;
            $scope.aclWriteData = file.aclDataDto.aclWriteData;
            $scope.sharePermissionsData = file.fileRootFolder.rootFolderSharePermissions;

            $scope.fileGroupName = file.fileDto.groupName==""?"<name to be set> ("+file.fileDto.groupId.substr(file.fileDto.groupId.length-6,5)+")":file.fileDto.groupName;
            $scope.fileGroupId =file.fileDto.groupId?file.fileDto.groupId:null;

            $scope.fileRootFolder =file.fileRootFolder.realPath;
            $scope.fileFullPath = $scope.filePath;
            $scope.fileRootFolderNickName =file.fileRootFolder.nickName;
            if(_.endsWith($scope.fileRootFolder,'/') ||_.endsWith($scope.fileRootFolder,'\\')  ) {
              $scope.fileRootFolder =  $scope.fileRootFolder.substring(0, $scope.fileRootFolder.length - 1);
            }
            $scope.displayedName = $scope.fileRootFolderNickName? $scope.filePath.replace($scope.fileRootFolder, $scope.fileRootFolderNickName):$scope.filePath;
            $scope.rootFolderMediaType =file.fileRootFolder.mediaType;
            $scope.fileProcessingState =file.fileProcessingState;
            $scope.isAnalyzed =file.fileProcessingState==Entities.DTOFileDetails.fileProcessingState_ANALYSED||file.fileProcessingState==Entities.DTOFileDetails.fileProcessingState_INGESTED;
            $scope.ingestionErrorInfo =file.ingestionError?file.ingestionError.detailText:null;
            $scope.fileOwner =file.fileOwner;
            $scope.fileSize =file.fileDto.fsFileSize;
            $scope.fileAuthor =file.author;
            $scope.fileDuplicationsCount =file.fileDto.contentMetadataDto?file.fileDto.contentMetadataDto.fileCount:null;
            $scope.fileNumOfAttachments =file.fileDto.numOfAttachments || 0;
            $scope.fileContentId =file.fileDto.contentId;
            $scope.acl_readsTitle=$filter('translate')(EntityDisplayTypes.acl_reads.toString())+'s';
            $scope.acl_writesTitle=$filter('translate')(EntityDisplayTypes.acl_writes.toString())+'s';
            $scope.fileOpenUrl = configuration.file_download_url.replace('{id}',$scope.id);
            $scope.viewContentAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent,Users.ESystemRoleName.ViewPermittedContent],file.fileDto.associatedFunctionalRoles.map(f=>f.id));
            $scope.openFileCheckRequired = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewPermittedContent],file.fileDto.associatedFunctionalRoles.map(f=>f.id))
              && !currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent],file.fileDto.associatedFunctionalRoles.map(f=>f.id))
              && currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewFiles],file.fileDto.associatedFunctionalRoles.map(f=>f.id));

            parseFileTags(file.fileDto);


            $scope.fileTags=file.fileDto.fileTagDtos;
            $scope.fileDocTypes=file.fileDto.docTypeDtos;
            $scope.fileDepartment=file.fileDto.departmentDto;
            $scope.fileFunctionalRoles=file.fileDto.associatedFunctionalRoles;
            $scope.clientAssociations=file.fileDto.associatedBizListItems;

            $scope.isAttachment = file.fileDto.itemType === Dashboards.EFileItemType.attachment;
            $scope.isEmail = file.fileDto.type === Dashboards.EFileType.mail;
            $scope.mailSender = file.fileDto.sender ? file.fileDto.sender.address: '';
            $scope.mailSentDate = _.isString(file.fileDto.sentDate) ? $filter('asDate')(file.fileDto.sentDate.split('+')[0]) : null;
            $scope.mailId =  $scope.isEmail? file.fileDto.id:file.fileDto.mailContainerId;
            $scope.mailRecipients = file.fileDto.recipients;
            $scope.mailRecipientsShortList = _.uniq(_.take(_.map(file.fileDto.recipients, function (r) {
              return r.address;
            }),3)).join(', ');

            if($scope.isViewBizListAssociationUser && file.fileDto.bizListExtractionSummary &&  file.fileDto.bizListExtractionSummary[0]) {
              $scope.bizListExtractionSummaryLoading=true;
                 bizListsResource.getAllBizLists(null,function (listsByIdDic:any) {
                      var bizListsDic=listsByIdDic;
                      file.fileDto.bizListExtractionSummary.forEach(s=>(<any>s).bizListName = bizListsDic[s.bizListId].name);
                      var uniqueTypes = propertiesUtils.unique(file.fileDto.bizListExtractionSummary, function (x:Entities.DTOExtractionSummary) {
                        return x.bizListItemType;
                      });
                   var listPerType=[];
                       uniqueTypes.forEach(uniqueType=>
                       {
                        var typeList = file.fileDto.bizListExtractionSummary.filter(summary=>  summary.bizListItemType == uniqueType.bizListItemType);
                        typeList.sort((t1:any, t2:any)=> {
                          if (t1.bizListName > t2.bizListName) {
                            return 1;
                          }
                          if (t1.bizListName < t2.bizListName) {
                            return -1;
                          }
                          return 0;
                        });
                        listPerType.push(typeList);
                      });
                       $scope.bizListExtractionSummaryLoading=false;
                      $scope.extractionSummaryPerType = listPerType;
                       $scope.extractionSummaryNotAggRuleExists = $scope.extractionSummaryPerType&& $scope.extractionSummaryPerType[0] && $scope.extractionSummaryPerType[0][0].sampleExtractions.filter(e=> !e.aggregated)[0]!=null;
                    },
                    function (error) {
                      $scope.bizListExtractionSummaryLoading=false;
                    }
                );
             }
            //$scope.patternExtractions=file.patternExtractions;
            $scope.searchPatternsCounting=file.fileDto.searchPatternsCounting;

            $scope.proposedTitles=file.proposedTitles;
            if($scope.fileDuplicationsCount>1)
            {
              initFileDuplications();
            }
            if($scope.fileNumOfAttachments>0)
            {
              initMailAttachments();
            }
            setTitle();

            $scope.initiated = true;
          }, onError);
        }

        $scope.showRecipients = function() {
          var dlg = dialogs.create('app/splitViews/recipient-dialog/recipientDialog.tpl.html', 'recipientDialogCtrl', {recipients:$scope.mailRecipients}, { size: 'md' });
          dlg.result.then(function (data) {
          }, function () {
          });
        }

        /*
        _.defer(() => {
          arrangeDom();
        },500);
        */
      };
      $scope.isFileContentAuthorized = function(){
        fileResource.isFileContentAuthorized($scope.id,function(isAuthorized:boolean){
          if(isAuthorized){
            $window.open(configuration.file_download_url.replace('{id}',$scope.id), "_top");
          }
          else{
            dialogs.notify("Operation canceled","Sorry, you do not have permissions to view this file content.")
          }
        },onError);
      }
      var initFileDuplications = function() {
        $scope.onExportDuplicationsToExcel=function(){
          $scope.exportDupToExcelInProgress = true;
          $scope.exportDupToExcel = {fileName: $scope.name+ 'duplications.xlsx'};
        };
        $scope.exportDupToExcelCompleted=function(notFullRequest:boolean)
        {
          if(  $scope.exportDupToExcelInProgress) {
            if (notFullRequest) {
              dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
            }
            $scope.exportDupToExcelInProgress = false;
          }
        }
      };
      var initMailAttachments = function() {
        $scope.onExportMailToExcel=function(){
          $scope.exportMailToExcelInProgress = true;
          $scope.exportMailToExcel = {fileName: $scope.name+ 'attachments.xlsx'};
        };
        $scope.exportMailToExcelCompleted=function(notFullRequest:boolean)
        {
          if(  $scope.exportMailToExcelInProgress) {
            if (notFullRequest) {
              dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
            }
            $scope.exportMailToExcelInProgress = false;
          }
        }
      };

      $scope.viewGroupReport = function(groupId,groupName) {
        if(groupId) {
          const urlParams = {};
          urlParams['left'] = EntityDisplayTypes.groups.toString();
          urlParams['right'] = EntityDisplayTypes.files.toString();
          urlParams['findIdLeft'] = encodeURIComponent(groupId);

          var url = $state.href(ERouteStateName.discoverSplitView, urlParams);
          url = url.replace('/v1','');
          $window.open(url,'_blank');
        }
      };

      var parseFileTags = function(item:Entities.DTOFileInfo) {
        splitViewBuilderHelper.parseTags(<Entities.ITaggable>item);
        splitViewBuilderHelper.parseDocTypes(<Entities.IDocTypesAssignee>item,configuration);
        splitViewBuilderHelper.parseDepartment(<Entities.IDepartmentAssignee>item,configuration);
        splitViewBuilderHelper.parseFunctionalRoles(<Entities.IFunctionalRolesAssignee>item);
        splitViewBuilderHelper.parseAssociatedBizListItems(<Entities.IBizListAssociatable>item);
      };

      var initFileHistogramData=function() {
        var filter = null;
        if ($scope.isFolderReport()) {
          filter = filterFactory.createSubFolderFilter()($scope.id, $scope.displayedName).toKendoFilter();
          dashboardChartResource.getFileTypeHistogram(filter, false, $scope.onFileTypeHistogramCompleted, onError);
        }
        else if($scope.isGroupReport()) { //group
          var filterBuilder = filterFactory.createFilterByID(EntityDisplayTypes.convertEFilterContextNames( $scope.contextName));
          filter = filterBuilder($scope.id ,  $scope.displayedName).toKendoFilter();
          dashboardChartResource.getFileExtensionHistogram(filter, $scope.onFileExtensionHistogramCompleted, onError);
        }

        dashboardChartResource.getFilesAccessHistogram(filter, false,  $scope.onGetFilesAccessHistogramCompleted, onError);
      };

      var setTitle = function() {
        if($scope.name) {
          pageTitle.set($scope.contextTitleName + ' > ' + 'Summary report: ' + $scope.name);
        }
        else {
          pageTitle.set($scope.contextTitleName + ' > ' + 'Summary report ' );
        }
      };
      $scope.isGroupReport=function() {
        return $scope.contextName ==  EFilterContextNames.group;
      };
      $scope.isFileReport=function() {
        return $scope.contextName ==  EFilterContextNames.file;
      };
      $scope.isFolderReport=function() {
        return ($scope.contextName ==  EFilterContextNames.folder);
      };
      var printHtml = function (html) {
        var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
        (<any> hiddenFrame).contentWindow.printAndRemove = function() {
          (<any> hiddenFrame).contentWindow.print();
          $(hiddenFrame).remove();
        };
        var htmlDocument = "<!doctype html>"+
            "<html>"+
            '<body onload="printAndRemove();">' + // Print only after document is loaded
            html +
            '</body>'+
            "</html>";
        var doc =  (<any> hiddenFrame).contentWindow.document.open("text/html", "replace");
        doc.write(htmlDocument);
        doc.close();
      };
      $scope.print = function() {
        var printContents = document.getElementById('print-content').innerHTML;
        //var popupWin = window.open('', '_blank', 'width=600,height=auto');
        //popupWin.document.open()
        //popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
        //popupWin.document.close();
        printHtml(printContents);
      };
      $scope.activeCharts = {};
      $scope.onCreateHighChart = function(chartName, myChart) {
        $scope.activeCharts[chartName] = myChart;
      }


      $scope.exportToPdf = function() {
        pdfOperationsResource.exportToPdf('.pdf-page', $scope.contextTitleName + '_' + $scope.name + ".pdf", () => {
        });
      }


      $scope.printPage = function() {
        window.print();
      }

      $scope.exportToExcel = function() {
        if($scope.chartsData) {
          var createChartRows = function(contextNames:EFilterContextNames,data:Dashboards.DtoCounterReport[]) {
            var rows = [];
            rows.push({ //columns header
              cells: [
                {value: contextNames?$filter('translate')(contextNames.toString()):''+' name'},
                {value: '# files'},
                {value: 'description'},

              ]
            });
            if (data && data.length > 0) {
              for (var i = 0; i < data.length; i++) {

                var dataItem = (data)[i];

                rows.push({
                  cells: [
                    {value: dataItem.name},
                    {value: dataItem.counter},
                    {value: dataItem.description},
                     ]
                });

              }

            }
            return rows;
          };

          var createDetailsRows = function() {
            var rows = [];

            rows.push({ //columns header
              cells: [
                {value: 'factor'},
                {value: 'value'},
              ]
            });

            rows.push({cells: [{value: 'name'},{value: $scope.name}]});
            rows.push({cells: [{value: 'full name'},{value: $scope.displayedName}]});
            rows.push({cells: [{value: '# files'},{value: $scope.filesCount}]});
            rows.push({cells: [{value: '# folders'},{value: $scope.foldersCount}]});
            if($scope.isUserGroup) {
              rows.push({cells: [{value: '# member groups'},{value: $scope.joinedGroupChilds.length}]});
              $scope.joinedGroupChilds ? $scope.joinedGroupChilds.forEach(g=> {
                rows.push({cells: [{value: 'member group'},{value: g.groupName+ '('+ g.numOfFiles+')'}]});
              }):null;
            }
            else if($scope.isSubGroup) {
              rows.push({cells: [{value: '# member groups'},{value: $scope.subGroupMembers.length}]});
              $scope.joinedGroupChilds ? $scope.subGroupMembers.forEach(g=> {
                rows.push({cells: [{value: 'member group'},{value: g.groupName+ '('+ g.numOfFiles+')'}]});
              }):null;
            }
            return rows;
          };

          var createSheet = function(rows,title) {
            var sheet = {
              columns: [
                {autoWidth: true},
                {autoWidth: true},

              ],
              title: title,
              rows: rows
            }
            return sheet;
          };

          var sheets = [
            createSheet( createDetailsRows(),'details'),
            $scope.chartsData.fileType?createSheet( createChartRows(null,$scope.chartsData.fileType),'File types'):
                createSheet( createChartRows(null,$scope.chartsData.fileExtension),'File extensions'),
            createSheet( createChartRows(null,$scope.chartsData.fileAccess),'File count per modified date')
          //  $scope.chartsData.rootFolder? createSheet( createChartRows(EFilterContextNames.root_folder,$scope.chartsData.rootFolder),$scope.charts.rootFolder.name):
            //createSheet( createChartRows(EFilterContextNames.group,$scope.chartsData.group),$scope.charts.group.name),
            //createSheet( createChartRows(EFilterContextNames.owner,$scope.chartsData.owner),$scope.charts.owner.name),
            //createSheet( createChartRows(EFilterContextNames.doc_type,$scope.chartsData.docType),$scope.charts.docType.name),
            //createSheet( createChartRows(EFilterContextNames.tag_type,$scope.chartsData.tagType),$scope.charts.tagType.name),
            //createSheet( createChartRows(EFilterContextNames.acl_read, $scope.chartsData.aclRead),$scope.charts.aclRead.name),
            //createSheet( createChartRows(EFilterContextNames.acl_write,$scope.chartsData.aclWrite),$scope.charts.aclWrite.name),
            //createSheet( createChartRows(EFilterContextNames.functional_role,$scope.chartsData.functionalRole),$scope.charts.functionalRole.name),
            //createSheet( createChartRows(EFilterContextNames.bizList_item_clients,$scope.chartsData.bizListItemClients),$scope.charts.bizListItemClients.name),
            //createSheet( createChartRows(EFilterContextNames.bizList_item_clients_extraction,$scope.chartsData.bizListItemClientsExtraction),$scope.charts.bizListItemClientsExtraction.name),
            //createSheet( createChartRows(EFilterContextNames.bizList_item_consumers_extraction_rule,$scope.chartsData.bizListItemConsumersExtractionRule),$scope.charts.bizListItemConsumersExtractionRule.name),
          ];
            availableContexts.forEach(c => {
              if($scope.viewPermitted(c))
              {
                sheets.push(createSheet( createChartRows(c,$scope.chartsData[c.toString()]),$scope.charts[c.toString()].name));

              }
            });
            $scope.chartsData.rootFolder?  sheets.push(createSheet( createChartRows(EFilterContextNames.root_folder,$scope.chartsData.rootFolder),$scope.charts.rootFolder.name)):null;
          if($scope.isFolderReport()) {
            sheets.push(createSheet(createChartRows(EFilterContextNames.group, $scope.chartsData.groupFullyContained), 'Group Fully Contained'));
            sheets.push(createSheet(createChartRows(EFilterContextNames.group, $scope.chartsData.groupNotFullyContained), 'Group Not Fully Contained'));
          }
          var workbook = new kendo.ooxml.Workbook({sheets:sheets});
          kendo.saveAs({dataURI: workbook.toDataURL(), fileName: $scope.contextTitleName +'_'+ $scope.name+'.xlsx'});
        }
      };

      var createEntitiesCharts = function() {
        if($scope.id) {



          var charts = {};

          availableContexts.forEach(c => {
            if (c&&!c.equals($scope.contextName) && $scope.viewPermitted(c) ){
              if($scope.isFolderReport()&& c== EFilterContextNames.group)
              {
                var newChartFullyContained = createChart($scope.contextName, c, $scope.id,EFullyContainedFilterState.fullyContained,true);
                var newChartNotContained = createChart($scope.contextName, c, $scope.id,EFullyContainedFilterState.notContained,true);
                charts['groupFullyContained'] = newChartFullyContained;
                charts['groupNotFullyContained'] = newChartNotContained;
              }

              var newChart = createChart($scope.contextName, c, $scope.id);
              if (newChart.populationJsonUrl) {
                charts[c.toString()] = newChart;
              }
            }
          });
          if (charts['rootFolder']) {
            charts['rootFolder'].name = 'Top Folder Directories';
          }
          $scope.charts = charts;
        }
      };

      var capitalizeFirstLetters = function (input:string) :string {
        return input;
        if (input !== null) {
          return input.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          });
        }
        return '';
      };

      var createChart = function(thisContext:EFilterContextNames,thatContext:EFilterContextNames,thisId,fullyContained?:EFullyContainedFilterState,forceDisableUnassociated?:boolean)
      {
        !fullyContained?'':fullyContained.toString();
        var newChart = new ui.SavedChart();
        newChart.chartType = ui.SavedChart.chartType_FILES_BY_ENTITY;
        newChart.chartDisplayType = ui.SavedChart.chartDisplayType_BAR_HORIZONTAL;
        newChart.sortType = ui.SavedChart.sortType_SIZE;
        newChart.maximumNumberOfEntries = 20;
        newChart.showOthers = true;
        var viewConfiguration=new ui.SavedChartViewConfiguration();
        viewConfiguration.forceDisableUnassociated=forceDisableUnassociated;
        newChart.populationSubsetJsonFilter =viewConfiguration;
        newChart.hidden = true;
        newChart.name = capitalizeFirstLetters('Top '+$filter('translate')(thatContext.toString()))+ (!fullyContained?'':' '+$filter('translate')('chart_'+fullyContained.toString()));
        newChart.mainEntityType = EntityDisplayTypes.convertEFilterContextNames(thatContext).toString();
        if($scope.isFolderReport()) {
          var includeSubfolderData = true;
          newChart.populationJsonUrl = getUrl(thisContext,thatContext,thisId,includeSubfolderData,fullyContained,false);
          //newChart.populationJsonFilter  =JSON.stringify(subFolderFilter);
        }
        else {
          newChart.populationJsonUrl = getUrl(thisContext,thatContext,thisId,false,null,false);
        }

      //  newChart.mainEntityType = $scope.leftPaneType.toString();
        return newChart;

      }




      function getUrl(restrictedEntityContextContext:EFilterContextNames,thatContext:EFilterContextNames,restrictedEntityId,includeSubfolderData,fullyContained:EFullyContainedFilterState,collapsedData) : string
      {
        var entityDisplayType = EntityDisplayTypes.convertEFilterContextNames(thatContext);
        if(entityDisplayType.equals(EntityDisplayTypes.doc_types)){
          entityDisplayType = EntityDisplayTypes.doc_types_flat;
        }
        else if(entityDisplayType.equals(EntityDisplayTypes.departments)){
          entityDisplayType = EntityDisplayTypes.departments_flat;
        }
        else if(entityDisplayType.equals(EntityDisplayTypes.matters)){
          entityDisplayType = EntityDisplayTypes.matters_flat;
        }
        var restrictedEntityDisplayType = null;
        if(restrictedEntityContextContext)
        {
         restrictedEntityDisplayType =  EntityDisplayTypes.convertEFilterContextNames(restrictedEntityContextContext);
        }
        if(entityDisplayType == EntityDisplayTypes.root_folders )
        {
          return configuration.group_root_folders_url.replace('{'+restrictedEntityContextContext+'Id}',restrictedEntityId);
        }
        return GridBehaviorHelper.getDataUrlBuilder2(entityDisplayType,restrictedEntityDisplayType,restrictedEntityId,configuration,includeSubfolderData,fullyContained,collapsedData);
      }
      var onError = function(e)
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
      $scope.onFileTypeHistogramCompleted=function(fileTypeHistogram:Dashboards.DtoMultiValueList)
      {
        $scope.chartsData.fileType = translateDtoMultiValueList(fileTypeHistogram).data;
      };
      $scope.onFileExtensionHistogramCompleted=function(fileExtensionHistogram:Dashboards.DtoMultiValueList)
      {
        $scope.chartsData.fileExtension = translateDtoMultiValueList(fileExtensionHistogram).data;
      };

      $scope.onGetFilesAccessHistogramCompleted=function(fileAccessHistogram:Dashboards.DtoMultiValueList)
      {
        $scope.chartsData.fileAccess = translateDtoMultiValueList(fileAccessHistogram).data;
      };

      var highLightError = function(srch:string) {
        return function (e) {
          log.debug("Highlight error(" + srch + "): err=" + JSON.stringify(e.data) + " status=" + e.status);
          $scope.showFileSearchSinippets=true;
          $scope.fileModifiedSinceScanned = false;
          $scope.filehighlightedContentHtml = [];
          return false;
        }
      };

      $scope.highlightFileContent = function(searchText) {
        if(searchText) {
          $scope.fileModifiedSinceScanned = false;
          $scope.filehighlightedContentHtml = null;
          $scope.showFileSearchSinippets = true;
          var contentFilter = filterFactory.createContentSearch()(searchText);
          fileResource.getFileHighlightsWithFilter($scope.id,
              contentFilter.toKendoFilter(),
              50, 100,
              function (htmlContent: Entities.DtoFileHighlights) {
                $scope.showFileSearchSinippets = true;
                $scope.fileModifiedSinceScanned = htmlContent.fileContentLastModifiedDate > $scope.lastModifiedDate;
                $scope.filehighlightedContentHtml = htmlContent.snippets;
                if (htmlContent.locations) {
                  var sheetNames = [];
                  for (var sheet in htmlContent.locations) {
                    sheetNames.push({sheetName: sheet, cells: (<any>htmlContent).locations[sheet]});
                  }
                  $scope.excelLocations = sheetNames;
                }
              },
              highLightError(searchText));
        }
      };

      $scope.findPatternsInFile = function() {
        $scope.fileModifiedSinceScanned = false;
        $scope.filePatternsSnippets = null;
        $scope.showFilePatternsSnippets=true;
        fileResource.getFilePatterns($scope.id,
            50, 100,
            function(htmlContent:Entities.DtoFileHighlights) {
              $scope.showFilePatternsSnippets=true;
              $scope.fileModifiedSinceScanned = htmlContent.fileContentLastModifiedDate > $scope.lastModifiedDate;
              $scope.filePatternsSnippets =  htmlContent.snippets;
              if((<any>htmlContent).locations) {
                var sheetNames = [];
                for (var sheet in (<any>htmlContent).locations) {
                  sheetNames.push({sheetName: sheet, cells: (<any>htmlContent).locations[sheet]});
                }
                $scope.excelPatternSnippetsLocations = sheetNames;
              }
            },
            highLightError('<patExt>'));
      };

      $scope.findExtractionsInFileContent= function() {
        $scope.fileModifiedSinceScanned = false;
        $scope.fileExtractionSinippets = null;
        $scope.showFileExtractionSinippets=true;
        fileResource.getFileEntityExtractions($scope.id,
            50, 100,
            function (htmlContent:Entities.DtoFileHighlights) {
              $scope.showFileExtractionSinippets=true;
              $scope.fileModifiedSinceScanned = htmlContent.fileContentLastModifiedDate > $scope.lastModifiedDate;
              $scope.fileExtractionSinippets =  htmlContent.snippets;
              if((<any>htmlContent).locations) {
                var sheetNames = [];
                for (var sheet in (<any>htmlContent).locations) {
                  sheetNames.push({sheetName: sheet, cells: (<any>htmlContent).locations[sheet]});
                }
                $scope.excelExtractionSnippetLocations = sheetNames;
              }
            },
            highLightError('<entExt>'));
      };

      var translateDtoMultiValueList = function(val:Dashboards.DtoMultiValueList):Dashboards.DtoMultiValueList {
        var res:Dashboards.DtoMultiValueList = <Dashboards.DtoMultiValueList>
            {
              name: val.name,
              type: val.type,
              data: []
            };

        for (var i:number=0;i<val.data.length;++i) {
          var counterItem:Dashboards.DtoCounterReport = val.data[i];
          res.data.push(new Dashboards.DtoCounterReport($filter('translate')(counterItem.name),counterItem.description, counterItem.counter));
        }
        return res;
      };

      var getIconName = function(contextName:EFilterContextNames) {
        if(configuration['icon_'+contextName.toString()]) {
          return configuration['icon_'+contextName];
        }
        return '';
      };

      $scope.associationIcon = getIconName(EFilterContextNames.bizList_item_clients);
      $scope.clientExtractionIcon =configuration.icon_bizListItemClientExtraction;
      $scope.consumerExtractionIcon = getIconName(EFilterContextNames.bizList_item_consumers_extraction_rule);
      $scope.tagIcon = getIconName(EFilterContextNames.tag);
      $scope.docTypeIcon = getIconName(EFilterContextNames.doc_type);
      $scope.departmentIcon = getIconName(EFilterContextNames.department);
      $scope.matterIcon = getIconName(EFilterContextNames.matter);
      $scope.functionalRoleIcon = getIconName(EFilterContextNames.functional_role);
      $scope.groupIcon = getIconName(EFilterContextNames.group);

    });

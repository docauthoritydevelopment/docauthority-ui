///<reference path='../../../common/all.d.ts'/>
'use strict';
angular.module('directives.dialogs.recipient', [

    ])
    .controller('recipientDialogCtrl',
        function ($window, $scope, $uibModalInstance, configuration, data, dialogs, $q) {

          $scope.recipientIcon = configuration.icon_recipient_address;
          $scope.recipients = data.recipients;

          $scope.close = function(){
            $uibModalInstance.dismiss('Canceled');
          };
    });





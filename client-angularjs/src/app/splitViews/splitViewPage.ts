///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('reports',[
  'reports.details',
  'services.auth',
  'groups',
  'folders',
  'directives.reports',
  'directives.dialogs.labels',
  'directives',
    'directives.splitView.toolbars.editMode'

])
.config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.discoverSplitView, {
        parent:ERouteStateName.discover,
        url: '/:left/:right?workflowId?&view?&findIdLeft?&pageLeft?&pageRight?&filterRightDisabled?&activePan?&readOnly?&fullyContained?&userSearchOnlyFilterRight?&includeSubEntityData?&collapsedDataRight?&selectedItemLeft?&selectedItemRight?&filter?',
     //   templateUrl: '/app/splitViews/splitViewPage.tpl.html',
        resolve: {
          userAuth: function($stateParams, currentUserDetailsProvider:ICurrentUserDetailsProvider) {
            return currentUserDetailsProvider.checkAuthReady();
          }
        },
        views: {
          '': {
            templateUrl: '/app/splitViews/splitViewPage.tpl.html',
            controller: 'SplitViewPageCtrl',
          },
          'centerSupHeader@': {
            template: ' <ul class="nav layout-horizontal full-width"> <li><user-search-box ></user-search-box></li></ul>'

          },
          'rightToolbarSupHeader@': {
            template:
            ' <ul class="nav pull-right layout-horizontal"><li><span class="hidden-sm"></span>' +
            '<button mode="left" tooltip-direction="bottom" title="View only left side" toggle-kendo-splitter class="btn btn-flat btn-sm"><i class="fa fa-list-alt"></i></button>' +
            ' <button mode="both" tooltip-direction="bottom" title="View both sides" toggle-kendo-splitter class="btn btn-flat active btn-sm"><i class="fa fa-columns"></i></button>' +
            ' <button mode="right" tooltip-direction="bottom" title="View only right side" toggle-kendo-splitter class="btn btn-flat btn-sm"><i class="fa fa-list-alt"></i></button>' +
            '  <button ng-hide="true" class="btn btn-flat" ng-click="testBug()"><i class="fa fa-bug"></i></button> </li> </ul>'+
            '<split-view-edit-mode-toolbar class="nav pull-right " style="margin-right:12px"></split-view-edit-mode-toolbar>',
          }
        },
      })

  })
  .controller('SplitViewPageCtrl', function ($scope,$timeout,$element,$filter,$state,$stateParams,$compile,$window,filterFactory:ui.IFilterFactory,userSettings:IUserSettingsService,
                                             Logger:ILogger,propertiesUtils, configuration:IConfig,splitViewHtmlPartsFactory,
                                             eventCommunicator:IEventCommunicator,splitViewFilterService:IFilterService,
                                             currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('SplitViewPageCtrl');
   var _this = this;
    $scope.workflowId = $stateParams.workflowId || null;
    var leftEntityType = $stateParams.left ? (currentUserDetailsProvider.isInstallationModeLegal() ? EntityDisplayTypes.parseLegalInstallationMode($stateParams.left.toLowerCase()) : EntityDisplayTypes.parse($stateParams.left.toLowerCase())) :  null;
     $scope.leftEntityType= leftEntityType &&  isElligibleLeftOption(leftEntityType)? leftEntityType:  EntityDisplayTypes.groups;
     $scope.leftEntityTypeName= $scope.leftEntityType.toString();

    var rightEntityType = $stateParams.right ? (currentUserDetailsProvider.isInstallationModeLegal() ? EntityDisplayTypes.parseLegalInstallationMode($stateParams.right.toLowerCase()) : EntityDisplayTypes.parse($stateParams.right.toLowerCase())) :  null;
    $scope.rightEntityType= rightEntityType && isElligibleRightOption(rightEntityType)? rightEntityType:  EntityDisplayTypes.files;
    $scope.rightEntityTypeName =  $scope.rightEntityType.toString();

    $scope.changeLeftPage = $stateParams.pageLeft && propertiesUtils.isInteger($stateParams.pageLeft) ? parseInt($stateParams.pageLeft) :null;
    $scope.changeRightPage= $stateParams.pageRight && propertiesUtils.isInteger( $stateParams.pageRight ) ? parseInt( $stateParams.pageRight ) : null;

    $scope.changeActivePan= $stateParams.activePan && $stateParams.activePan.toLowerCase()=='right' ? 'right' : 'left';

    $scope.changeSelectedItemLeft=propertiesUtils.decodeURIParam($stateParams.selectedItemLeft);
    $scope.changeSelectedItemRight=propertiesUtils.decodeURIParam($stateParams.selectedItemRight);

    $scope.changeDisableRightFilter= $stateParams.filterRightDisabled && propertiesUtils.isBoolean( $stateParams.filterRightDisabled ) ? propertiesUtils.parseBoolean( $stateParams.filterRightDisabled ) : null;
    $scope.changeUserSearchOnlyFilterRight= $stateParams.userSearchOnlyFilterRight && propertiesUtils.isBoolean( $stateParams.userSearchOnlyFilterRight ) ? propertiesUtils.parseBoolean( $stateParams.userSearchOnlyFilterRight ) : null;
    $scope.changeIncludeSubEntityData= $stateParams.includeSubEntityData && propertiesUtils.isBoolean( $stateParams.includeSubEntityData ) ? propertiesUtils.parseBoolean( $stateParams.includeSubEntityData ) : true;
    $scope.changeCollapsedDataRight= $stateParams.collapsedDataRight && propertiesUtils.isBoolean( $stateParams.collapsedDataRight ) ? propertiesUtils.parseBoolean( $stateParams.collapsedDataRight ) : null;
    $scope.changeFullyContainedFilterState= $stateParams.fullyContained? EFullyContainedFilterState.parse($stateParams.fullyContained) : EFullyContainedFilterState.all;

    $scope.findIdLeft = $stateParams.findIdLeft;
    if( $scope.findIdLeft&& $scope.findIdLeft!='') {
      $scope.changeLeftPage =null;
      $stateParams.findIdLeft = '';
      ;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    var setUserRoles = function() {
      $scope.isViewFuncRoleAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation]);
      $scope.isViewTagAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagAssociation],null,true);
      $scope.isViewBizListAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],null,true);
      $scope.isViewDocTypeAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeAssociation],null,true);
      $scope.isViewDepartmentAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true);
      $scope.isViewMatterAssociationUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],null,true);
    };
    setUserRoles();
    $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
      setUserRoles();
    });



  //  allTypes.sort( propertiesUtils.sortAlphbeticFun('value'));


    var init = function() {
      var allTypes;
      if (currentUserDetailsProvider.isInstallationModeLegal()) {
        allTypes = EntityDisplayTypes.allLegalInstallationMode();
      }
      else {
        allTypes = EntityDisplayTypes.all();
      }

      if (!currentUserDetailsProvider.isLabelingEnabled()){
        allTypes= allTypes.filter(t=> !t.equals(EntityDisplayTypes.labels));
      }

      if (!currentUserDetailsProvider.isSharePermissionEnabled()) {
        allTypes= allTypes.filter(t=> !t.equals(EntityDisplayTypes.share_permissions_allow_read));
      }

      if (!currentUserDetailsProvider.isMetadataEnabled()){
        allTypes= allTypes.filter(t=> !t.equals(EntityDisplayTypes.metadatas));
      }

      setLeftView($scope.leftEntityType);
      setRightView($scope.rightEntityType);

      $scope.viewLeftOptions = createLeftNavigateOptions(allTypes.filter(t=> !t.equals(EntityDisplayTypes.files)));
      $scope.viewRightOptions = createRightOptions(allTypes.filter(t=> !t.equals(EntityDisplayTypes.all_files)));
      $scope.followUpOptions = createFollowUpOptions(allTypes.filter(t=> !t.equals(EntityDisplayTypes.all_files)));


    };

    function openInNewTab(stateName,urlParams)
    {
      var url = $state.href(stateName, urlParams);
      url = url.replace('/v1','');
      $window.open(url,'_blank');
    }
  function isElligibleLeftOption(entityDisplayTypes:EntityDisplayTypes){
      if(entityDisplayTypes == EntityDisplayTypes.files){
        return false;
      }
      return true;
  }
    function isElligibleRightOption(entityDisplayTypes:EntityDisplayTypes){
      if(entityDisplayTypes == EntityDisplayTypes.all_files){
        return false;
      }
      return true;
    }
    //create($scope.leftEntityTypeName, null);
    //create(null, $scope.rightPaneTypeName);
    function setLeftView(entityDisplayTypes:EntityDisplayTypes){
      var html = splitViewHtmlPartsFactory.createLeft(entityDisplayTypes,isRestrictedEntityTypeIdEnabled(entityDisplayTypes));
      $scope.leftHtml = html;
      $scope.leftName = $filter('translate')(entityDisplayTypes.toString());
    }
    function setRightView(entityDisplayTypes:EntityDisplayTypes){
      var html = splitViewHtmlPartsFactory.createLeft(entityDisplayTypes,isRestrictedEntityTypeIdEnabled(entityDisplayTypes));
      var html = splitViewHtmlPartsFactory.createRight(entityDisplayTypes, $scope.leftEntityType,isRestrictedEntityTypeIdEnabled(entityDisplayTypes));

      $scope.rightHtml = html;
      $scope.rightName =$filter('translate')(entityDisplayTypes.toString());
    }

   function isRestrictedEntityTypeIdEnabled(entityDisplayTypes:EntityDisplayTypes):boolean {
      if (entityDisplayTypes == EntityDisplayTypes.file_created_dates)
        return true;
      if (entityDisplayTypes == EntityDisplayTypes.file_modified_dates)
        return  true;
      if (entityDisplayTypes == EntityDisplayTypes.file_accessed_dates)
        return  true;
      return false;
    };


    function  changeRightView(paneOption:EPaneOption) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDiscoverViewChanged, { action: 'onViewChanged' });

      $stateParams['right'] =paneOption.view;
      $stateParams['pageRight'] = 1;

      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
      $scope.rightEntityTypeName = paneOption.view;
      $scope.rightEntityType = (currentUserDetailsProvider.isInstallationModeLegal() ? EntityDisplayTypes.parseLegalInstallationMode($scope.rightEntityTypeName) : EntityDisplayTypes.parse($scope.rightEntityTypeName));
      setRightView($scope.rightEntityType);

    };

   function followUpHandler(paneOption:EPaneOption) {
     const urlParams = {};
     urlParams['left'] = $scope.rightEntityTypeName;
     urlParams['right'] = paneOption.view;
     urlParams['pageLeft'] =  $state.params['pageRight'];
     urlParams['selectedItemLeft']= encodeURIComponent($state.params['selectedItemRight']);
     urlParams['selectedItemRight']='';
     urlParams['pageRight'] = 1;
     urlParams['activePan'] = 'left';
     urlParams['filter']=splitViewFilterService.getThinFilterCopy();

     openInNewTab($state.current, urlParams);

    };



    $scope.openNewTab=function(left:EntityDisplayTypes,right:EntityDisplayTypes,findIdLeft,selectedItemRight, withFilter:boolean) {
      var params = {};
      params['left'] = left.value;
      params['right'] = right.value;
      params['findIdLeft']= findIdLeft?findIdLeft:'';
      params['selectedItemRight']=selectedItemRight?selectedItemRight:'';
      params['activePan'] = 'left';
      params['selectedItemLeft']='';

      if(withFilter) {
        var thinFilterCopy = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
          if (value == null) {
            return undefined;
          }
          return value;
        });
        params['filter'] = thinFilterCopy; //remove filter from url cause filter is set trough service
      }
      else {
        params['filter'] ='';
      }
      openInNewTab($state.current, params);
    };


    $scope.openManageAssociationPage = function(selectedBizListId,selecteditemId) {
      openInNewTab(ERouteStateName.bizListsManagement,{selectedListId:selectedBizListId,selectedItemId:selecteditemId});
    };
    $scope.openManageDocTypesPage = function(selectedItemId) {
      openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.docTypes,selectedItem:selectedItemId});
    };
    $scope.openManageViewsPage = function(selectedItemId) {
      openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.savedViews,selectedItem:selectedItemId});
    };
    $scope.openManageDepartmentPage = function(selectedItemId) {
      openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.settingsDepartments,selectedItem:selectedItemId});
    };
    $scope.openManageMatterPage = function(selectedItemId) {
      openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.settingsMatters,selectedItem:selectedItemId});
    };
    $scope.openManageFunctionalRolesPage = function(selectedItemId) {
      openInNewTab(ERouteStateName.functionalRolesManagement,{selectedId:selectedItemId, page:1});
    };
   $scope.openManageTagTypesPage = function(selectedItemId) {
     openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.tagTypes,selectedItem:selectedItemId});
   };
   function createLeftNavigateOptions(allTypes){
     var leftOptions = [];
     allTypes.forEach(t=> {
           leftOptions.push(createPaneOption(t, changeLeftView));
       }
     );
     return leftOptions;
   }
    function createRightOptions(allTypes) {
      var rightOptions = [];
      allTypes.forEach(t => {
          rightOptions.push(createPaneOption(t, changeRightView));
        }
      );
     return rightOptions;
    }
      var createFollowUpOptions = function(allTypes) {
        var leftFollowUp = function(includeSubEntityDataLeft:boolean,fullyContainedState:EFullyContainedFilterState) {
          var filterById;
         if(!includeSubEntityDataLeft) {
           filterById= filterFactory.createFilterByID( $scope.leftEntityType);
          }
          else if($scope.leftEntityType==EntityDisplayTypes.folders||$scope.leftEntityType==EntityDisplayTypes.folders_flat) {
            filterById = filterFactory.createSubFolderFilter();
          }
          else if($scope.leftEntityType==EntityDisplayTypes.doc_types) {
            filterById = filterFactory.createSubDocTypesFilter();
          }
         else if($scope.leftEntityType==EntityDisplayTypes.departments || $scope.leftEntityType==EntityDisplayTypes.departments_flat) {
           filterById = filterFactory.createSubDepartmentFilter();
         }
         else if($scope.leftEntityType==EntityDisplayTypes.matters || $scope.leftEntityType==EntityDisplayTypes.matters_flat) {
           filterById = filterFactory.createSubMatterFilter();
         }
          var fullyContainedFilter;
          if(fullyContainedState == EFullyContainedFilterState.fullyContained)
          {
            fullyContainedFilter = filterFactory.createFullyContainedFilterById(true,EFilterContextNames.convertEntityDisplayType($scope.leftEntityType),EFilterContextNames.convertEntityDisplayType($scope.rightEntityType));
          }
          else if(fullyContainedState == EFullyContainedFilterState.notContained)
          {
            fullyContainedFilter = filterFactory.createFullyContainedFilterById(false,EFilterContextNames.convertEntityDisplayType($scope.leftEntityType),EFilterContextNames.convertEntityDisplayType($scope.rightEntityType));
          }
          if(fullyContainedFilter)
          {
            return function(id,name=null) {
               return new AndCriteria([fullyContainedFilter(id,name)], [filterById(id,name)]);
            }
          }
          return filterById;
        }

        var rightFollowUp = filterFactory.createFilterByID($scope.rightEntityTypeName);
        var rightFollowUpOptions = [];
        allTypes.forEach(t=>rightFollowUpOptions.push(createPaneOption(t, followUpHandler, leftFollowUp, rightFollowUp)) );
       return rightFollowUpOptions;
      }

     $scope.viewRightDisabled = function (leftEntityTypeName:string,rightPaneOption:EPaneOption,str):boolean {
       if(rightPaneOption==null|| !leftEntityTypeName|| leftEntityTypeName=='')
       {
         return true;
       }
       if(rightPaneOption.view==leftEntityTypeName){
         return true;
       }
        var leftPane:string = leftEntityTypeName;
      //  if (startsWith(leftPane, "folders")) {
     //     leftPane = "folders";
      //  }
       if (startsWith(leftPane, "patterns")) {
         leftPane = "patterns";
         return startsWith(rightPaneOption.view, leftPane);
       }
        if (startsWith(leftPane, "acl_")) {
          leftPane = "acl_";
          return startsWith(rightPaneOption.view, leftPane);
        }
      // var result= startsWith(rightPaneOption.view, leftPane);

        return false;
      };
      $scope.viewPermitted = function(paneOption:EPaneOption) {
       var entityType: EntityDisplayTypes = paneOption.entityDisplayType;
        if(entityType ==EntityDisplayTypes.bizlistItem_clients_extractions||entityType ==EntityDisplayTypes.bizlistItem_consumers_extraction_rules || entityType ==EntityDisplayTypes.bizlistItems_clients) {
         return $scope.isViewBizListAssociationUser;
       }
        if(entityType ==EntityDisplayTypes.doc_types)
        {
          return $scope.isViewDocTypeAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.tag_types||entityType ==EntityDisplayTypes.tags)
        {
          return $scope.isViewTagAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.departments || entityType ==EntityDisplayTypes.departments_flat)
        {
          return $scope.isViewDepartmentAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.matters || entityType ==EntityDisplayTypes.matters_flat)
        {
          return $scope.isViewMatterAssociationUser;
        }
        if(entityType ==EntityDisplayTypes.functional_roles)
        {
          return $scope.isViewFuncRoleAssociationUser;
        }
        return true;
      };

      $scope.isExportToExcelDisabled = function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.folders)
          return true;
        return false;
      };
      $scope.isFollowUpOptionDisabled = function (paneOption:EPaneOption):boolean { //the option in the ddl
        return (paneOption.view === $scope.rightEntityTypeName);
      };
      $scope.isFollowUpDisabled = function (leftEntityDisplayType:EntityDisplayTypes, rightEntityDisplayType:EntityDisplayTypes):boolean {
        if (rightEntityDisplayType == EntityDisplayTypes.files)
          return true;
        if (leftEntityDisplayType == EntityDisplayTypes.all_files)
          return true;
        return false;
      }

      $scope.isBizListAssociationDisabled= function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.groups)
         return false;
        if (entityDisplayTypes == EntityDisplayTypes.folders)
          return  false;
        if (entityDisplayTypes == EntityDisplayTypes.folders_flat)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return false;
        return true;
      };

      $scope.isFunctionalRoleDisabled= function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.groups)
         return false;
        if (entityDisplayTypes == EntityDisplayTypes.folders)
          return  false;
        if (entityDisplayTypes == EntityDisplayTypes.folders_flat)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return false;
        return true;
      };
      $scope.isDocTypeDisabled= function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.groups)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.folders)
          return  false;
        if (entityDisplayTypes == EntityDisplayTypes.folders_flat)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return false;
        return true;
      };
    $scope.isDepartmentDisabled= function (entityDisplayTypes:EntityDisplayTypes):boolean {
      if (entityDisplayTypes == EntityDisplayTypes.folders)
        return  false;
      if (entityDisplayTypes == EntityDisplayTypes.folders_flat)
        return false;
      return true;
    };
      $scope.isTagDisabled = function (entityDisplayTypes:EntityDisplayTypes):boolean {
        /*if (splitViewFilterService.getFilterData().hasCriteria())
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.groups)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.folders)
          return  false;
        if (entityDisplayTypes == EntityDisplayTypes.folders_flat)
          return false;
        if (entityDisplayTypes == EntityDisplayTypes.tag_types)
          return false;*/
        return false;

      };
      $scope.isChartDisabled = function (entityDisplayTypes:EntityDisplayTypes):boolean {
        /* if (entityDisplayTypes == EntityDisplayTypes.regulations)
          return true;
        if (entityDisplayTypes == EntityDisplayTypes.patterns)
          return true; */
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return true;
        // if (entityDisplayTypes == EntityDisplayTypes.folders)
        //   return  true;
        if (entityDisplayTypes == EntityDisplayTypes.all_files)
          return true;
        // if (entityDisplayTypes == EntityDisplayTypes.departments)
        //   return true;
        return false;
      };
      $scope.isImmediateActionsDisabled = function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.files)
          return false;
        return true;
      };

      $scope.isCreatePolicyObjectDisabled = function (entityDisplayTypes:EntityDisplayTypes):boolean {
        if (entityDisplayTypes == EntityDisplayTypes.acl_reads ||entityDisplayTypes == EntityDisplayTypes.acl_writes ||entityDisplayTypes == EntityDisplayTypes.owners ||entityDisplayTypes == EntityDisplayTypes.extensions )
          return false;
        return true;
      };

      $scope.isSwitchViewsDisabled= function (leftEntityDisplayType:EntityDisplayTypes, rightEntityDisplayType:EntityDisplayTypes):boolean {
        if (rightEntityDisplayType == EntityDisplayTypes.files)
          return true;
        if (leftEntityDisplayType == EntityDisplayTypes.all_files)
          return true;
        return false;
      };

      $scope.isShowSunburstEnabled  = function (entityDisplayType:EntityDisplayTypes):boolean {
        if (entityDisplayType == EntityDisplayTypes.folders)
          return true;
        if (entityDisplayType == EntityDisplayTypes.folders_flat)
          return true;
        return false;
      }

      
      $scope.isDirectFilesToggleEnabled = function (entityDisplayType:EntityDisplayTypes):boolean {
        if (entityDisplayType == EntityDisplayTypes.folders)
          return true;
        if (entityDisplayType == EntityDisplayTypes.folders_flat)
          return  true;
        if (entityDisplayType == EntityDisplayTypes.doc_types)
          return  true;
        if (entityDisplayType == EntityDisplayTypes.departments)
          return  true;
        if (entityDisplayType == EntityDisplayTypes.departments_flat)
          return  true;
        if (entityDisplayType == EntityDisplayTypes.matters)
          return  true;
        if (entityDisplayType == EntityDisplayTypes.matters_flat)
          return  true;
        return false;
      };

      $scope.isFullyContainedToggleEnabled = function (leftEntityDisplayType:EntityDisplayTypes,rightEntityDisplayType:EntityDisplayTypes):boolean {
        if(rightEntityDisplayType== EntityDisplayTypes.groups) {
          if (leftEntityDisplayType == EntityDisplayTypes.folders)
            return true;
          if (leftEntityDisplayType == EntityDisplayTypes.folders_flat)
            return true;
        }
        return false;
      };
      $scope.isCollapsedDataRightEnabled = function (rightEntityDisplayType:EntityDisplayTypes):boolean {

          if (rightEntityDisplayType == EntityDisplayTypes.files)
            return true;

        return false;
      };


    $scope.getFilterDisplayedName = function(tag:SingleCriteria)
    {
        return filterFactory.getFilterReportDisplayedName(tag);
    }

    $scope.viewOptionHandler = function (paneOption:EPaneOption) {
      if (paneOption.handler) {
        paneOption.handler(paneOption);
      }
    };

    $scope.isFindByEnabled= function (entityDisplayType:EntityDisplayTypes):boolean {
      if (entityDisplayType == EntityDisplayTypes.folders)
        return true;
      return false;
    };
    $scope.isAttestationEnabled= function (entityDisplayType:EntityDisplayTypes):boolean {
      if (entityDisplayType == EntityDisplayTypes.folders || entityDisplayType == EntityDisplayTypes.folders_flat)
        return true;
      return false;
    };

    $scope.isFilterDatesEnabled= function (entityDisplayType:EntityDisplayTypes):boolean {
      if (entityDisplayType == EntityDisplayTypes.file_created_dates || entityDisplayType == EntityDisplayTypes.file_modified_dates || entityDisplayType == EntityDisplayTypes.file_accessed_dates)
        return true;
      return false;
    };

    var changeLeftView = function (paneOption:EPaneOption):void {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDiscoverViewChanged, { action: 'onViewChanged' });
      var rightView =  $scope.rightEntityTypeName;
      if(viewLeftDisabled(paneOption))
      {
        rightView=EntityDisplayTypes.files.toString();
      }

      $stateParams['left'] =paneOption.view;
      $stateParams['right'] = rightView;
      $stateParams['pageLeft'] = 1;
      $stateParams['pageRight'] = 1;
      $stateParams['selectedItemLeft'] ='';
      $stateParams['selectedItemRight'] ='';
  //    $stateParams['filter'] = 'none';

      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: true, inherit: false
          });

    };

    var viewLeftDisabled = function (paneOption:EPaneOption):boolean {
      if (paneOption.view === $scope.leftEntityTypeName) {
        return true;
      }
      var rightPane:string = $scope.rightEntityTypeName;
      if (startsWith(rightPane, "folders")) {
        rightPane = "folders";
      }
      if (startsWith(rightPane, "acl_")) {
        rightPane = "acl_";
      }
      return startsWith(paneOption.view, rightPane);
    };

    $scope.viewItemDetailsHandler = function(context,name,id){
      if(id)
      {
        $stateParams['context'] = context;
        $stateParams['name'] =name;
        $stateParams['id'] = id;

        $state.transitionTo('details',
            $stateParams,
            {
              notify: true, inherit: true
            });

      }
    };



    $scope.bizListSupportedTypes =[Entities.DTOBizList.itemType_CLIENTS];



    var startsWith = function (str1:string, str2:string):boolean {
      return str1.substring(0, str2.length) === str2;
    };

    $scope.onSelectedItemRight=function(idForRelocate)
    {
      $stateParams['selectedItemRight']=idForRelocate;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }


    $scope.onSelectedItemLeft=function(idForRelocate)
    {
      $stateParams['selectedItemLeft']=encodeURIComponent(idForRelocate);
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    };

    $scope.onActivePaneChanged = function(activePan) {
      $stateParams['activePan']=activePan;
      $state.transitionTo($state.current, $stateParams,{ notify: false, inherit: true });
    };

    $scope.switchPanes = function ():void {
      //verify all params are initiated. Due to bug:state transition performed twice when using optional state parameters
      $stateParams['left'] = $scope.rightEntityTypeName;
      $stateParams['right'] = $scope.leftEntityTypeName;
      $stateParams.findIdLeft= $stateParams['selectedItemRight'];
      $stateParams['selectedItemLeft']= encodeURIComponent($stateParams['selectedItemRight']);
      $stateParams['selectedItemRight']='';
      $stateParams['pageLeft']='';
      $stateParams['pageRight'] = 1;
      $stateParams['activePan'] = 'left';

      log.debug('xxx switchPanes set findIdLeft to $stateParams'+$stateParams.findIdLeft);

      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: true, inherit: false
          });
    };

    $scope.onClearFilter = function() {
      $scope.findIdLeft= propertiesUtils.decodeURIParam($stateParams['selectedItemLeft']);
      $scope.findIdRight= $stateParams['selectedItemRight'];
      $scope.changeSelectedItemLeft=propertiesUtils.decodeURIParam($stateParams.selectedItemLeft);
      $scope.changeSelectedItemRight=propertiesUtils.decodeURIParam($stateParams.selectedItemRight);
      $stateParams['filter']='none';
 //     delete($stateParams['filter']) ;
      $stateParams['pageLeft']='' ;
      $stateParams['pageRight']='' ;

      log.debug('xxx onClearFilter set findIdLeft to'+$scope.findIdLeft);
      log.debug('xxx onClearFilter set findIdRight to'+$scope.findIdRight);
      $state.transitionTo($state.current, $stateParams,{ notify: false, inherit: true });
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDiscoverViewChanged, { action: 'onClearFilter' });
    };

    $scope.onSetFilter = function() {
      var thinFilterCopy = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
        if (value == null) {
          return undefined;
        }
        return value;
      });
      if (splitViewFilterService.getFilterData().hasCriteria()) {
        $stateParams['filter'] = thinFilterCopy;
      }
      else {
        $stateParams['filter'] = "none";
      }
      $scope.findIdLeft= propertiesUtils.decodeURIParam($stateParams['selectedItemLeft']);
      $scope.findIdRight= propertiesUtils.decodeURIParam($stateParams['selectedItemRight']);
      $scope.changeSelectedItemLeft=propertiesUtils.decodeURIParam($stateParams.selectedItemLeft);
      $scope.changeSelectedItemRight=propertiesUtils.decodeURIParam($stateParams.selectedItemRight);

      $state.transitionTo($state.current, $stateParams,{ notify: false, inherit: true });
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDiscoverViewChanged, { action: 'onSetFilter' });
    };

    $scope.onPageLeftChanged = function(pageNumber:number)
    {
      $stateParams['pageLeft']=pageNumber;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }
    $scope.onPageRightChanged = function(pageNumber:number)
    {
      $stateParams['pageRight']=pageNumber;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    $scope.onFilterRightDisabledChanged = function(isDisabled) {
      $stateParams['filterRightDisabled'] = isDisabled;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    $scope.onUserSearchOnlyFilterRightChanged = function(isActive) {
      $stateParams['userSearchOnlyFilterRight'] = isActive;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    $scope.onIncludeSubEntityDataChanged = function(include:boolean) {
      $stateParams['includeSubEntityData'] = include;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    $scope.onCollapsedDataRightChanged = function(collapsed:boolean) {
      $stateParams['collapsedDataRight'] = collapsed;
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    $scope.onFullyContainedDataChanged = function(fullyContainedState:EFullyContainedFilterState) {
      $stateParams['fullyContained']=fullyContainedState.toString();
      $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
    }

    function createPaneOption(opt:EntityDisplayTypes, handler:(string)=>void,
                              leftFilterFunction?:(value: boolean,fullyContainedState?:EFullyContainedFilterState) =>(value,name) => any,rightFilterFunction?:(value,name) => any) : EPaneOption {

        return new EPaneOption($filter('translate')(opt.toString()), handler, opt,leftFilterFunction,rightFilterFunction);
    }
    init();
});

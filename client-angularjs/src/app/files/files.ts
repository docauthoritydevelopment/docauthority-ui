///<reference path='..\..\common\all.d.ts'/>
'use strict';

angular.module('files',[
  'resources.groups',
  'files.acl.read',
  'files.acl.write',
  'angular-clipboard',
  'files.dates',
  'directives.dialogs.files.grouping'

])

  .config(function ($stateProvider) {
    $stateProvider
      .state('groups.groupFiles', {
        url: '/files_report/{groupId}',
        params: {'groupId':null, 'name':null},
        templateUrl: '/files.tpl.html',
        controller: 'FilesPageCtrl'
      });
  })
  .controller('FilesCtrl', function
  ($scope, $stateParams, $timeout,$state,$location,$window, $filter,Logger:ILogger, propertiesUtils, configuration:IConfig,$element,bizListsResource:IBizListsResource,fileResource:IFileResource,
   userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,currentUserDetailsProvider:ICurrentUserDetailsProvider,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,filterFactory:ui.IFilterFactory,groupsResource:IGroupsResource ) {
    var _this = this;
    var log: ILog = Logger.getInstance('FilesCtrl');
    $scope.elementName='filesInfo-grid';
    $scope.entityDisplayType = EntityDisplayTypes.files;
    var gridOptions;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOFileInfo>($scope,log,configuration,eventCommunicator,userSettings,null,fields,
        parseData,$scope.dupData?dupDataRowTemplate:rowTemplate,null,null,getDataUrlBuilder,true,$scope.fixHeight);

      //gridOptions.gridSettings.dataSource.schema.total= function(response) {
      //  return response.totalElements; // total is returned in the "total" field of the response
      //}

      var doNotSelectFirstRowByDefault =$scope.fixHeight==null;
      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions,$scope.dupData?dupDataRowTemplate:rowTemplate, fields, onSelectedItemChanged,$element,
        doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

      updatePermissions();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        updatePermissions();
      });
    };


    var updatePermissions=function()
    {
      $scope.isTechUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.TechSupport]);
    }

    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig,
                                      includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState,collapsedData:boolean) {
      if( $scope.restrictedEntityDisplayTypeName == 'file' ) //For displaying duplicates list in file detailes page
      {
        if(restrictedEntityId) {
          //$scope.filterLeft = <ui.IDisplayElementFilterData>{
          //  filter: <ui.IKendoFilter>(filterFactory.createFileDuplicationsFilter()(restrictedEntityId,'Dup').toKendoFilter()),
          //  text: null
          //}
          var dupFilter = filterFactory.createFileDuplicationsFilter()(restrictedEntityId,'Dup').toKendoFilter();
          return configuration.files_url+'?filter='+encodeURIComponent(JSON.stringify(dupFilter));
        }
        $scope.clear?$scope.clear():null;
        return null;
      }

      if( $scope.restrictedEntityDisplayTypeName == 'mail' ) //For displaying duplicates list in file detailes page
      {
        if(restrictedEntityId) {
          var mailFilter = filterFactory.createMailFilter()(restrictedEntityId,$scope.filterData).toKendoFilter();
          return configuration.files_url+'?filter='+encodeURIComponent(JSON.stringify(mailFilter));
        }
        $scope.clear?$scope.clear():null;
        return null;
      }
      var url = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration,
        includeSubEntityData,fullyContained,collapsedData);
      return url;
    }
    var bizListsDic;
    var loadBizListList = function() {
      bizListsResource.getAllBizLists(null,function (listsByIdDic:any) {
          bizListsDic=listsByIdDic;

        },
        function (error) {
        }
      );
    }
    loadBizListList();
    var dTOFileInfo:Entities.DTOFileInfo =  Entities.DTOFileInfo.Empty();
    var dtoContentMetadata:Entities.DtoContentMetadata =  Entities.DtoContentMetadata.Empty();
    var prefixContentMetadataDtoFieldName =propertiesUtils.propName(dTOFileInfo, dTOFileInfo.contentMetadataDto) + '.';

    var id:ui.IFieldDisplayProperties =<ui.IFieldDisplayProperties> {
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.id),
      title: 'ID',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
    };
    var fileName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fileName),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      width: "35%",
      template: '<span title="#: fileName#">#: baseName#</span>'+
      "<span class='top-right-position' >" +
      "<a class='btn btn-link btn-xs da-cond-show1 ' target='_blank' ui-sref='details({ id: dataItem.id, context:contextName,name:dataItem.fileName})' title='View file details'><i   class='fa fa-info ng-hide'></i></a>" +
      "<a ng-if='dataItem.openFileAuthorized' class=' btn btn-link btn-xs' title='Open file' href='"+configuration.file_download_url.replace('{id}','#: id #')+"' target='_top'><i class='fa fa-download ng-hide' ></i></a>" +
      "<a class=' btn btn-link btn-xs' clipboard text='dataItem.fileName' title='copy path to clipboard'><i class='fa fa-copy ng-hide' ></i></a>" +
      "</span>"
    };
    var fileNameNewTemplate= '<span class="dropdown-wrapper dropdown " ><span class=" btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
      '<a title="File name: #: '+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fileName)+' #" class="title-secondary">#: '+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.baseName)+' # ' +
      `<i ng-if="restrictedEntityDisplayTypeName !== 'mail'  && restrictedEntityDisplayTypeName !== 'file'" class="caret ng-hide"></i></a></span>`+
      `<ul class="dropdown-menu"  ng-if="restrictedEntityDisplayTypeName !== 'mail' && restrictedEntityDisplayTypeName !== 'file'" > `+
      "<li><a class=' da-cond-show1 ' target='_blank' href='{{createDetailsPageUrl(dataItem.id,contextName)}}' title='View file details'><span  class='fa fa-info'></span> View file details</a></li>" +
      "<li><a  ng-if='dataItem.openFileAuthorized && !dataItem.openFileCheckRequired' title='Open file' href='"+configuration.file_download_url.replace('{id}','#: id #')+"' target='_top'><span class='fa fa-download' > </span> Open file</a></li>" +
      "<li><a  ng-if='dataItem.openFileAuthorized && dataItem.openFileCheckRequired' title='Open file' ng-click='isFileContentAuthorized(dataItem.id)'><span class='fa fa-download' > </span> Open file</a></li>" +
      "<li><a  ng-if='dataItem.fileHasNetWorkLink&&false' title='Open file' href='{{dataItem.fileName}}' target='_blank'><span class='fa fa-download' > </span> Open file in host application</a></li>" +
      "<li><a  ng-if='dataItem.openFileAuthorized && isTechUser' title='Open file' href='"+configuration.file_download_reingested_url.replace('{id}','#: id #')+"' target='_top'><span class='fa fa-download' > </span> Simulate pattern search</a></li>" +
      "<li ng-if='dataItem.hasCopies'><a  ng-click='setFilterByContentId(dataItem)' title='Filter for copies of this file'><span class='fa fa-filter'></span>  Filter by this content</a></li>"+
      "<li ng-if='dataItem.isMail'><a  ng-click='setFilterByMail(dataItem)' title='Filter for copies of this mail'><span class='fa fa-filter'></span>  Filter by this email</a></li>"+
      "<li ng-if='dataItem.isAttachment'><a  ng-click='setFilterByAttachment(dataItem)' title='Filter for copies of this mail'><span class='fa fa-filter'></span>  Filter by this email</a></li>"+
      "<li><a  clipboard text='dataItem.fileName' title='Copy path to clipboard'><span class='fa fa-copy ' ></span> Copy full path</a></li>" +
      "<li ng-if='!dataItem."+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+"'><a  ng-click='addGroupToFiles()'><span class='"+configuration.icon_group+"'></span> Add to group</a></li>"+
      "<li ng-if='dataItem.openFileAuthorized&&(dataItem.haveContentFilter||dataItem.havePatterns||dataItem.haveExtractions)' class='divider'></li>"+
      "<li ng-if='dataItem.openFileAuthorized'  ng-show='dataItem.haveContentFilter'><a ng-click='highlightFileContent(dataItem);'>Highlight text search in file</a></li>" +
      "<li ng-if='dataItem.openFileAuthorized' ng-show='dataItem.havePatterns'><a ng-click='highlightPatternsInFileContent(dataItem);'>Highlight patterns</a></li>" +
      "<li ng-if='dataItem.openFileAuthorized' ng-show='dataItem.haveExtractions'><a ng-click='highlightExtractionsInFileContent(dataItem);'>Highlight extractions</a></li>" +

      //"<li><a  ng-click='chaneLeftPane(\""+EntityDisplayTypes.folders_flat.toString()+"\",dataItem.folderName);' >Find file's folder</a></li>" +
      ' </ul></span> ';


    $scope.isFileContentAuthorized = function(id){
      fileResource.isFileContentAuthorized(id,function(isAuthorized:boolean){
        if(isAuthorized){
          $window.open(configuration.file_download_url.replace('{id}',id), "_top");
        }
        else{
          dialogs.notify("Operation canceled","Sorry, you do not have permissions to view this file content.")
        }
      },onError);
    }
    $scope.createDetailsPageUrl = function(id,context:EFilterContextNames){
      if(groupId) {
        const urlParams = {};
        urlParams['context'] = context.toString();
        urlParams['id'] = id;

        var url = $state.href(ERouteStateName.details, urlParams);
        url = url.replace('/v1','');
        return url;
        //  $window.open(url,'_blank');
      }
      return '';
    }
    //var viewContentTemplate1='<a ng-show="dataItem.showContentHtml" ng-click="dataItem.showContentHtml=!dataItem.showContentHtml">Hide results</a>' +
    //    '  <ul ng-show="dataItem.showContentHtml" class="highlightedContent"><li ng-repeat=" res in dataItem.filehighlightedContentHtml  track by $index" ng-bind-html="res" ></li>' +
    //    '<li ng-repeat=" location in dataItem.excelLocations  track by $index" ng-bind="Sheet {{location.sheetName}} Locations:{{location.cells}}"></li>' +
    //    '</ul>';
    var viewContentTemplate='<highlight-results file-snippets="dataItem.filehighlightedContentHtml" show-file-search-sinippets="dataItem.showContentHtml" excel-locations="dataItem.excelLocations"' +
      '  file-modified-since-scanned="dataItem.fileModifiedSinceScanned" search-text="dataItem.searchText" ></highlight-results>';


    var highLightError = function(dataItem: Entities.DTOFileInfo, srch:string) {
      return function (e) {
        log.debug("Highlight error(" + srch + "): err=" + JSON.stringify(e.data) + " status=" + e.status);
        (<any>dataItem).fileSnippets = [];
        (<any>dataItem).searchText =  srch;
        showHighlights(dataItem, null);
        return false;
      }
    };

    $scope.highlightFileContent = function(dataItem: Entities.DTOFileInfo) {
      onBeforeShowHighlights(dataItem);
      var searchTextItems = [];
      if ((<ui.IDisplayElementFilterData>$scope.filterData) && (<ui.IDisplayElementFilterData>$scope.filterData).text && (<ui.IDisplayElementFilterData>$scope.filterData).text.length > 0) {
        searchTextItems = (<ui.IDisplayElementFilterData>$scope.filterData).text.filter(f=>f.key == 'Content');
      }
      if (searchTextItems[0]) {
        //   var searchText = searchTextItems.map(s=>s.value).join(' ');
        fileResource.getFileHighlightsWithFilter(
          dataItem.id,
          (<ui.IDisplayElementFilterData>$scope.filterData).filter, 10, 100,
          function(htmlContent:Entities.DtoFileHighlights) {
            // (<any>dataItem).searchText =  searchTextItems.map(s=>s.value).join(' ');
            (<any>dataItem).searchText =  'Text search';
            showHighlights(dataItem, htmlContent);
          },
          highLightError(dataItem, 'text search')
        );
      }
    }

    $scope.highlightPatternsInFileContent = function(dataItem: Entities.DTOFileInfo) {
      onBeforeShowHighlights(dataItem);
      fileResource.getFilePatterns(
        dataItem.id, 10,100,
        function(htmlContent:Entities.DtoFileHighlights) {
          (<any>dataItem).searchText = 'Pattern extractions';
          showHighlights(dataItem,htmlContent);
        },
        highLightError(dataItem, 'pattern search')
      );
    }

    $scope.highlightExtractionsInFileContent = function(dataItem: Entities.DTOFileInfo) {
      onBeforeShowHighlights(dataItem);
      fileResource.getFileEntityExtractions(
        dataItem.id, 10, 100,
        function (htmlContent: Entities.DtoFileHighlights) {
          (<any>dataItem).searchText = 'Entity extractions';
          showHighlights(dataItem, htmlContent);
        },
        highLightError(dataItem, 'entity search')
      );
    }

    var onBeforeShowHighlights = function(dataItem: Entities.DTOFileInfo) {
      (<any>dataItem).fileModifiedSinceScanned =false;
      (<any>dataItem).filehighlightedContentHtml = null;
      (<any>dataItem).showContentHtml=true;
      (<any>dataItem).excelLocations=null;
    }
    var showHighlights = function(dataItem: Entities.DTOFileInfo, highlightsResults:Entities.DtoFileHighlights)
    {
      (<any>dataItem).fileModifiedSinceScanned = highlightsResults ? highlightsResults.fileContentLastModifiedDate > dataItem.fsLastModified : false;
      (<any>dataItem).filehighlightedContentHtml = highlightsResults ? highlightsResults.snippets : [];
      (<any>dataItem).showContentHtml=true;
      if(highlightsResults && highlightsResults.locations) {
        var sheetNames = [];
        for (var sheet in highlightsResults.locations) {
          sheetNames.push({sheetName: sheet, cells: highlightsResults.locations[sheet]});
        }
        (<any>dataItem).excelLocations = sheetNames;
      }
    }

    var baseName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fileName),
      title: 'Path',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
    };

    var startsWith = function (str1:string, str2:string):boolean {
      return str1.substring(0, str2.length) === str2;
    };
    $scope.getFileTypeClassName=function(type) {
      var font =$filter('fwfileType')(type);
      return font;
    };

    var groupName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupName),
      title: 'Group name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false,
      template: '#if ( groupName ) {#' +
      '<a class="top-right-position btn btn-link btn-sm"><i  ng-click="groupNameClicked(dataItem)" class="fa fa-filter ng-hide" title="Filter by group"></i>' +
      '</a> #: groupName# #}#',
      width: "27%",
    };

    $scope.groupNameClicked = function(item:Entities.DTOFileInfo) {

      setFilterByGroup(item.groupId,item.groupName);
    };
    $scope.locateGroup = function(item:Entities.DTOFileInfo) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDataChangeUserAction, {
        action: 'openNewTab',
        left:EntityDisplayTypes.groups,
        right:EntityDisplayTypes.files,
        findIdLeft:item.groupId,
        withFilter:false
      });
    };

    var groupNameNewTemplate =   '#if ( '+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+' || isFileExcluded) {#' + '<span class="dropdown-wrapper dropdown " ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  ng-if="dataItem.'+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+'" class="title-primary" title="File group: #: '+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupName)+' #">' +
      '<span group-type-html type="{{dataItem.groupType}}" file-type="{{dataItem.analyzeHint}}"></span> #: '+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupName)+' # </a>' +
      '<a  ng-if="dataItem.isFileExcluded && dataItem.mngGroupAuthorized" class="title-primary " title="Detached from native group" ><span class="ion-android-radio-button-off"></span><span ng-if="dataItem.processingRestoreGroup">&nbsp;&nbsp;Processing...</span></a>' +
      `<i class="caret ng-hide" ng-if="restrictedEntityDisplayTypeName !== 'file' && restrictedEntityDisplayTypeName !== 'mail' "></i></span>`+
      `<ul class="dropdown-menu"  ng-if="restrictedEntityDisplayTypeName !== 'file' && restrictedEntityDisplayTypeName !== 'mail' "> `+
      //"<li><a  ng-click='chaneLeftPane(\""+EntityDisplayTypes.groups.toString()+"\",dataItem."+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+");'>Set this group in left pane</a></li>"+
      "<li ng-if='dataItem."+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+"'><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this group</a></li>"+
      "<li ng-if='dataItem."+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+"'><a  ng-click='locateGroup(dataItem)'><span class='fa fa-map-marker'></span>  Locate this group</a></li>"+
      "<li ng-if='dataItem.mngGroupAuthorized && !dataItem.isFileExcluded && dataItem."+propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId)+"'><a  ng-click='detachFileFromGroup(dataItem)'><span class='fa fa-times'></span> Detach file from this group</a></li>"+
      "<li ng-if='dataItem.mngGroupAuthorized && dataItem.isFileExcluded'><a   ng-click='restoreOriginalGroupToFile(dataItem)' title='Restore file to its original native group'><span class='ion-loop' ></span> Re-assign to native group</a></li>" +
      '  </ul></span> # } #';

    var mailInfoTemplate = `<div class="file-grid-item" ng-if='dataItem.showMailSenderOrRecipients'> 
       <div ng-if="dataItem.sender && dataItem.sender.address" class="from long-and-truncated"  title="{{dataItem.sender.address}}">
          <div class="title">
             <span class="${configuration.icon_sender_address} icon"></span>
             <span>From:</span> 
          </div>
          <div  class="dropdown-wrapper dropdown dd-wrapper-from">
            <span class="btn-dropdown dropdown-toggle" data-toggle="dropdown">
               <div class="address long-and-truncated">
                <a style="color:gray">
                  <span>{{dataItem.sender.address}}</span>
                </a>
               </div> 
               <i  class="caret no-visible"></i>
            </span>
            <ul  class="dropdown-menu"  style="padding: 5px 0">
             <li style="display: flex;padding:7px 5px">
               <div class="${configuration.icon_sender_address} icon" style="padding:4px 7px 0 3px"></div>
               <div class="tag label label long-and-truncated" style="max-width:123px; padding-top:5px;" title="{{dataItem.sender.address}}">{{dataItem.sender.address}}</div>
               <div class="btn btn-link btn-xs " ng-if="restrictedEntityDisplayTypeName !== 'mail'" title="filter by this sender address" ng-click="filterByAddress(1, dataItem.sender.address)"><span class="fa fa-filter "></span></div>
              </li>
            </ul>
         </div>
        </div> 
        <div ng-if="dataItem.recipientsAddresses.length>0" class="to long-and-truncated">
         <div class="title">
             <span class="${configuration.icon_recipient_address} icon"></span>
             <span>To:</span> 
         </div>
         <div class="dropdown-wrapper dropdown" style="width: calc(100% - 70px);">
            <div class="btn-dropdown dropdown-toggle" data-toggle="dropdown" style="display:flex; cursor: pointer;">
              <div class="long-and-truncated">
               <span ng-repeat="r in dataItem.recipientsAddresses track by $index">
                  <span title="{{r}}">{{r}}</span>
                  <span ng-if="!$last">, </span>
               </span> 
              </div>
              <div ng-if="dataItem.recipientsAddresses.length > 1" class="recipient-count">[{{dataItem.recipientsAddresses.length}}]</div>
              <a  style="color:gray"><i class="caret no-visible"></i></a>          
              <div style="flex:1; cursor: default" ng-click="onSpaceClick($event)"></div>
            </div>
            <ul  class="dropdown-menu"  style="padding: 5px 0">
             <li  ng-repeat="address in dataItem.recipientsAddresses">
                <div style="display: flex;padding:7px 5px">
                   <div class="${configuration.icon_recipient_address}" style="padding:4px 7px 0 3px"></div>
                   <div class="tag label label long-and-truncated" style="max-width:123px; padding-top:5px;" title="{{address}}">{{address}}</div>
                   <div class="btn btn-link btn-xs " ng-if="restrictedEntityDisplayTypeName !== 'mail'" title="filter by this recipient address" ng-click="filterByAddress(2, address)"><span class="fa fa-filter "></span>
                </div>
              </li>
            </ul>
         </div>
       </div>
      </div>`;

    $scope.onSpaceClick = function($event){
      $event.preventDefault();
      $event.stopPropagation();
      $scope.selectItemIfNeeded($event);
    };

    $scope.selectItemIfNeeded = function (e, p) {
      var target = e.toElement || e.relatedTarget || e.target; //support Chrome/FF/IE
      var theClickedRow = $(target).closest('tr');
      if (e.ctrlKey || e.shiftKey || !theClickedRow.hasClass('k-state-selected')) {
        theClickedRow.click();
      }
    };

    $scope.filterByAddress = function(type, address:string) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: address,
        entityDisplayType: type === 1 ? EntityDisplayTypes.email_sender_address :EntityDisplayTypes.email_recipient_address,
        filterName:address
      });
    };

    $scope.refreshScreenData = function() //in order to refresh file count after dettache/attach to group
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDataChangeUserAction, {
        action: 'refreshSplitView',

      });
    };
    $scope.restoreOriginalGroupToFile = function(item:Entities.DTOFileInfo)
    {
      var dlg:any = dialogs.confirm('Confirm restore group', 'Are you sure you want to restore this file to it\'s original analysis group?', null);
      dlg.result.then(function(){
        (<any>item).processingRestoreGroup = true;
        groupsResource.findGroupAndAttachToFile(item.id,function(group:Entities.DTOGroupDetails){
          $scope.refreshScreenData();
        },onError)
      },function(btn){
        // $scope.confirmed = 'You confirmed "No."';
      });
    }
    $scope.getFileStateIcon = function(dataItem:any)
    {
      if( dataItem.isWorkflow ) {
        if(dataItem.workflowFileData.fileState == Entities.EFileState.APPROVED){
          return 'fa fa-check'
        }
        else if(dataItem.workflowFileData.fileState == Entities.EFileState.DISAPPROVED){
          return 'fa fa-exclamation-triangle notice'
        }
        else if(dataItem.workflowFileData.fileState == Entities.EFileState.ACTION_PENDING_APPROVAL){
          return ''
        }
      }
      return '';
    }

    $scope.detachFileFromGroup = function(item:Entities.DTOFileInfo) {

      var dlg:any = dialogs.confirm('Confirm detach', 'Are you sure you want to detach this file from it\'s group?', null);
      dlg.result.then(function(){
        groupsResource.detachFileFromGroup(item.id,function(group:Entities.DTOGroupDetails){
          $scope.refreshScreenData();
        },onError)
      },function(btn){
        // $scope.confirmed = 'You confirmed "No."';
      });
    };
    var groupId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.groupId),
      title: 'Group id',
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: false
    };

    var fsFileSize:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fsFileSize),
      title:'Size',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      width:'60px',
      ellipsis:true,
      template: "{{ dataItem."+ propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fsFileSize)+" | bytes }}"
    };
    var fsLastModified:ui.IFieldDisplayProperties =  <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fsLastModified),
      title:'Modified',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      width:'90px',
      parse: function(ts) {return $filter('date')(ts,configuration.dateTimeFormat)}

    };

    var fsLastModifiedTemplate= "<span title='Last modified date'> {{ dataItem.lastModifiedForDisplay }}</span>";

    var creationDate:ui.IFieldDisplayProperties =  <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.creationDate),
      title:'Created',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      parse: function(ts) {return $filter('date')(ts,configuration.dateTimeFormat)}
    };
    var fsLastAccess:ui.IFieldDisplayProperties =  <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.fsLastAccess),
      title:'Accessed',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      parse: function(ts) {return $filter('date')(ts,configuration.dateTimeFormat)}
    };
    var bizListExtrations:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'bizListExtractionSummaryAsFormattedString',
      title: 'Clients association',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };
    splitViewBuilderHelper.setExtractedBizListItemsTemplate($scope,bizListExtrations,eventCommunicator,false,true);

    var bizListAssociation:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'associatedBizListAsFormattedString', //for excel export
      title: 'Clients association',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };
    splitViewBuilderHelper.setAssociatedBizListItemsTemplate($scope,bizListAssociation,eventCommunicator,false,true);


    var tags:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'tagsAsFormattedString', //for excel export
      title: 'Tags',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };

    var docTypes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'docTypesAsFormattedString', //for excel export
      title: 'DocTypes',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true,

    };

    var department:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'departmentAsFormattedString', //for excel export
      title: 'Department',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,
      ddlInside:true
    };

    var functionalRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'functionalRolesAsFormattedString', //for excel export
      title: 'Data roles',
      type: EFieldTypes.type_other,
      displayed: true,
      editable: false,
      nullable: true,

    };

    var tagNewTemplate={};
    splitViewBuilderHelper.setTagsTemplate2($scope,tagNewTemplate,eventCommunicator,false,true);
    var tagsTemplate = (<any>tagNewTemplate).template;

    var patternNewTemplate={};
    splitViewBuilderHelper.setSearchPatternsTemplate2($scope,patternNewTemplate,eventCommunicator,false,true);
    var searchPatternsTemplate = (<any>patternNewTemplate).template;

    var labelNewTemplate={};
    splitViewBuilderHelper.setLabelsTemplate2($scope,labelNewTemplate,eventCommunicator,false,true);
    var labelTemplate = (<any>labelNewTemplate).template;

    var dNewTemplate={};
    splitViewBuilderHelper.setDocTypeTemplate2($scope,dNewTemplate,eventCommunicator,false,true,true,false);
    var docTypesTemplate = (<any>dNewTemplate).template;

    var depNewTemplate={};
    splitViewBuilderHelper.setDepartmentTemplate($scope,depNewTemplate,eventCommunicator,null,true,true,false);
    var departmentTemplate = (<any>depNewTemplate).template;

    var funcRoleNewTemplate={};
    splitViewBuilderHelper.setFunctionalRolesTemplate($scope,funcRoleNewTemplate, eventCommunicator,false,true);
    var functionalRolesTemplate = (<any>funcRoleNewTemplate).template;

    var bizListAssociationTemplate =bizListAssociation.template;
    var bizListExtrationsTemplate =bizListExtrations.template;

    var duplicationsCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixContentMetadataDtoFieldName+propertiesUtils.propName(dtoContentMetadata, dtoContentMetadata.fileCount),
      title: 'Copies #',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      template: '<span ng-if="dataItem.hasCopies">#= '+ prefixContentMetadataDtoFieldName+propertiesUtils.propName(dtoContentMetadata, dtoContentMetadata.fileCount)+' #</span>'

    };

    var duplicationsCountNewTemplate =  '<span ng-if="dataItem.hasCopies || dataItem.numOfAttachments > 0" class="title-third">&nbsp;&nbsp;(<span ng-if="dataItem.hasCopies">#= '+ prefixContentMetadataDtoFieldName+propertiesUtils.propName(dtoContentMetadata, dtoContentMetadata.fileCount)+' # copies</span><span ng-if="dataItem.numOfAttachments > 0"><span ng-if="dataItem.hasCopies">,&nbsp;</span>{{dataItem.numOfAttachments}} attachments</span>)</span>';

    var owner:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.owner),
      title: 'Owner',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false
    };
    var fileType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOFileInfo, dTOFileInfo.type),
      title: 'Type',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      width:'12px',
      template:"<div class='fileItem'><span title=' #: type #' class='fileType {{getFileTypeClassName(dataItem.type)}}'></span></div>"
    };
    var fileTypeRowTemplate=`<div style="position: relative;">
          <div file-type-html type=' #: type #' ></div>
          <div ng-if='dataItem.isAttachment' class="paper-clip-icon" style="position: absolute; top: 1px; left: -1px"></div>
        </div>`;

    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(fileName);
    if($scope.dupData=='true') {
      fields.push(baseName);
      fields.push(owner);
    }
    else {
      fields.push(fileType);
      fields.push(duplicationsCount);

      fields.push(bizListExtrations);
      fields.push(bizListAssociation);
      fields.push(docTypes);
      fields.push(department);
      fields.push(functionalRoles);
      fields.push(tags);
      fields.push(groupName);
      fields.push(fsFileSize);
      fields.push(owner);
      fields.push(fsLastModified);
      fields.push(creationDate);
      fields.push(fsLastAccess);
    }



    if($scope.dupData=='true') {
      //  fields.push(creationDate);
    }
    fields.push(groupId);

    var workflowTemplate = splitViewBuilderHelper.getFileWorklowDataTemplate();
    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate=`<tr  data-uid='#: uid #' colspan="${fields.length}">
       <td colspan='1' class='icon-column'>${fileTypeRowTemplate}</td>         
        <td colspan='${(visibleFileds.length-2)}' width='100%' class='ddl-cell break-text' >
          <span class='pull-right' style='max-width: 46%;'>
          <span class='pull-right'> ${functionalRolesTemplate}&nbsp;&nbsp;  ${bizListAssociationTemplate} &nbsp;${bizListExtrationsTemplate}&nbsp; ${labelTemplate}&nbsp; ${searchPatternsTemplate} &nbsp; ${tagsTemplate}</span>
          <div style='clear:right'></div>
           <span style='float: right'><div class="flex-container-row">${docTypesTemplate}&nbsp;${departmentTemplate}</div></span>
          </span>
          ${fileNameNewTemplate+ duplicationsCountNewTemplate +viewContentTemplate}
          <div>${groupNameNewTemplate}</div>
          ${mailInfoTemplate}
        </td>
                    
       <td colspan='1' ng-if='dataItem.isWorkflow' style='width:92px' >${workflowTemplate}</td>
       <td colspan='1' style='font-size: 12px;width:82px'>
          <div class="title-third">${fsLastModifiedTemplate}</div>  
          <div style='line-height: 20px;'>
             <div class='title-third'>${fsFileSize.template}</small>
          </div>
        </td>
        </tr> `;

    var dupDataRowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column'>"+fileTypeRowTemplate+"</td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell' >" +
      "<span class='pull-right' style='max-width: 46%;'>" +
      "<span class='pull-right'>"+functionalRolesTemplate+"&nbsp;&nbsp;</span>"+
      "<div style='clear:right'></div>"+
      " <span style='float: right'>" +docTypesTemplate+"</span>" +
      " <span style='float: right'>" +departmentTemplate+"</span>" +
      "</span>" +
      fileNameNewTemplate+  "<div>"+groupNameNewTemplate+"</div></td>" +

      "<td colspan='1' style='font-size: 12px;width:82px'>"+'<span class="title-third">'+ fsLastModifiedTemplate+'</span>    '+
      "</td>"+
      //"<td colspan='1' style='font-size: 12px;width:80px;' >"+fsLastModified.template+"</td>"+
      "</tr>";
    //var rowTemplate;
    //if($('#rowTemplate') && $('#rowTemplate').html()){
    //  rowTemplate = kendo.template($('#rowTemplate').html());
    //}

    var setFilterByGroup = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:EntityDisplayTypes.groups,
        filterName:gName
      });
    };
    $scope.setFilterByMail = function(item:Entities.DTOFileInfo)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doMailFilter',
        id: item.id,
        filterName:item.baseName
      });
    };
    $scope.setFilterByAttachment = function(item:Entities.DTOFileInfo)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doMailFilter',
        id: item.mailContainerId,
        filterName:'email of '+item.baseName
      });
    };
    $scope.setFilterByContentId = function(item:Entities.DTOFileInfo)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFileDuplicationsFilter',
        id: item.contentId,
        filterName:item.fileName
      });
    };


    $scope.addGroupToFiles = function()
    {

      if ($scope.selectedItems && $scope.selectedItems.length > 0) {

        var ungroupedSelectedItems = (<Entities.DTOFileInfo[]>$scope.selectedItems).filter(f=>f.groupId == null).map(s=> {
          return {id: s.id, name: s.baseName}
        });
        if (ungroupedSelectedItems.length == 0) {
          dialogs.notify('Add Un-Grouped files to group', 'File list does not contain any Un-Grouped files to assign to the new group.');
          return;
        }
        openAddToGroupDialog( ungroupedSelectedItems);

      }
    }

    var openAddToGroupDialog = function(ungroupedSelectedItems) {

      var dlg = dialogs.create(
        'common/directives/reports/dialogs/addFilesToGroupDialog.tpl.html',
        'addFilesToGroupDialogCtrl',
        {
          title: 'Add Un-Grouped files to group',
          filesLeftOutNumber: $scope.selectedItems.length - ungroupedSelectedItems.length,
          filesList: ungroupedSelectedItems
        },
        {size: 'md'});

      dlg.result.then(function (data) {
        var groupId = data.selectedGroupId;
        var files = data.checkedFileItems;
        var newGroupName = data.newGroupName;
        var fileIds:number[] = files.map(c=> (<any>c).id);
        groupsResource.attachFilesToGroup(fileIds, groupId, newGroupName, function (group:Entities.DTOGroupDetails) {
          markGroup(groupId,group.groupDto);
          $scope.refreshScreenData();
        }, onError);
      }, function (btn) {
        // $scope.confirmed = 'You confirmed "No."';
      });
    }

    var markGroup = function(prevGroupId:string,dataItem:Entities.DTOGroup)
    {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
        action: 'markItem',
        removeIds:[prevGroupId],
        id: dataItem.id,
        name: dataItem.groupName,
        entityDisplayType:EntityDisplayTypes.groups,
      });
    };
    var parseData = function (files:Entities.DTOFileInfo[]) {

      if(files) {
        files.forEach(function (file:Entities.DTOFileInfo) {
          if(file.itemSubject || file.itemSubject=='') {
            file.baseName = _.isEmpty(file.itemSubject) ? '(no subject)' : file.itemSubject;
          }
          if (!file.daLabelDtos ) {
            file.daLabelDtos = [];
          }

          !file.contentMetadataDto?file.contentMetadataDto=new Entities.DtoContentMetadata():null;
          file.groupName==''?file.groupName="<name to be set> ("+file.groupId.substr(file.groupId.length-6,5)+")":null;
          (<any>file).isFileExcluded = file.analyzeHint==Entities.EAnalyzeHint.EXCLUDED;
          (<any>file).isFileManuallyGrouped = file.analyzeHint==Entities.EAnalyzeHint.MANUAL;
          (<any>file).fileHasNetWorkLink = startsWith((file.fileName.toLowerCase()),'http');
          let clientExtractions = file.bizListExtractionSummary && file.bizListExtractionSummary[0]
          && file.bizListExtractionSummary.filter(x=>x.bizListItemType==Entities.DTOExtractionSummary.itemType_CLIENTS);
          (<any>file).haveExtractions = clientExtractions && clientExtractions[0] && clientExtractions[0].sampleExtractions.filter(e=> !e.aggregated)[0]!=null;
          (<any>file).havePatterns =  file.searchPatternsCounting && (file.searchPatternsCounting.length > 0 );

          (<any>file).haveContentFilter =  (<ui.IDisplayElementFilterData>$scope.filterData)&&(<ui.IDisplayElementFilterData>$scope.filterData).text &&
            (<ui.IDisplayElementFilterData>$scope.filterData).text.length>0 &&  (<ui.IDisplayElementFilterData>$scope.filterData).text.filter(f=>f.key == 'Content')[0];

          //NOTICE!!: all authorization operations must be done before parseFunctionalRoles, since if user is not allowed, the funcRole list is eareased
          (<any>file).openFileCheckRequired = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewPermittedContent],file.associatedFunctionalRoles.map(f=>f.id))
            && !currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent],file.associatedFunctionalRoles.map(f=>f.id))
            && currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewFiles],file.associatedFunctionalRoles.map(f=>f.id));
          (<any>file).openFileAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewContent,Users.ESystemRoleName.ViewPermittedContent],file.associatedFunctionalRoles.map(f=>f.id));
          (<any>file).mngGroupAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngGroups],file.associatedFunctionalRoles.map(f=>f.id));

          splitViewBuilderHelper.parseTags(<Entities.ITaggable>file);
          splitViewBuilderHelper.parseDepartment(<Entities.IDepartmentAssignee>file,configuration);
          splitViewBuilderHelper.parseDocTypes(<Entities.IDocTypesAssignee>file,configuration);
          splitViewBuilderHelper.parseAssociatedBizListItems(<Entities.IBizListAssociatable>file);
          splitViewBuilderHelper.parseFunctionalRoles(<Entities.IFunctionalRolesAssignee>file);
          if(bizListsDic) {
            splitViewBuilderHelper.parseExtractedBizListItems(<Entities.IBizListExtractable>file, bizListsDic);
          }
          (<any>file).isMail = file.type === Dashboards.EFileType.mail;
          (<any>file).hasCopies = file.contentMetadataDto && file.contentMetadataDto.fileCount >1?true:false;
          (<any>file).isWorkflow = (<Entities.DTOWorkflowFileInfo>file).workflowFileData !=null;
          (<any>file).numOfAttachments = file.numOfAttachments || 0;
          (<any>file).lastModifiedForDisplay = file.fsLastModified ? $filter('date')(file.fsLastModified,configuration.dateFormat) : '';
          (<any>file).isAttachment = _.has(file, 'itemType') && file.itemType === Dashboards.EFileItemType.attachment;
          (<any>file).recipientsAddresses = _.uniq(_.map(file.recipients, (r) => {
            return r.address;
          }));
          (<any>file).showMailSenderOrRecipients = !_.isEmpty((<any>file).recipientsAddresses) || !_.isEmpty(file.sender && !_.isEmpty(file.sender.address));
        });
        return files;
      }
      loadBizListList();
      return null;
    };


    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[baseName.fieldName],$scope.selectedItem[id.fieldName]);
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    var onError = function() {
      dialogs.error('Error','Sorry, an error occurred.');
    }
  })
  .directive('filesGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class="fill-height {{elementName}}"  k-on-change="handleSelectionChange(data, dataItem, columns)" kendo-grid ng-transclude k-on-data-bound="onDataBound()"' +
      ' k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          totalElements: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          collapsedData: '=',
          dupData: '@',
          fixHeight: '@',
          initialLoadParams: '=',
        },
      controller: 'FilesCtrl',
      link: function (scope:any, element, attrs:any) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('files.all',[

])
  .controller('AllFilesCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                       propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('AllFilesCtrl');

    var _this = this;
    $scope.elementName='all-files-grid';
    $scope.entityDisplayType = EntityDisplayTypes.all_files;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOFileInfo>($scope,log,configuration,eventCommunicator,userSettings,getDataUrlParams,fields,parseData,rowTemplate,$stateParams,null,
        getDataUrlBuilder, true );

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

    };

    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
      return configuration.files_count_url;
    }

    var dTOFileInfo:Entities.DTOFileInfo =  Entities.DTOFileInfo.Empty();


    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'name',
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      isPrimaryKey:true,
      editable: false,
      nullable: true,

    };
    var nameNewTemplate = '<span class="title-primary">#: name #</span>';


    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'numOfFiles',
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };




    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(name);
    fields.push(numOfFiles);

    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(null,'numOfFiles',null);

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column'><span class='"+configuration.icon_aclRead+"'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
      nameNewTemplate+"<div></div></td>" +
      //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

      '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
      "</tr>";

    var parseData = function (files:number) {
      if(files) {

          let file = {};
          (<any>file).name = 'Total files';
          (<any>file).count = files;
          (<any>file).numOfFiles = files;
          (<any>file).totalElements = 1;
          (<any>file).pageNumber = 1;
          (<any>file).pageSize = 30;
          (<any>file).numberOfElements = 1;

        return file;

      }
      return null;
    }
    var getDataUrlParams = function()
    {
      return null;
    };

    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[name.fieldName],$scope.selectedItem[name.fieldName]);
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('allFilesGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          initialLoadParams: '=',
        },
      controller: 'AllFilesCtrl',
      link: function (scope:any, element, attrs) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  });

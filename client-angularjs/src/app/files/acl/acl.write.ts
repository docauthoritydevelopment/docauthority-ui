///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('files.acl.write',[

])
  .controller('AclWriteCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                       propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log: ILog = Logger.getInstance('AclWriteCtrl');

    var _this = this;
   $scope.elementName='acl-write-grid';
    $scope.entityDisplayType = EntityDisplayTypes.acl_writes;

    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      _this.gridOptions  = GridBehaviorHelper.createGrid<Entities.DTOGroup>($scope,log,configuration,eventCommunicator,userSettings,getDataUrlParams,fields,parseData,rowTemplate,$stateParams,null,null,true );

     GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);
  //    $scope.mainGridOptions = _this.gridOptions.gridSettings;
    };

    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTOFileUser> =  Entities.DTOAggregationCountItem.Empty();
    var dTOFileUser:Entities.DTOFileUser =  Entities.DTOFileUser.Empty();
    var prefixFieldName= propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)+'.';


    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dTOFileUser, dTOFileUser.user),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      isPrimaryKey:true,
      editable: false,
      nullable: true,

    };
    var nameNewTemplate = '<span class="title-primary">#: '+ prefixFieldName+propertiesUtils.propName(dTOFileUser, dTOFileUser.user)+' #</span>';
    var nameNewTemplate =    '<span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="AclWrite: #: '+ name.fieldName+' #">#: '+ name.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this aclWrite</a></li>"+
        '  </ul></span>';
    $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTOFileUser>) {

      setFilterByEntity(item.item.user,item.item.user);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(name);
    fields.push(numOfFiles);


    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
        "<td colspan='1' class='icon-column'><span class='"+configuration.icon_aclWrite+"'></span></td>"+
        "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell break-text'>" +
        nameNewTemplate+"<div></div></td>" +
          //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

        '<td colspan="1" style="width:220px;" class="ellipsis">'+filterTemplate+'</td>'+
        "</tr>";


    var parseData = function (mails:Entities.DTOAggregationCountItem< Entities.DTOGroup>[]) {
      if(mails) {
        mails.map(function (listItem:Entities.DTOAggregationCountItem< Entities.DTOGroup>) {
          (<any>listItem).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(listItem.count , listItem.count2 );
        });
        return mails;
      }
      return null;
    };

    var getDataUrlParams = function() {
      return null;
    };

    var onSelectedItemChanged = function() {
      $scope.itemSelected($scope.selectedItem[name.fieldName],$scope.selectedItem[name.fieldName]);
    };
    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('aclWritesGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
      {

        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        itemsSelected: '=',
        unselectAll: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        totalElements: '=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
        initialLoadParams: '=',
      },
      controller: 'AclWriteCtrl',
      link: function (scope:any, element, attrs) {
        scope.Init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

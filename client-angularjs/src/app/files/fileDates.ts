///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('files.dates',[
  'directives.fileDates'
])
  .controller('FileDatesCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,
                                         propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
    var log: ILog = Logger.getInstance('FileDatesCtrl');

    var _this = this;
    $scope.elementName='file-dates-grid';
    var gridOptions:ui.IGridHelper;


    $scope.Init = function(restrictedEntityDisplayTypeName)
    {
      $scope.iconClass= getIconClass();
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
      $scope.lazyFirstLoad =true;
      gridOptions  = GridBehaviorHelper.createGrid<Entities.DTODateRangeItem>($scope,log,configuration,eventCommunicator,userSettings,getDataUrlParams,fields,parseData,
        rowTemplate,$stateParams,null,getDataUrlBuilder, true);

      GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element);

    };
    var getIconClass=function()
    {
      if( $scope.entityDisplayType == EntityDisplayTypes.file_modified_dates)
      {
        return configuration.icon_fileModifiedDate;
      }
      return configuration.icon_fileCreatedDate;
    }
    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,fullyContained,collapsedData) {
      if($scope.entityTypeId) {
        var url:string = GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName, restrictedEntityId, entityDisplayType, configuration, includeSubEntityData, fullyContained, collapsedData);
        return url.replace(new RegExp('{partitionId}', 'g'),$scope.entityTypeId);
      }
      return null;
    }



    var dTOAggregationCountItem:Entities.DTOAggregationCountItem<Entities.DTODateRangePartition> =  Entities.DTOAggregationCountItem.Empty();
    var dtoDateRangeItem:Entities.DTODateRangeItem =  Entities.DTODateRangeItem.Empty();
    var dtoDateRangePoint:Entities.DTODateRangePoint =  Entities.DTODateRangePoint.Empty();
    var prefixFieldName= propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item)+'.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties> {
      fieldName: prefixFieldName + propertiesUtils.propName(dtoDateRangeItem, dtoDateRangeItem.id),
      title: 'ID',
      type: EFieldTypes.type_string,
      isPrimaryKey: true,
      displayed: false,
      editable: false,
      nullable: true,

    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dtoDateRangeItem, dtoDateRangeItem.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
    var nameNewTemplate =    '<span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
      '<a  class="title-primary" title="Date: #: '+name.fieldName+' #">#: '+name.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this date</a></li>"+
      '  </ul></span>';
    $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Entities.DTODateRangeItem>) {

      setFilterByEntity(item.item.id,item.item.name);
    };
    var setFilterByEntity = function(gId,gName)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doFilterByID',
        id: gId,
        entityDisplayType:$scope.entityDisplayType,
        filterName:gName
      });
    };
    var startTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dtoDateRangeItem, dtoDateRangeItem.start)+'.'+propertiesUtils.propName(dtoDateRangePoint, dtoDateRangePoint.absoluteTime),
      title: 'Start date',
      type: EFieldTypes.type_date,
      displayed: true,
      editable: false,
      nullable: true,
      width:'90px',
      format:'{0:'+configuration.dateFormat+'}',

    };

    var endTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFieldName+propertiesUtils.propName(dtoDateRangeItem, dtoDateRangeItem.end)+'.'+propertiesUtils.propName(dtoDateRangePoint, dtoDateRangePoint.absoluteTime),
      title: 'End date',
      type: EFieldTypes.type_date,
      displayed: true,
      editable: false,
      nullable: true,
      width:'90px',
      format:'{0:'+configuration.dateFormat+'}',

    };
    var startTimeNewTemplate = '<span class="title-third"><span ng-if="!dataItem.isUnavailable">{{ dataItem.'+ startTime.fieldName+' |date:'+configuration.dateFormat+' }} to {{ dataItem.'+ endTime.fieldName+' |date:'+configuration.dateFormat+' }}</span></span>';
    var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2),
      title: '# files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };

    var numOfFilteredFiles :ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
      title: '# filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template: '<span class="filter-data" > #: '+ propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count)+'# </span>'
    };



    var inFolderPercentage:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'folderPercentage',
      title: '% filtered files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,

    };

    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(startTime);
    fields.push(numOfFilteredFiles);
    fields.push(numOfFiles);


    var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(numOfFiles.fieldName, numOfFilteredFiles.fieldName,'filterPercentage');

    var visibleFileds = fields.filter(f=>f.displayed);
    var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +
      "<td colspan='1' class='icon-column' style='padding-left: 6px;'><span class='{{iconClass}}'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>" +
      nameNewTemplate+"<div>"+startTimeNewTemplate+"</div></td>" +
      //"<td colspan='1' style='font-size: 12px;width:160px;' >"+inFolderPercentage.template+numOfFilteredFiles.template+numOfFiles.template+"</td>"+

      '<td colspan="1" style="width:220px" class="ellipsis">'+filterTemplate+'</td>'+
      "</tr>";



    var getDataUrlParams = function()
    {
      return null;
    };

    var parseData = function (dateRanges:Entities.DTOAggregationCountItem<Entities.DTODateRangeItem>[]) {

      if(dateRanges)
      {
        _.forEach(dateRanges, (d, index) => {
          (<any>d).item.start.absoluteTime =new Date( d.item.start.absoluteTime);
          (<any>d).isUnavailable = (<any>d).item.start.type === Entities.EDateRangePointType.UNAVAILABLE;

          if(!d.item.end) {
            d.item.end = Entities.DTODateRangePoint.NowAbsolute();
          }
          (<any>d).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(d.count , d.count2 );


        });

        dateRanges = _.filter(dateRanges, (d) => {
          let isHidden = d.item.start.type === Entities.EDateRangePointType.UNAVAILABLE && d.count == 0;
          return !isHidden;
        });
      }
      return dateRanges;
    }
    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName]);
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('fileCreatedDatesGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          entityTypeId: '=',
          restrictedEntityTypeId: '=',
          initialLoadParams: '=',
        },
      controller: 'FileDatesCtrl',
      link: function (scope:any, element, attrs) {
        scope.entityDisplayType = EntityDisplayTypes.file_created_dates;

        scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  })
  .directive('fileModifiedDatesGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude:true,
      scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          entityTypeId: '=',
          restrictedEntityTypeId: '=',
          initialLoadParams: '=',
        },
      controller: 'FileDatesCtrl',
      link: function (scope:any, element, attrs) {
        scope.entityDisplayType = EntityDisplayTypes.file_modified_dates;

        scope.Init(attrs.restrictedentitydisplaytypename);
      }

    }

  })
  .directive('fileAccessedDatesGrid', function () {
    return {
      restrict: 'EA',
      template: '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)" k-on-change="handleSelectionChange(data, dataItem, columns)" ' +
      'k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
      replace: true,
      transclude: true,
      scope: {
        lazyFirstLoad: '=',
        restrictedEntityId: '=',
        itemSelected: '=',
        itemsSelected: '=',
        unselectAll: '=',
        filterData: '=',
        sortData: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid: '=',
        totalElements: '=',
        refreshData: '=',
        pageChanged: '=',
        changePage: '=',
        processExportFinished: '=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        findId: '=',
        entityTypeId: '=',
        restrictedEntityTypeId: '=',
        initialLoadParams: '=',
      },
      controller: 'FileDatesCtrl',
      link: function (scope:any, element, attrs) {
        scope.entityDisplayType = EntityDisplayTypes.file_accessed_dates;
        scope.Init(attrs.restrictedentitydisplaytypename);
      }
    };
  });


'use strict';

describe('Controller: GroupfilesCtrl', function () {

  // load the controller's module
  beforeEach(module('daApp'));

  var GroupfilesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GroupfilesCtrl = $controller('GroupfilesCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});

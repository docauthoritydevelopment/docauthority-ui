///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.functionalRoles.grid',[

    ])
    .controller('functionalRolesGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                           propertiesUtils,userBizRolesResource:IUserBizRolesResource, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('functionalRolesGridCtrl');


      $scope.elementName = 'functional-role-grid';
      var gridOptions:ui.IGridHelper;
      $scope.entityDisplayType = EntityDisplayTypes.functional_roles;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;
        buildGrid();
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };


      var buildGrid = function(){
          gridOptions  = GridBehaviorHelper.createGrid<Users.DtoFunctionalRoleSummaryInfo>($scope, log,configuration, eventCommunicator,userSettings,  null,
              fields, parseData,  rowTemplate,
              $stateParams, null,getDataUrlBuilder, true);

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);
      }
      var setUserRoles = function()
      {
        $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,fullyContained,collapsedData) {
           return GridBehaviorHelper.getDataUrlBuilder(restrictedEntityDisplayTypeName, restrictedEntityId, entityDisplayType, configuration, includeSubEntityData,fullyContained,collapsedData);
      };

      var getDataUrlParams = function () {
        return null;
      };

      var dTOAggregationCountItem:Entities.DTOAggregationCountItem< Users.DtoFunctionalRole> = Entities.DTOAggregationCountItem.Empty();
      var dtoFunctionalRole:Users.DtoFunctionalRole =  Users.DtoFunctionalRole.Empty();
      var prefixFieldName = propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.item) + '.';



      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true,
        width:'35px'
      };

      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:'<span title="{{dataItem.'+ prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.name)+'}}" >#= '+prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.name)+' #</span></span>'
      };

      var nameNewTemplate =    '<span class="dropdown-wrapper dropdown" ><span class="btn-dropdown dropdown-toggle " data-toggle="dropdown"  >' +
        '<a  class="title-primary" title="Functional role: #: '+ name.fieldName+' #">#: '+ name.fieldName+' # <i class="caret ng-hide"></i></a></span>'+
        '<ul class="dropdown-menu"  > '+
        "<li><a  ng-click='groupNameClicked(dataItem)'><span class='fa fa-filter'></span>  Filter by this Data Role</a></li>"+
        '  </ul></span>';

      $scope.groupNameClicked = function(item:Entities.DTOAggregationCountItem<Users.DtoFunctionalRole>) {

        setFilterByEntity(item.item.id,item.item.displayName);
      };
      var setFilterByEntity = function(gId,gName)
      {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doFilterByID',
          id: gId,
          entityDisplayType:$scope.entityDisplayType,
          filterName:gName
        });
      };

      var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.description),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        template:'<span > #: '+prefixFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.description)+'# </span>'
      };

      var numOfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),
        title: '# files',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false
      };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(description);
      fields.push(numOfFiles);


      $scope.fields =fields;
      var filterTemplate= splitViewBuilderHelper.getFilesAndFilterTemplateFlat(propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count2), propertiesUtils.propName(dTOAggregationCountItem, dTOAggregationCountItem.count),'filterPercentage');

        var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
          "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span title='{{dataItem."+name.fieldName+"|translate}}' class='"+configuration.icon_functionalRole+" user-#: "+name.fieldName+"#" + " tag-{{dataItem.styleId}}'></span></td>"+
          "<td colspan='"+(visibleFileds.length-2)+"' width='100%' class='ddl-cell'>"+
        nameNewTemplate+
        "<div class='title-third'>"+description.template+"</div></td>"  +
        '<td colspan="1" style="width:200px" class="ellipsis">'+filterTemplate+'</td>'+
          "</tr>";

      var parseData = function (functionalRoles: Entities.DTOAggregationCountItem< Users.DtoFunctionalRole>[]) {
        if(functionalRoles) {

          functionalRoles.forEach(f=>{
            (<any>f).styleId = splitViewBuilderHelper.getFunctionalRoleStyleId(f.item.id);
            f.item.description = f.item.description? f.item.description:'';
            (<any>f).filterPercentage = splitViewBuilderHelper.getFilesNumPercentage(f.count , f.count2);
          });
        }
        return functionalRoles;
      };

      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      };



     $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('functionalRolesGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          showAllFunctionalRoles: '=',
          initialLoadParams: '=',
          findId: '=',
          fixHeight: '@',

        },
        controller: 'functionalRolesGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

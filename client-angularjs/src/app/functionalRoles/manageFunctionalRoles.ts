var manageFunctionalRoles  = function(): ng.IDirective {
  return <ng.IDirective> {
    bindToController: {
      selectItem: '=',
      changeFuncRoleSelectedItem: '=',
      onSelectedItem: '=',
      excelFileName: '=',
    },
    scope:true,
    templateUrl: '/app/functionalRoles/manageFunctionalRoles.tpl.html',
    replace:true,
    controllerAs:'manage_fr',
    controller:ManageScheduleGroupsController
  }
}

angular.module('users.functionalRoles.manage', [
  'users.functionalRoles.crud.grid'
]).directive('manageFunctionalRoles', manageFunctionalRoles)


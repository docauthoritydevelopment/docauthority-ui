///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('users.functionalRoles.crud.grid',[

  ])
  .controller('functionalRolesCrudGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                   propertiesUtils,userBizRolesResource:IUserBizRolesResource, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('functionalRolesCrudGridCtrl');


    $scope.elementName = 'functional-role-crud-grid';
    var gridOptions:ui.IGridHelper;
    $scope.entityDisplayType = EntityDisplayTypes.functional_roles;

    $scope.init = function (restrictedEntityDisplayTypeName) {
      $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;
      buildGrid();
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
    };


    var buildGrid = function(){
      gridOptions  = GridBehaviorHelper.createGrid<Users.DtoFunctionalRoleSummaryInfo>($scope, log,configuration, eventCommunicator,userSettings,  null,
        fields, parseData,  rowTemplate,
        $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, rowTemplate, fields, onSelectedItemChanged,$element);
    }
    var setUserRoles = function()
    {
      $scope.isMngRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
    }

    var getDataUrlBuilder = function(restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData,fullyContained,collapsedData) {
       return configuration.functionalRole_list_url;

    };

    var getDataUrlParams = function () {
      return null;
    };

    var DdtoFunctionalRolesSummaryInfo =  Users.DtoFunctionalRoleSummaryInfo.Empty();
    var dtoFunctionalRole:Users.DtoFunctionalRole =  Users.DtoFunctionalRole.Empty();
    var dtoSystemRoleDto:Users.DtoSystemRole =  Users.DtoSystemRole.Empty();
    var dtoTemplateRole:Users.DtoTemplateRole =  Users.DtoTemplateRole.Empty();
    var prefixFunctionalRoleFieldName = propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.functionalRoleDto) + '.';
    var prefixFunctionalRoleTemplateFieldName = prefixFunctionalRoleFieldName + propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.template) + '.';

    var dtoLdapGroupMapping =  Users.DtoLdapGroup.Empty();
    var prefixLdapGroupMapping =propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos) + '.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      width:'35px'
    };

    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      template:'<a class="top-right-position btn btn-link btn-sm"><i  ng-click="openEditDialog(dataItem)" class="fa fa-pencil ng-hide" title="Edit list item"></i>' +
      '</a><span class="fa fa-user icon-column "></span><span class="title-primary"> #: '+prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.name)+'# </span>',
    };
    var nameTemplate =  '<span ng-if="isMngRolesUser" class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
      '<a class="title-primary ">' +
      '<span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span> ' +
      '<i class="caret ng-hide"></i></a></span>'+
      '<ul class="dropdown-menu"  > '+
      '<li><a   ng-click="openEditDialog(dataItem)"><span class="fa fa-pencil"></span> Edit</a></li>'+
        //'<li ><a   ng-click="openWhoIsAssignedDialog(dataItem)">Who is assigned?</a></li>'+

      ' </ul></span> ';
    nameTemplate=nameTemplate+'<span ng-if="!isMngRolesUser" class="title-primary "><span title="{{dataItem.'+ name.fieldName+'}}" >#= '+name.fieldName+' #</span></span>';

    var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.description),
      title: 'Description',
      type: EFieldTypes.type_string,
      displayed: true,
      width:'100px',
      editable: false,
      nullable: true,
      template:'<span > #: '+prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.description)+'# </span>'
    };
    var managerUsername:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.managerUsername),
      title: 'Manager username',
      type: EFieldTypes.type_string,
      displayed: true,
      width:'150px',
      editable: false,
      nullable: true,
      template: '#if ( ' + prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.managerUsername) + ') {#' +
      '<div class="title-third title-main">Manager:</div><span class="title-secondary" title="manager username">' + '#:' + prefixFunctionalRoleFieldName+propertiesUtils.propName(dtoFunctionalRole, dtoFunctionalRole.managerUsername) + '# </span>' +
      ' #}#',
    };
    var ldapGroupName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:'ldapGroupsAsFormattedString',
      title: 'LDAP group',
      type: EFieldTypes.type_string,
      displayed: true,
      width:'120px',
      editable: false,
      nullable: true,
      template: ' #if ( '+ propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos)+'&& '
      +propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos)+'[0])  {#' +
      '<div class="title-third title-main">' + 'Ldap group:' + '</div>' +
      '<div class="title-secondary">' +
      '	# for (var i=0; i < '+ propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos)+'.length; i++) { # ' +

      '<span><a ng-click="openEditDialog(dataItem,\'ldapGroup\')" title="Ldap group name #: '+propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos)+'[i].'+ propertiesUtils.propName(dtoLdapGroupMapping, dtoLdapGroupMapping.groupName)+' # "> #: '
      + propertiesUtils.propName(DdtoFunctionalRolesSummaryInfo, DdtoFunctionalRolesSummaryInfo.ldapGroupMappingDtos)+'[i].'+ propertiesUtils.propName(dtoLdapGroupMapping, dtoLdapGroupMapping.groupName)+' # </a></span>' +

      '		# } #' +
      ' </div>#}#'
    };

    var templateRoles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixFunctionalRoleTemplateFieldName + propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.displayName),
      title: 'Role template',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      width:'100px',
      template:
      '<div class="title-third title-main">&nbsp; &nbsp;Role template:</div>' +
      '<div class="title-secondary ellipsis">' +
      '<span class="grid-list-item">' +
      '<a ng-click="openEditDialog(dataItem,\'templateRoles\')" title="{{dataItem.systemRolesTooltip}}"> #: '+ prefixFunctionalRoleTemplateFieldName + propertiesUtils.propName(dtoTemplateRole, dtoTemplateRole.displayName)+ '# </a>' +
      '</span>' +
      '</div>'
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(description);
    fields.push(managerUsername);
    fields.push(ldapGroupName);
    fields.push(templateRoles);

    $scope.fields =fields;

    var visibleFileds = fields.filter(f=>f.displayed);

    var rowTemplate="<tr  data-uid='#: uid #' colspan='5'>" +
      "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span title='{{dataItem."+name.fieldName+"|translate}}' class='"+configuration.icon_functionalRole+" user-#: "+name.fieldName+"#" + " tag-{{dataItem.styleId}}'></span></td>"+
      "<td colspan='1'   width='180px' class='ddl-cell break-text'>"+  nameTemplate+ "<div class='title-third'>"+description.template+"</div></td>"  +
      "<td colspan='1'    width='180px' class='ddl-cell break-text'>"+  templateRoles.template + "</td>"  +

      "<td colspan='1'   width='140px' class='ddl-cell break-text'>"+  ldapGroupName.template + "</td>"  +
      "<td colspan='1'   width='40%' class='ddl-cell break-text'>"+  managerUsername.template + "</td>"  +
      "</tr>";

    var parseData = function (data: any) {
      var functionalRoles: Users.DtoFunctionalRole[] =data;

      _.each(functionalRoles, (functionalRole:Users.DtoFunctionalRoleSummaryInfo) => {
        functionalRole.functionalRoleDto.description = functionalRole.functionalRoleDto.description || '';
        functionalRole.functionalRoleDto.managerUsername = functionalRole.functionalRoleDto.managerUsername || '';

        let systemRolesTooltip = buildSystemRolesTooltip(functionalRole.functionalRoleDto);
        if (systemRolesTooltip) {
          (<any>functionalRole).systemRolesTooltip = systemRolesTooltip;
        }
        (<any>functionalRole).id = functionalRole.functionalRoleDto.id;
        (<any>functionalRole).styleId = splitViewBuilderHelper.getFunctionalRoleStyleId(functionalRole.functionalRoleDto.id);
        (<any>functionalRole).ldapGroupsAsFormattedString = functionalRole.ldapGroupMappingDtos.map(m=>m.groupName).join(";");

      });
      return functionalRoles;
    };

    var buildSystemRolesTooltip = function (bizRole:Users.ITemplateRoleContainer) {
      let systemRolesTooltip = '';

      if(bizRole.template.systemRoleDtos) {
        bizRole.template.systemRoleDtos.sort( propertiesUtils.sortAlphbeticFun('name'));
        bizRole.template.systemRoleDtos.forEach(r=> {
          systemRolesTooltip += `<div class="sys-roles-tooltip-item"><i class='fa fa-wrench icon'></i><span class='name'>${r.displayName}</span></div>`
        });
        return  `<div class="sys-roles-tooltip">${systemRolesTooltip}</div>`;
      }
      return null;
    };

    var onSelectedItemChanged = function()
    {
      $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
    };

    $scope.openEditDialog = function(dataItem:any,openWithFieldName?:string) {
      if($scope.isMngRolesUser) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridBizRolesUserAction, {
          action: 'openEditItemDialog',
          item: dataItem,
          openWithFieldName: openWithFieldName
        });
      }
    };
    $scope.openWhoIsAssignedDialog = function(dataItem:any) {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridBizRolesUserAction, {
        action: 'whoIsAssigned',
        item: dataItem,
      });
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('functionalRoleCrudGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      replace: true,
      transclude:true,
      scope:
      {
        totalElements:'=',
        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        unselectAll: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        refreshDataSilently: '=',
        findId: '=',
        fixHeight: '@',

      },
      controller: 'functionalRolesCrudGridCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

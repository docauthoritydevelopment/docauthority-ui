///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('operations.connections.ldap.grid',[

    ])
    .controller('ldapConnectionsGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('ldapConnectionsGridCtrl');


      $scope.elementName = 'ldap-connections';
      var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName = restrictedEntityDisplayTypeName;

        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoLdapConnectionDetails>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData,  rowTemplate,
            $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        var doNotSelectFirstRowByDefault = false;
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,
            doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

      };

      var setUserRoles = function()
      {
        $scope.isMngScanConfigUser =  currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
      }
      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
        return configuration.ldap_connections_url;
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dTOLdapConnection:Operations.DtoLdapConnectionDetails =  Operations.DtoLdapConnectionDetails.Empty();

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"NAME",
        nullable: true,
      };

      var nameDisplayTemplate =  '<span title="{{dataItem.'+ propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.name)+'}}" >'+
        '#= '+ propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.name)+' #</span>';
      var nameTemplate = '<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem)" class="title-primary">'+nameDisplayTemplate+ '</a> ';
      var disableNameTemplate = '<span ng-if="!isMngScanConfigUser"  class="title-primary">'+nameDisplayTemplate+ '</span> ';

      name.template=disableNameTemplate+nameTemplate;

      var host:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.host),
        title: 'host',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"HOST",
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.host)+' #'
      };
      var username:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.manageDN),
        title: 'username',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"MANAGE_DN",
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.manageDN)+' #'
      };

      var ldapType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'ldapType',
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"TYPE",
        nullable: true,
        template:'#: ldapType #'
      };

      var port:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.port),
        title: 'Port',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        exportTitle:"PORT",
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.port)+' #'
      };

     var isSSL:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.ssl),
        title: 'SSL',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
         exportTitle:"SSL",
        nullable: true,
        template: propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.ssl)
      };

      var enabled:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dTOLdapConnection, dTOLdapConnection.enabled),
        title: 'Active',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        width:'80px',

      };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(host);
      fields.push(enabled);
      fields.push(ldapType);
      fields.push(username);
      fields.push(port);
      fields.push(isSSL);

      $scope.fields =fields;



      var sslTemplate = splitViewBuilderHelper.getCountTemplate('sslStr','SSL:',null,null);
      var activeTemplate =splitViewBuilderHelper.getToggleButtonTemplate(enabled.fieldName,'Active',"toggleEnableState(dataItem)", undefined, "Ldap");
      var ldapTypeTemplate = splitViewBuilderHelper.getCountTemplate(ldapType.fieldName,'Type:',null,null);
      var portTemplate = splitViewBuilderHelper.getCountTemplate(port.fieldName,'Port',null,null);
      var usernameTemplate = splitViewBuilderHelper.getTitledTemplate(username.fieldName,'Username');

      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+fields.length+"'>" +
         " <td colspan='1' class='icon-column' ><da-icon href='ldapConnection'></da-icon></td>"+
          "<td colspan='"+(visibleFileds.length-6)+"'  style='width: 70%;max-width:440px;word-wrap:break-word' class='ddl-cell'>"+
              nameTemplate+"<div class='title-third'><span title='host'>{{dataItem."+host.fieldName+"}}</span></div>" +
          "</td>"  +
          "<td colspan='1' width='50px'>"+activeTemplate+"</td>"+

          "<td colspan='1' width='100px'>"+portTemplate+"</td>"+
          "<td colspan='1' width='150px'>"+ldapTypeTemplate+"</td>"+
           "<td colspan='1' width='80px'>"+sslTemplate+"</td>"+
           "<td colspan='1' width='34%'>"+usernameTemplate+"</td>"+
          "</tr>";
0
      var parseData = function (ldapConnections: Operations.DtoLdapConnectionDetails[]) {
        if(ldapConnections)
        {
          ldapConnections.forEach(con=> {
            (<any>con).ldapType = $filter('translate')(Operations.ELdapServerDialect1[(<Operations.DtoLdapConnectionDetails>con).serverDialect]);
            (<any>con).sslStr = con.ssl? 'Yes':'No';
          })
        }
        return ldapConnections;
      }


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridLdapAction, {
          action: 'openEditDialog',
          ldapConnection: dataItem,
        });
      };


      $scope.toggleEnableState = function(dataItem:Operations.DtoLdapConnectionDetails) {
         eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridLdapAction, {
            action: 'setEnable',
            connectionId: dataItem.id,
            newState: !dataItem['enabled']
          });
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
    })
    .directive('ldapConnectionsGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',

          fixHeight: '@',

        },
        controller: 'ldapConnectionsGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }
    });

///<reference path='../../../common/all.d.ts'/>

'use strict';


angular.module('directives.ldapConnection.manage.menu', [
      'resources.operations',
      'directives.dialogs.microsoft.new',

    ])
    .controller('ldapConnectionMenuCtrl', function ($scope,$element,Logger:ILogger,$q, userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,
                                                    ldapConnectionResource:ILdapConnectionResource) {
      var log:ILog = Logger.getInstance('ldapConnectionMenuCtrl');


      $scope.init = function () {

      };

      $scope.openNewItemDialog = function()
      {
        var dlg = dialogs.create(
          'common/directives/dialogs/ldapSettingsDialog.tpl.html',
          'ldapSettingsDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function (ldapConnection:Operations.DtoLdapConnectionDetails) {
          $scope.reloadData([ldapConnection.id]);
        }, function () {

        });

      }


      $scope.openEditItemsDialog = function(ldapConnection:Operations.DtoLdapConnectionDetails)
      {
        var item:Operations.DtoLdapConnectionDetails = ldapConnection?ldapConnection: $scope.selectedListItemsToOperate[0];

        var dlg = dialogs.create(
          'common/directives/dialogs/ldapSettingsDialog.tpl.html',
          'ldapSettingsDialogCtrl',
          {ldapConnection: item},
          {size: 'md'});

        dlg.result.then(function (ldapConnection:Operations.DtoLdapConnectionDetails) {
          editItem(ldapConnection);
        }, function () {

        });
      }

      $scope.openDeleteItemsDialog = function()
      {
        var items:Operations.DtoLdapConnectionDetails[]=$scope.selectedListItemsToOperate;
        if(items&& items.length>0) {
          var userMsg;
          if(items.length==1)
          {
            userMsg="You are about to delete LDAP connection named: '"+items[0].name+"'. Continue?";
          }
          else if(items.length<6){
            userMsg="You are about to delete " + items.length + " LDAP connections. Continue?"+'<br>'+items.map(u=>u.name).join(', ');
          }
          else {
            userMsg="You are about to delete " + items.length + " LDAP connection. Continue?";
          }
          var dlg = dialogs.confirm('Confirmation', userMsg, null);
          dlg.result.then(function (btn) { //user confirmed deletion
            items.forEach(d=>  deleteItem(d));

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }

      }

      var editItem = function (item:Operations.DtoLdapConnectionDetails) {
        if ( item.name &&  item.name.trim() != '') {
          log.debug('edit ldap connection: ' +  item.name);
          ldapConnectionResource.updateConnection( item, function () {
            $scope.reloadData(item.id,item.id);
          },function (error) {
            if (error.status == 400 &&error.data.type == ERequestErrorCode.ITEM_ALREADY_EXISTS) {
              dialogs.error('Error', 'Ldap connection with this name already exists. \n\nChoose another name.');
            } else {
              dialogs.error('Error', 'Failed to edit Ldap connection.');
            }
          });
        }
      };

      var deleteItem = function(item:Operations.DtoLdapConnectionDetails)
      {
        log.debug('delete DataCenter: ' + item.name);
        ldapConnectionResource.deleteConnection(item.id, function () {
          $scope.reloadData();
        },onError);
      }

      $scope.openUploadItemsDialog = function()
      {
        var dlg = dialogs.create(
          'common/directives/dialogs/uploadFileDialogByServer.tpl.html',
          'uploadFileDialogByServerCtrl',
          {uploadResourceFn: ldapConnectionResource.uploadConnectionsFromCsv,validateUploadResourceFn: ldapConnectionResource.validateUploadConnectionsFromCsv,itemsName:'ldap connections'},
          {size:'md'});
        dlg.result.then(function(fileUploaded:boolean){
          if(fileUploaded) {
            $scope.reloadData();
          }
        },function(){

        });
      };

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridLdapAction,
        $scope,
        (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
          /*
           * fireParam object:
           *   { initSearch: false, value: searchText, mode: mode}
           */
          if (fireParam.action === 'openEditDialog') {
            $scope.openEditItemsDialog(fireParam.ldapConnection);
          }
          else if (fireParam.action === 'setEnable') {
            updateEnableState(fireParam.connectionId, fireParam.newState);
          }
        })));

      var updateEnableState = function (connectionId:number,enable:boolean) {
          ldapConnectionResource.updateLdapEnableState( connectionId, enable, function () {
            $scope.reloadData(connectionId,connectionId);
          },function (error) {
              dialogs.error('Error', 'Failed to enable Ldap connection.');
          });
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });



    })
    .directive('ldapConnectionManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/operations/ldapConnection/manageLdapConnectionsMenu.tpl.html',
            controller: 'ldapConnectionMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'firstSelectedListItemToOperate':'=',
              'reloadData':'=',
              'gridFindId':'=',
              'refreshDisplay':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    )



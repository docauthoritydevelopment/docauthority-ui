///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.connections.ldap', [
  'operations.connections.ldap.grid',
  'directives.ldapConnection.manage.menu'
]);

angular.module('operations.connections.ldap')

    .controller('ManageLdapConnectionsCtrl', function ($window, $scope, $state, $location, $element, $compile, eventCommunicator:IEventCommunicator, $timeout, scanService:IScanService,
                                                                Logger, propertiesUtils, $stateParams, pageTitle, bizListsResource:IBizListsResource, configuration:IConfig,$filter,routerChangeService,
                                                                dialogs, bizListResource:IBizListResource, activeScansResource:IActiveScansResource, userSettings:IUserSettingsService, filterFactory:ui.IFilterFactory) {
      var log:ILog = Logger.getInstance('ManageLdapConnectionsCtrl');

      $scope.totalListItemElements = 0;

      $scope.init = function () {
       $scope.initiated = true;
      };

      $scope.onLdapConnectionSelectItem = function (id,name,item) {
        $scope.ldapConnectionSelectedItemId = id;
        $scope.ldapConnectionSelectedItemName = name;
        $scope.ldapConnectionSelectedItem = item;
      };
      $scope.onLdapConnectionSelectItems = function (data) {
        $scope.ldapConnectionSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        let gotALotOfRecords = false;
        routerChangeService.addFileToProcess("Ldap_connections_"+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),"ldap_connections" ,{},{},gotALotOfRecords);
        //ophir need to add the export type
      };


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var onGetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };

    })
    .directive('manageLdapConnection',
      function () {
        return {
          // restrict: 'E',
          templateUrl: '/app/operations/ldapConnection/manageLdapConnections.tpl.html',
          controller: 'ManageLdapConnectionsCtrl',
          replace:true,
          scope:{
            selectItem: '=',
            onSelectedItem: '=',
            changeSelectedItem: '=',
            excelFileName: '='
          },
          link: function (scope:any, element, attrs) {
            scope.init(attrs.left);
          }
        };
      }
    );

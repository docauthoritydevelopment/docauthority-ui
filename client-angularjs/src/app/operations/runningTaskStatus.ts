///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.applyToSolr',[
  'resources.operations'
])

  .controller('RunningTaskStatusCtrl', function ($scope,$element,$state,$stateParams,$compile,$filter,eventCommunicator:IEventCommunicator,scanService:IScanService,
                                             Logger:ILogger,propertiesUtils, configuration:IConfig,$timeout,dialogs ,activeScansResource:IActiveScansResource,
                                             dashboardChartResource:IDashboardChartsResource,operationsResource:IOperationsResource,pageTitle) {
    var log:ILog = Logger.getInstance('ApplyToSolrPageCtrl');
    $scope.initiated=false;

    $scope.init=function(isMaster:boolean)
    {
      if(isMaster) {
       initCommunicationHandlers()
      }
      $scope.$on(scanService.activeRunsChangedEvent, function (sender, value) {
        getRunStatusCompleted(scanService.getActiveRuns());
      });
      $scope.$on(scanService.applyToSolrStatusChangedEvent, function (sender, value) {
        applyTagsToSolrProgressCompleted(scanService.getApplyToSolrStatus());
      });

    };

    $scope.startApplyTagsToSolr = function()
    {
      $scope.errorMsg='';
      $scope.enableStartApplyButton = false;
      scanService.startApplyToSolr();
    };

    $scope.startResyncSolr = function()
    {
      $scope.errorMsg='';
      $scope.enableStartApplyButton = false;
      scanService.startResyncSolr();
    };

    $scope.startApplyPolicyTagsToFiles = function()
    {
      var dlg = dialogs.confirm('Confirmation', 'Are you sure you want to apply files with Policy tags?', null);
      dlg.result.then(function (btn) {
        // Log only - do nothing for now
        log.debug('startApplyPolicyTagsToFiles - yes');
      }, function (btn) {
        //  'You confirmed "No."';
        log.debug('startApplyPolicyTagsToFiles - no');
      });
    };


    $scope.stopApplyTagsToSolr = function()
    {

      $scope.errorMsg='';
      scanService.stopApplyToSolr();
    };

    var applyTagsToSolrProgressCompleted = function(status:Operations.ApplyToSolrStatusView) {
      $scope.initiated = true;
      $scope.applyStatus = status.applyStatus;
      $scope.currentApplyToSolrProgressPercentage=status.currentPercentage;
      $scope.isApplyToSolrRunning = status.isRunning;
      $scope.enableStartApplyButton = status.enableStartApplyButton;
    };

    var getRunStatusCompleted = function(activeRuns:Operations.ActiveRunsView) {
      $scope.initiated=true;
      //$scope.isRunning = status.isRunning;
      //$scope.lastRunElapsedTime = status.lastRunElapsedTime;
      //$scope.isRunningElapsedTime = status.scanRunningElapsedTime;
      //$scope.lastRunFailed = status.isLastRunFailed;
      //$scope.lastRunFinished = status.isLastRunFailed;
      //$scope.enableStopScanRunButton = status.enableStopScanRunButton;
      //$scope.isInScanPhase = status.isInScanPhase;
      //$scope.currentPhaseProgressPercentage=status.currentPhaseProgressPercentage;
      //$scope.currentPhaseProgressPercentageStr = $scope.currentPhaseProgressPercentage?(""+$scope.currentPhaseProgressPercentage+"%"):"";
      //$scope.currentPhaseProgressCounter=status.runStatus.currentPhaseProgressCounter;
      //$scope.currentPhaseProgressFinishMark=status.runStatus.currentPhaseProgressFinishMark;
      //$scope.runningRate = status.runningRate;
      //$scope.phaseName = status.phaseName;
      //
      //$scope.runStatus=status.runStatus;
      //
      //$scope.runArgumentsCount = status.runArgumentsCount;
      //$scope.runArguments = status.runArguments;

    };

    $scope.$on('$destroy', function(){
      eventCommunicator.unregisterAllHandlers($scope);

    });

    $scope.stopScanRun = function()
    {
   //   scanService.stopRun();

    };

    var onGetError=function(e)
    {
      if(e&&e.userMsg) {
        var dlg = dialogs.error('Apply tags could not be started. Scan process is currently running.', null);
        dlg.result.then(function (btn) { //user confirmed deletion

        }, function (btn) {
          //  'You confirmed "No."';
        });

      }
      $scope.initiated = true;
      log.error("error get solr apply status " +e);
      $scope.enableStartApplyButton = true;
      $scope.errorMsg = 'Some difficulties occurred: Contact support'
    };

   var initCommunicationHandlers = function()
   {
      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportHeaderUserAction,
        $scope,
        (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
          /*
           * fireParam object:
           *   { initSearch: false, value: searchText, mode: mode}
           */
          if (fireParam.action === 'startApplyToSolr') {
            $scope.startApplyTagsToSolr();
          }
          else if (fireParam.action === 'startApplyPolicyTagsToFiles') {
            $scope.startApplyPolicyTagsToFiles();
          }
          else if (fireParam.action === 'startResyncSolr') {
            $scope.startResyncSolr();
          }
        })));
    }
  })
    .directive('runningTaskStatusBar',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/app/operations/runningTaskStatus.tpl.html',
        controller: 'RunningTaskStatusCtrl',
        replace:true,
        scope:{

        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
     )
    .directive('runningTaskStatusBarMini',
    function () {
      return {
        // restrict: 'E',
        template: '<div  ng-show="isRunning" title="progress: {{currentPhaseProgressCounter}}/{{currentPhaseProgressFinishMark}} {{currentPhaseProgressPercentageStr}}">' +
          '<uib-progressbar max="100"  value="currentPhaseProgressPercentage" style="height: 5px;margin: 0;"> </uib-progressbar>' +
         ' <small class="pull-left" ng-show="currentPhaseProgressFinishMark>0">{{currentPhaseProgressPercentage}}%</small>'+
         '<small class="pull-right"  ng-show="currentPhaseProgressFinishMark>0">{{currentPhaseProgressCounter}} / {{currentPhaseProgressFinishMark}}</small>'+
          '</div>',
        controller: 'RunningTaskStatusCtrl',
        replace:true,
        scope:{

        },
        link: function (scope:any, element, attrs) {
          scope.init(false);
        }
      };
    })

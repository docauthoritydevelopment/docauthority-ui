///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.serverComponents.list',[

])
  .controller('serverComponentsGridCtrl', function ($scope, $state,$window, $location, $templateCache, Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('serverComponentsGridCtrl');


    $scope.elementName = 'server-components-grid';
    var gridOptions:ui.IGridHelper;

    $scope.init = function () {

      $scope.dateTimeFormat = configuration.dateTimeFormat;

      $scope.isBrokerStatusError = false;

      gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
        fields, parseData, rowTemplate,
        $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

      var doNotSelectFirstRowByDefault = false;
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

    };
    var setUserRoles = function()
    {
      $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
    }
    var isMediaProcessor = $scope.restrictedEntityDisplayTypeName === "MEDIA_PROCESSOR";

    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
      let url;
      if(restrictedEntityId) {
        url= configuration.server_components_data_center_list_url.replace(':dataCenterId', restrictedEntityId);
      }
      else {
        url = configuration.server_components_list_with_status_url;
      }
      return url;
    }

    var getDataUrlParams = function () {
      return null;
    };

    var fields:ui.IFieldDisplayProperties[] =[];

    var dtoSummaryServerComponent:Operations.DtoSummaryServerComponent =  Operations.DtoSummaryServerComponent.Empty();
    var dtoServerComponent:Operations.DtoClaComponent =  Operations.DtoClaComponent.Empty();
    var dtoCustomerDataCenter:Operations.DtoCustomerDataCenter =  Operations.DtoCustomerDataCenter.Empty();

    var dtoServerComponentPrefix =  propertiesUtils.propName(dtoSummaryServerComponent, dtoSummaryServerComponent.claComponentDto)+'.';
    var dtoCustomerDataCenterPrefix =  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.customerDataCenterDto)+'.';


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      isPrimaryKey:true,
      width:'35px'
    };
    var dataCenterName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoCustomerDataCenterPrefix + propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name),
      title: 'Data center',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      template:'<a ng-if="dataItem.'+dtoCustomerDataCenterPrefix + propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.id)+'" ng-click="openEditDialog(dataItem)"><span>{{dataItem.'+dtoCustomerDataCenterPrefix + propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name)+'}}</span></a> '+
      '<a ng-if="dataItem.isDataCenterCandidate && !dataItem.'+dtoCustomerDataCenterPrefix + propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.id)+'" ng-click="openEditDialog(dataItem)">Add ...</a>'
    };

    var claComponentType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.claComponentType),
      title: 'Component type',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      width:'150px',
      template: '<span translate="{{dataItem.' + dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.claComponentType) + '}}"></span>'
    };
    var instanceId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.instanceId),
      title: 'Instance id',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
    var externalNetAddress:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.externalNetAddress),
      title: 'External net address',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      noWrapContent: true
    };
    var localNetAddress:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.localNetAddress),
      title: 'Local net address',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
    var location:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.location),
      title: 'Location',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
    var state:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.state),
      title: 'State',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      noWrapContent: true,
      template:
      '<span ng-if="dataItem.isStateOk" class="fa fa-check label-primary"></span>' +
      '<span ng-if="dataItem.isStateError" class="fa fa-times notice "></span>' +
      '<span ng-if="dataItem.isStatePending" class="fa fa-pause "></span>' +
      '<span ng-if="dataItem.isStateDisabled" class="fa fa-ban "></span>' +
      '<span style="margin-left:5px;" translate="{{dataItem.state}}"></span>'

    };
    var createdOnDB:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.createdOnDB)+'Date',
      title: 'Created on DB',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };
    var stateChangeDate:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.stateChangeDate)+'Date',
      title: 'State change date',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };
    var lastResponseDate:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.lastResponseDate)+'Date',
      title: 'Last response date',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };

    $scope.getLogs = function(){

    }

    $scope.getErrorLogs = function(){

    }


    $scope.openEditDialog = function(dataItem:any) {
      if($scope.isMngScanConfigUser) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridServerComponentsUserAction, {
          action: 'openEditServerComponentItemDialog',
          serverComponentItem: dataItem,
        });
      }
    };


    fields.push(id);

    if(isMediaProcessor) {
      fields.push(instanceId);
      fields.push(state);
      fields.push(dataCenterName);
      fields.push(location);
      fields.push(externalNetAddress);
      fields.push(localNetAddress);
    } else {
      fields.push(claComponentType);
      fields.push(instanceId);
      fields.push(state);
      fields.push(dataCenterName);
      fields.push(externalNetAddress);
      fields.push(localNetAddress);
      fields.push(location);
      fields.push(createdOnDB);
      fields.push(stateChangeDate);
      fields.push(lastResponseDate);
    }

    var rowTemplate =  '';

    var parseData = function (serverSummaryComp: Operations.DtoSummaryServerComponent[]) {

      if(serverSummaryComp)
      {
        serverSummaryComp.forEach(d=> {

          if(_.isEmpty(d.claComponentDto)) {
            let obj = _.extend({}, d);
            d.claComponentDto = obj;
          }

          (<any>d).id=d.claComponentDto.id;
          if(!_.isEmpty(d.extraMetrics)) {
            d.extraMetrics = JSON.parse(d.extraMetrics);

            if(d.extraMetrics ) {
              (<any>d).extraMetrics.usedPhysicalMemorySize = (<any>d).extraMetrics.totalPhysicalMemorySize - (<any>d).extraMetrics.freePhysicalMemorySize;
              (<any>d).extraMetrics.memoryPercentage = (<any>d).extraMetrics.totalPhysicalMemorySize && (<any>d).extraMetrics.totalPhysicalMemorySize >0 ? Math.floor((<any>d).extraMetrics.usedPhysicalMemorySize/ (<any>d).extraMetrics.totalPhysicalMemorySize * 100):0;
            }
          }

          if(!d.claComponentDto.customerDataCenterDto) {
            d.claComponentDto.customerDataCenterDto=<Operations.DtoCustomerDataCenter>{'name':''};
          }
          if(d.claComponentDto.createdOnDB) {
            (<any>d).createdOnDBDate = new Date(d.claComponentDto.createdOnDB);
          }
          if(d.claComponentDto.stateChangeDate) {
            (<any>d).stateChangeDateDate = new Date(d.claComponentDto.stateChangeDate);
            (<any>d).lastStateChangeElapsedTime = Date.now() - (<any>d).stateChangeDateDate;
            (<any>d).lastStateChangeElapsedTimeSpan= $filter('formatSpan')((<any>d).lastStateChangeElapsedTime);
          }
          if(d.claComponentDto.lastResponseDate) {
            (<any>d).lastResponseDateDate = new Date(d.claComponentDto.lastResponseDate);
            (<any>d).lastResponseElapsedTime = Date.now() - (<any>d).lastResponseDateDate;
            (<any>d).lastResponseElapsedTimeSpan= $filter('formatSpan')((<any>d).lastResponseElapsedTime);
          }

          (<any>d).mainTitle = $filter('translate')(d.claComponentDto.componentType);

          if(d.claComponentDto.componentType == 'MEDIA_PROCESSOR') {
            (<any>d).mainTitle = (<any>d).mainTitle + ': ' + d.claComponentDto.instanceId;
          }

          (<any>d).isMediaProcessor =  d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;
          (<any>d).isAppServer =  d.claComponentDto.claComponentType == Operations.EClaComponentType.APPSERVER;
          (<any>d).mainTitleBubble = getMainTitleBubble(d);

          (<any>d).isDataCenterExist = _.isNumber(d.claComponentDto.customerDataCenterDto.id);
          (<any>d).isDataCenterCandidate = d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;

          if(d.claComponentDto.active) {
            (<any>d).isStatePending = d.claComponentDto.state == Operations.EComponentState.PENDING_INIT;
            (<any>d).isStateError = d.claComponentDto.state ==  Operations.EComponentState.FAULTY;
            (<any>d).isStateOk = d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING;

            if(d.claComponentDto.claComponentType == Operations.EClaComponentType.BROKER && (<any>d).isStateError ) {
              $scope.isBrokerStatusError = true;
            }
          } else {
            (<any>d).isStateDisabled = true;
            (<any>d).state = "disabled";
          }
        });

        if(isMediaProcessor) {
          serverSummaryComp = _.filter(serverSummaryComp, (copm) => {
            return copm.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;
          });
        }
      }
      return serverSummaryComp;
    }

    var getMainTitleBubble = function(dataItem) {
      let localNetAddressHtml = '';

      if(dataItem.claComponentDto.localNetAddress) {
        localNetAddressHtml = `<div style='padding-top:12px;'>
        <div style='font-weight: 700; padding-bottom: 5px'>Local Net Address:</div>
        <div>${dataItem.claComponentDto.localNetAddress}</div>
        </div>`
      }
      return `<div>
        <div>${dataItem.mainTitle}</div>
        ${localNetAddressHtml}
      </div>`
    };

    var onSelectedItemChanged = function() {
      $scope.itemSelected?$scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[claComponentType.fieldName],$scope.selectedItem.claComponentDto):null;
    }



    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('serverComponentsGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      replace: true,
      transclude:true,
      scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          restrictedEntityDisplayTypeName: '@',
          localDataBound: '=',
          isBrokerStatusError: '=',
          findId: '=',

          fixHeight: '@',

        },
      controller: 'serverComponentsGridCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

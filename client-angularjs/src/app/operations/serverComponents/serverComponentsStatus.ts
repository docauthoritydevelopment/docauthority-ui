///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */

declare var _:any;
angular.module('operations.serverComponents.status',[

])
  .controller('serverComponentsStatusGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                          propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
    var log:ILog = Logger.getInstance('serverComponentsStatusGridCtrl');


    $scope.elementName = 'server-components-status-grid';
    var gridOptions:ui.IGridHelper;

    $scope.init = function (restrictedEntityDisplayTypeName) {

      $scope.lazyFirstLoad = true;
      gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
        fields, parseData, rowTemplate,
        $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

      var doNotSelectFirstRowByDefault = false;

      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

    };

    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {

      if (restrictedEntityId) {
        let url = configuration.server_components_history_url.replace('{id}', restrictedEntityId);
        if($scope.filterBySearchDateTime >-1) {
          url = url.replace(':timestamp', $scope.filterBySearchDateTime);
        }
        else {
          url =url.replace('timestamp=:timestamp', '');
        }

        if ($scope.customDataCenterId) {
          url = url.replace(':dataCenterId', $scope.customDataCenterId);
        }
        else {
          url = url.replace('&dataCenterID=:dataCenterId', '');
        }


        return url;
      }

      $scope.clear?$scope.clear():null;
      return null;
    }

    var getDataUrlParams = function () {
      return null;
    };

    var dtoClaComponentStatus_AppServer =  Operations.DtoClaComponentStatus_AppServer.Empty();
    var dtoClaComponentStatus_MP =  Operations.DtoClaComponentStatus_MP.Empty();
    var dtoClaComponentStatus_BROKER =  Operations.DtoClaComponentStatus_BROKER.Empty();
    var dtoClaComponentStatus_BROKER_ITEM =  Operations.DtoClaComponentStatus_BROKER_ITEM.Empty();


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      width:'35px'
    };

    var claComponentType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.componentType),
      title: 'Component Type',
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true,
    };

    var timestamp:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.timestamp),
      title: 'Timestamp',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width: '150px',
      format:'{0:'+configuration.dateTimeFormat+'}',
    };

    var startScanTaskRequestsTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Active maps:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.activeScanTasks)} || 0 | number:0}}</div>
        </div>
        <div class='row'>
           <div class='col'>Finished maps:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.startScanTaskRequestsFinished)} | number:0}}</div>
        </div>
        <div class='row'>
          <div class='col'>Total maps:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.startScanTaskRequests)} | number:0}}</div>
        </div>
        <hr />
        <div class='row'>
          <div class='col'>Completed tasks:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanTaskResponsesFinished)} | number:0}}</div>
        </div>
        <div class='row'>
          <div class='col'>Completed tasks with errors:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanTaskFailedResponsesFinished)} | number:0}}</div>
        </div>
      </div>`;

    var startScanTaskRequests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.startScanTaskRequests),
      title: 'Start Map',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px;" title="${startScanTaskRequestsTitle}">
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.activeScanTasks)} || 0 | number:0}}</span>/
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.startScanTaskRequestsFinished)} | number:0}}</span>/
           <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.startScanTaskRequests)} | number:0}}</span>&nbsp;-
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanTaskResponsesFinished)} | number:0}}</span>
          <span ng-if="dtoClaComponentStatus.scanTaskFailedResponsesFinished > 0">{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanTaskFailedResponsesFinished)} | number:0}}</span>
         </span>`
    };

    var ingestTaskRequestsTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Total ingest requests:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequests)} | number:0}}</div>
        </div>
       <div class='row'>
          <div class='col'>Failed requests:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestFailure)} | number:0}}</div>
        </div>
        <div class='row'>
           <div class='col'>Completed successfully:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestsFinished)} | number:0}}</div>
        </div>
        <div class='row'>
          <div class='col'>Completed with errors:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestsFinishedWithError)} | number:0}}</div>
        </div>

      </div>`;

    var ingestTaskRequests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequests),
      title: 'Ingest Task Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${ingestTaskRequestsTitle}">
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestsFinished)} | number:0}}</span>
          <span ng-if="dataItem.ingestTaskRequestsFinishedWithError > 0">/({{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestsFinishedWithError)} | number:0}})</span>
           <span ng-if="dataItem.ingestTaskRequestFailure > 0">/({{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequestFailure)} | number:0}})</span>
           <span><span>/</span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestTaskRequests)} | number:0}}</span>
          </span>`
    };

    var analyzeTaskRequestsTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Total analyze requests:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequests)} | number:0}}</div>
        </div>
       <div class='row'>
           <div class='col'>Failed requests:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequestFailure)} | number:0}}</div>
        </div>
        <div class='row'>
            <div class='col'>Completed successfully:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequestsFinished)} | number:0}}</div>
        </div>


      </div>`;

    var analyzeTaskRequests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequests),
      title: 'Analyze Task Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${analyzeTaskRequestsTitle}">
        <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequestsFinished)} | number:0}}</span>
           <span ng-if="dataItem.ingestTaskRequestFailure > 0">/({{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequestFailure)} | number:0}})</span>
           <span><span>/</span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedTaskRequests)} | number:0}}</span>
         </span>`
    };

    var throughputTitle =
      `<div class='table'>
        <div class='row'>
           <div class='col'>Map rate:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanRunningThroughput)} | number:1}}</div>
        </div>
        <div class='row'>
          <div class='col'>Ingest rate:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestRunningThroughput)} | number:1}}</div>
        </div>
      </div>`;
    var appServerThroughputTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Running rate:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedRunningThroughput)} | number:1}}</div>
        </div>

        <!-- div class='row'>
           <div class='col'>Average rate:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageAnalyzedThroughput)} | number:1}}</div>
        </div -->

      </div>`;

    var appServerIngestThroughputTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Ingest running rate:</div>
          <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.ingestRunningThroughput)} | number:1}}</div>
        </div>

        <div class='row'>
           <div class='col'>Average ingest rate:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageIngestThroughput)} | number:1}}</div>
        </div>
      </div>`;

    var getBrokerTitle = function(brokerNameField:string) {
      return `<div class='table'>
        <div class='row'>
          <div class='col'>Enqueue count:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount)} | number:0}}</div>
        </div>
         
            <div class='row'>
          <div class='col'>Dequeue count:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.DequeueCount)} | number:0}}</div>
        </div>
        </hr>
        <div class='row'>
          <div class='col'>Average enqueue time:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.AverageEnqueueTime)} | number:0}}</div>
        </div>
      
            <div class='row'>
          <div class='col'>Min enqueue time:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.MinEnqueueTime)} | number:0}}</div>
        </div>
          <div class='row'>
          <div class='col'>Max enqueue time:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.MaxEnqueueTime)} | number:0}}</div>
        </div>
         </hr>
             <div class='row'>
          <div class='col'>Consumer count:</div>
          <div class='col right'>{{dataItem.${brokerNameField+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount)} | number:0}}</div>
        </div>

      </div>`;
    }
    var getBrokerTemplate= function(brokerNameField:string) {
      var brokerNameField=brokerNameField+'.';
      return   `<span style="margin-left:5px" title="${getBrokerTitle(brokerNameField)}">
        <span>{{dataItem.${brokerNameField + propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.DequeueCount) }}}</span>
        <span>
            <span>/</span>{{dataItem.${brokerNameField+ propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount)}}}
        </span>
        <span>
            ({{ dataItem.${brokerNameField+ propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount)} - dataItem.${brokerNameField+ propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.DequeueCount)}  }})
        </span>
       </span>`;
    }
    var realtime_requests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.realtime_requests)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount),
      title: 'Realtime Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    realtime_requests.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.realtime_requests));

    var contentcheck_requests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.contentcheck_requests)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount),
      title: 'Content Check Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    contentcheck_requests.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.contentcheck_requests));



    var realtime_responses:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.realtime_responses)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount),
      title: 'Realtime Responses',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    realtime_responses.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.realtime_responses));

    var ingest_requests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.ingest_requests)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount),
      title: 'Ingest Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    ingest_requests.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.ingest_requests));



    var ingest_responses:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.ingest_responses)+ propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount),
      title: 'Ingest Responses',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    ingest_responses.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.ingest_responses));

    var scan_requests:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.scan_requests)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.ConsumerCount),
      title: 'Map Requests',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    scan_requests.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.scan_requests));

    var scan_responses:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.scan_responses)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount),
      title: 'Map Responses',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    scan_responses.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.scan_responses));

    var control_responses:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.control_responses)+propertiesUtils.propName(dtoClaComponentStatus_BROKER_ITEM, dtoClaComponentStatus_BROKER_ITEM.EnqueueCount),
      title: 'Control Responses',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    control_responses.template = getBrokerTemplate(propertiesUtils.propName(dtoClaComponentStatus_BROKER, dtoClaComponentStatus_BROKER.control_responses));

    var throughput:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.averageIngestThroughput),
      title: 'Throughput',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${throughputTitle}">
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.ingestRunningThroughput)} | number:1}}</span> /
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_MP, dtoClaComponentStatus_MP.scanRunningThroughput)} | number:1}}</span>
         </span>`
    };

    var appServerThroughput:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageAnalyzedThroughput),
      title: 'Analyze Throughput',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${appServerThroughputTitle}">
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.analyzedRunningThroughput)} | number:1}}</span> 
          <!-- span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageAnalyzedThroughput)} | number:1}}</span -->
         </span>`
    };

    var appServerIngestThroughput:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageAnalyzedThroughput),
      title: 'Ingest Throughput',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${appServerIngestThroughputTitle}">
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.ingestRunningThroughput)} | number:1}}</span> /
          <span>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.averageIngestThroughput)} | number:1}}</span>
         </span>`
    };

    var physicalMemoryTitle =
      `<div class='table'>
        <div class='row'>
          <div class='col'>Used memory size:</div>
          <div class='col right'>{{dataItem.usedPhysicalMemorySize | bytes}}</div>
        </div>
        <div class='row'>
           <div class='col'>Total memory size:</div>
           <div class='col right'>{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.totalPhysicalMemorySize)} | bytes}}</div>
        </div>
      </div>`;

    var physicalMemory:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.totalPhysicalMemorySize),
      title: 'Memory',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<span style="margin-left:5px" title="${physicalMemoryTitle}">
        <span>{{dataItem.usedPhysicalMemorySize | bytes }} / {{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.totalPhysicalMemorySize)} | bytes }} ({{dataItem.memoryPercentage}}%)</span>
       </span>`
    };

    var systemCpuLoad:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.systemCpuLoad),
      title: 'System CPU',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<div class="system-cpu">
          <div class="percentage-ruler">
            <div class="percentage" style="width:{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.systemCpuLoad)} * 100 }}%"></div>
          </div>
          <div class="percentage-text">{{dataItem.${propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.systemCpuLoad)} * 100 | number : 2 }}%</div>
       </div>`
    };


    var solrNodeDiskSpace:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  propertiesUtils.propName(dtoClaComponentStatus_AppServer, dtoClaComponentStatus_AppServer.systemCpuLoad),
      title: 'Disk space utilization',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template:
        `<div ng-bind-html="dataItem.diskSizeStr">
         
       </div>`
    };




    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(claComponentType);
    fields.push(timestamp);


    if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.SOLR_NODE){
      fields.push(solrNodeDiskSpace);
    }
    else if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.APPSERVER){
      fields.push(ingestTaskRequests);
      fields.push(analyzeTaskRequests);
      fields.push(appServerIngestThroughput);
      fields.push(appServerThroughput);
      fields.push(solrNodeDiskSpace);
    }
    else  if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.BROKER_DC){
      fields.push(scan_requests);
      fields.push(ingest_requests);
      fields.push(realtime_requests);
      fields.push(contentcheck_requests);
    }
    else  if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.BROKER_FC){
      fields.push(scan_responses);
      fields.push(ingest_responses);
      fields.push(realtime_responses);
      fields.push(control_responses);
    }
    else  if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.BROKER){
      fields.push(scan_requests);
      fields.push(ingest_requests);
      fields.push(realtime_requests);
      fields.push(contentcheck_requests);
    }
    else  if($scope.restrictedEntityDisplayTypeName ==  Operations.EClaComponentType.MEDIA_PROCESSOR){
      fields.push(startScanTaskRequests);
      fields.push(ingestTaskRequests);
      fields.push(throughput);
      fields.push(solrNodeDiskSpace);
    }
    else {
      fields.push(startScanTaskRequests);
      fields.push(ingestTaskRequests);
      fields.push(throughput);
    }

    if($scope.restrictedEntityDisplayTypeName !=  Operations.EClaComponentType.BROKER && $scope.restrictedEntityDisplayTypeName !=  Operations.EClaComponentType.SOLR_NODE && $scope.restrictedEntityDisplayTypeName !=  Operations.EClaComponentType.BROKER_FC && $scope.restrictedEntityDisplayTypeName !=  Operations.EClaComponentType.BROKER_DC) {
      fields.push(physicalMemory);
      fields.push(systemCpuLoad);
    }


    var rowTemplate="";

    var parseData = function (serverComp: Operations.DtoClaComponentStatus[]) {
      if(serverComp)
      {
        serverComp.forEach(d=> {
          if(d.extraMetrics) {
            let eMatrix = angular.fromJson(d.extraMetrics);
            let extraMetrixKeys = Object.keys(eMatrix);
            for (let count = 0 ; count < extraMetrixKeys.length ; count++) {
              if (extraMetrixKeys[count].indexOf('_')>-1) {
                eMatrix[extraMetrixKeys[count].substring(extraMetrixKeys[count].indexOf('_')+1)] =  eMatrix[extraMetrixKeys[count]];
              }
            }
            angular.extend(d, eMatrix);
          }
          if(d.timestamp) {
            (<any>d).timestamp = new Date(d.timestamp);
          }
          if((<any>d).driveData) {
            let trashHoldNum = 10000;
            let driveData:any = JSON.parse((<any>d).driveData);
            if (driveData.drive_capacities) {
              let specificStr = '';
              if (driveData.drive_capacities && driveData.drive_capacities.length > 0 ) {
                driveData.drive_capacities.forEach((dItem: any) => {
                  specificStr = specificStr+ dItem.drive_id +' ' +  $filter('bytes')(Number(dItem.total_space-dItem.free_space) * 1024 * 1024) +' / ' + $filter('bytes')(Number(dItem.total_space)  * 1024 * 1024)+ ' ('+ Math.ceil(100*(dItem.total_space-dItem.free_space)/dItem.total_space) +'%)';
                  specificStr = specificStr +'<br>';
                })
              }
              (<any>d).diskSizeStr =  specificStr ;
            }

          }
          else {
            (<any>d).diskSizeStr =  'N/A';
          }

          if( (<Operations.DtoClaComponentStatus_AppServer>d).processCpuLoad && (<Operations.DtoClaComponentStatus_AppServer>d).processCpuLoad<0 ) {
            (<Operations.DtoClaComponentStatus_AppServer>d).processCpuLoad=0;
          }
          if( (<Operations.DtoClaComponentStatus_AppServer>d).systemCpuLoad && (<Operations.DtoClaComponentStatus_AppServer>d).systemCpuLoad<0 ) {
            (<Operations.DtoClaComponentStatus_AppServer>d).systemCpuLoad=0;
          }
          (<any>d).usedPhysicalMemorySize = (<Operations.DtoClaComponentStatus_AppServer>d).totalPhysicalMemorySize - (<Operations.DtoClaComponentStatus_AppServer>d).freePhysicalMemorySize;
          (<any>d).memoryPercentage = (<Operations.DtoClaComponentStatus_AppServer>d).totalPhysicalMemorySize && (<Operations.DtoClaComponentStatus_AppServer>d).totalPhysicalMemorySize >0 ?
            Math.floor((<any>d).usedPhysicalMemorySize/ (<Operations.DtoClaComponentStatus_AppServer>d).totalPhysicalMemorySize * 100):0;
        });
      }
      return serverComp;
    }


    var onSelectedItemChanged = function()
    {

      $scope.itemSelected?$scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[claComponentType.fieldName],$scope.selectedItem):null;
    }

    $scope.localDataBound = function() {
      _.defer(()=> {
        $( ".system-cpu" ).each(function() {
          $(this).find('.percentage').css("width", $(this).find('.percentage-text').html());
        });
      })
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('serverComponentsStatusGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      replace: true,
      transclude:true,
      scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          customDataCenterId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          localDataBound: '=',
          findId: '=',
          filterBySearchDateTime: '=',
          restrictedEntityDisplayTypeName: '@',
          fixHeight: '@'
        },
      controller: 'serverComponentsStatusGridCtrl',
      link: function (scope:any, element, attrs) {
        scope.init();
      }
    }

  });

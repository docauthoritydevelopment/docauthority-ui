///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.serverComponents.tree',[

])
  .controller('serverComponentsTreeListCtrl', function ($rootScope,$scope, $state,$window, $location, $templateCache, Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,routerChangeService,
                                                        propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider,dialogs,saveFilesBuilder:ISaveFileBuilder, systemSettingsResource) {
    var log:ILog = Logger.getInstance('serverComponentsTreeListCtrl');


    $scope.elementName = 'server-components-tree-list';
    $scope.showNotificationWarinigPeriod = 600000;
    var gridOptions:ui.IGridHelper;



    $scope.init = function () {

      /*
      systemSettingsResource.getSystemSettings( (systemSettingList) => {
        if (systemSettingList != null) {
          let fList:any[] = systemSettingList.filter((sSettingsItem:any) => {
            return sSettingsItem.name === 'solr_node_disk_space_threshold';
          });
          if (fList.length > 0 ) {
            configuration.solr_node_disk_space_threshold = fList[0].value;
          }
        }
      },null); */



      $scope.dateTimeFormat = configuration.dateTimeFormat;

      $scope.isBrokerStatusError = false;


      gridOptions = GridBehaviorHelper.createTreeList($scope, log, configuration, eventCommunicator, userSettings, getDataUrlParams,
        fields, parseData, '',
        $stateParams, null, getDataUrlBuilder, true, $scope.fixHeight);
      gridOptions.gridSettings.change = handleSelectionChange;


      var doNotSelectFirstRowByDefault = false;
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

    };
    var setUserRoles = function()
    {
      $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
    }


    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {

      let url;
      if(restrictedEntityId) {
        url= configuration.server_components_data_center_list_url.replace(':dataCenterId', restrictedEntityId);
      }
      else {
        url = configuration.server_components_list_with_status_url;
      }
      if($scope.filterBySearchDateTime > -1) {
        url = url.replace(':timestamp', $scope.filterBySearchDateTime);
      }
      else {
        url =url.replace('timestamp=:timestamp', '');
      }
      url+='&pageSize=120&take=120';
      return url;
    }

    var getDataUrlParams = function () {
      return null;
    };

    var fields:ui.IFieldDisplayProperties[] =[];

    var dtoSummaryServerComponent:Operations.DtoSummaryServerComponent =  Operations.DtoSummaryServerComponent.Empty();
    var dtoServerComponent:Operations.DtoClaComponent =  Operations.DtoClaComponent.Empty();
    var dtoCustomerDataCenter:Operations.DtoCustomerDataCenter =  Operations.DtoCustomerDataCenter.Empty();

    var dtoServerComponentPrefix =  propertiesUtils.propName(dtoSummaryServerComponent, dtoSummaryServerComponent.claComponentDto)+'.';
    var dtoCustomerDataCenterPrefix =  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.customerDataCenterDto)+'.';

    var rowTemplate = $templateCache.get('/app/operations/serverComponents/serverComponentsGridItemRow.tpl.html') ;


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:propertiesUtils.propName(dtoServerComponent, dtoServerComponent.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      isPrimaryKey:true,
      width:'35px'
    };

    var expamndComp:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: '',
      title: 'Component title',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      expandable:true,
      width: '35px'
    };

    var compDetails:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: 'mainTitle',
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      expandable:false,
      template : rowTemplate
    };



    var dataCenterName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:'dataCenterName',
      title: 'Data center',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true
    };

    var diskSpaceUtilization:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:'solrDiskSpace',
      title: 'Disk space utilization',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true
    };

    var claComponentType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.claComponentType),
      title: 'Component Type',
      type: EFieldTypes.type_string,
      displayed: false,
      editable: false,
      nullable: true,
      expandable: true,
      width:'150px',
      template: '<span translate="{{dataItem.' + dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.claComponentType) + '}}"></span>'
    };


    var instanceId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.instanceId),
      title: 'Instance Id',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
    };
    var mainTitle:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'mainTitle',
      title: 'Component name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
    };
    var externalNetAddress:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.externalNetAddress),
      title: 'External Net Address',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      noWrapContent: true
    };
    var localNetAddress:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.localNetAddress),
      title: 'Local Net Address',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,

    };
    var location:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.location),
      title: 'Location',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
    };


    var state:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.state),
      title: 'State',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
      noWrapContent: true,
      template:
      '<span ng-if="dataItem.isStateOk" class="fa fa-check label-primary"></span>' +
      '<span ng-if="dataItem.isStateError" class="fa fa-times notice "></span>' +
      '<span ng-if="dataItem.isStatePending" class="fa fa-pause "></span>' +
      '<span ng-if="dataItem.isStateDisabled" class="fa fa-ban "></span>' +
      '<span style="margin-left:5px;" translate="{{dataItem.state}}"></span>'
    };


    var createdOnDB:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.createdOnDB)+'Date',
      title: 'Created On DB',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };

    var parentId:ui.IHierarchicalFieldDisplayProperties = <ui.IHierarchicalFieldDisplayProperties>{
      fieldName: 'parentId',
      title: 'Parent ID',
      isParentId:true,
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,

    };
    var stateChangeDate:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoServerComponentPrefix + propertiesUtils.propName(dtoServerComponent, dtoServerComponent.stateChangeDate),
      title: 'State Change Date',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };
    var lastResponseDate:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'statusTimestamp',
      title: 'Last response date',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };

    var createExcelSheets=()=>  {
      let gotALotOfRecords = false;
      routerChangeService.addFileToProcess("Server_components_"+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'system_components' ,{timestamp: $scope.filterBySearchDateTime},{},gotALotOfRecords);
    }

    $rootScope.$on('exportServerComponentTreeToExcel', function(event, data) {
      createExcelSheets();
    });


    $scope.openEditDialog = function(dataItem:any) {
      if($scope.isMngScanConfigUser) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridServerComponentsUserAction, {
          action: 'openEditServerComponentItemDialog',
          serverComponentItem: dataItem,
        });
      }
    };

    fields.push(id);
    fields.push(parentId);
    fields.push(expamndComp);
    fields.push(compDetails);


    /*
    fields.push(createdOnDB);
    fields.push(stateChangeDate);
    fields.push(lastResponseDate);
*/


    var latestParsedData = null;
    var treeComponentsOrderArray = [
      Operations.EClaComponentType.APPSERVER,
      Operations.EClaComponentType.DB,
      Operations.EClaComponentType.BROKER_FC,
      Operations.EClaComponentType.BROKER_DC,
      Operations.EClaComponentType.BROKER,
      Operations.EClaComponentType.ZOOKEEPER,
      Operations.EClaComponentType.SOLR_CLAFILE,
      Operations.EClaComponentType.SOLR_SIMILARITY,
      Operations.EClaComponentType.SOLR_SIM,
      Operations.EClaComponentType.SOLR_NODE,
      Operations.EClaComponentType.SOLR_ROOT_NODE,
      Operations.EClaComponentType.MEDIA_PROCESSOR,
      Operations.EClaComponentType.DATA_CENTER
    ];


    var parseData = function (serverSummaryComp: Operations.DtoSummaryServerComponent[]) {
      if(serverSummaryComp)
      {
        let totalDcBrokersNum = 0 ;
        let brokerObject = null;
        let foundSolrNode:boolean = false;
        let rootSolrNode: Operations.DtoSummaryServerComponent = null;

        serverSummaryComp.forEach(d=>
        {
          if (d.claComponentDto && d.claComponentDto.claComponentType === Operations.EClaComponentType.BROKER) {
            brokerObject = d;
            (<any>d).scanAverageEnqueueTime = 0;
            (<any>d).ingestAverageEnqueueTime =  0;
            (<any>d).scanDequeueCount = 0;
            (<any>d).ingestDequeueCount = 0;
            (<any>d).scanEnqueueCount = 0;
            (<any>d).ingestEnqueueCount = 0;
            (<any>d).level = 0;
            (<any>d).parents  = [(<any>d).claComponentDto.id ];
          }
          else if (d.claComponentDto && d.claComponentDto.claComponentType === Operations.EClaComponentType.SOLR_NODE) {
            foundSolrNode = true;
          }
        });

        if (foundSolrNode) {
          rootSolrNode = buildSolrNodeRoot();
        }

        let dataCenterMap = {};
        let solrNodeIndx= 1;
        serverSummaryComp.forEach(d=>
        {
          (<any>d).id=d.claComponentDto.id;
          (<any>d).elementId=d.claComponentDto.id;
          if(!_.isEmpty(d.extraMetrics)) {
            d.extraMetrics = JSON.parse(d.extraMetrics);

            if(d.extraMetrics ) {
              (<any>d).extraMetrics.usedPhysicalMemorySize = (<any>d).extraMetrics.totalPhysicalMemorySize - (<any>d).extraMetrics.freePhysicalMemorySize;
              (<any>d).extraMetrics.memoryPercentage = (<any>d).extraMetrics.totalPhysicalMemorySize && (<any>d).extraMetrics.totalPhysicalMemorySize >0 ? Math.floor((<any>d).extraMetrics.usedPhysicalMemorySize/ (<any>d).extraMetrics.totalPhysicalMemorySize * 100):0;
            }
          }
          if(!d.claComponentDto.customerDataCenterDto) {
            d.claComponentDto.customerDataCenterDto=<Operations.DtoCustomerDataCenter>{'name':''};
          }
          if(d.claComponentDto.createdOnDB) {
            (<any>d).createdOnDBDate = new Date(d.claComponentDto.createdOnDB);
          }

          if (d.claComponentDto.claComponentType == Operations.EClaComponentType.APPSERVER) {
            addDiskSpace(d);
          }

          let startDateNumber = Date.now();
          if ($scope.filterBySearchDateTime !== null && $scope.filterBySearchDateTime !== undefined && $scope.filterBySearchDateTime !== -1) {
            startDateNumber = $scope.filterBySearchDateTime;
          }
          if(d.claComponentDto.stateChangeDate && ($scope.filterBySearchDateTime === null || $scope.filterBySearchDateTime === undefined || $scope.filterBySearchDateTime===-1)) {
            (<any>d).stateChangeDateDate = new Date(d.claComponentDto.stateChangeDate);
            (<any>d).lastStateChangeElapsedTime = startDateNumber - (<any>d).stateChangeDateDate;
            (<any>d).lastStateChangeElapsedTimeSpan= $filter('formatSpan')((<any>d).lastStateChangeElapsedTime);
          }
          if(d.statusTimestamp) {
            (<any>d).lastResponseDateDate = new Date(d.statusTimestamp);
            (<any>d).lastResponseElapsedTime = startDateNumber - (<any>d).lastResponseDateDate;
            (<any>d).lastResponseElapsedTimeSpan= $filter('formatSpan')((<any>d).lastResponseElapsedTime);
          }

          (<any>d).subTitle = d.claComponentDto.externalNetAddress;
          addStateParams(d,d,$scope.filterBySearchDateTime);


          if(d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR) {
            buildMediaProcessorSubTree(d, serverSummaryComp, d.extraMetrics, dataCenterMap);
          }
          else if(d.claComponentDto.claComponentType === Operations.EClaComponentType.BROKER) {
            buildBrokerSubTree(d, serverSummaryComp, d.extraMetrics, $scope.filterBySearchDateTime);
            (<any>d).mainTitle =  'Broker - Datacenter';
            (<any>d).mainTitleBubble =  'Broker - Datacenter';
          }
          else if(d.claComponentDto.claComponentType === Operations.EClaComponentType.BROKER_DC) {
            if (!(<any>d).isStateDisabled)
            {
              buildBrokerDC(d, d.extraMetrics, brokerObject, (totalDcBrokersNum == 0));
              totalDcBrokersNum++;
            }
          }
          else if(d.claComponentDto.claComponentType === Operations.EClaComponentType.SOLR_NODE) {
            (<any>d).mainTitle = $filter('translate')(d.claComponentDto.componentType)+' '+ solrNodeIndx++;
            (<any>d).mainTitleBubble = getMainTitleBubble(d);
             buildSolrNodeSubTree(d, rootSolrNode);
          }
          else {
            d['parents'] = [(<any>d).id ];
            d['level'] = 0;
          }

          (<any>d).isMediaProcessor =  d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;
          (<any>d).isBroker =  d.claComponentDto.claComponentType == Operations.EClaComponentType.BROKER;
          (<any>d).isAppServer =  d.claComponentDto.claComponentType == Operations.EClaComponentType.APPSERVER;
          (<any>d).isSolrNode =  d.claComponentDto.claComponentType == Operations.EClaComponentType.SOLR_NODE;


          if(d.claComponentDto.claComponentType !== Operations.EClaComponentType.SOLR_NODE && d.claComponentDto.claComponentType !== Operations.EClaComponentType.BROKER_DC && d.claComponentDto.claComponentType !== Operations.EClaComponentType.BROKER) {
            (<any>d).mainTitle = $filter('translate')(d.claComponentDto.componentType);
            (<any>d).mainTitleBubble = getMainTitleBubble(d);
          }

          (<any>d).isDataCenterExist = _.isNumber(d.claComponentDto.customerDataCenterDto.id);
          if ((<any>d).isDataCenterExist) {
            (<any>d).dataCenterName = d.claComponentDto.customerDataCenterDto.name;
          }
          (<any>d).isDataCenterCandidate = d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;

        });
        brokerObject.scanAverageEnqueueTime = totalDcBrokersNum === 0 ? 0 : brokerObject.scanAverageEnqueueTime !== null ? brokerObject.scanAverageEnqueueTime / totalDcBrokersNum : null;
        brokerObject.ingestAverageEnqueueTime  =  totalDcBrokersNum === 0 ? 0 :  brokerObject.ingestAverageEnqueueTime !== null ? brokerObject.ingestAverageEnqueueTime / totalDcBrokersNum : null;
        if (rootSolrNode) {
          serverSummaryComp.push(rootSolrNode);
        }

        serverSummaryComp.sort((d1,d2): number => {
          let val1 = d1.claComponentDto ? treeComponentsOrderArray.indexOf(d1.claComponentDto.claComponentType) : -1;
          let val2 = d2.claComponentDto ? treeComponentsOrderArray.indexOf(d2.claComponentDto.claComponentType) : -1;
          return (val1 - val2);
        });

      }

      serverSummaryComp = serverSummaryComp.filter((d) => {
        return (d.extraMetrics ||  d.claComponentDto.claComponentType !== Operations.EClaComponentType.BROKER_DC ) && (d.claComponentDto.claComponentType !== Operations.EClaComponentType.BROKER_DC || !(<any>d).isStateDisabled)
      });


      latestParsedData = serverSummaryComp;
      return serverSummaryComp;
    }


    var addDiskSpace  = function(d) {
      d['solrDiskSpace'] = 'N/A';
      if((<any>d).driveData) {
        let trashHoldNum = 10000;
        let driveData:any = JSON.parse((<any>d).driveData);
        if (driveData.drive_capacities) {
          let totalSpace = 0 ;
          let freeSpace = 0 ;
          let specificStr = '';
          let diskSpaceTooltip = '';
          let passTreshold: boolean = false;
          if (driveData.drive_capacities && driveData.drive_capacities.length > 0 ) {
            driveData.drive_capacities.forEach((dItem: any) => {
              totalSpace = totalSpace + dItem.total_space;
              freeSpace = freeSpace + dItem.free_space;

              diskSpaceTooltip = diskSpaceTooltip+  '<b>'+dItem.drive_id +'</b>  '+   $filter('bytes') (1024*1024*Number(dItem.total_space-dItem.free_space))  +' / ' + $filter('bytes') (1024*1024*Number(dItem.total_space))  + ' ('+ Math.ceil(100*(dItem.total_space-dItem.free_space)/dItem.total_space) +'%)';
              if (( totalSpace && totalSpace!= 0 ) && (100*(totalSpace-freeSpace)/totalSpace  > Number(configuration['ui.solr_node_disk_space_threshold']))) {
                passTreshold = true;
                diskSpaceTooltip = diskSpaceTooltip+ ' <span title="Free disk space for device is bellow threshold" class="fa fa-exclamation-triangle notice-info"></span>';
              }
              diskSpaceTooltip = diskSpaceTooltip+ '<br>'
            })
          }
          let spacePercentage = ( totalSpace && totalSpace!= 0 )?  Math.ceil(100*(totalSpace-freeSpace)/totalSpace) + "%" : 'N/A';
          d['solrDiskSpace'] = spacePercentage ;
          d['diskSizeWarning'] = passTreshold;
          d['diskSizeTooltip']= diskSpaceTooltip;
        }

      }
    }


    var buildSolrNodeSubTree = function(d, rootSolrNode) {
      d['parents'] = [(<any>rootSolrNode).id, (<any>d).id ];
      d['parentId'] = (<any>rootSolrNode).id;
      d['level'] = 1;
      if (rootSolrNode.isStatePending) {
        rootSolrNode.isStateDisabled = !d.claComponentDto.active;
        rootSolrNode.isStatePending = !rootSolrNode.isStateDisabled && d.claComponentDto.state == Operations.EComponentState.PENDING_INIT;
        rootSolrNode.isStateError = !rootSolrNode.isStateDisabled && d.claComponentDto.state ==  Operations.EComponentState.FAULTY;
        rootSolrNode.isStateOk = !rootSolrNode.isStateDisabled &&  d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING;
        rootSolrNode.claComponentDto.state = d.claComponentDto.state;
      }
      else if ((d.claComponentDto.state != Operations.EComponentState.PENDING_INIT) && ((!d.claComponentDto.active  && !rootSolrNode.isStateDisabled) ||
        (rootSolrNode.isStateError != (d.claComponentDto.state ==  Operations.EComponentState.FAULTY)) ||
        (rootSolrNode.isStateOk != (d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING))))
      {
        rootSolrNode.isStatePending = false;
        rootSolrNode.isStateError = false;
        rootSolrNode.isStateOk = false;
        rootSolrNode.isStateDisabled = false;
        rootSolrNode.isStatePartial = true;
        rootSolrNode.claComponentDto.state = 'Partial';
      }

      if ( d.statusTimestamp && (!rootSolrNode.statusTimestamp || (d.statusTimestamp && rootSolrNode.statusTimestamp >   d.statusTimestamp ))){
        let startDateNumber = Date.now();
        if ($scope.filterBySearchDateTime !== null && $scope.filterBySearchDateTime !== undefined && $scope.filterBySearchDateTime !== -1) {
          startDateNumber = $scope.filterBySearchDateTime;
        }
        rootSolrNode.statusTimestamp =   d.statusTimestamp;
        rootSolrNode.lastResponseDateDate = new Date(rootSolrNode.statusTimestamp);
        rootSolrNode.lastResponseElapsedTime = startDateNumber - rootSolrNode.statusTimestamp;
        rootSolrNode.lastResponseElapsedTimeSpan= $filter('formatSpan')(rootSolrNode.lastResponseElapsedTime);
      }
      addDiskSpace(d);
      if (d['diskSizeWarning']) {
        rootSolrNode['diskSizeWarning'] = true;
      }
    }


    var buildBrokerDC = function(d, extraMetrics,brokerObject, isFirstDcBroker) {
      d.parents =  [d.id, brokerObject.id];
      d.parentId = brokerObject.id;
      d.level = 1;
      d.mainTitle = 'Broker - ' + d.claComponentDto.customerDataCenterDto.name;
      d.mainTitleBubble = 'Broker - ' + d.claComponentDto.customerDataCenterDto.name;
      d.isBrokerInfo = true;
      d.dataCenterId  = d.claComponentDto.customerDataCenterDto.id;

      if (extraMetrics) {
        let scanQueue = extraMetrics[d.claComponentDto.customerDataCenterDto.id + '_scan_requests'];
        d.scanAverageEnqueueTime = scanQueue? scanQueue.AverageEnqueueTime:0;
        d.scanDequeueCount = scanQueue? scanQueue.DequeueCount: 0;
        d.scanEnqueueCount =  scanQueue? scanQueue.EnqueueCount: 0;

        let ingestQueue = extraMetrics[d.claComponentDto.customerDataCenterDto.id + '_ingest_requests'];

        d.ingestAverageEnqueueTime = ingestQueue? ingestQueue.AverageEnqueueTime:0;
        d.ingestDequeueCount =  ingestQueue? ingestQueue.DequeueCount:0;
        d.ingestEnqueueCount =  ingestQueue? ingestQueue.EnqueueCount:0;


        if (isFirstDcBroker)
        {
          brokerObject.scanAverageEnqueueTime = d.scanAverageEnqueueTime ;
          brokerObject.ingestAverageEnqueueTime = d.ingestAverageEnqueueTime ;
          brokerObject.scanDequeueCount = d.scanDequeueCount ;
          brokerObject.ingestDequeueCount = d.ingestDequeueCount ;
          brokerObject.scanEnqueueCount = d.scanEnqueueCount ;
          brokerObject.ingestEnqueueCount = d.ingestEnqueueCount;
        }
        else {
          brokerObject.scanAverageEnqueueTime = d.scanAverageEnqueueTime !== null  && brokerObject.scanAverageEnqueueTime !== null ? brokerObject.scanAverageEnqueueTime + d.scanAverageEnqueueTime : null;
          brokerObject.ingestAverageEnqueueTime = d.ingestAverageEnqueueTime !== null  && brokerObject.ingestAverageEnqueueTime !== null ? brokerObject.ingestAverageEnqueueTime + d.ingestAverageEnqueueTime : null;
          brokerObject.scanDequeueCount = brokerObject.scanDequeueCount !== null && d.scanDequeueCount !== null ? brokerObject.scanDequeueCount + d.scanDequeueCount : null;
          brokerObject.ingestDequeueCount = brokerObject.ingestDequeueCount !== null && d.ingestDequeueCount !== null? brokerObject.ingestDequeueCount + d.ingestDequeueCount : null;
          brokerObject.scanEnqueueCount = brokerObject.scanEnqueueCount !== null && d.scanEnqueueCount !== null ? brokerObject.scanEnqueueCount + d.scanEnqueueCount : null;
          brokerObject.ingestEnqueueCount = brokerObject.ingestEnqueueCount !== null && d.ingestEnqueueCount !== null ? brokerObject.ingestEnqueueCount + d.ingestEnqueueCount : null;
        }
      }
      else if (isFirstDcBroker){
        brokerObject.scanAverageEnqueueTime = null;
        brokerObject.ingestAverageEnqueueTime = null;
        brokerObject.scanDequeueCount = null;
        brokerObject.ingestDequeueCount = null;
        brokerObject.scanEnqueueCount = null;
        brokerObject.ingestEnqueueCount = null;
      }
    }



    var buildMediaProcessorSubTree = function(d, serverSummaryComp, extraMetrics, dataCenterMap) {
      let startDateNumber = Date.now();
      if ($scope.filterBySearchDateTime !== null && $scope.filterBySearchDateTime !== undefined && $scope.filterBySearchDateTime !== -1) {
        startDateNumber = $scope.filterBySearchDateTime;
      }
      (<any>d).mainTitle = (<any>d).mainTitle + ': ' + d.claComponentDto.instanceId;
      (<any>d).mainTitleBubble = (<any>d).mainTitle + ': ' + d.claComponentDto.instanceId;
      addDiskSpace(d);
      let currentDataCenter:any = null;
      if (dataCenterMap[d.claComponentDto.customerDataCenterDto.id]){
        currentDataCenter = dataCenterMap[d.claComponentDto.customerDataCenterDto.id];

        if (currentDataCenter.isStatePending || currentDataCenter.isStateNone ) {
          currentDataCenter.isStateNone =  d.isStateNone;
          currentDataCenter.isStateDisabled = !d.claComponentDto.active;
          currentDataCenter.isStatePending = !currentDataCenter.isStateDisabled && d.claComponentDto.state == Operations.EComponentState.PENDING_INIT;
          currentDataCenter.isStateError = d.claComponentDto.state ==  Operations.EComponentState.FAULTY;
          currentDataCenter.isStateOk = !currentDataCenter.isStateDisabled &&  d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING;
          currentDataCenter.claComponentDto.state =  d.claComponentDto.state;
        }
        else if ((d.claComponentDto.state != Operations.EComponentState.PENDING_INIT) && (!d.isStateNone) && ((!d.claComponentDto.active  && !currentDataCenter.isStateDisabled) ||
          (currentDataCenter.isStateError != (d.claComponentDto.state ==  Operations.EComponentState.FAULTY)) ||
          (currentDataCenter.isStateNone != d.isStateNone) ||
          (currentDataCenter.isStateOk != (d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING))))
        {
          currentDataCenter.isStatePending = false;
          currentDataCenter.isStateError = false;
          currentDataCenter.isStateOk = false;
          currentDataCenter.isStateDisabled = false;
          currentDataCenter.isStatePartial = true;
          currentDataCenter.claComponentDto.state = 'Partial';
        }

        if (d.statusTimestamp && currentDataCenter.statusTimestamp >   d.statusTimestamp ){
          currentDataCenter.statusTimestamp =   d.statusTimestamp;
          currentDataCenter.lastResponseDateDate = new Date(currentDataCenter.statusTimestamp);
          currentDataCenter.lastResponseElapsedTime = startDateNumber - currentDataCenter.statusTimestamp;
          currentDataCenter.lastResponseElapsedTimeSpan= $filter('formatSpan')(currentDataCenter.lastResponseElapsedTime);
        }
      }
      else { //create stub datacenter row
        currentDataCenter = new Operations.DtoSummaryServerComponent();
        currentDataCenter.id =  1000000+d.claComponentDto.customerDataCenterDto.id;  //unique ID is needed for treelist
        currentDataCenter.elementId = d.claComponentDto.customerDataCenterDto.id;
        currentDataCenter.parents = [ 1000000+d.claComponentDto.customerDataCenterDto.id];
        currentDataCenter.claComponentDto = new Operations.DtoClaComponent();
        currentDataCenter.claComponentDto.claComponentType = Operations.EClaComponentType.DATA_CENTER;
        currentDataCenter.claComponentDto.id =  currentDataCenter.id;
        currentDataCenter.mainTitle = 'Datacenter - '+ d.claComponentDto.customerDataCenterDto.name;
        currentDataCenter.mainTitleBubble = 'Datacenter - '+ d.claComponentDto.customerDataCenterDto.name;

        currentDataCenter.scanTaskFinished = 0 ;
        currentDataCenter.ingestTaskFinished = 0 ;
        currentDataCenter.mapRate = 0 ;
        currentDataCenter.ingestRate = 0 ;
        currentDataCenter.isDataCenter = true;


        currentDataCenter.isStateDisabled = !d.claComponentDto.active;
        currentDataCenter.isStateNone = d.isStateNone;
        currentDataCenter.isStatePending = !currentDataCenter.isStateDisabled && d.claComponentDto.state == Operations.EComponentState.PENDING_INIT;
        currentDataCenter.isStateError = d.claComponentDto.state ==  Operations.EComponentState.FAULTY;
        currentDataCenter.isStateOk = !currentDataCenter.isStateDisabled &&  d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING;

        currentDataCenter.claComponentDto.state = d.claComponentDto.state;
        currentDataCenter.statusTimestamp = d.statusTimestamp;
        if (d.statusTimestamp) {
          currentDataCenter.lastResponseDateDate = new Date(currentDataCenter.statusTimestamp);
          currentDataCenter.lastResponseElapsedTime = startDateNumber - currentDataCenter.lastResponseDateDate;
          currentDataCenter.lastResponseElapsedTimeSpan = $filter('formatSpan')(currentDataCenter.lastResponseElapsedTime);
        }


        dataCenterMap[d.claComponentDto.customerDataCenterDto.id] = currentDataCenter;
        serverSummaryComp.push(currentDataCenter);
      }
      currentDataCenter.mpNumber =  (currentDataCenter.mpNumber ? currentDataCenter.mpNumber+1 : 1);
      currentDataCenter.subTitle = currentDataCenter.mpNumber+ (currentDataCenter.mpNumber ==1 ? ' media processor' : ' media processors');
      d['parentId'] = currentDataCenter.id;
      d['parents'] = [currentDataCenter.id,(<any>d).id ];
      d['level'] = 1;
      if (_.isObject(extraMetrics)) {
        d['scanTaskFinished'] = extraMetrics.scanTaskResponsesFinished + extraMetrics.scanTaskFailedResponsesFinished;
        d['ingestTaskFinished'] = extraMetrics.ingestTaskRequestsFinished + extraMetrics.ingestTaskRequestsFinishedWithError + extraMetrics.ingestTaskRequestFailure;
        d['mapRate'] = extraMetrics.scanRunningThroughput;
        d['ingestRate'] = extraMetrics.ingestRunningThroughput;
      }

      if (d['diskSizeWarning']) {
        currentDataCenter['diskSizeWarning'] = true;
      }
      currentDataCenter.scanTaskFinished = currentDataCenter.scanTaskFinished +  ( d['scanTaskFinished'] ? d['scanTaskFinished'] : 0);
      currentDataCenter.ingestTaskFinished = currentDataCenter.ingestTaskFinished +  ( d['ingestTaskFinished'] ? d['ingestTaskFinished'] : 0 );
      currentDataCenter.mapRate = currentDataCenter.mapRate +  ( d['mapRate'] ? d['mapRate'] : 0 );
      currentDataCenter.ingestRate = currentDataCenter.ingestRate +  ( d['ingestRate'] ? d['ingestRate'] : 0 ) ;
    };

    var buildBrokerSubTree = function(d, serverSummaryComp, extraMetrics, filterBySearchDateTime) {
      if(_.isObject(extraMetrics)) {
        addAppBroker(d, extraMetrics, serverSummaryComp,filterBySearchDateTime);
      }
    };


    var buildSolrNodeRoot = (): Operations.DtoSummaryServerComponent => {
      let currentSolrRoot= new Operations.DtoSummaryServerComponent();
      (<any>currentSolrRoot).id =  2000000;
      (<any>currentSolrRoot).elementId = 2000000;
      (<any>currentSolrRoot).parents = [ 2000000];
      (<any>currentSolrRoot).claComponentDto = new Operations.DtoClaComponent();
      (<any>currentSolrRoot).claComponentDto.claComponentType = Operations.EClaComponentType.SOLR_ROOT_NODE;
      (<any>currentSolrRoot).mainTitle = 'Solr Nodes';
      (<any>currentSolrRoot).mainTitleBubble = 'Solr node root folder';
      (<any>currentSolrRoot).isStatePending = true;
      (<any>currentSolrRoot).isSolrRootNode  = true;
      return currentSolrRoot;
    }

    var addStateParams = function(d, target, filterBySearchDateTime) {
      if(d.claComponentDto.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR && filterBySearchDateTime!= null && filterBySearchDateTime!= -1 && (d.statusTimestamp===null || d.statusTimestamp=== undefined)) {
        target.isStateNone = true;
        target.isStatePending = false;
        target.isStateError = false;
        target.isStateOk = false;
        target.claComponentDto.state = Operations.EComponentState.NA;
      }
      else if(d.claComponentDto.active) {
        target.isStateNone = false
        target.isStatePending = d.claComponentDto.state == Operations.EComponentState.PENDING_INIT;
        target.isStateError = d.claComponentDto.state ==  Operations.EComponentState.FAULTY;
        target.isStateOk = d.claComponentDto.state == Operations.EComponentState.OK || d.claComponentDto.state == Operations.EComponentState.STARTING;
        if(d.claComponentDto.claComponentType == Operations.EClaComponentType.BROKER && (<any>d).isStateError ) {
          target.isBrokerStatusError = true;
        }
      } else {
        target.isStateNone = false;
        target.isStateDisabled = true;
        target.isStatePending = false;
        target.isStateError = false;
        target.isStateOk = false;
        target.claComponentDto.state = "Disabled";
      }
    }


    var addAppBroker = function(d, extraMetrics, serverSummaryComp,filterBySearchDateTime) {
      let appBrokerInfo = <any>{};
      let startDateNumber = Date.now();
      if (filterBySearchDateTime !== null && filterBySearchDateTime !== undefined && filterBySearchDateTime !== -1) {
        startDateNumber = filterBySearchDateTime;
      }

      _.forEach(extraMetrics, (value, key) => {
        if(key.indexOf('_responses') > 0) {
          let res = key.split('_');
          appBrokerInfo[res[0]] = value
        }
      });
      if(!_.isEmpty(appBrokerInfo)) {
        let claComponentDto = new Operations.DtoClaComponent();
        claComponentDto.id = 1000000+d.id;
        claComponentDto.claComponentType = Operations.EClaComponentType.BROKER_FC;
        claComponentDto.state = d.claComponentDto.state;

        let appBroker:any  = {
          isBrokerInfo: true,
          isBrokerRC: true,
          id:  1000000+'_appBroker_' + d.id,  //unique ID is needed for treelist
          elementId : d.id,
          parentId: d.parentId,
          parents: [ d.id ],
          claComponentDto: claComponentDto,
          mainTitle: 'Broker - File cluster',
          mainTitleBubble: 'Broker - File cluster',
          scanAverageEnqueueTime: appBrokerInfo.scan.AverageEnqueueTime,
          ingestAverageEnqueueTime: appBrokerInfo.ingest.AverageEnqueueTime,
          scanDequeueCount: appBrokerInfo.scan.DequeueCount,
          ingestDequeueCount: appBrokerInfo.ingest.DequeueCount,
          scanEnqueueCount: appBrokerInfo.scan.EnqueueCount,
          ingestEnqueueCount: appBrokerInfo.ingest.EnqueueCount
        };
        if(d.statusTimestamp) {
          appBroker.statusTimestamp = d.statusTimestamp;
          appBroker.lastResponseDateDate = new Date(appBroker.statusTimestamp);
          appBroker.lastResponseElapsedTime = startDateNumber - appBroker.lastResponseDateDate;
          appBroker.lastResponseElapsedTimeSpan= $filter('formatSpan')(appBroker.lastResponseElapsedTime);
        }

        if(d.claComponentDto.stateChangeDate && (filterBySearchDateTime === null || filterBySearchDateTime === undefined || filterBySearchDateTime === -1)) {
          appBroker.stateChangeDateDate = new Date(d.claComponentDto.stateChangeDate);
          appBroker.lastStateChangeElapsedTime = startDateNumber - (<any>d).stateChangeDateDate;
          appBroker.lastStateChangeElapsedTimeSpan= $filter('formatSpan')((<any>d).lastStateChangeElapsedTime);
        }

        addStateParams(d,appBroker,filterBySearchDateTime);
        serverSummaryComp.push(appBroker);
      }

      return {};
    };

    var handleSelectionChange=function(e)
    {
      var treelist = $element.data("kendoTreeList");
      var selectedRows =   treelist.select();
      var selectedDataItems = [];
      for (var i = 0; i < selectedRows.length; i++) {
        var dataItem = treelist.dataItem(selectedRows[i]);
        selectedDataItems.push(dataItem);
      }
      $timeout(function() {
        $scope.$applyAsync(function () {

          $scope.handleSelectionChange(selectedDataItems);
        });
      });
    };


    var getMainTitleBubble = function(dataItem) {
      let localNetAddressHtml = '';

      if(dataItem.claComponentDto.localNetAddress) {
        localNetAddressHtml = `<div style='padding-top:12px;'>
        <div style='font-weight: 700; padding-bottom: 5px'>Local Net Address:</div>
        <div>${dataItem.claComponentDto.localNetAddress}</div>
        </div>`
      }
      return `<div>
        <div>${dataItem.mainTitle}</div>
        ${localNetAddressHtml}
      </div>`
    };

    var onSelectedItemChanged = function() {
      if ($scope.itemSelected) {
        $scope.itemSelected($scope.selectedItem.elementId, $scope.selectedItem.mainTitle, $scope.selectedItem[claComponentType.fieldName],$scope.selectedItem.claComponentDto,$scope.selectedItem.dataCenterId,$scope.selectedItem.id)
      };
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('serverComponentsTreeList', function(){
    return {
      restrict: 'EA',
      template:  '<div class=" {{elementName}}" kendo-tree-list ng-transclude k-on-data-binding="dataBinding(e,r)"   ' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      replace: true,
      transclude:true,
      scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          restrictedEntityDisplayTypeName: '@',
          localDataBound: '=',
          isBrokerStatusError: '=',
          findId: '=',
          filterBySearchDateTime: '=',
          fixHeight: '@',

        },
      controller: 'serverComponentsTreeListCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

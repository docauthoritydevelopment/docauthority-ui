///<reference path='../../../common/all.d.ts'/>

'use strict';
/* global kendo */


angular.module('operations.serverComponents.monitor', ['directives.dialogs.dateTimePicker'
])
  .config(function ($stateProvider) {

    $stateProvider
      .state(ERouteStateName.serverComponentsMonitor, {
        parent: 'operations.monitor',
        url: '/serverComponents?&serverComponentsPage?&historyPage?&selectedServerComponentId?&selectedHistoryId?&findIdComponent?&statusDateTime?',
        templateUrl: '/app/operations/serverComponents/serverComponentsMonitorPage.tpl.html',
        controller: 'MonitorServerComponentsPageCtrl',
        data: {
          authorizedRoleNames: [Users.ESystemRoleName.MngScanConfig, Users.ESystemRoleName.RunScans]
        }
      })

  })
  .controller('MonitorServerComponentsPageCtrl', function ($window, $rootScope, $scope, $element, $state, $stateParams, $compile, $filter, $timeout, scanService: IScanService, serverComponentResource: IServerComponentResource,
                                                           Logger: ILogger, propertiesUtils, configuration: IConfig, pageTitle, dialogs, eventCommunicator, userSettings: IUserSettingsService,routerChangeService,
                                                           rootFoldersResource: IRootFoldersResource, currentUserDetailsProvider: ICurrentUserDetailsProvider, $http) {
    var log: ILog = Logger.getInstance('MonitorServerComponentsPageCtrl');

    var resolutionsToText = {
      MINUTES:  60 * 1000,
      HOURS: 60 * 60 * 1000,
      DAYS: 60 * 60 * 1000 * 24,
      WEEKS: 60 * 60 * 1000 * 24 * 7,
      MONTHS: 60 * 60 * 1000 * 24 * 30,
      QUARTERS: 60 * 60 * 1000 * 24 * 364 / 4,
      YEARS: 60 * 60 * 1000 * 24 * 365
    };

    $scope.changeServerComponentsPage = $stateParams.serverComponentsPage && propertiesUtils.isInteger($stateParams.serverComponentsPage) ? parseInt($stateParams.serverComponentsPage) : null;
    $scope.changeServerComponentStatusPage = $stateParams.historyPage && propertiesUtils.isInteger($stateParams.historyPage) ? parseInt($stateParams.historyPage) : null;
    $scope.changeServerComponentSelectedItemId = $stateParams.selectedServerComponentId;
    $scope.changeServerComponentStatusSelectedItem = $stateParams.selectedHistoryId;
    $scope.statusDateTime = ($stateParams.statusDateTime !== null &&  $stateParams.statusDateTime !== undefined) ? Number($stateParams.statusDateTime) : -1;
    $scope.findIdComponent = $stateParams.findIdComponent;
    $scope.EClaComponentType = Operations.EClaComponentType;
    $scope.showDateFilter = false;

    if ($scope.findIdComponent && $scope.findIdComponent != '') {
      $scope.changeServerComponentsPage = null;
      $stateParams.findIdComponent = '';

      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: true
        });
    }

    $scope.showServerComponentsGrid = true;
    $scope.showServerComponentsStatusGrid = true;
    $scope.dateTimeFormat = configuration.dateTimeFormat;
    $scope.inputResolutionStep = null;
    $scope.viewResolutionStep = 0;
    $scope.inputMin =  0;
    $scope.inputStep =  15;
    $scope.resolutionType = 'MINUTES';
    $scope.resolutionTypeMS = resolutionsToText[$scope.resolutionType];

   $scope.openChangeDateDialog = function () {
      var dlg = dialogs.create(
        'common/directives/dialogs/pickDateAndTimeDialog.tpl.html',
        'pickDateAndTimeDialogCtrl',
        {
          theDate: ($scope.statusDateTime > -1) ? $scope.statusDateTime : (new Date()).getTime()
        },
        {size: 'md'});

      dlg.result.then(function (data) {
        $scope.statusDateTime = data.theDate;
        $stateParams['statusDateTime'] = $scope.statusDateTime;
        $scope.updateResolutionType();
        $state.transitionTo($state.current, $stateParams, {
          notify: false, inherit: true
        });
      }, function () {

      });
    };

    $scope.updateResolutionType = function(baseUnit) {

      // if($scope.statusDateTime > -1) {
      //   let dateDiff = new Date(Date.now()).getTime() - $scope.statusDateTime;
      //   let key = _.some(_.keys(resolutionsToText).reverse(), (key)=> {
      //     if(dateDiff >= resolutionsToText[key]) {
      //       $scope.changeResolutionType(key, true);
      //       $scope.changeResolutionStep(Math.floor(dateDiff / resolutionsToText[key]), true);
      //       return true;
      //     }
      //   });
      // }
     };

    $scope.changeResolutionType = function(baseUnit, ignoreUpdate) {

      $scope.resolutionType = baseUnit || 'MINUTES';
      $scope.inputStep = $scope.resolutionType === 'MINUTES' ? 15 : 1;
      $scope.inputMin =  $scope.resolutionType === 'MINUTES' ? 0 : 1;

      $scope.viewResolutionStep = 0;
      $scope.inputResolutionStep = null;

      $stateParams['resolutionType'] = $scope.resolutionType;
      $stateParams['statusDateTime'] = null;
      $scope.resolutionTypeMS = resolutionsToText[$scope.resolutionType];
      $state.transitionTo($state.current, $stateParams, {notify: true, inherit: true});

      $timeout(function () {
        $('.sc-input-resolution-step').focus();
      }, 30);
    };

    $scope.onCaretUp = function(){
      $scope.changeResolutionStep($scope.viewResolutionStep + $scope.inputStep)
    }

    $scope.onCaretDown = function(){
      let num = $scope.viewResolutionStep - $scope.inputStep;
      if(num >= $scope.inputMin) {
        $scope.changeResolutionStep(num);
      } else {
        $scope.changeResolutionStep(0);
      }
    }

    $scope.changeResolutionStep = function (resolutionStep, doUpdate) {

      function isNormalInteger(str) {
        let reg = /^\d+$/;
        return reg.test(str);
      }

      resolutionStep = isNormalInteger(resolutionStep) ? _.toInteger(resolutionStep): $scope.viewResolutionStep;

      $scope.viewResolutionStep = resolutionStep;
      $scope.inputResolutionStep = $scope.viewResolutionStep;

      if(!doUpdate) {
        updateServerComponent();
      }
    };

    var updateServerComponent = _.debounce(function (e, elm, title) {
      let dateObj = Date.now();
      let newDateObj = dateObj - (($scope.resolutionTypeMS || 0) * ($scope.viewResolutionStep || 0));

      $scope.statusDateTime = new Date(newDateObj).getTime();
      $stateParams['statusDateTime'] = $scope.statusDateTime > 0 ? $scope.statusDateTime : null;

      $state.transitionTo($state.current, $stateParams, {notify: false, inherit: true});
    }, 200);

    $scope.init = function () {
      pageTitle.set('Scan > Monitor system components');
      scanService.forceRunStatusRefresh(true);

      $scope.$on(scanService.serverComponentsChangedEvent, function (sender, value) {
        getServerComponentsStatusCompleted(scanService.getServerComponentsStatus());
        // refreshGrids();
      });
      $timeout(refreshGrids, configuration.polling_server_components_interval_idle);
      scanService.forceServerComponentsRefresh();

      setUserRoles();

      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });

      $scope.updateResolutionType();
    };

    var setUserRoles = function () {
      $scope.isRunScansUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
      $scope.isMngScanConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
    }

    var refreshGrids = function () {
      if ($scope.syncServerComponentData) {
        $scope.syncServerComponentData();
      }
      if ($scope.syncServerComponentStatusData && $scope.selectedServerComponentId) {
        $scope.syncServerComponentStatusData();
      }
      $timeout(refreshGrids, configuration.polling_server_components_interval_idle);
    }
    var lastServerComponentGridType;
    var setStatusGrid = function () {
      let html = `<server-components-status-grid fix-height="600px"  custom-data-center-id="selectedDataCenterId" restricted-entity-display-type-name="${$scope.selectedServerComponentType}" items-selected="onSelectServerComponentsStatus" item-selected="onSelectServerComponentStatus" change-page="changeServerComponentsStatusPage" export-to-excel="exportServerComponentsStatusToExcel"
        restricted-entity-id="selectedServerComponentId"  page-changed="serverComponentStatusPageChanged"   change-selected-item="changeServerComponentStatusSelectedItem" total-elements="serverComponentStatusTotalElements"
        find-id="findIdServerStatusComponent"  refresh-data="reloadServerComponentsData" refresh-data-silently="syncServerComponentStatusData" reload-data-to-grid="refreshServerComponentStatusDisplay"
        filter-data="filterDataStatus" process-export-finished="exportServerComponentsStatusToExcelCompleted" filter-by-search-date-time="statusDateTime"  ></server-components-status-grid>`;

      if ($scope.selectedServerComponentId && lastServerComponentGridType != $scope.selectedServerComponentType) {
        angular.element('.server-components-status-wrapper').empty();
        var elmStatusGrid = $compile(html)($scope);
        angular.element('.server-components-status-wrapper').append(elmStatusGrid);
        lastServerComponentGridType = $scope.selectedServerComponentType;
      }
    };

    var getServerComponentsStatusCompleted = function (serverComponentsSummary) {
      $scope.serverComponentsSummary = serverComponentsSummary;
    }

    $scope.onSelectServerComponent = function (componentId, mainTitle, path, item: Operations.DtoClaComponent, dataCenterId,id) {
      $scope.selectedServerComponentId = componentId;
      $scope.selectedDataCenterId = dataCenterId;
      $scope.selectedServerComponentName = mainTitle;
      $scope.selectedServerComponent = item;
      $scope.selectedServerComponentType = ( item ? item.claComponentType : null);


      $stateParams['selectedServerComponentId'] = id;

      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: false
        });

      setStatusGrid();
    }

    $scope.isAppServerSelected = function () {
      return $scope.selectedServerComponentType === Operations.EClaComponentType.APPSERVER;
    };
    $scope.isBrokerSelected = function () {
      return $scope.selectedServerComponentType === Operations.EClaComponentType.BROKER;
    };
    $scope.isMediaProcessorSelected = function () {
      return $scope.selectedServerComponentType === Operations.EClaComponentType.MEDIA_PROCESSOR;
    };

    $scope.onSelectServerComponents = function (data) {
      $scope.selectedServerComponents = data;
    };

    $scope.openEditServerComponentDialog = function (serverComponent: Operations.DtoClaComponent) {
      serverComponentResource.getServerComponent(serverComponent.id, function (sComponent) {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/editServerComponentDialog.tpl.html',
          'editServerComponentDialogCtrl',
          {serverComponent: sComponent},
          {size: 'md'});

        dlg.result.then(function (updatedItem: Operations.DtoClaComponent) {
          editItem(updatedItem);
        }, function () {

        });
      }, onError);
    };

    $scope.removeDateFilter = function() {
      $scope.statusDateTime = -1;
      $stateParams['statusDateTime'] = null;
      $state.transitionTo($state.current, $stateParams, {notify: true, inherit: true});
    };

    var editItem = function (updatedItem: Operations.DtoClaComponent) {
      if (updatedItem) {
        log.debug('edit customerDataCenterDto ');
        serverComponentResource.updateServerComponent(updatedItem, function (editedServerComponent) {
          $scope.serverComponentsReloadData([updatedItem.id]);
        }, onError);
      }
    }


    $scope.onExportServerComponentsToExcel = function () {
      $rootScope.$broadcast('exportServerComponentTreeToExcel');
    };

    $scope.onExportServerComponentsStatusToExcel = function () {
      let gotALotOfRecords = false;
      let exportName = 'filecluster_component_history';
      if ($scope.selectedServerComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR) {
        exportName = 'mp_component_history';
      }
      else if ($scope.selectedServerComponentType == Operations.EClaComponentType.DB) {
        exportName = 'db_component_history';
      }
      else if ($scope.selectedServerComponentType == Operations.EClaComponentType.BROKER || $scope.selectedServerComponentType == Operations.EClaComponentType.BROKER_DC || $scope.selectedServerComponentType == Operations.EClaComponentType.BROKER_FC) {
        exportName = 'broker_component_history';
      }
      else if ($scope.selectedServerComponentType == Operations.EClaComponentType.SOLR_NODE) {
        exportName = 'solr_node_component_history';
      }
      else if ($scope.selectedServerComponentType == Operations.EClaComponentType.SOLR_SIM || $scope.selectedServerComponentType == Operations.EClaComponentType.SOLR_ROOT_NODE || $scope.selectedServerComponentType == Operations.EClaComponentType.SOLR_SIMILARITY || $scope.selectedServerComponentType == Operations.EClaComponentType.SOLR_CLAFILE) {
        exportName = 'solr_collection_component_history';
      }
      else if ($scope.selectedServerComponentType == Operations.EClaComponentType.ZOOKEEPER) {
        exportName = 'zookeeper_component_history';
      }
      routerChangeService.addFileToProcess("Status_of_"+ $scope.selectedServerComponentName+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),exportName ,{timestamp: $scope.statusDateTime && $scope.statusDateTime >0 ? $scope.statusDateTime : '', componentId : $scope.selectedServerComponentId, dataCenterID : $scope.selectedDataCenterId},{},gotALotOfRecords);
    };

    $scope.serverComponentPageChanged = function (pageNumber: number) {
      $stateParams['serverComponentsPage'] = pageNumber;
      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: true
        });
    }


    $scope.onSelectServerComponentStatusItem = function (id, path, item) {
      $scope.selectedServerStatusId = id;
      $scope.selectedServerStatusName = path;
      $scope.selectedServerStatus = item;

      $stateParams['selectedHistoryId'] = $scope.selectedServerStatusId;

      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: false
        });


    }

    $scope.serverComponentStatusPageChanged = function (pageNumber: number) {
      $stateParams['historyPage'] = pageNumber;
      $state.transitionTo($state.current,
        $stateParams,
        {
          notify: false, inherit: true
        });
    }
    $scope.onSelectServerComponentStatusItems = function (data) {
      $scope.selectedServerStatusItems = data;
    }



    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridServerComponentsUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {
        /*
         * fireParam object:
         *   { initSearch: false, value: searchText, mode: mode}
         */
        if (fireParam.action === 'openEditServerComponentItemDialog') {
          $scope.openEditServerComponentDialog(fireParam.serverComponentItem);
        }

      })));

    $scope.$on('$destroy', function () {
      scanService.forceRunStatusRefresh(false);
      eventCommunicator.unregisterAllHandlers($scope);

    });


    var onError = function () {
      dialogs.error('Error', 'Sorry, an error occurred.');
    }

  });


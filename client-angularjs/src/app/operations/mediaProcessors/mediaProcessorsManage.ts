///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.mediaProcessors.list',[

  ])
  .controller('mediaProcessorsManageCtrl', function ($window, $scope, dialogs, Logger, serverComponentResource, eventCommunicator, currentUserDetailsProvider:ICurrentUserDetailsProvider,routerChangeService,$filter,configuration) {

    var log:ILog = Logger.getInstance('mediaProcessorsManageCtrl');

    $scope.lazyLoad = true;
    $scope.hideImport = true;

    $scope.init=function() {

      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
    };

    var setUserRoles = function()
    {
      $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
    }


    $scope.onExportToExcel = function () {
      let gotALotOfRecords = false;
      routerChangeService.addFileToProcess('Media_processors_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'system_components_settings' ,{customerDataCenterId : $scope.selectedDataCenterItem.id},{},gotALotOfRecords);
    };


    $scope.onSelectServerComponents = function(data:Operations.DtoClaComponent) {
      $scope.selectedListItemsToOperate = data;
    };
   $scope.onSelectServerComponent = function(id,path,item:Operations.DtoClaComponent){
        $scope.selectedListItemToOperate = item;
    };

    $scope.openNewItemDialog = function() {
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/editServerComponentDialog.tpl.html',
        'editServerComponentDialogCtrl',
        {dataCenterItem:$scope.selectedDataCenterItem, mediaProcessorOnly: true},
        {size: 'md'});

      dlg.result.then(function (claComponent:Operations.DtoClaComponent) {
        $scope.refreshData([claComponent.id]);
      }, function () {

      });
    };

    $scope.openEditItemsDialog = function(serverComponent: Operations.DtoClaComponent) {

      var dlg = dialogs.create(
        'common/directives/operations/dialogs/editServerComponentDialog.tpl.html',
        'editServerComponentDialogCtrl',
        {serverComponent:serverComponent, mediaProcessorOnly: true},
        {size: 'md'});

      dlg.result.then(function (claComponent:Operations.DtoClaComponent) {
        $scope.refreshData([claComponent.id]);
      }, function () {

      });
    };

    $scope.openDeleteItemsDialog = function() {
      var selectedItem = <Operations.DtoClaComponent>$scope.selectedListItemToOperate;
      if(selectedItem.claComponentType != Operations.EClaComponentType.MEDIA_PROCESSOR ) {
        dialogs.notify('Note', 'Only media processor can be deleted. ');
        return;
      }
      if((selectedItem.state == Operations.EComponentState.STARTING || selectedItem.state == Operations.EComponentState.OK) && selectedItem.active )
      {
        dialogs.notify('Note', 'Cannot delete active media processor. ');
        return;
      }
        let userMsg = "You are about to delete media processor named: '" + selectedItem.instanceId + (selectedItem.location ? " (" + selectedItem.location + ")" : "") + "'. Continue?";
        let dlg = dialogs.confirm('Confirmation', userMsg, null);

        dlg.result.then(function (btn) { //user confirmed deletion
          log.debug('delete server component (media processor): ' + selectedItem.id);
          serverComponentResource.deleteServerComponent(selectedItem.id, function () {
            $scope.refreshData();
          },function() {
            dialogs.error('Error','Sorry, an error occurred.');
          });
        }, function (btn) {

        });
    }

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridServerComponentsUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'openEditServerComponentItemDialog') {
          $scope.openEditItemsDialog(fireParam.serverComponentItem);
        }
      })));

    $scope.$on('$destroy', function(){
      eventCommunicator.unregisterAllHandlers($scope);
    });
  })
  .directive('mediaProcessorsManage', function(){
    return {
      restrict: 'EA',
      templateUrl: '/app/operations/mediaProcessors/mediaProcessorsManage.tpl.html',
      controller: 'mediaProcessorsManageCtrl',
      replace:true,
      scope: {
        selectedDataCenterItem: '=',
      },
      link: function (scope:any, element, attrs) {
        scope.init();
      }
    }
  });

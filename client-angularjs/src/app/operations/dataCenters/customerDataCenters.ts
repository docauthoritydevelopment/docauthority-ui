///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.customerDataCenters.list',[

  ])
  .controller('customerDataCentersGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('customerDataCentersGridCtrl');


    $scope.elementName = 'customer-data-centers-grid';
    var gridOptions:ui.IGridHelper;

    $scope.init = function (restrictedEntityDisplayTypeName) {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
        fields, parseData, rowTemplate,
        $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

      var doNotSelectFirstRowByDefault = false;
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

    };
    var setUserRoles = function()
    {
      $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
    }
    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
      return configuration.customerDataCenter_summary_info_url;
    }

    var getDataUrlParams = function () {
      return null;
    };


    var dtoCustomerDataCenterSummaryInfo:Operations.DtoCustomerDataCenterSummaryInfo =  Operations.DtoCustomerDataCenterSummaryInfo.Empty();
     var dtoCustomerDataCenter:Operations.DtoCustomerDataCenter =  Operations.DtoCustomerDataCenter.Empty();
    var dtoCustomerDataCenterPrefix =  propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.customerDataCenterDto)+'.';

    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:'id',
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      width:'35px'
    };
    var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name),
      title: 'Name',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
     };
    var nameDisplayTemplate =  '<da-icon href="dataCenter"></da-icon>&nbsp;<span title="{{dataItem.'+dtoCustomerDataCenterPrefix+ propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.description)+'}}" >'+
      '#= '+dtoCustomerDataCenterPrefix+ propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name)+' #</span>';
    var nameTemplate = '<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem,\'general\')" class="title-primary">'+nameDisplayTemplate+ '</a> ';
    var disableNameTemplate = '<span ng-if="!isMngScanConfigUser"  class="title-primary">'+nameDisplayTemplate+ '</span> ';

    name.template=disableNameTemplate+nameTemplate;

    var location:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.location),
      title: 'Location',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
     };
    var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.description),
      title: 'Description',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: true,
     };
    var defaultDataCenter:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.defaultDataCenter),
      title: 'Default',
      type: EFieldTypes.type_boolean,
      displayed: true,
      editable: false,
      nullable: true,
      width:'100px',
      template:'<span ng-if="dataItem.'+dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.defaultDataCenter)+'"><span class="primary fa fa-check"></span></span>'
     };
    var createdOnDB:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  dtoCustomerDataCenterPrefix+propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.createdOnDB)+'Date',
      title: 'Created on DB',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}'
    };
    var mediaProcessorCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.mediaProcessorCount),
      title: 'Media processors',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      centerText: true,
      width:'120px',

    };

    mediaProcessorCount.template = "<span ng-if='dataItem."+mediaProcessorCount.fieldName+"==0'><span class='fa fa-times notice'></span></span>"+
        "<span class='title-secondary' ng-if='dataItem."+mediaProcessorCount.fieldName+">0' >{{dataItem."+mediaProcessorCount.fieldName+"}}</span>";

    var rootFolderCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.rootFolderCount),
      title: 'Root folders',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: true,
      centerText: true,
      noWrapContent: true,
      width:'100px',
      template:'<span ng-if="dataItem.'+propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.rootFolderCount)+'>0" title="#: '+propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.rootFolderCount)+' #&nbsp;root-folders"> \
        <da-icon class="icon-item"  href="rootfolder"></da-icon> \
        &nbsp;#: '+propertiesUtils.propName(dtoCustomerDataCenterSummaryInfo, dtoCustomerDataCenterSummaryInfo.rootFolderCount)+' #</span>'
    };

    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(name);
    fields.push(defaultDataCenter);
    fields.push(description);
    fields.push(mediaProcessorCount);
    fields.push(rootFolderCount);
    fields.push(location);
    fields.push(createdOnDB);



    var rowTemplate="";

    var createDateAsUTC = function(date) {
      return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
    }

    var convertDateToUTC = function(date) {
      return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    }

    var parseData = function (centres: Operations.DtoCustomerDataCenterSummaryInfo[]) {
      centres.forEach(d=> {
        (<any>d).id = d.customerDataCenterDto.id;
        if (d.customerDataCenterDto.createdOnDB) {
          let tmpDateString: any = d.customerDataCenterDto.createdOnDB+'';
          if (tmpDateString.indexOf('+') > -1) {
            tmpDateString = tmpDateString.substring(0, tmpDateString.indexOf('+'));
            (<any>d).customerDataCenterDto.createdOnDBDate = createDateAsUTC(new Date( tmpDateString));
          }
          else {
            (<any>d).customerDataCenterDto.createdOnDBDate = new Date( tmpDateString);
          }

        }
      });
      return centres;
    }


    var onSelectedItemChanged = function()
    {

      $scope.itemSelected?$scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem):null;
    }

    $scope.openEditDialog = function(dataItem:any) {
      if($scope.isMngScanConfigUser) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridCustomerDataCenterUserAction, {
          action: 'openEditDialog',
          dataCenterItem: dataItem,
        });
      }
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

  })
  .directive('customerDataCentersGrid', function(){
    return {
      restrict: 'EA',
      template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      replace: true,
      transclude:true,
      scope:
      {
        totalElements:'=',
        lazyFirstLoad: '=',
        restrictedEntityId:'=',
        itemSelected: '=',
        itemsSelected: '=',
        filterData: '=',
        sortData: '=',
        unselectAll: '=',
        exportToPdf: '=',
        exportToExcel: '=',
        templateView: '=',
        reloadDataToGrid:'=',
        refreshData:'=',
        pageChanged:'=',
        changePage:'=',
        processExportFinished:'=',
        getCurrentUrl: '=',
        getCurrentFilter: '=',
        includeSubEntityData: '=',
        changeSelectedItem: '=',
        refreshDataSilently: '=',
        localDataBound: '=',
        findId: '=',

        fixHeight: '@',

      },
      controller: 'customerDataCentersGridCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

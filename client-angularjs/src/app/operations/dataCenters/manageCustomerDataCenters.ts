///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


var manageCustomerDataCenters  = function(): ng.IDirective {
  return <ng.IDirective> {
    bindToController: {
      selectItem: '=',
      changeDataCenterSelectedItem: '=',
      onSelectedItem: '=',
      excelFileName: '=',
      exportName: '='
    },
    scope:true,
    templateUrl: '/app/operations/dataCenters/manageCustomerDataCenters.tpl.html',
    replace:true,
    controllerAs:'manage_dc',
    controller:ManageScheduleGroupsController
  }
}

angular.module('operations.manageCustomerDataCenters.manage', [


]).directive('manageCustomerDataCenters', manageCustomerDataCenters)

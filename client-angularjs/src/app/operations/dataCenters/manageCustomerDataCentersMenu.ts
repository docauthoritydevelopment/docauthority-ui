///<reference path='../../../common/all.d.ts'/>

'use strict';


angular.module('directives.customerDataCenter.manage.menu', [
    'resources.operations',
    'directives.dialogs.customer.dataCenter.new',

  ])
  .controller('customerDataCenterMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,customerDataCenterResource:ICustomerDataCenterResource,
                                                       userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs) {
    var log:ILog = Logger.getInstance('customerDataCenterMenuCtrl');

    $scope.init = function () {

    };

    $scope.openNewItemDialog = function()
    {
      var dlg = dialogs.create(
        'common/directives/operations/dialogs/newDataCenterDialog.tpl.html',
        'newDataCenterItemDialogCtrl',
        {},
        {size: 'md'});

      dlg.result.then(function (dataCenter:Operations.DtoCustomerDataCenter) {
        $scope.reloadData([dataCenter.id]);
      }, function () {

      });

    }


    $scope.openEditItemsDialog = function(listItem:Operations.DtoCustomerDataCenterSummaryInfo)
    {
      var item:Operations.DtoCustomerDataCenterSummaryInfo=listItem?listItem: $scope.selectedListItemsToOperate[0];

            var dlg = dialogs.create(
            'common/directives/operations/dialogs/newDataCenterDialog.tpl.html',
            'newDataCenterItemDialogCtrl',
            {dataCenter: item.customerDataCenterDto},
            {size: 'md'});

          dlg.result.then(function (updatedItem:Operations.DtoCustomerDataCenter) {
            $scope.reloadData([updatedItem.id]);
          }, function () {

          });


    }

    $scope.openDeleteItemsDialog = function()
    {
      var items:Operations.DtoCustomerDataCenterSummaryInfo[]=$scope.selectedListItemsToOperate;
      if(items&& items.length>0) {
        var userMsg;
        if(items.filter(i=>i.customerDataCenterDto.defaultDataCenter)[0])
        {
          userMsg="Cannot delete the default data center.";
          dialogs.notify('Note', userMsg);
          return;
        }
        if(items.length==1)
        {
          userMsg="You are about to delete customer data center named: '"+items[0].customerDataCenterDto.name+"'. Continue ?";
        }
        else if(items.length<6){
          userMsg="You are about to delete " + items.length + " customer data centers. Continue?"+'<br>'+items.map(u=>u.customerDataCenterDto.name).join(', ');
        }
        else {
          userMsg="You are about to delete " + items.length + " customer data centers. Continue?";
        }
        var dlg = dialogs.confirm('Confirmation', userMsg, null);
        dlg.result.then(function (btn) { //user confirmed deletion
          items.forEach(d=>  deleteItem(d.customerDataCenterDto));

        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });
      }

    }

    var deleteItem = function(item:Operations.DtoCustomerDataCenter)
    {
      log.debug('delete DataCenter: ' + item.name);
      customerDataCenterResource.deleteDataCenter(item.id, function () {
        $scope.reloadData();
      },(err) => {
        if (err.status == 400 && err.data.type == ERequestErrorCode.ITEM_ATTACHED_TO_ANOTHER) {
          dialogs.error('Delete error', `Cannot delete '${item.name}'! <br/>Data Center is in use.`);
        }else{
          onError();
        }
      });
    };

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridCustomerDataCenterUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        /*
         * fireParam object:
         *   { initSearch: false, value: searchText, mode: mode}
         */
        if (fireParam.action === 'openEditDialog') {
          $scope.openEditItemsDialog(fireParam.dataCenterItem);
        }
      })));



    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });


  })
  .directive('customerDataCenterManageMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/manageMenu.tpl.html',
        controller: 'customerDataCenterMenuCtrl',
        replace:true,
        scope:{
          'selectedListItemsToOperate':'=',
          'selectedListItemToOperate':'=',
          'reloadData':'=',
          'gridFindId':'=',
          'refreshDisplay':'=',

        },
        link: function (scope:any, element, attrs) {
          scope.init(attrs.left);
          scope.hideImport = true;
        }
      };
    }
  )



///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.scheduleGroups.monitor',[

    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.activeScans, {
            parent:'operations.monitor',
            url: '/'+ERouteStateName.activeScans+'?&selectedScheduleGroupId?&rootFoldersPage?&scheduleGroupsPage?&selectedRootFolderId?&filterSortRootFolders?&selectedPhaseDataId?&rootFolderSearchText?&rootFolderSearchTextScheduleGroupId?',
            templateUrl: '/app/operations/activeScansPage.tpl.html',
            controller: 'ActiveScansPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngScanConfig,Users.ESystemRoleName.RunScans]
            }
          })

    })
    .controller('ActiveScansPageCtrl', function ($scope,$element,$state,$stateParams,$compile,$filter,$timeout,scanService:IScanService,scheduleGroupResource:IScheduleGroupResource,
                                                    Logger:ILogger,propertiesUtils, configuration:IConfig,pageTitle,dialogs,eventCommunicator,userSettings:IUserSettingsService,
                                                           rootFoldersResource:IRootFoldersResource,currentUserDetailsProvider:ICurrentUserDetailsProvider, activeScansResource:IActiveScansResource) {
      var log:ILog = Logger.getInstance('ActiveScansPageCtrl');
      var rootFolderIdToFind;

      $scope.changeScheduleGroupsPage= $stateParams.scheduleGroupsPage && propertiesUtils.isInteger( $stateParams.scheduleGroupsPage) ? parseInt( $stateParams.scheduleGroupsPage ) : null;
      $scope.changeRootFoldersPage= $stateParams.rootFoldersPage && propertiesUtils.isInteger( $stateParams.rootFoldersPage) ? parseInt( $stateParams.rootFoldersPage ) : null;
      $scope.rootFolderActionsEventName = EEventServiceEventTypes.ReportGridRootFoldersActiveScanActions;
      $scope.changeScheduleGroupSelectedItem=$stateParams.selectedScheduleGroupId;
      $scope.changeRootFolderSelectedItem=$stateParams.selectedRootFolderId;
      $scope.changePhaseDataSelectedItem=$stateParams.selectedPhaseDataId;
      $scope.showScheduleGroupsGrid =true;
      $scope.showRootFoldersGrid =true;
      $scope.showPhasesGrid =true;

      $scope.filterRootFoldersStatusOptions = [
      //  {  value:Operations.ERunStatus.NA,   selected: false },
        {  value:Operations.ERunStatus.FINISHED_SUCCESSFULLY,   selected: false },
        {   value:Operations.ERunStatus.FAILED, selected: false },
        {  value:Operations.ERunStatus.PAUSED,   selected: false },
        {  value:Operations.ERunStatus.RUNNING,selected: false },
        {  value:Operations.ERunStatus.STOPPED,selected: false },

      ];


      $scope.descRootFoldersDirection = null;

      var dtoRootFolder =  Operations.DtoRootFolder.Empty();

      $scope.sortRootFoldersOptions=[
        {value: propertiesUtils.propName(dtoRootFolder, dtoRootFolder.id),text:'creation'},
        {value: 'rootFolder.nickName',text:'nick name'},
        {value: 'rootFolder.realPath',text:'path'},
        {value: 'rootFolder.foldersCount',text:'folders count'},
        {value: 'rootFolder.mediaType',text:'media type'},
        {value: 'rootFolder.customerDataCenter.name',text:'data center'},
      ];

      var defaultSortField =  $scope.sortRootFoldersOptions[1]; //'nickName';

      var refreshRootFoldersGrid = function()
      {
        var filter =  $scope.filterRootFoldersStatusOptions.filter(s=>s.selected).map(d=>d.value);
        var sortData={dir:$scope.descRootFoldersDirection?'desc':'asc',field:$scope.sortRootFoldersSelectedOption.value};
        $scope.selectedRootFoldersStatusFilters = {filter:filter,sort: [sortData]};
        $stateParams['filterSortRootFolders'] = angular.toJson($scope.selectedRootFoldersStatusFilters);
        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
      }

      var setSelectedState = function() {
         _.defer(() => {
           let selectedState = $scope.filterRootFoldersStatusOptions.filter(s=>s.selected).map(d=>$filter('translate')(d.value));
           $scope.selectedState = _.isEmpty(selectedState) ? 'Status' : selectedState.join(', ');
           $scope.selectedStateTooltip =  _.isEmpty(selectedState) ? 'Filter by status' : 'Filtered by: ' + $scope.selectedState;
         });
      }

      var initRootFolderFilterAnSort = function () {
        setSelectedState();

        if ($stateParams.filterSortRootFolders && $stateParams.filterSortRootFolders.trim() != '') {
          try {
            var filterSort = angular.fromJson($stateParams.filterSortRootFolders);
            filterSort.filter.forEach(f=> {
              var theOption = $scope.filterRootFoldersStatusOptions.filter(o=> o.value == f)[0];
              theOption ? theOption.selected = true : null;
            })

            $scope.sortRootFoldersSelectedOption = $scope.sortRootFoldersOptions.filter(s=>filterSort.sort[0] && s.value == filterSort.sort[0].field)[0];
            $scope.descRootFoldersDirection =  filterSort.sort[0] &&filterSort.sort[0].dir=='asc'?false:  $scope.descRootFoldersDirection;

          }
          catch (e) {
            log.info('Faild to set filter and sorting from url')
          }

        }
        $scope.sortRootFoldersSelectedOption = $scope.sortRootFoldersSelectedOption ? $scope.sortRootFoldersSelectedOption :defaultSortField;
        $scope.descRootFoldersDirection =  $scope.descRootFoldersDirection ? $scope.descRootFoldersDirection: false;
        refreshRootFoldersGrid();
      }

      initRootFolderFilterAnSort();
      $scope.init=function()
      {
        scanService.forceRunStatusRefresh(true);

        pageTitle.set("Operations > monitor active scans");

        $scope.$on(scanService.activeRunsChangedEvent, function (sender, value) {
          refreshGrids();
          getActiveRunsStatusCompleted(scanService.getActiveRuns());
        });
        $scope.$on(scanService.runStatusOperationStateChangedEvent, function (sender, value) {
          refreshGrids(); //when state in top dashboard runs is changed, force refresh grids. In order to avoid state that grid rf is running and in the top all rf finished
        });
        $scope.$on(scanService.rootFoldersAggregatedSummaryInfoEvent, function (sender, value) {
          getRootFoldersAggregatedSummaryInfo(scanService.getRootFoldersAggregatedSummaryInfo());
        });

        //scanService.forceRunStatusRefresh(); //Do immediate refresh for display

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isRunScansUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
        $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
        $scope.isTechSupportUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.TechSupport]);
      }


      var getActiveRunsStatusCompleted = function(activeRuns:Operations.ActiveRunsView)
      {
        $scope.activeRuns=activeRuns.activeRunsView;
        $scope.isRunning = activeRuns.activeRuns.length>0;
      };

      var getRootFoldersAggregatedSummaryInfo = function(rootFoldersAggregatedSummaryInfo:Operations.DtoRootFoldersAggregatedSummaryInfo){
        $scope.rootFoldersAggregatedSummaryInfo=rootFoldersAggregatedSummaryInfo;
      }

      var refreshGrids=function()
      {
        if($scope.syncScheduleGroupData) {
          $scope.syncScheduleGroupData();
        }
     //   if($scope.selectedScheduleGroupId && $scope.syncRootFoldersData) { rootfolders grid is refreshed when schedule grid onDataBound
     //     $scope.syncRootFoldersData();
     //   }

        if( $scope.selectedRootFolderId && $scope.syncPhasesData) {
          $scope.syncPhasesData();
       }
      }

      $scope.toggleRootFoldersSortDir = function()
      {
        $scope.descRootFoldersDirection=!$scope.descRootFoldersDirection;
        refreshRootFoldersGrid();
      }
      $scope.sortRootFoldersBy = function(sortItem:any)
      {
        $scope.sortRootFoldersSelectedOption=sortItem;
        refreshRootFoldersGrid();
      }
      $scope.toggleRootFoldersStatusFilteredBy = function(event, option)
      {
        event.preventDefault();
        event.stopPropagation();

        option.selected=!option.selected;

        setSelectedState();
        refreshRootFoldersGrid();
      }

      $scope.reloadScheduleGroupAndRootFolderData = function(findScheduleId,findRootFolderId)
      {
        rootFolderIdToFind = findRootFolderId;
        if($scope.selectedScheduleGroupId == findScheduleId &&  $scope.selectedScheduleGroupId ) {
          $scope.reloadRootFoldersData(null,findRootFolderId);
        } else {
          $scope.reloadScheduleGroupsData(null,findScheduleId);
        }
      }

      $scope.rootFoldersLocalDataBound=function()
      {
        return findRootFolderIdIfNeeded();
      }

      $scope.locateRootFolderAtGrids = function(rootFolderId,scheduleGroupId)
      {
        $scope.showScheduleGroupsGrid =true;
        $scope.showRootFoldersGrid =true;
        $scope.showPhasesGrid =true;

        if(!scheduleGroupId)
        {
          rootFoldersResource.getRootFolderScheduleGroup(rootFolderId,function(scheduleGroup:Operations.DtoScheduleGroup)
          {
            scheduleGroupId = scheduleGroup.id;
            $scope.locateRootFolderAtGrids(rootFolderId,scheduleGroupId);
          },onError)
        }
        else {
          if($scope.selectedScheduleGroupId != scheduleGroupId) {
            rootFolderIdToFind = rootFolderId; //To be called after schedule group grid data bound
            $scope.reloadScheduleGroupsData(null,scheduleGroupId); //reload  in order to use findId
          }
          else {
            rootFolderIdToFind = rootFolderId; //To be called after schedule group grid data bound
            findRootFolderIdIfNeeded(); //called also after data to rootFolders grid is bound
          }
        }

      }
      var findRootFolderIdIfNeeded=function()
      {
        if(rootFolderIdToFind) {
          var rootFolderId = rootFolderIdToFind;
          rootFolderIdToFind = null;
          $scope.clearSearchText();
          $scope.searchRootFolders(); //clear search inorder to find rootfolder
          $scope.reloadRootFoldersData(null,rootFolderId);
          $('html, body').animate({scrollTop:$(".active-scan-root-folders-grid").offset().top - 120}, 600);
          return true;
        }
      }

      var updateRescanActive = function (rootFolder:Operations.DtoRootFolder,active:boolean) {

        rootFoldersResource.updateRootFolderRescanActive( rootFolder.id, active,function () {
          rootFolder.rescanActive = active;
          //$scope.refreshDisplay();
        }, onError);
      };

      $scope.onRootFolderEdit = function(ids,findId) {
        $scope.reloadRootFoldersData(ids);
      }

      $scope.scanSelectedRootFolders =function(isScan,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean)
      {
        if($scope.selectRootFolders&&$scope.selectRootFolders.length>0) {
           $scope.errorMsg = '';

          scanService.startRunRootFolders(
              $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.id),
            $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName),
              isScan,isCompleteScanNoExtract, isCompleteScanWithExtract, isExtractOnly);
        }
      };

      $scope.markForFullScan = function(mark:boolean){
        if($scope.selectedScheduleGroups&&$scope.selectedScheduleGroups.length>0) {

          scanService.markForFullScan($scope.selectedRootFolderId, mark);
        }
      };
      $scope.initScanStrategyData = function()
      {
        activeScansResource.getScanStrategyJobProcessingMode((jobProcessingMode:Operations.EJobProcessingMode) => {
          $scope.jobProcessingMode = _.filter(_.values(jobProcessingMode), (o) => {return _.isString(o)}).join('');  //TEMP ONLY!!! Data should returned in object
        },() => {
        });
      }
      $scope.scanSelectedScheduleGroups = function(isScan:boolean,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean)
      {
        if($scope.selectedScheduleGroups&&$scope.selectedScheduleGroups.length>0) {

          $scope.errorMsg = '';
          scanService.startRunScheduleGroups($scope.selectedScheduleGroups.map(s=>(<Operations.DtoScheduleGroupSummaryInfo>s).scheduleGroupDto.id),
              $scope.selectedScheduleGroups.map(s=>(<Operations.DtoScheduleGroupSummaryInfo>s).scheduleGroupDto.name),
              isScan, isCompleteScanNoExtract, isCompleteScanWithExtract, isExtractOnly);
        }
      };

      $scope.pauseSelectedRootFolderRuns = function()
      {
        if($scope.selectRootFolders&&$scope.selectRootFolders.length>0) {

          scanService.pauseRunsByID(
            $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).crawlRunDetailsDto.id),
            $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName?(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName:(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.path )
          );
        }
      }
      $scope.pauseSelectedScheduleGroupRuns = function()
      {
        if($scope.selectedScheduleGroups&&$scope.selectedScheduleGroups.length>0) {

          scanService.pauseRunsByID(
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).lastCrawlRunDetails.id),
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).scheduleGroupDto.name )
          );
        }
      }

      $scope.pauseSelectedJobs = function()
      {
        if($scope.selectedPhasesData&&$scope.selectedPhasesData.length>0) {
          var jobIDs: number[] =  $scope.selectedPhasesData.map(r=>(<Dashboards.DtoPhaseDetails>r).jobId);
          var names: string[] =   $scope.selectedPhasesData.map(r=>$filter('translate')('job_'+<Dashboards.DtoPhaseDetails>r.jobType )+' '+ (r.name?r.name:''));
          scanService.pauseJobsByID(jobIDs,names);
        }
      }

      $scope.resumeSelectedJobs = function()
      {
        if($scope.selectedPhasesData&&$scope.selectedPhasesData.length>0) {
          var jobIDs: number[] =  $scope.selectedPhasesData.map(r=>(<Dashboards.DtoPhaseDetails>r).jobId);
          var names: string[] =   $scope.selectedPhasesData.map(r=>$filter('translate')('job_'+<Dashboards.DtoPhaseDetails>r.jobType )+' '+ (r.name?r.name:''));
          scanService.resumeJobsByID(jobIDs,names);
        }
      }
      $scope.forceRerunEnqueuedTasksForSelectedJob = function()
      {
        if($scope.selectedPhasesData&&$scope.selectedPhasesData.length>0) {
          var jobIDs: number[] =  $scope.selectedPhasesData.map(r=>(<Dashboards.DtoPhaseDetails>r).jobId);
          var names: string[] =   $scope.selectedPhasesData.map(r=>$filter('translate')('job_'+<Dashboards.DtoPhaseDetails>r.jobType )+' '+ (r.name?r.name:''));
          scanService.forceRerunJobEnqueuedTasks(jobIDs,names);
        }
      }
      $scope.forceRerunEnqueuedTasksForRun = function()
      {
        if($scope.selectedRootFolderLastRunDetailsId) {
         var runId = $scope.selectedRootFolderLastRunDetailsId;
         var name =  $scope.selectedRootFolderNickName? $scope.selectedRootFolderNickName:$scope.selectedRootFolderName;
          scanService.forceRerunRunEnqueuedTasks(runId,name);
        }
      }

      $scope.continueRunSelectedRootFolder = function()
      {
        scanService.resumeRunsByID(
          $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).crawlRunDetailsDto.id),
          $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName?(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName:(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.path )
        );
      };

      $scope.continueRunSelectedScheduleGroups = function()
      {
        if($scope.selectedScheduleGroups&&$scope.selectedScheduleGroups.length>0) {

          scanService.resumeRunsByID(
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).lastCrawlRunDetails.id),
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).scheduleGroupDto.name )
          );
        }
      }
      $scope.stopRunSelectedScheduleGroups = function()
      {
        if($scope.selectedScheduleGroups&&$scope.selectedScheduleGroups.length>0) {

          scanService.stopRunsByID(
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).lastCrawlRunDetails.id),
            $scope.selectedScheduleGroups.map(r=>(<Operations.DtoScheduleGroupSummaryInfo>r).scheduleGroupDto.name )
          );
        }
      }
      $scope.stopRunSelectedRootFolder = function()
      {
        scanService.stopRunsByID(
          $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).crawlRunDetailsDto.id),
          $scope.selectRootFolders.map(r=>(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName?(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.nickName:(<Operations.DtoRootFolderSummaryInfo>r).rootFolderDto.path )
        );
      };
      $scope.toggleShowAllScheduleGroups = function()
      {
        $scope.showAllScheduleGroups = !$scope.showAllScheduleGroups;
      }


      $scope.pauseAllRuns = function()
      {
         scanService.pauseAllRuns( );
      };

      $scope.resumeAllRuns = function()
      {
         scanService.resumeAllRuns( );
      };

      $scope.onSelectScheduleGroup = function(id,path,item:Operations.DtoScheduleGroupSummaryInfo)
      {
        $scope.selectedScheduleGroupId = id;
        $scope.selectedScheduleGroupName = path;
        $scope.selectedScheduleGroup=item;
     //   $scope.selectedRootFolderLastRunDetailsId = null; //to reset phase grid when schedule is changing. causes auto refresh when rf grid refreshes, thus 2 sequential refreshes for jobs grid
        $scope.selectedScheduleGroupIsRunning = item? item.runStatus == Operations.ERunStatus.RUNNING:false;
        $scope.selectedScheduleGroupIsPaused = item? item.runStatus == Operations.ERunStatus.PAUSED:false;
        $scope.selectedScheduleGroupIsReadyForScan = (!$scope.selectedScheduleGroupIsRunning && !$scope.selectedScheduleGroupIsPaused) || !$scope.selectedScheduleGroupId;

        initRootFolderSearchTextIfNeeded();

        $stateParams['selectedScheduleGroupId'] = $scope.selectedScheduleGroupId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });


      }
      var initRootFolderSearchTextIfNeeded = function()
      {
        var rootFolderSearchTextScheduleGroupId =  $stateParams.rootFolderSearchTextScheduleGroupId && propertiesUtils.isInteger( $stateParams.rootFolderSearchTextScheduleGroupId) ? parseInt( $stateParams.rootFolderSearchTextScheduleGroupId ) : null;
        if($stateParams.rootFolderSearchText && $stateParams.rootFolderSearchText!='' && rootFolderSearchTextScheduleGroupId
        && $stateParams.rootFolderSearchTextScheduleGroupId == $scope.selectedScheduleGroupId ) {
          $scope.rootFolderFilterBySearchText =  $stateParams.rootFolderSearchText;
          $scope.searchText = decodeURIComponent($scope.rootFolderFilterBySearchText);
        }
        else if($scope.selectedScheduleGroupId && $stateParams.rootFolderSearchTextScheduleGroupId != $scope.selectedScheduleGroupId ){
         $scope.clearSearchText();
        }
      }

      $scope.searchRootFolders = function() {
        $scope.rootFolderFilterBySearchText =encodeURIComponent( $scope.searchText);
        $stateParams['rootFolderSearchText']=$scope.rootFolderFilterBySearchText;
        $stateParams['rootFolderSearchTextScheduleGroupId']=$scope.selectedScheduleGroupId;
        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
      };


      $scope.searchRootFolderChange = function() {
        if(_.isEmpty($scope.searchText)){
          $scope.searchRootFolders();
        }
      };

      $scope.clearSearchText = function() {
        $scope.searchText = '';
        $scope.searchRootFolders();
      };

      $scope.scheduleGroupPageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }
      $scope.onSelectScheduleGroups = function(data)
      {
        $scope.selectedScheduleGroups = data;
      }


      $scope.onSelectRootFolders = function (data:Operations.DtoRootFolderSummaryInfo[]) {
        $scope.selectRootFolders = data;

      };

      $scope.onSelectRootFolder = function(id,path,item:Operations.DtoRootFolderSummaryInfo) {
        $scope.selectedRootFolderId = id;
        $scope.selectedRootFolderName = path;
        $scope.selectedRootFolderNickName = item ? item.rootFolderDto.nickName : null;
        $scope.selectedRootFolder = item;

        $scope.selectedRootFolderLastRunDetailsId = item && item.crawlRunDetailsDto ? item.crawlRunDetailsDto.id : null;
        $scope.selectedRootFolderLastRunTriggeredBy = item && item.crawlRunDetailsDto ? item.crawlRunDetailsDto.scheduleGroupName : null;
        $scope.selectedRootFolderLastRunTriggeredByMaunal = item && item.crawlRunDetailsDto ? item.crawlRunDetailsDto.manual : null;
        $scope.selectedRootFolderIsRunning = item ? item.runStatus == Operations.ERunStatus.RUNNING : null;
        $scope.selectedRootFolderIsPaused = item ? item.runStatus == Operations.ERunStatus.PAUSED : null;
        $scope.selectedRootFolderIsNew = (!$scope.selectedRootFolderIsRunning && !$scope.selectedRootFolderIsPaused) || !$scope.selectedRootFolderId;

        $stateParams['selectedRootFolderId'] = $scope.selectedRootFolderId;

        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: false
          });

      }

      $scope.onSelectPhasesData = function (data:Dashboards.DtoPhaseDetails[]) {
        $scope.selectedPhasesData = data;

      };

      $scope.onSelectPhaseData = function(id,path,item:Dashboards.DtoPhaseDetails) {
        $scope.selectedPhaseDataId = id;
        $scope.selectedPhaseDataName = path;
        $scope.selectedPhaseData=item;

        if(item) {
          $scope.selectedPhasesDataIsRunning = item.jobState == Dashboards.EJobState.IN_PROGRESS ||item.jobState == Dashboards.EJobState.READY;
          $scope.selectedPhasesDataIsPaused = item.jobState == Dashboards.EJobState.PAUSED || item.jobState == Dashboards.EJobState.DONE;
         }

        $stateParams['selectedPhaseDataId'] = $scope.selectedPhaseDataId ;

        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: false
          });

      }
      $scope.updateScanStrategy = function (action) {
         $scope.jobProcessingMode = <Operations.EJobProcessingMode>action;
         scanService.updateScanStrategy(action);
      }

      $scope.getMpStatusIconHtml = function(mediaProcessors: Operations.DtoClaComponent) {
        return mediaProcessors.state == Operations.EComponentState.OK ? "&nbsp;<i  class='fa fa-check-square primary'></i>" :
          "&nbsp;<i class='fa fa-exclamation-triangle notice'></i>";
      }
      $scope.onExportScheduleGroupsToExcel=function(){
        $scope.exportScheduleGroupsToExcelInProgress = true;
        $scope.exportScheduleGroupsToExcel = {fileName: 'Schedule groups.xlsx'};
      };
      $scope.onExportRootFoldersToExcel=function(){
        $scope.exportRootFoldersToExcelInProgress = true;
        $scope.exportRootFoldersToExcel = {fileName: $scope.selectedScheduleGroupName+'root folders.xlsx'};
      };
      $scope.onExportPhasesToExcel=function(){
        $scope.exportPhasesToExcelInProgress = true;
        $scope.exportPhasesToExcel = {fileName: $scope.selectedRootFolderName +'Running phases.xlsx'};
      };

      $scope.exportPhasesToExcelCompleted=function(notFullRequest:boolean)
      {
        if(  $scope.exportPhasesToExcelInProgress) {
          if (notFullRequest) {
            dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
          }
          $scope.exportPhasesToExcelInProgress = false;
        }
      }
      $scope.exportRootFoldersToExcelCompleted=function(notFullRequest:boolean)
      {
        if(  $scope.exportRootFoldersToExcelInProgress) {
          if (notFullRequest) {
            dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
          }
          $scope.exportRootFoldersToExcelInProgress = false;
        }
      }
      $scope.exportScheduleGroupsToExcelCompleted=function(notFullRequest:boolean)
      {
        if(  $scope.exportScheduleGroupsToExcelInProgress) {
          if (notFullRequest) {
            dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
          }
          $scope.exportScheduleGroupsToExcelInProgress = false;
        }
      }
      $scope.rootFoldersPageChanged = function(pageNumber:number)
      {
        $stateParams['rootFoldersPage']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      };


      $scope.scheduleGroupPageChanged = function(pageNumber:number)
      {
        $stateParams['scheduleGroupsPage']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }


      eventCommunicator.registerHandler($scope.rootFolderActionsEventName, $scope, (new RegisteredEvent(function (registrarScope, registrationParam, fireParam) {

        switch (fireParam.action) {
          case 'continueRunning': {
            $scope.continueRunSelectedRootFolder();
            break;
          }
          case 'pauseRunning': {
            $scope.pauseSelectedRootFolderRuns();
            break;
          }
          case 'stopRunning': {
            $scope.stopRunSelectedRootFolder();
            break;
          }
          case 'scan': {
            $scope.scanSelectedRootFolders(false, false, false, false);
            break;
          }
          case 'map': {
            $scope.scanSelectedRootFolders(true, false, false, false);
            break;
          }
          case 'extract': {
            $scope.scanSelectedRootFolders(false, false, false, true);
            break;
          }
          case 'markForFullScan': {
            $scope.markForFullScan(fireParam.mark);
            break;
          }
          case 'setRescanState': {
            updateRescanActive(fireParam.rootFolderItem, fireParam.newState);
            break;
          }
          case 'openEditRootFolderItemDialog' : {
            eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridRootFoldersActions, {
              action: 'openEditRootFolderItemDialog',
              rootFolderItem: fireParam.rootFolderItem,
              triggeredFieldName: null
            });
            break;
          }
        }
      })));


      $scope.$on('$destroy', function(){
        scanService.forceRunStatusRefresh(false);
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }


    });


///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.fileCrawling.scheduleGroups',[

    ])
    .controller('scheduleGroupsGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                         propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('scheduleGroupsGridCtrl');


      $scope.elementName = 'schedule-groups-grid';
        var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;


        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData, $scope.minDetails?rowTemplateMinDetails:rowTemplate,
            $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        var doNotSelectFirstRowByDefault = false;
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

      };

      var setUserRoles = function()
      {
        $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
      }
      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
           return configuration.root_folder_schedules_summary_url+($scope.includeSubEntityData?'':'?withRootFolders=true');
      }

      var getDataUrlParams = function () {
        return null;
      };

      $scope.minDetails = $scope.minDetails?propertiesUtils.parseBoolean($scope.minDetails):null;
      var dtoScheduleGroup:Operations.DtoScheduleGroup =  Operations.DtoScheduleGroup.Empty();
      var dtoScheduleGroupSummaryInfo =  Operations.DtoScheduleGroupSummaryInfo.Empty();
      var dtoScheduleConfig =  Operations.DtoScheduleConfig.Empty();
      var dtoCrawlRunDetails =  Operations.DtoCrawlRunDetails.Empty();
      var dtoScheduleGroupPrefix =  propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.scheduleGroupDto)+'.';
      var dtoScheduleConfigPrefix =  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.scheduleConfigDto)+'.';
      var dtoLastCrawlRunDetailsPrefix =  propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.lastCrawlRunDetails)+'.';

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true,
        width:'35px'
      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        exportTitle:'NAME',

      };
      var nameDisplayTemplate =  '<da-icon href="scheduleGroup"></da-icon>&nbsp;<span title="{{dataItem.'+dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.schedulingDescription)+'}}" >'+
           '#= '+dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.name)+' #</span>';
      var nameTemplate = '<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem,\'general\')" class="title-primary">'+nameDisplayTemplate+ '</a> ';
      var disableNameTemplate = '<span ng-if="!isMngScanConfigUser"  class="title-primary">'+nameDisplayTemplate+ '</span> ';

      name.template=disableNameTemplate+nameTemplate;

      var nameNewTemplate =  '<span class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
          '<a class="title-primary">' +
              '<span title="{{dataItem.'+dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.schedulingDescription)+'}}" ng-class="{\'label-running\':dataItem.isRunning}" >#= '+dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.name)+' #</span> ' +
              '<span ng-show="dataItem.isRunning" class="fa fa-cog fa-spin "></span>'+
          '<i class="caret ng-hide"></i></a></span>'+
          '<ul class="dropdown-menu"  > '+
          '<li><a   ng-click="openEditDialog(dataItem,null)"><span class="fa fa-pencil"></span> Edit</a></li>'+
          ' </ul></span> ';

      var runState:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.runStatus),
        title: 'Last run status',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        width:'120px',
        template:'<span translate="scheduleGroup_{{dataItem.'+propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.runStatus)+'}}" title="Last run status"></span>'+
        '&nbsp;<span ng-show="dataItem.isEndedSuccessfully" class="fa fa-check primary" title="Last processing ended successfully"></span>' +
        '&nbsp;<span ng-show="dataItem.isEndedWithError" class="fa fa-exclamation-triangle notice" title="Last processing failed"></span>'
      };

      var schedulingDescription:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: dtoScheduleGroupPrefix+ propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.schedulingDescription),
        title: 'Description',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
        exportTitle:'DESCRIPTION',


      };
      var rootFoldersCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'rootFoldersCount',
        title: 'Root folders',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
        noWrapContent: true,
        width:'100px',
        template:'<span ng-if="dataItem.rootFoldersCount>0" title="#: rootFoldersCount #&nbsp;root-folders"> \
        <da-icon class="icon-item"  href="rootfolder"></da-icon> \
        &nbsp;#: rootFoldersCount #</span>'
      };
      var rootFoldersCountTemplate =  rootFoldersCount.template;

      var scheduleSummaryText:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'scheduleSummaryText',
        title: 'Schedule details',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        width:'280px',
        template:'#: scheduleSummaryText #'
      };

      $scope.getAnalyzePhaseBehaviorIcon = function(dataItem:Operations.DtoScheduleGroupSummaryInfo)
      {
        if(dataItem.scheduleGroupDto.analyzePhaseBehavior==Operations.ECrawlerPhaseBehavior.ONCE) {
          return "ion-qr-scanner";
        }

        return 'ion-ios-shuffle-strong';
      }

      var lastRunStartTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'lastRunStartTimeStr',
        title: 'last Run Start Time',
        type: EFieldTypes.type_dateTime,
        displayed: true,
        editable: false,
        nullable: false,
        width:'130px',
        format:'{0:'+configuration.dateTimeFormat+'}',

      };



      var lastRunEndTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'lastRunEndTimeStr',
        title: 'last Run End Time',
        type: EFieldTypes.type_dateTime,
        displayed: true,
        editable: false,
        nullable: false,
        format:'{0:'+configuration.dateTimeFormat+'}',

      };

      var nextPlannedWakeup:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.nextPlannedWakeup)+'Span',
        title: 'Next run due in',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        noWrapContent: true,
        centerText: true,
        width:'80px',
        template: '<span title="Next scan at {{dataItem.'+ propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.nextPlannedWakeup)+'\'}}" > ' +
        '#= nextPlannedWakeupSpan #</span> '


      };

      var lastRunElapsedTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  'lastRunElapsedTimeSpan',
        title: 'Elapsed Time',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        noWrapContent: true,
        centerText: true,
        width:'80px',
      };



      var active:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active),
        title: 'Active',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        width:'80px',
        exportTitle:'ACTIVE',
        template:'<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem,\'general\')" >'+
        '<span ng-show="dataItem.'+dtoScheduleGroupPrefix+propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active)+'"  title="Schedule group  is active"><span class="fa fa-plus ng-hide"></span> On</span>' +
        ' <span ng-show="!dataItem.'+dtoScheduleGroupPrefix+propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active)+'" title="Schedule group  is not active" class="notice"><span class="fa fa-minus  ng-hide" ></span> Off</span>'+
        '</a>'
      };
      var activeDisplayTemplate =    '<span ng-show="dataItem.'+dtoScheduleGroupPrefix+propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active)+'"  title="Schedule group  is active"><span class="fa fa-plus ng-hide"></span> On</span>' +
        ' <span ng-show="!dataItem.'+dtoScheduleGroupPrefix+propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active)+'" title="Schedule group  is not active" class="notice"><span class="fa fa-minus  ng-hide" ></span> Off</span>';

      var activeTemplate ='<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem,\'general\')" class="title-primaryy" >'+
        activeDisplayTemplate+
      '</a>';
      var activeDisabledTemplate = '<span ng-if="!isMngScanConfigUser" class="title-secondary">'+
        activeDisplayTemplate+
        '</span>';
      active.template =activeDisabledTemplate+ activeTemplate;

      var scheduleType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.scheduleType),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        exportTitle:'TYPE',
        width:'80px',
        tempale:'<span translate="'+dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.scheduleType)+'"></span>'
      };
      var scheduleEvery:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.every),
        title: 'Every',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        exportTitle:'SCHEDULE_EVERY',
        width:'80px',
      };
      var scheduleHour:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.hour),
        title: 'Hour',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        exportTitle:'SCHEDULE_HOUR',
        width:'80px',
      };
      var scheduleMinutes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.minutes),
        title: 'Minutes',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: false,
        centerText: true,
        exportTitle:'SCHEDULE_MINUTES',
        width:'80px',
      };
      var scheduleDaysOfTheWeek:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.daysOfTheWeek)+'AsStr',
        title: 'Days of the week',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,
        exportTitle:'SCHEDULE_DAYS_OF_THE_WEEK',
        template:  '#if (  '+dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.daysOfTheWeek)+') {#' +
        ' # for (var i=0; i < '+dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.daysOfTheWeek)+'.length; i++) { # ' +
        '<da-icon href="check"></da-icon><span class="" translate="#: '+dtoScheduleGroupPrefix+dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.daysOfTheWeek)+'[i] #"> </span>&nbsp; # } # # } #'
      };


      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
     fields.push(name);
      fields.push(scheduleSummaryText);

      fields.push(active);
      if(!$scope.minDetails) {
        fields.push(runState);
        fields.push(lastRunElapsedTime);
        fields.push(lastRunStartTime);
        fields.push(lastRunEndTime);
        fields.push(nextPlannedWakeup);
      }
      fields.push(scheduleType);
      fields.push(schedulingDescription);
      fields.push(scheduleEvery);
      fields.push(scheduleHour);
      fields.push(scheduleMinutes);
      fields.push(scheduleDaysOfTheWeek);
      fields.push(rootFoldersCount);



      var scanTimingsTemplate= splitViewBuilderHelper.getLastRunTimingTemplate(lastRunStartTime.fieldName,lastRunElapsedTime.fieldName,lastRunEndTime.fieldName,'isRunning');
      var statusTemplate= splitViewBuilderHelper.getStatusTemplate(runState.fieldName,'isEndedSuccessfully','isEndedWithError','isPaused','scheduleGroup_');
      var rescanActiveTemplate =splitViewBuilderHelper.getToggleButtonTemplate(active.fieldName,'rescan',"toggleRescanState(dataItem)");

      var nextPlannedWakeupTemplate = splitViewBuilderHelper.getCountTemplate(nextPlannedWakeup.fieldName,'Next scan in',null,'','Next scan at {{dataItem.'+ propertiesUtils.propName(dtoScheduleGroupSummaryInfo, dtoScheduleGroupSummaryInfo.nextPlannedWakeup)+'| date:\''+configuration.dateTimeFormat+'\'}}');


      var visibleFileds = fields.filter(f=>f.displayed);
      var rowTemplate="<tr  data-uid='#: uid #' colspan="+fields.length+">" +

          "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span  class='"+configuration.icon_scheduleGroup+"'></span></td>"+
          "<td colspan='"+(visibleFileds.length-7)+"' style='width:100%' class='ddl-cell'>" +  nameNewTemplate +
          "<div class='ellipsis title-third'>"+scheduleSummaryText.template+"</div></td>" +
          "<td colspan='1' width='40px'>"+rootFoldersCountTemplate+"</td>" +

          "<td colspan='1' width='70px'>"+statusTemplate+"</td>" +
          "<td colspan='1' width='110px'>"+nextPlannedWakeupTemplate+"</td>" +
          "<td colspan='1' width='40px'>"+rescanActiveTemplate+"</td>"+
          "<td colspan='1' width='100%'>"+scanTimingsTemplate+"</td>"+
          "</tr>";
      var rowTemplateMinDetails="";
      var parseData = function (schedule: Operations.DtoScheduleGroupSummaryInfo[]) {
        if(schedule)
        {
          schedule.forEach(d=> {
            (<any>d).id = d.scheduleGroupDto.id;

            (<any>d).isRunning = d.runStatus == Operations.ERunStatus.RUNNING;
            (<any>d).isEndedWithError = d.runStatus == Operations.ERunStatus.FAILED;
            (<any>d).isEndedSuccessfully = d.runStatus == Operations.ERunStatus.FINISHED_SUCCESSFULLY;
            (<any>d).rootFoldersCount = d.scheduleGroupDto.rootFolderIds ? d.scheduleGroupDto.rootFolderIds.length : 0;
            if (d.lastCrawlRunDetails) {

              (<any>d).lastRunEndTime = (!d.lastCrawlRunDetails.scanEndTime || d.lastCrawlRunDetails.scanEndTime == 0) ? null : new Date(d.lastCrawlRunDetails.scanEndTime);
              (<any>d).lastRunEndTimeStr =  $filter('date')((<any>d).lastRunEndTime, configuration.dateTimeFormat);
              (<any>d).lastRunStartTime = (!d.lastCrawlRunDetails.scanStartTime || d.lastCrawlRunDetails.scanStartTime == 0) ? null : new Date(d.lastCrawlRunDetails.scanStartTime);
              (<any>d).lastRunStartTimeStr =  $filter('date')((<any>d).lastRunStartTime, configuration.dateTimeFormat);
              var  endTime = (<any>d).isRunning?Date.now():d.lastCrawlRunDetails.scanEndTime;
              (<any>d).lastRunElapsedTime =endTime? Math.floor(endTime-d.lastCrawlRunDetails.scanStartTime):null;
              (<any>d).lastRunElapsedTimeSpan=(<any>d).lastRunElapsedTime?$filter('formatSpan')((<any>d).lastRunElapsedTime):null;
            }
            else
            {
              d.lastCrawlRunDetails=new Operations.DtoCrawlRunDetails();
            }
            (<any>d).nextPlannedWakeup= !d.scheduleGroupDto.active && (!d.nextPlannedWakeup||d.nextPlannedWakeup==0)?null:new Date(d.nextPlannedWakeup);
            (<any>d).scheduleSummaryText=scheduleConfigGetSummaryText(d.scheduleGroupDto.scheduleConfigDto);



             (<any>d).dueInElapsedTime =d.nextPlannedWakeup? Math.floor(d.nextPlannedWakeup-Date.now()):null;
            (<any>d).nextPlannedWakeupSpan=d.nextPlannedWakeup?$filter('formatSpan')( (<any>d).dueInElapsedTime):'';

            if(!  d.scheduleGroupDto.scheduleConfigDto )
            {
              d.scheduleGroupDto.scheduleConfigDto =new Operations.DtoScheduleConfig();
            }
            d.scheduleGroupDto.scheduleConfigDto["daysOfTheWeekAsStr"] = d.scheduleGroupDto.scheduleConfigDto.daysOfTheWeek? d.scheduleGroupDto.scheduleConfigDto.daysOfTheWeek.join(';'):null;


          });
        }
        return schedule;
      }

      var scheduleConfigGetSummaryText = function(scheduleConfigDto: Operations.DtoScheduleConfig)
      {
        var scheduleTypePart = scheduleConfigDto?$filter('translate')(scheduleConfigDto.scheduleType):'';
        if (scheduleConfigDto && scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.CUSTOM) {
          return scheduleTypePart;
        }
        var scheduleTimePart = scheduleConfigDto?', every '+scheduleConfigDto.every+' '+(scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.DAILY?
            'day':scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.HOURLY?'hour':'week')+' at '+(scheduleConfigDto.hour!=null?
            $filter('numberFixedLen')(scheduleConfigDto.hour,2):'xx')+':'+ $filter('numberFixedLen')(scheduleConfigDto.minutes,2):'';
        var scheduleDaysOfWeekPart = scheduleConfigDto&&scheduleConfigDto.daysOfTheWeek?' on '+scheduleConfigDto.daysOfTheWeek.map(d=>$filter('translate')(d)).join():'';
        return scheduleTypePart+scheduleTimePart+scheduleDaysOfWeekPart;
      }



      var onSelectedItemChanged = function()
      {
        //var idData=propertiesUtils.getDataFromDotted($scope.selectedItem,id.fieldName); //The regular way for item.path.id working only from remote and not from directlly set data
        //var nameData=propertiesUtils.getDataFromDotted($scope.selectedItem,name.fieldName);
        //$scope.itemSelected? $scope.itemSelected(idData,nameData,$scope.selectedItem):null;
        $scope.itemSelected?$scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem):null;
      }



      $scope.toggleRescanState = function(dataItem:Operations.DtoScheduleGroupSummaryInfo) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridScheduleGroupUserAction, {
          action: 'setRescanState',
          scheduleItem: dataItem.scheduleGroupDto,
          newState: !dataItem.scheduleGroupDto.active,
        });
      };

      $scope.openEditDialog = function(dataItem:any,openWithFieldName) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridScheduleGroupUserAction, {
            action: 'openEditItemDialog',
            item: dataItem,
            openWithFieldName: openWithFieldName,
          });
      };

      $scope.openDeleteDialog = function(dataItem:any) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridScheduleGroupUserAction, {
            action: 'openDeleteDialog',
            item: dataItem,
          });
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('scheduleGroupsGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',
          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          localDataBound: '=',
          findId: '=',

          fixHeight: '@',
          minDetails: '@',

        },
        controller: 'scheduleGroupsGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

///<reference path='../../../common/all.d.ts'/>


'use strict';
interface IScheduleGroupsManageMenuScope extends ng.IScope {
  selectedListItemsToOperate:Operations.DtoScheduleGroupSummaryInfo[];
  selectedListItemToOperate:Operations.DtoScheduleGroupSummaryInfo;
  reloadData: (selectedItems:number[]) => void;

}
interface IGridManageMenuController  {
  openAddNewDialog();
  openEditDialog (item?:Operations.DtoScheduleGroupSummaryInfo,openWithFieldName?:string);
  openDeleteDialog (item?:Operations.DtoScheduleGroupSummaryInfo);
  openUploadItemsDialog ();
  updateRescanActive(schedule:Operations.DtoScheduleGroup,active:boolean);
}

class ScheduleGroupManageMenuController  implements IGridManageMenuController{
  selectedListItemsToOperate:Operations.DtoScheduleGroupSummaryInfo[];
  selectedListItemToOperate:Operations.DtoScheduleGroupSummaryInfo;
  reloadData: (selectedItems:number[]) => void;

  private log:ILog = this.Logger.getInstance('ScheduleGroupManageMenuController');

  static $inject = ['$scope', 'scheduleGroupResource', 'configuration','dialogs','eventCommunicator','i18nNotifications','$q','localizedMessages','Logger'];
  constructor(protected isolateScope:IScheduleGroupsManageMenuScope,protected scheduleGroupResource:IScheduleGroupResource,protected configuration:IConfig,protected dialogs,
              protected eventCommunicator:IEventCommunicator,protected i18nNotifications:II18nNotifications,protected $q:angular.IQService,protected localizedMessages,protected Logger:ILogger) {

    let _this = this;
    this.initEventCommunicationHandlers();
    isolateScope.$on("$destroy", function() {
      _this.eventCommunicator.unregisterAllHandlers( (<any>_this).isolateScope);
    });

  }


  public openAddNewDialog () {
    let _this=this;
    let dlg = this.dialogs.create(
      'common/directives/operations/dialogs/newRootFolderScheduleDialog.tpl.html',
      'newRootFolderScheduleItemDialogCtrl',
      {},
      {size: 'md'});

    dlg.result.then(function (schedule:Operations.DtoScheduleGroup) {
      if(schedule) { //schedule item is added to server in the dialog
        _this.reloadData([schedule.id]);
      }
    }, function () {

    });
  }

  public openEditDialog(item?:Operations.DtoScheduleGroupSummaryInfo,openWithFieldName?:string) {

    let _this = this;
    let itemToAdd = item ? item : this.selectedListItemToOperate;
    if (itemToAdd) {
      let dlg = this.dialogs.create(
        'common/directives/operations/dialogs/newRootFolderScheduleDialog.tpl.html',
        'newRootFolderScheduleItemDialogCtrl',
        {scheduleGroup: itemToAdd.scheduleGroupDto, openWithFieldName: openWithFieldName},
        {size: 'md'});

      dlg.result.then(function (scheduleGroup:Operations.DtoScheduleGroup) {
        _this.editItem(scheduleGroup);
      }, function () {
      });
    }
  }

  public openDeleteDialog(item?:Operations.DtoScheduleGroupSummaryInfo) {
    let _self = this;
    let itemsToAdd:Operations.DtoScheduleGroupSummaryInfo[] = item?[item]:this.selectedListItemsToOperate;
    if (itemsToAdd && itemsToAdd.length > 0) {
      var userMsg;
      if(itemsToAdd.length==1)
      {
        userMsg="You are about to delete schedule group named: '"+itemsToAdd[0].scheduleGroupDto.name+"'. Continue?";
      }
      else if(itemsToAdd.length<6){
        userMsg="You are about to delete " + itemsToAdd.length + " schedule groups. Continue?"+'<br>'+itemsToAdd.map(u=>u.scheduleGroupDto.name).join(', ');
      }
      else {
        userMsg="You are about to delete " + itemsToAdd.length + " schedule groups. Continue?";
      }
      let dlg = this.dialogs.confirm('Confirmation', userMsg, null);
      dlg.result.then(function (btn) { //user confirmed deletion
        let loopPromises = [];
        itemsToAdd.forEach(d=> {
          let deferred = _self.$q.defer();
          loopPromises.push(deferred.promise);

          _self.deleteItem(d.scheduleGroupDto, deferred)
        });
        _self.queueAllCrudRequests(loopPromises);
      }, function (btn) {
        // $scope.confirmed = 'You confirmed "No."';
      });


    }

  }

  private queueAllCrudRequests(loopPromises,onCompletedCallback?) {
    let _self = this;
    this.$q.all(loopPromises).then(function (notifications:INotification[]) {
      var errorMsgs =  notifications.filter(n=>n!=null);

      if(errorMsgs[0]) {
        if(errorMsgs.length == 1) {
          _self.dialogs.error('Error','Failed to delete schedule group.'+'<br\>'+errorMsgs[0].message);
        }else{
          _self.i18nNotifications.onErrorInformUserAggregated(errorMsgs);
        }
      }
      if (onCompletedCallback) {
        onCompletedCallback();
      }
      if (_self.reloadData) {
        _self.reloadData([]);
      }

    });
  }
  public openUploadItemsDialog() {
    let _this = this;
    var dlg = this.dialogs.create(
      'common/directives/dialogs/uploadFileDialogByServer.tpl.html',
      'uploadFileDialogByServerCtrl',
      {
        uploadResourceFn: this.scheduleGroupResource.uploadScheduleGroupsFromCsv,
        validateUploadResourceFn: this.scheduleGroupResource.validateUploadScheduleGroupsFromCsv,
        itemsName: 'schedule groups'
      },
      {size: 'md'});
    dlg.result.then(function (fileUploaded:boolean) {
      if (fileUploaded) {
        _this.reloadData(null);
      }

    }, function () {

    });
  }
  public updateRescanActive = function (schedule:Operations.DtoScheduleGroup,active:boolean) {
    this.log.debug('update Rescan Active: ' + active);
    let _this = this;
    this.scheduleGroupResource.updateScheduleGroupRescanActive(schedule.id, active, function () {
      schedule.active = active;
      _this.reloadData([schedule.id]);

    }, _this.onError);
  }
  private deleteItem(schedulesGroup:Operations.DtoScheduleGroup,deferred)
  {
    let _this=this;
    this.log.debug('delete schedulesGroup: ' + schedulesGroup.name);
    this.scheduleGroupResource.deleteScheduleGroup(schedulesGroup.id, function () {
   //   var msg =   _this.i18nNotifications.createNotification('crud.assign.entity.remove.success', ENotificationType.success, {name : schedulesGroup.name,action:'Delete schedule group',entityName:'Schedule groups'},schedulesGroup);
    //  _this.i18nNotifications.pushNotificationForCurrentRoute(msg);
    //  deferred.resolve(msg);
      deferred.resolve(null);
    }, function (error) {
   //   var msg =  _this.i18nNotifications.createNotification('crud.assign.entity.remove.error', ENotificationType.error, {name : schedulesGroup.name,action:'Delete schedule group',entityName:'Schedule groups'},{error:error,selectedItem:schedulesGroup});
   //   _this.i18nNotifications.pushNotificationForCurrentRoute(msg);
      if (error.status == 400&&  error.data  && error.data.type == ERequestErrorCode.ITEM_ATTACHED_TO_ANOTHER) {
        var msg =  _this.i18nNotifications.createNotification('simple.item.in.use', ENotificationType.error, {name : schedulesGroup.name},{error:error,selectedItem:schedulesGroup});

        deferred.resolve(msg);
      }
      else{
        deferred.resolve( _this.i18nNotifications.createNotification('simple.item', ENotificationType.error, {name:schedulesGroup.name},null));
      }

       });
  }

  private editItem  (scheduleGroup:Operations.DtoScheduleGroup) {
    let _this = this;
    if (scheduleGroup.name && scheduleGroup.name.trim() != '') {
      _this.log.debug('edit scheduleGroup ' + scheduleGroup.name);
      this.scheduleGroupResource.updateScheduleGroup(scheduleGroup, function (newSchedule:Operations.DtoScheduleGroup) {
        _this.reloadData([newSchedule.id]);
      }, function (e) {
        if (e.status == 409) {
          _this.dialogs.error('Error', _this.localizedMessages.get('crud.update.entity.error.alreadyExists', {
            fieldValue: scheduleGroup.name,
            fieldName: 'name',
            entityName: 'schedule group'
          }));
        }
        else {
          _this.dialogs.error('Error', _this.localizedMessages.get('crud.update.entity.error.generic', {
            fieldValue: scheduleGroup.name,
            fieldName: 'name',
            entityName: 'schedule group'
          }));
        }
      });
    }
  }

  private initEventCommunicationHandlers() {
    let _this=this;
    this.eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridScheduleGroupUserAction,
      _this.isolateScope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'openDeleteDialog') {
          var item = fireParam.item;
          _this.openDeleteDialog(item);
        }
        else if (fireParam.action === 'openEditItemDialog') {
          var item = fireParam.item;
          var openWithFieldName = fireParam.openWithFieldName;
          _this.openEditDialog(item,openWithFieldName);
        }
        else if (fireParam.action === 'setRescanState') {

          _this.updateRescanActive(fireParam.scheduleItem,fireParam.newState);
        }

      })));
  }
  private onError() {
    this.i18nNotifications.onErrorInformUser();
  }

}
var schedulesManageMenu = function() {
  return {
    bindToController: {
      'selectedListItemsToOperate':'=',
      'selectedListItemToOperate':'=',
      'reloadData':'=',
    },
    scope:true,
    templateUrl: '/app/operations/scheduleGroups/scheduleGroupsManageMenu.tpl.html',
    replace:true,
    controllerAs:'schedules',
    controller:ScheduleGroupManageMenuController
  }
}
var schedulesManageMenuEdit = function(): ng.IDirective {
  return <ng.IDirective>{
    bindToController: {  // a property of the scope
      'selectedListItemsToOperate':'=',
      'selectedListItemToOperate':'=',
      'reloadData':'=',
    },
    scope:true,
    template: ' <a class="btn btn-link btn-xs"  ng-class="{disabled:!schedules.selectedListItemsToOperate||selectedListItemsToOperate.length>1||schedules.selectedListItemsToOperate.length==0}" \
           ng-click="schedules.openEditDialog();"> <span class="fa fa-pencil"></span>&nbsp;Edit \
             </a>',
    replace:true,
    controllerAs:'schedules',
    controller:ScheduleGroupManageMenuController
  }
}

angular.module('operations.schedules.manage.menu', [
    'resources.operations',
  ])
  .directive('schedulesManageMenu',schedulesManageMenu)
  .directive('schedulesManageMenuEdit', schedulesManageMenuEdit)

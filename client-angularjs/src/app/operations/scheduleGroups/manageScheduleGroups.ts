///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */

interface IScheduleGroupsManageScope extends ng.IScope {
  selectItem:Operations.DtoScheduleGroup[];
  onSelectedItem: (id,name,data:Operations.DtoScheduleGroup) => void;
  changeSelectedItem: (selectedItems:number[]) => void;
  excelFileName: string;
  exportName: string;
}

interface IGridManageController  {
  onSelectScheduleGroups(data:Operations.DtoScheduleGroup[]);
  onSelectScheduleGroup(id,name,data:Operations.DtoScheduleGroup);
  exportToExcelInProgress:boolean;
  exportToExcel:() => void;
  exportToExcelData:any;
}


class ManageScheduleGroupsController  implements IGridManageController {
  selectItems:Operations.DtoScheduleGroup[];
  selectItem:Operations.DtoScheduleGroup;
  isMngScanConfigUser:boolean;
  excelFileName:string;
  exportName:string;
  changeSelectedItem:(selectedItems:number[]) => void;
  exportToExcelData:any;
  exportToExcelInProgress:boolean;


  private log:ILog = this.Logger.getInstance('ManageScheduleGroupsController');


  static $inject = ['$scope','$timeout','configuration','dialogs','eventCommunicator','i18nNotifications','$q','localizedMessages','Logger','currentUserDetailsProvider','routerChangeService','$filter'];
  constructor(isolateScope:IScheduleGroupsManageScope,protected $timeout,protected configuration:IConfig,protected dialogs,
              protected eventCommunicator:IEventCommunicator,protected i18nNotifications:II18nNotifications,protected $q:angular.IQService,protected localizedMessages,protected Logger:ILogger,protected currentUserDetailsProvider:ICurrentUserDetailsProvider,protected routerChangeService,protected $filter) {
    let _self=this;
    isolateScope.$on("$destroy", function() {
      _self.eventCommunicator.unregisterAllHandlers( (<any>_self).isolateScope);
    });
    this.setUserRoles();
    isolateScope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
      _self.setUserRoles();
    });
  }
  private setUserRoles () {
     this.isMngScanConfigUser = this.currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
  };

  public onSelectScheduleGroups =(data:Operations.DtoScheduleGroup[])=> {
    this.selectItems = data;
  };
  public onSelectScheduleGroup=(id,name,data:Operations.DtoScheduleGroup)=> {  //this here would be the grid scope without the =>
    this.selectItem = data;

    if((<any>this).onSelectedItem) {
      (<any>this).onSelectedItem(id,name,data);
    }
  };


  public exportToExcel=()=> {
    let gotALotOfRecords = false;
    this.routerChangeService.addFileToProcess(this.excelFileName+ this.$filter('date')(Date.now(), this.configuration.exportNameDateTimeFormat),this.exportName ,{},{},gotALotOfRecords);
  };


};
var manageScheduleGroups  = function(): ng.IDirective {
  return <ng.IDirective> {
    bindToController: {
      selectItem: '=',
      onSelectedItem : '=',
      changeSelectedItem: '=',
      excelFileName: '=',
      exportName: '=',
      isMngScanConfigUser: '=',
    },
    scope:true,
    templateUrl: '/app/operations/scheduleGroups/manageScheduleGroups.tpl.html',
    replace:true,
    controllerAs:'schedulesmanage',
    controller:ManageScheduleGroupsController
  }
}

angular.module('operations.schedules.manage', [
  'operations.fileCrawling.scheduleGroups',
  'operations.schedules.manage.menu'

]).directive('manageScheduleGroups', manageScheduleGroups)


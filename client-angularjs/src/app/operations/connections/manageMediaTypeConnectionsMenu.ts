///<reference path='../../../common/all.d.ts'/>

'use strict';


angular.module('directives.mediaTypeConnection.manage.menu',['resources.operations','directives.dialogs.microsoft.new'])
    .controller('mediaTypeConnectionMenuCtrl', function ($scope,$element,Logger:ILogger,$q,
      mipConnectionResource:IMediaTypeConnectionResource<Operations.DtoMipConnectionDetails>, oneDriveConnectionResource:IMediaTypeConnectionResource<Operations.DtoOneDriveConnectionDetails>,
      configuration:IConfig, currentUserDetailsProvider, sharePointConnectionResource:IMediaTypeConnectionResource<Operations.DtoSharePointConnectionDetails>,
      boxConnectionResource:IMediaTypeConnectionResource<Operations.DtoBoxConnectionDetails>,exchange365ConnectionResource:IMediaTypeConnectionResource<Operations.DtoExchange365ConnectionDetails>,
      userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs) {

      var log:ILog = Logger.getInstance('mediaTypeConnectionMenuCtrl');

      $scope.init = function () {
        $scope.isExchange365SupportEnabled = configuration['exchange-365.support-enabled'] === 'true';
        $scope.isLabelingEnabled = currentUserDetailsProvider.isLabelingEnabled();
      };

      $scope.EMediaType = Operations.EMediaType;

      $scope.openNewMicrosoftConnectionDialog = function(mediaType:Operations.EMediaType) {
        var dlg = dialogs.create(
            'common/directives/operations/dialogs/newMicrosoftConnectionDialog.tpl.html',
            'newMicrosoftConnectionItemDialogCtrl',
            {mediaType:mediaType},
            {size: 'md'});
        dlg.result.then(function (connection:Operations.MicrosoftConnectionDetailsDtoBase) {
          $scope.reloadData(connection.id,connection.id);
        }, function () {
        });
      };

      $scope.openNewMipConnectionDialog = function(mediaType:Operations.EMediaType) {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/newMipConnectionDialog.tpl.html',
          'newMipConnectionItemDialogCtrl',
          {mediaType:mediaType},
          {size: 'md'});
        dlg.result.then(function (connection:Operations.DtoMediaTypeConnectionDetails) {
          $scope.reloadData(connection.id,connection.id);
        }, function () {
        });
      };

      $scope.openNewBoxConnectionDialog = function(e) {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/newBoxConnectionDialog.tpl.html',
          'newBoxConnectionItemDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function (connection:Operations.MicrosoftConnectionDetailsDtoBase) {
          $scope.reloadData(connection.id,connection.id);
        }, function () {
        });
      };

      $scope.openNewExchange365ConnectionDialog = function(e) {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/newExchange365ConnectionDialog.tpl.html',
          'newExchange365ConnectionItemDialogCtrl',
          {},
          {size: 'md'});

        dlg.result.then(function (connection:Operations.MicrosoftConnectionDetailsDtoBase) {
          $scope.reloadData(connection.id,connection.id);
        }, function () {
        });
      };

      var getMicrosoftResource = function(mediaType:Operations.EMediaType){
        return mediaType == Operations.EMediaType.ONE_DRIVE ? oneDriveConnectionResource : sharePointConnectionResource;
      };

      $scope.openEditSelectedItemDialog = function(listItem:Operations.DtoMediaTypeConnectionDetails) {
        var item:Operations.DtoMediaTypeConnectionDetails=listItem?listItem: $scope.selectedListItemsToOperate[0];

        if(item.mediaType == Operations.EMediaType.SHARE_POINT || item.mediaType == Operations.EMediaType.ONE_DRIVE) {
          getMicrosoftResource(item.mediaType).getConnection(item.id, function (connectionDetails:Operations.MicrosoftConnectionDetailsDtoBase) {
              var dlg = dialogs.create(
                  'common/directives/operations/dialogs/newMicrosoftConnectionDialog.tpl.html',
                  'newMicrosoftConnectionItemDialogCtrl',
                  {connection: connectionDetails,mediaType:item.mediaType},
                  {size: 'md'});

              dlg.result.then(function (item:Operations.DtoSharePointConnectionDetails) {
                editMicrosoftItem(item.mediaType, item);
              }, function () {

              });
          }, onError);
        }
        else if(item.mediaType == Operations.EMediaType.MIP) {
          mipConnectionResource.getConnection(item.id, function (connectionDetails:Operations.DtoMediaTypeConnectionDetails) {
            var dlg = dialogs.create(
              'common/directives/operations/dialogs/newMipConnectionDialog.tpl.html',
              'newMipConnectionItemDialogCtrl',
              {connection: connectionDetails,mediaType:item.mediaType},
              {size: 'md'});

            dlg.result.then(function (item:Operations.DtoMipConnectionDetails) {
              editMipItem(item);
            }, function () {
            });
          }, onError);
        }
        else if(item.mediaType == Operations.EMediaType.BOX) {
          boxConnectionResource.getConnection(item.id, function (boxConnection: Operations.DtoBoxConnectionDetails){
            var dlg = dialogs.create(
              'common/directives/operations/dialogs/newBoxConnectionDialog.tpl.html',
              'newBoxConnectionItemDialogCtrl',
              {connection: boxConnection},
              {size: 'md'});

            dlg.result.then(function (connection: Operations.DtoBoxConnectionDetails) {
              editBoxItem(connection);
            }, function () {

            });
          }, onError);
        }
        else if(item.mediaType == Operations.EMediaType.EXCHANGE365) {
          exchange365ConnectionResource.getConnection(item.id, function (exchange365Connection: Operations.DtoExchange365ConnectionDetails){
            var dlg = dialogs.create(
              'common/directives/operations/dialogs/newExchange365ConnectionDialog.tpl.html',
              'newExchange365ConnectionItemDialogCtrl',
              {connection: exchange365Connection},
              {size: 'md'});

            dlg.result.then(function (exchange365Connection: Operations.DtoExchange365ConnectionDetails) {
              editExchangeItem(exchange365Connection);
            }, function () {

            });
          }, onError);
        }
      };

      $scope.openUploadItemsDialog = function() {
        var dlg = dialogs.create(
          'common/directives/dialogs/uploadFileDialogByServer.tpl.html',
          'uploadFileDialogByServerCtrl',
          {uploadResourceFn: sharePointConnectionResource.uploadConnectionsFromCsv,validateUploadResourceFn: sharePointConnectionResource.validateUploadConnectionsFromCsv,itemsName:'media connections'},
          {size:'md'});
        dlg.result.then(function(fileUploaded:boolean){
          if(fileUploaded)
          {
            $scope.reloadData();
          }

        },function(){

        });
      };

      $scope.openDeleteSelectedItems = function() {
        var items:Operations.DtoMediaTypeConnectionDetails[]=$scope.selectedListItemsToOperate;
        if(items&& items.length>0) {
          var userMsg;
          if(items.length==1) {
            userMsg="You are about to delete connection named: '"+items[0].name+"'. Continue?";
          }
          else if(items.length<6) {
            userMsg="You are about to delete " + items.length + " connections. Continue?"+'<br>'+items.map(u=>u.name).join(', ');
          }
          else {
            userMsg="You are about to delete " + items.length + " connections. Continue ?";
          }
          var dlg = dialogs.confirm('Confirmation', userMsg, null);
          dlg.result.then(function (btn) { //user confirmed deletion
            items.forEach(d=>  deleteItem(d));

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }
      };

      var deleteItem = function(item:Operations.DtoMediaTypeConnectionDetails) {
        log.debug('delete media type connection: ' + item.name);
        sharePointConnectionResource.deleteConnection(item.id, function () {
          $scope.reloadData();
        },function(e) {
          if(e.status==400 && e.data.type==ERequestErrorCode.ITEM_ATTACHED_TO_ANOTHER)
          {
            dialogs.error('Delete error','Cannot delete \''+item.name+'\'! <br/> <br/>Media connection is in use.');
          }
          else{
            onError();
          }
        });
      };

      var editMicrosoftItem = function (mediaType, connection:Operations.DtoSharePointConnectionDetails) {
        if ( connection.sharePointConnectionParametersDto.url &&  connection.sharePointConnectionParametersDto.url.trim() != '') {
          log.debug('edit SharePointConnectionDetails to list  url: ' +  connection.url);
          getMicrosoftResource(mediaType).updateConnection(connection, function (newConnection:Operations.DtoSharePointConnectionDetails) {
                  $scope.reloadData([newConnection.id]);
          }, onError);
        }
      };

      var editMipItem = function (connection:Operations.DtoMipConnectionDetails) {
        if ( connection.url &&  connection.url.trim() != '') {
          log.debug('edit MipConnectionDetails to list  url: ' +  connection.url);
          mipConnectionResource.updateConnection(connection,function (newConnection:Operations.DtoMipConnectionDetails) {
            $scope.reloadData([newConnection.id]);
          }, onError);
        }
      };

      var editBoxItem = function (connection:Operations.DtoBoxConnectionDetails) {
          boxConnectionResource.updateConnection(connection, function (newConnection:Operations.DtoBoxConnectionDetails) {
                  $scope.reloadData([newConnection.id]);
          }, onError);
      };

      var editExchangeItem = function (connection:Operations.DtoExchange365ConnectionDetails) {
        exchange365ConnectionResource.updateConnection(connection, function (newConnection:Operations.DtoExchange365ConnectionDetails) {
          $scope.reloadData([newConnection.id]);
        }, onError);
      };

      var onError = function() {
        dialogs.error('Error','Sorry, an error occurred.');
      };

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridMediaTypeConnectionsUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        if (fireParam.action === 'openEditItemDialog') {
          $scope.openEditSelectedItemDialog(fireParam.item);
        }
      })));

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
    })
    .directive('mediaTypeConnectionManageMenu',
        function () {
          return {
            templateUrl: '/app/operations/connections/manageMediaTypeConnectionsMenu.tpl.html',
            controller: 'mediaTypeConnectionMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'firstSelectedListItemToOperate':'=',
              'reloadData':'=',
              'gridFindId':'=',
              'refreshDisplay':'='
            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    );



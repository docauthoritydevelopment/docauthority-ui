///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


var manageMediaConnections  = function(): ng.IDirective {
  return <ng.IDirective> {
    bindToController: {
      selectItem: '=',
      changeSelectedItem: '=',
      excelFileName: '=',
      exportName: '='
    },
    scope:true,
    templateUrl: '/app/operations/connections/manageMediaConnections.tpl.html',
    replace:true,
    controllerAs:'manage',
    controller:ManageScheduleGroupsController
  }
}

angular.module('operations.mediaTypeConnection.manage', [


]).directive('manageMediaConnections', manageMediaConnections)

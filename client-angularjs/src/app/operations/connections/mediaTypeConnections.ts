///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */

angular.module('operations.connections.mediaType.grid',[

    ])
    .controller('mediaTypeConnectionsGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                    propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log:ILog = Logger.getInstance('mediaTypeConnectionsGridCtrl');


      $scope.elementName = 'mediaType-connections';
      var gridOptions:ui.IGridHelper;

      $scope.init = function (restrictedEntityDisplayTypeName) {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData,  rowTemplate,
            $stateParams, null,getDataUrlBuilder,true,$scope.fixHeight);

        var doNotSelectFirstRowByDefault = false;
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,
            doNotSelectFirstRowByDefault,null,$scope.fixHeight!=null);

      };

      var setUserRoles = function()
      {
        $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
      }
      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
        return configuration.mediaType_connections_url;
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dTOMediaTypeConnection:Operations.DtoMediaTypeConnectionDetails =  Operations.DtoMediaTypeConnectionDetails.Empty();

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,

      };
      var name:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.name),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"NAME",
        nullable: true,

      };


      var nameDisplayTemplate =  '<span title="{{dataItem.'+ propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.username)+'}}" >'+
        '#= '+ propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.name)+' #</span>';
      var nameTemplate = '<a ng-if="isMngScanConfigUser" ng-click="openEditDialog(dataItem)" class="title-primary">'+nameDisplayTemplate+ '</a> ';
      var disableNameTemplate = '<span ng-if="!isMngScanConfigUser"  class="title-primary">'+nameDisplayTemplate+ '</span> ';

      name.template=disableNameTemplate+nameTemplate;

      var mediaType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.mediaType),
        title: 'Type',
        type: EFieldTypes.type_string,
        displayed: true,
        width:'100px',
        editable: false,
        nullable: true,
        exportTitle:"MEDIA TYPE",
        template:'<span translate="{{'+'dataItem.'+propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.mediaType)+'}}"></span>'
      };
      var username:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.username),
        title: 'username',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"USERNAME",
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.url)+' #'
      };
      var domain:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'domain',
        title: 'domain',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"DOMAIN",
        nullable: true,
        template:'#: domain #'
      };
      var url:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.url),
        title: 'Url',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"URL",
        nullable: true,
        template:'#: '+ propertiesUtils.propName(dTOMediaTypeConnection, dTOMediaTypeConnection.url)+' #'
      };

     var details:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:'Details',
        title: 'Details',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        template:"<a  ng-click='openEditDialog(dataItem)' >Details</a>"
      };
      var tenantId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'tenantId',
        title: 'Tenant ID',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"TENANT ID",
        nullable: true,
        template:'#: tenantId #'
      };
      var applicationId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'applicationId',
        title: 'Application ID',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        exportTitle:"APPLICATION ID",
        nullable: true,
        template:'#: applicationId #'
      };

      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(name);
      fields.push(mediaType);
      fields.push(username);
      fields.push(domain);
      fields.push(url);
      fields.push(tenantId);
      fields.push(applicationId);

      // fields.push(details);
      $scope.fields =fields;



      var typeTemplate = splitViewBuilderHelper.getCountTemplate(mediaType.fieldName,'Type:',null,null);
      var usernameTemplate = splitViewBuilderHelper.getTitledTemplate(username.fieldName,'Username',null,null);
      var domainTemplate = splitViewBuilderHelper.getTitledTemplate(domain.fieldName,'Domain',null,null);
      var tenantIdTemplate = splitViewBuilderHelper.getTitledTemplate(tenantId.fieldName,'Tenant ID',null,null);
      var applicationIdTemplate = splitViewBuilderHelper.getTitledTemplate(applicationId.fieldName,'Application ID',null,null);


      var visibleFileds = fields.filter(f=>f.displayed);

      var rowTemplate="<tr  data-uid='#: uid #' colspan='"+visibleFileds.length+"'>" +
          "<td colspan='1' class='icon-column'><span  class='"+configuration.icon_mediaTypeConnection+" '></span></td>"+
          "<td colspan='"+(visibleFileds.length-4)+"'  class='name-column ddl-cell'>"+
            "<span class='pull-right' style='max-width: 46%;'>" +
            "<span class='pull-right' ><span  class='repositoryType {{dataItem."+mediaType.fieldName+"}}'></span></span>" +
            "<div style='clear:right'></div>"+
            "</span>" + nameTemplate+"<div class='title-third' >"+url.template+"</div>" +
          "</td>"  +
          "<td colspan='1' class='type-column'>"+typeTemplate+"</td>"+
          "<td colspan='1' class='user-name-column'>" +
            "<span ng-if='!dataItem.isExchange365'>" + usernameTemplate + "</span>" +
            "<span ng-if='dataItem.isExchange365'>" + applicationIdTemplate + "</span>" +
          "</td>"+
          "<td colspan='1' class='domain-column'>" +
            "<span ng-if='!dataItem.isExchange365'>" + domainTemplate + "</span>" +
            "<span ng-if='dataItem.isExchange365'>" + tenantIdTemplate + "</span>" +
          "</td>"+

        // "<td colspan='1' width='30%'>"+details.template+"</td>"+

          "</tr>";

      var parseData = function (connections: Operations.DtoMediaTypeConnectionDetails[]) {
        if(connections)
        {
          connections.forEach(con=> {
            if(!_.isEmpty(con.connectionParametersJson)) {
              try {
                let connectionObj = JSON.parse(con.connectionParametersJson);
                (<any>con).domain = connectionObj.domain;
                (<any>con).tenantId = connectionObj.tenantId;
                (<any>con).applicationId = connectionObj.applicationId;
                (<any>con).isExchange365 = !_.isEmpty((<any>con).tenantId) || !_.isEmpty((<any>con).applicationId);
              } catch (error) {
                console.error('Cannot parse connectionParametersJson: '+con.connectionParametersJson);
              }
            }
            con.url = con.url?con.url:'';
          })

        }
        return connections;
      }


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.openEditDialog = function(dataItem:any) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMediaTypeConnectionsUserAction, {
          action: 'openEditItemDialog',
          item: dataItem,
        });
      };




      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('mediaTypeConnectionsGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class=" {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
        ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',

          fixHeight: '@',

        },
        controller: 'mediaTypeConnectionsGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

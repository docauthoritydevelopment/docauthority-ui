///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.connections.mediaType', [
  'operations.connections.mediaType.grid',
  'directives.mediaTypeConnection.manage.menu'
]);

angular.module('operations.connections.mediaType')
    .config(function ($stateProvider) {
      $stateProvider
          .state(ERouteStateName.mediaTypeConnectionsManagement, {
            parent: 'operations.configuration.connections',
            url: '/mediaType?&selectedId?',
            templateUrl: '/app/operations/connections/manageMediaTypeConnectionsPage.tpl.html',
            controller: 'ManageMediaTypeConnectionsPageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngScanConfig]
            }
          })

    })
    .controller('ManageMediaTypeConnectionsPageCtrl', function ($window, $scope, $state, $location, $element, $compile, eventCommunicator:IEventCommunicator, $timeout, scanService:IScanService,
                                                                Logger, propertiesUtils, $stateParams, pageTitle, bizListsResource:IBizListsResource, configuration:IConfig,
                                                                dialogs, bizListResource:IBizListResource, activeScansResource:IActiveScansResource, userSettings:IUserSettingsService, filterFactory:ui.IFilterFactory) {
      var log:ILog = Logger.getInstance('ManageMediaTypeConnectionsPageCtrl');


      $scope.init = function () {
        pageTitle.set("Manage media type connections ");
       $scope.initiated = true;
      };

      $scope.onMediaTypeConnectionSelectItem = function (id,name,item) {
        $scope.mediaTypeConnectionSelectedItemId = id;
        $scope.mediaTypeConnectionSelectedItemName = name;
        $scope.mediaTypeConnectionSelectedItem = item;
      };
      $scope.onMediaTypeConnectionSelectItems = function (data) {
        $scope.mediaTypeConnectionSelectedItems = data;
      };
      $scope.onExportToExcel=function(){
        $scope.exportToInProgress = true;
        log.debug('export '+' to excel');
        $scope.exportToExcel = {fileName:'Media type connections.xlsx'};
      };
      $scope.exportToExcelCompleted=function(notFullRequest:boolean)
      {
        if(notFullRequest) {
          dialogs.notify('Note','Excel file do not contain all data.<br/>Excel file created but records count limit reached.');

        }
        $scope.exportToExcelInProgress = false;
      }


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);

      });

      var onGetError=function(e)
      {

        $scope.initiated=true;
        dialogs.error('Error','Sorry, an error occurred.');
      };
      userSettings.getGridViewPreferences(function (data) {
        $scope.changeGridView = (!data) || data.gridView;
        $scope.init();
      });
    })

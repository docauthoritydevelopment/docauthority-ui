///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations',[
  'resources.operations',
  'operations.rootFolders.manage',
  'operations.fileCrawling.rootFolders',
   'operations.connections.mediaType.grid',
  'operations.connections.mediaType',
  'operations.connections.ldap',
  'operations.serverComponents.status',
    'operations.serverComponents.monitor',
  'operations.serverComponents.list',
  'operations.serverComponents.tree',
    'operations.customerDataCenters.list',
    'operations.manageCustomerDataCenters.manage',
    'directives.customerDataCenter.manage.menu',
    'operations.schedules.manage.menu',
    'operations.mediaProcessors.list',

])
  .config(function ($stateProvider) {

    $stateProvider
      .state('operations', {
        url: '/scan',
        template:'<div aaa ui-view  ></div>',
        abstract:true,

      })
      .state('operations.manage', {
        parent:'operations',
        template:'<div bbbb ui-view ng-style="height(-70)"></div>',
        url: '/manage',
        abstract:true,

      })
      .state('operations.monitor', {
        parent:'operations',
        template:'<div ccc ui-view ></div>',
        url: '/monitor',
        abstract:true,

      })
      .state('operations.monitor.history', {
        parent:'operations.monitor',
        template:'<div ccc ui-view ></div>',
        url: '/history',
        abstract:true,

      })
      .state('operations.configuration', {
        parent:'operations',
        template:'<div ddd ui-view  ng-style="height(-70)" ></div>',
        url: '/configuration',
        abstract:true,
      })
      .state('operations.configuration.connections', {
        parent:'operations.configuration',
        template:'<div eee ui-view  ng-style="height(-70)"></div>',
        url: '/connections',
        abstract:true,
      })

  })

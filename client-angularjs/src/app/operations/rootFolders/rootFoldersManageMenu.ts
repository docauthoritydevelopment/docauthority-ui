///<reference path='../../../common/all.d.ts'/>

'use strict';


angular.module('operations.rootFolders.manage.menu', ['doc2Services',
      'resources.operations',
      'directives.dialogs.importantConfirmation'
    ])
    .controller('rootFoldersManageMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q, scanService:IScanService,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                       routerChangeService,userSettings:IUserSettingsService,$timeout, eventCommunicator:IEventCommunicator,dialogs,rootFoldersResource:IRootFoldersResource) {
      var log:ILog = Logger.getInstance('rootFoldersManageMenuCtrl');

      $scope.init = function () {
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isMngDepartmentConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDepartmentConfig]);
      }
      $scope.openNewRootFolderDialog = function()
      {
        var dlg = dialogs.create(
            'common/directives/operations/dialogs/newRootFolderDialog.tpl.html',
            'newRootFolderItemDialogCtrl',
            {isMngDepartmentConfigUser:$scope.isMngDepartmentConfigUser},
            {size:'md'});

        dlg.result.then(function(data:any){
           addNewItem(<Operations.DtoRootFolder>(data.rootFolderItem),data.excludedFolders,data.scheduleGroup);
          if(data.createAnother === true) {
            $timeout(function () {
              $scope.openNewRootFolderDialog();
            })
          }
        },function(){

        });
      }



      $scope.openEditRootFolderDialog = function(rootFolderSummaryInfo:Operations.DtoRootFolderSummaryInfo,openWithFieldName?)
      {


        var dlg = dialogs.create(
            'common/directives/operations/dialogs/newRootFolderDialog.tpl.html',
            'newRootFolderItemDialogCtrl',
            {rootFolderSummaryInfo:rootFolderSummaryInfo,openWithFieldName:openWithFieldName,updateExcludedFolderSelectedItem:updateExcludedFolderSelectedItem,
              deleteExcludedFolder:deleteExcludedFolder,isMngDepartmentConfigUser:$scope.isMngDepartmentConfigUser},
            {size:'md'});

        dlg.result.then(function(data:any){

          editItem((<Operations.DtoRootFolder>data.rootFolderItem),data.scheduleGroup);
        },function(){


          $scope.reloadData();
        });
      }


      $scope.openDeleteRootFoldersDialog= function() {
        var rootFoldersItems:Operations.DtoRootFolderSummaryInfo[]=$scope.selectedListItemsToOperate;
        $scope.showDeleteRootFoldersDialogByRootFolders(rootFoldersItems);
      }


      $scope.showDeleteRootFoldersDialogByRootFolders= function(rootFoldersItems:Operations.DtoRootFolderSummaryInfo[])
      {
        if(rootFoldersItems&& rootFoldersItems.length>0) {
          var userMsg;
          var useSimpleDelete:boolean = true;
          if(rootFoldersItems.length==1)
          {
            let deleteFolderName:string = rootFoldersItems[0].rootFolderDto.nickName;
            if (!deleteFolderName) {
              deleteFolderName = rootFoldersItems[0].rootFolderDto.realPath;
            }
            if (!deleteFolderName) {
              deleteFolderName = rootFoldersItems[0].rootFolderDto.mailboxGroup;
            }
            if (canRootFolderUseSimpleDelete(rootFoldersItems[0])) {
              userMsg = "You are about to delete root folder: '" + deleteFolderName + "'. Continue?";
            }
            else {
              useSimpleDelete = false;
              if (!rootFoldersItems[0].crawlRunDetailsDto  || !rootFoldersItems[0].crawlRunDetailsDto.totalProcessedFiles) {
                userMsg = 'You are about to delete root folder: <br><b>' + deleteFolderName + '</b><br>' +
                  "<br>This operation may remove scanned files from the system.<br><br>Type 'YES' to continue:";
              }
              else {
                userMsg = 'You are about to delete root folder: <br><b>' + deleteFolderName + '</b><br>' +
                  "<br>This operation will remove <b>" + rootFoldersItems[0].crawlRunDetailsDto.totalProcessedFiles + " </b>scanned files from the system.<br><br>Type 'YES' to continue:";
              }
            }
          }
          else if(rootFoldersItems.length<6){
            userMsg="You are about to delete " + rootFoldersItems.length + " root folders. Continue?"+'<br>'+rootFoldersItems.map(u=>u.rootFolderDto.path).join(', ');
          }
          else {
            userMsg="You are about to delete " + rootFoldersItems.length + " root folders. Continue?";
          }

          if (useSimpleDelete) {
            var dlg = dialogs.confirm('Confirmation', userMsg, null);
            dlg.result.then(function (btn) { //user confirmed deletion
              var loopPromises = [];
              rootFoldersItems.forEach(d => {
                var deferred = $q.defer();
                loopPromises.push(deferred.promise);
                deleteItem(d.rootFolderDto, deferred)
              });
              $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                if ($scope.reloadScheduleData) {
                  $scope.reloadScheduleData();
                }
                else {
                  $scope.reloadData();
                }
              });
            }, function (btn) {
              // $scope.confirmed = 'You confirmed "No."';
            });
          }
          else {
            let importantDlg = dialogs.create(
                'common/directives/dialogs/importantConfirmationDialog.tpl.html',
                'importantConfirmationDialogCtrl',
                {msg : userMsg},
                {size:'md'});
            importantDlg.result.then(function(data:any){
              rootFoldersResource.deleteRootFolderForce(rootFoldersItems[0].rootFolderDto.id, function () {
                routerChangeService.updateAsyncUserOperations();
                $scope.reloadData();
              }, function (err) {
                dialogs.error('Error',err.data.message);
              });
            },function(){

            });
          }
        }
      }

      $scope.openUploadItemsDialog = function()
      {
        var dlg = dialogs.create(
          'common/directives/dialogs/uploadFileDialogByServer.tpl.html',
          'uploadFileDialogByServerCtrl',
          {uploadResourceFn: rootFoldersResource.uploadRootFoldersFromCsv,
            validateUploadResourceFn: rootFoldersResource.validateUploadRootFoldersFromCsv,
            itemsName:'root folders',createStubsOptionEnabled:true},
          {size:'md'});
        dlg.result.then(function(fileUploaded:boolean){

          if(fileUploaded)
          {
            $scope.reloadData();
          }

        },function(){


        });
      };

       $scope.showDeleteBtn = function() {
         var rootFoldersItems:Operations.DtoRootFolderSummaryInfo[]=$scope.selectedListItemsToOperate;
         if(rootFoldersItems&& rootFoldersItems.length>0) {
           if (rootFoldersItems.length == 1) {
             return (!rootFoldersItems[0].rootFolderDto.deleted && rootFoldersItems[0].crawlRunDetailsDto.runOutcomeState != Operations.ERunStatus.RUNNING);
           }
           else {
             let allAreSimple:boolean = true;
             rootFoldersItems.forEach((rootFolder) =>{
               if (!canRootFolderUseSimpleDelete(rootFolder)){
                 allAreSimple = false;
               }
             })
             return allAreSimple;
           }
         }
         return false;
       };

      var canRootFolderUseSimpleDelete = function(selectedRootFolder:Operations.DtoRootFolderSummaryInfo)
      {
        var rootfolderNotScannedYet = selectedRootFolder  &&(!selectedRootFolder.crawlRunDetailsDto || !selectedRootFolder.crawlRunDetailsDto.runOutcomeState);
        var rootfolderScannedWithNoFilesFound = selectedRootFolder  && selectedRootFolder.crawlRunDetailsDto && selectedRootFolder.crawlRunDetailsDto.totalProcessedFiles==0 && selectedRootFolder.rootFolderDto.numberOfFoldersFound == 0 &&
          (selectedRootFolder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FINISHED_SUCCESSFULLY ||
          selectedRootFolder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FINISHED_WITH_WARNINGS ||
          selectedRootFolder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FAILED);
        return rootfolderNotScannedYet||rootfolderScannedWithNoFilesFound  ;
      }

      var deleteItem = function(rootFolder:Operations.DtoRootFolder,deferred)
      {
        log.debug('delete rootFolder: ' + rootFolder.realPath);
        rootFoldersResource.deleteRootFolder(rootFolder.id, function () {
       //   $scope.reloadData();
          deferred.resolve(rootFolder);
        }, function (err) {
          deferred.resolve(rootFolder);
          dialogs.error('Error',err.data.message);
        });
      }

      var addNewItem = function (rootFolder:Operations.DtoRootFolder,excludedFolders:Operations.DtoFolderExcludeRule[],scheduleGroup:Operations.DtoScheduleGroup) {

          rootFoldersResource.addNewRootFolder(rootFolder,scheduleGroup.id, function (newRootFolder) {
            var loopPromises = [];
            if(excludedFolders&&excludedFolders.length>0)
            {

              excludedFolders.forEach(excludedFolder=> {
              //  if((<any>excludedFolder).active) {
                  var deferred = $q.defer();
                  loopPromises.push(deferred.promise);
                  rootFoldersResource.addNewExcludedFolderRule(<any>(newRootFolder.id), excludedFolder, function (newRootFolder) {
                    deferred.resolve();

                  }, function () {
                    deferred.resolve();
                  });
               // }
              });
            }

            $q.all(loopPromises).then(function (selectedParentIdsToUpdate) {
              if($scope.reloadScheduleData)
              {
                $scope.reloadScheduleData(scheduleGroup.id,newRootFolder.id);
              }
              else {
                $scope.reloadData(null,newRootFolder.id); //use findId to find and set the new rootfolder selected
              }

            });
          }, function (e) {
            if(e.status==409)
            {
              dialogs.error('Error','Root folder with this path already exists.');
            }
            else if(e.status==400 && e.data.type==ERequestErrorCode.LICENSE_QUOTA_EXCEEDED)
            {
              dialogs.error('License error','Root folder license max quota reached! <br/> <br/>Root folder not added.');
            }

            else {
              dialogs.error('Error', 'Failed to add root folder.');
            }
          });
      }

      var updateExcludedFolderSelectedItem = function( item:ParamActiveItem<Operations.DtoFolderExcludeRule>,successFunction,errorFunction) {

          var excludedFolder = item.item.value;
            excludedFolder.disabled = !item.active;
          if (!excludedFolder.id) {
            rootFoldersResource.addNewExcludedFolderRule(excludedFolder.rootFolderId, excludedFolder, function (newRootFolder) {
              successFunction();

            }, function () {
              errorFunction();
            });
          }
          else {
            rootFoldersResource.updateExcludedFolderRule(excludedFolder.rootFolderId, excludedFolder, function (newRootFolder) {
              successFunction();

            }, function () {
              errorFunction();
            });
          }

      }
      var deleteExcludedFolder = function( item:ParamActiveItem<Operations.DtoFolderExcludeRule>,successFunction,errorFunction) {
        var excludedFolder = <any>item.item.value;

        rootFoldersResource.deleteExcludedFolderRule(excludedFolder.rootFolderId, excludedFolder.id, function () {
          successFunction();

        }, function () {
          errorFunction();
        });

      }


      var editItem = function (rootFolder:Operations.DtoRootFolder,scheduleGroup:Operations.DtoScheduleGroup) {
         log.debug('edit rootFolder '+rootFolder.realPath );
          rootFoldersResource.updateRootFolder( rootFolder, function (editedRootFolder) {
            if(scheduleGroup)
            {
             rootFoldersResource.setRootFolderScheduleGroup(rootFolder.id, scheduleGroup.id, function (editedScheduleRootFolder) {
                if($scope.reloadScheduleData) {
                  $scope.reloadScheduleData(scheduleGroup.id,rootFolder.id); //use findId to find and set the new schedule selected
                } else {
                  $scope.reloadData([rootFolder.id]);
                }
              },onError);
            }
            else {
              $scope.reloadData([rootFolder.id]);
            }
            routerChangeService.updateAsyncUserOperations();

          }, onError);
      };

      var updateRescanActive = function (rootFolder:Operations.DtoRootFolder,active:boolean) {
          rootFoldersResource.updateRootFolderRescanActive( rootFolder.id, active,function () {
            rootFolder.rescanActive = active;
            $scope.refreshDisplay();
          }, onError);
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridRootFoldersActions,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
            if (fireParam.action === 'openDeleteRootFolderItemDialog') {
              $scope.showDeleteRootFoldersDialogByRootFolders([fireParam.rootFolderItem]);
            }
            if (fireParam.action === 'openEditRootFolderItemDialog') {
              $scope.openEditRootFolderDialog(fireParam.rootFolderItem, fireParam.triggeredFieldName);
            }
            if (fireParam.action === 'continueRunning') {
              $scope.continueRunSelectedRootFolder(fireParam.rootFolderItem);
            }
            else if (fireParam.action === 'pauseRunning') {
              $scope.pauseSelectedRootFolderRuns(fireParam.rootFolderItem)
            }
            else if (fireParam.action === 'stopRunning') {
              $scope.stopRunSelectedRootFolder(fireParam.rootFolderItem);
            }
            else if (fireParam.action === 'scan') {
              $scope.scanSelectedRootFolders(fireParam.rootFolderItem, false,false,false,false);
            }
            else if (fireParam.action === 'map') {
              $scope.scanSelectedRootFolders(fireParam.rootFolderItem, true,false,false,false);
            }
            else if (fireParam.action === 'extract') {
              $scope.scanSelectedRootFolders(fireParam.rootFolderItem, false,false,false,true);
            }
            else if (fireParam.action === 'setRescanState') {
              updateRescanActive(fireParam.rootFolderItem,fireParam.newState);
            }
            else if (fireParam.action === 'markForFullScan') {
              $scope.markForFullScan(fireParam.rootFolderItem, fireParam.mark);
            }
          })));

      $scope.continueRunSelectedRootFolder = function(rootFolderItem:Operations.DtoRootFolder) {
        scanService.resumeRunsByID([rootFolderItem.lastRunId], [rootFolderItem.nickName || rootFolderItem.realPath]);
      };

      $scope.pauseSelectedRootFolderRuns = function(rootFolderItem:Operations.DtoRootFolder) {
        scanService.pauseRunsByID([rootFolderItem.lastRunId], [rootFolderItem.nickName || rootFolderItem.realPath]);
      };

      $scope.stopRunSelectedRootFolder = function(rootFolderItem) {
        scanService.stopRunsByID([rootFolderItem.lastRunId], [rootFolderItem.nickName || rootFolderItem.realPath]);
      };

      $scope.scanSelectedRootFolders =function(rootFolderItem:Operations.DtoRootFolder,isScan,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean) {
          scanService.startRunRootFolders([rootFolderItem.id], [rootFolderItem.nickName || rootFolderItem.realPath], isScan,isCompleteScanNoExtract, isCompleteScanWithExtract, isExtractOnly);
      };

      $scope.markForFullScan = function(rootFolderItem:Operations.DtoRootFolder, mark: boolean){
        scanService.markForFullScan(rootFolderItem.id, mark);
      };
      $scope.$on(scanService.rootFoldersAggregatedSummaryInfoEvent, function (sender, value) {
        $scope.reloadData();
      });
    })
    .directive('rootFoldersManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/operations/rootFolders/rootFoldersManageMenu.tpl.html',
            controller: 'rootFoldersManageMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'selectedListItemToOperate':'=',
              'reloadData':'=',
              'gridFindId':'=',
              'refreshDisplay':'=',
              'disableAdd':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    );




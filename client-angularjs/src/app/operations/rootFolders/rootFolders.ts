///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */

declare var _:any;

angular.module('operations.fileCrawling.rootFolders',[

])
  .controller('FileCrawlingRootFoldersCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,$http,$filter,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                                       propertiesUtils,apiUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var log:ILog = Logger.getInstance('FileCrawlingRootFoldersCtrl');

    $scope.elementName = 'root-folders-grid';
    $scope.entityDisplayType = 'rootFolders';
    var gridOptions:ui.IGridHelper;

    $scope.init = function (restrictedEntityDisplayTypeName) {
      $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

      $scope.lazyFirstLoad = $scope.restrictedEntityDisplayTypeName?true:false;
      gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
        fields,parseData ,$scope.fullDetails?rowTemplateFullDetails: rowTemplateMinDetails,
        $stateParams, null,getDataUrlBuilder, true,$scope.fixHeight);

      var doNotSelectFirstRowByDefault = !$scope.includeRunningDetails;
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
      GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,doNotSelectFirstRowByDefault,null,
        $scope.fixHeight!=null,null,$scope.forceSelectFirstRowByDefault!=null);

      //    $scope.mainGridOptions = gridOptions.gridSettings;

    };

    var setUserRoles = function()
    {
      $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
      $scope.isRunScansUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
    };
    var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {
      var params=<any>{};
      if(includeSubEntityData)
      {
        includeSubEntityData.filter?params.states=(<Operations.ERunStatus[]>includeSubEntityData.filter).join():null;
        includeSubEntityData.sort?params.sort=angular.toJson(includeSubEntityData.sort):null;
      }

      if( $scope.restrictedEntityDisplayTypeName == 'scheduleGroup' )
      {
        if(restrictedEntityId) {
          var url = configuration.root_folders_for_scheduleGroup_url.replace(':scheduleGroupId', restrictedEntityId);
          url = apiUtils.buildUrl(url,params);
          if($scope.filterBySearchText) {
            url = url.replace(':namingSearchTerm', $scope.filterBySearchText);
          }
          else {
            url =url.replace(':namingSearchTerm', '').replace('namingSearchTerm=', '');
          }
          return url;
        }
        $scope.clear?$scope.clear():null;
        return null;
      }
      var url = configuration.root_folders_summary_url;
      url = apiUtils.buildUrl(url,params);
      if($scope.filterBySearchText && $scope.filterBySearchText.trim()!='' ) {
        url = url.replace(':namingSearchTerm',encodeURIComponent($scope.filterBySearchText));
      }
      else {
        url =url.replace(':namingSearchTerm', '').replace('namingSearchTerm=', '');
      }
      return url;

    }

    var getDataUrlParams = function () {
      return null;
    };
    $scope.fullDetails = $scope.fullDetails?propertiesUtils.parseBoolean($scope.fullDetails):null;
    var dtoRootFolder:Operations.DtoRootFolder =  Operations.DtoRootFolder.Empty();
    var dtoCrawlRunDetails =  Operations.DtoCrawlRunDetails.Empty();
    var dtoCustomerDataCenter =  Operations.DtoCustomerDataCenter.Empty();
    var DdtoRootFolderSummaryInfo =  Operations.DtoRootFolderSummaryInfo.Empty();
    var dtoScheduleGroup =  Operations.DtoScheduleGroup.Empty();
    var dtoDepartment = Operations.DtoDepartment.Empty();
    var prefixRootFolderFieldName = propertiesUtils.propName(DdtoRootFolderSummaryInfo, DdtoRootFolderSummaryInfo.rootFolderDto) + '.';
    var prefixRunDetailsFieldName =propertiesUtils.propName(DdtoRootFolderSummaryInfo, DdtoRootFolderSummaryInfo.crawlRunDetailsDto) + '.';
    var prefixScheduleGroupFieldName =propertiesUtils.propName(DdtoRootFolderSummaryInfo, DdtoRootFolderSummaryInfo.scheduleGroupDtos) + '.';
    var prefixDepartmentFieldName =propertiesUtils.propName(DdtoRootFolderSummaryInfo, DdtoRootFolderSummaryInfo.departmentDto) + '.';
    var prefixCustomerDataCenterDtoFieldName =propertiesUtils.propName(dtoRootFolder, dtoRootFolder.customerDataCenterDto) + '.';
    var selectedDataCenter = prefixRootFolderFieldName+prefixCustomerDataCenterDtoFieldName+ propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.id);


    var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.id),
      title: 'Id',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: true,
      width:'35px'
    };

    var scanErrors:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanErrors),
      title: 'Map errors',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template:'<span ng-show="dataItem.'+ prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanErrors)+'>0">' +
      '<span class="top-right-position" ><a class="btn btn-link btn-xs " target="_blank" ' +
      'ui-sref="processingErrors({phase:\'scan\',rootFolderId:dataItem.'+id.fieldName+'})">' +
      '<i class="fa fa-info ng-hide"></i></a>' +
      '</span>' +
      '<span>#= '+ prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanErrors)+' #</span></span> '

    };
    var scanProcessingErrors:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanProcessingErrors),
      title: 'Processing errors',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template:'<span ng-show="dataItem.'+ prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanProcessingErrors)+'>0"><span class="top-right-position" ><a class="btn btn-link btn-xs " target="_blank" ' +
      'ui-sref="processingErrors({phase:\'ingestion\',rootFolderId:dataItem.'+id.fieldName+'})">' +
      '<i class="fa fa-info ng-hide"></i></a>' +
      '</span>' +
      '<span>#= '+ prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanProcessingErrors)+' #</span></span> '
    };


    var path:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath),
      title: 'Path',
      type: EFieldTypes.type_string,
      displayed: true,
      exportTitle:"PATH",
      editable: false,
      nullable: false,
      width: "100%",
      template:  '<span title="#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+' # ">' +
      '   <span class="title-secondary" ng-show="!dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+'">#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+' # </span>' +
      '   <span class="title-secondary" ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+'">#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+' # </span>' +
      '<span ng-show="!dataItem.accessible && !dataItem.accessibleUnknown" title="Root folder not accessible" class="fa fa-exclamation-triangle notice"></span>'+
      '<span ng-show="dataItem.accessibleUnknown" title="Accessibility unknown" class="fa fa-question title-third"></span>'+
      '</span>'

    };

    var pathNewTemplate =  '<span class="dropdown-wrapper dropdown"><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"  >'  +
      '<span class="text-with-hidden-big-caret" title="#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+' # ">' +
      '  <a class="title-primary" ng-show="!dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+'">#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+' # </a>' +
      '  <a  ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+'" ><span style="display:inline-block"  class="title-primary">#= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName)+' #</span>' +
      '<span class="title-third" ">&nbsp;&nbsp; #= '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+' #</span></a>' +
      '<span style="margin-left: 4px;" ng-show="!dataItem.accessible && !dataItem.accessibleUnknown" title="Root folder not accessible" class="fa fa-exclamation-triangle notice"></span>'+
      '<span style="margin-left: 4px;" ng-show="dataItem.accessibleUnknown" title="Accessibility unknown" class="fa fa-question title-third"></span>'+
      '<span style="margin-left: 4px;" ng-show="dataItem.isRunning&&'+$scope.includeRunningDetails+'" class="fa fa-cog fa-spin " title="Processing running"></span>'+
      '<span style="margin-left: 4px;" ng-show="dataItem.shareFolderPermissionError" title="Share permission error" class="fa fa-user-circle error"></span>'+
      '</span> ' +
      '<i class="caret ng-hide"></i></span>'+
      '<ul class="dropdown-menu fixed-icon"  > '+
      '<li ng-if="isMngScanConfigUser"><a  ng-click="openEditDialog(dataItem,null)"><div class="icon fa fa-pencil"></div><div>Edit</div</a></li>'+
      '<li ng-if="(isMngScanConfigUser && !dataItem.rootFolderDto.deleted && (!dataItem.crawlRunDetailsDto || !dataItem.crawlRunDetailsDto.runOutcomeState || dataItem.crawlRunDetailsDto.runOutcomeState != \'' + Operations.ERunStatus.RUNNING + '\'  ))"><a  ng-click="openDeleteDialog(dataItem)"><div class="icon fa fa-trash-o"></div><div>Delete</div</a></li>'+

      '<li ng-if="isRunScansUser || isMngScanConfigUser" class="divider"></li>'+
      '<li ng-if="isRunScansUser" class="dropdown-submenu">'+
      '<a><div class="icon"></div><div style="flex:1;">Actions</div></a>'+
      '<ul class="dropdown-menu fixed-icon">'+
      '<li ng-show="dataItem.isRunning"><a ng-click="pauseSelectedRootFolderRuns(dataItem)" title="Pause scan for selected root folders"><i class="fa fa-pause notice-info icon"></i>Pause scan</a></li>'+
      '<li ng-show="dataItem.isPaused"><a ng-click="continueRunSelectedRootFolder(dataItem)" title="Resume paused scan for selected root folders"><i class="fa fa-repeat icon"></i>Resume scan</a></li>'+
      '<li ng-show="dataItem.isPaused"><a ng-click="stopRunSelectedRootFolder(dataItem)" title="stop scan for selected root folders"><i class="fa fa-stop icon"></i>Stop scan</a></li>'+
      '<li ng-class="{disabled:!(!dataItem.isPaused && !dataItem.isRunning)}" ng-show="(!dataItem.isPaused && !dataItem.isPaused)"><a ng-click="scanSelectedRootFolders(dataItem)" title="Run all steps for selected root folders"><i class="fa fa-play icon"></i>Scan</a></li>'+
      '<li class="divider" ng-if="(!dataItem.isPaused && !dataItem.isRunning)"></li>'+
      '<li ng-show="(!dataItem.isPaused && !dataItem.isRunning)"><a ng-click="mapSelectedRootFolders(dataItem)" title="Run map only for selected root folders"><i class="fa fa-play icon"></i>Map</a></li>'+
//       '<li ng-show="(!dataItem.isPaused && !dataItem.isRunning)"><a ng-click="extractSelectedRootFolders(dataItem)" title="Extract all active biz lists for selected root folders"><i class="fa fa-external-link icon"></i>Extract business lists</a></li>'+
      '</ul>'+
      '</li>'+
      '<li class="dropdown-submenu">'+
      '<a><div class="icon"></div><div style="flex:1;">Navigation</div></a>'+
      '<ul class="dropdown-menu fixed-icon">'+
      '<li><a title="Open discover root folder in a new tab " ng-click="openInReportPage(dataItem)"><div class="icon ion-android-exit" ></div><div>Go discover</div></a></li>'+
      '<li><a clipboard text="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.realPath)+'" title="copy path to clipboard"><div class="icon fa fa-copy" ></div><div>Copy full path</div></a></li>'+
      '<li ng-if="isRunScansUser"><a class="btn btn-link btn-xs " ng-click="openScanHistory(dataItem)" target="_blank"  title="View scan history of this rootfolder in a new tab"><div class="icon" ></div><div>View scan history</div></a></li>'+
      '<li><a class="btn btn-link btn-xs " target="_blank"  title="View data center of this rootfolder in a new tab"' +
      'ui-sref="'+ERouteStateName.settings+'({activeTab:\'customerDataCenters\',selectedItem:dataItem.' + selectedDataCenter + '})"><div class="icon" ></div><div>View data center</div></a></li>'+
      // '<li ng-if="isRunScansUser"><a class="btn btn-link btn-xs " target="_blank"  title="open change log page"' +
      // 'ui-sref="'+ERouteStateName.processingErrors+'({phase:\'changelog\',rootFolderId:dataItem.'+id.fieldName+'})"><div class="icon" ></div><div>View change log</div></a></li>'+
      '<li  ng-show="dataItem.'+ scanErrors.fieldName+'>0" ng-if="isRunScansUser"><a class="btn btn-link btn-xs " target="_blank" ' +
      'ui-sref="'+ERouteStateName.processingErrors+'({phase:\'scan\',rootFolderId:dataItem.'+id.fieldName+'})">' +
      '<div class="icon" ></div><div>View map errors</div></a></li>'+
      '<li ng-show="dataItem.'+ scanProcessingErrors.fieldName+'>0" ng-if="isRunScansUser"><a class="btn btn-link btn-xs " target="_blank" ' +
      'ui-sref="processingErrors({phase:\'ingestion\',rootFolderId:dataItem.'+id.fieldName+'})">' +
      '<div class="icon" ></div><div>View ingest errors</div></a></li>'+
      '</ul>'+
      '</li>'+
      '<li class="dropdown-submenu">'+
      '<a><div class="icon"></div><div style="flex:1;">Advanced</div></a>'+
      '<ul class="dropdown-menu fixed-icon">'+
      `<li ng-if="!dataItem.${prefixRootFolderFieldName}${propertiesUtils.propName(dtoRootFolder, dtoRootFolder.reingest)}"><a ng-click="markForFullScan(dataItem, true)" title="Mark selected rootfolders for full scan (re-ingest) files on next scan"><i class="fa fa-thumb-tack icon"></i><span>Mark for full scan</span></a></li>`+
      `<li ng-if="dataItem.${prefixRootFolderFieldName}${propertiesUtils.propName(dtoRootFolder, dtoRootFolder.reingest)} "><a ng-click="markForFullScan(dataItem, false)" title="Unmark selected rootfolders for full scan (re-ingest) files on next scan"><span style="padding-left:10px;">Unmark for full scan</span></a></li>`+
      '</ul>'+
      '</li>'+
      ' </ul></span> ';


    $scope.openScanHistory = function(dataItem:Operations.DtoRootFolderSummaryInfo) {
      $window.open('/scanni/ScanHistory?rootFolderId='+ dataItem.rootFolderDto.id +'&rootFolderName='+dataItem.rootFolderDto.nickName,'_top');
    }
    $scope.openInReportPage = function(dataItem:Operations.DtoRootFolderSummaryInfo)
    {
      $state.params['left'] =  EntityDisplayTypes.folders;
      $state.params['right'] =  EntityDisplayTypes.files;
      $state.params['selectedItemLeft']=   encodeURIComponent(dataItem.rootFolderDto.path); //.replace(/[/]/g,'\\');
      $state.params['activePan'] = 'left';

      var url = $state.href('report', $state.params);
      $window.open(url,'_blank');
    }

    var numberOfFoldersFound:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.numberOfFoldersFound),
      title: '# found folders',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      width:'120px',
      noWrapContent: true,
      template: '<span  ng-if=" dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.numberOfFoldersFound)+'">  ' +
      '<span title=" Found folders" class="fileType fa fa-folder-o"></span> #= '+  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.numberOfFoldersFound)+' #</span>  '
    };

    var scannedLimitCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'scannedLimitCount',
      title: 'scanned limit count',
      type: EFieldTypes.type_string,
      exportTitle:"SCANNED LIMIT COUNT",
      displayed: true,
      editable: false,
      nullable: false,
      width:'120px',
      noWrapContent: true,
      template:'<span translate="{{dataItem.scannedLimitCount}}"></span>'
    };

    var meaningfulFilesScanLimitCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'meaningfulFilesScanLimitCount',
      title: 'Meaningful files scan limit count',
      type: EFieldTypes.type_string,
      exportTitle:"MEANINGFUL FILES SCANNED LIMIT COUNT",
      displayed: true,
      editable: false,
      nullable: false,
      width:'120px',
      noWrapContent: true,
      template:'<span translate="{{dataItem.scannedMeaningfulFilesCountCap}}"></span>'
    };


    var numOfSubFoldersNewTemplate = '<span ng-if="dataItem.'+prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.totalProcessedFiles)+'" class="title-third" style="margin-right: 6px;">#: '+prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.totalProcessedFiles)+' #  files ' +
      //  '# if('+ prefixFieldName+propertiesUtils.propName(dTOFolderInfo, dTOFolderInfo.numOfSubFolders)+'>0) {#, #: kendo.toString('+ numOfAllFiles.fieldName+'-'+numOfDirectFiles.fieldName+',"n0") # files in
      ' in #: '+  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.numberOfFoldersFound)+' # subfolders ' +
      '</span>';
    var docStoreId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.docStoreId),
      title: 'docStoreId',
      type: EFieldTypes.type_number,
      displayed: false,
      editable: false,
      nullable: false,

    };
    var status:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'status',
      title: 'Status',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      width:'80px',
      template:'<span translate="{{dataItem.status}}"></span>'+
      '&nbsp;<span ng-show="dataItem.isEndedSuccessfully" class="fa fa-check primary" title="Last processing ended successfully"></span>' +
      '&nbsp;<span ng-show="dataItem.isEndedWithError" class="fa fa-exclamation-triangle notice" title="Last processing failed"></span>'

    };
    var scheduleGroupName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'scheduleGroupName',
      title: 'Schedule group',
      type: EFieldTypes.type_string,
      exportTitle:"SCHEDULE GROUP NAME",
      displayed: true,
      editable: false,
      nullable: false,
      template:'<a ng-click="openEditDialog(dataItem,\'scan\')" title="{{dataItem.scheduleGroupDescription+(!dataItem.scheduleGroupRescanActive?\' (not active)\':\'\')}}" >#= scheduleGroupName #</a>'

    };

    var rescanActive:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.rescanActive),
      title: 'Rescan',
      type: EFieldTypes.type_boolean,
      exportTitle:"RESCAN",
      displayed: true,
      editable: false,
      nullable: false,
      centerText: true,
      width:'80px',
      template:'<a ng-click="openEditDialog(dataItem,\'general\')" >'+
      '<span ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.rescanActive)+'"  title="Rescan is active"><span class="fa fa-plus ng-hide"></span> On</span>' +
      ' <span ng-show="!dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.rescanActive)+'" title="Rescan is not active" class="notice"><span class="fa fa-minus ng-hide" ></span> Off</span>'+
      '</a>'

    };

    var extractBizLists:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.extractBizLists),
      title: 'Extract business lists',
      type: EFieldTypes.type_boolean,
      exportTitle:"EXTRACT BUSINESS LISTS",
      displayed: true,
      editable: false,
      nullable: false,
      centerText: true,
      template:'<a ng-click="openEditDialog(dataItem,\'fileTypes\')">'+
      '<span ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.extractBizLists)+'" class="fa fa-plus" title="Extract business lists is active"></span>' +
      ' <span ng-show="!dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.extractBizLists)+'" class="fa fa-minus" title="Extract business lists is not active"></span>'+
      '</a>'

    };

    var extractBizListsTemplate='<a ng-click="openEditDialog(dataItem,\'fileTypes\')">'+
      '<span ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.extractBizLists)+'" class="'+configuration.icon_extractEntities+'" title="Extract business lists is active"></span>' +
      ' <span ng-show="!dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.extractBizLists)+'" class1="fa fa-minus1" title="Extract business lists is not active"></span>'+
      '</a>';
    var folderExcludeRulesCount:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.folderExcludeRulesCount),
      title: '# Excluded folders rules',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      template:'<a ng-click="openEditDialog(dataItem,\'excludedFolders\')" >'+
      '<span ng-show="dataItem.'+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.folderExcludeRulesCount)+'>0"  title="Number of Excluded folders rules"' +
      '>#: '+prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.folderExcludeRulesCount)+' #</span>'+
      '</span>'
    };

    var folderExcludedTemplate = '<span class="title-third" style="white-space: nowrap;">  '+
      '<span ng-show="dataItem.'+folderExcludeRulesCount.fieldName+'>0"  title="Number of Excluded folders rules"' +
      '><a ng-click="openEditDialog(dataItem,\'excludedFolders\')" ><span class="'+configuration.icon_excludedfolder+'"></span> #: '+folderExcludeRulesCount.fieldName+' #</a> </span>'+
      '</span>';
    var runId:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRunDetailsFieldName+propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.id),
      title: 'Run Id',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false
    };


    var mediaType:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.mediaType), //Bug - translated data not returned when field nested in 2 objects
      // fieldName: propertiesUtils.propName(dtoRootFolder, dtoRootFolder.mediaType)+'translated',
      title: 'Media type',
      type: EFieldTypes.type_string,
      exportTitle:'MEDIA TYPE',
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      width:'100px',
      //parse: function(ts: string) {
      //  var $translate = $filter('translate');
      // return $translate(ts);
      //},
      template:'<span translate="{{dataItem.'+prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.mediaType)+'}}"></span>'
    };
    var storeLocation:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.storeLocation),
      title: 'Store location',
      type: EFieldTypes.type_string,
      exportTitle:'STORE LOCATION',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var description:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.description),
      title: 'Description',
      type: EFieldTypes.type_string,
      exportTitle:'DESCRIPTION',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var storePurpose:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.storePurpose),
      title: 'Store purpose',
      type: EFieldTypes.type_string,
      exportTitle:'STORE PURPOSE',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var storeSecurity:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.storeSecurity),
      title: 'Store security',
      type: EFieldTypes.type_string,
      exportTitle:'STORE SECURITY',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var nickName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.nickName),
      title: 'Nick name',
      type: EFieldTypes.type_string,
      exportTitle:'NICK NAME',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var fileTypes:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: 'fileTypesAsString',
      title: 'Scanned file types',
      type: EFieldTypes.type_string,
      exportTitle:'SCANNED FILE TYPES',
      displayed: true,
      editable: false,
      nullable: false,
    };

    var scanTaskDepth:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ 'scanTaskDepth',
      title: 'scanTaskDepth',
      type: EFieldTypes.type_string,
      exportTitle:'SCAN TASK DEPTH',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var mediaConnectionName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.mediaConnectionName),
      title: 'Media connection name',
      type: EFieldTypes.type_string,
      exportTitle:'MEDIA CONNECTION NAME',
      displayed: true,
      editable: false,
      nullable: false,
    };
    var customerDataCenterName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+prefixCustomerDataCenterDtoFieldName+ propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name),
      title: 'Datacenter',
      type: EFieldTypes.type_string,
      exportTitle:'CUSTOMER DATA CENTER',
      displayed: true,
      editable: false,
      nullable: false,
      template:'<a ng-click="openEditDialog(dataItem,\'scan\')" title="Customer datacenter, that this folder will be procssesd by" >#= '+prefixRootFolderFieldName+prefixCustomerDataCenterDtoFieldName+ propertiesUtils.propName(dtoCustomerDataCenter, dtoCustomerDataCenter.name)+' #</a>'
    };

    var departmentName:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRootFolderFieldName+propertiesUtils.propName(dtoRootFolder, dtoRootFolder.departmentName),
      title: 'Department',
      type: EFieldTypes.type_string,
      exportTitle: 'DEPARTMENT',
      displayed: true,
      editable: false,
      nullable: false
    };
    var totalProcessedFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.totalProcessedFiles),
      title: '# processed files',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,

    };
    var processedExcelFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedExcelFiles),
      title: 'MS-Excel processed',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template: '<span  ng-if=" dataItem.'+prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedExcelFiles)+'">  ' +
      '<span title=" MS-Excel processed files" class="fileType fa fa-file-excel-o"></span> #= '+  prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedExcelFiles)+' #</span>  '

    };
    var processedPdfFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedPdfFiles),
      title: 'Pdf processed',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template: '<span  ng-if=" dataItem.'+prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedPdfFiles)+'">' +
      '<span title=" Pdf processed files" class="fileType fa fa-file-pdf-o"></span> #= '+  prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedPdfFiles)+' #</span>  '

    };


    var processedWordFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedWordFiles),
      title: 'MS-Word processed',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template: '<span ng-if=" dataItem.'+prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedWordFiles)+'">' +
      '<span title=" MS-Word processed files" class="fileType fa fa-file-word-o"> </span> #= '+  prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedWordFiles)+' #</span> '
    };
    var processedOtherFiles:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName: prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedOtherFiles),
      title: 'Other processed',
      type: EFieldTypes.type_number,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      template: '<span ng-if=" dataItem.'+prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedOtherFiles)+'" > ' +
      '<span title=" Other processed files" class="fileType fa fa-file-o"></span> #= '+  prefixRunDetailsFieldName+  propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.processedOtherFiles)+' #</span> '


    };


    var lastRunStartTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRunDetailsFieldName+ propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanStartTime)+'Str',
      title: 'last Run Start Time',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      width:'130px',
      format:'{0:'+configuration.dateTimeFormat+'}',

    };

    var lastRunEndTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:'lastRunEndTime'+'Str',
      title: 'last Run End Time',
      type: EFieldTypes.type_dateTime,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      format:'{0:'+configuration.dateTimeFormat+'}',

    };
    var accessible:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'accessibleStr',
      title: 'Accessibility',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };
    var reingestTimeStampMs:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'reingestTimeStampSec',
      title: 'Reingest date',
      type: EFieldTypes.type_number,
      exportTitle:'REINGEST TIME IN SECONDS',
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      //       format:'{0:'+configuration.dateTimeFormat+'}'
    };
    var fromDateScanFilter:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:   prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.fromDateScanFilter),
      title: 'From Date Scan Filter',
      type: EFieldTypes.type_number,
      exportTitle:'FROM DATE SCAN FILTER',
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true
    };
    var reingest:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.reingest),
      title: 'Reingest',
      type: EFieldTypes.type_string,
      exportTitle:'REINGEST',
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };

    var lastRunElapsedTime:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  'lastRunElapsedTimeSpan',
      title: 'Elapsed Time',
      type: EFieldTypes.type_string,
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
      centerText: true,
      width:'80px',
      template: '<span ng-if="!dataItem.isRunning" title="Ended at {{dataItem.'+ prefixRunDetailsFieldName +
      propertiesUtils.propName(dtoCrawlRunDetails, dtoCrawlRunDetails.scanEndTime)+'| date:\''+configuration.dateTimeFormat+' \'}}" > ' +
      '#= lastRunElapsedTimeSpan #</span> '+
      '<span ng-if="dataItem.isRunning" title="Scan running" > ' +
      '#= lastRunElapsedTimeSpan #</span>'
    };

    var mailboxGroup:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
      fieldName:  prefixRootFolderFieldName+ propertiesUtils.propName(dtoRootFolder, dtoRootFolder.mailboxGroup),
      title: 'Mailbox Group',
      type: EFieldTypes.type_string,
      exportTitle:'MAILBOX GROUP',
      displayed: true,
      editable: false,
      nullable: false,
      noWrapContent: true,
    };


    var fields:ui.IFieldDisplayProperties[] =[];
    fields.push(id);
    fields.push(path);

    fields.push(mediaType);
    if($scope.fullDetails) {
      fields.push(nickName);
      fields.push(fileTypes);
      fields.push(customerDataCenterName);

      fields.push(rescanActive);
      fields.push(reingest);
      fields.push(reingestTimeStampMs);
      fields.push(storeLocation);
      fields.push(storePurpose);
      fields.push(storeSecurity);
      fields.push(description);
      //     fields.push(rootFolderState);
      fields.push(mailboxGroup);
      fields.push(mediaConnectionName);
      if(!$scope.includeRunningDetails) {
        fields.push(scheduleGroupName);
      }
      fields.push(extractBizLists);
      fields.push(scanTaskDepth);
      fields.push(status);
      fields.push(departmentName);

    }
    //   fields.push(docStoreId);

    if($scope.includeRunningDetails) {
      fields.push(lastRunElapsedTime);
      fields.push(lastRunStartTime);
      fields.push(lastRunEndTime);

      fields.push(runId);
    }


    if($scope.fullDetails) {
      fields.push(folderExcludeRulesCount);
    }

    fields.push(numberOfFoldersFound);
    if($scope.fullDetails) {
      fields.push(accessible);
      fields.push(totalProcessedFiles);
      fields.push(processedPdfFiles);
      fields.push(processedWordFiles);
      fields.push(processedExcelFiles);
      fields.push(processedOtherFiles);
      fields.push(scanErrors);
      fields.push(scanProcessingErrors);
      fields.push(scannedLimitCount);
      fields.push(meaningfulFilesScanLimitCount);
      fields.push(fromDateScanFilter);
    }

    var fileTypesTemplate= splitViewBuilderHelper.getFileTypesTemplate (processedExcelFiles.fieldName,processedPdfFiles.fieldName,processedWordFiles.fieldName,processedOtherFiles.fieldName);
    var errorsTemplate= splitViewBuilderHelper.getFileProcessingErrorsTemplate(scanErrors.fieldName,scanProcessingErrors.fieldName);
    var scanTimingsTemplate= splitViewBuilderHelper.getLastRunTimingTemplate(lastRunStartTime.fieldName,lastRunElapsedTime.fieldName,lastRunEndTime.fieldName,'isRunning');
    var scheduleTemplate='<span class="'+configuration.icon_scheduleGroup+'" ng-class="{\'notice\':!dataItem.scheduleGroupRescanActive}"></span>&nbsp;'+ scheduleGroupName.template;
    var statusTemplate= splitViewBuilderHelper.getStatusTemplate(status.fieldName,'isEndedSuccessfully','isEndedWithError','isPaused');

    var markForFullScanTemplate = `
        <div ng-show="dataItem.${prefixRootFolderFieldName}${propertiesUtils.propName(dtoRootFolder, dtoRootFolder.reingest)}" style="padding:5px; box-sizing: border-box; width:20px;" title="Mark selected rootfolders for full scan (re-ingest) files on next scan">
          <div class="fa fa-thumb-tack"></i></div>
        </div>`;

    var dataCenterTemplate =
      `<div class='centerText data-center-col'>
          <div class="title-third"><span>Data center</span></div>
          <div class="title-secondary">
             <div class="data-center-name" translate="{{translatePrefix+dataItem.${customerDataCenterName.fieldName}}}" title="{{translatePrefix+dataItem.${customerDataCenterName.fieldName}}}"></div>
             <div class="data-center-icon" ng-if="dataItem.isDataCenterReady"><span class="fa fa-check primary" title="Data center is ready"></span></div>
             <div class="data-center-icon not-ready" ng-if="!dataItem.isDataCenterReady"><span class="fa fa-times" title="Data center is not ready"></span></div>
          </div>
        </div>`;

    var departmentTemplate =
      `<div class="title-secondary ellipsis" ng-if="dataItem.${departmentName.fieldName}" title="{{translatePrefix+dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.fullName)}}}">
            <span class="icon fa fa-building tag-{{getIconStyleId('${configuration.max_departments_colors}', 
             dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.id)},
             dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.parentId)})}}
              opacity-depth-{{getIconStyleId('${configuration.max_departments_colors}',
               dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.id)}, 
               dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.parentId)})}}">
            </span>
           &nbsp{{dataItem.${departmentName.fieldName}}}
          <div class="title-third">{{ dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.fullName)}}} </div>
        </div>`;

    if (currentUserDetailsProvider.isInstallationModeLegal()) {
      departmentTemplate =
        `<div class="title-secondary ellipsis" ng-if="dataItem.${departmentName.fieldName}" title="{{translatePrefix+dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.fullName)}}}">
            <span class="icon icon-da-special-matter-icon tag-{{getIconStyleId('${configuration.max_departments_colors}', 
             dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.id)},
             dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.parentId)})}}
              opacity-depth-{{getIconStyleId('${configuration.max_departments_colors}',
               dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.id)}, 
               dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.parentId)})}}">
            </span>
           &nbsp{{dataItem.${departmentName.fieldName}}}
          <div class="title-third">{{ dataItem.${prefixDepartmentFieldName}${propertiesUtils.propName(dtoDepartment, dtoDepartment.fullName)}}} </div>
        </div>`;
    }

    var rescanActiveTemplate =splitViewBuilderHelper.getToggleButtonTemplate(rescanActive.fieldName,'rescan',"toggleRescanState(dataItem)");

    var visibleFileds = fields.filter(f=>f.displayed);

    var rowTemplateFullDetails="<tr  ng-class='{\"freezeRow disabled1\":dataItem.rootFolderDto.deleted}'  data-uid='#: uid #' colspan='"+fields.length+"'>" +
      "<td colspan='1' class='icon-column' style='padding-top: 1px;'> \
        <da-icon  title='{{dataItem."+mediaType.fieldName+"|translate}}' href='rootfolder' ></da-icon> \
           </td>"+
      //"<td colspan='1' class='icon-wrap-2-lines' style='padding: 3px 8px;'>   \
      // <da-icon class='icon-wrap icon-wrap-2-lines' title='{{dataItem."+mediaType.fieldName+"|translate}}' href='rootfolder' ></da-icon> \
      //</td>"+
      "<td colspan='"+(visibleFileds.length-($scope.includeRunningDetails?9:8))+"'  style='width: 100%;max-width:440px;word-wrap:break-word' class='ddl-cell break-text'>"+
      "<span class='pull-right' style='max-width: 46%;'>" +
      '<span ng-if="dataItem.rootFolderDto.deleted">  ( Deleting folder )</span>' +
      "<div style='clear:right'></div>"+
      "</span>" +
      pathNewTemplate+"<div class='title-third' >"+numOfSubFoldersNewTemplate+"</div>" +
      "</td>" +
      `<td colspan='1' width='25px' >${markForFullScanTemplate}</td>` +
      "<td colspan='1' style='width: 40%; min-width: 150px;'>" + departmentTemplate + "</td>" +
      "<td colspan='1' width='90px' >"+statusTemplate+"</td>" +
      "<td colspan='1' width='50px'>"+rescanActiveTemplate+"</td>"+
      "<td colspan='1' width='70px'  ng-title='{{dataItem." + mediaType.fieldName+ "|translate}};{{dataItem." +mediaConnectionName.fieldName+ "?'Media connection: ' +dataItem." +mediaConnectionName.fieldName+ ":''}}' >" +
      "<div class='title-third'>Media type</div>"+
      "<div class='repositoryType {{dataItem."+mediaType.fieldName+"}} center'></div>" +
      "</td>"+
      "<td colspan='1' width='90px' >"+dataCenterTemplate+"</td>" +
      ($scope.includeRunningDetails?  "<td colspan='1' width='130px' >"+scanTimingsTemplate+"</td>":'')+

      //"<td colspan='1' width='40%' style='text-align: center'><div style='text-align: left; display: inline-block;'>"+ fileTypesTemplate+"</div></td>"+
      '<td colspan="1" style="width:110px;" >'+errorsTemplate+'</td>'+
      '<td colspan="1" style="'+($scope.includeRunningDetails? 'width:40px;':'width:100px;')+'" class="middleText">'+extractBizListsTemplate+'&nbsp;'+folderExcludedTemplate+'' +
      (!$scope.includeRunningDetails?  "<div class='title-secondary ellipsis'>"+scheduleTemplate+"</div>":'')+
      '</td>'+
      "</tr>";

    var rowTemplateMinDetails="<tr  ng-class='{\"freezeRow disabled1\":dataItem.rootFolderDto.deleted}' data-uid='#: uid #' colspan='"+fields.length+"'>" +
      "<td colspan='1' class='icon-column' style='padding-top: 1px;'><span title='{{dataItem."+mediaType.fieldName+"|translate}}' class='"+configuration.icon_rootfolder+"'></span></td>"+
      "<td colspan='"+(visibleFileds.length-2)+"'  style='width: 100%;max-width:440px;' class='ddl-cell break-text'>"+
      "<span class='pull-right' style='max-width: 46%;'>" +
      "<span class='pull-right' title='{{dataItem."+mediaType.fieldName+"|translate}}' ><span  class='repositoryType {{dataItem."+mediaType.fieldName+"}}' style='  width: 20px ; height:23px;'></span></span>" +
      '<span ng-if="dataItem.rootFolderDto.deleted">  ( Deleting folder )</span>' +
      "<div style='clear:right'></div>"+
      "</span>" +
      path.template+"<div class='title-third' >"+numOfSubFoldersNewTemplate+"</div>" +
      "</td>"  +
      "<td colspan='1' width='40%' style='text-align: center'><div style='text-align: left; display: inline-block;'>"+ fileTypesTemplate+"</div></td>"+


      "</tr>";


    var parseData = function (rootFolders:Operations.DtoRootFolderSummaryInfo[]) {
      if(rootFolders)
      {

        rootFolders.forEach(function(folder:Operations.DtoRootFolderSummaryInfo) {
          (<any>folder).id=folder.rootFolderDto.id;
          folder.crawlRunDetailsDto = folder.crawlRunDetailsDto?folder.crawlRunDetailsDto:new Operations.DtoCrawlRunDetails();
          (<any>folder).isScanned = folder.rootFolderDto.numberOfFoldersFound && folder.rootFolderDto.numberOfFoldersFound > 0 ? true : false;
          (<any>folder).scannedLimitCount =  folder.rootFolderDto.scannedFilesCountCap > 0 ? folder.rootFolderDto.scannedFilesCountCap.toString() : '';
          (<any>folder).meaningfulFilesScanLimitCount = folder.rootFolderDto.scannedMeaningfulFilesCountCap > 0 ? folder.rootFolderDto.scannedMeaningfulFilesCountCap.toString() : '';
          (<any>folder).fileTypesAsString = folder.rootFolderDto.fileTypes? folder.rootFolderDto.fileTypes.join(';'):'';

          (<any>folder).isRunning = folder.runStatus == Operations.ERunStatus.RUNNING;
          (<Operations.DtoRootFolderSummaryInfo>folder).scheduleGroupDtos=folder.scheduleGroupDtos?folder.scheduleGroupDtos:[];

          (<any>folder).status = folder.crawlRunDetailsDto.runOutcomeState?folder.crawlRunDetailsDto.runOutcomeState:'New';
          if ((<any>folder).status == Operations.ERunStatus.RUNNING  && folder.crawlRunDetailsDto  && folder.crawlRunDetailsDto.gracefulStop) {
            (<any>folder).status = Operations.ERunStatus.STOPPING;
          }
          if ((<any>folder).status == Operations.ERunStatus.PAUSED && folder.crawlRunDetailsDto  && folder.crawlRunDetailsDto['pauseReason'] != Operations.PauseReason.USER_INITIATED) {
            (<any>folder).status = Operations.ERunStatus.SUSPENDED;
          }


          (<any>folder).isEndedSuccessfully = folder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FINISHED_SUCCESSFULLY;
          (<any>folder).isEndedWithError = folder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FAILED;
          (<any>folder).isPaused = folder.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.PAUSED;
          var endTime = (<any>folder).isRunning?Date.now():folder.crawlRunDetailsDto.scanEndTime;
          (<any>folder).lastRunEndTime=(!folder.crawlRunDetailsDto.scanEndTime||folder.crawlRunDetailsDto.scanEndTime==0)?null:new Date(folder.crawlRunDetailsDto.scanEndTime);
          (<any>folder).lastRunEndTimeStr =  $filter('date')((<any>folder).lastRunEndTime, configuration.dateTimeFormat);
          (<any>folder).lastRunElapsedTime =endTime? Math.floor(endTime-folder.crawlRunDetailsDto.scanStartTime):null;
          folder.accessible = folder.rootFolderDto.isAccessible == Operations.EDirectoryExistStatus.YES?true:false;  //for excel download beauty
          folder.accessibleUnknown = folder.rootFolderDto.isAccessible != Operations.EDirectoryExistStatus.UNKNOWN?null:true; //for excel download beauty
          (<any>folder).shareFolderPermissionError = folder.rootFolderDto.shareFolderPermissionError;
          (<any>folder).accessibleStr= folder.accessibleUnknown?'Unknown':folder.accessible?'Yes':'No';
          (<any>folder).isDataCenterReady = !!folder.customerDataCenterReady;

          (<any>folder).lastRunElapsedTimeSpan=(<any>folder).lastRunElapsedTime?$filter('formatSpan')((<any>folder).lastRunElapsedTime):'';
          if(folder.scheduleGroupDtos&& folder.scheduleGroupDtos.length>0)
          {
            (<any>folder).scheduleGroupName=folder.scheduleGroupDtos[0].name;
            (<any>folder).scheduleGroupRescanActive=folder.scheduleGroupDtos[0].active;
            (<any>folder).scheduleGroupDescription=folder.scheduleGroupDtos[0].schedulingDescription;
          }
          else {
            (<any>folder).scheduleGroupName='Manually';
            (<any>folder).scheduleGroupDescription='No schedule group configured for this root folder';
          }
          (<any>folder).crawlRunDetailsDto.scanEndTime=(!folder.crawlRunDetailsDto.scanEndTime||folder.crawlRunDetailsDto.scanEndTime==0)?null:new Date(folder.crawlRunDetailsDto.scanEndTime);
          (<any>folder).crawlRunDetailsDto.scanStartTime=(!folder.crawlRunDetailsDto.scanStartTime||folder.crawlRunDetailsDto.scanStartTime==0)?null:new Date(folder.crawlRunDetailsDto.scanStartTime);
          (<any>folder).crawlRunDetailsDto.scanStartTimeStr =  $filter('date')((<any>folder).crawlRunDetailsDto.scanStartTime, configuration.dateTimeFormat);
          (<any>folder).reingestTimeStampSec = folder.rootFolderDto.reingestTimeStampMs?Math.round(folder.rootFolderDto.reingestTimeStampMs/1000):null;

          if(_.isEmpty((<any>folder).rootFolderDto.realPath)) {
            (<any>folder).rootFolderDto.realPath = (<any>folder).rootFolderDto.mailboxGroup || '';
          }
        });
      }
      return rootFolders;
    }

    var onSelectedItemChanged = function()
    {
      var idData=propertiesUtils.getDataFromDotted($scope.selectedItem,id.fieldName);
      var pathData=propertiesUtils.getDataFromDotted($scope.selectedItem,path.fieldName);

      $scope.itemSelected? $scope.itemSelected(idData,pathData,$scope.selectedItem):null;
    }


    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    $scope.openDeleteDialog = function(dataItem:any) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'openDeleteRootFolderItemDialog',
          rootFolderItem: dataItem
        });
      }
    };


    $scope.openEditDialog = function(dataItem:any,triggeredFieldName) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'openEditRootFolderItemDialog',
          rootFolderItem: dataItem,
          triggeredFieldName: triggeredFieldName
        });
      }
    };

    $scope.toggleRescanState = function(dataItem:Operations.DtoRootFolderSummaryInfo) {

      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'setRescanState',
          rootFolderItem: dataItem.rootFolderDto,
          newState: !dataItem.rootFolderDto.rescanActive
        });
      }
    };

    $scope.getIconStyleId = function(max_colors:string, id:number, parentId:number) {
      return propertiesUtils.getIconStyleId(max_colors, id, parentId);
    };

    $scope.continueRunSelectedRootFolder = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'continueRunning',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.pauseSelectedRootFolderRuns = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'pauseRunning',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.stopRunSelectedRootFolder = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'stopRunning',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.scanSelectedRootFolders = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'scan',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.mapSelectedRootFolders = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'map',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.extractSelectedRootFolders = function(dataItem) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'extract',
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    };

    $scope.markForFullScan = function(dataItem, mark) {
      if(_.isNumber($scope.eventName)) {
        eventCommunicator.fireEvent($scope.eventName, {
          action: 'markForFullScan',
          mark: mark,
          rootFolderItem: dataItem.rootFolderDto
        });
      }
    }
  })
  .directive('rootFoldersGrid', function(){
    return {
      restrict: 'EA',
      //template:  '<div class=" {{elementName}}" kendo-grid ng-transclude11 k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
      template:'<div kendo-grid  ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)"' +
      ' k-on-data-bound="onDataBound()" k-options="mainGridOptions"></div>',
      replace: true,
      transclude:true,
      scope:
        {
          totalElements:'=',

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          refreshDataSilently: '=',
          findId: '=',
          fullDetails: '@',
          fixHeight: '@',
          forceSelectFirstRowByDefault: '@',
          includeRunningDetails: '@',
          localDataBound: '=',
          eventName: '=',
          filterBySearchText: '='
        },
      controller: 'FileCrawlingRootFoldersCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(attrs.restrictedentitydisplaytypename);
      }
    }

  });

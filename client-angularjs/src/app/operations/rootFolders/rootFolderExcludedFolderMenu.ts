///<reference path='../../../common/all.d.ts'/>

'use strict';


angular.module('operations.rootFolders.excludedFolders.manage', [
      'resources.operations',
    ])
    .controller('excludedRootFoldersMenuCtrl', function ($window, $scope,$element,Logger:ILogger,$q,
                                             userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs) {
      var log:ILog = Logger.getInstance('excludedRootFoldersMenuCtrl');

      $scope.init = function () {

      };




      $scope.toggleApproveSelectedPendingItems = function () {

        //if ($scope.activeSelectedDocTypeItem && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
        //  var docTypeToToggle = <Entities.DTODocType>$scope.activeSelectedDocTypeItem;
        //  var firstSelectedItem = $scope.selectedDocTypesItemsToTag[0];
        //  var tags = (<any>firstSelectedItem).item ? (<any>firstSelectedItem).item.docTypeDtos : (<any>firstSelectedItem).docTypeDtos;
        //  if (isExplicitDocTypeExists(tags, docTypeToToggle)) {
        //    $scope.removeDocTypeFromSelectedItems();
        //  }
        //  else {
        //    $scope.addDocTypeToSelectedItems();
        //  }
        //}
      }


      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.removeApproveFromSelectedItems= function () {
        //if ($scope.activeSelectedDocTypeItem && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
        //  var docTypeToRemove = <Entities.DTODocType>$scope.activeSelectedDocTypeItem;
        //  var theDocTypeResource:IDocTypeResource = docTypeResource($scope.displayType);
        //  var loopPromises = [];
        //  $scope.selectedDocTypesItemsToTag.forEach(function (selectedItem) {
        //    var removeTag = theDocTypeResource.removeDocType(docTypeToRemove.id, selectedItem['id']);
        //    var deferred = $q.defer();
        //    loopPromises.push(deferred.promise);
        //    removeTag.$promise.then(function (g:any) {
        //          log.debug('receive removeDocType from selected items success for' + selectedItem['id']);
        //          updateDisplayedItemAfterRemove(selectedItem, docTypeToRemove);
        //          deferred.resolve(selectedItem);
        //        },
        //        function (error) {
        //          deferred.resolve();
        //          log.error('receive removeDocType from selected items failure' + error);
        //          onError();
        //        });
        //  })
        //  $q.all(loopPromises).then(function (selectedItemsToUpdate) {
        //    $scope.reloadData(); //no need when auto sync is false
        //  });
        //}
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('excludedRootFoldersManageMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/app/operations/rootFolders/rootFolderExcludedFolderMenu.tpl.html',
            controller: 'excludedRootFoldersMenuCtrl',
            replace:true,
            scope:{
              'selectedListItemsToOperate':'=',
              'reloadData':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init(attrs.left);
            }
          };
        }
    )

///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('operations.rootFolders.manage',[
      'resources.operations',
      'directives.operations',
      'operations.rootFolders.manage.menu',
      'operations.fileCrawling.rootFolders'
    ])
    .config(function ($stateProvider) {

      $stateProvider
          .state(ERouteStateName.rootFoldersManagement, {
            parent: 'operations.manage',
            url: '/rootfolders?&selectedRootFolderId?&page?searchText?findId?',
            templateUrl: '/app/operations/rootFolders/rootFoldersManagePage.tpl.html',
            controller: 'RootFoldersManagePageCtrl',
            data: {
              authorizedRoleNames: [Users.ESystemRoleName.MngScanConfig,Users.ESystemRoleName.RunScans]
            }
          })

    })
    .controller('RootFoldersManagePageCtrl', function ($scope, $element, $state, $stateParams, $compile, $filter, eventCommunicator, scanService:IScanService, scheduleGroupResource:IScheduleGroupResource,
                                                       Logger:ILogger, propertiesUtils, configuration:IConfig, $timeout, userSettings:IUserSettingsService, saveFilesBuilder:ISaveFileBuilder,
                                                       activeScansResource:IActiveScansResource, pageTitle, dialogs, rootFoldersResource:IRootFoldersResource,currentUserDetailsProvider:ICurrentUserDetailsProvider, routerChangeService,systemSettingsResource) {
      var log:ILog = Logger.getInstance('RootFoldersManagePageCtrl');

      $scope.changePage= $stateParams.page && propertiesUtils.isInteger( $stateParams.page) ? parseInt( $stateParams.page ) : null;
      $scope.changeSelectedItem=$stateParams.selectedRootFolderId;
      $scope.findId = $stateParams.findId && propertiesUtils.isInteger( $stateParams.findId) ? parseInt( $stateParams.findId ) : null;
      $scope.rootFolderFilterBySearchText= $stateParams.searchText;
      $scope.searchText = $scope.rootFolderFilterBySearchText?decodeURIComponent($scope.rootFolderFilterBySearchText):null;
      $scope.rootFolderActionsEventName = EEventServiceEventTypes.ReportGridRootFoldersActions;

      var statusTimeoutPromise;

      var confirmation = function(op:string) {
        return dialogs.confirm('Confirmation', 'Please confirm: '+ op, null);
      };





      $scope.init=function()
      {
        pageTitle.set("Root folder settings");
         $scope.initiated = true;
        $timeout(refreshGrids, configuration.pooling_interval_idle);

        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };

      var setUserRoles = function()
      {
        $scope.isRunScansUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunScans]);
        $scope.isMngScanConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngScanConfig]);
      }
      var refreshGrids=function()
      {
        if( $scope.syncRootFoldersData) {
          $scope.syncRootFoldersData();
        }
        $timeout(refreshGrids, configuration.pooling_interval_idle);
      }

      $scope.onSelectRootFolder = function(id,path,item)
      {
        $scope.selectedRootFolderId = id;
        $scope.selectedRootFolderName = path;
        $scope.selectedRootFolder=item;

        $stateParams['selectedRootFolderId'] = $scope.selectedRootFolderId ;

        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: false
            });

      }

      $scope.rootFolderPageChanged = function(pageNumber:number)
      {
        $stateParams['page']=pageNumber;
        $state.transitionTo($state.current,
            $stateParams,
            {
              notify: false, inherit: true
            });
      }

      $scope.selectRootFoldersCount=0;
      $scope.onSelectRootFolders = function (data:Operations.DtoRootFolderSummaryInfo[]) {
        $scope.selectRootFolders = data;
        $scope.selectRootFoldersWithScannedFolders = data? data.filter(d=>d.rootFolderDto.numberOfFoldersFound>0):false;
        $scope.selectRootFoldersCount = data?data.length:0;
      };

      $scope.$watch(function () { return $scope.gridFindId; }, function (value) {
        $scope.gridFindId11 = $scope.gridFindId;
      });


      $scope.onExportRootFoldersToExcel=function(){

        let queryParams:any = {
          "take":$scope.rootTotalElements,
          "skip":0,
          "page":1,
          "pageSize":$scope.rootTotalElements
        }
        if ($scope.searchText != null) {
          queryParams['namingSearchTerm'] = $scope.searchText;
        }
        routerChangeService.addFileToProcess('Root folders_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'root_folders',queryParams,{
          itemsNumber : $scope.rootTotalElements
        },$scope.rootTotalElements > configuration.minimum_items_to_open_export_popup);
        /*
        $scope.exportRootFoldersToExcelInProgress = true;
        $scope.exportRootFoldersToExcel = {fileName: 'Root folders.xlsx'};
        */
      };
   $scope.onExportScheduleGroupsToExcel=function(){
     scheduleGroupResource.getScheduleGroups(function (schedules:Operations.DtoScheduleGroup[]) {
       schedules.forEach(s=> s.scheduleConfigDto["daysOfTheWeekAsStr"] = s.scheduleConfigDto.daysOfTheWeek?s.scheduleConfigDto.daysOfTheWeek.join(','):null);
         var dtoScheduleGroup:Operations.DtoScheduleGroup =  Operations.DtoScheduleGroup.Empty();
         var dtoScheduleConfig =  Operations.DtoScheduleConfig.Empty();
         var dtoScheduleConfigPrefix =  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.scheduleConfigDto)+'.';
         var fields = [
           {fieldName:  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.name), type: EFieldTypes.type_string, title:'SCHEDULE_NAME'},
           {fieldName:  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.schedulingDescription), type: EFieldTypes.type_string, title:'SCHEDULE_DESCRIPTION'},
           {fieldName:  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.active), type: EFieldTypes.type_string, title:'ACTIVE'},
           {fieldName:  propertiesUtils.propName(dtoScheduleGroup, dtoScheduleGroup.cronTriggerString), type: EFieldTypes.type_string, title:'cronTriggerString'},
           {fieldName:  dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.scheduleType), type: EFieldTypes.type_string, title:'scheduleType'},
           {fieldName:  dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.daysOfTheWeek)+'AsStr', type: EFieldTypes.type_string, title:'daysOfTheWeek'},
           {fieldName:  dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.every), type: EFieldTypes.type_number, title:'every'},
           {fieldName:  dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.hour), type: EFieldTypes.type_number, title:'hour'},
           {fieldName:  dtoScheduleConfigPrefix+propertiesUtils.propName(dtoScheduleConfig, dtoScheduleConfig.minutes), type: EFieldTypes.type_number, title:'minutes'},

         ];
         saveFilesBuilder.saveAsExcelOneSheet(fields,schedules,'Schedule groups','all schedule groups');
       }
       ,onGetError)
      };
     $scope.onExportExcludedFoldersToExcel=function(){
         rootFoldersResource.getExcludedFolderRules(null,null,function (excludedFolders:Operations.DtoFolderExcludeRule[],totalElements:number) {

             var dtoFolderExcludeRule:Operations.DtoFolderExcludeRule =  Operations.DtoFolderExcludeRule.Empty();
             var fields = [
               {fieldName:  propertiesUtils.propName(dtoFolderExcludeRule, dtoFolderExcludeRule.operator), type: EFieldTypes.type_string, title:'operator'},
               {fieldName:  propertiesUtils.propName(dtoFolderExcludeRule, dtoFolderExcludeRule.matchString), type: EFieldTypes.type_string, title:'matchString'},
               {fieldName:  propertiesUtils.propName(dtoFolderExcludeRule, dtoFolderExcludeRule.disabled), type: EFieldTypes.type_string, title:'disabled'},
             ];
             saveFilesBuilder.saveAsExcelOneSheet(fields,excludedFolders,'Excluded folders','all excluded folders');
           }
           ,onGetError)
      };

      $scope.exportRootFoldersToExcelCompleted=function(notFullRequest:boolean)
      {
        if(  $scope.exportRootFoldersToExcelInProgress) {
          if (notFullRequest) {
            dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
          }
          $scope.exportRootFoldersToExcelInProgress = false;
        }
      };

      $scope.searchRootFolders = function()
      {
        $scope.rootFolderFilterBySearchText = encodeURIComponent($scope.searchText);
        $stateParams['searchText']=$scope.rootFolderFilterBySearchText;
        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
      }

      $scope.searchRootFolderChanged = function() {
        if(_.isEmpty($scope.searchText)) {
          $scope.searchRootFolders();
        }
      };

      $scope.clearSearchText = function() {
        $scope.searchText = '';
        $scope.searchRootFolders();
      };

      $scope.toggleShowUnscannedRootFolder = function()
      {
        if( !$scope.showUnscannedRootFoldersOnly ) {
          var filter = new SingleCriteria(null, 'numberOfFoldersFound', 0, EFilterOperators.equals);
          $scope.rootFolderFilter = {filter: filter.toKendoFilter()};
        }
        else {
          $scope.rootFolderFilter = {filter: {}};
        }
        $scope.showUnscannedRootFoldersOnly = !$scope.showUnscannedRootFoldersOnly;
      };

      $scope.$on('$destroy', function(){
        if (angular.isDefined(statusTimeoutPromise)) {
          $timeout.cancel(statusTimeoutPromise);
          statusTimeoutPromise = undefined;
        }
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onGetError=function(e)
      {
        log.error("error get file crawling status " +e);
        $scope.initiated=true;
        $scope.errorMsg = 'Some difficulties occurred: Contact support';
      };
    });



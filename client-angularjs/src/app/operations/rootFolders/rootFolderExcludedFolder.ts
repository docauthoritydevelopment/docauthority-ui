///<reference path='../../../common/all.d.ts'/>

'use strict';
/* global kendo */


angular.module('operations.rootFolders.excludedFolders',[

    ])
    .controller('ExcludedRootFoldersCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,
                                                         propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings:IUserSettingsService) {
      var log:ILog = Logger.getInstance('ExcludedRootFoldersCtrl');

      var _this = this;

      $scope.elementName = 'root-folders-excluded-grid';
      var gridOptions:ui.IGridHelper;

      $scope.Init = function () {
        gridOptions  = GridBehaviorHelper.createGrid<Operations.DtoRootFolder>($scope, log,configuration, eventCommunicator,userSettings,  getDataUrlParams,
            fields, parseData,  $scope.totalAggregatedCountFn,
            $stateParams, null,getDataUrlBuilder);

        GridBehaviorHelper.connect($scope, $timeout, $window, log,configuration, gridOptions, null, fields, onSelectedItemChanged,$element,true,$scope.resizeHeightValue);


        gridOptions.gridSettings.dataSource.schema.total= function(response) {
          return response.totalElements; // total is returned in the "total" field of the response
        }
    //    $scope.mainGridOptions = gridOptions.gridSettings;
      };

      var getDataUrlBuilder = function (restrictedEntityDisplayTypeName,restrictedEntityId,entityDisplayType, configuration:IConfig,includeSubEntityData) {

        return configuration.root_folder_excluded_rule_url.replace(':rootFolderId',restrictedEntityId);
      }

      var getDataUrlParams = function () {
        return null;
      };

      var dtoRootFolderExcluded:Operations.DtoFolderExcludeRule =  Operations.DtoFolderExcludeRule.Empty();

      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dtoRootFolderExcluded, dtoRootFolderExcluded.id),
        title: 'Id',
        type: EFieldTypes.type_number,
        displayed: false,
        editable: false,
        nullable: true,

      };
      var path:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(dtoRootFolderExcluded, dtoRootFolderExcluded.matchString),
        title: 'Path',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: false,


      };




      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(id);
      fields.push(path);


      var parseData = function (rootFolders:Operations.DtoRootFolder[]) {

        return rootFolders;
      }

      var onSelectedItemChanged = function()
      {
        $scope.itemSelected? $scope.itemSelected($scope.selectedItem[id.fieldName],$scope.selectedItem[path.fieldName]):null;
      }
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

    })
    .directive('excludedRootFoldersGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"   k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions"  ></div>',
        replace: true,
        transclude:true,
        scope:
        {
          totalElements:'=',

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          unselectAll: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          fullDetails: '=',
          resizeHeightValue: '=',
        },
        controller: 'ExcludedRootFoldersCtrl',
        link: function (scope:any, element, attrs) {
          scope.Init();
        }
      }

    });

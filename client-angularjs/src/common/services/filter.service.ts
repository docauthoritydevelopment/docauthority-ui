///<reference path='../all.d.ts'/>
'use strict';
interface IFilterService
{
  setFilterData(filterData:FilterData):void;
  getFilterData():FilterData;
  clearPageUserSearchFilters():void;
  clearUserSearchFilter():void;
  clearPageFilter():void;
  clearPageRestrictionFilter():void;
  toggleOperatorToFilterTag(filter: SingleCriteria):void;
  removePageFilter(criteria: SingleCriteria):void;
  removePageRestrictionFilter(criteria: SingleCriteria):void;
  removePageUserFilter(criteria: SingleCriteria):void;
  addUserSearchFilter(newCriteria: ICriteriaComponent):void;
  addPageUserSearchFilter(newCriteria: ICriteriaComponent):void;
  addPageFilter(newCriteria: ICriteriaComponent):void;
  addPageRestrictionFilter(newCriteria: ICriteriaComponent):void;
  toKendoFilter():any;
  getThinFilterCopy():string;

}


angular.module('services.filter', [

    ])
    .factory('splitViewFilterServiceFactory',  function(){

      return {
        getInstance: function () {
          return new splitViewFilterService(null);
        }
      };
    })
.factory('splitViewFilterService',  function(splitViewFilterServiceFactory){

  return splitViewFilterServiceFactory.getInstance();
});

  class splitViewFilterService implements IFilterService{


    private currentFilterData:FilterData;

    constructor(filterData:FilterData)
    {
      this.setFilterData(filterData);
    }
    public setFilterData(filterData:FilterData) {
      var _this=this;
      this.currentFilterData=new FilterData();
      if(filterData) {
        restoreFilter(filterData.pageRestrictionFilter, this.addPageRestrictionFilter);
        restoreFilter(filterData.userSearchFilter, this.addUserSearchFilter);
        restoreFilter(filterData.pageUserSearchFilters, this.addPageUserSearchFilter);
        restoreFilter(filterData.pageFilter, this.addPageFilter);
      }
      function restoreFilter(filterField,addFunc) {
        if (filterField) {
          var criteria = restoreFilterRecursive(filterField);
           addFunc.call(_this,criteria); //use call inorder the change 'this' in the called method
        }
      }
      function restoreFilterRecursive(filterField) {
          if (!filterField) {
            return null;
          }
          if (filterField.objType == 'SingleCriteria') {
            (<SingleCriteria>filterField).contextName =filterField.contextName? EFilterContextNames.parse(filterField.contextName.value):null;
            (<SingleCriteria>filterField).operator = filterField.operator?EFilterOperators.parse(filterField.operator.value):null;
            return new SingleCriteria((<SingleCriteria>filterField).contextName, (<SingleCriteria>filterField).fieldName, (<SingleCriteria>filterField).value, (<SingleCriteria>filterField).operator, (<SingleCriteria>filterField).displayedText,(<SingleCriteria>filterField).displayedFilterType,(<SingleCriteria>filterField).originalCriteriaOperationId);
          }
          // single item compound criteria - return the single leaf only
          if (filterField.criterias.length == 1) {
            return restoreFilterRecursive(filterField.criterias[0]);
          }
          // Multiple items handling ...
          var criterias = [];
          var result;

          filterField.criterias.forEach(c=> {
            var parts = restoreFilterRecursive(c);
            criterias.push(parts);
          });
          if (filterField.objType == 'AndCriteria') {
            result = new AndCriteria(criterias);
          }
          else if (filterField.objType == 'OrCriteria') {
            result = new OrCriteria(criterias);
          }
          return result;

        }


    };


    public toKendoFilter()
    {
      return this.currentFilterData.toKendoFilter();
    };

    clearPageRestrictionFilter= function(){
      this.currentFilterData.pageRestrictionFilter= null
    };

    clearPageFilter = function(){
      this.currentFilterData.pageFilter = null;

    };

    getFilterData=function()
    {
      return this.currentFilterData;
    };

    clearPageUserSearchFilters = function() {
      this.currentFilterData.pageUserSearchFilters = null;
    };
    clearUserSearchFilter = function() {
      this.currentFilterData.userSearchFilter = null;
    };

    toggleOperatorToFilterTag = function(filter: SingleCriteria) {
      if(filter.operator === EFilterOperators.equals) {
        filter.operator = EFilterOperators.notEquals;
      }
      else if(filter.operator === EFilterOperators.notEquals) {
        filter.operator = EFilterOperators.equals;
      }

      else if(filter.operator === EFilterOperators.contains) {
        filter.operator = EFilterOperators.doesNotContain;
      }
      else if(filter.operator === EFilterOperators.doesNotContain) {
        filter.operator = EFilterOperators.contains;
      }

    };

    removePageFilter= function(criteria: SingleCriteria) {
      this.currentFilterData.pageFilter=this.removeFilter(this.currentFilterData.pageFilter,criteria);


    };
    removePageRestrictionFilter= function(criteria: ICriteriaComponent) {
      this.currentFilterData.pageRestrictionFilter=this.removeFilter(this.currentFilterData.pageRestrictionFilter,criteria)

    };

    removePageUserFilter= function(criteria: ICriteriaComponent) {
      this.currentFilterData.pageUserSearchFilters=this.removeFilter(this.currentFilterData.pageUserSearchFilters,criteria);
    };

    private removeFilter= function(criteriaOperation:CriteriaOperation,criteriaItemToRemove: ICriteriaComponent)
    {
      if(criteriaOperation==criteriaItemToRemove)
      {
        return null;
      }
      if(criteriaOperation )
      {
        criteriaOperation.removeCriteria(criteriaItemToRemove);
      }
      return  criteriaOperation.criterias.length>0?criteriaOperation:null;
    };

    public addPageFilter(newCriteria: ICriteriaComponent) {
      this.currentFilterData.setPageFilters(newCriteria);

    };

    public addPageRestrictionFilter(newCriteria: ICriteriaComponent) {
      this.currentFilterData.setPageRestrictionFilters(newCriteria);

    };

    public addUserSearchFilter(newCriteria: ICriteriaComponent) {
      this.currentFilterData.setUserSearchFilter(newCriteria);

    };

    public addPageUserSearchFilter(newCriteria: ICriteriaComponent) {
      this.currentFilterData.setPageUserSearchFilters(newCriteria);

    };


    public getThinFilterCopy():string {
      const thinFilterCopy = JSON.stringify(this.getFilterData(), function (key, value) {
        if (value == null) {
          return undefined;
        }
        return value;
      });
      return thinFilterCopy;
    }

  };

interface IFilterData
{
  hasCriteria():boolean;

  toKendoFilter():any;
}



class FilterData implements IFilterData
{
   pageRestrictionFilter:CriteriaOperation; //from prev page selection
   userSearchFilter:ICriteriaComponent;
   pageUserSearchFilters:CriteriaOperation;
   pageFilter:CriteriaOperation; //column filter

  constructor()
  {

  }

  hasCriteria=function()
  {
    return ((this.pageRestrictionFilter? this.pageRestrictionFilter.hasCriteria():false) ||
      (this.pageFilter? this.pageFilter.hasCriteria():false) ||
      ( this.userSearchFilter? this.userSearchFilter.hasCriteria():false) ||
      ( this.pageUserSearchFilters? this.pageUserSearchFilters.hasCriteria():false)) ;

  };
  getPageRestrictionFilter= function()
  {
    return this.pageRestrictionFilter;
  };
  getPageFilter = function()
  {
    return this.pageFilter;
  };
  getUserSearchFilter = function()
  {
    return this.userSearchFilter;
  };
  getPageUserSearchFilters = function()
  {
    return this.pageUserSearchFilters;
  };

  setPageRestrictionFilters = function(pageRestrictionFilter:ICriteriaComponent)
  {
    if(this.pageRestrictionFilter) {
      this.pageRestrictionFilter = new AndCriteria([pageRestrictionFilter], this.pageRestrictionFilter);
    }
    else
    {
      this.pageRestrictionFilter = pageRestrictionFilter;
    }
  };

  setPageUserSearchFilters = function(pageUserSearchFilters:ICriteriaComponent)
  {

    if(this.pageUserSearchFilters) {
      this.pageUserSearchFilters = new AndCriteria([pageUserSearchFilters], this.pageUserSearchFilters);
    }
    else
    {
      this.pageUserSearchFilters = pageUserSearchFilters;
    }

  };
  setUserSearchFilter = function(userSearchFilter:ICriteriaComponent) {
    this.userSearchFilter = userSearchFilter;
  };

  setPageFilters = function(pageFilter:ICriteriaComponent)
  {
    if(this.pageFilter) {
      this.pageFilter = new AndCriteria([pageFilter], this.pageFilter);
    }
    else
    {
      this.pageFilter = pageFilter;
    }
  };

  toKendoFilter = function()
  {
    var allFilters:CriteriaOperation[]= [ ];
    if(this.userSearchFilter)
    {
      allFilters.push(this.userSearchFilter);
    }
    if(this.pageUserSearchFilters)
    {
      allFilters.push(this.pageUserSearchFilters);
    }
    if( this.pageRestrictionFilter)
    {
      allFilters.push(this.pageRestrictionFilter);
    }
    if( this.pageFilter)
    {
      allFilters.push(this.pageFilter);
    }
    var result = null;
    if(allFilters.length==1) {
      result = allFilters[0].toKendoFilter();
    }
    else if(allFilters.length>1) {
      var first = allFilters[0];
      allFilters.splice(0,1);
      var theFullCriteria = new AndCriteria([first], allFilters);
      result = theFullCriteria.toKendoFilter();
    }
    return result;
  };

}

interface ICriteriaComponent {
  toKendoFilter():any;
  hasCriteria():boolean;
  getType():string;
  removeCriteria(criteria:ICriteriaComponent):boolean;
 // removeCriteriaOperation(criteriaToRemove:CriteriaOperation):void;
  getAllCriteriaLeafs():SingleCriteria[];
  getAllUniqueCriteriaOperationLeafs(propertiesUtils):SingleCriteria[];
}

class SingleCriteria implements ICriteriaComponent
{
  public objType="SingleCriteria"; //for serialization
  constructor(public contextName:EFilterContextNames, public fieldName:string, public value:any, public operator:EFilterOperators ,public displayedText?:string,public displayedFilterType?:string ,public originalCriteriaOperationId?:any)
  {

  }
  toKendoFilter =function()
  {
    return { field: this.contextName?this.contextName.toString()+'.'+this.fieldName:this.fieldName, operator: this.operator.toString(), value: this.value };
  };
  hasCriteria =function()
  {
    return this.fieldName!=null && this.value!=null;
  };

  getAllCriteriaLeafs = function()
  {
    return [this];
  };
  getAllUniqueCriteriaOperationLeafs = function(propertiesUtils)
  {
    return [this];
  };
  getType =function()
  {
    return "SingleCriteria"
  };
  removeCriteria=function(criteria)
  {
    return false;
  };

  //removeCriteriaOperation(criteriaToRemove:CriteriaOperation)
  //{
  //  return null; //na
  //}

}

class CriteriaOperation implements ICriteriaComponent {
  criterias: ICriteriaComponent[];
  operator:string;
  constructor(operator:string, criteriaOperation?:ICriteriaComponent[],otherCriteriaOperation?:ICriteriaComponent[])
  {

    this.criterias=[];
    if(criteriaOperation) {
      this.criterias=criteriaOperation;
    }
    this.operator=operator;

    if(otherCriteriaOperation) {
      this.criterias= this.criterias.concat(otherCriteriaOperation);
    }
  }
  getType =function()
  {
    return "CriteriaOperation"
  };
  hasCriteria=function()
  {
    return (this.criterias.length>0 );
  };
  getAllCriteriaLeafs = function()
  {
    var result:SingleCriteria[] =[];
    this.criterias.forEach(c=> {
      var parts = c.getAllCriteriaLeafs();
      result = result.concat(parts);
    });
    return result;
  };

  getAllUniqueCriteriaOperationLeafs = function(propertiesUtils)
  {
    var allLeafs = this.getAllCriteriaLeafs();
    if(allLeafs&&allLeafs.length>0) {
      return propertiesUtils.unique(allLeafs, function (criteria:SingleCriteria) {
        return criteria.originalCriteriaOperationId;
      });
    }
    return [];
  };



  removeCriteria=function(criteria:ICriteriaComponent)
  {
    var found = -1;

    for (var i=0;i<this.criterias.length;++i) {
      if (criteria === this.criterias[i]) {
        found = i;
        break;
      }
    }
    if (found>=0) {
      this.criterias.splice(found, 1);

      return true;
    }
    for(var i=0; i<this.criterias.length; i++)
    {
      var c= this.criterias[i];
      if (c.removeCriteria(criteria)) {
        if(c.criterias.length==1 )
        {
          this.criterias[i]= c.criterias[0]; //cascade empty criterias
        }
        else if(c.criterias.length==0 )
        {
          this.criterias.splice(i, 1);//cascade empty criterias
        }
        return true;
      }
    }

    return false;
  };

  toKendoFilter=function()
  {
    if(!this.hasCriteria())
    {
      return null;
    }
    // single item compound criteria - return the single leaf only
    if (this.criterias.length==1) {
      return this.criterias[0].toKendoFilter();
    }
    // Multiple items handling ...
    var result = {
      logic: this.operator,
      filters:  []
    };

    this.criterias.forEach(c=> {
      var parts = c.toKendoFilter();
      result.filters.push(parts);
    });
    return result;
  };
  //{
//  logic: "or",
//    filters: [
//  { field: "fieldA", operator: "eq", value: 100 },
//  {
//    logic: "and",
//    filters: [
//      { field: "fieldA", operator: "lt", value: 100 },
//      { field: "fieldB", operator: "eq", value: true }
//    ]
//  }
//]
//}

}
class AndCriteria extends CriteriaOperation {
  public objType="AndCriteria"; //for serialization
  constructor( criteriaOperation:ICriteriaComponent[],otherCriteriaOperation?:ICriteriaComponent[])
  {
    super('and', criteriaOperation,otherCriteriaOperation);
  }




}
class OrCriteria extends CriteriaOperation {
  public objType="OrCriteria"; //for serialization
  constructor(  criteriaOperation:ICriteriaComponent[],otherCriteriaOperation?:ICriteriaComponent[])
  {
    super('or',criteriaOperation,otherCriteriaOperation);
  }
}





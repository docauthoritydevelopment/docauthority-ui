///<reference path='../all.d.ts'/>
'use strict';

angular.module('services.exceptionHandler', [
  'services.i18nNotifications'
]);

interface IExceptionHandlerFactory {
  push(exception, cause) :void;
}

//angular.module('services.exceptionHandler')
//  .factory('exceptionHandlerFactory',  function($injector) {
//    // Lazy load notifications to get around circular dependency
//    //Circular dependency: $rootScope <- notifications <- i18nNotifications <- $exceptionHandler
//    let i18nNotifications:II18nNotifications = $injector.get('i18nNotifications');
//    let factory:IExceptionHandlerFactory = <IExceptionHandlerFactory> {};
//    factory.push = function(exception, cause) {
//      // Push a notification error
//       i18nNotifications.pushForCurrentRoute('error.fatal', ENotificationType.error, {}, {
//         exception: exception,
//         cause: cause
//       });
//     }
//    return factory;
//});

//$exceptionHandler - Any uncaught exception in AngularJS expressions is delegated to this service.
// The default implementation simply delegates to $log.error which logs it into the browser console.

angular.module('services.exceptionHandler')
  .config( function($provide) {
  $provide.decorator('$exceptionHandler', function ($delegate,$injector) {
    let i18nNotifications:II18nNotifications = $injector.get('i18nNotifications');
    return function(exception, cause) {
      $delegate(exception, cause); //$delegate - The original service instance, which can be replaced, monkey patched, configured, decorated or delegated to.
      i18nNotifications.pushForCurrentRoute('error.fatal', ENotificationType.error, {}, {
        exception: exception,
        cause: cause
      });
    };
  });
});

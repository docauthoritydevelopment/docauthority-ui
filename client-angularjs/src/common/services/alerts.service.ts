///<reference path='../all.d.ts'/>
'use strict';

angular.module('services.alerts',[])
  .factory('Alerts', function ($rootScope, $timeout) {

    var alerts = [];
      /**
       * Add new alert
       * @param  {String} type        - values of alert module in Angular-Bootstrap UI (danger, success, info)
       * @param  {String} msg         - alert text
       * @return {Number}             - (optional) duration in ms before alert is automatically closed
       */
    function addAlert(type, msg, duration) {
      if (duration===0) {
        duration = (-1);
      }
      if (!duration) {
        duration = 8000;
      }
      if (duration>0) {
        $timeout(function(){
          for (var i=alerts.length-1;i>=0;--i) {
            if (alerts[i].msg===msg) {
              closeAlert(i);
              break;
            }
          }
        }, duration);
      }
      alerts.push({type:type, msg: msg});
    }
    function closeAlert (index) {
      alerts.splice(index, 1);
    }

    // Public API here
    var service = {

      /* Confirmation modals */
      alerts: alerts,
      init: function() {alerts.forEach(function(){alerts.pop();}); return service;},
      addAlert: addAlert,
      closeAlert: closeAlert,
      /**
       * Add new danger-type alert
       * @param  {String} msg        - alert text
       * @return {Number}            - (optional) duration in ms before alert is automatically closed
       */
      danger: function (msg, duration) {
        addAlert('danger',msg, duration);
      },
      /**
       * Add new success-type alert
       * @param  {String} msg        - alert text
       * @return {Number}            - (optional) duration in ms before alert is automatically closed
       */
      success: function (msg, duration) {
        addAlert('success',msg, duration);
      },
      /**
       * Add new info-type alert
       * @param  {String} msg        - alert text
       * @return {Number}            - (optional) duration in ms before alert is automatically closed
       */
      info: function (msg, duration) {
        addAlert(null, msg, duration);
      }
    };
    return service;
  });

///<reference path='../all.d.ts'/>
'use strict';


interface ILoginService {
  getVersion(successCallBack:(ver:string)=>void, errorCallback): void;
  logout():void;
  login(username,password);
  loginExpiredEvent:string;
}

interface IDemoService {
  getDemoInfo(successCallBack, errorCallback):void;
}

interface ICurrentUserDetailsProvider {
  userDataUpdatedEvent: string;
  getUserData(): Users.UserAuthenticationData;
  setUserData(newUserData:Users.DTOUser):void;
  isDemoUser():boolean;
  isDemoInstallation(): boolean;
  isAuthorized(authorizedSystemRoleNames:Users.ESystemRoleName[],withFunctionalRoleIds?:number[],withAllFuncRoles?:boolean):boolean;
  isAuthenticated():boolean;
  removeUserPermissions(authorizedSystemRoleNames:Users.ESystemRoleName[]):void;
  clear(expired?:boolean):void;
  getIsExpired():boolean;
  isLoginLicenseExpired():boolean;
  getLastUsername():string;
  isInstallationModeStandard():boolean;
  isInstallationModeLegal():boolean;
  isLabelingEnabled():boolean;
  isMetadataEnabled():boolean;
  isAdvancedSearchEnabled():boolean;
  isSharePermissionEnabled():boolean;


  checkAuthReady();
}
angular.module('services.auth',[])
    .service('currentUserDetailsProvider',function ($rootScope,$q, configuration:IConfig) {
      var provider:ICurrentUserDetailsProvider = this;
      var userData:Users.UserAuthenticationData;
      var expired=false;
      var lastUsername='';
      provider.userDataUpdatedEvent = 'userDataUpdatedEvent';
      var isLoginLicenseExpired;
      var authorized;
      var installationMode;
      var labelingEnabled;
      var metadataEnabled;
      var advancedSearchEnabled;
      var sharePermissionEnabled;
      var deferCheckAuth = $q.defer();

      provider.checkAuthReady = function() {
        return deferCheckAuth.promise;
      }


      provider.clear = function (iexpired?:boolean) {
        authorized = null;
        lastUsername = userData?userData.username:lastUsername;
        userData = null;
        expired= iexpired;
      }
      provider.getIsExpired  = function () {
        return expired;
      };
      provider.isLoginLicenseExpired  = function () {
        return isLoginLicenseExpired;
      };
      provider.isInstallationModeStandard = function () {
        return installationMode == 'standard';
      };
      provider.isInstallationModeLegal = function () {
        return installationMode == 'legal';
      };
      provider.isLabelingEnabled = function () {
        return labelingEnabled;
      };
      provider.isMetadataEnabled = function () {
        return metadataEnabled;
      };
      provider.isAdvancedSearchEnabled = function() {
        return advancedSearchEnabled;
      }
      provider.isSharePermissionEnabled = function() {
        return sharePermissionEnabled;
      }
      provider.getLastUsername  = function () {
        return lastUsername;
      };
      provider.setUserData = function (newUserData:Users.DTOUser) {

        expired=false;
        userData=<Users.UserAuthenticationData>newUserData;
        userData.bizRoleSysRoleByNameMap = {};
        var bizRoleSysRoles:Users.DtoSystemRole[] =userData.bizRoleDtos[0]? _(userData.bizRoleDtos).flatMap(r=>r.template.systemRoleDtos):[];

        bizRoleSysRoles.forEach(s=> userData.bizRoleSysRoleByNameMap[s.name]=s);
        userData.funcRoleSysRolesByIdMap={};
        userData.funcRoleDtos.forEach(f=> {
          var funcRoleSysRoleByIdMap = {};
          f.template.systemRoleDtos.forEach(s=> funcRoleSysRoleByIdMap[s.name]=s );
          userData.funcRoleSysRolesByIdMap[f.id] = funcRoleSysRoleByIdMap;
        });
        isLoginLicenseExpired = userData.loginLicenseExpired;
        authorized = true;

        _.forEach(userData.configurationList, (item) => {
          let fieldName = item.name;
          configuration[fieldName] = item.value;
        });

        installationMode = configuration['installation-mode'] ? configuration['installation-mode'] : 'standard';
        labelingEnabled = configuration['labeling-enabled'] == 'true' ? true : false;
        metadataEnabled = configuration['metadata-enabled'] == 'true' ? true : false;
        advancedSearchEnabled = configuration['advanced-search-enabled'] == 'true' ? true : false;
        sharePermissionEnabled = configuration['share-permissions-enabled'] == 'true' ? true : false;

        deferCheckAuth.resolve(authorized);
      };
      provider.isAuthenticated = function () {
        return authorized&&userData&&userData.username?true:false;
      };
      provider.removeUserPermissions = function (authorizedSystemRoles:Users.ESystemRoleName[]) {
        authorizedSystemRoles.forEach(toRemove=>{
        delete userData.bizRoleSysRoleByNameMap[toRemove];
        for(var funcRoleId in userData.funcRoleSysRolesByIdMap){
         delete userData.funcRoleSysRolesByIdMap[funcRoleId][toRemove];
        }
      });

        $rootScope.$broadcast( this.userDataUpdatedEvent, this.getUserData());
      };
      provider.getUserData = function () {
        return userData;
      };
      provider.isDemoUser = function () {
        return provider.isAuthorized([Users.ESystemRoleName.demo_showcase]);
      };
      provider.isDemoInstallation = function () {
        return userData && userData.isDemoInstallation;
      };
      provider.isAuthorized= function (authorizedSystemRoles:Users.ESystemRoleName[],withFunctionalRoleIds?:number[],withAllFuncRoles?:boolean) {
        if(userData) {


          if (authorizedSystemRoles.indexOf(Users.ESystemRoleName.None)>-1) {
            return true;
          }
          var found = authorizedSystemRoles.filter(a=>userData.bizRoleSysRoleByNameMap[a]!= null).length>0; //At least one was found

          if(!found) {
            if (withAllFuncRoles) {
              withFunctionalRoleIds = userData.funcRoleDtos.map(f=>f.id);
            }
            if (withFunctionalRoleIds && withFunctionalRoleIds[0]) {
              var userFuncRolesIds = withFunctionalRoleIds.filter(a=>userData.funcRoleSysRolesByIdMap[a] != null);
              if (userFuncRolesIds[0]) {
                let i = 0;
                while (!found && i < userFuncRolesIds.length) {
                  var userFuncRolesId = userFuncRolesIds[i];
                  i++;
                  var sysRoleByNameMap = userData.funcRoleSysRolesByIdMap[userFuncRolesId];
                  var foundSysRole = authorizedSystemRoles.filter(a=>sysRoleByNameMap[a] != null).length > 0; //At least one was found
                  if (foundSysRole) {
                    found = true;
                  }
                }
              }
            }
          }
          return found;
        }
        return false;
      }

    })
    .service('loginService', function ($http, $stateParams,Logger:ILogger,$window,$location,$rootScope,$state,usersResource:IUsersResource,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log: ILog = Logger.getInstance('loginService');
      var _this: ILoginService = this;

      var init = function () {


        usersResource.getCurrentUserAuthData(function(authData:Users.DTOUser)
        {
          currentUserDetailsProvider.setUserData(authData);
          $rootScope.$broadcast( currentUserDetailsProvider.userDataUpdatedEvent, currentUserDetailsProvider.getUserData());
        },function()
        {
          currentUserDetailsProvider.setUserData(null);
          $rootScope.$broadcast( currentUserDetailsProvider.userDataUpdatedEvent,null);

        });

        $rootScope.$on('$stateChangeStart', function (event, newState,toParams) {
          var authorizedRoleNames = newState.data?newState.data.authorizedRoleNames:null;
          if (authorizedRoleNames && !currentUserDetailsProvider.isAuthenticated()) {
            event.preventDefault(); //wait till auto data returned and only then continue the redirection
            var pendingNewState = newState;
            var pendingNewStateParams = toParams;
            $rootScope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
              redirectIfNeeded(authorizedRoleNames,pendingNewState,pendingNewStateParams);
              pendingNewState = null;
              pendingNewStateParams=null;
            });
          }
          else {
            redirectIfNeeded(authorizedRoleNames);
          }
          function redirectIfNeeded(authorizedRoleNames,pendingNewState?,pendingNewStateParams?)
          {
            if (authorizedRoleNames && !currentUserDetailsProvider.isAuthorized(authorizedRoleNames)) {
              event.preventDefault();
              if (currentUserDetailsProvider.isAuthenticated()) {
                $state.go('unauthorized',{_u:encodeURIComponent(pendingNewState.name)});
                return;
              }
            }
            if(pendingNewState)
            {
              $state.go(pendingNewState,pendingNewStateParams);
            }
          }
        });
      };
      init();
      _this.logout = function()
      {

        $http.post('/logout')
          .success(function(data) {
            currentUserDetailsProvider.clear(true);
            window.location.reload();
          }
          ).error(function(err) {

          log.error('Error logout');
        });

      };
      _this.loginExpiredEvent='loginExpired';
      _this.login = function(username,password) {
        $http({
          url: '/login',
          method: "POST",
          data: {'username': username, password: password}
        })
            .then(function (data) {
                  if (data.success) {
                    var authData = new Users.UserAuthenticationData();
                    authData.username = username;
                    currentUserDetailsProvider.setUserData(authData);
                  }
                },
                function (response) { // optional
                  $rootScope.$broadcast(_this.loginExpiredEvent);
                });

      };


      _this.getVersion = function(successCallBack:(ver:string)=>void, errorCallback) {
        $http.get('/getVersion')
            .success(function(data) {
              var ver:string =  data.version;
              log.info('getVersion returned: '+ ver);
              successCallBack(ver);
            }).error(function(err) {

          log.error('Error getting UserInfo: '+JSON.stringify(err));
        });
      };

    })
    .service('demoService', function ($http, $stateParams,Logger:ILogger,$window,$location,$rootScope,$state,usersResource:IUsersResource,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var log: ILog = Logger.getInstance('demoService');
      var _this: IDemoService = this;
      var demoInfo = null;

      _this.getDemoInfo = function (successCallBack, errorCallback) {
        if (demoInfo) {
          successCallBack(demoInfo);
        } else {
          usersResource.getDemoInfo(function (data: any) {
            demoInfo = JSON.parse(data);
            successCallBack(data);
          }, function (err) {
            errorCallback(err);
          });
        }
      };
  });

angular.module('services.auth').config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('loginTimeoutRedirectInjector');
}]);
angular.module('services.auth').factory('loginTimeoutRedirectInjector',function($q,$rootScope,currentUserDetailsProvider:ICurrentUserDetailsProvider,configuration:IConfig) {
  var loginExpiredEvent='loginExpired';
  var canceller = $q.defer();
  var interceptor = {
    request : function(config) {
      if(config.url == "/login")
      {
        return config;
      }
      var access_token =currentUserDetailsProvider.getUserData()?currentUserDetailsProvider.getUserData().username:null;
       if (access_token) {
        config.headers[configuration.csrf_token_header_key] = access_token;
      }
      else {
         config.headers[configuration.csrf_token_header_key] = configuration.csrf_token_header_freepass_value;
       }

      // promise that should abort the request when resolved.
      config.timeout = canceller.promise;

      if(currentUserDetailsProvider.getIsExpired()) {
        canceller.resolve();
      }
      return config || $q.when(config);
    },
    response: function(response) {
      // do something on success
      return response;
    },
    responseError: function(response) {
      if (response.status ==401) {
        currentUserDetailsProvider.clear(true);
        $rootScope.$broadcast(loginExpiredEvent);
        console.error('Login Expired!!!\n\nPlease re-login');
        //     location.href = "/login.html";



      }
      return $q.reject(response);
    }
  };
  return interceptor;
});

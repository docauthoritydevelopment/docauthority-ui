///<reference path='../all.d.ts'/>
'use strict';
interface IBreadcrumbsService
{
  getAll():Breadcrumb[];
  getFirst():Breadcrumb;

}

class Breadcrumb
{
  path:string;
  name:string;

}
angular.module('services.breadcrumbs', [

])
 .factory('breadcrumbs',  function($rootScope, $location){

  var breadcrumbs = [];
  var breadcrumbsService:IBreadcrumbsService = <IBreadcrumbsService>{};

  //we want to update breadcrumbs only when a route is actually changed
  //as $location.path() will get updated immediatelly (even if route change fails!)
  $rootScope.$on('$stateChangeSuccess', function(event, toState){

    var pathElements = $location.path().split('/'), result = [], i;
    var breadcrumbPath = function (index) {
      return '/' + (pathElements.slice(0, index + 1)).join('/');
    };

    pathElements.shift();
    for (i=0; i<pathElements.length; i++) {
      var newBread = new Breadcrumb();
      newBread.name = pathElements[i];
      newBread.path = breadcrumbPath(i);
      result.push(newBread);
    }

    breadcrumbs = result;
  });

  breadcrumbsService.getAll = function() {
    return breadcrumbs;
  };

  breadcrumbsService.getFirst = function() {
    return breadcrumbs[0] || {};
  };

  return breadcrumbsService;
});

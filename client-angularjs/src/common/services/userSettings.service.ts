///<reference path='../all.d.ts'/>
'use strict';
interface IUserSettingsService
{

  setDiscoverSearch(value):void;
  getDiscoverSearch(successFunction: (data:any) => void ):void;
  setCurrentTag(value):void;
  getCurrentTag(successFunction: (data:any) => void ):void;
  setRecentlyUsedTagTypes(values:number[]):void;
  setRecentlyUsedTagItemsPerType(values:any):void;
  setSpiltViewDisplayedToolbars(values:any):void;
  getSpiltViewDisplayedToolbars(successFunction: (data:any[]) => void ):void;
  getRecentlyUsedTagTypes(successFunction: (data:number[]) => void ):void;
  getRecentlyUsedTagItemsPerType(successFunction: (data:any) => void ):void;
  activeSelectedTagItemUpdatedEvent: string;
  activeRecentlyUsedTagTypeItemUpdatedEvent: string;
  activeRecentlyUsedTagItemsPerTypeItemUpdatedEvent: string;
  activeSpiltViewDisplayedToolbarsItemKeyUpdatedEvent: string;
  setCurrentDepartment(value):void;
  setCurrentDocType(value):void;
  setCurrentFunctionalRole(value):void;
  getCurrentDepartment(successFunction: (data:any) => void ):void;
  getCurrentDocType(successFunction: (data:any) => void ):void;
  getCurrentFunctionalRole(successFunction: (data:any) => void ):void;
  setCurrentDateRange(value):void;
  getCurrentDateRange(successFunction: (data:any) => void ):void;
  setRecentlyUsedDocType(values:string[]):void;
  getRecentlyUsedDocType(successFunction: (data:any) => void ):void;
  setRecentlyUsedDepartment(values:string[]):void;
  getRecentlyUsedDepartment(successFunction: (data:any) => void ):void;
  getRecentlyUsedFunctionalRole(successFunction: (data:number[]) => void ):void;
  setRecentlyUsedFunctionalRole(values:number[]):void;
  setGDPRTagsConfig(values:any):void;
  getGDPRTagsConfig(successFunction: (data:any) => void ):void;
  setRecentlyUsedDateRanges(values:Entities.DTODateRangeItem[]):void;
  getRecentlyUsedDateRanges(successFunction: (data:Entities.DTODateRangeItem[]) => void ):void;
  setRecentlyUsedMarkedItems(values:DTOMarkedItem[]):void;
  getRecentlyUsedMarkedItems(successFunction: (markedItems:DTOMarkedItem[]) => void ):void;
  activeRecentlyUsedMarkedItemUpdatedEvent: string;
  setRecentlyUsedDateRange(values:string):void;
  getRecentlyUsedDateRange(successFunction: (data:any) => void ):void;
  activeSelectedDepartmentItemUpdatedEvent: string;
  activeSelectedDocTypeItemUpdatedEvent: string;
  activeSelectedFunctionalRoleItemUpdatedEvent: string;
  activeSelectedDateRangeItemUpdatedEvent: string;
  activeRecentlyUsedDocTypeItemUpdatedEvent: string;
  activeRecentlyUsedDepartmentItemUpdatedEvent: string;
  activeRecentlyUsedFunctionalRoleItemUpdatedEvent: string;
  activeRecentlyUsedDateRangesItemUpdatedEvent: string;
  activeRecentlyUsedDateRangeItemUpdatedEvent: string;
  getRecentlyUsedBizListAssociations(bizListType,successFunction: (data:any) => void ):void;
  getBizListAssociationsPreferences(bizListType,successFunction: (data:any) => void ):void;
  getDepartmentsPreferences(successFunction: (data:any) => void ):void;
  getDocTypesPreferences(successFunction: (data:any) => void ):void;
  getFunctionalRolesPreferences(successFunction: (data:any) => void ):void;
  getMarkedItemsPreferences(successFunction: (data:any) => void ):void;
  getMarkedItemsPreferences(successFunction: (data:any) => void ):void;
  getTagsPreferences(successFunction: (data:any) => void ):void;
  getDocTypeTagColumns(successFunction: (data:any) => void ):void;
  getDateRangesPreferences(successFunction: (data:any) => void ):void;
  setRecentlyUsedBizListAssociations(bizListType,values:any[]):void;
  setBizListAssociationsPreferences(bizListType,value:any):void;
  setDocTypesPreferences(value:any):void;
  setDepartmentsPreferences(value:any):void;
  setDocTypesTagColumns(value:any):void;
  setFunctionalRolesPreferences(value:any):void;
  setMarkedItemsPreferences(value:any):void;
  setDateRangesPreferences(value:any):void;
   setTagsPreferences(value:any):void;
  activeRecentlyUsedBizListAssociationItemUpdatedEvent: string;

  setLastSelectRootFolderScheduleGroup(value):void;
  setLastSelectRootFolderDataCenter(value):void;
  setLastSelectMediaProcessorDataCenter(value):void;
  getLastSelectRootFolderScheduleGroup(successFunction: (data:any) => void ):void;
  getLastSelectRootFolderDataCenter(successFunction: (data:any) => void ):void;
  getLastSelectMediaProcessorDataCenter(successFunction: (data:any) => void ):void;

  getGridViewPreferences(successFunction: (data:any) => void ):any;
  setGridViewPreferences(data:any ):void;
  toggleNewLook():boolean;
  toggleTagIcons():boolean;
  toggleDocTypeIcons():boolean;

  getDontShowAssociationWarning(successFunction: (data:any) => void ):void;
  setDontShowAssociationWarning(value):void;

  getFiltersPaneVisibility(successFunction: (data:any) => void ):void;
  setFiltersPaneVisibility(value:any):void;

  getMainToolbarHidden(successFunction: (data:any) => void ):void;
  setMainToolbarHidden(value:any):void;

  gridViewPreferencesUpdatedEvent: string;
  cachedGridViewPreferences:any;
}


angular.module('services.userSettings', [

])
  .factory('userSettings',  function($rootScope, localStorage:IUserStorageService ,remoteUserStorageResource:IUserStorageService,sessionLocalStorage:IUserStorageService){
    var userStorage:IUserStorageService = remoteUserStorageResource;

    var activeSelectedDepartmentItemKey='activeSelectedDepartmentItem';
    var discoverSearchItemKey='discoverSearchItemKey';
    var activeSelectedTagItemKey='activeSelectedTagItem';
    var activeSelectedDocTypeItemKey='activeSelectedDocTypeItem';
    var activeSelectedFunctionalRoleItemKey='activeSelectedFunctionalRoleItemKey';
    var activeSelectedDateRangeItemKey='activeSelectedDateRangeItem';
    var recentlyUsedDocTypesItemKey='recentlyUsedDocTypesItemKey';
    var recentlyUsedDepartmentsItemKey='recentlyUsedDepartmentsItemKey';
    var recentlyUsedFunctionalRolesItemKey='recentlyUsedFunctionalRolesItemKey';
    var recentlyUsedDateRangesItemKey='recentlyUsedDateRangesItemKey';
    var recentlyUsedMarkedItemKey='recentlyUsedMarkedItemKey';
    var recentlyUsedDateRangeItemKey='recentlyUsedDateRangeItemKey';
    var recentlyUsedTagTypesItemKey='recentlyUsedTagTypesItemKey';
    var recentlyUsedTagItemsItemKey='recentlyUsedTagItemsItemKey';
    var spiltViewDisplayedToolbarsItemKey='spiltViewDisplayedToolbarsItemKey';
    var recentlyUsedBizListAssociationItemKey='recentlyUsedBizListAssociationItemKey';
    var bizListAssociationsPreferencesItemKey='bizListAssociationsPreferencesItemKey';
    var docTypesPreferencesItemKey='docTypesPreferencesItemKey';
    var departmentsPreferencesItemKey='departmentsPreferencesItemKey';
    var docTypesTagColumnsKey='docTypesTagColumnsKey';
    var functionalRolesPreferencesItemKey='functionalRolesPreferencesItemKey';
    var markedItemsPreferencesItemKey='markedItemsPreferencesItemKey';
    var dateRangesPreferencesItemKey='dateRangesPreferencesItemKey';
    var tagsPreferencesItemKey='tagsPreferencesItemKey';
    var gridViewPreferencesItemKey='gridViewPreferencesItemKey';

    var lastSelectedRootFolderScheduleGroupItemKey='lastSelectedRootFolderScheduleGroup';
    var lastSelectedRootFolderDataCenterItemKey='lastSelectedRootFolderDataCenter';
    var lastSelectedMediaProcessorDataCenterItemKey='lastSelectedMediaProcessorDataCenter';
    var filtersPaneVisibility='filtersPaneVisibility';
    var mainToolbarHidden='mainToolbarHidden';

    var userSettings:IUserSettingsService = <IUserSettingsService>{};
    userSettings.activeSelectedTagItemUpdatedEvent = 'activeSelectedTagItemUpdatedEvent';
    userSettings.activeRecentlyUsedMarkedItemUpdatedEvent = 'activeRecentlyUsedMarkedItemUpdatedEvent';
    userSettings.activeSelectedDepartmentItemUpdatedEvent = 'activeSelectedDocTypeItemUpdatedEvent';
    userSettings.activeSelectedDocTypeItemUpdatedEvent = 'activeSelectedDocTypeItemUpdatedEvent';
    userSettings.activeSelectedFunctionalRoleItemUpdatedEvent = 'activeSelectedFunctionalRoleItemUpdatedEvent';
    userSettings.activeRecentlyUsedDocTypeItemUpdatedEvent = 'activeRecentlyUsedDocTypeItemUpdatedEvent';
    userSettings.activeRecentlyUsedDepartmentItemUpdatedEvent = 'activeRecentlyUsedDepartmentItemUpdatedEvent';
    userSettings.activeRecentlyUsedDateRangesItemUpdatedEvent = 'activeRecentlyUsedDateRangesItemUpdatedEvent';
    userSettings.activeRecentlyUsedDateRangeItemUpdatedEvent = 'activeRecentlyUsedDateRangeItemUpdatedEvent';
    userSettings.activeRecentlyUsedTagTypeItemUpdatedEvent = 'activeRecentlyUsedTagTypeItemUpdatedEvent';
    userSettings.activeRecentlyUsedTagItemsPerTypeItemUpdatedEvent = 'activeRecentlyUsedTagItemsPerTypeItemUpdatedEvent';
    userSettings.activeSpiltViewDisplayedToolbarsItemKeyUpdatedEvent = 'activeSpiltViewDisplayedToolbarsItemKeyUpdatedEvent';
    userSettings.activeRecentlyUsedBizListAssociationItemUpdatedEvent = 'activeRecentlyUsedBizListAssociationItemUpdatedEvent';
    userSettings.gridViewPreferencesUpdatedEvent = 'gridViewPreferencesUpdatedEvent';
    userSettings.cachedGridViewPreferences = null;

    var dontShowAssociationWarning='dontShowAssociationWarning';
    var activeSelectedFunctionalRoleItemKey='activeSelectedFunctionalRoleItemKey';

    var defaultGridViewPreferences = {
      gridView: false,
      newLook: true,
      expandTags: false,
      expandDocTypes: false,
    };
    var toggleGridViewPrefField = function (field:string):boolean {
      if (userSettings.cachedGridViewPreferences) {
        userSettings.cachedGridViewPreferences[field] = !userSettings.cachedGridViewPreferences[field];
        userSettings.setGridViewPreferences(userSettings.cachedGridViewPreferences);
        return userSettings.cachedGridViewPreferences[field];
      }
      return false;
    };

    userSettings.setGridViewPreferences = function(value) {
      // localStorage.setUserData('user.settings',gridViewPreferencesItemKey,value,function()
      userSettings.cachedGridViewPreferences = value;
      remoteUserStorageResource.setUserData('user.settings',gridViewPreferencesItemKey,value,
          function() {
            $rootScope.$broadcast(userSettings.gridViewPreferencesUpdatedEvent, value);
          },
          function(){}
      );
    };
    userSettings.getGridViewPreferences = function( successFunction: (data:any) => void) {
      if (!userSettings.cachedGridViewPreferences) {
        remoteUserStorageResource.getUserData('user.settings',gridViewPreferencesItemKey,
            function(data) {
              userSettings.cachedGridViewPreferences = ((!data) || (typeof data !== 'object'))?defaultGridViewPreferences:data;
              successFunction(userSettings.cachedGridViewPreferences);
            },
            function() {
              userSettings.cachedGridViewPreferences = "false";
              successFunction(userSettings.cachedGridViewPreferences);
            }
        );
        return null;
      }
      successFunction(userSettings.cachedGridViewPreferences);
      return userSettings.cachedGridViewPreferences;
      // return localStorage.getUserData('user.settings',gridViewPreferencesItemKey,function(data){successFunction(data)},successFunction);
      // return remoteUserStorageResource.getUserData('user.settings',gridViewPreferencesItemKey,function(data){successFunction(data)},successFunction);
    };
    userSettings.toggleNewLook = function ():boolean {
      return toggleGridViewPrefField('newLook');
    };
    userSettings.toggleTagIcons = function ():boolean {
      return toggleGridViewPrefField('expandTags');
    };
    userSettings.toggleDocTypeIcons = function ():boolean {
      return toggleGridViewPrefField('expandDocTypes');
    };
    userSettings.getFiltersPaneVisibility = function(successFunction: (data:any) => void ) {
      localStorage.getUserData('user.settings',filtersPaneVisibility,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.setFiltersPaneVisibility = function(value) {
      localStorage.setUserData('user.settings',filtersPaneVisibility,JSON.stringify(value),function() {
      },function(){});
    };

    userSettings.getMainToolbarHidden = function(successFunction: (data:any) => void ) {
      localStorage.getUserData('user.settings',mainToolbarHidden,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.setMainToolbarHidden = function(value) {
      localStorage.setUserData('user.settings',mainToolbarHidden,JSON.stringify(value),function() {
      },function(){});
    };

    userSettings.setCurrentDepartment = function(value) {
      localStorage.setUserData('user.settings',activeSelectedDepartmentItemKey,JSON.stringify(value),function() {
        $rootScope.$broadcast(userSettings.activeSelectedDepartmentItemUpdatedEvent,value);
      },function(){});
    };

    userSettings.setCurrentDocType = function(value) {
      localStorage.setUserData('user.settings',activeSelectedDocTypeItemKey,JSON.stringify(value),function()
      {
        $rootScope.$broadcast(userSettings.activeSelectedDocTypeItemUpdatedEvent,value);
      },function(){});
    };

    userSettings.setCurrentFunctionalRole = function(value) {
      localStorage.setUserData('user.settings',activeSelectedFunctionalRoleItemKey,JSON.stringify(value),function()
      {
        $rootScope.$broadcast(userSettings.activeSelectedFunctionalRoleItemUpdatedEvent,value);
      },function(){});
    };

    userSettings.setCurrentDateRange = function(value) {
      localStorage.setUserData('user.settings',activeSelectedDateRangeItemKey,JSON.stringify(value),function()
      {
        $rootScope.$broadcast(userSettings.activeSelectedDateRangeItemUpdatedEvent,value);
      },function(){});

    };

    userSettings.setCurrentTag = function(value) {
      localStorage.setUserData('tags',activeSelectedTagItemKey,angular.toJson(value),function()
      {
        $rootScope.$broadcast(userSettings.activeSelectedTagItemUpdatedEvent,value);
      },function(){});

    };


    userSettings.setDiscoverSearch= function(value) {
      userStorage.setUserData('discover',discoverSearchItemKey,angular.toJson(value),function()
      {
       // $rootScope.$broadcast(userSettings.activeSelectedTagItemUpdatedEvent,value);
      },function(){});

    };

    userSettings.setRecentlyUsedDocType = function(values:string[]) {
      userStorage.setUserData('user.settings',recentlyUsedDocTypesItemKey,angular.toJson(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedDocTypeItemUpdatedEvent,values);
      },function(){});

    };

    userSettings.setRecentlyUsedDepartment = function(values:string[]) {
      userStorage.setUserData('user.settings',recentlyUsedDepartmentsItemKey,angular.toJson(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedDepartmentItemUpdatedEvent,values);
      },function(){});

    };

    userSettings.setRecentlyUsedFunctionalRole = function(values:number[]) {
      userStorage.setUserData('user.settings',recentlyUsedFunctionalRolesItemKey,angular.toJson(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedFunctionalRoleItemUpdatedEvent,values);
      },function(){});

    };

    userSettings.setGDPRTagsConfig = function(values:any) {
      userStorage.setUserData('GDPR.configuration','GDPR',angular.toJson(values),function()
      {

      },function(){});

    };

    userSettings.setRecentlyUsedDateRanges = function(values:Entities.DTODateRangeItem[]) {
      localStorage.setUserData('user.settings',recentlyUsedDateRangesItemKey,angular.toJson(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedDateRangesItemUpdatedEvent,values);
      },function(){});

    };
  userSettings.setRecentlyUsedDateRange = function(values:string) {
      localStorage.setUserData('user.settings',recentlyUsedDateRangeItemKey,angular.toJson(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedDateRangeItemUpdatedEvent,values);
      },function(){});

    };
    userSettings.setRecentlyUsedMarkedItems = function(values:DTOMarkedItem[]) {
      userStorage.setUserData('user.settings',recentlyUsedMarkedItemKey,JSON.stringify(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedMarkedItemUpdatedEvent,values);
      },function(){});

    };

    userSettings.setRecentlyUsedTagTypes = function(values:number[]) {
      userStorage.setUserData('user.settings',recentlyUsedTagTypesItemKey,JSON.stringify(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedTagTypeItemUpdatedEvent,values);
      },function(){
        $rootScope.$broadcast(userSettings.activeRecentlyUsedTagTypeItemUpdatedEvent,values);
      });

    };
    userSettings.setRecentlyUsedTagItemsPerType = function(values:any) {
      userStorage.setUserData('user.settings',recentlyUsedTagItemsItemKey,JSON.stringify(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedTagItemsPerTypeItemUpdatedEvent,values);
      },function(){});

    };
  userSettings.setSpiltViewDisplayedToolbars = function(values:any) {
      userStorage.setUserData('user.settings',spiltViewDisplayedToolbarsItemKey,JSON.stringify(values),function()
      {
        $rootScope.$broadcast(userSettings.activeSpiltViewDisplayedToolbarsItemKeyUpdatedEvent,values);
      },function(){});

    };

    userSettings.setRecentlyUsedBizListAssociations = function(bizListType,values:any[]) {
      userStorage.setUserData('user.settings',recentlyUsedBizListAssociationItemKey+'.'+bizListType,JSON.stringify(values),function()
      {
        $rootScope.$broadcast(userSettings.activeRecentlyUsedBizListAssociationItemUpdatedEvent,values);
      },function(){});

    };
    userSettings.setBizListAssociationsPreferences = function(bizListType,value:any) {
      userStorage.setUserData('user.settings',bizListAssociationsPreferencesItemKey+'.'+bizListType,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setDocTypesPreferences = function(value:any) {
      userStorage.setUserData('user.settings',docTypesPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setDepartmentsPreferences = function(value:any) {
      userStorage.setUserData('user.settings',departmentsPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setDocTypesTagColumns = function(value:any) {
      userStorage.setUserData('user.settings',docTypesTagColumnsKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setFunctionalRolesPreferences = function(value:any) {
      userStorage.setUserData('user.settings',functionalRolesPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setMarkedItemsPreferences = function(value:any) {
      userStorage.setUserData('user.settings',markedItemsPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setDateRangesPreferences = function(value:any) {
      userStorage.setUserData('user.settings',dateRangesPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setTagsPreferences = function(value:any) {
      userStorage.setUserData('user.settings',tagsPreferencesItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setLastSelectRootFolderScheduleGroup = function(value:any) {
      remoteUserStorageResource.setUserData('operations.rootfolders',lastSelectedRootFolderScheduleGroupItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setLastSelectRootFolderDataCenter = function(value:any) {
      remoteUserStorageResource.setUserData('operations.rootfolders',lastSelectedRootFolderDataCenterItemKey,JSON.stringify(value),function(){},function(){});
    };
    userSettings.setLastSelectMediaProcessorDataCenter = function(value:any) {
      remoteUserStorageResource.setUserData('operations.syscomponents',lastSelectedMediaProcessorDataCenterItemKey,JSON.stringify(value),function(){},function(){});
    };

    var parseResult = function(data,successFunction)
    {
      var result = null;
      if (data) {
        try {
          result = JSON.parse(data);
          if (_.isArray(result)) {
            result = _.filter(result, function (item) {
              return item !== '';
            });
          }
        } catch (e) {
          console.error('Failed to parse user setting');
        }
      }
        successFunction(result);

    }
    var onError = function(data)
    {

    }
    userSettings.getCurrentTag = function(successFunction: (data:any) => void  ) {

      localStorage.getUserData('user.settings',activeSelectedTagItemKey,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getDiscoverSearch = function(successFunction: (data:any) => void  ) {

      userStorage.getUserData('discover',discoverSearchItemKey,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getRecentlyUsedDocType = function( successFunction: (data:any) => void) {

      userStorage.getUserData('user.settings',recentlyUsedDocTypesItemKey,function(data)
      {
        var result = null;
        if (data) {
          result = JSON.parse(data);
          if(_.isArray(result)) {
            result = _.filter(result, function(item) {
              return item !== '' && !isNaN(item);
            });
          }
        }
        successFunction(result);
      },function() {
        successFunction([]);
      });
    };

    userSettings.getRecentlyUsedDepartment = function( successFunction: (data:any) => void) {
      userStorage.getUserData('user.settings',recentlyUsedDepartmentsItemKey,function(data) {
        var result = null;
        if (data) {
          result = JSON.parse(data);
          if(_.isArray(result)) {
            result = _.filter(result, function(item) {
              return item !== '' && !isNaN(item);
            });
          }
        }
        successFunction(result);
      },function() {
        successFunction([]);
      });
    };

    userSettings.getRecentlyUsedFunctionalRole = function( successFunction: (data:number[]) => void) {
      userStorage.getUserData('user.settings',recentlyUsedFunctionalRolesItemKey,function(data){
        parseResult(data,successFunction)
      },onError);
    };

    userSettings.getGDPRTagsConfig = function( successFunction: (data:any) => void) {
      userStorage.getUserData('GDPR.configuration','GDPR',function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getRecentlyUsedDateRanges = function( successFunction: (values:Entities.DTODateRangeItem[]) => void) {

      localStorage.getUserData('user.settings',recentlyUsedDateRangesItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getRecentlyUsedMarkedItems = function( successFunction: (data:DTOMarkedItem[]) => void) {

      userStorage.getUserData('user.settings',recentlyUsedMarkedItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getRecentlyUsedDateRange = function( successFunction: (data:any) => void) {

      localStorage.getUserData('user.settings',recentlyUsedDateRangeItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getCurrentDepartment = function(successFunction: (data:any) => void ) {

      localStorage.getUserData('user.settings',activeSelectedDepartmentItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getCurrentDocType = function(successFunction: (data:any) => void ) {

      localStorage.getUserData('user.settings',activeSelectedDocTypeItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getCurrentFunctionalRole = function(successFunction: (data:any) => void ) {

      localStorage.getUserData('user.settings',activeSelectedFunctionalRoleItemKey,function(data){
        parseResult(data,successFunction)},
        onError);

    };
    userSettings.getCurrentDateRange = function(successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',activeSelectedDateRangeItemKey,function(data){parseResult(data,successFunction)},onError);

    };

    userSettings.getRecentlyUsedTagTypes = function( successFunction: (data:number[]) => void) {

      userStorage.getUserData('user.settings',recentlyUsedTagTypesItemKey,function(data){parseResult(data,successFunction)},onError);

    };
    userSettings.getRecentlyUsedTagItemsPerType = function(successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',recentlyUsedTagItemsItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getSpiltViewDisplayedToolbars = function(successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',spiltViewDisplayedToolbarsItemKey,function(data){parseResult(data,successFunction)},onError);
    };


    userSettings.getRecentlyUsedBizListAssociations = function( bizListType,successFunction: (data:any) => void) {

      userStorage.getUserData('user.settings',recentlyUsedBizListAssociationItemKey+'.'+bizListType,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getBizListAssociationsPreferences = function(bizListType,successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',bizListAssociationsPreferencesItemKey+'.'+bizListType,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getDepartmentsPreferences = function(successFunction: (data:any) => void ) {
      userStorage.getUserData('user.settings',departmentsPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getDocTypesPreferences = function(successFunction: (data:any) => void ) {
      userStorage.getUserData('user.settings',docTypesPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getFunctionalRolesPreferences= function(successFunction: (data:any) => void ) {
      userStorage.getUserData('user.settings',functionalRolesPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getMarkedItemsPreferences = function(successFunction: (data:any) => void ) {
      userStorage.getUserData('user.settings',markedItemsPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getDateRangesPreferences = function(successFunction: (data:any) => void ) {
      userStorage.getUserData('user.settings',dateRangesPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getTagsPreferences = function(successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',tagsPreferencesItemKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getDocTypeTagColumns = function(successFunction: (data:any) => void ) {

      userStorage.getUserData('user.settings',docTypesTagColumnsKey,function(data){parseResult(data,successFunction)},onError);
    };
    userSettings.getLastSelectRootFolderScheduleGroup = function(successFunction: (data:any) => void ) {

      remoteUserStorageResource.getUserData('operations.rootfolders',lastSelectedRootFolderScheduleGroupItemKey,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getLastSelectRootFolderDataCenter = function(successFunction: (data:any) => void ) {

      remoteUserStorageResource.getUserData('operations.rootfolders',lastSelectedRootFolderDataCenterItemKey,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getLastSelectMediaProcessorDataCenter = function(successFunction: (data:any) => void ) {

      remoteUserStorageResource.getUserData('operations.syscomponents',lastSelectedMediaProcessorDataCenterItemKey,function(data){parseResult(data,successFunction)},onError);
    };

    userSettings.getDontShowAssociationWarning = function(successFunction: (data:any) => void ) {

      localStorage.getUserData('user.settings',dontShowAssociationWarning,function(data){
          parseResult(data,successFunction)},
        onError);
    };

    userSettings.setDontShowAssociationWarning = function(value) {
      localStorage.setUserData('user.settings',dontShowAssociationWarning,JSON.stringify(value),function()
      {
      },function(){});
    };


    return userSettings;
  });

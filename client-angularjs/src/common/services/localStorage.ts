///<reference path='../all.d.ts'/>
'use strict';


angular.module('services.localStorage', [

])
  .factory('localStorage',  function(appSettings,Logger:ILogger,localStorageService, currentUserDetailsProvider:ICurrentUserDetailsProvider){

    var log: ILog = Logger.getInstance('localStorage service');
    var localStorage:IUserStorageService = <IUserStorageService>{};

    var keyBuilder = function(moduleName:string,key,isUserData?:boolean)
    {
      var modulePart = (!moduleName || moduleName == '') ? '' : moduleName + '.';
      if(isUserData ) {
        if(currentUserDetailsProvider.getUserData()) {
          var userPart = !isUserData ? '' : currentUserDetailsProvider.getUserData().username + '.';
          return userPart + modulePart + key;
        }
        log.warn('session key was request, but no user data loaded yet. Key: '+key);
        return null;
      }
      if(!isUserData){
        return  modulePart + key;
      }

    }

    localStorage.setUserData = function(moduleName:string,key,value,successFunction: () => void , errorFunction) {
      var builtKey = keyBuilder(moduleName,key,true);
      localStorageService.set(builtKey,value);
      successFunction();
    };
    localStorage.getUserData = function(moduleName:string,key,successFunction: (data) => void , errorFunction) {
      var builtKey = keyBuilder(moduleName, key,true);
      var data = localStorageService.get(builtKey)
      successFunction(data);
      return data;
    };
    localStorage.deleteUserData = function(moduleName:string,key,successFunction: () => void , errorFunction) {
      var builtKey = keyBuilder(moduleName, key,true);
      localStorageService.set(builtKey,'');
      successFunction( );
    };



    return localStorage;
  })
    .factory('sessionLocalStorage',  function(appSettings,Logger:ILogger, currentUserDetailsProvider:ICurrentUserDetailsProvider){

      var log: ILog = Logger.getInstance('sessionLocalStorage service');
      var localStorage:IUserStorageService = <IUserStorageService>{};

      var keyBuilder = function(moduleName:string,key,isUserData?:boolean)
      {
        var modulePart = (!moduleName || moduleName == '') ? '' : moduleName + '.';
        if(isUserData ) {
          if(currentUserDetailsProvider.getUserData()) {
            var userPart = !isUserData ? '' : currentUserDetailsProvider.getUserData().username + '.';
            return userPart + modulePart + key;
          }
          log.warn('session key was request, but no user data loaded yet. Key: '+key);
          return null;
        }
        if(!isUserData){
          return  modulePart + key;
        }

      }

      localStorage.setUserData = function(moduleName:string,key,value,successFunction: () => void , errorFunction) {
        var builtKey = keyBuilder(moduleName,key,true);
        sessionStorage.setItem(builtKey,value);
        successFunction();
      };
      localStorage.getUserData = function(moduleName:string,key,successFunction: (data) => void , errorFunction) {
        var builtKey = keyBuilder(moduleName, key,true);
        var data = sessionStorage.getItem(builtKey);
        successFunction(data);
        return data;
      };
      localStorage.deleteUserData = function(moduleName:string,key,successFunction: () => void , errorFunction) {
        var builtKey = keyBuilder(moduleName, key,true);
        sessionStorage.removeItem('builtKey');
        successFunction( );
      };



      return localStorage;
    });

///<reference path='../all.d.ts'/>
'use strict';

angular.module('services.i18nNotifications', [
    'services.notifications',
    'services.localizedMessages'
  ]);

interface II18nNotifications {

  pushSticky (msgKey, type:ENotificationType, formattingParams, otherProperties):void;
  pushForCurrentRoute(msgKey, type:ENotificationType, formattingParams, otherProperties):void;
  pushNotificationForCurrentRoute(notification:INotification):void;
  pushForNextRoute(msgKey, type:ENotificationType, formattingParams, otherProperties):void;
  createNotification(msgKey, type:ENotificationType, formattingParams, otherProperties):INotification;
  getCurrent():INotification;
  remove(notification:INotification):void;
  onErrorInformUser(msgKey?,formattingParams?):void;
  onErrorInformUserAggregated(notifications:INotification[]):void;

}

angular.module('services.i18nNotifications')
  .factory('i18nNotifications',  function ($window, $injector, notifications:INotificationsService,dialogs) {
    let localizedMessages = $injector.get('localizedMessages');


    var I18nNotificationsFactory:II18nNotifications = <II18nNotifications> {
        pushSticky:function (msgKey, type:ENotificationType, formattingParams, otherProperties) {
           notifications.pushSticky(
             this.createNotification(msgKey, type, formattingParams, otherProperties));
        },
        pushForCurrentRoute:function (msgKey, type:ENotificationType, formattingParams, otherProperties) {
           notifications.pushForCurrentRoute(
             this.createNotification(msgKey, type, formattingParams, otherProperties));
        },
      pushNotificationForCurrentRoute:function (notification:INotification) {
           notifications.pushForCurrentRoute(notification);
        },
        pushForNextRoute:function (msgKey, type:ENotificationType, formattingParams, otherProperties) {
           notifications.pushForNextRoute(
             this.createNotification(msgKey, type, formattingParams, otherProperties));
        },
        getCurrent:function () {
          return notifications.getCurrent();
        },
        remove:function (notification) {
           notifications.remove(notification);
        },
        onErrorInformUser :function(msgKey?,formattingParams?)
        {
          var msgInternal =msgKey? localizedMessages.get(msgKey,formattingParams):null;
          var msg = localizedMessages.get('general.error', {msg:msgInternal});
          dialogs.error('Error',msg);
        },
        onErrorInformUserAggregated :function(notifications:INotification[])
        {
          var errors = notifications.filter(n=>n && n.type == ENotificationType.error);
          if(errors[0]) {
            if (errors.length > 1) {
              var aggregatedErrMsg = localizedMessages.get('general.aggregated.errors');
              aggregatedErrMsg += errors.slice(0, 4).map(t=>t.message).join('<br \>');
              if(errors.length>4){
                aggregatedErrMsg+='<br \>More ('+(errors.length-4)+')';
              }
              dialogs.error('Error', aggregatedErrMsg);
            }
            else {
              this.onErrorInformUser(errors[0].message, null);
            }
          }
        },
        createNotification : function(msgKey, type:ENotificationType, formattingParams, otherProperties):INotification {
            return <INotification>angular.extend({
              message: localizedMessages.get(msgKey, formattingParams),
              type: type
            }, otherProperties);
      }

  };


  return I18nNotificationsFactory;
});



angular.module('services.localizedMessages', [

])
  .factory('localizedMessages',  function ($interpolate, I18nMESSAGES) {

  var handleNotFound = function (msg, msgKey) {
    return msg || '?' + msgKey + '?';
  };

  return {
    get : function (msgKey, formattingParams?):string {
      var msg =  I18nMESSAGES[msgKey];
      if (msg) {
        return $interpolate(msg)(formattingParams);
      } else {
        return handleNotFound(msg, msgKey);
      }
    }
  };
});


angular.module('services.localizedMessages')
  .constant('I18nMESSAGES', {
  'general.error':'Sorry, an error occurred.<br\><br\>{{msg}}',
  'general.aggregated.errors':'Sorry, following errors occurred:<br\><br\>',
  'simple.item':'{{name}}' ,
  'simple.item.in.use':'{{name}} - in use' ,
  'crud.assign.entity.remove.success':"{{action}} '{{name}}'  was removed successfully from '{{entityName}}'.",
  'crud.assign.entity.remove.error':"Something went wrong when {{action}} '{{name}}'  from '{{entityName}}' .",
  'crud.assign.entity.add.success':"{{action}} '{{name}}'  was added successfully to '{{entityName}}'.",
  'crud.assign.entity.add.error':"Something went wrong when {{action}} '{{name}}'  adding '{{entityName}}' .",

    'delete.confirmation':'Are you sure you want to delete {{counter}} {{entityName}}?',
    'crud.add.entity.error.generic':"Failed to add {{entityName}} {{fieldName}}: {{fieldValue}}.",
    'crud.add.entity.error.alreadyExists':"{{entityName}} with {{fieldName}}: {{fieldValue}} already exists.",
    'crud.update.entity.error.generic':"Failed to add {{entityName}} {{fieldName}}: {{fieldValue}}.",
    'crud.update.entity.error.alreadyExists':"Failed to update. {{entityName}} with {{fieldName}}: {{fieldValue}} already exists.",


})
//'errors.route.changeError':'Route change error',
//  'crud.user.save.success':"A user with id '{{id}}' was saved successfully.",
//'crud.user.remove.error':"Something went wrong when removing user with id '{{id}}'.",
//  'crud.user.save.error':"Something went wrong when saving a user...",
//  'crud.project.save.success':"A project with id '{{id}}' was saved successfully.",
//  'crud.project.remove.success':"A project with id '{{id}}' was removed successfully.",
//  'crud.project.save.error':"Something went wrong when saving a project...",
//  'login.reason.notAuthorized':"You do not have the necessary access permissions.  Do you want to login as someone else?",
//  'login.reason.notAuthenticated':"You must be logged in to access this part of the application.",
//  'login.error.invalidCredentials': "Login failed.  Please check your credentials and try again.",
//  'login.error.serverError': "There was a problem with authenticating: {{exception}}."

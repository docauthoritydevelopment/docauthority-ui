///<reference path='../all.d.ts'/>

'use strict';
interface IScanService
{
  forceRunStatusRefresh(force:boolean):void;
  forceServerComponentsRefresh():void
  getActiveRuns():Operations.ActiveRunsView;
  getRootFoldersAggregatedSummaryInfo():Operations.DtoRootFoldersAggregatedSummaryInfo;
  getServerComponentsStatus():{};
  getMediaProcessors(dataCenterId):Operations.DtoClaComponent[];
  getActiveExtractionRunForBizList(bizListId:number):Operations.DtoRunSummaryInfo;
  getApplyToSolrStatus():Operations.ApplyToSolrStatusView;
  startApplyToSolr():void;
  stopApplyToSolr():void;
  startResyncSolr():void;
  startRunScheduleGroups(scheduleGroupIds:number[],scheduleGroupNames:string[],isScan:boolean,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean):void;
  startRunRootFolders(rootFolderIds:number[],rootFolderNames:string[],isScan:boolean,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean):void;
  stopRunScheduleGroup():void;
  pauseAllRuns():void;
  resumeAllRuns():void;
  startExtractionForList(bizListId,bizListName):void;
  pauseRunsByID(runIDs:number[],names:string[]);
  resumeRunsByID(runIDs:number[],names:string[]);
  pauseJobsByID(jobIDs:number[],names:string[]);
  resumeJobsByID(jobIDs:number[],names:string[]);
  forceRerunJobEnqueuedTasks(jobIDs:number[],names:string[]);
  forceRerunRunEnqueuedTasks(runID:number,name:string);
  stopRunsByID(runIDs:number[],names:string[]);
  stopRunScheduleGroup(scheduleGroupID,name);
  markForFullScan(rootFolderId:number,mark:boolean):void;
  updateScanStrategy(strategy:Operations.EJobProcessingMode):void;
  activeRunsChangedEvent: string;
  serverComponentsChangedEvent: string;
  applyToSolrStatusChangedEvent: string;
  runStatusOperationStateChangedEvent: string;
  rootFoldersAggregatedSummaryInfoEvent: string;
}


angular.module('services.scan', [

    ])
    .factory('scanService',  function($window, $rootScope, activeScansResource:IActiveScansResource, propertiesUtils,Logger:ILogger, $timeout, configuration:IConfig, scheduleGroupResource:IScheduleGroupResource,
                                      dashboardChartResource:IDashboardChartsResource, operationsResource:IOperationsResource, dialogs ,serverComponentResource:IServerComponentResource, $filter){
      var log:ILog = Logger.getInstance('scanService');
      var doPolling = false;
      var scanTimeoutPromise;
      var applyTimeoutPromise;
      var serverComponentsTimeoutPromise;
      var checkServerComponentsPromise;
      var activeRunsView:Operations.ActiveRunsView;
      var failedRunsView:Operations.ActiveRunsView;
      var rootFoldersAggregatedSummaryInfo:Operations.DtoRootFoldersAggregatedSummaryInfo;
      var serverComponentsView:Operations.ServerComponentsView;
      var applyToSolrStatusView:Operations.ApplyToSolrStatusView;
      var checkStatusPromise,checkApplyToSolrStatusPromise,checkServerComponentsStatusPromise,checkRootFoldersAggregatedSummaryInfo;
      var serverComponentsSummary = {};
      var forceCheckStatus=true;
      var checkStatus = function()
      {
        checkStatusPromise = activeScansResource.getActiveRuns(getActiveRunsCompleted,onGetError);
        checkRootFoldersAggregatedSummaryInfo = activeScansResource.getRootFoldersAggregatedSummaryInfo(getRootFoldersAggregatedSummaryInfoCompleted,onGetError);
      };

      var checkServerComponents = function(){
        checkServerComponentsStatusPromise = serverComponentResource.getServerComponents(serverComponentStatusCompleted,onGetError);
      }

      var checkApplyStatus = function()
      {
        checkApplyToSolrStatusPromise = operationsResource.applyTagsToSolrProgressStatus(applyTagsToSolrProgressCompleted,onGetError);
      };


      var init=function() {
      }

      var serverComponentStatusCompleted = function(components:Operations.DtoClaComponent[]) {

        // let brokers = components.filter((c)=> {
        //   return c.claComponentType == EClaComponentType.DB && c.active
        // });
        //
        // let appServers = components.filter((c)=> {
        //   return c.claComponentType == EClaComponentType.APPSERVER && c.active
        // });
        //
        // let brokers = components.filter((c)=> {
        //   return c.claComponentType == EClaComponentType.BROKER && c.active
        // });

        let mpLive = components.filter((c)=> {
          return c.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR &&
                 c.active &&
                (c.state == Operations.EComponentState.STARTING || c.state == Operations.EComponentState.OK)
        });

        let mpFailed = components.filter((c)=> {
          return c.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR &&
             c.active &&
            (c.state == Operations.EComponentState.FAULTY)
        });

        let mpDisabled = components.filter((c)=> {
          return c.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR && !c.active
        });

        let solrLive = components.filter((c)=> {
          return (c.claComponentType == Operations.EClaComponentType.SOLR_NODE) &&
            (c.state == Operations.EComponentState.STARTING || c.state == Operations.EComponentState.OK)
        });

        let solrFailed = components.filter((c)=> {
          return (c.claComponentType == Operations.EClaComponentType.SOLR_NODE) &&
            (c.state == Operations.EComponentState.FAULTY)
        });

        serverComponentsSummary = {
          dbs: 1, //appServers.length,
          brokers: 1, //brokers.length,
          appServers: 1, //appServers.length,
          mpLive: mpLive.length,
          mpFailed: mpFailed.length,
          mpDisabled : mpDisabled.length,
          solrLive: solrLive.length,
          solrFailed: solrFailed.length
        };

        $rootScope.$broadcast(factory.serverComponentsChangedEvent);

        if(doPolling) {
          serverComponentsTimeoutPromise= $timeout(checkServerComponents, configuration.polling_server_components_interval_idle);
        }
      };

      var getActiveRunsCompleted = function(activeRuns:Operations.DtoRunSummaryInfo[]) {
       log.debug('get Active Run Status from server ***: '+activeRuns.length);

        // var activeRuns=lastRuns.filter(r=>r.crawlRunDetailsDto.runOutcomeState==Operations.ERunStatus.RUNNING);
        var activeRunsViewLocal = createRunsView(activeRuns);
        if(activeRunsView && activeRunsViewLocal.activeRuns.length == activeRunsView.activeRuns.length)
        {
          var notChanged = true;
          activeRunsViewLocal.activeRuns.forEach(r=> {
            notChanged = notChanged && activeRunsView.activeRuns.filter(a=>a.crawlRunDetailsDto.id == r.crawlRunDetailsDto.id)[0]!=null;
          });
        }
        var operationStateChanged = !notChanged;
        activeRunsView = activeRunsViewLocal;

        if(forceCheckStatus || operationStateChanged) {// inform that the local runs changed their status, for immediate grids refresh
          $rootScope.$broadcast(factory.runStatusOperationStateChangedEvent,activeRunsView);
        }
        forceCheckStatus = false;
        $rootScope.$broadcast(factory.activeRunsChangedEvent,failedRunsView);

        if(doPolling) {
          scanTimeoutPromise= $timeout(checkStatus, activeRunsView.activeRuns.length>0 ?configuration.pooling_interval_running:configuration.pooling_interval_idle);
        }
      };

      var getRootFoldersAggregatedSummaryInfoCompleted = function(_rootFoldersAggregatedSummaryInfo:Operations.DtoRootFoldersAggregatedSummaryInfo) {
          rootFoldersAggregatedSummaryInfo = _rootFoldersAggregatedSummaryInfo;
          $rootScope.$broadcast(factory.rootFoldersAggregatedSummaryInfoEvent,failedRunsView);
      };

      var createRunsView = function(activeRuns:Operations.DtoRunSummaryInfo[]):Operations.ActiveRunsView
      {
        var activeRunsViewLocal = new Operations.ActiveRunsView();
        activeRunsViewLocal.activeRuns = activeRuns;
        activeRunsViewLocal.activeRunsView = [];
        activeRunsViewLocal.activeRuns.forEach(r=>
          {

            var runView = createRunSummaryView(r);

            activeRunsViewLocal.activeRunsView.push(runView);
          }
        )

        return activeRunsViewLocal;
      }
      function createRunSummaryView(r:Operations.DtoRunSummaryInfo):Operations.DtoRunSummaryInfoView{

        var activeJobs;
        var runView = new Operations.DtoRunSummaryInfoView();

        var aggregatedJob = {
          arrType: [],
          arrName: [],
          total: 0,
          counter: 0,
          startTime: 0,
          tasksPerSecRate: 0,
          tasksPerSecRunningRate: 0
        };

        activeJobs = _.filter(r.jobs, (job) => {
          return job.jobState == Dashboards.EJobState.IN_PROGRESS || job.jobState == Dashboards.EJobState.READY;
        });
        //
        //if(_.isEmpty(activeJobs)) {
        //  activeJobs = _.filter(r.jobs, (job) => {
        //    return job.jobState == Dashboards.EJobState.READY
        //  });
        //}

        _.each(activeJobs, function(job) {
            aggregatedJob.arrType.push($filter('translate')('job_'+job.jobType));
            aggregatedJob.arrName.push(job.name);
            aggregatedJob.counter += job.phaseCounter || 0;
            aggregatedJob.tasksPerSecRate += job.tasksPerSecRate || 0;
            aggregatedJob.tasksPerSecRunningRate += job.tasksPerSecRunningRate || 0;
            aggregatedJob.startTime = Math.min(aggregatedJob.startTime, job.phaseStart);

            if (!_.isNull(aggregatedJob.total)) {
              aggregatedJob.total = job.phaseMaxMark ? aggregatedJob.total + job.phaseMaxMark : null;
            }
        });

        runView.id = r.crawlRunDetailsDto.id;
        runView.isRunning = r.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.RUNNING;
        if(r.crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.RUNNING)
        {
          runView.elapsedTime = (new Date().getTime())-r.crawlRunDetailsDto.scanStartTime;
        }
        else
        {
          runView.elapsedTime = (r.crawlRunDetailsDto.scanEndTime - r.crawlRunDetailsDto.scanStartTime) ;
        }
        runView.nickName = r.crawlRunDetailsDto.rootFolderDto.nickName?r.crawlRunDetailsDto.rootFolderDto.nickName:r.crawlRunDetailsDto.rootFolderDto.realPath;
        runView.runStatus = r.crawlRunDetailsDto.runOutcomeState;
        runView.startTime = r.crawlRunDetailsDto.scanStartTime?r.crawlRunDetailsDto.scanStartTime:null;
        runView.endTime = r.crawlRunDetailsDto.scanEndTime;
        runView.jobType = aggregatedJob.arrType.join(',');
        runView.jobName = aggregatedJob.arrName.join(',');
        runView.isManualRun = r.crawlRunDetailsDto.manual;
        runView.rootFolderId = r.crawlRunDetailsDto.rootFolderDto.id;
        runView.scheduleGroupName = r.crawlRunDetailsDto.scheduleGroupName;
        runView.scheduleGroupId = r.crawlRunDetailsDto.scheduleGroupId;
        // //limit to no more than 100% progress

        runView.estimatedPhaseFinish = null;
        runView.tasksPerSecRate = aggregatedJob.tasksPerSecRate;
        runView.currentPhaseProgressCounter = aggregatedJob.counter;
        runView.currentPhaseProgressFinishMark =  aggregatedJob.total || 0;
        runView.currentPhaseProgressPercentage = Math.floor( aggregatedJob.counter / (aggregatedJob.total || 1) * 100);
        runView.tasksPerSecRunningRate = aggregatedJob.tasksPerSecRunningRate?$filter('number')(aggregatedJob.tasksPerSecRunningRate, (aggregatedJob.tasksPerSecRunningRate<50)?2:0):0;

        if (runView.currentPhaseProgressFinishMark > 0 && runView.tasksPerSecRate > 0) {
          var counterLeft = runView.currentPhaseProgressFinishMark - runView.currentPhaseProgressCounter;
          var secondsLeft = counterLeft/runView.tasksPerSecRate + 60;  // Add an extra minute
          runView.estimatedPhaseFinish = (secondsLeft >= 3600*24) ?
            (secondsLeft/3600/24).toFixed(0)+' days and '+(((secondsLeft/3600))%24).toFixed(1)+' hours':
            $filter('date')(new Date(2016,1,1,0,0,secondsLeft,0),"HH:mm");
        }

        return runView;
      }

      var applyTagsToSolrProgressCompleted = function(status:Operations.DTOApplyToSolrStatus) {
        var _applyToSolrStatusView = new Operations.ApplyToSolrStatusView();
        log.debug('apply to solr check status '+status.applyToSolrStatus);
        _applyToSolrStatusView.applyStatus = status;
        _applyToSolrStatusView.currentPercentage=Math.round(status.applyCounter/status.applyCounterMaxMark*100);
        _applyToSolrStatusView.isRunning = status.applyToSolrStatus == Operations.DTOApplyToSolrStatus.status_IN_PROGRESS;
        _applyToSolrStatusView.enableStartApplyButton = status.applyToSolrStatus != Operations.DTOApplyToSolrStatus.status_IN_PROGRESS;
        applyToSolrStatusView=_applyToSolrStatusView;
        $rootScope.$broadcast(factory.applyToSolrStatusChangedEvent,applyToSolrStatusView);

        if(doPolling) {
          applyTimeoutPromise = $timeout(checkApplyStatus, applyToSolrStatusView.isRunning ? configuration.pooling_interval_running : configuration.pooling_interval_idle);
        }
      };

      var startServerComponentsPolling=function(){

      }


      var checkStatusAndReInitPolling=function(_doPolling)
      {
          doPolling = _doPolling;

          if(doPolling) {
            forceCheckStatus = true;
            $timeout.cancel(scanTimeoutPromise);

            if(checkStatusPromise) {
              checkStatusPromise.abort();
              checkStatusPromise=null;
            }
            if(checkServerComponentsStatusPromise) {
              checkServerComponentsStatusPromise=null;
            }
            checkStatus();
          }
      };

      var checkApplyStatusAndReInitPolling=function()
      {
        $timeout.cancel(applyTimeoutPromise);
        if(checkApplyToSolrStatusPromise)
        {
          checkApplyToSolrStatusPromise.abort();
          checkApplyToSolrStatusPromise=null;
        }
          checkApplyStatus();
      };

      var startApplyToSolr = function()
      {
      //  if(applyToSolrStatusView.enableStartApplyButton) {
          var dlg = dialogs.confirm('Confirmation', 'Are you sure you want to apply assignment updates?', null);
          dlg.result.then(function (btn) { //user confirmed deletion
            //applyToSolrStatusView.enableStartApplyButton = false;
            operationsResource.startApplyTagsToSolr(function () {
              checkApplyStatusAndReInitPolling();
            }, function (errMsg) {
              onGetError(errMsg)
            });

          }, function (btn) {
            //  'You confirmed "No."';
          });
    //    }
     //   else {
     //     dialogs.notify('Notification', 'Apply changes process is currently running');
     //   }
      };

      var startResyncSolr = function()
      {
        if(applyToSolrStatusView.enableStartApplyButton) {
          var dlg = dialogs.confirm('Confirmation', 'Are you sure you want to re-sync indexes?', null);
          dlg.result.then(function (btn) { //user confirmed deletion
            //applyToSolrStatusView.enableStartApplyButton = false;
            operationsResource.startResyncSolr(function () {
              checkApplyStatusAndReInitPolling();
            }, function (errMsg) {
              onGetError(errMsg)
            });

          }, function (btn) {
            //  'You confirmed "No."';
          });
        }
        else {
          dialogs.notify('Notification', 'Apply changes process is currently running');
        }
      };

      var stopApplyToSolr = function()
      {
        var dlg = dialogs.confirm('Confirmation', 'Are you sure you want to stop applying tags?', null);
        dlg.result.then(function (btn) { //user confirmed deletion
          operationsResource.stopApplyTagsToSolr(function(){
            checkStatusAndReInitPolling(true);

          },onGetError);
        }, function (btn) {
          //  'You confirmed "No."';
        });


      };

      var startExtractionForList = function(bizListId,bizListName)
      {
          var dlg = dialogs.confirm('Confirmation', 'Start extraction for list ' + bizListName + '?', null);
          dlg.result.then(function (btn) { //user confirmed deletion
            activeScansResource.startBizListExtraction(bizListId, function () {
                  checkStatusAndReInitPolling(true);
                 },
                onGetError
               );
           }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
      };

      var startRunScheduleGroups=function(scheduleGroupIds:number[],scheduleGroupNames:string[],isScan:boolean,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean)
      {
          var dlg = dialogs.confirm('Confirmation', 'Start processing schedule groups <br/>'+scheduleGroupNames.join('<br/>')+'?', null);

          dlg.result.then(function (btn) { //user confirmed deletion

            scheduleGroupResource.startRunForScheduleGroups(scheduleGroupIds,isScan,isCompleteScan,isExtractAfterAnalyze,isExtractOnly, function () {

                  checkStatusAndReInitPolling(true);

                },
                function (e) {
                  if (e.status == 400 && e.data.type == ERequestErrorCode.LICENSE_EXPIRED) {
                    dialogs.error('Scan', 'Scan license expired!');
                  }
                  else {
                    onGetError(e)
                  }
                });
          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
      };

      var markForFullScan = function(rootFolderId:number, mark:boolean) {
        let action = mark? 'mark' : 'unmark';
        var dlg = dialogs.confirm('Confirmation', `Are you sure you want to ${action} for full scan?`, null);
        dlg.result.then(function (btn) {

          activeScansResource.markForFullScan(rootFolderId, mark, function () {
            checkStatusAndReInitPolling(true);
          }, onGetError);
        }, function (btn) {
          // $scope.confirmed = 'You confirmed "No."';
        });
      };

      var updateScanStrategy = function(strategy:Operations.EJobProcessingMode) {
        activeScansResource.updateScanStrategy(strategy, function () {
        }, onGetError);
      };

      var startRunRootFolders=function(rootFolderIds:number[],rootFolderNames:string[],isScan,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean)
      {
          var dlg = dialogs.confirm('Confirmation', 'Start processing root folder'+(rootFolderNames.length > 1 ? 's' : '') +' <br/>'+rootFolderNames.join('<br/>')+'?', null);

          dlg.result.then(function (btn) { //user confirmed deletion

            activeScansResource.startScanRootFolders(rootFolderIds,isScan,isCompleteScan,isExtractAfterAnalyze,isExtractOnly, function () {

                  checkStatusAndReInitPolling(true);

                },
                function (e) {

                  if (e.status == 400 && e.data.type == ERequestErrorCode.LICENSE_EXPIRED) {
                    dialogs.error('Scan', 'Scan license expired!');
                  }
                  else {
                    onGetError(e)
                  }
                });
          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
       };

      var  pauseRunsByID= function(runIDs:number[],names:string[])
            {
             var displayedNames =  names.join('<br/>');
              var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to pause processing:<br/>'+displayedNames+'<br>Note: Scan remaining in paused status for more than '+ configuration['handle-stuck-in-pause-runs.stop-threshold-days']+' days is stopped!', null);
              dlg.result.then(function(btn){ //user confirmed deletion
                //It may take up to a minute for the stop to actually happen
                activeScansResource.pauseRunsByID(runIDs,function(){
                  checkStatusAndReInitPolling(true);

                },onGetError);
              },function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });

            };
     var  pauseJobsByID= function(jobIDs:number[],names:string[])
            {
             var displayedNames =  names.join('<br/>');
              var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to pause processing:<br/>'+displayedNames, null);
              dlg.result.then(function(btn){ //user confirmed deletion
                //It may take up to a minute for the stop to actually happen
                activeScansResource.pauseJobsByID(jobIDs,function(){
                  checkStatusAndReInitPolling(true);

                },onGetError);
              },function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });

            };

      var  stopRunsByID= function(runIDs:number[],names:string[])
      {
        var displayedNames =  names.join('<br/>');
        var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to stop processing:<br/>'+displayedNames, null);
        dlg.result.then(function(btn){ //user confirmed deletion
          //It may take up to a minute for the stop to actually happen
          activeScansResource.stopRunsByID(runIDs,function(){
            checkStatusAndReInitPolling(true);

          },onGetError);
        },function(btn){
          // $scope.confirmed = 'You confirmed "No."';
        });

      };

      var  resumeRunsByID= function(runIDs:number[],names:string[])
      {
        var displayedNames =  names.join('<br/>');
        var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to continue processing:<br/>'+displayedNames, null);
        dlg.result.then(function(btn){ //user confirmed deletion
          //It may take up to a minute for the stop to actually happen
          activeScansResource.resumeRunsByID(runIDs,function(){
            checkStatusAndReInitPolling(true);

          },onGetError);
        },function(btn){
          // $scope.confirmed = 'You confirmed "No."';
        });

      };
      var  resumeJobsByID= function(jobIDs:number[],names:string[])
      {
        var displayedNames =  names.join('<br/>');
        var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to continue processing:<br/>'+displayedNames, null);
        dlg.result.then(function(btn){ //user confirmed deletion
          activeScansResource.resumeJobsByID(jobIDs,function(){
            checkStatusAndReInitPolling(true);

          },onGetError);
        },function(btn){
          // $scope.confirmed = 'You confirmed "No."';
        });

      };
      var  forceRerunJobEnqueuedTasks= function(jobIDs:number[],names:string[])
      {
        var displayedNames =  names.join('<br/>');
        var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to force re-run enqueued tasks for:<br/>'+displayedNames, null);
        dlg.result.then(function(btn){ //user confirmed deletion
          activeScansResource.forceRerunJobEnqueuedTasks(jobIDs,function(){
           // checkStatusAndReInitPooling();

          },onGetError);
        },function(btn){
          // $scope.confirmed = 'You confirmed "No."';
        });

      };
      var  forceRerunRunEnqueuedTasks= function(runId:number,name:string)
      {

        var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to force re-run enqueued tasks for:<br/>'+name, null);
        dlg.result.then(function(btn){ //user confirmed deletion
          activeScansResource.forceRerunRunEnqueuedTasks(runId,function(){
         //   checkStatusAndReInitPooling();

          },onGetError);
        },function(btn){
          // $scope.confirmed = 'You confirmed "No."';
        });

      };

      var  resumeAllRuns= function()
            {

              var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to resume *ALL* pause runs?', null);
              dlg.result.then(function(btn){ //user confirmed deletion
                 activeScansResource.resumeAllRuns(function(){
                  checkStatusAndReInitPolling(true);

                },onGetError);
              },function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });

            };

      var  pauseAllRuns= function()
            {
              var dlg:any = dialogs.confirm('Confirmation', 'Are you sure you want to pause *ALL* processing?', null);
              dlg.result.then(function(btn){ //user confirmed deletion
                //It may take up to a minute for the stop to actually happen
                activeScansResource.pauseAllRuns(function(){
                  checkStatusAndReInitPolling(true);

                },onGetError);
              },function(btn){
                // $scope.confirmed = 'You confirmed "No."';
              });

            };

      $rootScope.$on('$destroy', function(){
        if (angular.isDefined(applyTimeoutPromise)) {
          $timeout.cancel(applyTimeoutPromise);
          applyTimeoutPromise = undefined;
        }
        if (angular.isDefined(scanTimeoutPromise)) {
          $timeout.cancel(scanTimeoutPromise);
          scanTimeoutPromise = undefined;
        }
        if (angular.isDefined(serverComponentsTimeoutPromise)) {
          $timeout.cancel(serverComponentsTimeoutPromise);
          serverComponentsTimeoutPromise = undefined;
        }

        if(checkStatusPromise)
        {
          checkStatusPromise.abort();
          checkStatusPromise=null;
        }
        if(checkServerComponentsPromise)
        {
          checkServerComponentsPromise.abort();
          checkServerComponentsPromise=null;
        }
        if(checkServerComponentsStatusPromise)
        {
          checkServerComponentsStatusPromise.abort();
          checkServerComponentsStatusPromise=null;
        }
        if(checkApplyToSolrStatusPromise)
        {
          checkApplyToSolrStatusPromise.abort();
          checkApplyToSolrStatusPromise=null;
        }
      });



      var onGetError=function(e)
      {

        log.error("error running status " + JSON.stringify( e));

      };

      var factory:IScanService = <IScanService>{};
      factory.activeRunsChangedEvent = 'activeRunsChangedEvent';
      factory.applyToSolrStatusChangedEvent = 'applyToSolrStatusChangedEvent';
      factory.runStatusOperationStateChangedEvent = 'runStatusOperationStateChangedEvent';
      factory.getActiveRuns=function():Operations.ActiveRunsView
      {
        return activeRunsView;
      };
      factory.getRootFoldersAggregatedSummaryInfo=function():Operations.DtoRootFoldersAggregatedSummaryInfo
      {
        return rootFoldersAggregatedSummaryInfo;
      };
      factory.getServerComponentsStatus = function() {
        return serverComponentsSummary;
      };
      factory.getMediaProcessors=function(dataCenterId):Operations.DtoClaComponent[]
      {
        return serverComponentsView? serverComponentsView.components.filter(c=>c.claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR && c.customerDataCenterDto.id == dataCenterId):[];
      };
      factory.getActiveExtractionRunForBizList=function(bizListId:number):Operations.DtoRunSummaryInfo
      {
        if(activeRunsView && activeRunsView.activeRuns[0]){
         var extractBizListActiveRun = activeRunsView.activeRuns.filter(r=>r.jobs.filter(j=>j.bizListId == bizListId)[0]!=null)[0];
          return extractBizListActiveRun;
        }
        return null;
      };
      factory.getApplyToSolrStatus=function()
      {
        return applyToSolrStatusView;
      };
      factory.startApplyToSolr=function()
      {
        startApplyToSolr();
      };
      factory.startResyncSolr=function()
      {
        startResyncSolr();
      };
      factory.stopApplyToSolr=function()
      {
        stopApplyToSolr();
      };
      factory.startExtractionForList=function(bizListId,bizListName)
      {
        startExtractionForList(bizListId,bizListName);
      };

      factory.pauseRunsByID=function(runIDs:number[],names:string[])
      {
        pauseRunsByID(runIDs,names);
      };
      factory.pauseJobsByID=function(jobIDs:number[],names:string[])
      {
        pauseJobsByID(jobIDs,names);
      };
      factory.stopRunsByID=function(runIDs:number[],names:string[])
      {
        stopRunsByID(runIDs,names);
      };
      factory.resumeAllRuns=function()
      {
        resumeAllRuns();
      };

      factory.pauseAllRuns=function()
      {
        pauseAllRuns();
      };
      factory.resumeRunsByID=function(runIDs:number[],names:string[])
      {
        resumeRunsByID(runIDs,names);
      };
      factory.resumeJobsByID=function(jobIDs:number[],names:string[])
      {
        resumeJobsByID(jobIDs,names);
      };
      factory.forceRerunJobEnqueuedTasks=function(jobIDs:number[],names:string[])
      {
        forceRerunJobEnqueuedTasks(jobIDs,names);
      };
      factory.forceRerunRunEnqueuedTasks=function(runID:number,name:string)
      {
        forceRerunRunEnqueuedTasks(runID,name);
      };
      factory.forceRunStatusRefresh=function(force)
      {
        checkStatusAndReInitPolling(force);
      };
      factory.forceServerComponentsRefresh=function()
      {
        checkServerComponents();
      };
      factory.startRunScheduleGroups=function(scheduleGroupIds:number[], scheduleGroupNames:string[],isScan,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean)
      {
        startRunScheduleGroups(scheduleGroupIds,scheduleGroupNames,isScan,isCompleteScan,isExtractAfterAnalyze,isExtractOnly);
      };
      factory.markForFullScan=function(rootFolderId:number, mark:boolean)
      {
        markForFullScan(rootFolderId, mark);
      };
      factory.updateScanStrategy = function(strategy:Operations.EJobProcessingMode)
      {
        updateScanStrategy(strategy);
      };
      factory.startRunRootFolders=function(rootFolderIds:number[],rootFolderNames:string[],isScan,isCompleteScan:boolean,isExtractAfterAnalyze:boolean,isExtractOnly:boolean)
      {
        startRunRootFolders(rootFolderIds,rootFolderNames,isScan,isCompleteScan,isExtractAfterAnalyze,isExtractOnly);
      };


      init();

      return factory;
    });

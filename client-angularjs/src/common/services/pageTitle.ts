///<reference path='../all.d.ts'/>
'use strict';
interface IPageTitleService
{
  set(title:string):void;
  get():string;

}

angular.module('services.pageTitle', ['doc2Services'

])
  .factory('pageTitle',  function($rootScope, routerChangeService) {

    var pageTitle = '';
    var titleService:IPageTitleService = <IPageTitleService>{};

   //set defualt title if not explicitly set
    $rootScope.$on('stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){
        titleService.set(toState);
      })


    titleService.set = function (title) {
      pageTitle = title;
      routerChangeService.setTitle(title);

    };

    titleService.get = function () {
      return pageTitle;
    };

    return titleService;
  });

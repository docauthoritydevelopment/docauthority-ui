///<reference path='../all.d.ts'/>
'use strict';


interface IRegisteredEvent {
  activate(registrarScope:any, registrationParam?:any, fireParam?:any): void;
}

enum EEventServiceEventTypes {
  ReportSearchBoxUserAction = 10000,            // Generator: searchBox; Consumer: any; Purpose: signal user search action
  ReportSearchBoxSetState = 10001,            // Generator: any; Consumer: searchBox; Purpose: set search box state from another component
  ReportFilterTagsUserAction = 10002,            // Generator: filterTags; Consumer: any; Purpose:signal user action
  ReportFilterTagsSetState = 10003,            // Generator: any; Consumer: filterTags; Purpose:set filter tags state from another component
  ReportTagsMenuUserAction = 10004,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportTagsMenuSetState = 10005,            // Generator: any; Consumer: tagsMenu; Purpose:set tagsMenu state from another component
  ReportSetSearchClearUserInput = 10006,    // Generator: any; Consumer: searchBox; Purpose: make search box clear user input
  // ReportNotifyReady = 10007,                // Generator: ?; Consumer: ?; Purpose: ?
  ReportGridTagsUserAction = 10008,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridFilterUserAction = 10009,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportDocTypeMenuUserAction = 10010,            // Generator: docTypesMenu; Consumer: any; Purpose:signal user action
  ReportHeaderSetState = 10011,            // Generator: any; Consumer: header; Purpose:set saved filters after edit them in report page
  ReportHeaderUserAction = 10311,            // Generator: any; Consumer: header; Purpose:set saved filters after edit them in report page
  ReportGridDocTypesUserAction = 10012,            // Generator: grid; Consumer: docTypesMenu; Purpose:signal user action
  ReportDocTypesManageMenuUserAction = 10013,            // Generator: docTypesManageMenu; Consumer: docTypesMenu; Purpose:signal user action

  ReportGridBizListUserAction = 10014,            // Generator: grid; Consumer: bizListMenu; Purpose:signal user action
  ReportGridManageBizListUserAction = 10014,            // Generator: grid; Consumer: bizListMenu; Purpose:signal user action
  ReportBizListAssociationMenuUserAction = 10015,            // Generator: menu; Consumer: bizListMenu; Purpose:signal user action

  ReportDepartmentUserAction = 10016,            // Generator: menu; Consumer: department; Purpose:signal user action

  ReportReportDDLMenuUserAction = 10017,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportActionsRunningTasksStatusUserAction = 10018,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportChartUserAction = 10019,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportGridPoliciesUserAction = 10020,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportPolicyItemDialogUserAction = 10021,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridRootFoldersUserAction = 10022,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridScheduleGroupUserAction = 10023,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action

  ReportDateFilterMenuUserAction = 10024,            // Generator: docTypesMenu; Consumer: any; Purpose:signal user action
  ReportGridSearchPatternsUserAction = 10025,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridDataChangeUserAction = 10026,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridUsersUserAction = 10027,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportTrendChartUserAction = 10028,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportGridMediaTypeConnectionsUserAction = 10029,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportProfileUserUserAction = 10030,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridBizRolesUserAction = 10031,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridMarkItemUserAction = 10032,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportMarkedItemMenuUserAction = 10033,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridTagTypesManageUserAction = 10034,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGDPRGridTagsUserAction = 10035,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGDPRGridProgressUserAction = 10036,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridServerComponentsUserAction = 10037,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action

  ReportBizListAutoGenerateAction = 10038,
  ReportBizListApproveAllPendingAction = 10039,
  ReportBizlistUploadAction = 10040,

  ReportFunctionalRolesAction = 10041,
  ReportGridRootFoldersActiveScanActions = 10042,
  ReportGridRootFoldersActions = 10043,
  ReportGridCustomerDataCenterUserAction = 10044,
  ReportGridTemplateRolesUserAction = 10045,
  ReportRootFolderByTexSetState = 10046,
  ReportBizlistRefreshMenu = 10047,
  ReportGridDateMenuChangeAction = 10048,
  ReportGridLdapAction = 10049,

  ReportGridDepartmentUserAction = 10050,            // Generator: grid; Consumer: departmentMenu; Purpose:signal user action
  ReportDepartmentMenuUserAction = 10051,            // Generator: departmentMenu; Consumer: any; Purpose:signal user action

  ReportMatterMenuUserAction = 10052,            // Generator: matterMenu; Consumer: any; Purpose:signal user action

  ReportDiscoverViewChanged = 10053,

}

class RegisteredEvent implements IRegisteredEvent {
  handler:any;
  constructor(handler:any) {
    this.handler = handler;
    if (handler == null || (typeof handler) !== "function") {
      console.error("Bad handler");
    }
  }
  activate = function(registrarScope:any, registrationParam:any, fireParam:any) {
    if (this.handler && typeof this.handler === "function") {
      this.handler(registrarScope, registrationParam, fireParam);
    }
  }
}

interface IEventCommunicator {
  registerHandler(eventName:EEventServiceEventTypes, registrarScope:any, handler:IRegisteredEvent, registrationParam?:any):number;
  unregisterHandler(eventName:EEventServiceEventTypes, registrarScope:any):number;
  unregisterAllHandlers( registrarScope:any);
  fireEvent(eventName:EEventServiceEventTypes, fireParam?:any):number;
  //constructor(...eventNames:string[]);
}

class EventHandlerData {
  registrarScope:any;
  handler:IRegisteredEvent;
  registrationParam:any;
  lastActivated:number;

  static minActivationPeriod_ms:number = 100;

  constructor(registrarScope:any, handler:IRegisteredEvent, registrationParam?:any) {
    this.registrarScope = registrarScope;
    this.handler = handler;
    this.registrationParam = registrationParam;
    this.lastActivated = 0;
  }
  equals(other:EventHandlerData) {
    return other && other.registrarScope===this.registrarScope && other.handler===this.handler && other.registrationParam===this.registrationParam;
  }
  isLoop():boolean {
    var now:number = (new Date()).getTime();
    return (now - this.lastActivated) < EventHandlerData.minActivationPeriod_ms;
  }
  getTimeFromLast():number {
    var now:number = (new Date()).getTime();
    return (now - this.lastActivated);
  }
  setLastActivated():void {
    this.lastActivated = (new Date()).getTime();
  }
}

/*
 * Usage:
 * Triggering component:
 *     eventCommunicator.fireEvent('ExcelExport', optionalParam);
 *
 *
 * Firing component:
 *      eventCommunicator.registerHandler('ExcelExport',$scope, (new RegisteredEvent(function( registrarScope:any, registrationParam?:any, fireParam?:any ) {
 *        _this.exportToExcel();
 *      })));
 *
 *
 */

angular.module('services.events',[])
  .service('eventCommunicator',
  function (Logger:ILogger) {
    var _this:IEventCommunicator = this;
    var _local = this;
    var log: ILog = Logger.getInstance('eventCommunicator');

    _local.handlers = {};

    //_this.constructor = function (...eventNames:string[]) {
    //  _local.supportedEvents = eventNames;
    //  for (var i=0;i<_local.supportedEvents.length;++i) {
    //    _local.handlers[_local.supportedEvents[i]] = [];
    //  }
    //};

    _this.registerHandler = function(eventName:EEventServiceEventTypes, registrarScope:any, handler:IRegisteredEvent, registrationParam?:any) {
      if (!_local.handlers[eventName]) {
        //log.error('Unsupported event name: '+ eventName);
        //return -1;
        _local.handlers[eventName] = [];
      }
      var ehd:EventHandlerData = new EventHandlerData(registrarScope, handler, registrationParam);
      _local.handlers[eventName].forEach(h=>{
        if (h.equals(ehd)) {
          log.debug('Duplicate handler found for event '+eventName + ' #'+_local.handlers[eventName].length);
          return -1;
        }
      });
      _local.handlers[eventName].push(ehd);
      log.debug('Set handler for event '+eventName + ' #'+_local.handlers[eventName].length);
      return 0;
    };

    _this.unregisterHandler = function(eventName:EEventServiceEventTypes, registrarScope:any) {
      if ((!_local.handlers[eventName]) || _local.handlers[eventName].length==0) {
        log.error('No handlers for event name: ' + eventName);
        return -1;
      }
      var numFound = 0;
      var found=false;
      do {
        for (var i = 0; i < _local.handlers[eventName].length; ++i) {
          var h:EventHandlerData = _local.handlers[eventName][i];
          if (h.registrarScope === registrarScope) {
            _local.handlers[eventName].splice(i, 1);
            numFound++;
            found = true;
            break;
          }
        }
      } while (found);
      return numFound;
    };


    _this.unregisterAllHandlers = function(registrarScope)
    {
      if(_local.handlers) {
          for (var eventName in  _local.handlers) {
            for (var i = 0; i < _local.handlers[eventName].length; ++i) {
              var h:EventHandlerData = _local.handlers[eventName][i];
              if (h.registrarScope === registrarScope) {
                _local.handlers[eventName].splice(i, 1);

              }
            }
          }
        }

    }
    _this.fireEvent = function(eventName:EEventServiceEventTypes, fireParam?:any) {
      if ((!_local.handlers[eventName]) || _local.handlers[eventName].length==0) {
        log.info('No handlers for event name: ' + eventName);
        return -1;
      }
      for (var i=0;i<_local.handlers[eventName].length;++i) {
        var h:EventHandlerData = _local.handlers[eventName][i];

        if (h.isLoop()) {
          log.warn("Potential event firing loop. not activating event " + eventName +
              ". Activated " + h.getTimeFromLast() + "ms ago. " +
              "fireParam=" + (fireParam?JSON.stringify(fireParam):"<null>"));
        }
        else {
          h.setLastActivated();
          h.handler.activate(h.registrarScope, h.registrationParam, fireParam);
        }
      }
      return 0;
    };

  });

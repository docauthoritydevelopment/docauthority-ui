///<reference path='../all.d.ts'/>
'use strict';

interface INotificationsService {

  pushSticky (notification:INotification):void;
  pushForCurrentRoute(notification:INotification):void;
  pushForNextRoute(notification:INotification):void;
  getCurrent():void;
  remove(notification:INotification):void;
  removeAll(notification:INotification):void;

}
class INotification {
  message: string;
  type: ENotificationType;


}

enum  ENotificationType {
  error= <any>"error",
  success= <any>"success",

}


angular.module('services.notifications', [

])
  .factory('notifications',  function ($injector,Logger:ILogger,$rootScope) {

    var log: ILog = Logger.getInstance('notifications');
  var notifications = {};
    notifications['STICKY'] =<INotification[]>[];
    notifications['ROUTE_CURRENT'] =<INotification[]>[];
    notifications['ROUTE_NEXT'] =<INotification[]>[];

  var notificationsService:INotificationsService = <INotificationsService>{};

  notificationsService.getCurrent = function(){
    return [].concat( notifications['STICKY'],  notifications['ROUTE_CURRENT']);
  };


    notificationsService.pushSticky = function(notification:INotification) {
    if(notification) {
      notifications['STICKY'].push(notification);
      logNotification(notification);
    }
  };

  notificationsService.pushForCurrentRoute = function(notification) {
    if(notification) {
      notifications['ROUTE_CURRENT'].push(notification);
      logNotification(notification);
    }
  };

  notificationsService.pushForNextRoute = function(notification) {
    if(notification) {
      notifications['ROUTE_NEXT'].push(notification);
      logNotification(notification);
    }
  };

  notificationsService.remove = function(notification){
    angular.forEach(notifications, function (notificationsByType:INotification[]) {
      var idx = notificationsByType.indexOf(notification);
      if (idx>-1){
        notificationsByType.splice(idx,1);
      }
    });
  };

  notificationsService.removeAll = function(){
    angular.forEach(notifications, function (notificationsByType:INotification[]) {
      notificationsByType.length = 0;
    });
  };

   // let $rootScope = $injector.get('$rootScope');
   $rootScope.$on('$routeChangeSuccess', function () {
      notifications['ROUTE_CURRENT'].length = 0;

      notifications['ROUTE_CURRENT'] = angular.copy(notifications['ROUTE_NEXT']);
      notifications['ROUTE_NEXT'].length = 0;
    });



    var  logNotification = function(notification:INotification) {
      if (notification.type == ENotificationType.error) {
        log.error(notification.message)
      }
      else {
        log.info(notification.message)
      }
    }
  return notificationsService;
});

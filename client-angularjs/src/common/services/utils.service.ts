///<reference path='../all.d.ts'/>
'use strict';

angular.module('services.utils',[])
  .factory('apiUtils', function ($httpParamSerializer) {

    // Public API here
    var service = {

      /**
       * Convert array of row-arrays into an array of row-objects
       * @param  {Array} arrayOfArrays  - The array of arrays to be converted
       * @param  {Function} converter   - single row converter
       * @return {Array}                - The converted object array
       */
      convertArrayToObjects: function (arrayOfArrays, converter) {
        if (!Array.isArray(arrayOfArrays)) {
          return arrayOfArrays;
        }
        var arrayOfObjects = [];
        arrayOfArrays.forEach(function (arr) {
          if (!Array.isArray(arr)) {
            arrayOfObjects.push(arr);
          } else { // need to convert array to object
            arrayOfObjects.push(converter(arr));
          }
        });
        //$log.debug(JSON.stringify(arrayOfObjects));
        return arrayOfObjects;
      },
      /**
       * returns the basename of a path
       * @param  {String} path  - a path string
       * @return {String}       - the path basename
       */
      basename: function (path) {
        var inx = path.lastIndexOf('/');
        if (inx < 0) {
          inx = path.lastIndexOf('\\');
        }
        if (inx < 0) {
          return path;
        }
        return path.substr(inx + 1);
      },
      parseQuery:function(str) {
        return (str).replace('?','&').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
      },
      buildUrl:function buildUrl(url:string, params:any) :string{
      var serializedParams = $httpParamSerializer(params);

      if (serializedParams.length > 0) {
        url += ((url.indexOf('?') === -1) ? '?' : '&') + serializedParams;
      }

      return url;
     },
      triggerWindowResizeEvent: function() {
        if (typeof (Event) === 'function') { //modern browsers
          window.dispatchEvent(new Event('resize'));
        } else { //IE11
          let resizeEvent = <any>(window.document.createEvent('UIEvents'));
          resizeEvent.initUIEvent('resize', true, false, window, 0);
          window.dispatchEvent(resizeEvent);
        }
      }
    };
    return service;
  })
  .factory('propertiesUtils', function () {

    // Public API here
    var service = {
      parseQuery:function(str) {
        return (str).replace('?','&').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
      },
      creatUuid : function() {
        var buf = new Uint32Array(4);
        if(!window.crypto)
        {
          return Date.now().toString();
        }

        window.crypto.getRandomValues(buf);
        var idx = -1;
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          idx++;
          var r = (buf[idx>>3] >> ((idx%8)*4))&15;
          var v = c == 'x' ? r : (r&0x3|0x8);
          return v.toString(16);
        });
      },
      propName: function (prop, value) {
        var res = '';
        propName(prop, value);
        function propName(prop, value) {
          for (var i in prop) {
            if (typeof prop[i] == 'object') {
              if (propName(prop[i], value)) {
                return res;
              }
            }
            else {
              if (prop[i] == value) {
                res = i;
                return res;
              }
            }
          }
          return undefined;
        }

        return res;
      },
      isInteger: function (value) {
        if (isNaN(parseInt(value))) {
          return false;
        }
        return true;
      },
      isBoolean: function (value) {
        if (value.trim().toLowerCase() == "true" || value.trim().toLowerCase() == "false") {
          return true;
        }

        return false;
      },
      parseBoolean: function (value) {
        if (value.trim().toLowerCase() == "true") {
          return true;
        }
        else if (value.trim().toLowerCase() == "false") {
          return false;
        }

        return null;
      },

      unique: function (arr:any[],func?) {
        if (arr && arr.length > 0) {
          var a = [], l = arr.length;
          for (var i = 0; i < l; i++) {
            for (var j = i + 1; j < l; j++) {
             if(func)
             {
               if(func(arr[i]) == func(arr[j]))
               {
                 j = ++i
               }
             }
              else {
               if (arr[i] === arr[j]) {
                 j = ++i
               }
               ;
             }
            }
            a.push(arr[i]);
          }
          return a;
        }
        return null;
      },

      countOccurencs: function(arr:any[],func)
      {
        var counts = [];
        for(let i = 0; i< arr.length; i++) {
          var key = func(arr[i]);
          counts[key] = counts[key] ? counts[key]+1 : 1;
        }
        var result=[];
        for(let key in counts) {
          result.push({name:key,value:counts[key]});
        }
        return result;
      },

     sortAlphbeticFun:function(sortFieldName:string) {
       return function (t1, t2) {
         if (t1[sortFieldName] > t2[sortFieldName]) {
           return 1;
         }
         if (t1[sortFieldName] < t2[sortFieldName]) {
           return -1;
         }
         return 0;
       }
     },

      getDataFromDotted:function( dataItem,fieldName)
      {
        var result;
        if(fieldName.indexOf('.')<0) {
          result = dataItem[fieldName];
        }
        else
        {
          var fields =fieldName.split('.');
          var data = dataItem;
          fields.forEach(f=>data = data[f]);
          result = data;
        }
        return result
      },
      capitalize:function(str:string) {
        if(str&&str.charAt)
        {
          return str.charAt(0).toUpperCase() + str.slice(1);
        }
        return null;
    },
      ellipsisMiddle:function(path:string,baseName:string,maxLength:number,maxLength_basename:number) {
        var result;
       if(path && path.length>maxLength&&(path.length-maxLength)>2) {
          var baseNamePartLength = baseName ? Math.min(baseName.length ,maxLength_basename) : maxLength_basename;
         result = path.substr(0,Math.min(path.length - baseNamePartLength, maxLength-baseNamePartLength) ) +'...'+ path.substr(path.length -baseNamePartLength ,  baseNamePartLength );
        }
        else {
         result = path;
        }
        return result;
      },
      decodeURIParam:function(param:any):string {
        let decodedParam = decodeURIComponent(param);
        return (decodedParam !== 'null' && decodedParam !== 'undefined' && decodedParam !== '') ? decodedParam : null;
      },
      isStrCurrency(str) {
        return /^\$?(?:\d+|\d{1,}(?:,\d{3})*)(?:\.\d{1,}){0,1}$/.test(str);
      },
      isValidDate(sDate) {
        try {
          let currDate = (new Date(sDate)).getTime();
          return currDate > 0;
        }
        catch(err){
          return false;
        }
      },
      isValidShortDate(str) {
        let re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

        if(this.isValidDate(str)) {
          return str.match(re)
        }
        return false;
      },
      getIconStyleId(max_colors:string, id:number, parentId:number) {
        var colors = parseInt(max_colors);
        if (parentId) {
          return parentId % colors + 1;
        }
        return id % colors + 1;
      }
  };
    return service;
});


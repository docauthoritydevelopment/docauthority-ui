
interface IConfig {
  'advanced-search-enabled':string;
  'share-permissions-enabled':string;
  'facet-count-hll.lower-bound':number
  BACKEND_SERVER: string;
  csrf_token_header_key:string;
  csrf_token_header_freepass_value:string;
  http_request_timeout:number;
  //////////////// REPORTS //////////////////////////

  //FILTERS
  contentFilterFieldName :string,
  labelFieldName :string,
  sharePermissionFieldName :string,
  tagFilterFieldName :string,
  noneMailFilterFieldName: string,
  openAccessRightsFilterFieldName: string,
  ungroupedFilterFieldName: string,
  expiredUserFilterFieldName: string,
  fileGeneralProcessingStateFilterFieldName: string,
  fileSpecificProcessingStateFilterFieldName: string,
  mailFilterFieldName :string,
  recipientAddressFilterFieldName: string,
  senderAddressFilterFieldName:string,
  recipientDomainFilterFieldName: string,
  senderDomainFilterFieldName:string,
  tagTypeFilterFieldName :string,
  docTypeFilterFieldName :string,
  functionalRoleFilterFieldName :string,
  bizListItemAssociationFilterFieldName :string,
  rootFolderNameLike :string,
  bizListItemAssociationTypeFilterFieldName :string,
  bizListItemExtractionFilterFieldName :string,
  bizListItemExtractionTypeFilterFieldName :string,
  bizListItemExtractionRuleFilterFieldName :string,
  subDocTypesFilterFieldName :string,
  fileDuplicationsFilterFieldName :string,
  subFolderFilterFieldName :string,
  departmentFilterFieldName :string,
  subDepartmentFilterFieldName :string,
  matterFilterFieldName :string,
  subMatterFilterFieldName :string,
  tagTypeMismatchFilterFieldName :string,
  fullyConatinedFilterFieldName :string,
  fileCreatedDatesFilterFieldName :string,
  fileModifiedDatesFilterFieldName :string,
  fileAccessedDatesFilterFieldName: string,
  'exchange-365.support-enabled': string,
  fileContentFilterFieldName : string,
  fileExtensionFilterFieldName : string,
  regulationFilterFieldName : string,
  regulationNameFilterFieldName : string,
  metadataFilterFieldName : string,
  metadataTypeFilterFieldName : string,
  patternFilterFieldName : string,
  mediaTypeFilterFieldName : string,
  patternMultiFilterFieldName : string,
  labelFilterFieldName : string,
  sharePermissionFilterFieldName : string,
  fileTypeCategoryFilterFieldName : string,
  fileTagTypeNameFilterFieldName : string,
  subDocTypeNameFilterFieldName : string,
  subDepartmentNameFilterFieldName : string,
  patternNameFilterFieldName : string,
  patternManyNameFilterFieldName : string,
  fileTypeCategoryNameFilterFieldName : string,
  fileSizeFilterFieldName : string,
  explicitTagTypeAssociation : string,
  explicitTagAssociation : string,

  groups_url:string;
  folders_flat_url:string;

  folder_url:string;
  folders_url:string;
  owners_url:string;
  labels_url:string;
  share_permissions_allow_read_url:string;
  regulations_url:string;
  patterns_url:string;
  metadatas_url:string;
  patterns_multi_url:string;

  contents_url: string;
  extensions_url:string;
  file_type_categories_url:string;
  file_sizes_url:string;


  categories_account_url:string;

  groups_faceted_search_url:string;
  folder_parent_url:string;
  file_details_url:string;
  is_file_content_authorized:string;
  file_highlights_url:string;
  file_highlights_list_url:string;
  file_highlights_patterns_url:string;
  file_highlights_entities_url:string;
  group_details_url:string;
  group_recover_from_freeze_url:string;


  group_details_update_url:string;
  file_download_url:string;
  file_download_reingested_url:string;
  file_detach_group_url:string;
  file_attach_auto_group_url:string;
  files_attach_group_url:string;
  group_create_joinedGroup_url:string;
  group_delete_joinedGroup_url:string;
  group_remove_from_joinedGroup_url:string;
  group_add_to_joinedGroup_url:string;
  group_list_url:string;
  subGroup_cluster_list_url:string;
  subGroups_set_url:string;
  subGroups_applyRules_url:string;
  subGroups_deleteRules_url:string;

  acl_writes_url:string;


  acl_reads_url:string;

  files_count_url:string;
  files_url:string;
  tag_types_url:string;
  tags_url:string;

  group_root_folders_url:string;
  server_folders_url:string;
  media_type_server_folders_url:string;
  media_type_server_folders_by_datacenter_url:string;
  subtrees_url:string;

  fileTagTypes:string;
  allFileTags_url:string;
  associateTags_url:string;
  fileTagsOfType:string;
  updateFileTag:string;
  deleteFileTag:string;
  fileTagType_add_url:string;
  fileTagType_update_url:string;
  fileTagType_delete_url:string;
  addRemoveFileTagUrl:string;
  addRemoveGroupTagUrl:string;
  addRemoveFolderTagUrl:string;
  addRemoveFilesTagUrl:string;
  addRemoveDocTypeTagUrl:string;
  addNewFileTag:string;

  doc_types_url:string;
  doc_types_flat_url:string;
  docType_list_url:string;
  docType_add_url:string;
  docType_update_url:string;
  docType_delete_url:string;
  docType_move_url:string;
  addRemove_Group_DocType_Url:string;
  addRemove_Folder_DocType_Url:string;
  addRemove_File_DocType_Url:string;

  functionalRole_list_url:string;
  functionalRole_by_ids_url:string;
  functionalRole_ldap_list_url:string;
  functionalRole_add_url:string;
  functionalRole_edit_url:string;
  functionalRole_delete_url:string;
  functionalRole_systemRole_list_url:string;
  functionalRole_assigned_users_url:string;
  addRemove_Group_FunctionalRole_Url:string;
  addRemove_Folder_FunctionalRole_Url:string;
  addRemove_File_FunctionalRole_Url:string;

  dateRanges_partitions_url:string;
  dateRange_specific_partition_url:string;
  dateRange_add_url:string;
  dateRange_update_url:string;
  dateRange_delete_url:string;

  searchPattern_list_url:string;
  searchPattern_items_import_url:string;
  searchPattern_cancel_import_url:string;
  searchPattern_items_upload_url:string;
  searchPattern_items_upload_status_update_url:string;
  searchPattern_add_url:string;
  searchPattern_edit_url:string;
  searchPattern_edit_active_url:string;
  searchPattern_edit_not_active_url:string;
  searchPattern_delete_url:string;

  user_list_url:string;
  user_add_url:string;
  user_edit_url:string;
  user_delete_url:string;
  user_current_auth_url:string;
  user_demo_info_url:string;
  user_current_url:string;
  user_current_with_tokens_url:string;
  user_update_password_url:string;
  user_unlock_url:string;
  user_current_update_password_url:string;
  user_assign_bizRole_url:string;
  user_remove_assigned_bizRole_url:string;
  user_systemRole_list_url:string;

  templateRole_edit_url:string;
  templateRole_add_url:string;
  templateRole_delete_url:string;
  templateRole_list_url:string;
  templateRole_systemRole_list_url:string;
  templateRole_summary_list_url:string;

  bizRole_ldap_list_url:string;
  bizRole_assigned_users_url:string;
  bizRole_list_url:string;
  bizRole_summary_list_url:string;
  bizRole_add_url:string;
  bizRole_edit_url:string;
  bizRole_delete_url:string;

  bizList_list_url:string;
  bizList_summary_list_url:string;
  bizList_consumer_templates_url:string;
  bizList_list_types_url:string;
  bizList_add_url:string;
  bizList_update_url:string;
  bizList_delete_url:string;
  bizList_items_list_url:string;
  bizList_items_add_url:string;
  bizList_items_edit_url:string;
  bizList_items_delete_url:string;
  bizList_items_upload_status_update_url:string;
  bizList_cancel_import_url:string;
  bizList_items_upload_url:string;
  bizList_items_update_status_url:string;
  bizList_item_update_status_url:string;
  bizList_items_import_url:string;
  bizList_items_create_from_folder_names_url:string;
  addRemove_file_bizList_associate_url:string;
  addRemove_folder_bizList_associate_url:string;
  addRemove_group_bizList_associate_url:string;
  bizList_auto_classify_groups_url:string;
  bizList_auto_classify_groups_runStatus_url:string;
  reingestRootFolder_url:string;
  updateScanstrategy_url:string;
  getScanStrategyJobProcessingMode_url:string;

  dashboard_scan_url:string;
  dashboard_groupSize_histogram_url:string;
  dashboard_fileType_histogram_url:string;
  dashboard_fileType_histogram_role_url:string;
  dashboard_fileExtension_histogram_url:string;
  dashboard_file_access_histogram_url:string;
  dashboard_file_access_histogram_role_url:string;

  dashboard_scan_criteria:string;
  dashboard_scan_configuration:string;
  dashboard_scan_statistics:string;

  dashboard_user_documents_statistics:string;
  dashboard_admin_documents_statistics:string;

  bizlist_extract_start:string;
  //file_crawling_url:string;
  //file_crawling_stop:string;
  //file_crawling_stop_run:string;
  //file_crawling_continue_run:string;
  //file_crawling_state:string;
  //file_crawling_files_state:string;
  file_crawling_ingest_errors_last_url:string;
  file_crawling_ingest_errors_url:string;
  file_crawling_ingest_errors_for_rootfolder_url:string;
  file_crawling_scan_errors_url:string;
  file_crawling_scan_errors_for_rootfolder_url:string;
  file_crawling_changelog_url:string;
  file_crawling_changelog_for_rootfolder_url:string;
  //file_crawling_details_url:string;
  //file_crawling_details_for_rootfolder_url:string;
  //file_crawling_details_for_schedule_group_url:string;
  //file_crawling_phases_for_crawl_run_details:string;
  startScanRootFolder_url;
  startScanRootFolders_url;
  pauseAllRuns_url;
  pauseRuns_url;
  pauseJobs_url;
  stopRuns_url;
  resumeRuns_url;
  resumeJobs_url;
  forceRerunJobEnqueuedTasks_url;
  forceRerunRunEnqueuedTasks_url;
  resumeAllRun_url;
  resumeRunsForRootFolder_url;
  extractBizList_url;
  runJobs_list_url;
  runs_list_url;
  geActiveRunsDetails_url;
  getRootFoldersAggregatedSummaryInfo_url;
  get_system_settings;
  get_system_settings_by_name;

  scheduleGroup_start_crawl_url:string;
  scheduleGroup_stop_crawl_url:string;

  server_logs:string;
  filter_url:string;
  chart_url:string;
  user_view_url:string;
  user_views_url:string;
  user_view_with_filter_url:string;
  user_view_with_filter_delete_url:string;
  user_view_data_upload_url:string;
  user_view_data_validate_upload_url:string;
  trendChart_url:string;
  trendChart_delete_url:string;
  trendChart_data_url:string;

  applyTagsToSolr:string;
  startResyncSolr:string;
  alternativeLabelingSet:string;
  clearRunErrors:string;
  clearFatalRunErrors:string;
  getRunErrors:string;
  getRunErrorsFatal:string;
  groups_naming_debug_url:string;
  immediate_action_start_url:string;
  immediate_action_start_for_files_url:string;
  immediate_action_file_result_url:string;
  immediate_action_stop_url:string;
  immediate_action_progress_url:string;
  immediate_action_supported:string;
  immediate_action_scripts_files_url:string;


  root_folders_url:string;
  root_folder_url:string;
  delete_root_folder_force_url:string;
  root_folder_upload_url:string;
  root_folder_validate_upload_url:string;
  root_folder_excluded_rule_upload_url:string;
  root_folder_excluded_rule_validate_upload_url:string;
  root_folders_summary_url:string;
  root_folder_schedules_summary_url:string;
  root_folder_excluded_rule_url:string;
  //rerun_root_folder_url:string;
  //rerun_root_folders_url:string;
  addRemove_root_folder_scheduleGroup_url:string;
  editRemove_scheduleGroup_url:string;
  scheduleGroups_validate_upload_url:string;
  scheduleGroups_upload_url:string;
  scheduleGroups_url:string;
  scheduleGroup_url:string;
  scheduleGroups_for_root_folder_url:string;
  root_folders_for_scheduleGroup_url:string;
  mediaType_connections_url:string;
  microsoft_connections_url:string;
  add_microsoft_connection_url:string;
  editRemove_microsoft_connection_url:string;
  editRemove_connection_url:string;
  test_media_connection_url:string;
  test_media_connection_to_datacenter_url:string;
  test_media_connection_saved_to_datacenter_url:string;
  test_box_connection_saved_to_datacenter_url:string;
  add_media_connection_url:string;
  editRemove_media_connection_url:string;
  media_connections_by_type_url:string;
  connection_validate_upload_url:string;
  connection_upload_url:string;
  test_microsoft_connection_url:string;
  rootfolder_scan_fileTypes:string;
  rootfolder_map_fileTypes:string;
  rootfolder_map_container_fileTypes:string;
  server_components_status_url:string;
  server_components_list_url:string;
  server_components_list_with_status_url: string;
  server_components_data_center_list_url:string;
  server_components_history_url:string;
  addServerComponent_url:string;
  editServerComponent_url:string;
  customerDataCenter_url:string;
  customerDataCenter_list_url:string;
  customerDataCenter_summary_info_url:string;
  audit_url:string;

  customerDataCenters_url:string;
  editRemove_customerDataCenter_url:string;
  add_customerDataCenter_url:string;

  license_key_import_string_url:string;
  license_key_import_file_url:string;
  license_status_url:string;
  apply_label_url:string;
  apply_label_by_id_url:string;
  get_labels_url:string,
  login_expired_url:string;

  mail_notification_settings_url:string;
  mail_health_notification_now_url:string;
  health_notification_html_url:string;
  mail_notification_settings_test_connection_url:string;

  ldap_connection_url:string;
  ldap_connections_url:string;
  editRemove_ldap_connections_url:string;
  ldap_connection_upload_url:string;
  ldap_connection_validate_upload_url:string;

  ldap_settings_url:string;
  ldap_settings_test_connection_url:string;

  department_url:string;
  departments_url:string;
  departments_list_url:string;
  departments_flat_url:string;

  matter_url:string;
  matters_url:string;
  matters_list_url:string;
  matters_flat_url:string;

  minimum_items_to_open_export_popup: number;

  ////////////////////////////////////////////////////////////
  //////////////// DISPLAY SETTINGS//////////////////////////
  ////////////////////////////////////////////////////////////

  showGoogleLikeSearchInDiscover:boolean;
  dateTimeFormat:string;
  exportNameDateTimeFormat:string;
  dateFormat:string
  inputDateFormat:string;
  dateNoYearFormat:string;
  dateNoDayFormat:string;
  timeFormat:string;
  timeDateFormat:string;
  yearFormat:string;
  monthFormat:string;

  max_recently_used_biz_lists_association:string;
  max_recently_used_docTypes:string;
  max_recently_used_departments:string;
  max_recently_used_functionalRoles:string;
  max_recently_used_markedItems:string;
  max_tags_in_list:string;
  max_recently_used_tags:string;
  max_tags_buttons_in_toolbar:string;
  max_docTypes_colors:string;
  max_tags_colors:string;
  max_departments_colors:string;
  max_data_records_per_request:number;
  max_data_records_in_dialog_to_scroll:number;
  displayed_items_when_more_then_max:number;
  chart_folder_basename_maxLength:number;
  chart_user_basename_maxLength:number;
  chart_folder_path_maxLength:number;
  MAX_FOLDERS_IN_TREE:number;

  solr_node_disk_space_threshold:number;

  ///ICONS:
  'icon_owner':string;
  'icon_group':string;
  'icon_folder':string;
  'icon_excludedfolder':string;
  'icon_extractEntities':string;
  'icon_extension':string;
  'icon_fileTypeCategory':string;
  'icon_rootfolder':string;
  'icon_mail':string;
  'icon_domain':string;
  'icon_recipient_address':string;
  'icon_sender_address':string;
  'icon_aclRead':string;
  'icon_aclWrite':string;
  'icon_docType':string;
  'icon_logLevels':string;
  'icon_functionalRole':string;
  'icon_templateRole':string;
  'icon_tagType':string;
  'icon_regulation':string;
  'icon_dateFilter': string;
  'icon_fileContent':string;
  'icon_department': string;
  'icon_matter': string;
  'icon_tag':string;
  'icon_file_sizes':string;
  'icon_label':string;
  'icon_share_permission_allow_read':string;
  'icon_metadata':string;
  'icon_pattern':string;
  'icon_pattern_multi':string;
  'icon_pattern_filter':string;
  'icon_tagTypeMismatch':string;
  'icon_bizListItemClients':string;
  'icon_bizListItemExtractionRule':string;
  'icon_bizListItemClientExtraction':string;
  'icon_bizListItemConsumerExtraction':string;
  'icon_fileCreatedDate':string;
  'icon_fileModifiedDate':string;
  'icon_fileAccessedDate':string;
  'icon_scheduleGroup':string;
  'icon_user':string;
  'icon_ldap_user':string;
  'icon_bizRole':string;
  'icon_sysRole':string;
  'icon_searchPattern':string;
  'icon_ldapConnection':string;
  'icon_mediaTypeConnection':string;
  'icon_markedItem':string;
  'icon_unmarkedItem':string;
  'icon_GDPR_Erasure':string;

  user_saved_data_url:string;
  pooling_interval_idle:number;
  pooling_interval_running:number;
  polling_server_components_interval_idle:number;
  association_warning_threshold:number;

  ///////////////////////////////////////////////////////////
  //////////////   POLICY ///////////////////////////////////
  ///////////////////////////////////////////////////////////

  policies_list_url:string;
  policies_url:string;
  objects_by_type_url:string;
  objects_list_url:string;
  objects_url:string;
  policy_add_url:string;
  policy_update_url:string;
  policy_delete_url:string;
  object_add_url:string;
  object_update_url:string;
  object_delete_url:string;

  ///////////////////////////////////////////////////////////
  //////////////  GDPR ///////////////////////////////////
  ///////////////////////////////////////////////////////////

  GDPR_erasure_request_list:string;
  GDPR_erasure_request:string;
  GDPR_erasure_add_url:string;
  GDPR_erasure_update_url:string;
  GDPR_erasure_delete_url:string;

  GDPR_workflow_tagType_id:string;
  GDPR_recommended_action_tagType_id:string;
  GDPR_confidence_level_tagType_id:string;
  GDPR_action_tagType_id:string;
  GDPR_workflow_state_tagType_id:string;
  GDPR_file_relevancy_tagType_id:string;
  GDPR_action_tag_id_icon:string;
  GDPR_confidence_tag_id_icon:string;
  GDPR_filter_panel_tagType_Ids:string;
  GDPR_file_relevant_tag_id:string;
  GDPR_file_irrelevant_tag_id:string;
  'handle-stuck-in-pause-runs.stop-threshold-days' : string;

}

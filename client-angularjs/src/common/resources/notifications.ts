
///<reference path='..\all.d.ts'/>
'use strict';

interface INotificationSettingsResource {
  getMailNotificationSettings(successFunction:(settings:AppSettings.DtoMailNotificationConfiguration) => void, errorFunction): void;
  updateMailNotificationSettings(settings:AppSettings.DtoMailNotificationConfiguration, successFunction:(settingsUpdated:AppSettings.DtoMailNotificationConfiguration) => void, errorFunction): void;
  testMailConnection (settings:AppSettings.DtoMailNotificationConfiguration,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
  testMailConnectionWithSavedConnection(settings:AppSettings.DtoMailNotificationConfiguration,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
}

interface IHealthNotificationResource {
  sendMailNotificationNow (successFunction:() => void, errorFunction): void;
  getHtmlNotification (successFunction:(html:string) => void, errorFunction): void;
 }
angular.module('resources.notifications', [
  ])

  .factory( 'notificationSettingsResource', function( $resource,configuration:IConfig,propertiesUtils ) {
    var res = $resource(configuration.mail_notification_settings_url, '', {
      getMailNotificationSettings: {
        method: 'GET',
        isArray: false },
      updateMailNotificationSettings: {
        method: 'POST',
        isArray: false },
      testMailConnectionWithSavedConnection: {
         url:configuration.mail_notification_settings_test_connection_url,
         method: 'POST',
         isArray: false },
      testMailConnection: {
        url:configuration.mail_notification_settings_test_connection_url,
        method: 'POST',
        isArray: false },

    });
    var factory = <INotificationSettingsResource>{
      getMailNotificationSettings: function (successFunction:(settings:AppSettings.DtoMailNotificationConfiguration) => void, errorFunction) {
        return res.getMailNotificationSettings(null,
          function (settings:AppSettings.DtoMailNotificationConfiguration) {
             successFunction(settings);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      updateMailNotificationSettings: function (settings:AppSettings.DtoMailNotificationConfiguration, successFunction:(settingsUpdated:AppSettings.DtoMailNotificationConfiguration) => void, errorFunction) {
        return res.updateMailNotificationSettings(null,settings,
          function (settingsUpdated:AppSettings.DtoMailNotificationConfiguration) {
            if(settingsUpdated) {

              successFunction(settingsUpdated);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      testMailConnectionWithSavedConnection: function (settings:AppSettings.DtoMailNotificationConfiguration, successFunction:(success:boolean,reason:string) => void, errorFunction) {
        return res.testMailConnectionWithSavedConnection(null,settings,
          function (result:DTORequestBase) {
            if(result) {
              successFunction(result.result,result.reason);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      testMailConnection: function (settings:AppSettings.DtoMailNotificationConfiguration, successFunction:(success:boolean,reason:string) => void, errorFunction) {
        return res.testMailConnection(null,settings,
          function (result:DTORequestBase) {
            if(result) {
              successFunction(result.result,result.reason);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },


    }
    return factory;
  })

  .factory( 'healthNotificationResource', function( $resource,configuration:IConfig,propertiesUtils ) {
    var res = $resource(configuration.mail_notification_settings_url, '', {
      sendMailNotificationNow: {
        url:configuration.mail_health_notification_now_url,
        method: 'GET',
        isArray: false },
      getHtmlNotification: {
        url:configuration.health_notification_html_url,
        method: 'GET',
        transformResponse: function (response) { //when result is string, resource convert it to char array. To to wrap result with object
           return {html: response};
        },
        isArray: false },

    });
    var factory = <IHealthNotificationResource>{
      sendMailNotificationNow: function ( successFunction:() => void, errorFunction) {
        return res.sendMailNotificationNow(null,
          function () {
              successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getHtmlNotification: function (successFunction:(html:string) => void, errorFunction) {
        return res.getHtmlNotification(
          function (data) {
            if(data) {
              successFunction(data.html);
            }
            else {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
    }
    return factory;
  });

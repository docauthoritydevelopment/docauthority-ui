
///<reference path='..\all.d.ts'/>
'use strict';

interface ILicenseResource {
  getLicenseStatus(successFunction:(lic:License.DtoDocAuthorityLicense) => void, errorFunction): void;
  importLicenseFromFile(fileData,applyLic:boolean,successFunction:(importResult:License.DtoLicenseImportResult) => void, errorFunction): void;
  importLicenseString(licenseKey:string,applyLic:boolean,successFunction:(importResult:License.DtoLicenseImportResult) => void , errorFunction): void;
  isLoginExpired(successFunction:(expired:boolean) => void, errorFunction): void;

}
angular.module('resources.license', [
    ])

    .factory( 'licenseResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.license_status_url, '', {
        getLicenseStatus: {
          method: 'GET',
          url:configuration.license_status_url,
          isArray: false },
        importLicenseFromFile: {
          method: 'POST',
          url:configuration.license_key_import_file_url,
          headers: {'content-type':undefined},//
          //   responseType: "arraybuffer",
          transformRequest:angular.identity,
          isArray: false },
        importLicenseString: {
          method: 'POST',
          url: configuration.license_key_import_string_url,
          isArray: false },
        isLoginExpired: {
          method: 'GET',
          url: configuration.login_expired_url,
         isArray: false },


      });
      var factory = <ILicenseResource>{
        getLicenseStatus: function (successFunction:(lic:License.DtoDocAuthorityLicense) => void, errorFunction) {
          return res.getLicenseStatus(null,
              function (lic:License.DtoDocAuthorityLicense) {
                if(lic) {
                  successFunction(lic);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to getLicenseStatus " , error);
              }
          )
        },
        importLicenseFromFile: function (fileData,applyLic:boolean,successFunction:(importResult:License.DtoLicenseImportResult) => void, errorFunction) {
          return res.importLicenseFromFile({save:applyLic},fileData,
              function (importResult:License.DtoLicenseImportResult) {
                successFunction(importResult);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        importLicenseString: function (licenseKey:string,applyLic:boolean,successFunction:(importResult:License.DtoLicenseImportResult) => void, errorFunction) {
          return res.importLicenseString({license:licenseKey,save:applyLic},null,
              function (importResult:License.DtoLicenseImportResult) {
                successFunction(importResult);
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to importLicenseString" + error);
              }
          )
        },
        isLoginExpired: function (successFunction:(expired:boolean) => void, errorFunction) {
          return res.isLoginExpired(null,
              function (expired:boolean) {
                successFunction(expired);
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to isLoginExpired" + error);
              }
          )
        },

      }
      return factory;
    });




///<reference path='..\all.d.ts'/>
'use strict';

interface IDocTypeResource
{
  getDocTypes(fileTagId,itemId,successFunction:(docTypes:Entities.DTODocType[]) => void):any;

  addDocType(fileTagId,itemId):any;
  removeDocType(fileTagId,itemId):any;
}
interface IDocTypesResource {
  getDocTypes(successFunction:(docTypes:Entities.DTODocType[]) => void, errorFunction): void;
  getAssignedDocTypes(filter,successFunction:(docTypes:Entities.DTOAggregationCountItem<Entities.DTODocType>[],totalAggregatedCount?:number) => void, errorFunction): void;
  addNewDocType(docType:Entities.DTODocType,successFunction:(addedDocType) => void , errorFunction): void;
  updateDocType(docType:Entities.DTODocType,successFunction:() => void, errorFunction): void;
  moveDocType(sourceDocType:Entities.DTODocType,destinationDocType:Entities.DTODocType,successFunction:() => void, errorFunction): void;
  deleteDocType(docTypeID:number,successFunction:() => void, errorFunction): void;

}
angular.module('resources.docTypes', [
    ])

    .factory( 'docTypesResource', function( $resource,configuration:IConfig,propertiesUtils ) {
      var res = $resource(configuration.docType_list_url, '', {
        getDocTypes: {
          method: 'GET',
          url:configuration.docType_list_url,
          isArray: false },
        getAssignedDocTypes: {
          method: 'GET',
          url:configuration.doc_types_url,
          isArray: false },
        addNewDocType: {
          method: 'PUT',
          url: configuration.docType_add_url,
          params: {docTypeParentId: '@docTypeParentId'},
          isArray: false },
        updateDocType: {
          method: 'POST',
          url: configuration.docType_update_url,
          params: {id: '@id'},
          isArray: false },
        moveDocType: {
          method: 'POST',
          url: configuration.docType_move_url,
          params: {id: '@id',newParentId:'@newParentId'},
          isArray: false },
        deleteDocType: {
          method: 'DELETE',
          url: configuration.docType_delete_url+'?recursive=true',
          params: {id: '@id'},
          isArray: false },

      });
      var factory = <IDocTypesResource>{
        getDocTypes: function (successFunction:(docTypes:Entities.DTODocType[]) => void, errorFunction) {
          return res.getDocTypes(null,
              function (docTypes:Entities.DTODocType[]) {
                if(docTypes&& (<any>docTypes).content) {
                  (<any>docTypes).content.sort(propertiesUtils.sortAlphbeticFun('name'));
                    successFunction((<any>docTypes).content);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getAssignedDocTypes: function (filter,successFunction:(docTypes:Entities.DTOAggregationCountItem<Entities.DTODocType>[],totalAggregatedCount?:number) => void, errorFunction) {
          return res.getAssignedDocTypes(filter?{filter:JSON.stringify(filter)}:null,
              function (docTypes:Entities.DTOQueryEntityResult<Entities.DTOAggregationCountItem<Entities.DTODocType>>) {
                if(docTypes&& docTypes.content) {
                  (<any>docTypes).content.sort((t1, t2)=> {
                    if (t1.item.name > t2.item.name) {
                      return 1;
                    }
                    if (t1.item.name < t2.item.name) {
                      return -1;
                    }
                    return 0;
                  });
                  successFunction(docTypes.content,docTypes.totalAggregatedCount);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        addNewDocType: function (docType:Entities.DTODocType,successFunction:(addedDocType) => void, errorFunction) {
          return res.addNewDocType(null,JSON.stringify(docType),
              function (addedDocType) {
                successFunction(addedDocType);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        updateDocType: function (docType:Entities.DTODocType,successFunction:() => void, errorFunction) {
          return res.updateDocType({id:docType.id},JSON.stringify(docType),
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        moveDocType: function (sourceDocType:Entities.DTODocType,destinationDocType:Entities.DTODocType,successFunction:() => void, errorFunction) {
          return res.moveDocType({id:sourceDocType.id,newParentId:destinationDocType?destinationDocType.id:null},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        deleteDocType: function (docTypeId:number,successFunction:() => void, errorFunction) {
          return res.deleteDocType({id:docTypeId},null,
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
      }
      return factory;
    }).factory( 'docTypeResource', function( $resource,configuration:IConfig) {
      var group = $resource(configuration.addRemove_Group_DocType_Url, {docTypeId:'@docTypeId', groupId: '@groupId' }, {
        getDocTypes: { method: 'GET' },
        addDocType: { method: 'PUT' },
        removeDocType: { method: 'DELETE' },

      });
      var folder = $resource(configuration.addRemove_Folder_DocType_Url, {docTypeId:'@docTypeId', folderId: '@folderId' }, {
        getDocTypes: { method: 'GET' },
        addDocType: { method: 'PUT' },
        removeDocType: { method: 'DELETE' },

      });
      var file = $resource(configuration.addRemove_File_DocType_Url, {docTypeId:'@docTypeId', fileId: '@fileId' }, {
        getDocTypes: { method: 'GET' },
        addDocType: { method: 'PUT' },
        removeDocType: { method: 'DELETE' },

      });
      return function(entityDisplayType: EntityDisplayTypes):IDocTypeResource
      {
       if(entityDisplayType==EntityDisplayTypes.groups)
        {
          var result:IDocTypeResource =
          {
            getDocTypes:function(docTypeId,itemId)
            {
              return group.getDocTypes({docTypeId:docTypeId,groupId:itemId});
            },
            addDocType:function(docTypeId,itemId)
            {
              return group.addDocType({docTypeId:docTypeId,groupId:itemId});
            },
            removeDocType:function(docTypeId,itemId)
            {
              return group.removeDocType({docTypeId:docTypeId,groupId:itemId});
            }
          }
          return result;
        }
        if(entityDisplayType==EntityDisplayTypes.folders|| entityDisplayType==EntityDisplayTypes.folders_flat)
        {
          var result:IDocTypeResource =
          {
            getDocTypes:function(docTypeId,itemId)
            {
              return folder.getDocTypes({docTypeId:docTypeId,folderId:itemId});
            },
            addDocType:function(docTypeId,itemId)
            {
              return folder.addDocType({docTypeId:docTypeId,folderId:itemId});
            },
            removeDocType:function(docTypeId,itemId)
            {
              return folder.removeDocType({docTypeId:docTypeId,folderId:itemId});
            }
          }
          return result;
        }
        if(entityDisplayType==EntityDisplayTypes.files)
        {
          var result:IDocTypeResource =
          {
            getDocTypes:function(docTypeId,itemId)
            {
              return file.getDocTypes({docTypeId:docTypeId,fileId:itemId});
            },
            addDocType:function(docTypeId,itemId)
            {
              return file.addDocType({docTypeId:docTypeId,fileId:itemId});
            },
            removeDocType:function(docTypeId,itemId)
            {
              return file.removeDocType({docTypeId:docTypeId,fileId:itemId});
            }
          }
          return result;
        }
        return null;
      }


});



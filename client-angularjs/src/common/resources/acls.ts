
///<reference path='..\all.d.ts'/>
'use strict';

interface IAclsResource {

  getAclReads(maxElementCount:number, baseFilterValue,baseFilterField,filterKendoFormat,successFunction: (acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[],totalElements:number) => void , errorFunction): void;
  getAclWrites(maxElementCount:number,baseFilterValue,baseFilterField,filterKendoFormat,successFunction: (acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[],totalElements:number) => void , errorFunction): void;

}

angular.module('resources.acls', [
    ])

    .factory( 'aclsResource', function( $resource ,configuration:IConfig) {
      var res = $resource(configuration.acl_reads_url, null, {
        getAclReads:  {
          method: 'GET',
          url:configuration.acl_reads_url,
          params: {filter: '@filter', baseFilterValue: '@baseFilterValue', baseFilterField: '@baseFilterField'},
          isArray: false
        },
        getAclWrites:  {
          method: 'GET',
          url:configuration.acl_writes_url,
          params: {filter: '@filter', baseFilterValue: '@baseFilterValue', baseFilterField: '@baseFilterField'},
          isArray: false
        },

      });

      var factory = <IAclsResource>{

        getAclReads: function (maxElementCount:number,baseFilterValue,baseFilterField,filterKendoFormat,successFunction:(acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[],totalElements:number) => void, errorFunction) {
          return res.getAclReads({filter:filterKendoFormat,baseFilterValue:baseFilterValue,baseFilterField:baseFilterField,take:maxElementCount},
              function (f:Entities.DTOQueryEntityResult<Entities.DTOAggregationCountItem<Entities.DTOFileUser>>) {
                if(f) {
                  successFunction(f.content,f.numberOfElements);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getAclWrites: function (maxElementCount:number,baseFilterValue,baseFilterField,filterKendoFormat,successFunction:(acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[],totalElements:number) => void, errorFunction) {
          return res.getAclWrites({filter:filterKendoFormat,baseFilterValue:baseFilterValue,baseFilterField:baseFilterField,take:maxElementCount},
              function (f:Entities.DTOQueryEntityResult<Entities.DTOAggregationCountItem<Entities.DTOFileUser>>) {
                if(f) {
                  successFunction(f.content,f.numberOfElements);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },

      };
      return factory;
    })

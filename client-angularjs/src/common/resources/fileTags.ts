
///<reference path='..\all.d.ts'/>
'use strict';

interface IfileTagResource
{
  getTags(fileTagId,itemId?):any;
  addTag(fileTagId,itemId?, successFunction?, errorFunction? ):any;
  removeTag(fileTagId,itemId?,successFunction?,errorFunction?):any;
}

interface IFileTagsResource {
  getTagTypes(successFunction: (tags:Entities.DTOFileTagType[]) => void , errorFunction): void;
  getTagListByTagType(tagTypeId,successFunction: (tags:Entities.DTOFileTag[]) => void , errorFunction): void;
  setDocTypeTags(docTypesTags,successFunction: (res) => void , errorFunction): void;
  getAllTags(successFunction: (fileTagLists:Entities.DTOFileTagTypeAndTags[]) => void , errorFunction): void;
  addNewTag(tagTypeId:number,tag:Entities.DTOFileTag,successFunction: (newTag:Entities.DTOFileTag) => void , errorFunction): void;
  addNewTagType(tagType:Entities.DTOFileTagType,successFunction: (newTagType:Entities.DTOFileTagType) => void , errorFunction): void;
  updateTag(tag:Entities.DTOFileTag,successFunction: () => void , errorFunction): void;
  updateTagType(tagType:Entities.DTOFileTagType,successFunction: (updatedtagType:Entities.DTOFileTagType) => void , errorFunction): void;
  deleteTag(tagId:number,tagTypeId:number,successFunction: () => void , errorFunction): void;
  deleteTagType(tagTypeId,successFunction: () => void , errorFunction): void;
  getAssignedFilesForTag(tag:Entities.DTOFileTag,successFunction: (tagDetails:Entities.DTOAggregationCountItem<Entities.DTOFileTag>[],totalAggregatedCount) => void , errorFunction): void;
  getAssignedFilesForTagType(tag:Entities.DTOFileTagType,successFunction: (tagTypeDetails:Entities.DTOAggregationCountItem<Entities.DTOFileTagType>[],totalAggregatedCount) => void , errorFunction): void;
}
angular.module('resources.fileTags',[])
  .factory( 'fileTagsResource', function( $resource,$cacheFactory,configuration:IConfig,propertiesUtils,filterFactory:ui.IFilterFactory ) {

    var res = $resource(configuration.fileTagTypes, '', {
      getTagTypes: { method: 'GET', isArray: true },
      getTagListByTagType: {
        method: 'GET',
        url:configuration.fileTagsOfType,
        params: {typeId: '@typeId'},
        isArray: true },
      setDocTypeTags: {
        method: 'PUT',
        url: configuration.associateTags_url,
        isArray: true},
      getAllTags: {
        method: 'GET',
        url:configuration.allFileTags_url,
        isArray: true },
      addNewTag: {
        method: 'PUT',
        url: configuration.addNewFileTag,
        params: {fileTagTypeId: '@fileTagTypeId',name:'@name',description:'@description',entityId:'@entityId',docTypeId :'@docTypeId'},
        isArray: false },
      updateTag: {
        method: 'POST',
        url: configuration.updateFileTag,
        params: {id: '@id'},
        isArray: false },
      deleteTag: {
        method: 'DELETE',
        url: configuration.deleteFileTag,
        params: {id: '@id'},
        isArray: false },
      addNewTagType: {
        method: 'PUT',
        url: configuration.fileTagType_add_url,
        isArray: false },
      updateTagType: {
        method: 'POST',
        params: {tagTypeId: '@tagTypeId'},
        url: configuration.fileTagType_update_url,
        isArray: false },
      deleteTagType: {
        method: 'DELETE',
        url: configuration.fileTagType_delete_url,
        params: {id: '@id'},
        isArray: false },
      getAssignedFilesForTag: {
        method: 'GET',
        url:configuration.tags_url,
        params: {id: '@id'},
        isArray: false },
      getAssignedFilesForTagType: {
        method: 'GET',
        url:configuration.tag_types_url,
        params: {id: '@id'},
        isArray: false },

    });
    var tagsCache = $cacheFactory('tags');
    var invalidateCache = function(tagTypeId) //lazy cache implementation
    {
      tagsCache.remove(tagTypeId);
    }
    var factory = <IFileTagsResource>{
      getTagTypes: function (successFunction:(tags:Entities.DTOFileTagType[]) => void, errorFunction) {
        return res.getTagTypes(
          function (tags:Entities.DTOFileTagType[]) {
            if(tags) {
              tags.sort( propertiesUtils.sortAlphbeticFun('name'));
              successFunction(tags);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getTagListByTagType: function (tagTypeId,successFunction:(tags:Entities.DTOFileTag[]) => void, errorFunction) {
        if(tagsCache.get(tagTypeId))
        {
          return successFunction(tagsCache.get(tagTypeId));
        }
        return res.getTagListByTagType({typeId:tagTypeId},
          function (tags:Entities.DTOFileTag[]) {
            if(tags) {
              if(tags[0]&& tags[0].type.name.toLowerCase().indexOf('gdpr')==-1) { //DO not sort GDPR tags
                tags.sort(propertiesUtils.sortAlphbeticFun('name'));
              }
              tagsCache.put(tagTypeId,tags);
              successFunction(tags);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      setDocTypeTags:function(docTypesTags, successFunction: (res) => void , errorFunction) {
        return res.setDocTypeTags(docTypesTags,
          function (res) {
            successFunction(res);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getAllTags:function(successFunction: (fileTagLists:Entities.DTOFileTagTypeAndTags[]) => void , errorFunction) {
         return res.getAllTags(null,
            function (fileTagLists:Entities.DTOFileTagTypeAndTags[]) {
              if(fileTagLists) {
                fileTagLists.sort(function (t1:Entities.DTOFileTagTypeAndTags, t2:Entities.DTOFileTagTypeAndTags) {
                    if (t1.fileTagTypeDto.name> t2.fileTagTypeDto.name) {
                      return 1;
                    }
                    if (t1.fileTagTypeDto.name < t2.fileTagTypeDto.name) {
                      return -1;
                    }
                    return 0;
                  });
                tagsCache.removeAll();
                fileTagLists.forEach(t=>
                {
                  var tags = t.fileTags;
                  if(tags[0]&& tags[0].type.name.toLowerCase().indexOf('gdpr')==-1) { //DO not sort GDPR tags
                    tags.sort(propertiesUtils.sortAlphbeticFun('name'));
                  }

                  tagsCache.put(t.fileTagTypeDto.id,tags);
                 }
               )
               successFunction(fileTagLists);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      addNewTag: function (tagTypeId,tag:Entities.DTOFileTag,successFunction:(newTag:Entities.DTOFileTag) => void, errorFunction) {
        return res.addNewTag({fileTagTypeId:tagTypeId,name:tag.name,description:tag.description,},
          function (newTag) {
            invalidateCache(tagTypeId);
               successFunction(newTag);
          }
          , function (response) {
            if(response.status === 409) {
              errorFunction({userMsg:"Tag with this name already exists. Name: "+tag.name});
            }
            else
            {
              errorFunction(response);
            }

          }
        )
      },
      updateTag: function (tag:Entities.DTOFileTag,successFunction:() => void, errorFunction) {
        return res.updateTag({id:tag.id},JSON.stringify(tag),
            function () {
              invalidateCache(tag.type.id);
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      deleteTag: function (tagId:number,tagTypeId:number,successFunction:() => void, errorFunction) {
        return res.deleteTag({id:tagId},null,
            function () {
              invalidateCache(tagTypeId);
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

      addNewTagType: function (tagType:Entities.DTOFileTagType,successFunction:(newTagType:Entities.DTOFileTagType) => void, errorFunction) {
        return res.addNewTagType(null,JSON.stringify(tagType),
            function (newTagType:Entities.DTOFileTagType) {
              successFunction(newTagType);
            }
            , function (response) {
            if (response.status === 400 && response.data.type == 'ITEM_ALREADY_EXISTS') {
              errorFunction({userMsg: "Tag type with this name already exists. Name: " + tagType.name});
            }
            else {
              errorFunction(response);
              console.log("failed to addNewTagType" + response);
            }

          }
        )
      },
      updateTagType: function (tagType:Entities.DTOFileTagType,successFunction:(updateTagType:Entities.DTOFileTagType) => void, errorFunction) {
        return res.updateTagType({tagTypeId:tagType.id},tagType,
            function (updateTagType:Entities.DTOFileTagType) {
              invalidateCache(updateTagType.id);
              successFunction(updateTagType);
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to update tag type " + error);
            }
        )
      },
      deleteTagType: function (tagTypeId:number,successFunction:() => void, errorFunction) {
        return res.deleteTagType({id:tagTypeId},null,
            function () {
              invalidateCache(tagTypeId);
              successFunction();
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to delete tag type" + error);
            }
        )
      },

      getAssignedFilesForTag(tag:Entities.DTOFileTag,successFunction: (tagDetails:Entities.DTOAggregationCountItem<Entities.DTOFileTag>[],totalAggregatedCount) => void , errorFunction) {
        var tagFilter = filterFactory.createFilterByID(EntityDisplayTypes.tags)(tag.id,tag.name).toKendoFilter();
        return res.getAssignedFilesForTag({id:tag.id,filter:tagFilter},null,
            function (tagDetails:Entities.DTOQueryEntityResult<Entities.DTOAggregationCountItem<Entities.DTOFileTag>>) {
              successFunction(tagDetails.content,tagDetails.totalAggregatedCount);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      getAssignedFilesForTagType(tagType:Entities.DTOFileTagType,successFunction: (tagTypeDetails:Entities.DTOAggregationCountItem<Entities.DTOFileTagType>[],totalAggregatedCount) => void , errorFunction) {
        var tagTypeFilter = filterFactory.createFilterByID(EntityDisplayTypes.tag_types)(tagType.id,tagType.name).toKendoFilter();
        return res.getAssignedFilesForTagType({id:tagType.id,filter:tagTypeFilter},null,
            function (tagTypeDetails:Entities.DTOQueryEntityResult<Entities.DTOAggregationCountItem<Entities.DTOFileTagType>>) {
              successFunction(tagTypeDetails.content,tagTypeDetails.totalAggregatedCount);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
    }
    return factory;
  })

  .factory( 'fileTagResource', function( $resource,configuration:IConfig) {
    var file = $resource(configuration.addRemoveFileTagUrl, {fileTagId:'@fileTagId', fileId: '@fileId' }, {
      getTags: { method: 'GET' },
      addTag: { method: 'PUT' },
      removeTag: { method: 'DELETE' }
    });
    var group = $resource(configuration.addRemoveGroupTagUrl, {fileTagId:'@fileTagId', groupId: '@groupId' }, {
      getTags: { method: 'GET' },
      addTag: { method: 'PUT' },
      removeTag: { method: 'DELETE' }
    });
    var folder = $resource(configuration.addRemoveFolderTagUrl, {fileTagId:'@fileTagId', folderId: '@folderId' }, {
      getTags: { method: 'GET' },
      addTag: { method: 'PUT' },
      removeTag: { method: 'DELETE' }
    });
    var files = $resource(configuration.addRemoveFilesTagUrl, {fileTagId: '@fileTagId', filter: '@filter',  baseFilterField: '@baseFilterField', baseFilterValue: '@baseFilterValue'}, {
      getTags: { method: 'GET' },
      addTag: { method: 'PUT' },
      removeTag: { method: 'DELETE' }
    });

    return function(entityDisplayType: EntityDisplayTypes):IfileTagResource {
        if(entityDisplayType==EntityDisplayTypes.files) {
          var result:IfileTagResource = {
            getTags:function(fileTagId,itemId) {
              return file.getTags({fileTagId:fileTagId,fileId:itemId});
            },
            addTag:function(fileTagId,itemId) {
              return file.addTag({fileTagId:fileTagId,fileId:itemId});
            },
            removeTag:function(fileTagId,itemId) {
              return file.removeTag({fileTagId:fileTagId,fileId:itemId});
            }
          };

          return result;
        }
        else if(entityDisplayType==EntityDisplayTypes.groups) {
          var result:IfileTagResource = {
            getTags:function(fileTagId,itemId) {
              return group.getTags({fileTagId:fileTagId,groupId:itemId});
            },
            addTag:function(fileTagId,itemId) {
              return group.addTag({fileTagId:fileTagId,groupId:itemId});
            },
            removeTag:function(fileTagId,itemId) {
              return group.removeTag({fileTagId:fileTagId,groupId:itemId});
            }
          };

          return result;
        }
        else if(entityDisplayType==EntityDisplayTypes.folders || entityDisplayType==EntityDisplayTypes.folders_flat) {
          var result:IfileTagResource = {
            getTags:function(fileTagId,itemId) {
              return folder.getTags({fileTagId:fileTagId,folderId:itemId});
            },
            addTag:function(fileTagId,itemId) {
              return folder.addTag({fileTagId:fileTagId,folderId:itemId});
            },
            removeTag:function(fileTagId,itemId) {
              return folder.removeTag({fileTagId:fileTagId,folderId:itemId});
            }
          };

          return result;
        }
        else {
          var result:IfileTagResource = {
            getTags:function(fileTagId, params) {
              return null;
            },
            addTag:function(fileTagId, params, successFunction: (res) => void) {
              return files.addTag({fileTagId: fileTagId, filter: params.filter, baseFilterField: params.baseFilterField, baseFilterValue: params.baseFilterValue}, function (res) {
                successFunction(res);
              });
            },
            removeTag:function(fileTagId, params, successFunction: (res) => void) {
              return files.removeTag({fileTagId:fileTagId, filter: params.filter, baseFilterField: params.baseFilterField, baseFilterValue: params.baseFilterValue}, function (res) {
                successFunction(res);
              });
            }
          };

          return result;
        }
    }
  });

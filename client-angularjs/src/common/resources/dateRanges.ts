
///<reference path='..\all.d.ts'/>
'use strict';

interface IDateRangeResource {
  getDateRanges(successFunction:(dateRanges:Entities.DTODateRangePartition[]) => void, errorFunction): void;
  getPartition(rangePartitionId:number,successFunction:(datePartition:Entities.DTODateRangePartition) => void, errorFunction): void;
  addNewDateRange(dateRange:Entities.DTODateRangePartition,successFunction:(addedDateRange) => void , errorFunction): void;
  updateDateRange(dateRange:Entities.DTODateRangePartition,successFunction:(datePartition:Entities.DTODateRangePartition) => void, errorFunction): void;
  deleteDateRange(rangePartitionId:number,successFunction:() => void, errorFunction): void;

}
angular.module('resources.dateRange', [
    ])

    .factory( 'dateRangeResource', function( $resource,configuration:IConfig,propertiesUtils ) {
      var res = $resource(configuration.dateRanges_partitions_url, '', {
        getDateRanges: {
          method: 'GET',
          url:configuration.dateRanges_partitions_url,
          isArray: false },
        getPartition: {
          method: 'GET',
          url:configuration.dateRange_specific_partition_url,
          params: {partitionId: '@partitionId'},
          isArray: false },
       addNewDateRange: {
          method: 'PUT',
          url: configuration.dateRange_add_url,
         isArray: false },
        updateDateRange: {
          method: 'POST',
          url: configuration.dateRange_update_url,
          params: {partitionId: '@partitionId'},
          isArray: false },
       deleteDateRange: {
          method: 'DELETE',
          url: configuration.dateRange_delete_url,
          params: {partitionId: '@partitionId'},
          isArray: false },

      });
      var factory = <IDateRangeResource>{
        getDateRanges: function (successFunction:(dateRanges:Entities.DTODateRangePartition[]) => void, errorFunction) {
          return res.getDateRanges(null,
              function (dateRanges:Entities.DTODateRangePartition[]) {
                if(dateRanges&& (<any>dateRanges).content) {
                  (<any>dateRanges).content.sort((t1, t2)=> propertiesUtils.sortAlphbeticFun('name'));
                  for(var i=0; i<(<any>dateRanges).content.length; i++) {
                    if( (<any>dateRanges).content[i].dateRangeItemDtos ) {
                      (<any>dateRanges).content[i].dateRangeItemDtos = (<any>dateRanges).content[i].dateRangeItemDtos.reverse();
                      // if( Entities.DTODateRangePoint.IsStartOfTime(   (<any>dateRanges).content[i].dateRangeItemDtos[  (<any>dateRanges).content[i].dateRangeItemDtos.length-1].start)) {
                      //   (<any>dateRanges).content[i].dateRangeItemDtos[  (<any>dateRanges).content[i].dateRangeItemDtos.length-1] = null;
                      // }
                    }
                  }
                  successFunction((<any>dateRanges).content);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to get data ranges " + error);
              }
          )
        },
        getPartition: function (partitionId:number,successFunction:(datePartition:Entities.DTODateRangePartition) => void, errorFunction) {
          return res.getPartition({partitionId:partitionId},
              function (datePartition:Entities.DTODateRangePartition) {
                datePartition.dateRangeItemDtos =  datePartition.dateRangeItemDtos.reverse();
                // if( Entities.DTODateRangePoint.IsStartOfTime( datePartition.dateRangeItemDtos[datePartition.dateRangeItemDtos.length-1].start)) {
                //   datePartition.dateRangeItemDtos[datePartition.dateRangeItemDtos.length-1] = null;
                // }
                successFunction(datePartition);
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to get data ranges " + error);
              }
          )
        },
        addNewDateRange: function (dateRange:Entities.DTODateRangePartition,successFunction:(addedDateRange) => void, errorFunction) {
          dateRange.dateRangeItemDtos =  dateRange.dateRangeItemDtos.reverse();
          // if( ! dateRange.dateRangeItemDtos[dateRange.dateRangeItemDtos.length-1].start) {
          //   dateRange.dateRangeItemDtos[dateRange.dateRangeItemDtos.length - 1].start = Entities.DTODateRangePoint.StartOfTime();
          // }
          return res.addNewDateRange(null,dateRange,
              function (addedDateRange) {
                successFunction(addedDateRange);
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to add  data range " + error);
              }
          )
        },
        updateDateRange: function (dateRange:Entities.DTODateRangePartition,successFunction:(updatedDatePartition:Entities.DTODateRangePartition) => void, errorFunction) {
          dateRange.dateRangeItemDtos =  dateRange.dateRangeItemDtos.reverse();
          return res.updateDateRange({partitionId:dateRange.id},dateRange,
              function (updatedDatePartition) {
                successFunction(updatedDatePartition);
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to update data range " + error);
              }
          )
        },
        deleteDateRange: function (partitionId:number,successFunction:() => void, errorFunction) {
          return res.deleteDateRange({partitionId:partitionId},null,
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.error("failed to delete data range " + error);
              }
          )
        },
      }
      return factory;
    });



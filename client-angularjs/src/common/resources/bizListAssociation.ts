
///<reference path='..\all.d.ts'/>
'use strict';

interface IBizListAssociationResource
{
  addBizListAssociation(bizListId,bizListItemId,itemId):any;
  removeBizListAssociation(bizListId,bizListItemId,itemId):any;
}

angular.module('resources.bizListAssociations', [
    ])

  .factory( 'bizListAssociationResource', function( $resource,configuration:IConfig) {
  var group = $resource(configuration.addRemove_group_bizList_associate_url, {bizListId:'@bizListId',bizListItemId:'@bizListItemId', groupId: '@groupId' }, {

    addBizListAssociation: { method: 'PUT' },
    removeBizListAssociation: { method: 'DELETE' },

  });
  var folder = $resource(configuration.addRemove_folder_bizList_associate_url, {bizListId:'@bizListId',bizListItemId:'@bizListItemId', folderId: '@folderId' }, {

    addBizListAssociation: { method: 'PUT' },
    removeBizListAssociation: { method: 'DELETE' },

  });
  var file = $resource(configuration.addRemove_file_bizList_associate_url, {bizListId:'@bizListId',bizListItemId:'@bizListItemId', fileId: '@fileId' }, {

    addBizListAssociation: { method: 'PUT' },
    removeBizListAssociation: { method: 'DELETE' },

  });
  return function(entityDisplayType: EntityDisplayTypes):IBizListAssociationResource
  {
    if(entityDisplayType==EntityDisplayTypes.groups)
    {
      var result:IBizListAssociationResource =
      {

        addBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return group.addBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,groupId:itemId});
        },
        removeBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return group.removeBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,groupId:itemId});
        }
      }
      return result;
    }
    if(entityDisplayType==EntityDisplayTypes.folders|| entityDisplayType==EntityDisplayTypes.folders_flat)
    {
      var result:IBizListAssociationResource =
      {

        addBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return folder.addBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,folderId:itemId});
        },
        removeBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return folder.removeBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,folderId:itemId});
        }
      }
      return result;
    }
    if(entityDisplayType==EntityDisplayTypes.files)
    {
      var result:IBizListAssociationResource =
      {

        addBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return file.addBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,fileId:itemId});
        },
        removeBizListAssociation:function(bizListId,bizListItemId,itemId)
        {
          return file.removeBizListAssociation({bizListId:bizListId,bizListItemId:bizListItemId,fileId:itemId});
        }
      }
      return result;
    }
    return null;
  }


});



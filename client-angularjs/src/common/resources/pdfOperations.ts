///<reference path='..\all.d.ts'/>
'use strict';
interface IPdfOperations
{
  exportToPdf(objectsSelector:string, fileName:string, finishFunc:any): void;
}

declare function unescape(s:string): string;

angular.module('resources.IPdfOperations', [
    ])
    .factory( 'pdfOperationsResource', function( $resource ) {


      function beforePDFPrinting() {
        var cc = document.getElementsByTagName("svg");
        for (let i = 0; i < cc.length; i++) {
          var svg:any  = cc[i];
          var rect = svg.getBoundingClientRect();

          var img:any  = document.createElement("img");
          img.src = 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg.outerHTML)));
          img.style = "position:absolute;top:0px;left:0px;";
          img.className = "remove-after-print";
          svg.parentNode.insertBefore(img, svg);
        }
      }


      function afterPDFPrinting() {
        $(".remove-after-print").remove();
      }



      var factory = <IPdfOperations>{
        exportToPdf:  function(objectsSelector:string, fileName:string, finishFunc:any): void  {
          beforePDFPrinting();
          setTimeout(()=> {
            var root = new kendo.drawing.Group();
            let totalItems: number = $(objectsSelector).length;
            $(objectsSelector).each(function (section) {
              kendo.drawing.drawDOM(this).then(function (group) {
                group.options.set("pdf", {
                  paperSize: "A4",
                  margin: "1cm",
                  landscape: true

                });
                root.append(group);

              }).done(function (data) {
                totalItems--;
                if (totalItems == 0) {
                  setTimeout(() => {
                    (<any>root).options.set("pdf", {
                      multiPage: 'true',
                    });
                    (<any>kendo).drawing.pdf.saveAs(root, fileName);
                    afterPDFPrinting();
                    if (finishFunc !== null) {
                      finishFunc();
                    }
                  });
                }
              });
            });
          });
        }};

      return factory;
    })


///<reference path='..\all.d.ts'/>
'use strict';
interface IDashboardScanResource {

  getScanStatistics(successFunction: (counterReportDtos:Dashboards.DtoCounterReport[]) => void , errorFunction): void;
  getScanConfiguration(successFunction: (stringListReportDtos:Dashboards.DtoStringListReport[],stringReportDtos:Dashboards.DtoStringReport[]) => void , errorFunction): void;
  getScanCriteria(successFunction: (scanCriterias:Dashboards.DtoMultiValueList) => void , errorFunction): void;
}


interface IDashboardAdminResource {
  getAdminDocumentStatistics(successFunction: (counterReportDtos:Dashboards.DtoCounterReport[]) => void , errorFunction): void;
}


interface IDashboardUserResource {
  getUserDocumentStatistics(successFunction: (counterReportDtos:Dashboards.DtoCounterReport[]) => void , errorFunction): void;
}

interface IDashboardChartsResource {
  getGroupSizeHistogram(successFunction: (groupSizeHistogram:Dashboards.DtoMultiValueList) => void , errorFunction): void;
  getFileTypeHistogram(filter, filterByRole: boolean, successFunction: (fileTypeHistogram:Dashboards.DtoMultiValueList) => void , errorFunction): void;
  getFileExtensionHistogram(filter,successFunction: (fileTypeHistogram:Dashboards.DtoMultiValueList) => void , errorFunction): void;
  getFilesAccessHistogram(filter, filterByRole: boolean,successFunction: (fileTypeHistogram:Dashboards.DtoMultiValueList) => void , errorFunction): void;
}

angular.module('resources.dashboards', [
])

  .factory( 'dashboardUserResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.dashboard_user_documents_statistics, "", {
      getUserDocumentStatistics: {method: 'Get', params: {}, isArray: false}
    });

    var factory = <IDashboardUserResource>{
      getUserDocumentStatistics: function (successFunction:(counterReportDtos:Dashboards.DtoCounterReport[]) => void, errorFunction) {
        return res.getUserDocumentStatistics(
          function (summaryReport:Dashboards.DTOSummaryReport) {
            if(summaryReport) {
              successFunction(summaryReport.counterReportDtos);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (response,error) {

            errorFunction(error);
            console.log("failed to get scan statistics " + error);
          }
        )
      }
    };
    return factory;
  })

  .factory( 'dashboardAdminResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.dashboard_admin_documents_statistics, "", {
      getAdminDocumentStatistics: {method: 'Get', params: {}, isArray: false}
    });

    var factory = <IDashboardAdminResource>{
      getAdminDocumentStatistics: function (successFunction:(counterReportDtos:Dashboards.DtoCounterReport[]) => void, errorFunction) {
        return res.getAdminDocumentStatistics(
          function (summaryReport:Dashboards.DTOSummaryReport) {
            if(summaryReport) {
              successFunction(summaryReport.counterReportDtos);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (response,error) {

            errorFunction(error);
            console.log("failed to get scan statistics " + error);
          }
        )
      }
    };
    return factory;
  })


  .factory( 'dashboardScanResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.dashboard_scan_url + "/:action", "", {
      getScanStatistics: {method: 'Get', params: {action: configuration.dashboard_scan_statistics}, isArray: false},
      getScanConfiguration: {
        method: 'Get',
        params: {action: configuration.dashboard_scan_configuration},
        isArray: false
      },
      getScanCriteria: {method: 'Get', params: {action: configuration.dashboard_scan_criteria}, isArray: false},

    });

    var factory = <IDashboardScanResource>{
      getScanStatistics: function (successFunction:(counterReportDtos:Dashboards.DtoCounterReport[]) => void, errorFunction) {
        return res.getScanStatistics(
          function (summaryReport:Dashboards.DTOSummaryReport) {
            if(summaryReport) {
              successFunction(summaryReport.counterReportDtos);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (response,error) {

            errorFunction(error);
            console.log("failed to get scan statistics " + error);
          }
        )
      },
      getScanCriteria: function (successFunction:(scanCriterias:Dashboards.DtoMultiValueList) => void, errorFunction) {
        return res.getScanCriteria(
          function (summaryReport:Dashboards.DTOSummaryReport) {
            if(summaryReport) {
              successFunction(summaryReport.multiValueListDto);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get scan getScanCriteria " + error);
          }
        )
      },
      getScanConfiguration: function (successFunction:(stringListReportDtos:Dashboards.DtoStringListReport[],stringReportDtos:Dashboards.DtoStringReport[]) => void, errorFunction) {
        return res.getScanConfiguration(
          function (summaryReport:Dashboards.DTOSummaryReport) {
            if(summaryReport) {
              successFunction(summaryReport.stringListReportDtos,summaryReport.stringReportDtos);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
             errorFunction(error);
            console.log("failed to get scan getScanCriteria " + error);
          }
        )
      },
    };
    return factory;
  })
  .factory( 'dashboardChartResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.dashboard_groupSize_histogram_url + "/:action", "", {
       getGroupSizeHistogram: {
        method: 'Get',
        params: {},
        isArray: false
      },
      getFileTypeHistogram: {
        method: 'Get',
        params: {},
        url: configuration.dashboard_fileType_histogram_url,
        isArray: false
      },
      getFileTypeHistogramRole: {
        method: 'Get',
        url: configuration.dashboard_fileType_histogram_role_url,
        params: {files: true},
        isArray: false
      },
      getFileExtensionHistogram: {
        method: 'Get',
        url: configuration.dashboard_fileExtension_histogram_url,
        isArray: false,
        params: {}
      },
      getFilesAccessHistogram: {
        method: 'Get',
        params: {},
        url: configuration.dashboard_file_access_histogram_url,
        isArray: false
      },
      getFilesAccessHistogramRole: {
        method: 'Get',
        url: configuration.dashboard_file_access_histogram_role_url,
        params: {files: true},
        isArray: false
      },

    });

    var factory = <IDashboardChartsResource>{
      getFileTypeHistogram: function (filter,filterByRole, successFunction:(fileTypeHistogram:Dashboards.DtoMultiValueList) => void, errorFunction) {
        if (filterByRole) {
          return res.getFileTypeHistogramRole({filter: filter},
            function (summaryReport: Dashboards.DtoMultiValueList) {
              if (summaryReport) {
                successFunction(summaryReport);
              }
              else {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get scan getScanConfiguration " + error);
            }
          )
        }
        else {
          return res.getFileTypeHistogram({filter: filter},
            function (summaryReport: Dashboards.DtoMultiValueList) {
              if (summaryReport) {
                successFunction(summaryReport);
              }
              else {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get scan getScanConfiguration " + error);
            }
          )
        }
      },
      getFileExtensionHistogram: function (filter,successFunction:(fileExtensionHistogram:Dashboards.DtoMultiValueList) => void, errorFunction) {
        return res.getFileExtensionHistogram({filter:filter},
            function (summaryReport:Dashboards.DtoMultiValueList) {
              if(summaryReport) {
                successFunction(summaryReport);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get scan getScanConfiguration " + error);
            }
        )
      },
      getGroupSizeHistogram: function (successFunction:(groupSizeHistogram:Dashboards.DtoMultiValueList) => void, errorFunction) {
        return res.getGroupSizeHistogram(
          function (summaryReport:Dashboards.DtoMultiValueList) {
            if(summaryReport) {
              successFunction(summaryReport);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get scan getScanConfiguration " + error);
          }
        )
      },
      getFilesAccessHistogram: function (filter,filterByRole, successFunction:(groupSizeHistogram:Dashboards.DtoMultiValueList) => void, errorFunction) {
        if (filterByRole) {
          return res.getFilesAccessHistogramRole({filter: filter},
            function (summaryReport: Dashboards.DtoMultiValueList) {
              if (summaryReport) {
                successFunction(summaryReport);
              }
              else {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get scan getFilesAccessHistogram " + error);
            }
          )
        }
        else {
          return res.getFilesAccessHistogram({filter: filter},
            function (summaryReport: Dashboards.DtoMultiValueList) {
              if (summaryReport) {
                successFunction(summaryReport);
              }
              else {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get scan getFilesAccessHistogram " + error);
            }
          )
        }
      }
    };
    return factory;
  });


///<reference path='..\all.d.ts'/>
import IPromise = angular.IPromise;
import EMediaType = Operations.EMediaType;
'use strict';
interface IOperationsResource {

  startApplyTagsToSolr(successFunction: () => void , errorFunction): void;
  stopApplyTagsToSolr(successFunction: () => void , errorFunction): void;
  startResyncSolr(successFunction: () => void , errorFunction): void;
  applyTagsToSolrProgressStatus(successFunction: (status:Operations.DTOApplyToSolrStatus) => void , errorFunction): void;
  toggleLabelingSet(successFunction:(status:any) => void, errorFunction:(p:any) => void);

}
interface IAbortablePromise {
  promise: IPromise<any>,
  abort () :void
}
interface IActiveScansResource {

  startScanRootFolders(rootFolderIds:number[],isScan:boolean,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean, successFunction:() => void, errorFunction): void;
  pauseAllRuns(successFunction: () => void , errorFunction): void;
  pauseRunsByID(runIDs:number[] , successFunction: () => void , errorFunction): void;
  pauseJobsByID(jobIDs:number[] , successFunction: () => void , errorFunction): void;
  stopRunsByID(runIDs:number[] , successFunction: () => void , errorFunction): void;
  resumeRunsByID(runIDs:number[]  , successFunction: () => void , errorFunction): void;
  resumeJobsByID(runIDs:number[]  , successFunction: () => void , errorFunction): void;
  forceRerunJobEnqueuedTasks(jobIDs:number[]  , successFunction: () => void , errorFunction): void;
  forceRerunRunEnqueuedTasks(runID:number  , successFunction: () => void , errorFunction): void;
  resumeAllRuns(successFunction: () => void , errorFunction): void;
  startBizListExtraction(bizListId, successFunction: () => void , errorFunction): void;
  markForFullScan(rootFolderId, mark:boolean, successFunction: () => void , errorFunction): void;
  updateScanStrategy(strategy:Operations.EJobProcessingMode, successFunction: () => void , errorFunction): void;
  getScanStrategyJobProcessingMode(successFunction:(JobProcessingMode:Operations.EJobProcessingMode) => void , errorFunction): void;
  getActiveRuns(successFunction: (runs:Operations.DtoRunSummaryInfo[]) => void , errorFunction): IAbortablePromise;
  getRootFoldersAggregatedSummaryInfo(successFunction: (rootFoldersAggregatedSummaryInfo:Operations.DtoRootFoldersAggregatedSummaryInfo) => void , errorFunction): IAbortablePromise;
  getAutoClassifyGroupsRunStatus(successFunction: (runStatus:Entities.DtoClassificationProgress) => void , errorFunction): void;
//  getLastIngestError(successFunction: (t:string) => void , errorFunction): void;
}

interface IRootFoldersResource {

  getRootFolders(successFunction: (t:Operations.DtoRootFolder[]) => void , errorFunction): void;
  getRootFolder(rootFolderId,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  getRootFolderScheduleGroup(rootFolderId,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  setRootFolderScheduleGroup(rootFolderId,scheduleGroupId,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  addNewRootFolder(rootFolder:Operations.DtoRootFolder,scheduleGroupId,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  uploadRootFoldersFromCsv(fileData,updateDuplicates:boolean,createStubs:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadRootFoldersFromCsv(fileData,maxErrorRowsReport:number, createStubs:boolean, successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  uploadExcludedFolderRulesFromCsv(fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadExcludedFolderRulesFromCsv(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  updateRootFolder(rootFolder:Operations.DtoRootFolder,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  updateRootFolderRescanActive(rootFolderId:number,active:boolean,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  deleteRootFolder(rootFolderId:number,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  deleteRootFolderForce(rootFolderId:number,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction): void;
  addNewExcludedFolderRule(rootFolderId:string,excludedFolderRule:Operations.DtoFolderExcludeRule,successFunction: (t:Operations.DtoFolderExcludeRule) => void , errorFunction): void;
  updateExcludedFolderRule(rootFolderId:string,excludedFolderRule:Operations.DtoFolderExcludeRule,successFunction: (t:Operations.DtoFolderExcludeRule) => void , errorFunction): void;
  deleteExcludedFolderRule(rootFolderId:string,excludedFolderId:number,successFunction: () => void , errorFunction): void;
  getExcludedFolderRules(rootFolderId,maxElementCount:number,successFunction:(excludedFolders:Operations.DtoFolderExcludeRule[],totalElements:number) => void, errorFunction) :void;

}

interface IMediaTypeConnectionResource<T extends Operations.DtoMediaTypeConnectionDetails> {
  getConnections(successFunction: (list:T[]) => void , errorFunction): void;
  getConnection(connectionId,successFunction: (t:T) => void , errorFunction): void;
  addNewConnection(connection:T,successFunction: (t:T) => void , errorFunction): void;
  updateConnection(connection:T,successFunction: (t:T) => void , errorFunction): void;
  deleteConnection(connectionId:number,successFunction: (t:T) => void , errorFunction): void;
  testConnection (connectionParams:string|Operations.DtoSharePointConnectionParameters,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
  testConnectionWithDatacenterWithSavedConnection (connectionId:number,connectionParams,dataCenterId:number,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
  testConnectionWithDatacenter (connectionParams,dataCenterId:number,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
  uploadConnectionsFromCsv(fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadConnectionsFromCsv(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
}

interface ILdapConnectionResource
{
  getConnections(successFunction: (list:Operations.DtoLdapConnectionDetails[]) => void , errorFunction): void;
  getConnection(connectionId,successFunction: (t:Operations.DtoLdapConnectionDetails) => void , errorFunction): void;
  addNewConnection(connection:Operations.DtoLdapConnectionDetails,successFunction: (t:Operations.DtoLdapConnectionDetails) => void , errorFunction): void;
  updateConnection(connection:Operations.DtoLdapConnectionDetails,successFunction: (t:Operations.DtoLdapConnectionDetails) => void , errorFunction): void;
  deleteConnection(connectionId:number,successFunction: (t:Operations.DtoLdapConnectionDetails) => void , errorFunction): void;
  testConnection(ldapConnection:Operations.DtoLdapConnectionDetails,successFunction: (success:boolean,reason:string) => void , errorFunction): void;
  updateLdapEnableState(connectionId:number, enabled:boolean,successFunction: (success:boolean,reason:string) => void , errorFunction): void;
  uploadConnectionsFromCsv(fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadConnectionsFromCsv(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
}

interface IScheduleGroupResource {
  getScheduleGroups(successFunction: (t:Operations.DtoScheduleGroup[]) => void , errorFunction): void;
  getScheduleGroup(scheduleGroupId,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  addNewScheduleGroup(schedule:Operations.DtoScheduleGroup,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  updateScheduleGroup(schedule:Operations.DtoScheduleGroup,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  updateScheduleGroupRescanActive(scheduleId:number,active:boolean,successFunction: (t:Operations.DtoScheduleGroup) => void , errorFunction): void;
  deleteScheduleGroup(scheduleGroupId:number,successFunction: () => void , errorFunction): void;
  startRunForScheduleGroups(scheduleGroupIds:number[],isScan:boolean,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean, successFunction:() => void, errorFunction): void;
  stopRunForScheduleGroup(scheduleGroupId,successFunction: () => void , errorFunction): void;
  uploadScheduleGroupsFromCsv(fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadScheduleGroupsFromCsv(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
}

interface IDepartmentResource {
  getDepartments(successFunction: (t:Operations.DtoDepartment[]) => void , errorFunction): void;
  getDepartment(departmentId:number,successFunction: (t:Operations.DtoDepartment) => void , errorFunction): void;
  addNewDepartment(department:Operations.DtoDepartment,successFunction: (t:Operations.DtoDepartment) => void , errorFunction): void;
  updateDepartment(department:Operations.DtoDepartment,successFunction: (t:Operations.DtoDepartment) => void , errorFunction): void;
  deleteDepartment(departmentId:number,successFunction: () => void , errorFunction): void;
  addDepartment(departmentId:number,folderId:number);
  removeDepartment(departmentId:number,folderId:number);
}

interface IUserStorageService  {

  setUserData(moduleName:string,key,data,successFunction: () => void , errorFunction): void;
  deleteUserData(moduleName:string,key,successFunction: () => void , errorFunction): void;
  getUserData(moduleName:string,key,successFunction: (data:any) => void , errorFunction): any;
}

interface ICustomerDataCenterResource  {

  updateDataCenter(dataCenter:Operations.DtoCustomerDataCenter,successFunction: (updatedDataCenter:Operations.DtoCustomerDataCenter) => void , errorFunction): void;
  addDataCenter(dataCenter:Operations.DtoCustomerDataCenter,successFunction: (updatedDataCenter:Operations.DtoCustomerDataCenter) => void , errorFunction): void;
  deleteDataCenter(dataCenterId:number,successFunction: () => void , errorFunction): void;
  getDataCenters(successFunction: (dataCenters:Operations.DtoCustomerDataCenter[]) => void , errorFunction): any;
  getDataCenter(dataCenterId,successFunction: (dataCenters:Operations.DtoCustomerDataCenter) => void , errorFunction): any;
}
interface IServerComponentResource  {
  addServerComponent(component:Operations.DtoClaComponent,successFunction: (updated:Operations.DtoClaComponent) => void , errorFunction): void;
  getServerComponent(componentId:any,successFunction: (updated:Operations.DtoClaComponent) => void , errorFunction): void;
  updateServerComponent(component:Operations.DtoClaComponent,successFunction: (updated:Operations.DtoClaComponent) => void , errorFunction): void;
  deleteServerComponent(serverComponentId:number,successFunction: () => void , errorFunction): void;
  getServerComponents(successFunction: (components:Operations.DtoClaComponent[]) => void , errorFunction): any;
}

angular.module('resources.operations', [
])
  .factory( 'operationsResource', function( $resource,configuration:IConfig,$q ) {

    var actionsConfig = {
      startApplyTagsToSolr: {method: 'POST', params: null, isArray: false},
      stopApplyTagsToSolr: {method: 'DELETE', params: null, isArray: false},
      applyTagsToSolrProgressStatus: {method: 'GET', params: null, isArray: false},
      startResyncSolr: {method: 'GET', params: null,url:configuration.startResyncSolr , isArray: false},
    };

    var res = $resource(configuration.applyTagsToSolr , "", actionsConfig);
    var  outstanding = [];
    //for each action add wrapper to promise that supports abort
    Object.keys(actionsConfig).forEach(function(action) {
      var method = res[action];

      res[action] = function () {
        var deferred = $q.defer();
        var promise = method.apply(null, arguments).$promise;

        abortablePromiseWrap(promise, deferred, outstanding);

        return {
          promise: deferred.promise,
          abort: function () {
            deferred.reject('Aborted');
          }
        };
      };
    });
    /**
     * Abort all the outstanding requests on
     * this $resource. Calls promise.reject() on outstanding [].
     */
    res.abortAll = function () {
      _.invoke(outstanding, 'reject', 'Aborted all');
      outstanding = [];
    };
    function abortablePromiseWrap (promise, deferred, outstanding) {
      promise.then(function() {
        deferred.resolve.apply(deferred, arguments);
      });

      promise.catch(function() {
        deferred.reject.apply(deferred, arguments);
      });
      /**
       * Remove from the outstanding array
       * on abort when deferred is rejected
       * and/or promise is resolved/rejected.
       */
      deferred.promise.finally(function() {
        var index = outstanding.indexOf(deferred, 0);
        if (index > -1) {
          outstanding.splice(index, 1);
        }
      });
      outstanding.push(deferred);
    }
    var alternativeLabelingSetRes = $resource(configuration.alternativeLabelingSet , "", {
      toggleLabelingSet: {method: 'GET', params: {toggle:true}, isArray: false},
    });

    var factory = <IOperationsResource>{
      startApplyTagsToSolr: function (successFunction:() => void, errorFunction) {
        return res.startApplyTagsToSolr(
          function () {

              successFunction();
          }
          ,  function(response) {

              //404 or bad
              if(response.status === 409) {
                errorFunction({userMsg:"Scan is currently running. Try apply tags later."});
              }
              else
              {
                errorFunction(null);
              }

          }
        );
      },
      stopApplyTagsToSolr: function (successFunction:() => void, errorFunction) {
        return res.stopApplyTagsToSolr(
          function () {

              successFunction();

          }
          , function (response,error) {

          }
        );
      },
      applyTagsToSolrProgressStatus: function (successFunction:(status:Operations.DTOApplyToSolrStatus) => void, errorFunction) {
        var checkStatusPromise =  res.applyTagsToSolrProgressStatus();
        checkStatusPromise.promise.then(function(status:Operations.DTOApplyToSolrStatus) {
              if(status) {
                successFunction(status);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        );
        return checkStatusPromise;
      },
      startResyncSolr: function (successFunction:() => void, errorFunction) {
        return res.startResyncSolr(
            function () {
              successFunction();
            }
            ,  function(response) {

              //404 or bad
              if(response.status === 409) {
                errorFunction({userMsg:"Scan is currently running. Try re-sync later."});
              }
              else
              {
                errorFunction(null);
              }

            }
        );
      },
      toggleLabelingSet: function (successFunction:(s:any) => void, errorFunction:(e:any) => void) {
        return alternativeLabelingSetRes.toggleLabelingSet(
            function (s) {
              successFunction(s);
            }
            , function(response) {
              errorFunction(null);
            }
        );
      },
    };
    return factory;
  })
  .factory( 'activeScansResource', function( $resource,$q,configuration:IConfig ) {
    var actionsConfig = {
      startScanRootFolders: {
        method: 'POST',
        url:  configuration.startScanRootFolders_url,
        params: { 'op':'@op', rootFolderIds: '@rootFolderIds'},
        isArray: true},//true because return value is string
      pauseRunsByID: {
        method: 'POST',
        url:  configuration.pauseRuns_url,
        params: { 'runIds':'@runIds'},
        isArray: false},
      pauseJobsByID: {
        method: 'POST',
        url:  configuration.pauseJobs_url,
        params: { 'runIds':'@jobIds'},
        isArray: false},
      stopRunsByID: {
        method: 'POST',
        url:  configuration.stopRuns_url,
        params: { 'runIds':'@runIds'},
        isArray: false},
      resumeRunsByID: {
        method: 'POST',
        params: { 'runIds':'@runIds'},
        url:  configuration.resumeRuns_url,
        isArray: false},
      resumeJobsByID: {
        method: 'POST',
        params: { 'runIds':'@jobIds'},
        url:  configuration.resumeJobs_url,
        isArray: false},
      forceRerunJobEnqueuedTasks: {
        method: 'POST',
        params: { 'jobIds':'@jobIds'},
        url:  configuration.forceRerunJobEnqueuedTasks_url,
        isArray: false},
      forceRerunRunEnqueuedTasks: {
        method: 'POST',
        params: { 'runId':'@runId'},
        url:  configuration.forceRerunRunEnqueuedTasks_url,
        isArray: false},
      resumeAllRuns: {
        method: 'POST',
        url:  configuration.resumeAllRun_url,
        isArray: false},
      pauseAllRuns: {
        method: 'POST',
        url:  configuration.pauseAllRuns_url,
        isArray: false},
      startBizListExtraction: {
        method: 'POST',
        url:configuration.extractBizList_url,
        params: {bizListId:'@bizListId'},
        isArray: false},
      getActiveRuns: {
        method: 'Get',
        url: configuration.geActiveRunsDetails_url,
        isArray: true,
      },
      getRootFoldersAggregatedSummaryInfo: {
        method: 'Get',
        url: configuration.getRootFoldersAggregatedSummaryInfo_url,
        isArray: false
      },
      getAutoClassifyGroupsRunStatus: {
        method: 'GET',
        url:configuration.bizList_auto_classify_groups_runStatus_url,
        isArray: false
      },
      markForFullScan: {
        method: 'POST',
        params: {rootFolderId: '@rootFolderId'},
        url:configuration.reingestRootFolder_url,
        data:{reIngest:'@mark'},
        isArray: false
      },
      updateScanStrategy: {
        method: 'PUT',
        params: {jobProcessingMode: '@jobProcessingMode'},
        url:configuration.updateScanstrategy_url,
        isArray: false
      },
      getScanStrategyJobProcessingMode : {
        method: 'GET',
        url:configuration.getScanStrategyJobProcessingMode_url,
        isArray: false
      }
    };

    var res = $resource(configuration.startScanRootFolders_url , "", actionsConfig);
    var  outstanding = [];
    //for each action add wrapper to promise that supports abort
    Object.keys(actionsConfig).forEach(function(action) {
      var method = res[action];

      res[action] = function () {
        var deferred = $q.defer();
        var promise = method.apply(null, arguments).$promise;

        abortablePromiseWrap(promise, deferred, outstanding);

        return <IAbortablePromise> {
          promise: deferred.promise,
          abort: function () {
            deferred.reject('Aborted');
          }
        };
      };
    });
          /**
           * Abort all the outstanding requests on
           * this $resource. Calls promise.reject() on outstanding [].
           */
      res.abortAll = function () {
        _.invoke(outstanding, 'reject', 'Aborted all');
        outstanding = [];
      };
     function abortablePromiseWrap (promise, deferred, outstanding) {
            promise.then(function() {
              deferred.resolve.apply(deferred, arguments);
            });

            promise.catch(function() {
              deferred.reject.apply(deferred, arguments);
            });
            /**
             * Remove from the outstanding array
             * on abort when deferred is rejected
             * and/or promise is resolved/rejected.
             */
            deferred.promise.finally(function() {
              var index = outstanding.indexOf(deferred, 0);
              if (index > -1) {
                outstanding.splice(index, 1);
              }
            });
            outstanding.push(deferred);
          }
    var factory = <IActiveScansResource>{
      startScanRootFolders: function (rootFolderIds:number[],isScan:boolean,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean, successFunction:() => void, errorFunction){
        var folderIds =rootFolderIds? rootFolderIds.join():null;
        var op;
        if(isCompleteScanNoExtract)
        {
          op='ingest,analyze';
        }
        else  if(isScan)
        {
          op='scan';
        }
        else  if(isCompleteScanWithExtract)
        {
          op='ingest,analyze,extract';
        }
        else  if(isExtractOnly)
        {
          op='extract';
        }
        else {
          op='scan,ingest,analyze,extract';
        }
        return res.startScanRootFolders({rootFolderIds:folderIds,op:op},
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      pauseAllRuns: function (successFunction:() => void, errorFunction) {
        return res.pauseAllRuns(
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      pauseRunsByID: function (runIds:number[], successFunction:() => void, errorFunction) {
        return res.pauseRunsByID({runIds:runIds},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      pauseJobsByID: function (jobIds:number[], successFunction:() => void, errorFunction) {
        return res.pauseJobsByID({jobIds:jobIds},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      stopRunsByID: function (runIds:number[], successFunction:() => void, errorFunction) {
        return res.stopRunsByID({runIds:runIds},null,
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      resumeRunsByID: function (runIds:number[], successFunction:() => void, errorFunction) {
        return res.resumeRunsByID({runIds:runIds},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      resumeJobsByID: function (jobIds:number[], successFunction:() => void, errorFunction) {
        return res.resumeJobsByID({jobIds:jobIds},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      forceRerunJobEnqueuedTasks: function (jobIds:number[], successFunction:() => void, errorFunction) {
        return res.forceRerunJobEnqueuedTasks({jobIds:jobIds},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      forceRerunRunEnqueuedTasks: function (runId:number, successFunction:() => void, errorFunction) {
        return res.forceRerunRunEnqueuedTasks({runId:runId},null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      resumeAllRuns: function (successFunction:() => void, errorFunction) {
        return res.resumeAllRuns(null,null,
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

      resumeScanRootfolder: function (analyzeAll:boolean,extractAfterAnalyze:boolean, successFunction:() => void, errorFunction) {

        return res.resumeScanRootfolder(extractAfterAnalyze?{op:'ingest,analyze,extract',includeOther:analyzeAll}:{op:'ingest,analyze',includeOther:analyzeAll},
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      startBizListExtraction: function (bizListId,successFunction:() => void, errorFunction) {
        return res.startBizListExtraction({bizListId:bizListId},
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      getActiveRuns: function (successFunction:(status:Operations.DtoRunSummaryInfo[]) => void, errorFunction) :IAbortablePromise{
        return res.getActiveRuns(
          function (runs:Operations.DtoRunSummaryInfo[]) {
            successFunction(runs);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getRootFoldersAggregatedSummaryInfo: function (successFunction:(status:Operations.DtoRootFoldersAggregatedSummaryInfo) => void, errorFunction) :IAbortablePromise{
        return res.getRootFoldersAggregatedSummaryInfo(
          function (rootFoldersAggregatedSummaryInfo:Operations.DtoRootFoldersAggregatedSummaryInfo) {
            successFunction(rootFoldersAggregatedSummaryInfo);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      markForFullScan: function (rootFolderId:number,mark:boolean, successFunction:() => void, errorFunction) :IAbortablePromise{
        return res.markForFullScan({rootFolderId:rootFolderId},mark,
          function () {
            successFunction();
          }, function (error) {
            errorFunction(error);
          }
        )
      },

      updateScanStrategy: function (strategy: Operations.EJobProcessingMode, successFunction:() => void, errorFunction) :IAbortablePromise{
        return res.updateScanStrategy({jobProcessingMode :strategy},
          function () {
            successFunction();
          }, function (error) {
            errorFunction(error);
          }
        )
      },
      getScanStrategyJobProcessingMode:function (successFunction:(jobProcessingMode:Operations.EJobProcessingMode) => void, errorFunction) {
        return res.getScanStrategyJobProcessingMode(
          function (jobProcessingMode:Operations.EJobProcessingMode) {
            successFunction(jobProcessingMode)
          }, function (error) {
            errorFunction(error);
          }
        )
      },
      getAutoClassifyGroupsRunStatus: function (successFunction:(runStatus:Entities.DtoClassificationProgress) => void, errorFunction) {
        var checkStatusPromise =  res.getAutoClassifyGroupsRunStatus();
        checkStatusPromise.promise.then(function(runStatus:Entities.DtoClassificationProgress) {
          if(runStatus) {
            successFunction(runStatus);
          }
          else
          {
            errorFunction(null);
          }
        }, function(error) {
          errorFunction(error);
          console.log("failed to get Run Status " + error);
        });
        return  checkStatusPromise;
        //return res.getAutoClassifyGroupsRunStatus(null,
        //    function (runStatus:Entities.DtoClassificationProgress) {
        //        successFunction(runStatus);
        //    }
        //    , function (error) {
        //      errorFunction(error);
        //      console.log("failed to get autoClassifyGroupsRunStatus " + error);
        //    }
        //)
      },

    };
    return factory;
  })
  .factory( 'rootFoldersResource', function( $resource,configuration:IConfig) {
    var res = $resource(configuration.root_folders_url, "", {
      getRootFolders: {
        method: 'Get',
        url: configuration.root_folders_url,
        isArray: false},
      getExcludedFolderRules: {
        method: 'Get',
        url: configuration.root_folder_excluded_rule_url,
        isArray: false},
      getRootFolder: {
        method: 'Get',
        params: {id: '@id'},
        url: configuration.root_folder_url,
        isArray: false},
      getRootFolderScheduleGroup: {
        method: 'Get',
        url: configuration.scheduleGroups_for_root_folder_url,
        params: {rootFolderId: '@rootFolderId'},
        isArray: false},
      setRootFolderScheduleGroup: {
        method: 'POST',
        url: configuration.addRemove_root_folder_scheduleGroup_url,
        params: {rootFolderId: '@rootFolderId',scheduleGroupId: '@scheduleGroupId'},
        isArray: false},

      addNewExcludedFolderRule: {
        method: 'POST',
        params: {rootFolderId: '@rootFolderId'},
        data:{},
        url: configuration.root_folder_excluded_rule_url+'/new',
        isArray: false},
      updateExcludedFolderRule: {
        method: 'POST',
        params: {rootFolderId: '@rootFolderId',id: '@_id'},
        data:{},
        url: configuration.root_folder_excluded_rule_url+'/:id',
        isArray: false},
      deleteExcludedFolderRule: {
        method: 'DELETE',
        url: configuration.root_folder_excluded_rule_url+'/:id',
        params: {rootFolderId: '@rootFolderId',id: '@_id'},
        isArray: false},

      addNewRootFolder: {
        method: 'POST',
        url: configuration.root_folders_url+'/new?scheduleGroupId=:scheduleGroupId',
        params: {scheduleGroupId: '@scheduleGroupId'},
        data:{},
        isArray: false},
      uploadRootFoldersFromCsv: {
        method: 'POST',
        url: configuration.root_folder_upload_url,
        params: {id: '@id'},
        timeout:680000,
        headers: {'content-type': undefined},//
        //   responseType: "arraybuffer",
        transformRequest: angular.identity,
      },
      validateUploadRootFoldersFromCsv: {
        method: 'POST',
        url: configuration.root_folder_validate_upload_url,
        params: {maxErrorRowsReport: '@maxErrorRowsReport'},
        headers: {'content-type': undefined},//
        //   responseType: "arraybuffer",
        transformRequest: angular.identity,
      },
      uploadExcludedFolderRulesFromCsv: {
        method: 'POST',
        url: configuration.root_folder_excluded_rule_upload_url,
        params: {id: '@id'},
        headers: {'content-type': undefined},//
        //   responseType: "arraybuffer",
        transformRequest: angular.identity,
      },
      validateExcludedFolderRulesFromCsv: {
        method: 'POST',
        url: configuration.root_folder_excluded_rule_validate_upload_url,
        params: {maxErrorRowsReport: '@maxErrorRowsReport'},
        headers: {'content-type': undefined},//
        //   responseType: "arraybuffer",
        transformRequest: angular.identity,
      },
      updateRootFolder: {
        method: 'POST',
        url: configuration.root_folders_url+'/:id',
        params: {id: '@_id'},
        data:{},
        isArray: false},
      updateRootFolderRescanActive: {
        method: 'POST',
        url: configuration.root_folders_url+'/:id/active',
        params: {id: '@_id'},
        data:{},
        isArray: false},
      deleteRootFolder: {
        method: 'DELETE',
        url: configuration.root_folders_url+'/:id',
        params: {id: '@_id'},
        isArray: false},
      deleteRootFolderForce: {
        method: 'DELETE',
        url: configuration.delete_root_folder_force_url,
        params: {id: '@_id'},
        isArray: false},
      });

    var factory = <IRootFoldersResource>{

      updateRootFolder: function (rootFolder:Operations.DtoRootFolder, successFunction:(t:Operations.DtoRootFolder) => void, errorFunction) {
        return res.updateRootFolder({id:rootFolder.id},rootFolder,
            function (t:Operations.DtoRootFolder) {

              successFunction(t);

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to updateRootFolder " + error);
            }
        )
      },
      updateRootFolderRescanActive: function (rootFolderId:number,active:boolean,successFunction: (t:Operations.DtoRootFolder) => void , errorFunction) {
        return res.updateRootFolderRescanActive({id:rootFolderId},active,
            function (t:Operations.DtoRootFolder) {

              successFunction(t);

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to updateRootFolderRescanActive " + error);
            }
        )
      },
      deleteRootFolder: function (rootFolderId, successFunction:() => void, errorFunction) {
        return res.deleteRootFolder({id:rootFolderId},
            function () {

                successFunction();

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to deleteRootFolder " + error);
            }
        )
      },
      deleteRootFolderForce: function (rootFolderId, successFunction:() => void, errorFunction) {
        return res.deleteRootFolderForce({id:rootFolderId},
          function () {

            successFunction();

          }
          , function (error) {
            errorFunction(error);
            console.log("failed to deleteRootFolderForce  " + error);
          }
        )
      },
      addNewRootFolder: function (rootFolder:Operations.DtoRootFolder,scheduleGroupId,successFunction:(t:Operations.DtoRootFolder) => void, errorFunction) {
        return res.addNewRootFolder({scheduleGroupId: scheduleGroupId},rootFolder,
            function (t:Operations.DtoRootFolder) {

                successFunction(t);

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to addNewRootFolder " + error);
            }
        )
      },
      uploadRootFoldersFromCsv: function (fileData,updateDuplicates:boolean ,createStubs:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
        return res.uploadRootFoldersFromCsv({updateDuplicates:updateDuplicates,createStubs:createStubs},fileData,
          function (importResult:Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to uploadRootFoldersFromCsv " + error);
          }
        )
      },
      validateUploadRootFoldersFromCsv: function (fileData,maxErrorRowsReport:number,createStubs:boolean, successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
        return res.validateUploadRootFoldersFromCsv({maxErrorRowsReport:maxErrorRowsReport,createStubs:createStubs},fileData,
          function (importResult:Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to  validate uploadRootFoldersFromCsv " + error);
          }
        )
      },
      getRootFolders: function (successFunction:(t:Operations.DtoRootFolder[]) => void, errorFunction) {
        return res.getRootFolders(
            function (t:Entities.DTOQueryEntityResult<Operations.DtoRootFolder>) {
              if(t) {
                successFunction(t.content);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to getRootFolders " + error);
            }
        )
      },
      getRootFolder: function (rootFolderId,successFunction:(t:Operations.DtoRootFolder) => void, errorFunction) {
        return res.getRootFolder({id:rootFolderId},
            function (t:Operations.DtoRootFolder) {
              if(t) {
                successFunction(t);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to getRootFolder " + error);
            }
        )
      },
      getRootFolderScheduleGroup: function (rootFolderId,successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
        return res.getRootFolderScheduleGroup({rootFolderId:rootFolderId},
            function (t:Entities.DTOQueryEntityResult< Operations.DtoScheduleGroup>) {
              if(t) {
                successFunction(t.content[0]);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to getRootFolderScheduleGroup " + error);
            }
        )
      },
      setRootFolderScheduleGroup: function (rootFolderId,scheduleGroupId,successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
        return res.setRootFolderScheduleGroup({rootFolderId:rootFolderId,scheduleGroupId:scheduleGroupId},
            function (t:Operations.DtoScheduleGroup) {
              if(t) {
                successFunction(t);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to setRootFolderScheduleGroup " + error);
            }
        )
      },
      uploadExcludedFolderRulesFromCsv: function (fileData,updateDuplicates,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
        return res.uploadExcludedFolderRulesFromCsv({updateDuplicates:updateDuplicates},fileData,
          function (importResult:Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to uploadRootFoldersFromCsv " + error);
          }
        )
      },
      validateUploadExcludedFolderRulesFromCsv: function (fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
        return res.validateUploadExcludedFolderRulesFromCsv({maxErrorRowsReport:maxErrorRowsReport},fileData,
          function (importResult:Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to  validate uploadRootFoldersFromCsv " + error);
          }
        )
      },

      getExcludedFolderRules: function (rootFolderId,maxElementCount:number,successFunction:(excludedFolders:Operations.DtoFolderExcludeRule[],totalElements:number) => void, errorFunction) {
        return res.getExcludedFolderRules({rootFolderId:rootFolderId,take:maxElementCount},
            function (f:Entities.DTOQueryEntityResult<Operations.DtoFolderExcludeRule>) {
              if(f) {
                successFunction(f.content,f.numberOfElements);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get getAclReads" + error);
            }
        )
      },
      addNewExcludedFolderRule(rootFolderId,excludedFolder:Operations.DtoFolderExcludeRule,successFunction: (t:Operations.DtoFolderExcludeRule) => void , errorFunction) {
        return res.addNewExcludedFolderRule({rootFolderId:rootFolderId},excludedFolder,
            function (t:Operations.DtoFolderExcludeRule) {

              successFunction(t);

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to addNewRootFolderSchedule " + error);
            }
        )
      },
      updateExcludedFolderRule(rootFolderId,excludedFolder:Operations.DtoFolderExcludeRule,successFunction: (t:Operations.DtoFolderExcludeRule) => void , errorFunction) {
        return res.updateExcludedFolderRule({rootFolderId:rootFolderId,id:excludedFolder.id},excludedFolder,
            function (t:Operations.DtoFolderExcludeRule) {

              successFunction(t);

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to updateExcludedFolderRule " + error);
            }
        )
      },
      deleteExcludedFolderRule(rootFolderId,excludedFolderId:number,successFunction: () => void , errorFunction) {
        return res.deleteExcludedFolderRule({rootFolderId:rootFolderId,id:excludedFolderId},null,
            function () {

              successFunction();

            }
            , function (error) {
              errorFunction(error);
              console.log("failed to deleteRootFolderSchedule " + error);
            }
        )
      },

    };

    return factory;
  })
  .factory( 'scheduleGroupResource', function( $resource,configuration:IConfig ) {
     var res = $resource(configuration.scheduleGroups_url , "", {
       getScheduleGroups: {
         method: 'Get',
         url: configuration.scheduleGroups_url,
         isArray: false},
       getScheduleGroup: {
         method: 'Get',
         url: configuration.scheduleGroup_url,
         params: {'scheduleGroupId':'@scheduleGroupId'},
         isArray: false},
       addNewScheduleGroup: {
         method: 'PUT',
         url: configuration.scheduleGroups_url,
         isArray: false},
       updateScheduleGroup: {
         method: 'POST',
         params: {'scheduleGroupId':'@scheduleGroupId'},
         url: configuration.editRemove_scheduleGroup_url,
         isArray: false},
       uploadScheduleGroupsFromCsv: {
         method: 'POST',
         url: configuration.scheduleGroups_upload_url,
         params: {id: '@id'},
         headers: {'content-type': undefined},//
         //   responseType: "arraybuffer",
         transformRequest: angular.identity,
       },
       validateUploadScheduleGroupsFromCsv: {
         method: 'POST',
         url: configuration.scheduleGroups_validate_upload_url,
         params: {maxErrorRowsReport: '@maxErrorRowsReport'},
         headers: {'content-type': undefined},//
         //   responseType: "arraybuffer",
         transformRequest: angular.identity,
       },
       updateScheduleGroupRescanActive: {
         method: 'POST',
         params: {'scheduleGroupId':'@scheduleGroupId'},
         url: configuration.editRemove_scheduleGroup_url+'/active',
         isArray: false},
       deleteScheduleGroup: {
         method: 'DELETE',
         url: configuration.editRemove_scheduleGroup_url,
         params: {scheduleGroupId: '@scheduleGroupId'},
         isArray: false},
        startRunForScheduleGroups: {
          method: 'POST',
          params: {'scheduleGroupIds':'@scheduleGroupIds','op':'@op'},
          url: configuration.scheduleGroup_start_crawl_url,
          isArray: true},
        stopRunForScheduleGroup: {
          method: 'POST',
          params: {'scheduleGroupId':'@scheduleGroupId'},
          url: configuration.scheduleGroup_stop_crawl_url,
          isArray: true},

      });

      var factory = <IScheduleGroupResource>{
        getScheduleGroups: function (successFunction:(t:Operations.DtoScheduleGroup[]) => void, errorFunction) {
          return res.getScheduleGroups(
              {
                take:99,
                page:1,
                pageSize:99
              },
              function (t:Entities.DTOQueryEntityResult<Operations.DtoScheduleGroup>) {
                if(t) {
                  successFunction(t.content);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to getRootFolderSchedulesList " + error);
              }
          )
        },
        getScheduleGroup: function (scheduleGroupId,successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
          return res.getScheduleGroup({scheduleGroupId:scheduleGroupId},
              function (t:Operations.DtoScheduleGroup) {
                if(t) {
                  successFunction(t);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to getRootFolderSchedulesList " + error);
              }
          )
        },
        addNewScheduleGroup: function (schedule:Operations.DtoScheduleGroup, successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
            return res.addNewScheduleGroup(null,schedule,
              function (t:Operations.DtoScheduleGroup) {

                successFunction(t);

              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateRootFolderSchedule " + error);
              }
          )
        },
        uploadScheduleGroupsFromCsv: function (fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
          return res.uploadScheduleGroupsFromCsv({updateDuplicates:updateDuplicates},fileData,
            function (importResult:Operations.DTOImportSummaryResult) {
              successFunction(importResult);
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to uploadRootFoldersFromCsv " + error);
            }
          )
        },
        validateUploadScheduleGroupsFromCsv: function (fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
          return res.validateUploadScheduleGroupsFromCsv({maxErrorRowsReport:maxErrorRowsReport},fileData,
            function (importResult:Operations.DTOImportSummaryResult) {
              successFunction(importResult);
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to  validate uploadRootFoldersFromCsv " + error);
            }
          )
        },
        updateScheduleGroup: function (schedule:Operations.DtoScheduleGroup, successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
          return res.updateScheduleGroup({scheduleGroupId:schedule.id},schedule,
              function (t:Operations.DtoScheduleGroup) {

                successFunction(t);

              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateScheduleGroup " + error);
              }
          )
        },
        updateScheduleGroupRescanActive: function (scheduleId:number,active:boolean, successFunction:(t:Operations.DtoScheduleGroup) => void, errorFunction) {
          return res.updateScheduleGroupRescanActive({scheduleGroupId:scheduleId},active,
              function (t:Operations.DtoScheduleGroup) {

                successFunction(t);

              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateScheduleGroupRescanActive " + error);
              }
          )
        },
        deleteScheduleGroup: function (scheduleGroupId,successFunction:() => void, errorFunction) {
           return res.deleteScheduleGroup({scheduleGroupId:scheduleGroupId},null,
              function () {

                successFunction();

              }
              , function (error) {
                errorFunction(error);
                console.log("failed to deleteScheduleGroup " + error);
              }
          )
        },
        startRunForScheduleGroups: function (scheduleGroupIds:number[],isScan:boolean,isCompleteScanNoExtract:boolean,isCompleteScanWithExtract:boolean,isExtractOnly:boolean, successFunction:() => void, errorFunction) {
          var scheduleIds =scheduleGroupIds? scheduleGroupIds.join():null;
          var op;
          if(isCompleteScanNoExtract)
          {
            op='ingest,analyze';
          }
          else  if(isScan)
          {
            op='scan,analyze';
          }
          else  if(isCompleteScanWithExtract)
          {
            op='ingest,analyze,extract';
          }
          else  if(isExtractOnly)
          {
            op='extract';
          }
          else {
            op='scan,ingest,analyze,extract';
          }

          return res.startRunForScheduleGroups({scheduleGroupIds:scheduleIds,op:op},null,
              function () {

                successFunction();
              }
              ,  function(error) {
                errorFunction(error);
                console.log("failed to startRunForScheduleGroup " + error);
              }
          )
        },
        stopRunForScheduleGroup: function (scheduleGroupId,successFunction:() => void, errorFunction) {
          return res.stopRunForScheduleGroup({scheduleGroupId:scheduleGroupId},
              function () {

                successFunction();

              }
              , function (response,error) {
                errorFunction(error);
                console.log("failed to stopRunForScheduleGroup " + error);
              }
          )
        },

      }
      return factory;
    })

  .factory( 'departmentResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.department_url , "", {
      getDepartments: {
        method: 'Get',
        url: configuration.department_url + '/list',
        isArray: false},
      getDepartment: {
        method: 'GET',
        url: configuration.department_url+'/:id',
        params: {id: '@_id'},
        data:{},
        isArray: false},
      addNewDepartment: {
        method: 'PUT',
        url: configuration.department_url,
        isArray: false},
      updateDepartment: {
        method: 'POST',
        url: configuration.department_url,
        params: {'departmentId':'@departmentId'},
        isArray: false},
      deleteDepartment: {
        method: 'DELETE',
        url: configuration.department_url,
        params: {'departmentId': '@departmentId'},
        isArray: false},
      addDepartment: {
        method: 'PUT',
        url: configuration.department_url+'/:id/associate/folder/:folderId',
        params: {id: '@id', folderId: '@folderId'},
        data:{},
        isArray: false},
      removeDepartment: {
        method: 'DELETE',
        url: configuration.department_url+'/:id/associate/folder/:folderId',
        params: {id: '@id', folderId: '@folderId'},
        data:{},
        isArray: false}
    });
    var factory = <IDepartmentResource>{
      getDepartments: function (successFunction:(t:Operations.DtoDepartment[]) => void, errorFunction) {
        return res.getDepartments(
          {
            take:99,
            page:1,
            pageSize:99
          },
          function (t:Entities.DTOQueryEntityResult<Operations.DtoDepartment>) {
            if(t) {
              successFunction(t.content);
            }
            else {
              errorFunction(null);
            }
          }, function (error) {
            errorFunction(error);
            console.log("failed to get department list " + error);
          }
        )
      },
      getDepartment: function (departmentId,successFunction:(t:Operations.DtoDepartment) => void, errorFunction) {
        return res.getDepartment({id:departmentId},
          function (t:Operations.DtoDepartment) {
            if(t) {
              successFunction(t);
            }
            else {
              errorFunction(null);
            }
          }, function (error) {
            errorFunction(error);
            console.log("failed to get department " + error);
          }
        )
      },
      addNewDepartment: function (department:Operations.DtoDepartment, successFunction:(t:Operations.DtoDepartment) => void, errorFunction) {
        return res.addNewDepartment(null,department,
          function (t:Operations.DtoDepartment) {
            successFunction(t);
          }, function (error) {
            errorFunction(error);
            console.log("failed to update department " + error);
          }
        )
      },
      updateDepartment: function (department:Operations.DtoDepartment, successFunction:(t:Operations.DtoDepartment) => void, errorFunction) {
        return res.updateDepartment({departmentId:department.id},department,
          function (t:Operations.DtoDepartment) {
            successFunction(t);
          }, function (error) {
            errorFunction(error);
            console.log("failed to update department " + error);
          }
        )
      },
      deleteDepartment: function (departmentId,successFunction:() => void, errorFunction) {
        return res.deleteDepartment({departmentId:departmentId},null,
          function () {
            successFunction();
          }, function (error) {
            errorFunction(error);
            console.log("failed to delete department " + error);
          }
        )
      },
      addDepartment(departmentId, folderId) {
        return res.addDepartment({id:departmentId, folderId: folderId},
          function () {}, function (error) {
            //errorFunction(error);
            console.log("failed to get department " + error);
          }
        )
      },
      removeDepartment(departmentId, folderId) {
        return res.removeDepartment({id:departmentId, folderId: folderId},
          function () {}, function (error) {
            //errorFunction(error);
            console.log("failed to get department " + error);
          }
        )
      }
    };
    return factory;
  })

  .factory( 'remoteUserStorageResource', function( $resource,configuration:IConfig ) {
  var res = $resource(configuration.user_saved_data_url , "", {
    setUserData: {
      method: 'PUT',
      params: {key: '@key'},
      headers:{'Content-Type':'application/text'},
      isArray: false},
    deleteUserData: {
      method: 'DELETE',
      params: {key: '@key'},
      isArray: false},
    getUserData: {
      method: 'GET',
      params:{key: '@key'},
      isArray: false
    },
  });

  var factory = <IUserStorageService>{
    setUserData: function (moduleName:string,key,data,successFunction:() => void, errorFunction) {
      return res.setUserData({key:key}, JSON.stringify(data),
          function () {

            successFunction();
          }
          ,  function(error) {
            errorFunction(error);
              console.log("failed to setUserData " + error);
            }
      )
    },
    deleteUserData: function (moduleName:string,key,successFunction:() => void, errorFunction) {
      return res.deleteUserData({key:key},
          function () {

            successFunction();

          }
          , function (response,error) {
            errorFunction(error);
            console.log("failed to deleteUserData " + error);
          }
      )
    },
    getUserData: function (moduleName:string,key,successFunction:(data:any) => void, errorFunction) {
      return res.getUserData({key:key},
          function (userData:Operations.UserSavedData) {
            if(userData) {
              if (userData.data) {
                try {
                  let ans = JSON.parse(userData.data)
                  successFunction(ans);
                }
                catch (error) {
                  errorFunction(error);
                }
              }
              else {
                successFunction(null);
              }
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get user data " + error);
          }
      )
    },
  };
  return factory;
})
  .factory( 'sharePointConnectionResource', function( $resource,configuration:IConfig ) {

     var factory = new MediaConnectionResource<Operations.DtoSharePointConnectionDetails>($resource,configuration,Operations.EMediaType.SHARE_POINT);
    return factory;
})
  .factory( 'oneDriveConnectionResource', function( $resource,configuration:IConfig ) {
    var factory = new MediaConnectionResource<Operations.DtoOneDriveConnectionDetails>($resource,configuration,Operations.EMediaType.ONE_DRIVE);
    return factory;
  })
  .factory( 'mipConnectionResource', function( $resource,configuration:IConfig ) {
    var factory = new MediaConnectionResource<Operations.DtoMipConnectionDetails>($resource,configuration,Operations.EMediaType.MIP);
    return factory;
  })
  .factory( 'boxConnectionResource', function( $resource,configuration:IConfig ) {
    var factory = new MediaConnectionResource<Operations.DtoBoxConnectionDetails>($resource,configuration,Operations.EMediaType.BOX);
    return factory;
  })
  .factory( 'exchange365ConnectionResource', function( $resource,configuration:IConfig ) {
    var factory = new MediaConnectionResource<Operations.DtoExchange365ConnectionDetails>($resource,configuration,Operations.EMediaType.EXCHANGE365);
    return factory;
  })
   .factory( 'customerDataCenterResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.customerDataCenter_url , "", {
        getDataCenter: {
          method: 'Get',
          url: configuration.customerDataCenter_url,
          params: {'id':'@id'},
          isArray: false},
        getDataCenters: {
          method: 'Get',
          url: configuration.customerDataCenters_url,
          isArray: false},
        addDataCenter: {
          method: 'PUT',
          url: configuration.add_customerDataCenter_url,
          isArray: false},
        updateDataCenter: {
          method: 'POST',
          params: {'id':'@id'},
          url: configuration.editRemove_customerDataCenter_url,
          isArray: false},
        deleteDataCenter: {
          method: 'DELETE',
          url: configuration.editRemove_customerDataCenter_url,
          params: {id: '@id'},
          isArray: false},

  });

  var factory = <ICustomerDataCenterResource>{
    getDataCenters: function (successFunction:(dataCenters:Operations.DtoCustomerDataCenter[]) => void, errorFunction) {
      return res.getDataCenters(
        function (t:Entities.DTOQueryEntityResult<Operations.DtoCustomerDataCenter>) {
          if(t) {
            successFunction(t.content);
          }
          else
          {
            errorFunction(null);
          }
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to getDataCenters " + error);
        }
      )
    },
    getDataCenter: function (dataCenterId,successFunction:(t:Operations.DtoCustomerDataCenter) => void, errorFunction) {
      return res.getDataCenter({id:dataCenterId},
        function (t:Operations.DtoCustomerDataCenter) {
          if(t) {
            successFunction(t);
          }
          else
          {
            errorFunction(null);
          }
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to getDataCenter " + error);
        }
      )
    },
    addDataCenter: function (dataCenter:Operations.DtoCustomerDataCenter, successFunction:(t:Operations.DtoCustomerDataCenter) => void, errorFunction) {
      return res.addDataCenter(null,dataCenter,
        function (t:Operations.DtoCustomerDataCenter) {

          successFunction(t);

        }
        , function (error) {
          errorFunction(error);
          console.log("failed to addDataCenter " + error);
        }
      )
    },
    updateDataCenter: function (dataCenter:Operations.DtoCustomerDataCenter, successFunction:(t:Operations.DtoCustomerDataCenter) => void, errorFunction) {
      return res.updateDataCenter({id:dataCenter.id},dataCenter,
        function (t:Operations.DtoCustomerDataCenter) {

          successFunction(t);

        }
        , function (error) {
          errorFunction(error);
          console.log("failed to updateDataCenter " + error);
        }
      )
    },
    deleteDataCenter: function (dataCenterId,successFunction:() => void, errorFunction) {
      return res.deleteDataCenter({id:dataCenterId},null,
        function () {

          successFunction();

        }
        , function (error) {
          errorFunction(error);
          console.log("failed to deleteDataCenter " + error);
        }
      )
    },

  }
  return factory;
})

  .factory('systemSettingsResource', function ($resource, configuration) {
    var res = $resource(configuration.get_system_settings, "", {
      getSystemSettings: {
        method: 'GET',
        url: configuration.get_system_settings,
        isArray: true
      },
      getSystemSettingsByName: {
        method: 'GET',
        params: { 'name': '@name' },
        url: configuration.get_system_settings_by_name,
        isArray: true
      }
    });
    var factory = {
      getSystemSettings: function (successFunction, errorFunction) {
        return res.getSystemSettings(function (t) {
          successFunction(t);
        }, function (error) {
          errorFunction(error);
          console.log("failed to getSystemSettings " + error);
        });
      },
      getSystemSettingsByName: function (name, successFunction, errorFunction) {
        return res.getSystemSettingsByName({name : name}, function (t) {
          successFunction(t);
        }, function (error) {
          errorFunction(error);
          console.log("failed to getSystemSettingsByName " + error);
        });
      },
    };
    return factory;
  })
   .factory( 'serverComponentResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.editServerComponent_url , "", {

        addServerComponent: {
          method: 'PUT',
          params: {'id':'@id'},
          url: configuration.addServerComponent_url,
          isArray: false
        },
        getServerComponent: {
          method: 'GET',
          params: {'id':'@id'},
          url: configuration.editServerComponent_url,
          isArray: false
        },
        updateServerComponent: {
          method: 'POST',
          params: {'id':'@id'},
          url: configuration.editServerComponent_url,
          isArray: false
        },
        deleteServerComponent: {
          method: 'DELETE',
          params: {'id':'@id'},
          url: configuration.editServerComponent_url,
          isArray: false
        },
        getServerComponents: {
          method: 'GET',
          url: configuration.server_components_list_url,
          isArray: false
        },


  });

  var factory = <IServerComponentResource>{
    getServerComponents: function ( successFunction:(t:Operations.DtoClaComponent[]) => void, errorFunction) {
      return res.getServerComponents({take:1000},
        function (t:Entities.DTOQueryEntityResult< Operations.DtoClaComponent>) {

          successFunction(t.content);

        }
        , function (error) {
          errorFunction(error);
          console.log("failed to getServerComponents " + error);
        }
      )
    },
    addServerComponent: function (serverComponent:Operations.DtoClaComponent, successFunction:(t:Operations.DtoClaComponent) => void, errorFunction) {
      return res.addServerComponent(serverComponent,
        function (t:Operations.DtoClaComponent) {
          successFunction(t);
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to addServerComponent " + error);
        }
      )
    },
    getServerComponent: function (serverComponentId:any, successFunction:(t:Operations.DtoClaComponent) => void, errorFunction) {
      return res.getServerComponent({id : serverComponentId},
        function (t:Operations.DtoClaComponent) {
          successFunction(t);
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to getServerComponent " + error);
        }
      )
    },

    updateServerComponent: function (component:Operations.DtoClaComponent, successFunction:(t:Operations.DtoClaComponent) => void, errorFunction) {
      return res.updateServerComponent({id:component.id},component,
        function (t:Operations.DtoClaComponent) {

          successFunction(t);

        }
        , function (error) {
          errorFunction(error);
          console.log("failed to updateServerComponent " + error);
        }
      )
    },
    deleteServerComponent: function (serverComponentId,successFunction:() => void, errorFunction) {
      return res.deleteServerComponent({id:serverComponentId},null,
        function () {
          successFunction();
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to delete serverComponent " + error);
        }
      )
    },
  }
  return factory;
})
  .factory( 'ldapConnectionResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.ldap_connections_url , "", {

      getConnections: {
        method: 'Get',
        url: configuration.ldap_connections_url,
        isArray: true
      },
      getConnection: {
        method: 'Get',
        params: {id: '@_id'},
        url: configuration.ldap_connections_url,
        isArray: false
      },
      addNewConnection: {
        method: 'POST',
        params: {},
        url: configuration.ldap_connections_url,
        isArray: false
      },
      updateConnection: {
        method: 'PUT',
        params: {'id': '@id'},
        url: configuration.editRemove_ldap_connections_url,
        isArray: false
      },
      deleteConnection: {
        method: 'DELETE',
        url: configuration.editRemove_ldap_connections_url,
        params: {id: '@id'},
        isArray: false
      },
      testConnection: {
        url: configuration.ldap_settings_test_connection_url,
        method: 'POST',
        isArray: false
      },
      updateLdapEnableState: {
        method: 'PUT',
        url: configuration.ldap_connections_url+'/:id/enabled',
        params: {id: '@_id'},
        data:{},
        isArray: false
     },
      uploadConnectionsFromCsv: {
        method: 'POST',
        url: configuration.ldap_connection_upload_url,
        params: {id: '@id'},
        headers: {'content-type': undefined},
        transformRequest: angular.identity,
      },
      validateUploadConnectionsFromCsv: {
        method: 'POST',
        url: configuration.ldap_connection_validate_upload_url,
        params: {maxErrorRowsReport: '@maxErrorRowsReport'},
        headers: {'content-type': undefined},
        transformRequest: angular.identity,
      },
    });

    var factory = <ILdapConnectionResource>{

      getConnections: function (successFunction:(t:Operations.DtoLdapConnectionDetails[]) => void, errorFunction) {
        return res.getConnections(
          function (t:Entities.DTOQueryEntityResult<Operations.DtoLdapConnectionDetails>) {
            successFunction(t.content);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to getServerComponents " + error);
          }
        )
      },
      getConnection: function ( connectionId:number, successFunction:(t:Operations.DtoLdapConnectionDetails) => void, errorFunction) {
        return res.getConnection({id : connectionId},
          function (t:Operations.DtoLdapConnectionDetails) {
            successFunction(t);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to getServerComponents " + error);
          }
        )
      },
      addNewConnection: function (connection:Operations.DtoLdapConnectionDetails, successFunction:(t:Operations.DtoLdapConnectionDetails) => void, errorFunction) {
        return res.addNewConnection(null,connection,
          function (t:Operations.DtoLdapConnectionDetails) {
            successFunction(t);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to addNewLdapConnection " + error);
          }
        )
      },
      updateConnection: function (ldapConnection:any, successFunction:(t:Operations.DtoLdapConnectionDetails) => void, errorFunction) {
        return res.updateConnection({id : ldapConnection.id}, ldapConnection,
          function (t:Operations.DtoLdapConnectionDetails) {
            successFunction(t);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to updateLdapConnection " + error);
          }
        )
      },
      deleteConnection: function (ldapConnectionId:any,successFunction:() => void, errorFunction) {
        return res.deleteConnection({id:ldapConnectionId},null,
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to delete ldapconnection " + error);
          }
        )
      },
      testConnection: function (ldapConnection:Operations.DtoLdapConnectionDetails, successFunction:(success:boolean,reason:string) => void, errorFunction) {
        return res.testConnection(null,ldapConnection,
          function (result:DTORequestBase) {
            if(result) {
              successFunction(result.result,result.reason);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      updateLdapEnableState: function (connectionId:number,enabled:boolean,successFunction: (success:boolean,reason:string) => void , errorFunction) {
        return res.updateLdapEnableState({id:connectionId},enabled,
          function (success:boolean,reason:string) {
            successFunction(success, reason);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to updateLdapEnableState " + error);
          }
        )
      },
      uploadConnectionsFromCsv: function (fileData, updateDuplicates: boolean, successFunction: (importResult: Operations.DTOImportSummaryResult) => void, errorFunction) {
        return this.res.uploadConnectionsFromCsv({updateDuplicates: updateDuplicates}, fileData,
          function (importResult: Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to uploadRootFoldersFromCsv " + error);
          }
        )
      },
      validateUploadConnectionsFromCsv: function (fileData, maxErrorRowsReport: number, successFunction: (importResult: Operations.DTOImportSummaryResult) => void, errorFunction) {
        return this.res.validateUploadConnectionsFromCsv({maxErrorRowsReport: maxErrorRowsReport}, fileData,
          function (importResult: Operations.DTOImportSummaryResult) {
            successFunction(importResult);
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to  validate uploadRootFoldersFromCsv " + error);
          }
        )
      }
    };
    return factory;
  });

class MediaConnectionResource<T extends Operations.DtoMediaTypeConnectionDetails> implements IMediaTypeConnectionResource<T>{
  private res;
  constructor($resource,configuration,private mediaType:Operations.EMediaType){
    this.res = this.createResource($resource,configuration,mediaType);
  }
  createResource ($resource,configuration:IConfig,mediaType:Operations.EMediaType){

    let mediaTypeStr = 'box';

    switch(mediaType) {
      case Operations.EMediaType.ONE_DRIVE:
        mediaTypeStr = 'onedrive';
        break;
      case Operations.EMediaType.SHARE_POINT:
        mediaTypeStr = 'sharepoint';
        break;
      case Operations.EMediaType.EXCHANGE365:
        mediaTypeStr = 'exchange365';
        break;
      case Operations.EMediaType.MIP:
        mediaTypeStr = 'mip';
        break;
    }

    return $resource(configuration.media_connections_by_type_url, "", {

        getConnections: {
          method: 'Get',
          params: {mediaType: '@mediaType'},
          url: configuration.media_connections_by_type_url,
          isArray: true
        },
        getConnection: {
          method: 'Get',
          params: {id: '@_id', mediaType: mediaTypeStr},
          url: configuration.editRemove_media_connection_url,
          isArray: false
        },
        addNewConnection: {
          method: 'POST',
          params: {mediaType: mediaTypeStr},
          url: configuration.add_media_connection_url,
          data: {},
          isArray: false
        },
        updateConnection: {
          method: 'POST',
          url: configuration.editRemove_media_connection_url,
          params: {id: '@_id', mediaType: mediaTypeStr},
          data: {},
          isArray: false
        },
        uploadConnectionsFromCsv: {
          method: 'POST',
          url: configuration.connection_upload_url,
          params: {id: '@id'},
          headers: {'content-type': undefined},//
          //   responseType: "arraybuffer",
          transformRequest: angular.identity,
        },
        validateUploadConnectionsFromCsv: {
          method: 'POST',
          url: configuration.connection_validate_upload_url,
          params: {maxErrorRowsReport: '@maxErrorRowsReport'},
          headers: {'content-type': undefined},//
          //   responseType: "arraybuffer",
          transformRequest: angular.identity,
        },
        deleteConnection: {
          method: 'DELETE',
          url: configuration.editRemove_connection_url,
          params: {id: '@_id'},
          isArray: false
        },
        testConnection: {
          method: 'POST',
          params: {mediaType: mediaTypeStr},
          url: configuration.test_media_connection_url,
          isArray: false
        },
      testConnectionWithDatacenter: {
          method: 'POST',
          params: {mediaType: mediaTypeStr, customerDataCenterId: '@customerDataCenterId'},
          url: configuration.test_media_connection_to_datacenter_url,
          isArray: false
        },
      testConnectionWithDatacenterWithSavedConnection: {
        method: 'POST',
        params: {mediaType: mediaTypeStr, customerDataCenterId: '@customerDataCenterId', connectionId:'@connectionId'},
        url: configuration.test_media_connection_saved_to_datacenter_url,
        isArray: false
      },
      testBoxConnectionWithDatacenterWithSavedConnection: {
        method: 'POST',
        params: {mediaType: mediaTypeStr, customerDataCenterId: '@customerDataCenterId'},
        url: configuration.test_box_connection_saved_to_datacenter_url,
        isArray: false
      },
      });

  }

  public getConnections =(successFunction:(t:T[]) => void, errorFunction)=> {
    let _localMediaType = this.mediaType;
    return this.res.getConnections({mediaType:this. mediaType.toString()},
      function (connections:Operations.DtoMediaTypeConnectionDetails[]) {
        if(connections) {
          if(_localMediaType== Operations.EMediaType.ONE_DRIVE)
          {
            let parsedConns =  connections.map(c=> {
           //   let conn = new Operations.DtoOneDriveConnectionDetails();
              try {
                (<Operations.DtoOneDriveConnectionDetails>c).sharePointConnectionParametersDto = JSON.parse(c.connectionParametersJson);
                return c;
              }
              catch (e) {
                 (<Operations.DtoOneDriveConnectionDetails>c).sharePointConnectionParametersDto = new Operations.DtoSharePointConnectionParameters();
                 return c;
              }
            });
            successFunction(<any[]>parsedConns);
          }
          if(_localMediaType== Operations.EMediaType.SHARE_POINT)
          {
            let parsedConns =  connections.map(c=> {
            //  let conn = new Operations.DtoSharePointConnectionDetails();
              try {
                (<Operations.DtoSharePointConnectionDetails>c).sharePointConnectionParametersDto = JSON.parse(c.connectionParametersJson);
                return c;
              }catch (e) {
                (<Operations.DtoSharePointConnectionDetails>c).sharePointConnectionParametersDto = new Operations.DtoSharePointConnectionParameters();
                return c;
              }
            });
            successFunction(<any[]>parsedConns);
          }
          if(_localMediaType== Operations.EMediaType.BOX)
          {
            let parsedConns =  connections.map(c=> {
              //let conn = new Operations.DtoBoxConnectionDetails();
              (<Operations.DtoBoxConnectionDetails>c).jwt = c.connectionParametersJson;
              return c;
            });
            successFunction(<any[]>parsedConns);
          }
          if(_localMediaType== Operations.EMediaType.EXCHANGE365)
          {
            let parsedConns =  connections.map(c=> {
              try {
                (<Operations.DtoExchange365ConnectionDetails>c).exchangeConnectionParametersDto = JSON.parse(c.connectionParametersJson);
                return c;
              }catch (e) {
                (<Operations.DtoExchange365ConnectionDetails>c).exchangeConnectionParametersDto = new Operations.DtoExchangeConnectionParameters();
                return c;
              }
            });
            successFunction(<any[]>parsedConns);
          }
          if(_localMediaType== Operations.EMediaType.MIP)
          {
            let parsedConns =  connections.map(c=> {
              //   let conn = new Operations.DtoOneDriveConnectionDetails();
              try {
                (<Operations.DtoMipConnectionDetails>c).mipconnectionParametersDto = JSON.parse(c.connectionParametersJson);
                return c;
              }
              catch (e) {
                (<Operations.DtoMipConnectionDetails>c).mipconnectionParametersDto = new Operations.DtoMipConnectionParameters();
                return c;
              }
            });
            successFunction(<any[]>parsedConns);
          }

          // let parsedConns = connections.map(c=>{
          //   if(c.mediaType == Operations.EMediaType.ONE_DRIVE){
          //    let conn = new Operations.DtoOneDriveConnectionDetails();
          //    conn.sharePointConnectionParametersDto = JSON.parse(c.connectionParametersJson);
          //    return conn;
          //   }
          //   if(c.mediaType == Operations.EMediaType.SHARE_POINT){
          //     let conn = new Operations.DtoSharePointConnectionDetails();
          //     conn.sharePointConnectionParametersDto = JSON.parse(c.connectionParametersJson);
          //     return conn;
          //   }
          //   if(c.mediaType == Operations.EMediaType.BOX){
          //     let conn = new Operations.DtoBoxConnectionDetails();
          //     conn.jwt =  c.connectionParametersJson;
          //     return conn;
          //   }
          // });
         // successFunction(<any[]>parsedConns);
        }
        else
        {
          errorFunction(null);
        }
      }
      , function (error) {
        errorFunction(error);
        console.log("failed to getConnections " + error);
      }
    )
  };
  public getConnection= (id,successFunction:(t:T) => void, errorFunction) =>{
    return this.res.getConnection({id:id},
      function (t:T) {
        if(t) {
          // if(!t.sharePointConnectionParametersDto)
          // {
          //   t.sharePointConnectionParametersDto = new Operations.DtoSharePointConnectionParameters();
          // }
          successFunction(t);
        }
        else
        {
          errorFunction(null);
        }
      }
      , function (error) {
        errorFunction(error);
        console.log("failed to getConnections " + error);
      }
    )
  };
  public uploadConnectionsFromCsv =(fileData,updateDuplicates:boolean, successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) =>{
    return this.res.uploadConnectionsFromCsv({updateDuplicates:updateDuplicates},fileData,
      function (importResult:Operations.DTOImportSummaryResult) {
        successFunction(importResult);
      }
      , function (error) {
        errorFunction(error);
        console.log("failed to uploadRootFoldersFromCsv " + error);
      }
    )
  };
  public validateUploadConnectionsFromCsv =(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction)=> {
    return this.res.validateUploadConnectionsFromCsv({maxErrorRowsReport:maxErrorRowsReport},fileData,
      function (importResult:Operations.DTOImportSummaryResult) {
        successFunction(importResult);
      }
      , function (error) {
        errorFunction(error);
        console.log("failed to  validate uploadRootFoldersFromCsv " + error);
      }
    )
  };
  public updateConnection= (connection:T, successFunction:(t:T) => void, errorFunction) =>{
    return this.res.updateConnection({id:connection.id},connection,
      function (t:T) {

        successFunction(t);

      }
      , function (error) {
        errorFunction(error);
        console.log("failed to updateConnection " + error);
      }
    )
  };
  public deleteConnection= (connectionId, successFunction:(t:T) => void, errorFunction) =>{
    return this.res.deleteConnection({id:connectionId},
      function (t:T) {

        successFunction(t);

      }
      , function (error) {
        errorFunction(error);
        console.log("failed to deleteConnection " + error);
      }
    )
  };
  public addNewConnection= (connection:T,successFunction:(t:T) => void, errorFunction)=> {
    return this.res.addNewConnection(null,connection,
      function (t:T) {

        successFunction(t);

      }
      , function (error) {
        errorFunction(error);
        console.log("failed to addNewConnection " + error);
      }
    )};
  public testConnection= (connectionParams:string|Operations.DtoSharePointConnectionParameters,successFunction:(success:boolean,reason:string) => void, errorFunction)=> {
    return this.res.testConnection(null,connectionParams,
      function (result:DTORequestBase) {
        if(result) {
          successFunction(result.result,result.reason);
        }
        else {
          errorFunction();
        }

      }
      , function (error) {
        errorFunction(error);
        console.log("failed to testConnection " + error);
      }
    )
  };
  public testConnectionWithDatacenter= (connectionParams,dataCenterId:number,successFunction:(success:boolean,reason:string) => void, errorFunction)=> {
    return this.res.testConnectionWithDatacenter({customerDataCenterId:dataCenterId},connectionParams,
      function (result:DTORequestBase) {
        if(result) {
          successFunction(result.result,result.reason);
        }
        else {
          errorFunction();
        }

      }
      , function (error) {
        errorFunction(error);
        console.log("failed to testConnection " + error);
      }
    )
  };
  public testConnectionWithDatacenterWithSavedConnection= (connectionId:number,connectionParams,dataCenterId:number,successFunction:(success:boolean,reason:string) => void, errorFunction)=> {
    if (connectionParams.mediaType == EMediaType.BOX) {
      return this.res.testBoxConnectionWithDatacenterWithSavedConnection({
          customerDataCenterId: dataCenterId
        }, connectionParams,
        function (result: DTORequestBase) {
          if (result) {
            successFunction(result.result, result.reason);
          }
          else {
            errorFunction();
          }
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to testConnection " + error);
        }
      )
    }
    else {
      return this.res.testConnectionWithDatacenterWithSavedConnection({
          customerDataCenterId: dataCenterId,
          connectionId: connectionId
        }, connectionParams,
        function (result: DTORequestBase) {
          if (result) {
            successFunction(result.result, result.reason);
          }
          else {
            errorFunction();
          }
        }
        , function (error) {
          errorFunction(error);
          console.log("failed to testConnection " + error);
        }
      )
    }
  };
}

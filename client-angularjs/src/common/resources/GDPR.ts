
///<reference path='..\all.d.ts'/>
'use strict';
interface IGDPRErasureResource
{
   getErasureRequests(successFunction: (request:Entities.DTOFileTag[]) => void , errorFunction):void;
   getErasureRequest(erasureRequestId:number,successFunction: (request:Entities.DTOFileTag) => void , errorFunction):void;
   addErasureRequest(erasureRequest:GDPR.DtoErasureRequest,successFunction: (item:Entities.DTOFileTag) => void , errorFunction):void;
   removeErasureRequest(erasureRequestId:number,successFunction: () => void , errorFunction):void;
   editErasureRequest(bizErasureRequest:GDPR.DtoErasureRequest,successFunction: () => void , errorFunction):void;
  // updateAllErasureRequestsStatus(erasureRequestId:number,oldStatus:string,newStatus:string,successFunction: () => void , errorFunction):void;
  // updateErasureRequestStatus(erasureRequestId:number,bizErasureRequestId,oldStatus:string,newStatus:string,successFunction: (updatedItem) => void , errorFunction):void;
  // uploadErasureRequestsFromFile(erasureRequestId:number,fileData,successFunction: (importResult:Entities.DTOErasureRequestImportResult) => void , errorFunction):void;
  // cancelUploadErasureRequestsFromFile(erasureRequestId:number,importId):void;
  // updateUploadErasureRequestsStatus(erasureRequestId:number,importId:any,status:string,successFunction: () => void , errorFunction):void;
  // createErasureRequestsFromFolderNames(erasureRequestId:number,folderPath:string,level:number,successFunction: (importResult:Entities.DTOErasureRequestImportResult) => void , errorFunction):void;
}

angular.module('resources.GDPR.erasure', [
])

  .factory( 'GDPRErasureResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.GDPR_erasure_request_list, '', {
      getAllErasureRequests: {
        method: 'GET',
       isArray: true },
      getErasureRequest: {
        method: 'GET',
        url:configuration.GDPR_erasure_request,
        params: {id: '@id'},
        isArray: false },
      updateErasureRequest: {
        method: 'POST',
        url:configuration.GDPR_erasure_update_url,
        params: {id: '@id'},
        isArray: false },
      addNewErasureRequest: {
        method: 'PUT',
        url: configuration.GDPR_erasure_add_url,
        isArray: false },
      deleteErasureRequest: {
        method: 'DELETE',
        url: configuration.GDPR_erasure_delete_url,
        params: {id: '@id'},
        isArray: false },

    });


    var factory = <IGDPRErasureResource>{

      getErasureRequests: function (successFunction: (requests:Entities.DTOFileTag[]) => void , errorFunction) {
        return res.getErasureRequest(null,
            function (requests:Entities.DTOFileTag[]) {

              successFunction(requests);
            }
            , function (error) {
              errorFunction(error);
            }
        )


      },
      getErasureRequest: function (erasureRequestId:number,successFunction: (request:Entities.DTOFileTag) => void , errorFunction) {
        return res.getErasureRequest({id:erasureRequestId},
            function (request:Entities.DTOFileTag) {

              successFunction(request);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      editErasureRequest: function (erasureRequest:GDPR.DtoErasureRequest,successFunction:() => void, errorFunction) {
        return res.updateErasureRequest({id:erasureRequest.id},erasureRequest,
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      addErasureRequest: function (erasureRequest:GDPR.DtoErasureRequest,successFunction:(newErasureRequest:Entities.DTOFileTag) => void, errorFunction) {
        return res.addNewErasureRequest(null,JSON.stringify(erasureRequest),
          function (newErasureRequest) {
            successFunction(newErasureRequest);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      removeErasureRequest: function (erasureRequestId:number,successFunction:() => void, errorFunction) {
        return res.deleteErasureRequest({id:erasureRequestId},
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

    };

    return factory;
  })

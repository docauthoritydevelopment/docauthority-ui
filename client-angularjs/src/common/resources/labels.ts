
///<reference path='..\all.d.ts'/>
'use strict';

interface ILabelResource {
  applyLabel(params:any): void;
  applyLabelById(params:any): void;
  getLabels(successFunction: (t:Entities.DTOLabel[]) => void , errorFunction): void;
}
angular.module('resources.label', [
    ])

    .factory( 'labelResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.apply_label_url, '', {
        applyLabel: {
          method: 'PUT',
          url:configuration.apply_label_url,
          params: { baseFilterField: '@baseFilterField' , baseFilterValue : '@baseFilterValue' , filter : '@filter' },
          isArray: false },
        applyLabelById: {
          method: 'PUT',
          params: { baseFilterField: '@baseFilterField' , baseFilterValue : '@baseFilterValue' , filter : '@filter' , labelId: '@labelId'},
          url:configuration.apply_label_by_id_url,
          isArray: false },
        getLabels: {
          method: 'GET',
          url:configuration.get_labels_url,
          isArray: false }
      });
      var factory = <ILabelResource>{
        applyLabel: function (params:any) {
          return res.applyLabel(params,
            function (t:any) {

            }
            , function (error) {


            }
          )
        },
        applyLabelById: function (params:any) {
          return res.applyLabelById(params,
            function (t:any) {

            }
            , function (error) {


            }
          )
        },
        getLabels: function (successFunction:(t:Entities.DTOLabel[]) => void, errorFunction) {
          return res.getLabels(
            function (t:Entities.DTOQueryEntityResult<Entities.DTOLabel>) {
              if(t) {
                successFunction(t.content);
              }
              else {
                errorFunction(null);
              }
            }, function (error) {
              errorFunction(error);
              console.log("failed to get labels list " + error);
            }
          )
        },
      }
      return factory;
    });



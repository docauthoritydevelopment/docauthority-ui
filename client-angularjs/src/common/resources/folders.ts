
///<reference path='..\all.d.ts'/>
'use strict';

interface IFolderResource {

  getFolderById(folderId,successFunction: (g:Entities.DTOFolderInfo ) => void , errorFunction): void;

}

angular.module('resources.folders', [
    ])

    .factory( 'folderResource', function( $resource ,configuration:IConfig) {
      var res = $resource(configuration.folder_url, { id: '@id' }, {
        getFolderById:  {
          method: 'GET',
          url:configuration.folder_url,
          params: {id: '@id'},
          isArray: false
        },

      });

      var factory = <IFolderResource>{

        getFolderById: function (folderId,successFunction:(f:Entities.DTOFolderInfo) => void, errorFunction) {
          return res.getFolderById({id:folderId},
              function (f:Entities.DTOFolderInfo) {
                if(f) {
                  successFunction(f);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },

      };
      return factory;
    })

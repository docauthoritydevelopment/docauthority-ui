'use strict';

interface IUserTemplateRolesResource
{
  getTemplateRoles(templateType:Users.ETemplateType, successFunction:(templateRoles:Users.DtoTemplateRole[]) => void):any;
  getSystemRoles(successFunction:(roles:Users.DtoSystemRole[]) => void, errorFunction):any;
  addNewTemplateRole(templateRoleSummaryInfo, successFunction:(addedTemplateRole:Users.DtoTemplateRole)=>void, errorFunction):void;
  deleteTemplateRole(id, successFunction:()=>void, errorFunction):void;
  updateTemplateRole(templateRoleSummaryInfo, successFunction:(addedTemplateRole:Users.DtoTemplateRole)=>void, errorFunction):void;
}

angular.module('resources.templateRoles', [])
  .factory('templateRolesResource', function ($resource, configuration, propertiesUtils) {
    var res = $resource(configuration.templateRole_list_url, '', {
      getTemplateRoles: {
        method: 'GET',
        params: { type: '@type' },
        url: configuration.templateRole_list_url,
        isArray: false },
      addNewTemplateRole: {
        method: 'PUT',
        url: configuration.templateRole_add_url,
        params: { docTypeParentId: '@docTypeParentId' },
        isArray: false },
      updateTemplateRole: {
        method: 'POST',
        url: configuration.templateRole_edit_url,
        params: { id: '@id' },
        isArray: false },
      deleteTemplateRole: {
        method: 'DELETE',
        url: configuration.templateRole_delete_url,
        params: { id: '@id' },
        isArray: false },
      getSystemRoles: {
        method: 'GET',
        url:configuration.user_systemRole_list_url,
        isArray: true }
    });
    var factory = {
      getTemplateRoles: function (type:Users.ETemplateType, successFunction:(templateRoles:Users.DtoTemplateRole[]) => void, errorFunction) {
        return res.getTemplateRoles({ type: type ,page:1 , take:1000}, function (data:Entities.DTOQueryEntityResult<Users.DtoTemplateRole>) {
          if (data && data.content) {
            //sort: [{"dir":"asc","field":"name"}]
            data.content.sort(propertiesUtils.sortAlphbeticFun('name'));
            successFunction(data.content);
          }
          else {
            errorFunction(null);
          }
        }, function (error) {
          errorFunction(error);
          console.error("failed to getTemplateRoles " + error);
        });
      },
      addNewTemplateRole: function (templateRole, successFunction:(addedTemplateRole:Users.DtoTemplateRole)=>void, errorFunction) {
        return res.addNewTemplateRole(null, JSON.stringify(templateRole), function (addedTemplateRole) {
          successFunction(addedTemplateRole);
        }, function (error) {
          errorFunction(error);
          console.error("failed to addNewTemplateRole " + error);
        });
      },
      updateTemplateRole: function (templateRole, successFunction:(addedTemplateRole:Users.DtoTemplateRole)=>void, errorFunction) {
        return res.updateTemplateRole({ id: templateRole.id }, JSON.stringify(templateRole), function (addedTemplateRole) {
          successFunction(addedTemplateRole);
        }, function (error) {
          errorFunction(error);
          console.error("failed to updateTemplateRole " + error);
        });
      },
      deleteTemplateRole: function (id, successFunction:()=>void, errorFunction) {
        return res.deleteTemplateRole({ id: id }, null, function () {
          successFunction();
        }, function (error) {
          errorFunction(error);
        });
      },
      getSystemRoles: function (successFunction:(roles:Users.DtoSystemRole[]) => void, errorFunction) {
        return res.getSystemRoles(null,
          function (roles:Users.DtoSystemRole[]) {
            successFunction(roles);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      }
    };
    return factory;
  });


///<reference path='..\all.d.ts'/>
'use strict';

interface IPoliciesResource {
  getPolicyLayerTypes(successFunction:(policyTypes:Policy.DtoPolicyLayerType[]) => void, errorFunction): void;
  getPolicyItemsByLayer(layerId,successFunction:(policyItems:Policy.DtoPolicyLayerItemBase[]) => void, errorFunction): void;
  addNewPolicy(policyItem:Policy.DtoPolicyLayerItemBase,successFunction:(addedPolicyItem) => void , errorFunction): void;
  updatePolicy(policyItem:Policy.DtoPolicyLayerItemBase,successFunction:() => void, errorFunction): void;
  deletePolicy(policyItemId:string,successFunction:() => void, errorFunction): void;

  getPolicyObjectTypes(parentObjectTypeIds:number[],successFunction:(policyObjectTypes:Policy.DtoPolicyObjectType[]) => void, errorFunction): void;
  getPolicyObjectTypesForGrid(parentObjectTypeId,successFunction:(policyObjectTypes:Policy.DtoPolicyObjectType[]) => void, errorFunction): void;
  getObjectItemsByType(objectTypeId,includeChildTypes:boolean, successFunction:(objectItems:Policy.DtoPolicyObjectItemBase[]) => void, errorFunction): void;
  getObjectItemsByID(objectItemId,successFunction:(objectItem:Policy.DtoPolicyObjectItemBase) => void, errorFunction): void;
  addNewObject(object:Policy.DtoPolicyObjectItemBase,successFunction:(addedObject) => void , errorFunction): void;
  addNewObjectValue(objectId:string,value,successFunction:(addedValue) => void , errorFunction): void;
  deleteObjectValue(objectId:string,value,successFunction:(addedValue) => void , errorFunction): void;
  editObjectValueState(objectId:string,value,successFunction:(addedValue) => void , errorFunction): void;
  updateObject(object:Policy.DtoPolicyObjectItemBase,successFunction:() => void, errorFunction): void;
  deleteObject(objectId:string,successFunction:() => void, errorFunction): void;

  reloadDataFromLocalStorage(): void;
}
angular.module('resources.policies', [
    ])

    .factory( 'policiesResource', function( $resource,configuration:IConfig,propertiesUtils ,localStorage:IUserStorageService,remoteUserStorageResource:IUserStorageService,$q ) {
      var res = $resource(configuration.docType_list_url, '', {
        addNewPolicy: {
          method: 'PUT',
          url: configuration.policy_add_url,
          params: {layerId: '@layerId'},
          isArray: false },
        updatePolicy: {
          method: 'POST',
          url: configuration.policy_update_url,
          params: {id: '@id'},
          isArray: false },
        deletePolicy: {
          method: 'DELETE',
          url: configuration.policy_delete_url,
          params: {id: '@id'},
          isArray: false },

        getPolicyObjectTypes: {
          method: 'GET',
          url: configuration.objects_list_url,
          params: {parentObjectId: '@parentObjectId'},
          isArray: false },
        addNewObject: {
          method: 'PUT',
          url: configuration.object_add_url,
          params: {parentObjectId: '@parentObjectId'},
          isArray: false },
        updateObject: {
          method: 'POST',
          url: configuration.object_update_url,
          params: {id: '@id'},
          isArray: false },
        deleteObject: {
          method: 'DELETE',
          url: configuration.object_delete_url,
          params: {id: '@id'},
          isArray: false },
        getObjectItemsByType: {
          method: 'GET',
          url: configuration.objects_by_type_url,
          params: {objectTypeId: '@objectTypeId'},
          isArray: false },

      });
      var policyLayerTypes;
      var policyObjectTypes;
      var objectItemsList:Policy.DtoPolicyObjectItemBase[]=[];
      var policyItemsList:Policy.DtoPolicyLayerItemBase[]=[];
      var objectItemsListByIdDic={};
      var userStorage:IUserStorageService = remoteUserStorageResource;
      var policiesUserDataInitiated;
      var queuePoliciesInitiatedFn;
      var factory = <IPoliciesResource>{
        reloadDataFromLocalStorage:function()
        {
          var loopPromises = [];

          var objectsDeferred = $q.defer();
          loopPromises.push(objectsDeferred.promise);

          userStorage.getUserData('policies', 'objectItemsList',function(objectsData) {

              objectItemsList=objectsData?objectsData:[];
            if (objectsData) {
              objectItemsList.forEach(o=> {
                objectItemsListByIdDic[o.id] = o;
              });

            }
            objectsDeferred.resolve();
          },function(){  objectsDeferred.resolve();});
          var policiesDeferred = $q.defer();
          loopPromises.push(policiesDeferred.promise);
          userStorage.getUserData('policies','policyItemsList',function(policiesData) {
            policyItemsList = policiesData?policiesData:[];
            policiesDeferred.resolve();

            },function(){ policiesDeferred.resolve();})
          $q.all(loopPromises).then(function () {
            if (policyItemsList&&objectItemsListByIdDic) {
              policyItemsList.forEach(p=> {
                if (p.filter) {
                  p.filter.forEach(f=> {
                    if (f.values) {
                      f.values.forEach(v=> {
                        var foundObject = objectItemsListByIdDic[v.item.id];
                        if (foundObject) {
                          v.item = foundObject;
                        }
                        else {
                          v.item = null;
                        }

                      })
                      f.values = f.values.filter(v=><any>v.item != null); //delete removed objects from policy item
                    }
                  })
                }
                if (p.actionParams) {
                  p.actionParams.forEach(param=> {
                    if (param.values) {
                      param.values.forEach(val=> {
                        var foundObject = objectItemsListByIdDic[val.item.id];
                        if (foundObject) {
                          val.item = foundObject;
                        }
                        else {
                          val.item = null;
                        }
                      })
                      param.values = param.values.filter(v=><any>v.item != null); //delete removed objects from policy item
                    }
                  })
                }
              })
              userStorage.setUserData('policies', 'policyItemsList', policyItemsList,function(){},function(){});
              policiesUserDataInitiated=true;
              if(queuePoliciesInitiatedFn)
              {
                queuePoliciesInitiatedFn.fun.apply(undefined,queuePoliciesInitiatedFn.args)
                queuePoliciesInitiatedFn=null;
              }
            }
          });
        },
        getPolicyLayerTypes: function (successFunction:(policyTypes:Policy.DtoPolicyLayerType[]) => void, errorFunction) {
          return res.getPolicyLayerTypes( null ,
              function (addedDocType) {
                successFunction(addedDocType);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getPolicyItemsByLayer: function (layerId,successFunction:(policyItems:Policy.DtoPolicyLayerItemBase[]) => void, errorFunction) {
          if(policiesUserDataInitiated) {
            var layerItems = policyItemsList ? policyItemsList.filter(p=>p.layerType.id == layerId) : null;
            successFunction(layerItems);
          }
          else
          {
            queuePoliciesInitiatedFn = {fun:this.getPolicyItemsByLayer,args:[layerId,successFunction]};
          }
        },
        addNewPolicy: function (policyItem:Policy.DtoPolicyLayerItemBase,successFunction:(addedObjectItem:Policy.DtoPolicyLayerItemBase) => void, errorFunction) {

          policyItemsList.push(policyItem);
          userStorage.setUserData('policies','policyItemsList',policyItemsList,function() {
            successFunction(policyItem);
          },function(){});
          return;

        },
        updatePolicy: function (policyItem:Policy.DtoPolicyLayerItemBase,successFunction:() => void, errorFunction) {
          var item = policyItemsList.filter(p=> p.id==policyItem.id)[0];
          if(item)
          {
            var index= policyItemsList.indexOf(item);
            policyItemsList[index]=policyItem;
            userStorage.setUserData('policies','policyItemsList',policyItemsList,function() {
              successFunction();
            },function(){});
          }

          return;

        },
        deletePolicy: function (policyItemId:string,successFunction:() => void, errorFunction) {
          var item = policyItemsList.filter(p=> p.id==policyItemId)[0];
          if(item) {
            var index = policyItemsList.indexOf(item);
            policyItemsList.splice(index, 1);
            userStorage.setUserData('policies', 'policyItemsList', policyItemsList, function () {
              successFunction();
            }, function () {
            });
          }

          return;

        },
        getPolicyObjectTypes(parentObjectTypeIds:number[],successFunction:(policyObjectTypes:Policy.DtoPolicyObjectType[]) => void, errorFunction) {
          if(!policyObjectTypes) {
            res.getPolicyObjectTypes({parentObjectTypeId: parentObjectTypeIds},
                function (data:any) {
                  policyObjectTypes=data.content;
                  var result =  getTypes(parentObjectTypeIds);
                  successFunction(result);
                }
                , function (error) {
                  errorFunction(error);
                  console.error("failed to  get policy types  " + error);
                }
            )
          }
          else {
            var result = getTypes(parentObjectTypeIds);
            successFunction(result);
          }
          function getTypes(parentObjectTypeIds:number[]) {

            if (parentObjectTypeIds && parentObjectTypeIds[0]) {
              var result=[];
              parentObjectTypeIds.forEach(parentObjectTypeId=>
              {
                var parents = policyObjectTypes.filter(t=>t.parents.indexOf(parentObjectTypeId) > -1 );
                if(parents&&parents.length>0) {
                  result = result.concat(parents);
                }
              })
              return result;
            }
            return policyObjectTypes;
          }
        },
        getPolicyObjectTypesForGrid(parentObjectTypeId,successFunction:(policyObjectTypes:Policy.DtoPolicyObjectType[]) => void, errorFunction) {
          if(!policyObjectTypes) {
            res.getPolicyObjectTypes({parentObjectTypeId: parentObjectTypeId},
                function (data:any) {
                  policyObjectTypes = data.content;
                  var result = getTypes(parentObjectTypeId);
                  setItemCounts(result,successFunction);
                }
                , function (error) {
                  errorFunction(error);
                }
            )

          }
          else {
            var result = getTypes(parentObjectTypeId);
            setItemCounts(result,successFunction);
            //  successFunction(result);
          }
          function getTypes(parentObjectTypeId) {
            if (parentObjectTypeId) {
              var parents = policyObjectTypes.filter(t=>t.parents.indexOf(parentObjectTypeId) > -1 && t.id != parentObjectTypeId);
              return parents;
            }
            return policyObjectTypes;
          }
          function setItemCounts(result,successFunction)
          {
            var loopPromises = [];
            result.forEach(r=>{
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              factory.getObjectItemsByType(r.id,false,function(objectItems:Policy.DtoPolicyObjectItemBase[])
                  {
                    (<any>r).count = objectItems?objectItems.length:0;
                    deferred.resolve(r);
                  }
                  , function() {
                    deferred.resolve();
                  } );
              $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                successFunction(result);
              });
            });
          }
        },
        getObjectItemsByType(objectTypeId,includeChildTypes:boolean,successFunction:(objectItems:Policy.DtoPolicyObjectItemBase[]) => void, errorFunction) {

          if(includeChildTypes)
          {
            this.getPolicyObjectTypes([objectTypeId],function(policyObjectTypes:Policy.DtoPolicyObjectType[])
            {
              var result=[];
              if(policyObjectTypes) {
                policyObjectTypes.forEach(t=> {
                  var objectItems = objectItemsList ? objectItemsList.filter(p=>p.policyObjectType.id == t.id) : null;
                  if (objectItems) {
                    result = result.concat(objectItems);
                  }

                })
                successFunction(result);
              }
            },null);
          }
          else {
            var objectItems = objectItemsList ? objectItemsList.filter(p=>p.policyObjectType.id == objectTypeId) : null;
            successFunction(objectItems);
          }
        },
        getObjectItemsByID(objectItemId,successFunction:(objectItem:Policy.DtoPolicyObjectItemBase) => void, errorFunction) {


            var objectItem = objectItemsList ? objectItemsList.filter(p=>p.id == objectItemId)[0] : null;
            successFunction(objectItem);

        },
        addNewObject: function (objectItem:Policy.DtoPolicyObjectItemBase,successFunction:(addedObject) => void, errorFunction) {
          objectItemsList.push(objectItem);
          userStorage.setUserData('policies','objectItemsList',objectItemsList,function() {
              successFunction(policyItemsList);
          },function(){});

          return;

        },
        addNewObjectValue(objectId:string,value,successFunction:() => void , errorFunction) {
          var item = objectItemsList.filter(p=> p.id==objectId)[0];
          item.values.push(value);
          factory.updateObject(item,successFunction,errorFunction);
          return;

        },
        deleteObjectValue(objectId:string,value,successFunction:() => void , errorFunction) {
          var item = objectItemsList.filter(p=> p.id==objectId)[0];
          if (item.values) {
            var valueItem = item.values.filter(v=> v.item == value.item)[0];
            if(valueItem) {
             var index = item.values.indexOf(valueItem);
              item.values.splice(index,1);
              userStorage.setUserData('policies','objectItemsList',objectItemsList,function() {
                factory.reloadDataFromLocalStorage();
                successFunction();
              },function(){});

            }
          }
          return;

        },
       editObjectValueState(objectId:string,value,successFunction:() => void , errorFunction) {
         var item = objectItemsList.filter(p=> p.id == objectId)[0];
         if (item.values) {
           var valueItem = item.values.filter(v=> v.item.id == value.item.id)[0];
           valueItem.active=value.active;
           factory.updateObject(item, successFunction, errorFunction);
         }
          return;

        },
        updateObject: function (object:Policy.DtoPolicyObjectItemBase,successFunction:() => void, errorFunction) {
          var item = objectItemsList.filter(p=> p.id==object.id)[0];
          if(item)
          {
            var index= objectItemsList.indexOf(item);
            objectItemsList[index]=object;
            userStorage.setUserData('policies','objectItemsList',objectItemsList,function() {
              factory.reloadDataFromLocalStorage();
              successFunction();
            },function(){});


          }
          return;

        },
        deleteObject: function (objectId:string,successFunction:() => void, errorFunction) {
          var item = objectItemsList.filter(p=> p.id==objectId)[0];
          if(item)
          {
            var index= objectItemsList.indexOf(item);
            objectItemsList.splice(index,1);
            userStorage.setUserData('policies','objectItemsList',objectItemsList,function() {
              factory.reloadDataFromLocalStorage();
              successFunction();
            },function(){});

          }
          return;

        },

      }
      factory.reloadDataFromLocalStorage();
      return factory;
    })




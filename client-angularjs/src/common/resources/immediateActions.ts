
///<reference path='..\all.d.ts'/>
'use strict';
interface IImmediateActionsResource {

  startImmediateAction(actionRequest:Actions.DtoExecuteActionRequest,baseFilterValue:string,baseFilterField:string,filter,successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction): void;
  startImmediateActionForFiles(actionRequest:Actions.DtoExecuteActionRequest,fileIds:string[],successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction): void;
  stopImmediateAction(actionTriggerId:number,successFunction:() => void, errorFunction): void;
  getImmediateActionScriptFiles(actionTriggerId:number,successFunction:() => void, errorFunction): void;
  immediateActionsProgress(actionTriggerId:number,successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction): void;
  immediateActionsFilesResult(actionTriggerId:number,successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction): void;
  getSupportedActions(): string[];
}


angular.module('resources.actions.immediate', [
    ])
    .factory( 'immediateActionsResource', function( $resource,configuration:IConfig, currentUserDetailsProvider) {
      var res = $resource(configuration.immediate_action_start_url, "", {
        startImmediateAction: {
          method: 'POST',
          url: configuration.immediate_action_start_url,
          params: {filter: '@filter', baseFilterValue: '@baseFilterValue', baseFilterField: '@baseFilterField'},
          isArray: false
        },
        startImmediateActionForFiles: {
          method: 'POST',
          url: configuration.immediate_action_start_for_files_url,
          params: {fileIds:'@fileIds'},
          isArray: false
        },
        stopImmediateAction: {
          method: 'DELETE',
          url: configuration.immediate_action_stop_url,
          params: {actionTriggerId: '@actionTriggerId'},
          isArray: false
        },
        immediateActionsProgress: {
          method: 'GET',
          url: configuration.immediate_action_progress_url,
          params: {actionTriggerId: '@actionTriggerId'},
          isArray: false
        },
        immediateActionsFilesResult: {
          method: 'GET',
          url: configuration.immediate_action_file_result_url,
          params: {actionTriggerId: '@actionTriggerId'},
          isArray: false
        },
        getImmediateActionScriptFiles: {
          method: 'GET',
          url: configuration.immediate_action_scripts_files_url,
          params: {actionTriggerId: '@actionTriggerId'},
          isArray: false
        },
      });

      var factory = <IImmediateActionsResource>{
        startImmediateAction: function (actionRequest:Actions.DtoExecuteActionRequest, baseFilterValue:string, baseFilterField:string, filter, successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction) {
          return res.startImmediateAction({ filter: filter,
                baseFilterValue: baseFilterValue,
                baseFilterField: baseFilterField}, actionRequest,
              function (status:Actions.DtoActionExecutionTaskStatus) {

                successFunction(status);
              }
              , function (response) {

                //404 or bad
                if (response.status === 409) {
                  errorFunction({userMsg: "Immediate action is currently running. Try again later."});
                  console.log("failed to start immediate action due one is currently running " + response);
                }
                else {
                  errorFunction(null);
                  console.log("failed to start immediate action " + response);
                }

              }
          )
        },
        startImmediateActionForFiles: function (actionRequest:Actions.DtoExecuteActionRequest,fileIds:string[], successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction) {
          return res.startImmediateActionForFiles({ fileIds:fileIds.toString()}, actionRequest,
              function (status:Actions.DtoActionExecutionTaskStatus) {

                successFunction(status);
              }
              , function (response) {

                //404 or bad
                if (response.status === 409) {
                  errorFunction({userMsg: "Immediate action is currently running. Try again later."});
                  console.log("failed to start immediate action for files due one is currently running " + response);
                }
                else {
                  errorFunction(null);
                  console.log("failed to start immediate action for files " + response);
                }

              }
          )
        },
        stopImmediateAction: function (actionTriggerId:number, successFunction:() => void, errorFunction) {
          return res.stopImmediateAction({}, {actionTriggerId: actionTriggerId},
              function () {

                successFunction();

              }
              , function (response, error) {

                console.log("failed to stop immediate action " + error);
              }
          )
        },
        getImmediateActionScriptFiles: function (actionTriggerId:number, successFunction:() => void, errorFunction) {
          return res.getImmediateActionScriptFiles({actionTriggerId: actionTriggerId},
              function () {

                successFunction();

              }
              , function (response, error) {
                if (response.status > 400 && response.status < 500 && response.status != 404) {
                  console.error('Login Expired!!!\n\nPlease re-login');
                  location.href = "/login.html";
                }
                console.log("failed to stop immediate action " + error);
              }
          )
        },
        immediateActionsProgress: function (actionTriggerId:number, successFunction:(status:Actions.DtoActionExecutionTaskStatus) => void, errorFunction) {
          return res.immediateActionsProgress({actionTriggerId: actionTriggerId},
              function (status:Actions.DtoActionExecutionTaskStatus) {
                if (status) {
                  successFunction(status);
                }
                else {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get immediate action progress  " + error);
              }
          )
        },
        immediateActionsFilesResult: function (actionTriggerId:number, successFunction:() => void, errorFunction) {
          return res.immediateActionsFilesResult({actionTriggerId: actionTriggerId},
              function () {
                  successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get result files " + error);
              }
          )
        },
        getSupportedActions: function (successFunction:(actions:string[]) => void, errorFunction) {
          return JSON.parse(configuration.immediate_action_supported).filter((actionName)=>{
            return (actionName != "APPLY_LABEL" || currentUserDetailsProvider.isLabelingEnabled());
          });
        }
      };
      return factory;
    })


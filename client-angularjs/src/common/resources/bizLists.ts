
///<reference path='..\all.d.ts'/>
'use strict';
interface IBizListResource
{
  getListItems(bizListId:number,successFunction: (lists:Entities.DTOSimpleBizListItem[]) => void , errorFunction):void;
  addListItem(bizListItem:Entities.DTOSimpleBizListItem,successFunction: (item:Entities.DTOSimpleBizListItem) => void , errorFunction):void;
  removeListItem(bizListItem:Entities.DTOSimpleBizListItem,successFunction: () => void , errorFunction):void;
  editListItem(bizListItem:Entities.DTOSimpleBizListItem,successFunction: () => void , errorFunction):void;
  updateAllListItemsStatus(bizListId:number,oldStatus:string,newStatus:string,successFunction: () => void , errorFunction):void;
  updateListItemStatus(bizListId:number,bizListItemId,oldStatus:string,newStatus:string,successFunction: (updatedItem) => void , errorFunction):void;
  uploadListItemsFromFile(bizListId:number,fileData,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction):void;
  cancelUploadListItemsFromFile(bizListId:number,importId):void;
  updateUploadListItemsStatus(bizListId:number,importId:any,status:string,successFunction: () => void , errorFunction):void;
  createListItemsFromFolderNames(bizListId:number,folderPath:string,level:number,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction):void;
}

interface IBizListsResource {

  getBizListAllowedTypes(successFunction: (types:string[]) => void , errorFunction): void;
  getAllBizLists(bizListType, successFunction: (listsByIdDic:any) => void , errorFunction): void;
  updateBizList(bizList:Entities.DTOBizList,successFunction: () => void , errorFunction): void;
  addNewBizList(bizList:Entities.DTOBizList,successFunction: (newBizList:Entities.DTOBizList) => void , errorFunction): void;
  deleteBizList(bizListId:number,successFunction: () => void , errorFunction): void;
  getBizListConsumerTemplateTypes(successFunction: (templateTypes:Entities.DTOBizListTemplateType[]) => void , errorFunction): void;
  autoClassifyGroups(bizListType,options, successFunction: () => void , errorFunction): void;

}


angular.module('resources.bizLists', [
    ])

    .factory( 'bizListsResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.bizList_summary_list_url, '', {
        getAllBizLists: {
          method: 'GET',
          params: {bizListType: '@bizListType', pageSize: 400,take:400},
          isArray: false },
        getBizListAllowedTypes: {
          method: 'Get',
          url:configuration.bizList_list_types_url,
          isArray: true },
        updateBizList: {
          method: 'POST',
          url:configuration.bizList_update_url,
          params: {id: '@id'},
          isArray: false },
        autoClassifyGroups: {
          method: 'POST',
          url:configuration.bizList_auto_classify_groups_url,
          params: {bizListType: '@bizListType'},
          isArray: false },

        addNewBizList: {
          method: 'PUT',
          url: configuration.bizList_add_url,
          isArray: false },
        deleteBizList: {
          method: 'DELETE',
          url: configuration.bizList_delete_url,
          params: {id: '@id'},
          isArray: false },
        getAssignedBizList: {
          method: 'GET',
          url:configuration.bizList_list_url,
          isArray: false },
        getBizListConsumerTemplateTypes: {
          method: 'GET',
          url:configuration.bizList_consumer_templates_url,
          isArray: true },
      });
      var  bizListsDic;

      var factory = <IBizListsResource>{

        getAllBizLists: function (bizListType:string,successFunction:(listsByIdDic:any) => void, errorFunction) {

            return res.getAllBizLists({bizListType: bizListType},
                function (bizListSummaryInfo) {
                    if (bizListSummaryInfo.content) {
                      let bizListsDicLocal = {};
                      bizListSummaryInfo.content.map(function (item) {
                        item.bizListDto.itemsCount = item.itemsCount;
                        bizListsDicLocal[item.bizListDto.id] = item.bizListDto;
                      });
                      bizListsDic = bizListsDicLocal;
                      successFunction(filterDicByType(bizListType, bizListsDic));
                    }
                    else if(errorFunction) {
                      errorFunction(null);
                    }
                  }
                  , function (error) {
                    if(errorFunction) {
                      errorFunction(error);
                    }
                    console.log("failed to get  getAllBizLists" + error);

                 }
            )

          function filterDicByType(bizListType:string,bizListsDic:any)
          {
            if(!bizListType)
            {
              return bizListsDic;
            }
            var result = {};
           for(var listProp in bizListsDic)
            {
              if((<Entities.DTOBizList>bizListsDic[listProp]).bizListItemType == bizListType)
              {
                result[listProp]=bizListsDic[listProp];
              }
            }
            return result;
          }
        },
        getBizListAllowedTypes: function (successFunction:(types:string[]) => void, errorFunction) {
          return res.getBizListAllowedTypes(
              function (types:string[]) {
                if(types) {
                  successFunction(types);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get  getAllBizLists" + error);
              }
          )
        },
        updateBizList: function (bizList:Entities.DTOBizList,successFunction:() => void, errorFunction) {
          return res.updateBizList({id:bizList.id},JSON.stringify(bizList),
              function () {
                  successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get scan getScanConfiguration " + error);
              }
          )
        },
        autoClassifyGroups: function (bizListType,options:any,successFunction:() => void, errorFunction) {
          return res.autoClassifyGroups({bizListType:bizListType},options,
              function () {
                  successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to autoClassifyGroups " + error);
              }
          )
        },

        addNewBizList: function (bizList:Entities.DTOBizList,successFunction:(newBizList:Entities.DTOBizList) => void, errorFunction) {
          return res.addNewBizList(null,JSON.stringify(bizList),
              function (newBizList) {
                successFunction(newBizList);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to  addNewBizList " + error);
              }
          )
        },
        deleteBizList: function (bizListId:number,successFunction:() => void, errorFunction) {
          return res.deleteBizList({id:bizListId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to deleteBizList" + error);
              }
          )
        },
        getBizListConsumerTemplateTypes: function (successFunction:(templateTypes:Entities.DTOBizListTemplateType[]) => void, errorFunction) {
          return res.getBizListConsumerTemplateTypes({},
              function (templateTypes:Entities.DTOBizListTemplateType[]) {
                successFunction(templateTypes);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to getBizListConsumerTemplateTypes" + error);
              }
          )
        },
      };

      return factory;
    })

    .factory( 'bizListResource', function( $resource,configuration:IConfig) {
      var res = $resource(configuration.bizList_items_list_url, '', {
        getListItems: {
          method: 'GET',
          params: {id: '@id'},
          isArray: false },
        editListItem: {
          method: 'POST',
          url: configuration.bizList_items_edit_url,
          params: {id: '@id'},
          isArray: false },
        addListItem: {
          method: 'PUT',
          url: configuration.bizList_items_add_url,
          params: {id: '@id'},
          isArray: false },
        removeListItem: {
          method: 'DELETE',
          url: configuration.bizList_items_delete_url,
          params: {id: '@id',itemId:'@itemId'},
          isArray: false },
        updateAllListItemsStatus: {
          method: 'POST',
          url: configuration.bizList_items_update_status_url,
          params: {id: '@id',oldStatus:'@oldStatus',newStatus:'@newStatus'},
          isArray: false },
        updateListItemStatus: {
          method: 'POST',
          url: configuration.bizList_item_update_status_url,
          params: {id: '@id',oldStatus:'@oldStatus',newStatus:'@newStatus',bizListItemId:'@bizListItemId'},
          isArray: false },
        uploadListItemsFromFile: {
          method: 'POST',
          url: configuration.bizList_items_upload_url,
          params: {id: '@id'},
          headers: {'content-type':undefined},//
       //   responseType: "arraybuffer",
          transformRequest:angular.identity,
          isArray: false },
        createListItemsFromFolderNames: {
          method: 'POST',
          url: configuration.bizList_items_create_from_folder_names_url,
          params: {id: '@id',relativeDepth:'@relativeDepth',folderPath:'@folderPath'},
          isArray: false },
        updateUploadListItemsStatus :{
          method: 'POST',
          url: configuration.bizList_items_upload_status_update_url,
          params: {id: '@id',importId:'@importId',newStatus:'@newStatus'},
          isArray: false },
        cancelUploadListItemsFromFile :{
          method: 'DELETE',
          url: configuration.bizList_cancel_import_url,
          params: {id: '@id',importId:'@importId'},
          isArray: false },
      });
      var factory = <IBizListResource>{
        getListItems: function (bizListId:number,successFunction: (lists:Entities.DTOSimpleBizListItem[]) => void , errorFunction) {
          return res.getListItems({id:bizListId},
              function (lists:Entities.DTOSimpleBizListItem[]) {
                if(lists) {
                  successFunction(lists);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get scan getScanConfiguration " + error);
              }
          )
        },
        editListItem: function (bizListItem:Entities.DTOSimpleBizListItem,successFunction: () => void , errorFunction) {
          return res.editListItem({id:bizListItem.bizListId,itemId:bizListItem.id},bizListItem,
             function () {
                  successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to editListItem " + error);
              }
          )
        },
        addListItem: function (bizListItem:Entities.DTOSimpleBizListItem,successFunction: (item:Entities.DTOSimpleBizListItem) => void , errorFunction) {
          return res.addListItem({id:bizListItem.bizListId},bizListItem,
              function (item) {
                successFunction(item);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to addListItem " + error);
              }
          )
        },
        removeListItem: function (bizListItem:Entities.DTOSimpleBizListItem,successFunction: () => void , errorFunction) {
          return res.removeListItem({id:bizListItem.bizListId,itemId:bizListItem.id},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to removeListItem " + error);
              }
          )
        },
        updateAllListItemsStatus: function (bizListId:number,oldStatus:string,newStatus:string,successFunction: () => void , errorFunction) {
          return res.updateAllListItemsStatus({id:bizListId,oldStatus:oldStatus,newStatus:newStatus},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateAllListItemsStatus " + error);
              }
          )
        } ,
        updateListItemStatus(bizListId:number,bizListItemId,oldStatus:string,newStatus:string,successFunction: (updatedItem) => void , errorFunction) {
          return res.updateListItemStatus({id:bizListId,bizListItemId:bizListItemId,oldStatus:oldStatus,newStatus:newStatus},
              function (updatedItem) {
                successFunction(updatedItem);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateListItemStatus " + error);
              }
          )
        } ,
        uploadListItemsFromFile: function (bizListId:number,fileData,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction) {
          return res.uploadListItemsFromFile({id:bizListId},fileData,
              function (importResult:Entities.DTOBizListImportResult) {
                successFunction(importResult);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to uploadListItemsFromFile " + error);
              }
          )
        },
        cancelUploadListItemsFromFile: function (bizListId:number,importId) {
          return res.cancelUploadListItemsFromFile({id:bizListId,importId:importId},
              function () {

              }
              , function (error) {
                 console.log("failed to cancelUploadListItemsFromFile " + error);
              }
          )
        },
        createListItemsFromFolderNames: function (bizListId:number,folderPath:string,level:number,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction) {
          return res.createListItemsFromFolderNames({id:bizListId,folderPath:folderPath,relativeDepth:level},
              function (importResult:Entities.DTOBizListImportResult) {
                successFunction(importResult);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to createListItemsFromFolderNames " + error);
              }
          )
        },
        updateUploadListItemsStatus: function (bizListId:number,importId:any,status:string,successFunction: () => void , errorFunction) {
          return res.updateUploadListItemsStatus({id:bizListId,importId:importId,newStatus:status},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateUploadListItemsStatus " + error);
              }
          )
        }
      };
      return factory;

    });

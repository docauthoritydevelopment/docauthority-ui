
///<reference path='..\all.d.ts'/>
import IKendoFilter = ui.IKendoFilter;
import SavedChart = ui.SavedChart;
'use strict';
interface IChartsResource
{
  getCharts(successFunction: (charts:ui.SavedChart[]) => void , errorFunction): void;
  getChart(chartId, successFunction: (charts:ui.SavedChart) => void , errorFunction): void;
  addNewChart(chart:ui.SavedChart, successFunction: (chart:ui.SavedChart) => void , errorFunction): void;
  updateChart(chart:ui.SavedChart, successFunction: () => void , errorFunction): void;
  deleteChart(chartId:number,successFunction: () => void , errorFunction): void;
  getChartData(url:string,filter:IKendoFilter,maximumNumberOfEntries:number,includeOther:boolean,isHierarchicalData:boolean,demoInfo:any,successFunction:(chartData:Dashboards.DtoCounterReport[],data:Entities.DTOQueryEntityResult<Dashboards.DtoCounterReport>) => void, errorFunction): void;
  uploadUserViewDataFromCsv(fileData,updateDuplicates:boolean,createStubs:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
  validateUploadUserViewDataFromCsv(fileData,maxErrorRowsReport:number, createStubs:boolean, successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction): void;
}
interface ITrendChartsResource
{
  getCharts(successFunction: (charts:Dashboards.DtoTrendChart[]) => void , errorFunction): void;
  addNew(chart:Dashboards.DtoTrendChart,successFunction: (chart:Dashboards.DtoTrendChart) => void , errorFunction): void;
  updateChart(chart:Dashboards.DtoTrendChart,successFunction: () => void , errorFunction): void;
  deleteChart(chartId:number,successFunction: () => void , errorFunction): void;
  getChartData(chartId:number,startTime:number,endTime:number,sampleCount:number,viewResolutionMs:number,successFunction:(chartData:Dashboards.DtoTrendChartDataSummary) => void, errorFunction) : void;
}


angular.module('resources.charts', [
    ])

    .factory( 'chartResource', function( $resource,configuration:IConfig,propertiesUtils ) {
      var res = $resource(configuration.user_views_url, '', {
        getUserViews: {
          url:configuration.user_view_url,
          method: 'GET',
          params: {displayType: '@displayType', byUser: true},
          isArray: false },
        getUserView: {
          url:configuration.user_view_url,
          method: 'GET',
          params: {id: '@id'},
          isArray: false },
        updateUserView: {
          url:configuration.user_view_with_filter_url,
          method: 'POST',
          isArray: false },
        addNewUserView: {
          url:configuration.user_view_with_filter_url,
          method: 'PUT',
          isArray: false },
        deleteUserView: {
          url:configuration.user_view_with_filter_delete_url,
          method: 'DELETE',
          params: {id: '@id'},
          isArray: false },
        uploadUserViewDataFromCsv: {
          method: 'POST',
          url: configuration.user_view_data_upload_url,
          params: {},
          timeout:680000,
          headers: {'content-type': undefined},//
          transformRequest: angular.identity,
        },
        validateUploadUserViewDataFromCsv: {
          method: 'POST',
          url: configuration.user_view_data_validate_upload_url,
          params: {},
          headers: {'content-type': undefined},
          transformRequest: angular.identity,
        },

      });


      var convertFromSavedChartToUserViewDataDto = function(chart:ui.SavedChart) : Entities.UserViewDataDto{
        let chartCopy = _.cloneDeep(chart);
        let newUserView: Entities.UserViewDataDto = new Entities.UserViewDataDto();

        delete chartCopy.savedFilterDto;
        delete chartCopy.owner;
        newUserView.displayData = JSON.stringify(chartCopy);
        newUserView.id = chart.id;
        newUserView.name=chart.name;
        newUserView.displayType = Entities.EFilterDisplayType.CHART;
        newUserView.filter=chart.savedFilterDto;
        newUserView.globalFilter=chart.globalFilter;

        return newUserView;
      };

      var convertToSavedChart = function(userView: Entities.UserViewDataDto):ui.SavedChart{
        let savedChart:SavedChart = JSON.parse(userView.displayData);
        savedChart.id = userView.id;
        savedChart.name = userView.name;
        savedChart.savedFilterDto = userView.filter;
        savedChart.owner = userView.owner;
        savedChart.globalFilter = userView.globalFilter;
        return savedChart;
      };

      var factory = <IChartsResource>{
        getChart: function (chartId, successFunction: (chart:ui.SavedChart) => void , errorFunction) {
          return res.getUserView({id:chartId},
            function (userView: Entities.UserViewDataDto) {
              if(userView) {
                successFunction(convertToSavedChart(userView));
              }
              else
              {
                errorFunction('');
              }
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to get chart " + error);
            }
          )
        },
        getCharts: function (successFunction:(charts:ui.SavedChart[]) => void, errorFunction) {
          return res.getUserViews( {displayType: Entities.EFilterDisplayType.CHART},
              function (result:Entities.DTOQueryEntityResult<Entities.UserViewDataDto>) {
                if(result &&  result.content) {
                  let savedCharts= result.content
                    .map(c=>convertToSavedChart(c));
                  successFunction(savedCharts);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get saved charts " + error);
              }
          )
        },
        addNewChart: function (chart:ui.SavedChart, successFunction: (chart:ui.SavedChart) => void , errorFunction) {
          var newSavedCharts:Entities.UserViewDataDto = convertFromSavedChartToUserViewDataDto(chart);
          newSavedCharts.globalFilter = false;
          return res.addNewUserView(null,newSavedCharts,
            function (chart:Entities.UserViewDataDto) {
              successFunction(convertToSavedChart(chart));
            }
            ,function (error) {
              errorFunction(error);
              console.log("failed to add new saved chart " + error);
            }
          );
        },
        updateChart: function (chart:ui.SavedChart, successFunction:() => void, errorFunction) {
          return res.updateUserView(null,angular.toJson(convertFromSavedChartToUserViewDataDto(chart)),
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to update saved chart " + error);
              }
          )
        },
        deleteChart: function (chartId:number,successFunction:() => void, errorFunction) {
          return res.deleteUserView({id:chartId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to delete saved chart " + error);
              }
          )
        },
        uploadUserViewDataFromCsv: function (fileData,updateDuplicates:boolean ,createStubs:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
          return res.uploadUserViewDataFromCsv({updateDuplicates:updateDuplicates,createStubs:createStubs},fileData,
            function (importResult:Operations.DTOImportSummaryResult) {
              successFunction(importResult);
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to uploadUserViewDataFromCsv " + error);
            }
          )
        },
        validateUploadUserViewDataFromCsv: function (fileData,maxErrorRowsReport:number,createStubs:boolean, successFunction: (importResult:Operations.DTOImportSummaryResult) => void , errorFunction) {
          return res.validateUploadUserViewDataFromCsv({maxErrorRowsReport:maxErrorRowsReport,createStubs:createStubs},fileData,
            function (importResult:Operations.DTOImportSummaryResult) {
              successFunction(importResult);
            }
            , function (error) {
              errorFunction(error);
              console.log("failed to  validate uploadRootFoldersFromCsv " + error);
            }
          )
        },
        getChartData: function (url:string,filter:IKendoFilter,maximumNumberOfEntries:number,includeOther:boolean,isHierarchicalData:boolean,demoInfo:any, successFunction:(chartData:Dashboards.DtoCounterReport[],data:Entities.DTOQueryEntityResult<any>) => void, errorFunction) {
          var res = $resource(url, {},{timeout: 3 * 60 * 1000});
          return res.get({take:maximumNumberOfEntries,filter:filter},
              function (data:Entities.DTOQueryEntityResult<any>) {
                if(data && data.content&&maximumNumberOfEntries<100) {
                  var chartData:Dashboards.DtoCounterReport[] = [];
                  var hierarchicalDataParentDic={};
                  if(isHierarchicalData&& data.content[0]&&  (<any>data.content[0]).item.parents)
                  {
                    data.content.forEach(d=>hierarchicalDataParentDic[(<any>d).item.id]=(<any>d).item.name);
                  }
                  data.content.forEach(d=>{
                    if(!(<Entities.DTOAggregationCountItem<any>>d).item)
                    {
                      throw ('Chart data has no counter data');
                    }
                    var item:any=(<Entities.DTOAggregationCountItem<any>>d).item;
                    var counter = (<Entities.DTOAggregationCountItem<any>>d).count!=null?(<Entities.DTOAggregationCountItem<any>>d).count:
                      (<Entities.DTOAggregationCountFolder>d).numOfDirectFiles!=null && (<Entities.DTOAggregationCountFolder>d).numOfDirectFiles>-1? (<Entities.DTOAggregationCountFolder>d).numOfDirectFiles:
                        (<Entities.DTOAggregationCountFolder>d).numOfAllFiles;
                    var totalCounter = (<Entities.DTOAggregationCountItem<any>>d).count2!=null && (<Entities.DTOAggregationCountItem<any>>d).count2 > 0 ?(<Entities.DTOAggregationCountItem<any>>d).count2: item.numOfFiles ? item.numOfFiles : item.totalCount;
                    if(isHierarchicalData&&item.parents)
                    {
                      var parentsNames = [];
                      item.parents.forEach(pId=>parentsNames.push(hierarchicalDataParentDic[pId]));
                      item.path = parentsNames.join('/');
                    }
                    var description= item.path?item.path:'';

                    if(item.path || item.address || item.user || item.extension || item.label || item.fullName || item.identifier)
                    {
                      if(item.path && _.endsWith(item.path,'\\'))
                      {
                        item.path = item.path.substr(0,item.path.length-1);
                      }
                      var baseName =  item.name;

                      //item.path that is empty string is considered false in item.path?. Will try to find another match to display
                      let origText:string = item.path ? item.path : (item.address ? item.address: (item.user ? item.user : item.extension ? item.extension : (item.label ? item.label : item.fullName?item.fullName: item.identifier)));
                      item.name = propertiesUtils.ellipsisMiddle( origText,baseName,configuration.chart_folder_path_maxLength,item.path ? configuration.chart_folder_basename_maxLength : configuration.chart_user_basename_maxLength);
                    }
                    description = item.description?item.description:description;
                    var id = item.id?item.id:item.user;
                    var name = item.name?item.name:item.user?item.user:item.address?item.address:item.domain?item.domain:item.groupName?item.groupName:item.path?item.path:typeof(item) == "string"?item:'';
                    var extraInfo = null;

                    if(_.has(demoInfo, name)){
                      extraInfo = demoInfo[name];
                    }
                    chartData.push(new Dashboards.DtoCounterReport(name,description,counter,id, extraInfo,totalCounter) )
                  });
                  if(includeOther && chartData.length>0) {
                    chartData.push(new Dashboards.DtoCounterReport('Others', null, data.remainingAggregatedCount<0?0:data.remainingAggregatedCount,'others'));
                  }
                  //if(isHierarchicalData)
                  //{
                  //  chartData = chartData.filter(c=>c.counter>0);
                  //  chartData.sort((t1, t2)=> propertiesUtils.sortAlphbeticFun('counter'));
                  //  chartData = chartData.slice(0,maximumNumberOfEntries);
                  //
                  //}
                  successFunction(chartData,data);
                }
                else if(data && data.content&&maximumNumberOfEntries>=100){
                  successFunction(null,data);
                }
                else {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to delete saved filter " + error);
              }
          )
        },
      };
      return factory;
    })

    .factory( 'trendChartResource', function( $resource,configuration:IConfig,propertiesUtils ) {
      var res = $resource(configuration.trendChart_url, '', {
        getCharts: {
          method: 'GET',
          isArray: false },
        getChartData: {
          method: 'GET',
          url:configuration.trendChart_data_url,
          params: {id: '@id'},
          isArray: false },
        updateChart: {
          method: 'POST',
          isArray: false },
        addNew: {
          method: 'PUT',
          isArray: false },
        deleteChart: {
          method: 'DELETE',
          url:configuration.trendChart_delete_url,
          isArray: false },

      });
      var factory = <ITrendChartsResource>{
        getCharts: function (successFunction:(charts:Dashboards.DtoTrendChart[]) => void, errorFunction) {
          return res.getCharts(
              function (result:Entities.DTOQueryEntityResult<Dashboards.DtoTrendChart>) {
                if(result) {
                  successFunction(result.content);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get saved charts " + error);
              }
          )
        },
        addNew: function (chart:Dashboards.DtoTrendChart, successFunction: (chart:Dashboards.DtoTrendChart) => void , errorFunction) {
          return res.addNew(null,JSON.stringify(chart),
              function (chart:Dashboards.DtoTrendChart) {
                successFunction(chart);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to add new saved chart " + error);
              }
          )
        },
        updateChart: function (chart:Dashboards.DtoTrendChart,successFunction:() => void, errorFunction) {
          return res.updateChart(null,angular.toJson(chart),
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to update saved chart " + error);
              }
          )
        },
        deleteChart: function (chartId:number,successFunction:() => void, errorFunction) {
          return res.deleteChart({id:chartId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to delete saved chart " + error);
              }
          )
        },
        getChartData: function (chartId,startTime:number,endTime:number,sampleCount:number,viewResolutionMs:number,successFunction:(chartData:Dashboards.DtoTrendChartDataSummary) => void, errorFunction) {
          var options = {id:chartId};
          startTime? options['startTime']=startTime:null;
          endTime? options['endTime']=endTime:null;
          sampleCount? options['sampleCount']=sampleCount:null;
          viewResolutionMs? options['viewResolutionMs']=viewResolutionMs:null;
          return res.getChartData(options,
              function (chartData:Dashboards.DtoTrendChartDataSummary) {
                successFunction(chartData);
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to get saved trend chart data " + error);
              }
          )

        },
      };
      return factory;
    });

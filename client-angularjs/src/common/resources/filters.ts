///<reference path='..\all.d.ts'/>
'use strict';

interface IFilterResource {
  getFilters(successFunction: (filters:ui.SavedDiscoveredFilter[]) => void , errorFunction): void;
  getViews(pageParams,successFunction: (filters:ui.SavedDiscoveredFilter[]) => void , errorFunction): void;
  addNew(filter:ui.SavedDiscoveredFilter, successFunction: (filter:ui.SavedDiscoveredFilter) => void , errorFunction): void;
  updateFilter(filter:ui.SavedDiscoveredFilter, successFunction: (filter:ui.SavedDiscoveredFilter) => void , errorFunction): void;
  deleteFilter(filterId:number,successFunction: () => void , errorFunction): void;
  getView(viewId:number, successFunction: (filter:ui.SavedDiscoveredFilter) => void , errorFunction): void;
}

angular.module('resources.filters', [])
  .factory( 'filterResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.user_view_with_filter_url, '', {
      getUserViews: {
        url:configuration.user_view_url,
        method: 'GET',
        params: {displayType: '@displayType', byUser: true},
        isArray: false
      },
      updateUserView: {
        method: 'POST',
        isArray: false
      },
      addNewUserView: {
        method: 'PUT',
        isArray: false
      },
      deleteUserView: {
        url:configuration.user_view_with_filter_delete_url,
        params: {id: '@id'},
        method: 'DELETE',
        isArray: false
      },
      getUserView: {
        url:configuration.user_view_url,
        method: 'GET',
        params: {id: '@id'},
        isArray: false
      },
    });

    var convertFromSavedDiscoveredFilterToUserViewDataDto  = function(filter:ui.SavedDiscoveredFilter) : Entities.UserViewDataDto {
      let filterCopy = _.cloneDeep(filter);
      var userView:Entities.UserViewDataDto = new Entities.UserViewDataDto();

      delete filterCopy.savedFilterDto;
      userView.displayData = JSON.stringify(filterCopy);
      userView.id = filter.id ;
      userView.name=filter.name;
      userView.displayType = Entities.EFilterDisplayType.DISCOVER;
      userView.filter = filter.savedFilterDto;
      userView.globalFilter = filter.globalFilter;
      userView.displayType = filter.displayType;
      return userView;
    };

    var convertToSavedDiscoverFilters = function(userView: Entities.UserViewDataDto):ui.SavedDiscoveredFilter{
      var discoverFilter:ui.SavedDiscoveredFilter = JSON.parse(userView.displayData);
      discoverFilter.id = userView.id;
      discoverFilter.name = userView.name;
      discoverFilter.savedFilterDto = userView.filter;
      discoverFilter.globalFilter = userView.globalFilter;
      discoverFilter.username = userView.owner.name;
      discoverFilter.displayType = userView.displayType;
      return discoverFilter;
    };

    var factory = <IFilterResource>{
      getFilters: function (successFunction:(filters:ui.SavedDiscoveredFilter[]) => void, errorFunction) {
        return res.getUserViews(
          {displayType: Entities.EFilterDisplayType.DISCOVER},
          function (result:Entities.DTOQueryEntityResult<Entities.UserViewDataDto>) {
            if(result &&  result.content) {
              let savedFilters= result.content
                .map(c=>convertToSavedDiscoverFilters(c));
              successFunction(savedFilters);
            }
            else {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get saved filters " + error);
          }
        );
      },
      getViews: function (pageParams, successFunction:(filters:ui.SavedDiscoveredFilter[]) => void, errorFunction) {
        let params = {displayType: Entities.EFilterDisplayType.DISCOVER_VIEW};
        if(params) {
          params = angular.extend(params,pageParams);
        }
        return res.getUserViews(params,function (result:Entities.DTOQueryEntityResult<Entities.UserViewDataDto>) {
            if(result &&  result.content) {
              let savedFilters= result.content
                .map(c=>convertToSavedDiscoverFilters(c));
              successFunction(savedFilters);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get saved filters " + error);
          }
        );
      },
      addNew: function (filter:ui.SavedDiscoveredFilter, successFunction: (filter:ui.SavedDiscoveredFilter) => void , errorFunction) {
        var newSavedFilters:Entities.UserViewDataDto = convertFromSavedDiscoveredFilterToUserViewDataDto(filter);
        newSavedFilters.globalFilter = false;
        return res.addNewUserView(null,JSON.stringify(newSavedFilters),
              function (newFilter:ui.SavedDiscoveredFilter) {
                successFunction(newFilter);
              }
              ,function (error) {
                errorFunction(error);
                console.log("failed to add new saved filter " + error);
              }
        );
      },
      updateFilter: function (filter:ui.SavedDiscoveredFilter, successFunction:(filter:ui.SavedDiscoveredFilter) => void, errorFunction) {
        return res.updateUserView(null,JSON.stringify(convertFromSavedDiscoveredFilterToUserViewDataDto(filter)),
          function (filter:ui.SavedDiscoveredFilter) {
            successFunction(filter);
          }
          , function (error) {
            errorFunction(error);
          }
        );
      },
      deleteFilter: function (filterId:number,successFunction:() => void, errorFunction) {
        return res.deleteUserView({id:filterId},
          function () {
            successFunction();
          }
          , function (error) {
            errorFunction(error);
          }
        );
      },
      getView: function (viewId:number, successFunction:(view:ui.SavedDiscoveredFilter) => void, errorFunction) {
        return res.getUserView({id: viewId},
          function (view:Entities.UserViewDataDto) {
            if(view) {
              let savedView= convertToSavedDiscoverFilters(view);
              successFunction(savedView);
            }
            else {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
            console.log("failed to get view " + error);
          }
        )
      }
    };
    return factory;
  });

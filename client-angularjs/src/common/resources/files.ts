
///<reference path='..\all.d.ts'/>
'use strict';

interface IFileResource {

  getFileById(fileId,successFunction: (f:Entities.DTOFileDetails ) => void , errorFunction): void;
  isFileContentAuthorized(fileId,successFunction: (f:boolean ) => void , errorFunction): void;
  getFileHighlights(fileId,searchToken,successFunction: (htmlContent:string[] ) => void , errorFunction): void;
  getFilePatterns(fileId,limitCount:number,itemFullLength:number,successFunction: (htmlContent:Entities.DtoFileHighlights ) => void , errorFunction): void;
  getFileEntityExtractions(fileId,limitCount:number,itemFullLength:number,successFunction: (htmlContent:Entities.DtoFileHighlights ) => void , errorFunction): void;
  getFileHighlightsWithFilter(fileId,filter:IKendoFilter,limitCount:number,itemFullLength:number,successFunction: (data:Entities.DtoFileHighlights) => void , errorFunction): void;

}
interface IFilesResource {

  getFilesCount(filter,urlParams,successFunction: (count:number ) => void , errorFunction): void;
  getFiles(kendoFilterFormat,successFunction: (response ) => void , errorFunction): void;

}

angular.module('resources.files', [
])

  .factory( 'filesResource', function( $resource,configuration:IConfig ) {
    var res1 = $resource(configuration.files_url, { }, {
      getFiles: {
        method: 'GET',
        params: {take: '@take',page: '@page'},
        isArray: false },
    });
    var factory = <IFilesResource>{
      getFiles: function (kendoFilterFormat,successFunction:(response) => void, errorFunction) {
        return res1.getFiles({take: 1,page: 1,filter:kendoFilterFormat},
            function (response) {

                successFunction(response);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      getFilesCount: function (filter,urlParams,successFunction:(count:number) => void, errorFunction) {
       var url;
        if(urlParams)
        {
         url = configuration.files_url+'?'+encodeURI(urlParams)
        }
        else {
          url = configuration.files_url
        }
        var res = $resource(url, { }, {
          getFilesCount: {
            method: 'GET',
            params: {take: '@take',page: '@page',pageSize: '@pageSize',filter:'@filter'},
            isArray: false },
        });
        var params=filter?{filter:JSON.stringify(filter)}:{};
        params['take']=0;
        return res.getFilesCount(params,
            function (f:Entities.DTOQueryEntityFIlesResult) {
              if(f) {
                successFunction(f.totalElements);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

    };
    return factory;

  })


  .factory( 'fileResource', function( $resource,configuration:IConfig,$http ) {
    var res = $resource(configuration.file_details_url, { id: '@_id' }, {
      getFileById:  {
        method: 'GET',
        params: {id: '@id'},
        isArray: false
      },
      getFileHighlights:  {
        method: 'GET',
        url:configuration.file_highlights_url,
        params: {fileId: '@fileId',contentSearch: '@contentSearch'},
        isArray: false
      },
      isFileContentAuthorized:  {
        method: 'GET',
        url:configuration.is_file_content_authorized,
        params: {fileId: '@fileId'},
        isArray: false
      },
      getFileHighlightsWithFilter:  {
        method: 'GET',
        url:configuration.file_highlights_list_url,
        params: {fileId: '@fileId',filter: 'filter'},
        isArray: false
      },
      getFilePatterns:  {
        method: 'GET',
        url:configuration.file_highlights_patterns_url,
        params: {fileId: '@fileId'},
        isArray: false
      },
      getFileEntityExtractions:  {
        method: 'GET',
        url:configuration.file_highlights_entities_url,
        params: {fileId: '@fileId'},
        isArray: false
      },


    });
    var factory = <IFileResource>{

      getFileById: function (fileId,successFunction:(f:Entities.DTOFileDetails) => void, errorFunction) {
        return res.getFileById({id:fileId},
            function (f:Entities.DTOFileDetails) {
              if(f) {
                successFunction(f);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      isFileContentAuthorized: function (fileId,successFunction:(isAuthorized:boolean) => void, errorFunction) {
        //resource cannot return boolean. Use $http instead
        return $http.get(configuration.is_file_content_authorized.replace(':fileId',fileId)).then(
            function (response:any) {
                successFunction(response.data);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

      getFileHighlightsWithFilter: function (fileId,filter:IKendoFilter,limitCount:number,itemFullLength:number, successFunction:(data:Entities.DtoFileHighlights) => void, errorFunction) {
        return res.getFileHighlightsWithFilter({fileId:fileId,take:limitCount,page:1,filter:filter,itemFullLength:itemFullLength},
            function (data:Entities.DtoFileHighlights) {

                successFunction(data);

            }
            , function (error) {
              errorFunction(error);
              console.error("failed to get file highlights id: "+fileId +' ' + error);
            }
        )
      },
      getFilePatterns: function (fileId,limitCount:number,itemFullLength:number,successFunction:(htmlContent:Entities.DtoFileHighlights) => void, errorFunction) {
        return res.getFilePatterns({fileId:fileId,take:limitCount,itemFullLength:itemFullLength},
            function (data:Entities.DtoFileHighlights) {

                successFunction(data);

            }
            , function (error) {
              errorFunction(error);
              console.error("failed to get file patterns: "+fileId +' ' + error);
            }
        )
      },
      getFileEntityExtractions: function (fileId,limitCount:number,itemFullLength:number,successFunction:(htmlContent:Entities.DtoFileHighlights) => void, errorFunction) {
        return res.getFileEntityExtractions({fileId:fileId,take:limitCount,itemFullLength:itemFullLength},
            function (data:Entities.DtoFileHighlights) {

                successFunction(data);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },


    };
    return factory;

  })

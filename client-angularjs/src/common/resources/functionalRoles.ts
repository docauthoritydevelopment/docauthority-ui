'use strict';

interface IFunctionalRoleResource
{
  addFunctionalRole(fileTagId,itemId):any;
  removeFunctionalRole(fileTagId,itemId):any;
}

interface IFunctionalRolesResource
{
  getFunctionalRoles(successFunction:(functionalRoles:Users.DtoFunctionalRoleSummaryInfo[]) => void):any;
  getFunctionalRolesByIds(ids:number[],successFunction:(functionalRoles:Users.DtoFunctionalRole[]) => void, errorFunction):any;
  getSystemRoles(successFunction: (roles:Users.DtoSystemRole[]) => void , errorFunction): void;
  getFunctionalRolesLdapGroups(successFunction, errorFunction):void;
  addNewFunctionalRole(functionalRoleSummaryInfo, successFunction, errorFunction):void;
  deleteFunctionalRole(id, successFunction, errorFunction):void;
  updateFunctionalRole(functionalRoleSummaryInfo, successFunction, errorFunction):void;
  getFunctionalRoleAssignedUsers(roleId,successFunction:(users:Users.DTOUser[],totalElements:number) => void, errorFunction): void;
}

angular.module('resources.functionalRoles', [])
  .factory('functionalRolesResource', function ($resource, configuration, propertiesUtils) {
    var res = $resource(configuration.functionalRole_list_url, '', {
      getFunctionalRoles: {
        method: 'GET',
        url: configuration.functionalRole_list_url,
        isArray: false },
      getFunctionalRolesByIds: {
        method: 'GET',
        url: configuration.functionalRole_by_ids_url,
        params: { 'funcIds':'@funcIds'},
        isArray: true },
      getFunctionalRolesLdapGroups: {
        method: 'GET',
        url: configuration.functionalRole_ldap_list_url,
        isArray: true },
      addNewFunctionalRole: {
        method: 'PUT',
        url: configuration.functionalRole_add_url,
        params: { docTypeParentId: '@docTypeParentId' },
        isArray: false },
      updateFunctionalRole: {
        method: 'POST',
        url: configuration.functionalRole_edit_url,
        params: { id: '@id' },
        isArray: false },
      deleteFunctionalRole: {
        method: 'DELETE',
        url: configuration.functionalRole_delete_url,
        params: { id: '@id' },
        isArray: false },
      getSystemRoles: {
        method: 'GET',
        url:configuration.functionalRole_systemRole_list_url,
        isArray: true },
      getFunctionalRoleAssignedUsers: {
        method: 'GET',
        params: {functionalRoleId: '@functionalRoleId'},
        url:configuration.functionalRole_assigned_users_url,
        isArray: false },
    });
    var factory = {
      getFunctionalRoles: function (successFunction, errorFunction) {
        return res.getFunctionalRoles(
          {
            take:299,
            page:1,
            pageSize:299
          },
          function (functionalroles) {
          if (functionalroles && functionalroles.content) {
            functionalroles.content.sort(function (t1, t2) { return propertiesUtils.sortAlphbeticFun('name'); });
            successFunction(functionalroles.content);
          }
          else {
            errorFunction(null);
          }
        }, function (error) {
          errorFunction(error);
        });
      },
      getFunctionalRolesByIds: function (funcIds:number[],successFunction, errorFunction) {
        return res.getFunctionalRolesByIds({funcIds:funcIds}, function (functionalroles) {
          if (functionalroles) {
            successFunction(functionalroles);
          }
          else {
            errorFunction(null);
          }
        }, function (error) {
          errorFunction(error);
        });
      },
      getFunctionalRolesLdapGroups: function (successFunction, errorFunction) {
        return res.getFunctionalRolesLdapGroups(null, function (groups) {
          successFunction(groups);
        }, function (error) {
          errorFunction(error);
        });
      },
      addNewFunctionalRole: function (functionalRoleSummaryInfo, successFunction, errorFunction) {
        return res.addNewFunctionalRole(null, JSON.stringify(functionalRoleSummaryInfo), function (addedFunctionalRole) {
          successFunction(addedFunctionalRole);
        }, function (error) {
          errorFunction(error);
        });
      },
      updateFunctionalRole: function (functionalRoleSummaryInfo, successFunction, errorFunction) {
        return res.updateFunctionalRole({ id: functionalRoleSummaryInfo.functionalRoleDto.id }, JSON.stringify(functionalRoleSummaryInfo), function () {
          successFunction();
        }, function (error) {
          errorFunction(error);
        });
      },
      deleteFunctionalRole: function (id, successFunction, errorFunction) {
        return res.deleteFunctionalRole({ id: id }, null, function () {
          successFunction();
        }, function (error) {
          errorFunction(error);
        });
      },
      getSystemRoles: function (successFunction:(roles:Users.DtoSystemRole[]) => void, errorFunction) {
        return res.getSystemRoles(null,
          function (roles:Users.DtoSystemRole[]) {
            successFunction(roles);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getFunctionalRoleAssignedUsers: function(functionalRoleId,successFunction:(users:Users.DTOUser[],totalElement) => void, errorFunction) {
        return res.getFunctionalRoleAssignedUsers({functionalRoleId:functionalRoleId},
          function (data:Entities.DTOQueryEntityResult<Users.DTOUser>) {

            successFunction(data.content,data.numberOfElements);
          }
          , function (error) {
            errorFunction(error);
          }
        )
      }
    };
    return factory;
  })
  .factory( 'functionalRoleResource', function( $resource,configuration:IConfig) {
    var group = $resource(configuration.addRemove_Group_FunctionalRole_Url, {functionalRoleId:'@functionalRoleId', groupId: '@groupId' }, {
      getFunctionalRoles: { method: 'GET' },
      addFunctionalRole: { method: 'PUT' },
      removeFunctionalRole: { method: 'DELETE' }
    });
    var folder = $resource(configuration.addRemove_Folder_FunctionalRole_Url, {functionalRoleId:'@functionalRoleId', folderId: '@folderId' }, {
      getFunctionalRoles: { method: 'GET' },
      addFunctionalRole: { method: 'PUT' },
      removeFunctionalRole: { method: 'DELETE' }
    });
    var file = $resource(configuration.addRemove_File_FunctionalRole_Url, {functionalRoleId:'@functionalRoleId', fileId: '@fileId' }, {
      getFunctionalRoles: { method: 'GET' },
      addFunctionalRole: { method: 'PUT' },
      removeFunctionalRole: { method: 'DELETE' }
    });
    return function(entityDisplayType: EntityDisplayTypes):IFunctionalRoleResource {
      if(entityDisplayType==EntityDisplayTypes.groups)
      {
        var result:IFunctionalRoleResource = <IFunctionalRoleResource>
          {
            getFunctionalRoles:function(functionalRoleId,itemId) {
              return group.getFunctionalRoles({functionalRoleId:functionalRoleId,groupId:itemId});
            },
            addFunctionalRole:function(functionalRoleId,itemId) {
              return group.addFunctionalRole({functionalRoleId:functionalRoleId,groupId:itemId});
            },
            removeFunctionalRole:function(functionalRoleId,itemId) {
              return group.removeFunctionalRole({functionalRoleId:functionalRoleId,groupId:itemId});
            }
          }
        return result;
      }
      if(entityDisplayType==EntityDisplayTypes.folders|| entityDisplayType==EntityDisplayTypes.folders_flat)
      {
        var result:IFunctionalRoleResource = <IFunctionalRoleResource>
          {
            getFunctionalRoles:function(functionalRoleId,itemId) {
              return folder.getFunctionalRoles({functionalRoleId:functionalRoleId,folderId:itemId});
            },
            addFunctionalRole:function(functionalRoleId,itemId) {
              return folder.addFunctionalRole({functionalRoleId:functionalRoleId,folderId:itemId});
            },
            removeFunctionalRole:function(functionalRoleId,itemId) {
              return folder.removeFunctionalRole({functionalRoleId:functionalRoleId,folderId:itemId});
            }
          }
        return result;
      }
      if(entityDisplayType==EntityDisplayTypes.files)
      {
        var result:IFunctionalRoleResource = <IFunctionalRoleResource>
          {
            getFunctionalRoles:function(functionalRoleId,itemId) {
              return file.getFunctionalRoles({functionalRoleId:functionalRoleId,fileId:itemId});
            },
            addFunctionalRole:function(functionalRoleId,itemId) {
              return file.addFunctionalRole({functionalRoleId:functionalRoleId,fileId:itemId});
            },
            removeFunctionalRole:function(functionalRoleId,itemId) {
              return file.removeFunctionalRole({functionalRoleId:functionalRoleId,fileId:itemId});
            }
          }
        return result;
      }
      return null;
    };
  });

//# sourceMappingURL=functionalRoles.js.map

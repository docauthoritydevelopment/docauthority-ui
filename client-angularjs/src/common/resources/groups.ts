
///<reference path='..\all.d.ts'/>
'use strict';

interface IGroupResource {

  getGroupById(groupId,successFunction: (g:Entities.DTOGroupDetails ) => void , errorFunction): void;
  updateGroupName(groupId,newGroupName:string,successFunction: (group:Entities.DTOGroup) => void , errorFunction): void;
  recoverGroupFromFreeze(groupId,successFunction: (group:Entities.DTOGroup) => void , errorFunction): void;
  groupNamingDebugData(groupId,weightData:GroupNaming.DTOFactors,saveNewName:boolean, successFunction: (data:GroupNaming.DTOGroupNamingDebugData) => void , errorFunction): void;
  setRefinementRules(groupId,tokens:string[], successFunction: (g:Entities.DtoRefinementRuleDto[] ) => void , errorFunction): void;
  deleteAllRefinementRules(groupId, successFunction: ( ) => void , errorFunction): void;
  applyRefinementRules(groupId, successFunction: () => void , errorFunction): void;
  listRulesForSubGroupCluster(subGroupId,successFunction: (g:DTOPagedResult<Entities.DtoRefinementRuleDto> ) => void , errorFunction): void;


}
interface IGroupsResource {

  detachFileFromGroup(fileId,successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
  findGroupAndAttachToFile(fileId,successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
  attachFilesToGroup(fileIds:number[],newGroupId,newGroupName, successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
  createJoinedGroup(joinedGroup:Entities.DTONewJoinedGroupDto, successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
  deleteJoinedGroup(joinedGroupId, successFunction: () => void , errorFunction): void;
 removeChildGroupFromJoinedGroup(joinedGroupId,groupId, successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
 addGroupsToJoinedGroup(joinedGroupId,joinedGroup:Entities.DTONewJoinedGroupDto, successFunction: (group:Entities.DTOGroupDetails) => void , errorFunction): void;
  getGroups(groupIds:string[],successFunction: (groups:Entities.DTOGroup[]) => void , errorFunction):void;
}

angular.module('resources.groups', [
 ])

  .factory( 'groupsResource', function( $resource,configuration:IConfig ) {
    var res = $resource(configuration.group_details_url, { id: '@id' }, {
      detachFileFromGroup:  {
        method: 'POST',
        url:configuration.file_detach_group_url,
        params: {fileId: '@fileId'},
        isArray: false
      },
      findGroupAndAttachToFile: {
        method: 'POST',
        url:configuration.file_attach_auto_group_url,
        params:{fileId: '@fileId'},
        isArray: false
      },
      attachFilesToGroup:  {
        method: 'POST',
        url:configuration.files_attach_group_url,
        isArray: false
      },
      createJoinedGroup:  {
        method: 'PUT',
        url:configuration.group_create_joinedGroup_url,
        params: {},
        isArray: false
      },

      deleteJoinedGroup:  {
        method: 'DELETE',
        url:configuration.group_delete_joinedGroup_url,
        params: {id: '@id'},
        isArray: false
      },
      removeChildGroupFromJoinedGroup:  {
            method: 'POST',
            url:configuration.group_remove_from_joinedGroup_url,
            params: {id: '@id',groupId: '@groupId'},
            isArray: false
          },
      addGroupsToJoinedGroup:  {
        method: 'POST',
        url:configuration.group_add_to_joinedGroup_url,
        params: {id: '@id',groupId: '@groupId'},
        isArray: false
      },
      getGroups:  {
        method: 'GET',
        url:configuration.group_list_url,
        params: {groupIds: '@groupIds'},
        isArray: true
      },



    });

    var factory = <IGroupsResource>{
      detachFileFromGroup: function (fileId,successFunction:(group:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.detachFileFromGroup({fileId:fileId},null,
            function (group:Entities.DTOGroupDetails) {

              successFunction(group);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      findGroupAndAttachToFile: function (fileId,successFunction:(g:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.findGroupAndAttachToFile({fileId:fileId},
            function (g:Entities.DTOGroupDetails) {
              if(g) {
                successFunction(g);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

      attachFilesToGroup: function (fileIds:number[],newGroupId,newGroupName,successFunction:(data:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.attachFilesToGroup(null,{ fileIds:fileIds,groupId:newGroupId,groupName:newGroupName},
            function (g:Entities.DTOGroupDetails) {
              successFunction(g);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      createJoinedGroup: function (joinedGroup:Entities.DTONewJoinedGroupDto,successFunction:(g:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.createJoinedGroup(null,joinedGroup,
            function (g:Entities.DTOGroupDetails) {
              if(g) {
                successFunction(g);
              }
              else
              {
                errorFunction(null);
              }
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      deleteJoinedGroup: function (joinedGroupId,successFunction:() => void, errorFunction) {
        return res.deleteJoinedGroup({id:joinedGroupId},null,
            function () {

                successFunction();

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      removeChildGroupFromJoinedGroup: function (joinedGroupId,groupId,successFunction:(group:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.removeChildGroupFromJoinedGroup({id:joinedGroupId,groupId:groupId},null,
            function (group:Entities.DTOGroupDetails) {

                successFunction(group);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      addGroupsToJoinedGroup: function (joinedGroupId,joinedGroup:Entities.DTONewJoinedGroupDto,successFunction:(g:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.addGroupsToJoinedGroup({id:joinedGroupId},joinedGroup,
            function (g:Entities.DTOGroupDetails) {

                successFunction(g);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      getGroups: function (groupIds:string[],successFunction: (groups:Entities.DTOGroup[]) => void , errorFunction) {
        return res.getGroups({groupIds:groupIds},
            function (g:Entities.DTOGroup[]) {

                successFunction(g);

            }
            , function (error) {
              errorFunction(error);
            }
        )
      },

    };
    return factory;

  })


  .factory( 'groupResource', function( $resource ,configuration:IConfig) {
    var res = $resource(configuration.group_details_url, { id: '@id' }, {
      getGroupById:  {
            method: 'GET',
            url:configuration.group_details_url,
            params: {id: '@id'},
        isArray: false
      },
      updateGroupName: {
        method: 'POST',
        url:configuration.group_details_update_url,
        data:{id: '@id',groupName:'@groupName'},
        isArray: false
      },
      recoverGroupFromFreeze: {
        method: 'POST',
        url:configuration.group_recover_from_freeze_url,
        data:{groupId: '@groupId'},
        isArray: false
      },
      groupNamingDebugData:  {
        method: 'GET',
        url:configuration.groups_naming_debug_url,
        params: {id: '@id',weights:'@weights',save:'@save'},
        isArray: false
      },
      setRefinementRules:  {
        method: 'POST',
        url:configuration.subGroups_set_url,
        params: {groupId: '@groupId'},
        isArray: true
      },
      applyRefinementRules:  {
        method: 'POST',
        url:configuration.subGroups_applyRules_url,
        params: {groupId: '@groupId'},
        isArray: false
      },
      deleteAllRefinementRules:  {
        method: 'DELETE',
        url:configuration.subGroups_deleteRules_url,
        params: {groupId: '@groupId'},
        isArray: false
      },
      listRulesForSubGroupCluster:  {
        method: 'Get',
        url:configuration.subGroup_cluster_list_url,
        params: {groupId: '@groupId'},
        isArray: false
      },
    });

    var factory = <IGroupResource>{
      updateGroupName: function (groupId,groupName:string,successFunction:(group:Entities.DTOGroup) => void, errorFunction) {
        return res.updateGroupName(null,{id:groupId,groupName:groupName},
          function (group) {

              successFunction(group);

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      recoverGroupFromFreeze: function (groupId,successFunction:(group:Entities.DTOGroup) => void, errorFunction) {
        return res.recoverGroupFromFreeze({groupId:groupId},null,
          function (group) {

              successFunction(group);

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      getGroupById: function (groupId,successFunction:(g:Entities.DTOGroupDetails) => void, errorFunction) {
        return res.getGroupById({id:groupId},
          function (g:Entities.DTOGroupDetails) {
            if(g) {
              successFunction(g);
            }
            else
            {
              errorFunction(null);
            }
          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      groupNamingDebugData: function (groupId,weightData:GroupNaming.DTOFactors,saveNewName:boolean,
                                      successFunction:(data:GroupNaming.DTOGroupNamingDebugData) => void, errorFunction) {
        return res.groupNamingDebugData(
            {
              id:groupId,
              weights:GroupNaming.DTOFactors.toUrlString(weightData),
              save:false
            },
            function (data:GroupNaming.DTOGroupNamingDebugData) {
                successFunction(data);
            }
            , function (error) {
              errorFunction(error);
            }
        )
      },
      setRefinementRules: function (groupId,tokens:string[],successFunction: (groups:Entities.DtoRefinementRuleDto[]) => void , errorFunction) {
        return res.setRefinementRules({groupId:groupId},tokens,
          function (g:Entities.DtoRefinementRuleDto[]) {

            successFunction(g);

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },

      applyRefinementRules: function (groupId,successFunction: () => void , errorFunction) {
        return res.applyRefinementRules({groupId:groupId},
          function () {

            successFunction();

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      deleteAllRefinementRules: function (groupId,successFunction: () => void , errorFunction) {
        return res.deleteAllRefinementRules({groupId:groupId},
          function () {

            successFunction();

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
      listRulesForSubGroupCluster: function (groupId:string,successFunction: (groups:DTOPagedResult<Entities.DtoRefinementRuleDto>) => void , errorFunction) {
        return res.listRulesForSubGroupCluster({groupId:groupId},
          function (g:DTOPagedResult<Entities.DtoRefinementRuleDto>) {

            successFunction(g);

          }
          , function (error) {
            errorFunction(error);
          }
        )
      },
    };
    return factory;
  })
  .factory( 'groupFacetedSearchResource', function($http, $resource,configuration:IConfig ) {

    var factory= {
      get: function() {
        return {$promise: $http.get(configuration.groups_faceted_search_url)};
      }
    };
    return factory;
  });

///<reference path='../all.d.ts'/>
'use strict';

angular.module('resources.users', [
 ])
interface IUsersResource
{
  updateUser(user:Users.DTOUser,successFunction: (user:Users.DTOUser) => void , errorFunction): void;
  addNewUser(user:Users.DTOUser,successFunction: (user:Users.DTOUser) => void , errorFunction): void;
  deleteUser(userId:number,successFunction: () => void , errorFunction): void;
  getCurrentUserDetails(successFunction: (user:Users.DTOUser) => void, errorFunction): void;
  getCurrentUserDetailsWithTokens(successFunction: (user:Users.DTOUser) => void, errorFunction): void;
  updateCurrentUserDetails(user:Users.DTOUser,successFunction: (user:Users.DTOUser) => void , errorFunction): void;
  updateCurrentUserPassword(oldPassword,newPassword,successFunction: (pwd) => void , errorFunction): void;
  updateUserPassword(userId,newPassword,successFunction: () => void , errorFunction): void;
  getCurrentUserAuthData(successFunction: (user:Users.UserAuthenticationData) => void , errorFunction): void;
  addBizRoleToUser(userId,bizRoleId,successFunction:(user:Users.DTOUser) => void, errorFunction): void;
  removeBizRoleFromUser(user:Users.DTOUser,successFunction: (user:Users.DTOUser) => void , errorFunction): void;
  unlockUser(user:Users.DTOUser,successFunction: (user:Users.DTOUser) => void , errorFunction): void;
  getDemoInfo(successFunction: (data:any) => void , errorFunction): void;
}
interface IUserBizRolesResource
{
  getSystemRoles(successFunction: (roles:Users.DtoSystemRole[]) => void , errorFunction): void;
  getBizRoles(successFunction: (roles:Users.DtoBizRole[]) => void , errorFunction): void;
  getBizRolesLdapGroups(successFunction: (groups:Users.DtoLdapGroup[]) => void , errorFunction): void;
  addNewBizRole(role:Users.DtoBizRoleSummaryInfo,successFunction:(newUser:Users.DtoBizRoleSummaryInfo) => void, errorFunction): void;
  deleteBizRole(roleId:number,successFunction: () => void , errorFunction): void;
  updateBizRole(role:Users.DtoBizRoleSummaryInfo,successFunction:(updatedRole:Users.DtoBizRoleSummaryInfo) => void, errorFunction): void;
  getBizRoleAssignedUsers(roleId,successFunction:(users:Users.DTOUser[],totalElements:number) => void, errorFunction): void;
}

interface ILDapUsersResource
{
  getLDapSettings(successFunction:(settings:AppSettings.DtoLdapConnectionDetails) => void, errorFunction): void;
  updateLDapSettings(settings:AppSettings.DtoLdapConnectionDetails, successFunction:(settingsUpdated:AppSettings.DtoLdapConnectionDetails) => void, errorFunction): void;
  testLdapConnection (settings:AppSettings.DtoLdapConnectionDetails,successFunction:(success:boolean,reason?:string) => void, errorFunction): void;
}


angular.module('resources.users', [
    ])

   .factory( 'usersResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.user_list_url, '', {

        updateUser: {
          method: 'POST',
          url:configuration.user_edit_url,
          params: {id: '@id'},
          isArray: false },
        addNewUser: {
          method: 'PUT',
          url: configuration.user_add_url,
          isArray: false },
        deleteUser: {
          method: 'DELETE',
          url: configuration.user_delete_url,
          params: {id: '@id'},
          isArray: false},
        getCurrentUserAuthData: {
          method: 'GET',
          url:configuration.user_current_auth_url,
          isArray: false },
        getDemoInfo: {
          method: 'GET',
          url:configuration.user_demo_info_url,
          isArray: false },
        getCurrentUserDetails: {
          method: 'GET',
          url:configuration.user_current_url,
          isArray: false },
        getCurrentUserDetailsWithTokens: {
          method: 'GET',
          url:configuration.user_current_with_tokens_url,
          isArray: false },
       updateCurrentUserDetails: {
          method: 'POST',
          url:configuration.user_current_url,
          isArray: false },
        updateCurrentUserPassword: {
          method: 'POST',
          url:configuration.user_current_update_password_url,
          isArray: false },
        updateUserPassword: {
          method: 'POST',
          url:configuration.user_update_password_url,
          params: {userId: '@userId'},
          isArray: false },
        addBizRoleToUser: {
          method: 'PUT',
          url:configuration.user_assign_bizRole_url,
          params: {userId: '@userId',bizRoleId: '@bizRoleId'},
          isArray: false },
        removeBizRoleFromUser: {
          method: 'DELETE',
          url: configuration.user_remove_assigned_bizRole_url,
          params: {userId: '@userId',bizRoleId: '@bizRoleId'},
          isArray: false},
        unlockUser: {
          method: 'POST',
          url:configuration.user_unlock_url,
          params: {userId: '@userId'},
          isArray: false }
      });

      var factory = <IUsersResource>{

        updateUser: function (user:Users.DTOUser,successFunction:(user) => void, errorFunction) {
          return res.updateUser({id:user.id},JSON.stringify(user),
              function (user) {
                successFunction(user);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        addNewUser: function (user:Users.DTOUser,successFunction:(newUser:Users.DTOUser) => void, errorFunction) {
          return res.addNewUser(null,JSON.stringify(user),
              function (newUser) {
                successFunction(newUser);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        deleteUser: function (userId:number,successFunction:() => void, errorFunction) {
          return res.deleteUser({id:userId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getCurrentUserAuthData: function (successFunction:(user:Users.UserAuthenticationData) => void, errorFunction) {
          const anyParent: any = window.parent;
          if(anyParent && anyParent !== window &&  anyParent.shellApi &&  anyParent.shellApi.getUserInfo) {
            return anyParent.shellApi.getUserInfo().then(result => successFunction(result)
            , error => errorFunction(error))
          }
          return res.getCurrentUserAuthData({pr:(new Date()).getTime()},
              function (authData:Users.UserAuthenticationData) {

                successFunction(authData);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getCurrentUserDetails: function (successFunction:(user:Users.DTOUser) => void, errorFunction) {
          return res.getCurrentUserDetails(null,
              function (user:Users.DTOUser) {
                successFunction(user);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getCurrentUserDetailsWithTokens: function (successFunction:(user:Users.DTOUser) => void, errorFunction) {
          return res.getCurrentUserDetailsWithTokens(null,
            function (user:Users.DTOUser) {
              successFunction(user);
            }
            , function (error) {
              errorFunction(error);
            }
          )
        },
        getDemoInfo: function (successFunction, errorFunction) {
          return res.getDemoInfo(null,
            function (demoInfo) {
              successFunction(demoInfo.data);
            }
            , function (error) {
              errorFunction(error);
            }
          )
        },
        updateCurrentUserDetails: function (user:Users.DTOUser,successFunction:(user:Users.DTOUser) => void, errorFunction) {
          return res.updateCurrentUserDetails(null,user,
              function (user) {
                successFunction(user);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        updateCurrentUserPassword: function (oldPassword,newPassword,successFunction:() => void, errorFunction) {
          return res.updateCurrentUserPassword(null,{oldPassword:oldPassword,newPassword:newPassword},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        updateUserPassword: function (userId,newPassword,successFunction:() => void, errorFunction) {
          return res.updateUserPassword({userId:userId},{newPassword:newPassword},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        unlockUser: function (user,successFunction:() => void, errorFunction) {
          return res.unlockUser({userId:user.id},
            function () {
              successFunction();
            }
            , function (error) {
              errorFunction(error);
            }
          )
        },
        addBizRoleToUser: function (userId,bizRoleId,successFunction:(user:Users.DTOUser) => void, errorFunction) {
          return res.addBizRoleToUser({userId:userId,bizRoleId:bizRoleId},null,
              function (newUser) {
                successFunction(newUser);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        removeBizRoleFromUser: function (userId,bizRoleId,successFunction:() => void, errorFunction) {
          return res.removeBizRoleFromUser({userId:userId,bizRoleId:bizRoleId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
      };

      return factory;
    })
   .factory( 'userBizRolesResource', function( $resource,configuration:IConfig, ldapUsersResource:ILDapUsersResource ) {
      var res = $resource(configuration.user_list_url, '', {

        updateBizRole: {
          method: 'POST',
          url:configuration.bizRole_edit_url,
          params: {id: '@id'},
          isArray: false },
        addNewBizRole: {
          method: 'PUT',
          url: configuration.bizRole_add_url,
          isArray: false },
        deleteBizRole: {
          method: 'DELETE',
          url: configuration.bizRole_delete_url,
          params: {id: '@id'},
          isArray: false},
        getBizRoles: {
          method: 'GET',
          url:configuration.bizRole_list_url,
          isArray: true },
        getBizRolesLdapGroups: {
          method: 'GET',
          url:configuration.bizRole_ldap_list_url,
          isArray: true },
        getSystemRoles: {
          method: 'GET',
          url:configuration.user_systemRole_list_url,
          isArray: true },
        getBizRoleAssignedUsers: {
          method: 'GET',
          params: {bizRoleId: '@bizRoleId'},
          url:configuration.bizRole_assigned_users_url,
          isArray: false },
      });

      var factory = <IUserBizRolesResource>{

        updateBizRole: function (role:Users.DtoBizRoleSummaryInfo,successFunction:(updatedRole:Users.DtoBizRoleSummaryInfo) => void, errorFunction) {
          return res.updateBizRole({id:role.bizRoleDto.id},JSON.stringify(role),
              function (updatedRole) {
                successFunction(updatedRole);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        addNewBizRole: function (role:Users.DtoBizRoleSummaryInfo,successFunction:(newUser:Users.DtoBizRoleSummaryInfo) => void, errorFunction) {
          return res.addNewBizRole(null,JSON.stringify(role),
              function (newUser) {
                successFunction(newUser);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        deleteBizRole: function (roleId:number,successFunction:() => void, errorFunction) {
          return res.deleteBizRole({id:roleId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getBizRoles: function (successFunction:(roles:Users.DtoBizRole[]) => void, errorFunction) {
          return res.getBizRoles(
            {
              take:299,
              page:1,
              pageSize:299
            },
            function (roles:Users.DtoBizRole[]) {
                successFunction(roles);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },

        getBizRolesLdapGroups: function(successFunction:(groups:Users.DtoLdapGroup[]) => void, errorFunction) {
          return res.getBizRolesLdapGroups(null,
            function (groups:Users.DtoLdapGroup[]) {
              successFunction(groups);
            }
            , function (error) {
              errorFunction(error);
            }
          )
        },
        getSystemRoles: function (successFunction:(roles:Users.DtoSystemRole[]) => void, errorFunction) {
          return res.getSystemRoles(null,
              function (roles:Users.DtoSystemRole[]) {
                successFunction(roles);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        getBizRoleAssignedUsers: function(bizRoleId,successFunction:(users:Users.DTOUser[],totalElement) => void, errorFunction) {
          return res.getBizRoleAssignedUsers({bizRoleId:bizRoleId},
              function (data:Entities.DTOQueryEntityResult<Users.DTOUser>) {

                successFunction(data.content,data.numberOfElements);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },

      };

      return factory;
    })

   .factory( 'ldapUsersResource', function( $resource,configuration:IConfig,propertiesUtils ) {
        var res = $resource(configuration.ldap_settings_url, '', {
          getLDapSettings: {
            method: 'GET',
            isArray: false },
          updateLDapSettings: {
            method: 'POST',
            isArray: false },
          testLdapConnection: {
            url:configuration.ldap_settings_test_connection_url,
            method: 'POST',
            isArray: false },

        });
        var factory = <ILDapUsersResource>{

          getLDapSettings: function (successFunction:(settings:AppSettings.DtoLdapConnectionDetails) => void, errorFunction) {
            return res.getLDapSettings(null,
              function (settings:AppSettings.DtoLdapConnectionDetails) {
                successFunction(settings);
              }
              , function (error) {
                errorFunction(error);
              }
            )
          },
          updateLDapSettings: function (settings:AppSettings.DtoLdapConnectionDetails, successFunction:(settingsUpdated:AppSettings.DtoLdapConnectionDetails) => void, errorFunction) {
            return res.updateLDapSettings(null,settings,
              function (settingsUpdated:AppSettings.DtoLdapConnectionDetails) {
                if(settingsUpdated) {

                  successFunction(settingsUpdated);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
            )
          },
          testLdapConnection: function (settings:AppSettings.DtoLdapConnectionDetails, successFunction:(success:boolean,reason:string) => void, errorFunction) {
            return res.testLdapConnection(null,settings,
              function (result:DTORequestBase) {
                if(result) {
                  successFunction(result.result,result.reason);
                }
                else
                {
                  errorFunction(null);
                }
              }
              , function (error) {
                errorFunction(error);
              }
            )
          },


        }
        return factory;
  })

///<reference path='..\all.d.ts'/>
'use strict';
interface ISearchPatternsResource
{
  updateSearchPattern(searchPattern:Operations.DTOSearchPattern,successFunction: (pattern:Operations.DTOSearchPattern) => void , errorFunction): void;
  updateSearchPatternActiveState(searchPatternId,active:boolean,successFunction: (pattern:Operations.DTOSearchPattern) => void , errorFunction): void;
  addNewSearchPattern(searchPattern:Operations.DTOSearchPattern,successFunction: (pattern:Operations.DTOSearchPattern) => void , errorFunction): void;
  deleteSearchPattern(searchPatternId:number,successFunction: () => void , errorFunction): void;
  uploadListItemsFromFile(fileData,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction):void;
  updateUploadListItemsStatus(importId:any,status:string,successFunction: () => void , errorFunction):void;
  cancelUploadListItemsFromFile(importId):void;

}

angular.module('resources.searchPatterns', [
    ])

    .factory( 'searchPatternsResource', function( $resource,configuration:IConfig ) {
      var res = $resource(configuration.searchPattern_list_url, '', {

       updateSearchPattern: {
          method: 'POST',
          url:configuration.searchPattern_edit_url,
          params: {id: '@id'},
          isArray: false },
        updateSearchPatternActiveStateDisabled: {
          method: 'POST',
          url:configuration.searchPattern_edit_not_active_url,
          params: {id: '@id'},
          isArray: false },
        updateSearchPatternActiveStateEnabled: {
          method: 'POST',
          url:configuration.searchPattern_edit_active_url,
          params: {id: '@id'},
          isArray: false },
       addNewSearchPattern: {
          method: 'PUT',
          url: configuration.searchPattern_add_url,
          isArray: false },
       deleteSearchPattern: {
          method: 'DELETE',
          url: configuration.searchPattern_delete_url,
          params: {id: '@id'},
          isArray: false },
        uploadListItemsFromFile: {
          method: 'POST',
          url: configuration.searchPattern_items_upload_url,
          params: {id: '@id'},
          headers: {'content-type':undefined},//
          //   responseType: "arraybuffer",
          transformRequest:angular.identity,
          isArray: false },
        updateUploadListItemsStatus :{
          method: 'POST',
          url: configuration.searchPattern_items_upload_status_update_url,
          params: {importId:'@importId',newStatus:'@newStatus'},
          isArray: false },
        cancelUploadListItemsFromFile :{
          method: 'DELETE',
          url: configuration.searchPattern_cancel_import_url,
          params: {importId:'@importId'},
          isArray: false },
      });
      var  searchPatternsDic;

      var factory = <ISearchPatternsResource>{

        updateSearchPattern: function (searchPattern:Operations.DTOSearchPattern,successFunction:(pattern) => void, errorFunction) {
          return res.updateSearchPattern({id:searchPattern.id}, angular.toJson(searchPattern),
              function (pattern) {
                successFunction(pattern);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        updateSearchPatternActiveState: function (searchPatternId,active:boolean,successFunction:(pattern:Operations.DTOSearchPattern) => void, errorFunction) {
          if(active) {
            return res.updateSearchPatternActiveStateEnabled({id: searchPatternId}, null,
                function (pattern) {
                  successFunction(pattern);
                }
                , function (error) {
                  errorFunction(error);
                }
            )
          }
          else {
            return res.updateSearchPatternActiveStateDisabled({id: searchPatternId}, null,
                function (pattern) {
                  successFunction(pattern);
                }
                , function (error) {
                  errorFunction(error);
                }
            )
          }
        },
        addNewSearchPattern: function (searchPattern:Operations.DTOSearchPattern,successFunction:(newSearchPattern:Operations.DTOSearchPattern) => void, errorFunction) {
          return res.addNewSearchPattern(null, angular.toJson(searchPattern),
              function (newSearchPattern) {
                successFunction(newSearchPattern);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        deleteSearchPattern: function (searchPatternId:number,successFunction:() => void, errorFunction) {
          return res.deleteSearchPattern({id:searchPatternId},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        uploadListItemsFromFile: function (fileData,successFunction: (importResult:Entities.DTOBizListImportResult) => void , errorFunction) {
          return res.uploadListItemsFromFile({},fileData,
              function (importResult:Entities.DTOBizListImportResult) {
                successFunction(importResult);
              }
              , function (error) {
                errorFunction(error);
              }
          )
        },
        cancelUploadListItemsFromFile: function (importId) {
          return res.cancelUploadListItemsFromFile({importId:importId},
              function () {

              }
              , function (error) {
              }
          )
        },
        updateUploadListItemsStatus: function (importId:any,status:string,successFunction: () => void , errorFunction) {
          return res.updateUploadListItemsStatus({importId:importId,newStatus:status},
              function () {
                successFunction();
              }
              , function (error) {
                errorFunction(error);
                console.log("failed to updateUploadListItemsStatus " + error);
              }
          )
        },
      };

      return factory;
    })

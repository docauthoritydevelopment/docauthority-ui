module Dashboards {



  import SavedDiscoverFilter = Entities.SavedFilterDto;

  export class DTOSummaryReport  {
    counterReportDtos:DtoCounterReport[];
    stringReportDtos: DtoStringReport[];
    stringListReportDtos: DtoStringListReport[];
    multiValueListDto: DtoMultiValueList;
  }

  export class DtoCounterReport  {
    counter:number;
    name:string;
    description:string;
    extraInfo:any;
    totalCounter:number;

   id:any;
    constructor(name:string,desc:string,counter:number,id?:any,extraInfo?:any,totalCounter?:number) {
      this.name=name;
      this.description=desc;
      this.counter=counter;
      this.extraInfo=extraInfo;
      this.totalCounter = totalCounter;
      this.id=id;
    }

    static Empty():DtoCounterReport{
      var g = new DtoCounterReport('a','b',1, 2, 3);
      return g;
    }
  }

  export class DtoItemCounter<T>  {
    count:number;
    item:T;
    timestamp:Date;
    description:string;
    constructor(item:T,desc:string,count:number,timestamp?:Date) {
      this.item=item;
      this.timestamp=timestamp;
      this.description=desc;
      this.count=count;
    }

    static Empty():DtoItemCounter<string>{
      var g = new DtoItemCounter<string>('a','b',1,new Date(Date.now()));
      return g;
    }
  }


  export class DtoStringReport  {
    name:string;
    description:string;
    value:number;

  }

  export class DtoStringListReport  {
    stringList:string[];
    name:string;
    description:string;

  }
  export class DtoListReport<T>  {
    data:T[];
    name:string;
    description:string;
    constructor(n:string,d:string,data:T[]) {
      this.name=n;
      this.data=data;
      this.description=d;
    }
  }

  export class DtoMultiValueList  {
    data:DtoCounterReport[];
    name:string;
    type:string;
    constructor(n:string,t:string,d:DtoCounterReport[]) {
      this.name=n;
      this.type=t;
      this.data=d;
    }
  }

  export interface ICounterItem
  {

  }

  export class DtoFileTypeState  implements ICounterItem {
    fileState:EFileState;
    fileType:EFileType;
  }

  export enum  EFileState {
    SCANNED,
    INGESTED,
    ANALYSED,
    ERROR,
  }
  export enum  EFileType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    msWord= <any>"WORD",
    pdf= <any>"PDF",
    text= <any>"TEXT",
    msExcel= <any>"EXCEL",
    csv= <any>"CSV",
    other= <any>"OTHER",
    scannedPDF= <any>"SCANNED_PDF",
    package= <any>"PACKAGE",
    mail= <any>"MAIL"
  }
  export enum  EFileItemType {
    attachment= <any>"ATTACHMENT",
    message= <any>"MESSAGE"
  }

export class DtoCrawlFileChangeLog  {
  id:number;
  fileId:number;
  crawlEventType:string;
  fileName:string;
  baseName:string;
  rootFolderId:number;
  runId:number;
  timeStamp:number;
  processed:boolean;

 static Empty():DtoCrawlFileChangeLog{
    var g = new DtoCrawlFileChangeLog();
    g.id=111;g.crawlEventType='b';g.fileName='c';g.baseName='d'; g.processed=true; g.runId=2; g.timeStamp=3;
    return g;
  }
  static crawlEventType_NEW= 'NEW';
  static crawlEventType_DELETE="DELETE";
  static crawlEventType_UPDATE_CONTENT="UPDATE_CONTENT";
  static crawlEventType_UPDATE_OWNER_ACL="UPDATE_OWNER_ACL";

}
  //export class DtoFileCrawlingAverageThroughput  {
  //  averageScanThroughput:number;
  //  averageWordIngestThroughput:number;
  //  averageExcelIngestThroughput:number;
  //  averagePdfIngestThroughput:number;
  //
  //}
  //
  //export class FileCrawlingStateView {
  //  runStatus:DtoFileCrawlingState;
  //  isRunning:boolean;
  //  isFullRun:boolean;
  //  isRootFolderRun:boolean;
  //  lastRunElapsedTime:number;
  //  scanRunningElapsedTime:number;
  //  isLastRunFailed:boolean;
  //  isLastRunFinished:boolean;
  //  enableStopScanRunButton:boolean;
  //  phaseName:string;
  //  isInScanPhase:boolean;
  //  isExtractPhase:boolean;
  //  currentPhaseProgressPercentage:number;
  //  currentPhaseProgressPercentageStr:string;
  //  runningRate:number;
  //  runArgumentsCount:number;
  //  runArguments:string;
  //  extractInProcessJobs:any[];
  //}
  //
  //export class DtoFileCrawlingState  {
  //  stateString:number;
  //  runOperationList:string[];
  //  lastRunStartTime:number;
  //  lastRunStatus:string;
  //  lastRunStopTime:number;
  //  runningRate:number;
  //  rate:number;
  //  fileCrawlerRunning:boolean;
  //  fileCrawlPhase:string;
  //  rootFolderId:number;
  //  rootFolderPath:string;
  //  rootFolderNickname:string;
  //  scheduleGroupId:number;
  //  scheduleGroupName:string;
  //  scannedTypes:string;
  //  currentPhaseProgressCounter:number;
  //  currentPhaseProgressFinishMark:number;
  //  manualRun:boolean;
  //
  //  static lastRunStatus_FINISHED_SUCCESSFULLY = 'FINISHED_SUCCESSFULLY';
  //  static lastRunStatus_NA="NA";
  //  static lastRunStatus_FAILED="FAILED";
  //
  //}

  export class DtoPhaseDetails  {

    name:string;
    jobType:Operations.EJobType;
    phaseCounter:number;
    phaseMaxMark:number;
    phaseStart:number;
    phaseLastStopTime:number;
    pauseDuration:number;
    phaseError:string;
    failedTasksCount:number;
    jobState:EJobState;
    stateDescription:string;
    jobId:number;
    tasksPerSecRunningRate:number;
    tasksPerSecRate:number;
    runningRateDurationInMsc:number;
    completionStatus:EJobCompletionStatus;
    taskRetriesCount:number;
    bizListId:number;

    static Empty():DtoPhaseDetails{
      var g = new DtoPhaseDetails();
     g.name='a'; g.jobId=1;  g.tasksPerSecRate=2;  g.failedTasksCount=9; g.phaseCounter=4; g.phaseMaxMark=5; g.phaseStart=6;g.completionStatus =EJobCompletionStatus.OK;
      g.pauseDuration=7; g.phaseLastStopTime=8;g.tasksPerSecRunningRate=10;g.runningRateDurationInMsc=12; g.phaseError='b';g.jobState=EJobState.IN_PROGRESS;g.stateDescription='c';g.jobType=Operations.EJobType.ANALYZE;
      g.taskRetriesCount=13;g.bizListId=14;
      return g;
    }


    static status_NOT_RUNNING= 'NOT_RUNNING';
    static status_SCAN="SCAN";
    static status_INGEST="INGEST";
    static status_ANALYZE="ANALYZE";
    static status_EXTRACT_ENTITIES="EXTRACT_ENTITIES";
    static status_FINALIZE="FINALIZE";

    static runStatus_FAILED="FAILED";
    static runStatus_RUNNING="RUNNING";
    static runStatus_NA="NA";
    static runStatus_FINISHED_SUCCESSFULLY="FINISHED_SUCCESSFULLY";

  }

  export enum EJobCompletionStatus {
    UNKNOWN = <any>"UNKNOWN",
    OK = <any>"OK",
    CANCELLED = <any>"CANCELLED",
    ERROR = <any>"ERROR",
    TIMED_OUT = <any>"TIMED_OUT",
    DONE_WITH_ERRORS = <any>"DONE_WITH_ERRORS",
    SKIPPED = <any>"SKIPPED",
    STOPPED = <any>"STOPPED"
  }


  export enum EJobState
  {
    NEW= <any>"NEW",
    IN_PROGRESS= <any>"IN_PROGRESS",
    PAUSED= <any>"PAUSED",
    DONE= <any>"DONE",
    READY= <any>"READY",
    WAITING= <any>"WAITING",
    PENDING= <any>"PENDING",
  }

  export class DtoProcessingError implements Entities.IQuerybleEntity  {
    id:number;
    type:string;
    fileName:string;
    baseName:string;
    fileId:number;
    text:string;
    detailText:string;
    rootFolder:string;
    rootFolderId:number;
    folder:string;
    fileType:string;
    runId:number;
    contentId:number;
    timeStamp:number;
    duplicationsCount:number;

    static Empty():DtoProcessingError{
      var g = new DtoProcessingError();
      g.id=111;g.type='b';g.fileName='c'; g.fileId=33;g.text='d';g.detailText='e';g.rootFolderId=11; g.baseName='f';g.fileType='h';g.folder='';g.rootFolder='i';g.runId=2; g.timeStamp=3;g.contentId=4;g.duplicationsCount=5;
      return g;
    }

  }



  export class DtoTrendChart {
    name:string;
    chartDisplayType:EChartDisplayType ;
    chartValueFormatType:EChartValueFormatType;
    ownerId:number;
    trendChartCollectorId:number;
    seriesType:string;
    valueType:string;
    sortType:ESortType ;
    filters:string;
    viewConfiguration:string ;
    viewTo:Entities.DTODateRangePoint ;
    viewSampleCount:number ;
    viewResolutionMs:number ;
    viewFrom:Entities.DTODateRangePoint;
    id:number;
    hidden:boolean;


  }

  export enum  EChartValueFormatType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    NUMERIC= <any>"NUMERIC",
    PERCENTAGE= <any>"PERCENTAGE",

  }
  export class TrendChartViewConfiguration
  {
    totalSeriesHidden:boolean;
    otherSeriesHidden:boolean;
    totalSeriesType:string;
    otherSeriesType:string;
    viewResolutionStep:number;
    showInMenu:boolean;

  }

  export enum  EChartDisplayType  {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    PIE= <any>"PIE",
    BAR_HORIZONTAL= <any>"BAR_HORIZONTAL",
    BAR_VERTICAL= <any>"BAR_VERTICAL",
    TREND_BAR= <any>"TREND_BAR",
    TREND_BAR_STACKED= <any>"TREND_BAR_STACKED",
    TREND_LINE= <any>"TREND_LINE",
  }

  export enum  ESortType  {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    NAME= <any>"NAME",

  }

  export class DtoTrendChartDataSummary {
    trendChartDto:DtoTrendChart;
    trendChartSeriesDtoList:DtoTrendChartSeries[] ;
    startTime:number;
    endTime:number;
    viewResolutionMs:number;
    sampleCount :number;
  }

  export class DtoTrendChartSeries {
    id:number;
    name:string ;
    filter:string;
    values:any[];

  }
}

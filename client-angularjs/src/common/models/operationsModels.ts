module Operations {

  import DtoPhaseDetails = Dashboards.DtoPhaseDetails;
  export class ActiveRunsView {
    activeRuns:DtoRunSummaryInfo[];
    activeRunsView:DtoRunSummaryInfoView[];
  }
  export class DtoRunSummaryInfoView {
    id;
    elapsedTime:number;
    startTime:number;
    endTime:number;
    isRunning:boolean;
    nickName:string;
    jobName:string;
    jobType:string;
    runStatus:ERunStatus;
    isManualRun:boolean;
    rootFolderId:number;
    scheduleGroupName:string;
    scheduleGroupId:number;
    currentPhaseProgressCounter:number;
    currentPhaseProgressPercentage:number;
    currentPhaseProgressFinishMark:number;
    estimatedPhaseFinish:number;
    tasksPerSecRate:number;
    tasksPerSecRunningRate:number;
  }
  export class ServerComponentsView {
    components:Operations.DtoClaComponent[];
    componentTypes:NameValuePair[];
    faultyComponentsCount:number;

  }

  export class ApplyToSolrStatusView {
    applyStatus:DTOApplyToSolrStatus;
    currentPercentage:number;
    isRunning:boolean;
    enableStartApplyButton:boolean;


  }
  export class DTOApplyToSolrStatus {
    applyToSolrStatus:string;
    applyCounterMaxMark:number;
    applyCounter:number;

    static Empty():DTOApplyToSolrStatus {
      var g = new DTOApplyToSolrStatus();
      g.applyToSolrStatus = 'a';
      g.applyCounterMaxMark = 2;
      g.applyCounter = 3;
      return g;
    }

    static status_NOT_STARTED = 'NOT_STARTED';
    static status_IN_PROGRESS = "IN_PROGRESS";
    static status_CANCELED = "CANCELED";
    static status_DONE = "DONE";
  }

  export class DtoLdapConnectionDetails  {
    id:number;
    name:string;
    ssl:boolean;
    enabled:boolean;
    host:string;
    manageDN:string;
    port:number;
    managerPass:string;
    serverDialect:ELdapServerDialect1;

    static Empty(): DtoLdapConnectionDetails {
      var g = new DtoLdapConnectionDetails();
      g.id = 1; g.name = 'a';  (<any>g).ssl = 'b'; (<any>g).enabled = 'c'; g.host = 'd'; g.manageDN = 'e';g.port = 2;g.managerPass = 'f';g.serverDialect=ELdapServerDialect1.ACTIVE_DIRECTORY_LEGACY;
      return g;
    }
  }

  export enum ELdapServerDialect1   {
    ACTIVE_DIRECTORY= <any>"ACTIVE_DIRECTORY",
    ACTIVE_DIRECTORY_LEGACY= <any>"ACTIVE_DIRECTORY_LEGACY",
    ApacheDS= <any>"ApacheDS",
  }


  export class DtoMediaTypeConnectionDetails {
    id:number;
    name:string;
    username:string;
    url:string;
    mediaType:EMediaType;
    connectionParametersJson: string;


    static Empty():DtoMediaTypeConnectionDetails {
      var g = new DtoMediaTypeConnectionDetails();
      g.id = 1;
      g.name = 'b';
      g.url = 'c';
      (<any>g).mediaType = 'd';
      g.username = 'e';
      return g;
    }
  }
  export class DtoBoxConnectionDetails  extends DtoMediaTypeConnectionDetails {
    jwt: string;
    type:string;

    constructor() {
      super();
      this.type = '.BoxConnectionDetailsDto';
      this.mediaType = EMediaType.BOX;

    }
  }

  export class DtoExchange365ConnectionDetails  extends DtoMediaTypeConnectionDetails {
    type:string;
    exchangeConnectionParametersDto:DtoExchangeConnectionParameters;

    constructor() {
      super();
      this.type = '.Exchange365ConnectionDetailsDto';
      this.mediaType = EMediaType.EXCHANGE365;
    }
  }

  export class DtoSharePointConnectionParameters {
    username:string;
    url:string;
    password:string;
    domain:string;

    static Empty():DtoSharePointConnectionParameters {
      var g = new DtoSharePointConnectionParameters();
      g.domain = 'a';
      g.url = 'b';
      g.username = 'c';
      g.domain = 'e';
      return g;
    }
  }

  export class DtoExchangeConnectionParameters {
    username:string;
    url:string;
    domain:string;
    tenantId:string;
    applicationId:string;
    password:string;

    static Empty():DtoExchangeConnectionParameters {
      var g = new DtoExchangeConnectionParameters();
      g.domain = 'a';
      g.url = 'b';
      g.username = 'c';
      g.tenantId = 'e';
      g.applicationId = 'f';
      return g;
    }
  }

  export class DtoMipConnectionParameters {
    clientId:string;
    password:string;
    resource:string;

    static Empty():DtoMipConnectionParameters {
      var g = new DtoMipConnectionParameters();
      g.clientId = 'a';
      g.password = 'b';
      g.resource = 'c';
      return g;
    }
  }

  export class MicrosoftConnectionDetailsDtoBase  extends DtoMediaTypeConnectionDetails {

    sharePointConnectionParametersDto:DtoSharePointConnectionParameters;
    type:string;
  }

  export class DtoSharePointConnectionDetails extends MicrosoftConnectionDetailsDtoBase  {

    constructor()
    {
      super();
      this.type = '.SharePointConnectionDetailsDto';
      this.mediaType = EMediaType.SHARE_POINT;
    }
  }
  export class DtoOneDriveConnectionDetails extends MicrosoftConnectionDetailsDtoBase  {

    constructor()
    {
      super();
      this.type = '.OneDriveConnectionDetailsDto';
      this.mediaType = EMediaType.ONE_DRIVE;
    }
  }
  export class DtoMipConnectionDetails extends DtoMediaTypeConnectionDetails  {
    type:string;
    mipconnectionParametersDto:DtoMipConnectionParameters;

    constructor()
    {
      super();
      this.type = 'com.cla.connector.domain.dto.media.MIPConnectionDetailsDto';
      this.mediaType = EMediaType.MIP;
    }
  }

  export enum EAclEntryType{
    ALLOW = <any>"ALLOW",
    DENY = <any>"DENY",
    AUDIT = <any>"AUDIT",
    ALARM = <any>"ALARM"
  }

  export enum  EMediaType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    FILE_SHARE = <any>"FILE_SHARE",
    SHARE_POINT = <any>"SHARE_POINT",
    ONE_DRIVE = <any>"ONE_DRIVE",
    BOX = <any>"BOX",
    EXCHANGE365= <any>"EXCHANGE_365",
    MIP = <any>"MIP"
  }

  export enum EDirectoryExistStatus {
    YES = <any>"YES",
    NO = <any>"NO",
    UNKNOWN = <any>"UNKNOWN"
  }

  export class DtoRootFolderSharePermission {
    id : number;
    rootFolderId:number;
    user:string;
    aclEntryType:EAclEntryType ;
    mask:number;
    textPermissions:string[];
    rootFolderIds: number[];
  }

  export class DtoRootFolder {
    id:number;
    lastRunId:number;
    path:string;
    realPath:string;
    mailboxGroup:string;
    docStoreId:number;
    deleted:boolean;
    fromDateScanFilter:number;
    numberOfFoldersFound:number;
    folderExcludeRulesCount:number;
    scannedFilesCountCap:number;
    scannedMeaningfulFilesCountCap:number;
    storeLocation:string;
    storePurpose:string;
    storeSecurity:string;
    extractBizLists:boolean;
    mediaType:EMediaType;
    rescanActive:boolean;
    description:string;
    rootFolderSharePermissions: DtoRootFolderSharePermission[];
    fileTypes:Dashboards.EFileType[];
    ingestFileTypes:Dashboards.EFileType[];
    mediaConnectionDetailsId:number;
    mediaConnectionName:string;
    nickName:string;
    customerDataCenterDto:Operations.DtoCustomerDataCenter;
    reingestTimeStampMs: number;
    reingest: boolean;
    isAccessible:EDirectoryExistStatus;
    shareFolderPermissionError:boolean;
    departmentId:number;
    departmentName:string;

    static Empty():DtoRootFolder {
      var g = new DtoRootFolder();
      g.id = 1;
      g.path = 'b';
      g.docStoreId = 2;
      g.numberOfFoldersFound = 3;
      g.scannedFilesCountCap = 4;
      (<any>g).rescanActive = 'd';
      g.folderExcludeRulesCount = 5;
      (<any>g).mediaType = 'e';
      (<any>g).extractBizLists = 'f';
      g.mediaConnectionDetailsId = 6;
      g.nickName = 'g';
      g.realPath = 'h';
      g.mediaConnectionName = 'i';
      g.storeLocation = 'j';
      g.storePurpose = 'k';
      g.storeSecurity = 'l';
      g.description = 'm';
      g.reingestTimeStampMs = 7;
      g.lastRunId =8;
      g.mailboxGroup='z';
      (<any>g).reingest = 'n';
      (<any>g).customerDataCenterDto = 'o';
      (<any>g).isAccessible = 'p';
      (<any>g).fromDateScanFilter = 9;
      g.departmentId = 10;
      g.departmentName = 'q';
      (<any>g).ingestFileTypes = 'r';
      return g;
    }

    static  storeLocation_UNDEFINED = 'UNDEFINED';
    static  storeLocation_LOCAL_DATACENTER = 'LOCAL_DATACENTER';
    static  storeLocation_REMOTE_DATACENTER = 'REMOTE_DATACENTER';
    static  storeLocation_CLOUD_FILESHARE = 'CLOUD_FILESHARE';
    static  storeLocation_CLOUD_SAAS = 'CLOUD_SAAS';
    static  storeLocation_CLOUD_PASS = 'CLOUD_PASS';
    static  storeLocation_OTHER = 'OTHER';

    static  storePurpose_PUBLIC = 'PUBLIC';
    static  storePurpose_DEPARTMENT = 'DEPARTMENT';
    static  storePurpose_PERSONAL = 'PERSONAL';
    static  storePurpose_UNDEFINED = 'UNDEFINED';
    static  storePurpose_IT = 'IT';

    static  storeSecurity_CLEAR = 'CLEAR';
    static  storeSecurity_ENCRYPTED = 'ENCRYPTED';


  }

  export class DtoRootFolderSummaryInfo {

    rootFolderDto:DtoRootFolder;
    crawlRunDetailsDto:DtoCrawlRunDetails;
    runStatus:ERunStatus;
    accessible:boolean;
    accessibleUnknown:boolean;
    customerDataCenterReady:boolean;
    scheduleGroupDtos:DtoScheduleGroup[];
    departmentDto:DtoDepartment;

    static Empty():DtoRootFolderSummaryInfo {
      var g = new DtoRootFolderSummaryInfo();
      (<any>g).rootFolderDto = 'a';
      (<any>g).crawlRunDetailsDto = 'b';
      (<any>g).runState = 'c';
      (<any>g).rootFolderState = 'd';
      (<any>g).customerDataCenterReady = 'f';
      g.accessible = true;
      g.accessibleUnknown = false;
      (<any>g).scheduleGroupDtos = 'e';
      (<any>g).departmentDto = 'g';

      return g;
    }


  }

  export enum  ERunOperation {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    SCAN = <any>"SCAN",
    INGEST = <any>"INGEST",
    ANALYZE = <any>"ANALYZE",
    EXTRACT = <any>"EXTRACT"
  }

  export enum ERunStatus{  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    STOPPED = <any>"STOPPED",
    FAILED = <any>"FAILED",
    RUNNING = <any>"RUNNING",
    NA = <any>"NA",
    FINISHED_SUCCESSFULLY = <any>"FINISHED_SUCCESSFULLY",
    FINISHED_WITH_WARNINGS = <any>"FINISHED_WITH_WARNINGS",
    STOPPING = <any>"STOPPING",
    PAUSED = <any>"PAUSED",
    SUSPENDED = <any>"SUSPENDED"
  }

  export enum PauseReason {
    USER_INITIATED = <any>'USER_INITIATED',
    SOLR_DOWN = <any>'SOLR_DOWN',
    NO_AVAILABLE_MEDIA_PROCESSOR = <any>'NO_AVAILABLE_MEDIA_PROCESSOR',
    SYSTEM_INITIATED = <any>'SYSTEM_INITIATED',
    LICENSE_EXPIRED = <any>'LICENSE_EXPIRED',
    SYSTEM_ERROR = <any>'SYSTEM_ERROR'
  }

  export enum ERunType {
    ROOT_FOLDER= <any>"ROOT_FOLDER",
    BIZ_LIST= <any>"BIZ_LIST",
    SCHEDULE_GROUP= <any>"SCHEDULE_GROUP",
  }


  export class DtoCrawlRunDetails {
    id:number;
    rootFolderDto:DtoRootFolder;
    scanStartTime:number;
    scanEndTime:number;
    runType:ERunType;
    runOutcomeState:ERunStatus;
    numberOfFoldersFound:number;
    totalProcessedFiles:number;
    processedPdfFiles:number;
    processedWordFiles:number;
    processedExcelFiles:number;
    processedOtherFiles:number;
    deletedFiles:number;
    newFiles:number;
    updatedContentFiles:number;
    updatedMetadataFiles:number;
    scanErrors:number;
    scanProcessingErrors:number;
    manual:boolean;
    accessible:boolean;
    ingestedRootFolders:number[];
    analyzedRootFolders:number[];
    scanFiles:boolean;
    ingestFiles:boolean;
    analyzeFiles:boolean;
    scheduleGroupId:number;
    scheduleGroupName:string;
    stateString:string;
    gracefulStop:boolean


    static Empty():DtoCrawlRunDetails {
      var g = new DtoCrawlRunDetails();
      g.id = 11112;
      (<any>g).rootFolderDto = 'a';
      g.scanStartTime = 1;
      g.scanEndTime = 2;
      g.accessible = true;
      g.scheduleGroupName = 'aa';
      g.runOutcomeState = ERunStatus.FINISHED_SUCCESSFULLY;
      g.runType = ERunType.ROOT_FOLDER;
      g.processedPdfFiles = 4;
      g.processedExcelFiles = 5;
      g.processedOtherFiles = 6;
      g.processedWordFiles = 7;
      g.scanProcessingErrors = 8;
      g.scanErrors = 9;
      g.totalProcessedFiles = 10;
      g.scanErrors = 11;
      g.scanProcessingErrors = 12;
      g.scheduleGroupId = 13;
      g.numberOfFoldersFound = 14;
      (<any>g).manual = 'c2';
      g.updatedContentFiles = 15;
      g.updatedMetadataFiles = 16;
      g.deletedFiles = 17;
      g.newFiles = 18;
      g.stateString = 'd';

      return g;
    }

  }
 export class DtoRunSummaryInfo{
    jobs: Dashboards.DtoPhaseDetails[];
    crawlRunDetailsDto: DtoCrawlRunDetails;
 }
  export class DtoRootFoldersAggregatedSummaryInfo{

    totalRootFoldersCount: number;
    failedRootFoldersCount: number;
    newRootFoldersCount: number;
    scannedRootFoldersCount: number;
    runningRootFoldersCount: number;
  }

  export class DtoServerResource {
    fullName:string;
    name:string;
    accessible:boolean;
    hasChildren:boolean;
    type:string;
    id:number|string;


    static Empty():DtoServerResource {
      var g = new DtoServerResource();
      g.fullName = 'b';
      g.name = 'c';
      g.accessible = true;
      g.hasChildren = false;
      g.type = 'd';
      g.id = 1;

      return g;
    }

    static serverResourceType_DRIVE = 'DRIVE';
    static serverResourceType_SERVER = "SERVER";
    static serverResourceType_FOLDER = "FOLDER";
    static serverResourceType_SHARE = "SHARE";
  }
  export class DtoScheduleGroupSummaryInfo {

    scheduleGroupDto:DtoScheduleGroup;
    runStatus:ERunStatus;
    missedScheduleTimeStamp:number;
    lastCrawlRunDetails:DtoCrawlRunDetails;
    currentRunningPhase:string;
    nextPlannedWakeup:number;

    static Empty():DtoScheduleGroupSummaryInfo {
      var g = new DtoScheduleGroupSummaryInfo();
      (<any>g).runStatus = 'a';
      (<any>g).lastRunStartTime = 'b';
      (<any>g).lastCrawlRunDetails = 'c';
      g.missedScheduleTimeStamp = 4;
      g.currentRunningPhase = 'e';
      (<any>g).scheduleGroupDto = 'f';
      (g).nextPlannedWakeup = 5;

      return g;
    }


  }

  export class DtoDepartment {
    id:number;
    name:string;
    parentId:number;
    description:string;
    fullName:string;

    static Empty():DtoDepartment {
      var g = new DtoDepartment();
      g.id = 111;
      g.name = 'b';
      g.parentId = 222;
      g.description = 'c';
      g.fullName = 'e';
      return g;
    }
  }

  export class DtoScheduleGroup {
    id:number;
    name:string;
    analyzePhaseBehavior:ECrawlerPhaseBehavior;
    rootFolderIds:number[];
    active:boolean;
    cronTriggerString:string;
    schedulingDescription:string;
    scheduleConfigDto:DtoScheduleConfig;


    static Empty():DtoScheduleGroup {
      var g = new DtoScheduleGroup();
      g.id = 111;
      g.name = 'b';
      g.analyzePhaseBehavior = ECrawlerPhaseBehavior.EVERY_ROOT_FOLDER;
      g.active = true;
      g.schedulingDescription = 'c';
      g.cronTriggerString = 'd';
      (<any>g).scheduleConfigDto = 'e';
      return g;
    }
  }
  export enum ECrawlerPhaseBehavior {
    ONCE = <any>"ONCE",
    EVERY_ROOT_FOLDER = <any>"EVERY_ROOT_FOLDER",

  }

  export enum EJobType {
    UNKNOWN = <any>"UNKNOWN",
    SCAN = <any>"SCAN",
    SCAN_FINALIZE = <any>"SCAN_FINALIZE",
    INGEST = <any>"INGEST",
    INGEST_FINALIZE = <any>"INGEST_FINALIZE",
    OPTIMIZE_INDEXES = <any>"OPTIMIZE_INDEXES",
    ANALYZE = <any>"ANALYZE",
    ANALYZE_FINALIZE = <any>"ANALYZE_FINALIZE",
    EXTRACT = <any>"EXTRACT",
    EXTRACT_FINALIZE = <any>"EXTRACT_FINALIZE",
    }

    export enum EJobProcessingMode{
      SIMPLE = <any>"UNKNOWN",
      FAIR = <any>"FAIR",
      INGEST_ONLY = <any>"INGEST_ONLY",
      ANALYZE_ONLY = <any>"ANALYZE_ONLY",
      AUTO_EXCLUSIVE = <any>"AUTO_EXCLUSIVE",
    }

  export class DtoFolderExcludeRule {
    id:number;
    matchString:string;
    rootFolderId:string;
    operator:string;
    disabled:boolean;

    static Empty():DtoFolderExcludeRule {
      var g = new DtoFolderExcludeRule();
      g.id = 111;
      g.matchString = 'b';
      g.operator = 'c';
      g.disabled = true;
      return g;
    }


  }

  export class DtoScheduleConfig {
    scheduleType:EScheduleConfigType;
    hour:number;
    minutes:number;
    every:number;
    daysOfTheWeek:EDayOfTheWeek[];
    customCronConfig:string;

    static Empty():DtoScheduleConfig {
      var g = new DtoScheduleConfig();
      (<any>g).scheduleType = 'a';
      (<any>g).daysOfTheWeek = 'b';
      g.minutes = 1;
      g.hour = 2;
      g.every = 3;
      (<any>g).customCronConfig = 'c';

      return g;
    }

  }
  export enum EScheduleConfigType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    WEEKLY = <any>"WEEKLY",
    DAILY = <any>"DAILY",
    HOURLY = <any>"HOURLY",
    CUSTOM = <any>"CUSTOM",

  }
  export enum EDayOfTheWeek {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    SUN = <any>"SUN",
    MON = <any>"MON",
    TUE = <any>"TUE",
    WED = <any>"WED",
    THU = <any>"THU",
    FRI = <any>"FRI",
    SAT = <any>"SAT",

  }

  export class DTOImportSummaryResult {
    importId:number;
    errorImportRowResultDtos:DTOImportRowResult[];
    duplicateImportRowResultDtos:DTOImportRowResult[];
    warningImportRowResultDtos:DTOImportRowResult[];
    resultDescription:string;
    importedRowCount:number;
    importFailed:boolean;
    duplicateRowCount:number;
    errorRowCount:number;
    warningRowCount:number;
    rowHeaders:string[];

    static Empty():DTOImportSummaryResult {
      var g = new DTOImportSummaryResult();
      g.importId = 111;
      (<any>g).errorImportRowResultDtos = 'b';
      (<any>g).duplicateImportRowResultDtos = 'c';
      (<any>g).warningImportRowResultDtos = 'e';
      g.resultDescription = 'd';
      g.importedRowCount = 2;
      g.duplicateRowCount = 3;
      g.warningRowCount = 5;
      g.errorRowCount = 4;
      return g;
    }

  }
  export class DTOImportRowResult {
    rowDto:DTORow;
    importFailed:boolean;
    message:string;


    static Empty():DTOImportRowResult {
      var g = new DTOImportRowResult();
      (<any>g).rowDto = 'a';
      (<any>g).importFailed = 'b';
      g.message = 'c';
      return g;
    }

  }
  export class DTORow {
    fieldsValues:string[];
    lineNumber:number;

    static Empty():DTORow {
      var g = new DTORow();
      (<any>g).fieldsValues = 'a';
      g.lineNumber = 1;
      return g;
    }

  }


  export class UserSavedData {
    id:number;
    user:string;
    key:string;
    data:string;

  }

  export enum EUpdateFileOperationType{  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    SET = <any>"SET",
    CLEAR = <any>"CLEAR",

  }

  export class DTOSearchPattern {
    id:number;
    patternType:ESearchPatternType;
    pattern:string;
    name:string;
    description:string;
    active:boolean;

    static Empty():DTOSearchPattern {
      var g = new DTOSearchPattern();
      g.id = 111;
      g.name = 'b';
      g.description = 'c';
      g.active = true;
      g.pattern = 'd';
      g.patternType = ESearchPatternType.REGEX;
      return g;
    }

  }

  export enum ESearchPatternType{  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    REGEX = <any>"REGEX",
    PREDEFINED = <any>"PREDEFINED",

  }


  export enum EClaComponentType{
    MEDIA_PROCESSOR = <any>"MEDIA_PROCESSOR",
    KAFKA = <any>"KAFKA",
    ZOOKEEPER = <any>"ZOOKEEPER",
    BROKER = <any>"BROKER",
    BROKER_DC = <any>"BROKER_DC",
    BROKER_FC = <any>"BROKER_FC",
    DATA_CENTER = <any>"DATA_CENTER",
    DB = <any>"DB",
    SOLR_CLAFILE = <any>"SOLR_CLAFILE",
    SOLR_SIM = <any>"SOLR_SIM",
    SOLR_SIMILARITY = <any>"SOLR_SIMILARITY",
    SOLR_NODE = <any>"SOLR_NODE",
    SOLR_ROOT_NODE = <any>"SOLR_ROOT_NODE",
    APPSERVER = <any>"APPSERVER",

  }
  export enum EComponentState{
    OK = <any>"OK",
    STOPPED = <any>"STOPPED",
    SHUTTING_DOWN = <any>"SHUTTING_DOWN",
    STARTING = <any>"STARTING",
    FAULTY = <any>"FAULTY",
    UNKNOWN = <any>"UNKNOWN",
    NA = <any>"N/A",
    REMOVED = <any>"REMOVED",
    PENDING_INIT = <any>"PENDING_INIT"
  }
  export enum EComponentErrorLevel{
    OK = <any>"OK",
    WARNING = <any>"WARNING",
    ERROR = <any>"ERROR",
    FATAL = <any>"FATAL",
  }
  export class DtoCustomerDataCenter {
    id:number;
    location:string;
    name:string;
    description:string;
    createdOnDB:number;
    defaultDataCenter:boolean;


    static Empty():DtoCustomerDataCenter {
      var g = new DtoCustomerDataCenter();
      g.id = 1;
      g.createdOnDB = 2;
      (<any>g).location = 'a';
      g.name = 'b';
      g.description = 'c';
      (<any>g).defaultDataCenter = 'd';

      return g;
    }

  }
  export class DtoCustomerDataCenterSummaryInfo  {
    customerDataCenterDto: DtoCustomerDataCenter;
    rootFolderCount: number;
    mediaProcessorCount: number;


    static Empty():DtoCustomerDataCenterSummaryInfo {
      var g = new DtoCustomerDataCenterSummaryInfo();

      (<any>g).customerDataCenterDto = 'b';
      g.rootFolderCount = 1;
      g.mediaProcessorCount = 2;

      return g;
    }
  }

  export class DtoServerComponent  {
    id:number;
    location:string;
    name:string;

    static Empty():DtoServerComponent {
      var g = new DtoServerComponent();
      g.id = 1;
      return g;
    }

  }

  export class DtoSummaryServerComponent  {
     claComponentDto: DtoClaComponent;
     extraMetrics : string;
     statusTimestamp: number;

    static Empty():DtoSummaryServerComponent {
      var g = new DtoSummaryServerComponent();
      g.extraMetrics = 'a';
      g.statusTimestamp = 1;
      (<any>g).claComponentDto = 'c';

      return g;
    }
  }

  export class DtoClaComponent {
    id:number;
    claComponentType:EClaComponentType;
    componentType:string;
    instanceId:string;
    location:string;
    externalNetAddress:string;
    localNetAddress:string;
    createdOnDB:number;
    active:boolean;
    state:EComponentState;
    stateChangeDate:number;
    lastResponseDate:number;
    customerDataCenterDto:Operations.DtoCustomerDataCenter;


    static Empty():DtoClaComponent {
      var g = new DtoClaComponent();
      g.id = 1;
      g.claComponentType = EClaComponentType.APPSERVER;
      g.componentType = 'a';
      g.instanceId = 'b';
      g.instanceId = 'c';
      g.location = 'd';
      g.externalNetAddress = 'e';
      g.localNetAddress = 'f';
      g.createdOnDB = 2;
      (<any>g).active = 'g';
      g.state = EComponentState.FAULTY;
      g.stateChangeDate = 3;
      g.lastResponseDate = 4;
      (<any>g).customerDataCenterDto = 'h';

      return g;
    }

  }
  export class DtoClaComponentStatus {
    id:number;
    claComponentId:number;
    updatedOnDb:number;
    timestamp:number;
    componentType:string;
    componentState:EComponentState;
    extraMetrics:string;
  }
  export class DtoClaComponentStatus_AppServer extends DtoClaComponentStatus{

    analyzedTaskRequests:number;
    analyzedTaskRequestsFinished:number;
    analyzedTaskRequestFailure:number;

    ingestRunningThroughput:number;
    averageIngestThroughput:number;

    analyzedRunningThroughput:number;
    averageAnalyzedThroughput:number;

    processCpuLoad:number;
    systemCpuLoad:number;
    totalPhysicalMemorySize:number;
    freePhysicalMemorySize:number;

    static Empty():DtoClaComponentStatus_AppServer {
      var g = new DtoClaComponentStatus_AppServer();
      g.id = 1;
      g.claComponentId = 2;

      g.updatedOnDb = 3;
      g.timestamp = 4;
      g.componentType = 'b';
      g.componentState = EComponentState.REMOVED;
       g.processCpuLoad = 23;
      g.systemCpuLoad = 24;
      g.totalPhysicalMemorySize = 25;
      g.freePhysicalMemorySize = 26;
      g.analyzedTaskRequests=27;
      g.analyzedTaskRequestsFinished=28;
      g.analyzedTaskRequestFailure=29;
      g.analyzedRunningThroughput=30;
      g.averageAnalyzedThroughput=31;
      g.ingestRunningThroughput=32;
      g.averageIngestThroughput=33;
      return g;
    }

  }
  export class DtoClaComponentStatus_MP extends DtoClaComponentStatus{
    startScanTaskRequests:number;
    startScanTaskRequestsFinished:number;
    scanTaskResponsesFinished:number;
    scanTaskFailedResponsesFinished:number;
    activeScanTasks:number;

    ingestTaskRequests:number;
    ingestTaskRequestsFinished:number;
    ingestTaskRequestsFinishedWithError:number;
    ingestTaskRequestFailure:number;


    averageIngestThroughput:number;
    scanRunningThroughput:number;
    ingestRunningThroughput:number;
    averageScanThroughput:number;


    processCpuLoad:number;
    systemCpuLoad:number;
    totalPhysicalMemorySize:number;
    freePhysicalMemorySize:number;

    static Empty():DtoClaComponentStatus_MP {
      var g = new DtoClaComponentStatus_MP();
      g.id = 1;
      g.claComponentId = 2;

      g.updatedOnDb = 3;
      g.timestamp = 4;
      g.componentType = 'b';
      g.componentState = EComponentState.REMOVED;

      g.startScanTaskRequests = 8;
      g.startScanTaskRequestsFinished = 9;
      g.scanTaskResponsesFinished = 10;
      g.scanTaskFailedResponsesFinished = 11;
      g.activeScanTasks = 12;
      g.ingestTaskRequests = 13;
      g.ingestTaskRequestsFinished = 14;
      g.ingestTaskRequestsFinishedWithError = 15;
      g.ingestTaskRequestFailure = 16;
      g.ingestTaskRequestsFinishedWithError = 17;
      g.ingestTaskRequestFailure = 18;
      g.averageIngestThroughput = 19;
      g.scanRunningThroughput = 20;
      g.ingestRunningThroughput = 21;
      g.averageScanThroughput = 22;
      g.processCpuLoad = 23;
      g.systemCpuLoad = 24;
      g.totalPhysicalMemorySize = 25;
      g.freePhysicalMemorySize = 26;

      return g;
    }

  }
  export class DtoClaComponentStatus_BROKER_ITEM {
    QueueSize:number;
    EnqueueCount:number;
    ConsumerCount:number;
    MaxEnqueueTime:number;
    ExpiredCount:number;

    DequeueCount:number;
    AverageEnqueueTime:number;
    MinEnqueueTime:number;


    static Empty():DtoClaComponentStatus_BROKER_ITEM {
      var g = new DtoClaComponentStatus_BROKER_ITEM();
      g.QueueSize = 8;
      g.EnqueueCount = 9;
      g.ConsumerCount = 10;
      g.ExpiredCount = 11;
      g.MaxEnqueueTime = 12;
      g.DequeueCount = 13;
      g.AverageEnqueueTime = 14;
      g.MinEnqueueTime = 15;

      return g;
    }

  }
  export class DtoClaComponentStatus_BROKER extends DtoClaComponentStatus{
    realtime_requests:DtoClaComponentStatus_BROKER_ITEM;
    scan_requests:DtoClaComponentStatus_BROKER_ITEM;
    scan_responses:DtoClaComponentStatus_BROKER_ITEM;
    realtime_responses:DtoClaComponentStatus_BROKER_ITEM;
    ingest_requests:DtoClaComponentStatus_BROKER_ITEM;
    ingest_responses:DtoClaComponentStatus_BROKER_ITEM;
    control_responses:DtoClaComponentStatus_BROKER_ITEM;
    contentcheck_requests:DtoClaComponentStatus_BROKER_ITEM;

    EnqueueCount:number;
    ConsumerCount:number;
    MaxEnqueueTime:number;
    ExpiredCount:number;

    DequeueCount:number;
    AverageEnqueueTime:number;
    MinEnqueueTime:number;


    static Empty():DtoClaComponentStatus_BROKER {
      var g = new DtoClaComponentStatus_BROKER();
      g.id = 1;
      g.claComponentId = 2;

      g.updatedOnDb = 3;
      g.timestamp = 4;
      g.componentType = 'b';
      g.componentState = EComponentState.REMOVED;
      (<any>g).scan_requests = 5;
      (<any>g).scan_responses = 6;
      (<any>g).control_responses = 7;
      (<any>g).realtime_requests = 8;
      (<any>g).realtime_responses = 9;
      (<any>g).ingest_requests = 10;
      (<any>g).ingest_responses = 11;
      (<any>g).contentcheck_requests = 12;
      return g;
    }

  }
}

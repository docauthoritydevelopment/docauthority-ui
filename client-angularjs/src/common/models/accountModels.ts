'use strict';

class DTOResultBase {
  type: ERequestErrorCode;
}

class DTORequestBase {
    reason:string;
    result:boolean;
}

class DTOPagedResult<T>{
  content:T[];
  totalPages:number;
  totalElements:number;
  size:number;
  last:boolean;
  first:boolean;
  sort:any;
  numberOfElements:number

}

enum ERequestErrorCode  {
  UNAUTHORIZED= <any>"UNAUTHORIZED",
  LICENSE_EXPIRED= <any>"LICENSE_EXPIRED",
  LICENSE_QUOTA_EXCEEDED= <any>"LICENSE_QUOTA_EXCEEDED",
  ITEM_ALREADY_EXISTS= <any>"ITEM_ALREADY_EXISTS",
  GROUP_LOCKED= <any>"GROUP_LOCKED",
  ITEM_ATTACHED_TO_ANOTHER= <any>"ITEM_ATTACHED_TO_ANOTHER",
  MISSING_FIELD= <any>"MISSING_FIELD"
}

module License {

  export class DtoLicenseImportResult {
    docAuthorityLicenseDto:DtoDocAuthorityLicense;
    success:boolean;
    reason:string;

  }
  export class DtoDocAuthorityLicense  {
    id:number;
    creationDate:number;
    applyDate:number;
    registeredTo:string;
    issuer:string;
    restrictions:any;
    serverId:string;
    loginExpired:boolean;
  }


}
module AppSettings {

    export class DtoSmtpConfiguration {
        host:string;
      port:number;
      username:string;
      password:string;
      smtpAuthentication:boolean;
      useTls:boolean;
      useSsl:boolean;

    }
  export class DtoMailNotificationConfiguration  {
    smtpConfigurationDto:DtoSmtpConfiguration;
    addresses:string;
    enabled:boolean;
  }

  export class DtoLdapConnectionDetails  {
    id:number;
    name:string;
    isSSL:boolean;
    host:string;
    manageDN:string;
    port:number;
    managerPass:string;
    serverDialect:ELdapServerDialect

    static Empty(): DtoLdapConnectionDetails {
      var g = new DtoLdapConnectionDetails();
      g.id = 1; g.name = 'b';  g.isSSL = false; g.host = 'a'; g.manageDN = 'b';g.port = 2;g.managerPass = 'c';g.serverDialect=ELdapServerDialect.ACTIVE_DIRECTORY_LEGACY;
      return g;
    }
  }

  export enum ELdapServerDialect   {
    ACTIVE_DIRECTORY= <any>"ACTIVE_DIRECTORY",
    ACTIVE_DIRECTORY_LEGACY= <any>"ACTIVE_DIRECTORY_LEGACY",
    ApacheDS= <any>"ApacheDS",


  }
  export class DtoAuditTrail {

    eventId:number;
    username:string;
    timestamp:number;
    objectType:string;
    message:string;
    crudAction:EAuditAction;
    objectId:string;
    onObjectType:string;
    onObjectId:string;
    params:string;
    auditType:EAuditType;

    static Empty(): DtoAuditTrail {
      var g = new DtoAuditTrail();
      g.eventId = 1; g.username = 'b';  g.timestamp = 2; g.objectType = 'g'; g.message = 'c';g.crudAction = EAuditAction.CREATE;g.objectId = 'e';g.onObjectType = 'e';g.onObjectId = 'e';g.params = 'e';g.auditType = EAuditType.ACTION_EVENT;
      return g;
    }
  }
  export enum EAuditType {
    ACTION_EVENT,
    GROUP,
    FILE,
    FOLDER,
    BIZ_LIST,
    ROOT_FOLDER,
    MEDIA_PROCESSOR,
    MEDIA_CONNECTION_DETAILS,
    SEARCH_PATTERN,
    SCHEDULE_GROUP,
    DOC_TYPE,
    TAG_TYPE,
    TAG,
    DATE_PARTITION,
    CUSTOMER_DATA_CENTER,
    SCAN,
    SOLR,
    USER,
    BIZ_ROLE, // deprecated use OPERATIONAL_ROLE
    FUNCTIONAL_ROLE, // deprecated use DATA_ROLE
    ROLE_TEMPLATE,
    SETTINGS,
    CHART,
    TREND_CHART,
    QUERY,
    LOGIN,
    LOGOUT,
    OPERATIONAL_ROLE,
    DATA_ROLE
  }

  export enum EAuditAction {
    CREATE= <any>"CREATE",
    CREATE_ITEM= <any>"CREATE_ITEM",
    UPDATE= <any>"UPDATE",
    UPDATE_ITEM= <any>"UPDATE_ITEM",
    DELETE= <any>"DELETE",
    DELETE_ITEM = <any>"DELETE_ITEM",
    ASSIGN= <any>"ASSIGN",
    UNASSIGN= <any>"UNASSIGN",
    IMPORT= <any>"IMPORT",
    IMPORT_ITEMS= <any>"IMPORT_ITEMS",
    IMPORT_ITEM_ATTACH= <any>"IMPORT_ITEM_ATTACH",
    IMPORT_ITEM_CREATE= <any>"IMPORT_ITEM_CREATE",
    IMPORT_ITEM_UPDATE= <any>"IMPORT_ITEM_UPDATE",
    START= <any>"START",
    STOP= <any>"STOP",
    PAUSE= <any>"PAUSE",
    RESUME= <any>"RESUME",
    ATTACH= <any>"ATTACH",
    DETACH= <any>"DETACH",
    VIEW= <any>"VIEW",
    SUCCEEDED= <any>"SUCCEEDED",
    FAILED= <any>"FAILED",
    AUTO= <any>"AUTO",
    VIEW_INFO= <any>"VIEW_INFO",
    UNDELETE= <any>"UNDELETE",
  }
}

module Users {
  export interface ITemplateRoleContainer
  {
    template: DtoTemplateRole;
  }
  export class DTOUser {
    username:string;
    name:string;
    id:number;
    password:string;
    roleName:string;
    lastPasswordChangeTimeStamp:number;
    userState:EUserState;
    userType:EUserType;
    email:string;
    passwordMustBeChanged:boolean;
    bizRoleDtos: DtoBizRole[];
    funcRoleDtos: DtoFunctionalRole[];
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    credentialsNonExpired: boolean;
    consecutiveLoginFailuresCount: number;
    ldapUser:boolean;
    loginLicenseExpired:boolean;
    accessTokens:string[];

    static Empty():DTOUser{
      var g = new DTOUser();
      g.id=1;g.username='b';g.name='c';g.userState=EUserState.ACTIVE;
      g.email='e';g.lastPasswordChangeTimeStamp=2;g.userType = EUserType.INTERNAL;(<any>g).passwordMustBeChanged = 'f';
      (<any>g).bizRoleDtos = 'h';(<any>g).functionalRoleDtos = 'i';
      (<any>g).accountNonExpired = 'i'; (<any>g).accountNonLocked = 'j'; (<any>g).credentialsNonExpired = 'k';
      (<any>g).consecutiveLoginFailuresCount = 2; (<any>g).ldapUser = 'l'
      return g;
    }
  }

  export class DtoLdapGroup  {
    id:number;
    groupName:string;
    groupDN:string;
    connectionDetailsDto: AppSettings.DtoLdapConnectionDetails;
    daRolesIds: number[];

    static Empty():DtoLdapGroup{
      var g = new DtoLdapGroup();
      g.id=1;g.groupName='c';g.groupDN='d';(<any>g).connectionDetails = 'h';(<any>g).daRolesIds='i';
      return g;
    }
  }

  export class DtoLdapGroupUser  {
    id:number;
    userName:string;

    static Empty():DtoLdapGroupUser{
      var g = new DtoLdapGroupUser();
      g.id=1;g.userName='c';
      return g;
    }
  }

  export enum EUserType   {
    INTERNAL= <any>"INTERNAL",
    SYSTEM= <any>"SYSTEM",
    ACTIVE_DIRECTORY= <any>"ACTIVE_DIRECTORY",

  }
  export enum EUserState  {
    ACTIVE= <any>"ACTIVE",
    EXPIRED= <any>"EXPIRED",
    DISABLED= <any>"DISABLED",
  }

  export enum ETemplateType{
    BIZROLE = <any>"BIZROLE",
    FUNCROLE= <any>"FUNCROLE",
    NONE= <any>"NONE"
  }

  export class DtoBizRole implements ITemplateRoleContainer {
    name:string;
    displayName:string;
    id:number;
    description:string;
    ldapGroup: DtoLdapGroup;
    template: DtoTemplateRole;

    static Empty():DtoBizRole{
      var g = new DtoBizRole();
      g.id=1;g.name='b';g.displayName='c';g.description='d';(<any>g).ldapGroup='i';(<any>g).template='l'
      return g;
    }
  }


  export class DtoBizRoleSummaryInfo {
    bizRoleDto: DtoBizRole;
    ldapGroupMappingDtos: DtoLdapGroup[];

    static Empty():DtoBizRoleSummaryInfo{
      var g = new DtoBizRoleSummaryInfo();
      (<any>g).bizRoleDto = 'a';
      (<any>g).ldapGroupMappingDtos = 'b';
      return g;
    }
  }

  export class DtoTemplateRole {
    name:string;
    displayName:string;
    id:number;
    description:string;
    templateType:ETemplateType;
    systemRoleDtos: DtoSystemRole[];

    static Empty():DtoTemplateRole{
      var g = new DtoTemplateRole();
      g.id=1;g.name='b';g.displayName='c';g.description='d';(<any>g).systemRoleDtos='e';
      return g;
    }
  }

  export interface IFunctionalItem
  {
    permittedOperations: DtoSystemRole[];
  }

  export class DtoFunctionalRole implements ITemplateRoleContainer{
    id:number;
    name:string;
    displayName:string;
    description:string;
    managerUsername:string;
    implicit:boolean;
    template:DtoTemplateRole;

    static Empty():DtoFunctionalRole{
      var g = new DtoFunctionalRole();
      g.id=1;g.name='c';g.name='d';g.description='e';g.managerUsername='f';g.implicit=true;(<any>g).template='h';
      return g;
    }
  }

  export class DtoFunctionalRoleSummaryInfo {
    functionalRoleDto: DtoFunctionalRole;
    ldapGroupMappingDtos: DtoLdapGroup[];

    static Empty():DtoFunctionalRoleSummaryInfo{
      var g = new DtoFunctionalRoleSummaryInfo();
      (<any>g).functionalRoleDto = 'a';
      (<any>g).ldapGroupMappingDtos = 'b';
      return g;
    }
  }

  export class DtoSystemRole {
    name:ESystemRoleName;
    id:number;
    description:string;
    displayName:string;
    authorities:string[];
    static Empty():DtoSystemRole{
      var g = new DtoSystemRole();
      g.id=1;(<any>g).name='c';g.description='d';(<any>g).authorities = 'h';g.displayName='e';
      return g;
    }
  }

  export enum ESystemRoleName  {

    demo_showcase= <any>"demo_showcase",

    MngScanConfig= <any>"MngScanConfig",
    RunScans= <any>"RunScans",
    MngRoles= <any>"MngRoles",
    MngUsers = <any>"MngUsers",
    ViewUsers = <any>"ViewUsers",
    MngUserRoles= <any>"MngUserRoles",
    ViewAuditTrail= <any>"ViewAuditTrail",
    GeneralAdmin= <any>"GeneralAdmin",
    MngTagConfig= <any>"MngTagConfig",

    AssignTag= <any>"AssignTag",
    AssignSecurityTag = <any>"AssignSecurityTag",
    ViewTagTypes= <any>"ViewTagTypes",
    ViewTagAssociation= <any>"ViewTagAssociation",
    ViewSecurityTagAssociation= <any>"ViewSecurityTagAssociation",
    MngDocTypeConfig= <any>"MngDocTypeConfig",
    AssignDocType= <any>"AssignDocType",
    ViewDocTypeTree= <any>"ViewDocTypeTree",
    ViewDocTypeAssociation= <any>"ViewDocTypeAssociation",
    AssignDepartment= <any>"AssignDepartment",
    ViewDepartmentTree= <any>"ViewDepartmentTree",
    ViewDepartmentAssociation= <any>"ViewDepartmentAssociation",
    AssignDataRole= <any>"AssignDataRole",
    ViewDataRoleAssociation= <any>"ViewDataRoleAssociation",
    MngBizLists = <any>"MngBizLists",
    ViewBizListAssociation = <any>"ViewBizListAssociation",
    AssignBizList = <any>"AssignBizList",
    RunActions = <any>"RunActions",
    ViewContent= <any>"ViewContent",
    ViewFiles= <any>"ViewFiles",
    ViewPermittedContent= <any>"ViewPermittedContent",
    ViewPermittedFiles= <any>"ViewPermittedFiles",
    MngGroups= <any>"MngGroups",
    ViewReports= <any>"ViewReports",
    MngReports= <any>"MngReports",
    TechSupport= <any>"TechSupport",
    ManageSearchPatterns = <any>"ManageSearchPatterns",
    ActivateRegulations = <any>"ActivateRegulations",
    ActivatePatternCategories = <any>"ActivatePatternCategories",
    None = <any>"none",
    MngDepartmentConfig= <any>"MngDepartmentConfig",
    MngUserViewData = <any>"MngUserViewData",
    AssignPublicUserViewData = <any>"AssignPublicUserViewData",
    AssignTagFilesBatch = <any>"AssignTagFilesBatch"
  }

  export class UserAuthenticationData extends DTOUser {
    token:string;
    bizRoleSysRoleByNameMap:any;
    funcRoleSysRolesByIdMap:any;
    configurationList:any;
    isDemoInstallation:boolean;
    demoInfo:any;
    dashboards: {
      AdminSysRole: ESystemRoleName,
      ITSysRole: ESystemRoleName
    };
  }

  export class LogOnModel {
    userName: string;
    password: string;
  }

}

module GroupNaming
{
  export class DTOGroupNamingDebugData{
    groupDto:Entities.DTOGroup;
    newName:string;
    debugInfo:string;
    factors:DTOFactors;
    useFilename:boolean;
    titleBasedTokens:DTOToken[];
    filenameBasedTokens:DTOToken[];
    combinedListTokens:DTOToken[];
    parentDirBasedTokens:DTOToken[];
    grandparentDirBasedTokens:DTOToken[];
    topToken:DTOToken;
  }

  export class DTOFactors {
    titleTokenFactorTfidf :number;
    titleTokenFactorTf:number;
    titleTokenFactorLength :number;
    titleTokenFactorNumWords:number;
    titleBoostFactor :number;
    titleScoreFactor:number;
    titleOrderFactor :number;
    optimalTitleLength :number;
    minRatedTitleLength:number;
    maxRatedTitleLength :number;
    topTitlesBoostValue :number;
    fileNameBoostValue :number;
    parentDirBoostValue :number;
    grandParentDirBoostValue :number;

    static toUrlString(f:DTOFactors):string {
      if (!f) {
        return null;
      }
      return '' +
        f.titleTokenFactorTfidf + ',' + f.titleTokenFactorTf + ',' + f.titleTokenFactorLength +
        ',' + f.titleTokenFactorNumWords + ',' + f.titleBoostFactor + ',' + f.titleScoreFactor +
        ',' + f.titleOrderFactor + ',' + f.optimalTitleLength + ',' + f.minRatedTitleLength +
        ',' + f.maxRatedTitleLength + ',' + f.topTitlesBoostValue + ',' + f.fileNameBoostValue +
        ',' + f.parentDirBoostValue + ',' + f.grandParentDirBoostValue
      ;
    }
  }

  export class DTOToken {
    token:string;
    tfidf:number;
    tf:number;
    nTfidf:number;
    nTf:number;
    nLen:number;
    nWords:number;
    tokenScore:number;
    titleRate:number;
    titleLen:number;
    titleOrder:number;

    static Empty():DTOToken{
      var g = new DTOToken();
      g.token='aa';g.tf=1;g.tfidf=2;g.nTfidf=3;g.nTf=4;g.nLen=5;g.nWords=6;g.tokenScore=7;g.titleRate=8;g.titleLen=9;g.titleOrder=10;
      return g;
    }
  }

}

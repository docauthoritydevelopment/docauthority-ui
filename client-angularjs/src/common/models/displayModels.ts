///<reference path='groupModels.ts'/>
'use strict';

enum  ERouteStateName {
   scanHistory= <any>"scanHistory",
   activeScans= <any>"activeScans",
   empty= <any>"empty",
   serverComponentsMonitor= <any>"serverComponentsMonitor",
   mediaTypeConnectionsManagement= <any>"mediaTypeConnectionsManagement",
   rootFoldersManagement= <any>"rootFoldersManagement",
   discover= <any>"discover",
   search= <any>"search",
   discoverSplitView= <any>"report",
   GDPRErasureManagement= <any>"GDPRErasureManagement",
   GDPRErasureManagementSelected= <any>"GDPRErasureManagementSelected",
   bizListsManagement= <any>"bizListsManagement",
   searchPatternsManagement= <any>"searchPatternsManagement",
   chartView= <any>"charti",
   trendChartView= <any>"trendcharti",
   trendChartsManagement= <any>"trendCharts",
   chartsManagement= <any>"charts",
   docTypesChart= <any>"docTypesHier",
   details= <any>"details",
   settings= <any>"settings",
   profile= <any>"profile",
   policyManagement= <any>"policyManagement",
   policy= <any>"policy",
   usersManagement= <any>"usersManagement",
   bizRolesManagement= <any>"bizRolesManagement",
   functionalRolesManagement= <any>"functionalRolesManagement",
   templateRolesManagement= <any>"templateRolesManagement",
   processingErrors= <any>"processingErrors",
   auditTrail= <any>"auditTrail",
   dashboardIT= <any>"dashboardIT",
   dashboardForwarder= <any>"dashboardForwarder",
   dashboardSystem= <any>"dashboardSystem",
   unauthorized= <any>"unauthorized",
   dashboardUser= <any>"dashboardUser"
 }
class EPaneOption {
  title:string;
  handler:(string)=>void;
  view:string;
  entityDisplayType:EntityDisplayTypes;
  leftFilterFunction: (value: boolean,fullyContainedState?:EFullyContainedFilterState ) =>(value,name) => any;
  rightFilterFunction: (value,name) => any;

  constructor(title:string, handler:(string)=>void, view:EntityDisplayTypes, leftFilterFunction?:(value: boolean) =>(value,name) => any,rightFilterFunction?:(value,name) => any) {
    this.title = title;
    this.handler = handler;
    this.view = view.toString();
    this.entityDisplayType = view;
    this.leftFilterFunction = leftFilterFunction;
    this.rightFilterFunction = rightFilterFunction;
  }
}
class EUserSearchOption {
  constructor(public value:string) {
  }

  equals(o) {
    return this.value === o.value;
  }

  toString() {
    return this.value;
  }

  // values
  static all = new EUserSearchOption("All");
  static selection = new EUserSearchOption("Selection");
  static master = new EUserSearchOption("Master");
  static none = new EUserSearchOption("None");

}
class EUserSearchMode {
  constructor(public value:string) {
  }

  equals(o) {
    return this.value === o.value;
  }

  toString() {
    return this.value;
  }

  // values
  static properties = new EUserSearchMode("Properties");
  static content = new EUserSearchMode("Content");

  static all() {

    var arr = [
      EUserSearchMode.properties,
      EUserSearchMode.content,
    ];
    return arr;
  }
}


 class DTOMarkedItem{
   constructor(public id:any,public entity:EntityDisplayTypes,public name?:string, public styleId?:string) {
   }
}
class EFilterContextNames implements IDisplayType {
  constructor(public value:string) {
  }

  equals(o) {
    return this.value === o.value;
  }

  toString() {
    return this.value;
  }

  // values
  static acl_read = new EFilterContextNames("aclRead");
  static acl_write = new EFilterContextNames("aclWrite");
  static recipient_address = new EFilterContextNames("recipientAddress");
  static sender_address = new EFilterContextNames("senderAddress");
  static mail = new EFilterContextNames("mail");
  static group = new EFilterContextNames("group");
  static folder = new EFilterContextNames("folder");
  static file = new EFilterContextNames("file");
  static owner = new EFilterContextNames("owner");
  static regulation = new EFilterContextNames("regulation");
  static metadata = new EFilterContextNames("metadata");
  static pattern = new EFilterContextNames("pattern");
  static patternMulti = new EFilterContextNames("patternMulti");
  static share_permission_allow_read = new EFilterContextNames("share_permission_allow_read");
  static label = new EFilterContextNames("label");
  static content = new EFilterContextNames("content");
  static extension = new EFilterContextNames("extension");
  static fileTypeCategory = new EFilterContextNames("fileTypeCategory");
  static fileSize= new EFilterContextNames("fileSize");
  static tag = new EFilterContextNames("tag");
  static functional_role = new EFilterContextNames("functionalRole");
  static tag_type = new EFilterContextNames("tagType");
  static root_folder = new EFilterContextNames("rootFolder");
  static doc_type = new EFilterContextNames("docType");
  static bizList_item_clients = new EFilterContextNames("bizListItemClients");
  static bizList_item_clients_extraction = new EFilterContextNames("bizListItemClientsExtraction");
  static bizList_item_consumers_extraction_rule = new EFilterContextNames("bizListItemConsumersExtractionRule");
  static file_created_date = new EFilterContextNames("fileCreatedDate");
  static file_modified_date = new EFilterContextNames("fileModifiedDate");
  static file_accessed_date = new EFilterContextNames("fileAccessedDate");
  static all_files = new EFilterContextNames("allFiles");
  static department = new EFilterContextNames("department");
  static matter = new EFilterContextNames("matter");

  static parse(str:string)
  {
    if(str == EFilterContextNames.acl_read.toString())
      return EFilterContextNames.acl_read;
    if(str == EFilterContextNames.mail.toString())
      return EFilterContextNames.mail;
    if(str == EFilterContextNames.acl_write.toString())
      return EFilterContextNames.acl_write;
    if(str == EFilterContextNames.recipient_address.toString())
      return EFilterContextNames.recipient_address;
    if(str == EFilterContextNames.sender_address.toString())
      return EFilterContextNames.sender_address;
    if(str == EFilterContextNames.group.toString())
      return EFilterContextNames.group;
    if(str == EFilterContextNames.folder.toString())
      return EFilterContextNames.folder;
    if(str == EFilterContextNames.file.toString())
      return EFilterContextNames.file;
    if(str == EFilterContextNames.owner.toString())
      return EFilterContextNames.owner;
    if(str == EFilterContextNames.regulation.toString())
      return EFilterContextNames.regulation;
    if(str == EFilterContextNames.metadata.toString())
      return EFilterContextNames.metadata;
    if(str == EFilterContextNames.pattern.toString())
      return EFilterContextNames.pattern;
    if(str == EFilterContextNames.patternMulti.toString())
      return EFilterContextNames.patternMulti;
    if(str == EFilterContextNames.label.toString())
      return EFilterContextNames.label;
    if(str == EFilterContextNames.content.toString())
      return EFilterContextNames.content;
    if(str == EFilterContextNames.extension.toString())
      return EFilterContextNames.extension;
    if(str == EFilterContextNames.fileTypeCategory.toString())
      return EFilterContextNames.fileTypeCategory;
    if(str == EFilterContextNames.fileSize.toString())
      return EFilterContextNames.fileSize;
    if(str == EFilterContextNames.tag.toString())
      return EFilterContextNames.tag;
    if(str == EFilterContextNames.tag_type.toString())
      return EFilterContextNames.tag_type;
    if(str == EFilterContextNames.root_folder.toString())
      return EFilterContextNames.root_folder;
    if(str == EFilterContextNames.doc_type.toString())
      return EFilterContextNames.doc_type;
    if(str == EFilterContextNames.functional_role.toString())
      return EFilterContextNames.functional_role;
   if(str == EFilterContextNames.bizList_item_clients.toString())
      return EFilterContextNames.bizList_item_clients;
    if(str == EFilterContextNames.bizList_item_clients_extraction.toString())
      return EFilterContextNames.bizList_item_clients_extraction;
    if(str == EFilterContextNames.bizList_item_consumers_extraction_rule.toString())
      return EFilterContextNames.bizList_item_consumers_extraction_rule;
    if(str == EFilterContextNames.file_created_date.toString())
      return EFilterContextNames.file_created_date;
    if(str == EFilterContextNames.file_modified_date.toString())
      return EFilterContextNames.file_modified_date;
    if(str == EFilterContextNames.file_accessed_date.toString())
      return EFilterContextNames.file_accessed_date;
    if(str == EFilterContextNames.all_files.toString())
      return EFilterContextNames.all_files;
    if(str == EFilterContextNames.department.toString())
      return EFilterContextNames.department;
    if(str == EFilterContextNames.matter.toString())
      return EFilterContextNames.matter;
    return null;
  }

  static convertEntityDisplayTypeToBaseFilterField(type:EntityDisplayTypes) {
    if(!type.equals) {
      return null;
    }
    var restrictedEntityDisplayTypeName = type.toString();
    if(type.equals(EntityDisplayTypes.folders)||type.equals(EntityDisplayTypes.folders_flat)) {
      restrictedEntityDisplayTypeName = 'subfolders';
    }
    else if(type.equals(EntityDisplayTypes.departments)||type.equals(EntityDisplayTypes.departments_flat)) {
      restrictedEntityDisplayTypeName = 'subdepartments';
    }
    else if(type.equals(EntityDisplayTypes.matters)||type.equals(EntityDisplayTypes.matters_flat)) {
      restrictedEntityDisplayTypeName = 'submatters';
    }
    else if(type.equals(EntityDisplayTypes.doc_types)) {
      restrictedEntityDisplayTypeName = 'subdoc_types';
    }
    return restrictedEntityDisplayTypeName + '_entityNameId';
  }

  static convertEntityDisplayType(type:EntityDisplayTypes)
  {
    if(!type.equals)
    {
      return null;
    }
    if(type.equals( EntityDisplayTypes.groups))
    {
      return EFilterContextNames.group;
    }
    if(type.equals(  EntityDisplayTypes.owners))
    {
      return EFilterContextNames.owner;
    }
    if(type.equals(  EntityDisplayTypes.regulations))
    {
      return EFilterContextNames.regulation;
    }
    if(type.equals(  EntityDisplayTypes.metadatas))
    {
      return EFilterContextNames.metadata;
    }
    if(type.equals(  EntityDisplayTypes.patterns))
    {
      return EFilterContextNames.pattern;
    }
    if(type.equals(  EntityDisplayTypes.patterns_multi))
    {
      return EFilterContextNames.patternMulti;
    }
    if(type.equals(  EntityDisplayTypes.share_permissions_allow_read))
    {
      return EFilterContextNames.share_permission_allow_read;
    }
    if(type.equals(  EntityDisplayTypes.labels))
    {
      return EFilterContextNames.label;
    }
    if(type.equals(  EntityDisplayTypes.contents))
    {
      return EFilterContextNames.content;
    }
    if(type.equals(  EntityDisplayTypes.extensions))
    {
      return EFilterContextNames.extension;
    }
    if(type.equals(  EntityDisplayTypes.file_type_categories))
    {
      return EFilterContextNames.fileTypeCategory;
    }
    if(type.equals(  EntityDisplayTypes.file_sizes))
    {
      return EFilterContextNames.fileSize;
    }
    if(type.equals(  EntityDisplayTypes.acl_reads))
    {
      return EFilterContextNames.acl_read;
    }
    if(type.equals( EntityDisplayTypes.acl_writes))
    {
      return EFilterContextNames.acl_write;
    }
    if(type.equals( EntityDisplayTypes.folders_flat))
    {
      return EFilterContextNames.folder;
    }
    if(type.equals( EntityDisplayTypes.folders))
    {
      return EFilterContextNames.folder;
    }
    if(type.equals( EntityDisplayTypes.files))
    {
      return EFilterContextNames.file;
    }
    if(type.equals( EntityDisplayTypes.tag_types))
    {
      return EFilterContextNames.tag_type;
    }
    if(type.equals( EntityDisplayTypes.tags))
    {
      return EFilterContextNames.tag;
    }
    if(type.equals( EntityDisplayTypes.root_folders))
    {
      return EFilterContextNames.root_folder;
    }
    if(type.equals( EntityDisplayTypes.functional_roles))
    {
      return EFilterContextNames.functional_role;
    }
    if(type.equals( EntityDisplayTypes.doc_types))
    {
      return EFilterContextNames.doc_type;
    }
    if(type.equals( EntityDisplayTypes.bizlistItems_clients))
    {
      return EFilterContextNames.bizList_item_clients;
    }
    if(type.equals( EntityDisplayTypes.bizlistItem_clients_extractions))
    {
      return EFilterContextNames.bizList_item_clients_extraction;
    }
    if(type.equals( EntityDisplayTypes.bizlistItem_consumers_extraction_rules))
    {
      return EFilterContextNames.bizList_item_consumers_extraction_rule;
    }
    if(type.equals( EntityDisplayTypes.file_created_dates))
    {
      return EFilterContextNames.file_created_date;
    }
    if(type.equals( EntityDisplayTypes.file_modified_dates))
    {
      return EFilterContextNames.file_modified_date;
    }
    if(type.equals( EntityDisplayTypes.file_accessed_dates))
    {
      return EFilterContextNames.file_accessed_date;
    }
    if(type.equals( EntityDisplayTypes.all_files))
    {
      return EFilterContextNames.all_files;
    }
    if(type.equals( EntityDisplayTypes.departments))
    {
      return EFilterContextNames.department;
    }
    if(type.equals( EntityDisplayTypes.departments_flat))
    {
      return EFilterContextNames.department;
    }
    if(type.equals( EntityDisplayTypes.matters))
    {
      return EFilterContextNames.matter;
    }
    if(type.equals( EntityDisplayTypes.matters_flat))
    {
      return EFilterContextNames.matter;
    }
  }

  static all() {

    var arr = [
      EFilterContextNames.mail,
      EFilterContextNames.acl_read,
      EFilterContextNames.acl_write,
      EFilterContextNames.recipient_address,
      EFilterContextNames.file,
      EFilterContextNames.folder,
      EFilterContextNames.group,
      EFilterContextNames.owner,
      EFilterContextNames.regulation,
      EFilterContextNames.metadata,
      EFilterContextNames.pattern,
      EFilterContextNames.patternMulti,
      EFilterContextNames.label,
      EFilterContextNames.content,
      EFilterContextNames.extension,
      EFilterContextNames.fileTypeCategory,
      EFilterContextNames.fileSize,
      EFilterContextNames.tag,
      EFilterContextNames.tag_type,
      EFilterContextNames.root_folder,
      EFilterContextNames.functional_role,
      EFilterContextNames.doc_type,
      EFilterContextNames.bizList_item_clients,
      EFilterContextNames.bizList_item_clients_extraction,
      EFilterContextNames.bizList_item_consumers_extraction_rule,
      EFilterContextNames.file_created_date,
      EFilterContextNames.file_modified_date,
      EFilterContextNames.file_accessed_date,
      EFilterContextNames.file_accessed_date,
      EFilterContextNames.department
    ];
    return arr;
  }

  static allLegalInstallationMode() {

    var arr = [
      EFilterContextNames.mail,
      EFilterContextNames.acl_read,
      EFilterContextNames.acl_write,
      EFilterContextNames.recipient_address,
      EFilterContextNames.file,
      EFilterContextNames.folder,
      EFilterContextNames.group,
      EFilterContextNames.owner,
      EFilterContextNames.regulation,
      EFilterContextNames.metadata,
      EFilterContextNames.pattern,
      EFilterContextNames.patternMulti,
      EFilterContextNames.label,
      EFilterContextNames.content,
      EFilterContextNames.extension,
      EFilterContextNames.fileTypeCategory,
      EFilterContextNames.fileSize,
      EFilterContextNames.tag,
      EFilterContextNames.tag_type,
      EFilterContextNames.root_folder,
      EFilterContextNames.functional_role,
      EFilterContextNames.doc_type,
      EFilterContextNames.bizList_item_clients,
      EFilterContextNames.bizList_item_clients_extraction,
      EFilterContextNames.bizList_item_consumers_extraction_rule,
      EFilterContextNames.file_created_date,
      EFilterContextNames.file_modified_date,
      EFilterContextNames.file_accessed_date,
      EFilterContextNames.file_accessed_date,
      EFilterContextNames.matter
    ];
    return arr;
  }

}
class EntityDisplayTypes implements IDisplayType{
  constructor(public value:string) {
  }

  equals(o) {
    if(!o) return false;
    return this.value == o.value;
  }

  toString() {
    return this.value;
  }

  // values
  static acl_reads = new EntityDisplayTypes("acl_reads");
  static acl_writes = new EntityDisplayTypes("acl_writes");
  static groups = new EntityDisplayTypes("groups");
  static folders = new EntityDisplayTypes("folders");
  static folders_flat = new EntityDisplayTypes("folders_flat");
  static files = new EntityDisplayTypes("files");
  static owners = new EntityDisplayTypes("owners");
  static regulations = new EntityDisplayTypes("regulations");
  static metadatas = new EntityDisplayTypes("metadatas");
  static metadatas_type = new EntityDisplayTypes("metadatas_type");
  static patterns = new EntityDisplayTypes("patterns");
  static labels = new EntityDisplayTypes("labels");
  static patterns_multi = new EntityDisplayTypes("patterns_multi");
  static share_permissions_allow_read = new EntityDisplayTypes("share_permissions_allow_read");
  static contents = new EntityDisplayTypes("contents");
  static extensions = new EntityDisplayTypes("extensions");
  static file_type_categories = new EntityDisplayTypes("file_type_categories");
  static file_sizes = new EntityDisplayTypes("file_sizes");
  static functional_roles = new EntityDisplayTypes("functional_roles");
  static tags = new EntityDisplayTypes("tags");
  static tag_types = new EntityDisplayTypes("tag_types");
  static root_folders = new EntityDisplayTypes("root_folders");
  static doc_types = new EntityDisplayTypes("doc_types");
  static bizlistItems_clients = new EntityDisplayTypes("bizlist_items_clients");
  static bizlistItem_clients_extractions = new EntityDisplayTypes("bizlist_item_clients_extractions");
  static bizlistItem_consumers_extraction_rules = new EntityDisplayTypes("bizlist_item_consumers_extraction_rules");
  static file_created_dates = new EntityDisplayTypes("file_created_dates");
  static file_modified_dates = new EntityDisplayTypes("file_modified_dates");
  static file_accessed_dates = new EntityDisplayTypes("file_accessed_dates");
  static all_files = new EntityDisplayTypes("all_files");
  static doc_types_flat = new EntityDisplayTypes("doc_types_flat");
  static email_sender_domain = new EntityDisplayTypes("sender_domain");
  static email_recipient_domains = new EntityDisplayTypes("recipient_domain");
  static email_sender_address = new EntityDisplayTypes("sender_address");
  static email_recipient_address = new EntityDisplayTypes("recipient_address");
  static departments = new EntityDisplayTypes("departments");
  static departments_flat = new EntityDisplayTypes("departments_flat");
  static matters = new EntityDisplayTypes("matters");
  static matters_flat = new EntityDisplayTypes("matters_flat");
  static media_type = new EntityDisplayTypes("media_type");

  static all() {

    var arr = [
      EntityDisplayTypes.groups,
      EntityDisplayTypes.files,
      EntityDisplayTypes.file_created_dates,
      EntityDisplayTypes.file_modified_dates,
      EntityDisplayTypes.file_accessed_dates,
      EntityDisplayTypes.folders,
      EntityDisplayTypes.folders_flat,
      EntityDisplayTypes.owners,
      EntityDisplayTypes.regulations,
      EntityDisplayTypes.metadatas,
      EntityDisplayTypes.patterns,
      EntityDisplayTypes.patterns_multi,
      EntityDisplayTypes.share_permissions_allow_read,
//      EntityDisplayTypes.labels,
      EntityDisplayTypes.contents,
      EntityDisplayTypes.extensions,
      EntityDisplayTypes.file_type_categories,
      EntityDisplayTypes.file_sizes,
      EntityDisplayTypes.acl_reads,
      EntityDisplayTypes.acl_writes,
      EntityDisplayTypes.all_files,
      EntityDisplayTypes.tags,
      EntityDisplayTypes.doc_types,
      EntityDisplayTypes.functional_roles,
      EntityDisplayTypes.bizlistItems_clients,
      EntityDisplayTypes.bizlistItem_clients_extractions,
      EntityDisplayTypes.bizlistItem_consumers_extraction_rules,
      EntityDisplayTypes.email_sender_domain,
      EntityDisplayTypes.email_recipient_domains,
      EntityDisplayTypes.email_sender_address,
      EntityDisplayTypes.email_recipient_address,
      EntityDisplayTypes.departments,
      EntityDisplayTypes.departments_flat,
    ];
    return arr;
  }

  static allLegalInstallationMode() {

    var arr = [
      EntityDisplayTypes.groups,
      EntityDisplayTypes.files,
      EntityDisplayTypes.file_created_dates,
      EntityDisplayTypes.file_modified_dates,
      EntityDisplayTypes.file_accessed_dates,
      EntityDisplayTypes.folders,
      EntityDisplayTypes.folders_flat,
      EntityDisplayTypes.owners,
      EntityDisplayTypes.regulations,
      EntityDisplayTypes.metadatas,
      EntityDisplayTypes.patterns,
      EntityDisplayTypes.patterns_multi,
      EntityDisplayTypes.share_permissions_allow_read,
//      EntityDisplayTypes.labels,
      EntityDisplayTypes.contents,
      EntityDisplayTypes.extensions,
      EntityDisplayTypes.file_type_categories,
      EntityDisplayTypes.file_sizes,
      EntityDisplayTypes.acl_reads,
      EntityDisplayTypes.acl_writes,
      EntityDisplayTypes.all_files,
      EntityDisplayTypes.tags,
      EntityDisplayTypes.doc_types,
      EntityDisplayTypes.functional_roles,
      EntityDisplayTypes.bizlistItems_clients,
      EntityDisplayTypes.bizlistItem_clients_extractions,
      EntityDisplayTypes.bizlistItem_consumers_extraction_rules,
      EntityDisplayTypes.email_sender_domain,
      EntityDisplayTypes.email_recipient_domains,
      EntityDisplayTypes.email_sender_address,
      EntityDisplayTypes.email_recipient_address,
      EntityDisplayTypes.matters,
      EntityDisplayTypes.matters_flat
    ];
    return arr;
  }


  static convertEFilterContextNames(type:EFilterContextNames)
  {
    if(type.equals( EFilterContextNames.group))
    {
      return EntityDisplayTypes.groups;
    }
    if(type.equals(  EFilterContextNames.owner))
    {
      return EntityDisplayTypes.owners;
    }
    if(type.equals(  EFilterContextNames.regulation))
    {
      return EntityDisplayTypes.regulations;
    }
    if(type.equals(  EFilterContextNames.metadata))
    {
      return EntityDisplayTypes.metadatas;
    }
    if(type.equals(  EFilterContextNames.pattern))
    {
      return EntityDisplayTypes.patterns;
    }
    if(type.equals(  EFilterContextNames.label))
    {
      return EntityDisplayTypes.labels;
    }
    if(type.equals(  EFilterContextNames.patternMulti))
    {
      return EntityDisplayTypes.patterns_multi;
    }
    if(type.equals(  EFilterContextNames.share_permission_allow_read))
    {
      return EntityDisplayTypes.share_permissions_allow_read;
    }
    if(type.equals(  EFilterContextNames.acl_read))
    {
      return EntityDisplayTypes.acl_reads;
    }
    if(type.equals(  EFilterContextNames.functional_role))
    {
      return EntityDisplayTypes.functional_roles;
    }
    if(type.equals( EFilterContextNames.acl_write))
    {
      return EntityDisplayTypes.acl_writes;
    }
    if(type.equals( EFilterContextNames.folder))
    {
      return EntityDisplayTypes.folders;
    }

    if(type.equals( EFilterContextNames.file))
    {
      return EntityDisplayTypes.files;
    }
    if(type.equals( EFilterContextNames.tag))
    {
      return EntityDisplayTypes.tags;
    }
    if(type.equals( EFilterContextNames.tag_type))
    {
      return EntityDisplayTypes.tag_types;
    }
    if(type.equals( EFilterContextNames.root_folder))
    {
      return EntityDisplayTypes.root_folders;
    }
    if(type.equals( EFilterContextNames.doc_type))
    {
      return EntityDisplayTypes.doc_types;
    }

    if(type.equals( EFilterContextNames.bizList_item_clients))
    {
      return EntityDisplayTypes.bizlistItems_clients;
    }
    if(type.equals( EFilterContextNames.bizList_item_clients_extraction))
    {
      return EntityDisplayTypes.bizlistItem_clients_extractions;
    }
    if(type.equals( EFilterContextNames.bizList_item_consumers_extraction_rule))
    {
      return EntityDisplayTypes.bizlistItem_consumers_extraction_rules;
    }
    if(type.equals( EFilterContextNames.file_created_date))
    {
      return EntityDisplayTypes.file_created_dates;
    }
    if(type.equals( EFilterContextNames.file_modified_date))
    {
      return EntityDisplayTypes.file_modified_dates;
    }
    if(type.equals( EFilterContextNames.file_accessed_date))
    {
      return EntityDisplayTypes.file_accessed_dates;
    }
    if(type.equals( EFilterContextNames.all_files))
    {
      return EntityDisplayTypes.all_files;
    }
    if(type.equals( EFilterContextNames.department))
    {
      return EntityDisplayTypes.departments;
    }
    if(type.equals( EFilterContextNames.matter))
    {
      return EntityDisplayTypes.matters;
    }
  }

  static parse(entityDisplayTypeName:string)
  {
    return  EntityDisplayTypes.all().filter(i=>i.toString()==entityDisplayTypeName)[0];
  }

  static parseLegalInstallationMode(entityDisplayTypeName:string)
  {
    return  EntityDisplayTypes.allLegalInstallationMode().filter(i=>i.toString()==entityDisplayTypeName)[0];
  }
}

class EFieldTypes {
  constructor(public value:string) {
  }

  toString() {
    return this.value;
  }

  // values
  static type_string = new EFieldTypes("string");
  static type_number = new EFieldTypes("number");
  static type_boolean = new EFieldTypes("boolean");
  static type_date = new EFieldTypes("date");
  static type_dateTime = new EFieldTypes("dateTime");
  static type_other = new EFieldTypes("");
}



class EFilterOperators {
  constructor(public value:string) {
  }

  toString() {
    return this.value;
  }

  // values
  static equals = new EFilterOperators("eq");
  static notEquals = new EFilterOperators("neq");
  static contains = new EFilterOperators("contains");
  static doesNotContain = new EFilterOperators("doesnotcontain");
  static greaterThen = new EFilterOperators("ge");
  static lessThen = new EFilterOperators("lt");

  static parse(str:string)
  {
    if(str == EFilterOperators.equals.toString())
       return EFilterOperators.equals;
    if(str == EFilterOperators.notEquals.toString())
      return EFilterOperators.notEquals;
    if(str == EFilterOperators.contains.toString())
      return EFilterOperators.contains;
    if(str == EFilterOperators.doesNotContain.toString())
      return EFilterOperators.doesNotContain;
    if(str == EFilterOperators.greaterThen.toString())
      return EFilterOperators.greaterThen;
    if(str == EFilterOperators.lessThen.toString())
      return EFilterOperators.lessThen;
  }
}
class ECriteriaOperators {
  constructor(public value:string) {
  }

  toString() {
    return this.value;
  }

  // values
  static and = new ECriteriaOperators("AND");
  static or = new ECriteriaOperators("OR");

}

class EPolicyTabType implements IDisplayType {
  constructor(public value:string) {
  }

  toString() {
    return this.value;
  }

  // values
  static policies = new EPolicyTabType("policies");
  static objects = new EPolicyTabType("objects");

  static all() {

    var arr = [
      EPolicyTabType.policies  ,
      EPolicyTabType.objects,


    ];
    return arr;
  }

  static parse(policyTabTypeName:string)
  {
    return  EPolicyTabType.all().filter(i=>i.toString()==policyTabTypeName)[0];

  }
}

enum  ESettingsTabType {
  scheduleGroups = <any>"scheduleGroups",
  tagTypes = <any>"tagtypes",
  dateFilters = <any>"dateFilters",
  searchPatterns = <any>"searchPatterns",
  patternCategories = <any>"patternCategories",
  regulations = <any>"regulations",
  docTypes = <any>"doctypes",
  mailSettings = <any>"mailSettings",
  functionalRoles = <any>"functionalRoles",
  mediaProcessors = <any>"mediaProcessors",
  mediaConnections = <any>"mediaConnections",
  customerDataCenters = <any>"customerDataCenters",
  ldapConnections = <any>"ldapConnections",
  licenseKey = <any>"licenseKey",
  settingsDepartments = <any>"departments",
  settingsMatters = <any>"matters",
  logLevels = <any>"logLevels",
  savedFilters = <any>"filters",
  savedViews = <any>"views",
  savedCharts = <any>"charts",
  savedWidgets = <any>"widgets",
  savedDashboards = <any>"dashboards"
}


class ToolbarItem
{
  name:string;
  element:any;
  iconClass:string;
  showToolbarModelName:string;
  visible:boolean;
  showByDefault:boolean;
  multiple:ToolbarItem[];
  changeShowToolbarModel:(val)=>void;
  order:number;
}

class ParamOption<T>
{
  id:any;
  value:T;
  display:any;
  subdisplay:any;
  title:any;
  groupByLable:string;
  icon:string;
  displayClass:string;
  static Empty():ParamOption<any>{
    var g = new ParamOption<any>();
    g.id='a';g.value='b';g.display='c';g.subdisplay='d';
    return g;
  }
}
class ParamActiveItem<T>
{
  active:boolean;
  item:ParamOption<T>;
  static Empty():ParamActiveItem<any>{
    var g = new ParamActiveItem();
    (<any>g).item='a';g.active=true;
    return g;
  }
}
interface IDisplayType
{
  toString();
}

class KeyValuePair {
   constructor(public key:string,public value:any) {
   }
}
class NameValuePair {
   constructor(public name:string,public value:any) {
   }
}

class EFullyContainedFilterState {
  constructor(public value:string) {
  }

  equals(o) {
    return this.value === o.value;
  }

  toString() {
    return this.value;
  }

  // values
  static all = new EUserSearchOption("all");
  static notContained = new EUserSearchOption("notContained");
  static fullyContained = new EUserSearchOption("fullyContained");
  static parse=function(value)
  {
    if(value == EFullyContainedFilterState.all.toString())
    {
      return EFullyContainedFilterState.all;
    }
    if(value == EFullyContainedFilterState.fullyContained.toString())
    {
      return EFullyContainedFilterState.fullyContained;
    }
    if(value == EFullyContainedFilterState.notContained.toString())
    {
      return EFullyContainedFilterState.notContained;
    }
  }

}
class ProcessingErrorsPhase {
  static CHANGE_LOG='changelog';
  static INGESTION='ingestion';
  static SCAN='scan';


}



module ui {


  export interface IDisplayElementFilterData {
    filter:IKendoFilter;
    text:KeyValuePair[];
    isClientFilter?:boolean;
 }
  export interface IKendoFilter {

  }
  export interface IGridOptionsBuilder<T> {
    createGrid(url:string, urlParams,
               fields:IFieldDisplayProperties[],
               maxRecordsPerRequest:number,
               dataManipulationFn:(data:T[])=> T[],
               totalAggregatedCountFn:(totalAggregatedCount,pageNumber:number,data:T[])=> void,
               autoBind:boolean,
               rowTemplate?:string, updateUrl?:string,filterData?:any,onDataCompleteFn?:(pageSize:number,pageSizeReduced:boolean)=>void,
               manipulateReadRequestFn?:(queryParams: any)=>void,onErrorReadResponseFn?:(status,url,error: any)=>void,totalElementsIsReturned?:boolean,onExcelPreparationsFn?:(workbook: any)=>void):IGridHelper;

  }

  export interface IGridHelper {
    gridSettings:any;
    selectFirstRow(gridElement:any):void;
    filter(gridElement:any, filterField:string, filterValue:any, fieldType:string, operator?:EFilterOperators):void;
    filterKendoFormat(gridElement:any,filter:ui.IKendoFilter,isClientFilter?:boolean):void;
    setFilterKendoFormatToDataSource(dataSource:any,filter:ui.IKendoFilter):void;
    sort(gridElement:any, sortField:string, descDirection:boolean):void;
    loadData(gridElement:any,url,pageNumber:number,filter,isClientFilter,sortField,descDirection):void;
    registerToFocusEvent(gridElement:any);
    reloadData(gridElement:any, finishFunc?: any):void;
    unselectAll(gridElement:any):void;
    getCurrentUrl(gridElement:any):string;
    getCurrentFilter(gridElement:any):any;
    refreshDisplay(gridElement:any):void;
    refreshTemplate(gridElement:any, template:any):void;
    saveAsExcel(gridElement:any, fileName:string):void;
    saveAsPdf(gridElement:any, fileName:string):void;
    clear(gridElement:any):void;
    resizeGrid(gridElement:any,addNumber?,marginBottom?:number):void;
    scrollSelectedRowIntoView(gridElement:any):void;
    nextPage(gridElement:any):void;
    prevPage(gridElement:any):void;
    hidePageButtonsIfNeeded(gridElement:any):void;
    setData(gridElement:any,data:any[]):void;
    getData(gridElement:any):any[];
    getPageNumber(gridElement:any):number;
    getTotalElementsCount(gridElement:any):number;
    getSelectedRowsUid(gridElement:any):any;
    getRowDataByUid(gridElement:any,uid:string):any;
    setMultipleRowSelection(gridElement:any,enabled:boolean):void;
    selectRowByUid(gridElement:any,rowUidToSelect):boolean;
    selectRowsByUid(gridElement:any,rowUidsToSelect:string[]):boolean;
    selectRowByDataItem(gridElement:any,dataItemIds:number[]):boolean;
    selectPrevRow(gridElement:any):boolean;
    setPage(gridElement:any, pageNumber:number):void;
    getSelectedRowsDataItems(gridElement:any):any[];
    editCellMode(gridElement:any,targetElement:any):void;
    closeEditCellMode(gridElement:any):void;
    setOnlyOnePageStateClass(gridElement,totalElements:number,configuration, entityDisplayType: EntityDisplayTypes):void;
  }

  export interface ITreeOptionsBuilder<T> {
    createTree<T>(elementName:string,url:string, getDataUrlParam,
                  fields:IFieldDisplayProperties[],
                  maxRecordsPerRequest:number,
                  hasChildrenFn:(data:T)=> boolean,
                  dataManipulationFn:(data: T[])=> T[],
                  autoBind:boolean,
                  itemTemplate?:string,filterData?:any,manipulateReadRequestFn?:(queryParams: any)=>void,handleMultiSelectionChangeFn?:any,
                  onErrorReadResponseFn?:(error: any)=>void, onExpandNodeFn?:(nodeElement: any)=>void):ITreeHelper;

  }

  export interface ITreeHelper {
    treeSettings:any;
    refresh(treeElement:any, url, urlParams:any):void;
    reloadData(treeElement:any):void;
    unselectAll(treeElement:any):void;
    expandFirstNode(treeElement:any):void;
    expandPath(treeElement:any,text:string,isInPathFun:(dataItem,pathItem)=>any,expandSelected:boolean):any;
    selectFirstNode(treeElement:any):void;
    resizeTree(treeElement:any,marginBottom:number):void;
    scrollSelectedNodeIntoView(treeElement:any):void;
    filterKendoFormat(treeview:any,filter:any):void;
    refreshDisplay(treeElement:any):void;
    getCurrentUrl(gridElement:any):string;
    getCurrentFilter(gridElement:any):any;
    getCheckedItems(treeElement):any[];
    getMultiSelectedItems(treeElement):any[];
  }

  export interface IChartHelper {
    chartSettings:any;
    setTitle(chartElement:any,title:string) :void;
    showTitle(chartElement:any,show:boolean):void;
    saveAsExcel(chartElement:any, fileName:string):void;
    saveAsPdf(chartElement:any, fileName:string,successFn):void;
    saveAsImage(chartElement:any, fileName:string,successFn):void;
  }
  export interface IFilterFactory {
    searchTextAbbreviationsDic:{}
    createFilterByProperties(): (value) => ICriteriaComponent;
    isInvertedFilterEnabled(criteria:SingleCriteria): boolean;
    createTagFilter(): (tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators) => ICriteriaComponent;
    createExplicitTagAssociationFilter(): (tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators) => ICriteriaComponent;
    createMailFilter(): (value,name?) => ICriteriaComponent;
    createTagTypeFilter(): (tags:Entities.DTOFileTagType[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators) => ICriteriaComponent;
    createExplicitTagTypeAssociationFilter(): (tags:Entities.DTOFileTagType[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators) => ICriteriaComponent;
    createDocTypeFilter(): (docTypes:Entities.DTODocType[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ) => ICriteriaComponent;
    createSubFolderFilter(): (value,name?) => ICriteriaComponent;
    createSubDepartmentFilter(): (value,name?) => ICriteriaComponent;
    createSubMatterFilter(): (value,name?) => ICriteriaComponent;
    createFileDuplicationsFilter(): (value,name?) => ICriteriaComponent;
    createDateRangeFilter(): (dateRanges:Entities.DTODateRangeItem[],criteriaOperation:ECriteriaOperators,filterFieldType:string,operator?:EFilterOperators) => ICriteriaComponent;
    createFunctionalRoleFilter(): (funcRoles:Users.DtoFunctionalRole[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators) => ICriteriaComponent;
    createTagTypeMismatchFilter(): (value) => ICriteriaComponent;
    createBizListItemAssociationTypeFilter(): (value) => ICriteriaComponent;
    createBizListItemExtractionTypeFilter(displayedFilterType): (value) => ICriteriaComponent;
    createSubDocTypesFilter(): (value,name?) => ICriteriaComponent;
    createDepartmentFilter(): (department:Entities.DTODepartment,criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ) => ICriteriaComponent;
    createDepartmentsFilter(): (departments:Entities.DTODepartment[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ) => ICriteriaComponent;
    createMatterFilter(): (department:Entities.DTODepartment,criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ) => ICriteriaComponent;
    createMattersFilter(): (departments:Entities.DTODepartment[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ) => ICriteriaComponent;
    createFilterByID(entityDisplayTypes:EntityDisplayTypes,displayedFilterType?):(value,name) => ICriteriaComponent
    createFilterBySearchText(entityDisplayTypes:EntityDisplayTypes):(value) => ICriteriaComponent
    createRootFolderNameLikeFilter() :(searchText:string) => ICriteriaComponent
    createAllByFieldFilter():(fieldName:string,filterOperator:EFilterOperators,displayText:string)=> ICriteriaComponent
    createUnassociatedFilter(entityDisplayTypes:EntityDisplayTypes,displayedFilterType?):() => ICriteriaComponent
    createContentSearch():(value) => ICriteriaComponent
    createBizListItemStateFilter(state):() => ICriteriaComponent
    createSearchPatternsItemStateFilter(state):() => ICriteriaComponent
    createFilterByUngrouped():ICriteriaComponent
    createFilterByExpiredUser():ICriteriaComponent
    createFilterBySpecificProcessingState(state):ICriteriaComponent
    createFilterByGeneralProcessingState(state):ICriteriaComponent
    createFilterByGroupDetachedFiles():ICriteriaComponent
    createFilterByNoneMailRelatedDataFiles():ICriteriaComponent
    createFilterByOpenAccessRights():ICriteriaComponent
    createFullyContainedFilterById(isFullyContained:boolean, from:EFilterContextNames,to:EFilterContextNames,includeSubEntityDataLeft?:boolean):(id,name?) => ICriteriaComponent
    getFilterReportDisplayedName (tag:SingleCriteria):string;
    getFilterDataDisplayedText(filterData:FilterData):KeyValuePair[];
    getFullyContainedText(val:EFullyContainedFilterState):string;
    isFullyContainedText(isContained:boolean):string;
    getUnassociatedText (entityDisplayType:EntityDisplayTypes):string;
  }

  export interface IChartOptionsBuilder {
    createPie(histogramData,chartTitle:string,
                 fields:ui.IChartFieldDisplayProperties[],
                 seriesColors:string[]):IChartHelper;
    createBarChart(histogramData,chartTitle:string,
                   fields:ui.IChartFieldDisplayProperties[],
                   gap);
    createColumnChart(stackedHistogramData,chartTitle:string,
                      fields:ui.IChartFieldDisplayProperties[],
                      seriesColors:string[]);
    createLineChart(stackedHistogramData,chartTitle:string,
                      fields:ui.IChartFieldDisplayProperties[],
                      seriesColors:string[]);
    createStackedBarChart(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                          fields:ui.IChartFieldDisplayProperties[],
                          seriesColors:string[]);

    createStackedBar100Chart(stackedHistogramData,chartTitle:string,
                             fields:ui.IChartFieldDisplayProperties[]);
  }

  export interface ISplitViewBuilderHelper {
    parseMarkedItems (markableItems:any[],entityDisplayType:EntityDisplayTypes,element,gridOptions) ;
    parseTags(taggableItem:Entities.ITaggable)
    parseDocTypes(assigneeItem:Entities.IDocTypesAssignee,configuration);
    parseDepartment(assigneeItem:Entities.IDepartmentAssignee,configuration);
    parseFunctionalRoles(assigneeItem:Entities.IFunctionalRolesAssignee);
    parseAssociatedBizListItems(assigneeItem:Entities.IBizListAssociatable);
    parseExtractedBizListItems(assigneeItem:Entities.IBizListExtractable,bizListDic);
    getMarkedItemTemplate(scope,eventCommunicator);
    setFunctionalRolesTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setTagsTemplate2(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setSearchPatternsTemplate2(scope,patternsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setLabelsTemplate2(scope,patternsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setDocTypeTemplate2(scope,tagsField,eventCommunicator,dialogs,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean, withoutDropDown?: boolean);
    setDepartmentTemplate(scope,tagsField,eventCommunicator,dialogs,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean, withoutDropDown?: boolean);
    setAssociatedBizListItemsTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setExtractedBizListItemsTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    getFilesAndFilterTemplate (numOfFilesFieldName,numOfFilteredFilesFieldName,inFolderPercentageFieldName):string;
    parseFilesAndFilterTemplate(entity:Entities.IFunctionalRolesAssignee);
    getFilesAndFilterTemplateFlat (numOfFilesFieldName,numOfFilteredFilesFieldName,inFolderPercentageFieldName,tooltip?:string):string;
    getFileTypesTemplate (processedExcelFiles,processedPdfFiles,processedWordFiles,processedOtherFiles):string;
    getFileProcessingErrorsTemplate (scanErrors,scanProcessingErrors):string;
    getLastRunTimingTemplate (lastRunStartTime,lastRunElapsedTime,lastRunEndTime,isRunning):string;
    getStatusTemplate (status,isEndedSuccessfully,isEndedWithError,isPaused,translatePrefix?):string;
    getCountTemplate (countFiledName,title,titleClass?,translatePrefix?,countFiledNameTooltip?):string;
    getTitledTemplate(innerTemplate,title,titleClass?,translatePrefix?,countFiledNameTooltip?):string;
    getToggleButtonTemplate(active,titleText,onClickCallBack,disabledField?, hoverTitle?):string;
    getTagStyleId(tag:Entities.DTOFileTag):number;
    getMetadataStyleId(typeIndex:number):number;

    getFileWorklowDataTemplate():string;
    getTagTypeStyleId(tag:Entities.DTOFileTagType):number;
    getDocTypeStyleId(docType):number;
    getFunctionalRoleStyleId(funcRoleId:number):number;
    getDepartmentStyleId(department:Entities.DTODepartment):number;

    getDocTypeDepth(docType):number;
    getDepartmentDepth(docType):number;
    bizListTypeIcon(listTypeName):string;
    getFilesNumPercentage(filteredNumber:number , totalNumber : number):number;
  }

  export interface IGDPRViewBuilderHelper{
    initTagTypes():angular.IPromise<boolean> ;

    getConfidenceLevelTagTypeId():number;
    getRecommedationTagTypeId():number;
    getActionTagTypeId():number;
    getWorkflowTypeId():number;
    getWorkflowStateTypeId():number;
    getRecToAppliedTags():any;
    getAppliedToRecTags():any;
    getTagIconsById():any;
    getFileIrrelevantTagId():number;
    getFileRelevantTagId():number;
    getDisplayedFilterPanelTagTypeIds():any;
    parseFilesResolveStatus(taggableItem:Entities.ITaggable,fileTagDtos:Entities.DTOAggregationCountItem< Entities.DTOFileTag>[]);
    setFilesResolveStatusTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    parseGDPRRecommendation(taggableItem:Entities.ITaggable);
    setGDPRRecommendationTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    setGDPRActionTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    parseConfidenceLevel(taggableItem:Entities.ITaggable);
    setConfidenceLevelTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    parseConfidenceLevelCounters(taggableItem:Entities.ITaggable,fileTagDtos:Entities.DTOAggregationCountItem< Entities.DTOFileTag>[]);
    setConfidenceLevelCountersTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    parseIrrelevant(taggableItem:Entities.ITaggable);
    parseTagsLeaveOnlyRelevantTags(taggableItem:Entities.ITaggable);
    setIrrelevantTemplate(scope,tagsField,eventCommunicator,doNotHaveContainer?:boolean,noAggragetedDTo?:boolean);
    getFilesAndFilterTemplate (numOfFilesFieldName,numOfFilteredFilesFieldName,inFolderPercentageFieldName):string;

  }
  export interface ITreeListOptionsBuilder<T> {
    create<T>(elementName:string,url:string, getDataUrlParam,
                  fields:IFieldDisplayProperties[],
                  maxRecordsPerRequest:number,
                  hasChildrenFn:(data:T)=> boolean,
                  dataManipulationFn:(data: T[])=> T[],
                  autoBind:boolean,
                  itemTemplate?:string,filterData?:any,onDataCompleteFn?,onExportToExcelPreparations?,onCollapseFn?,onExpandFn?,onErrorReadResponseFn?:(error: any)=>void):IGridHelper;
    createFromGridOptions<T>(gridOptions:kendo.ui.GridOptions, fields:IFieldDisplayProperties[],filterData,onCollapseFn,onExpandFn):IGridHelper;

  }

  export interface ITreeListHelper {
    treeSettings:any;
    refresh(treeElement:any, url, urlParams:any):void;
    reloadData(treeElement:any):void;
    unselectAll(treeElement:any):void;
    expandFirstNode(treeElement:any):void;
    expandPath(treeElement:any,text:string,isInPathFun:(dataItem,pathItem)=>any):any;
    selectFirstNode(treeElement:any):void;
    filterKendoFormat(treeview:any,filter:any):void;
    refreshDisplay(treeElement:any):void;
    getCurrentUrl(gridElement:any):string;
    getCurrentFilter(gridElement:any):any;
  }
  export interface IFacetedSearchOptionsBuilder {
    createFacetSearch(fields:IFacetedSearchFieldDisplayProperties[],
                      data:Entities.DTOFacetedSearchField[]):any

  }
  export interface IFacetedSearchFieldDisplayProperties {
    fieldName:string;
    fieldType:any;
    title:string;
    type: any;
    scrollbar: boolean;
  }
  export interface IFacetedSearchField {
    fieldName:string;
    title:string;
    type: any;
    scrollbar: boolean;
    fieldType:any;
    fields: ui.IFacetedSearchFieldValue[];
    range:boolean;
  }
  export interface IFacetedSearchFieldValue {
    value:string;
    numberOfItems:number;
  }
  export interface IFieldDisplayPropertiesBase {
    fieldName?: string;
    type?: any;
    title?: string;
    template?: string;
    format?:string;
  }

  export interface IChartFieldDisplayProperties extends  IFieldDisplayPropertiesBase{
    isCategoryField?: boolean;
    isCategoryDataField?: boolean;
    isSeriesField?:boolean;
    dir?:string;
    isToolTipField?:boolean;
    isValueField?:boolean;

  }
  export interface IStackedChartData {
    categories: ICategoryChartData[];
    series: ISeriesChartData[];
  }

  export interface ISeriesChartData
  {
    name:string;
    data:any[];
  }
  export interface ICategoryChartData
  {
    name:string;
    data?:any;
  }
  export interface IFieldDisplayProperties extends  IFieldDisplayPropertiesBase {
    fieldName?: string;
    isPrimaryKey?: boolean;
    type: any;
    nullable?: boolean;
    filterable?: boolean;
    footerTemplate?: any;
    format?: string;
    groupHeaderTemplate?: any;
    groupFooterTemplate?: any;
    headerAttributes?: any;
    headerTemplate?: any;
    displayed: boolean;
    hidden?: boolean;
    sortable?: boolean;
    editable?: boolean;
    template?: any;
    title?: string;
    parse?:(ts:any)=>any;
    validation?:any;
    editor?:any;
    width?:any;
    ellipsis?:boolean;
    centerText?:boolean;
    noWrapContent?:boolean;
    ddlInside?:boolean;
    exportTitle?:string;
  }
  export interface IHierarchicalFieldDisplayProperties extends IFieldDisplayProperties {
    isParentId?:boolean;
    expandable?:boolean;
  }
  export interface ISortingDetails {
    direction?: string;
    property: any;
    ignoreCase?: boolean;
    ascending?: boolean;
  }


  export class FilterDescriptor {
    logic:string;
    field: string;
    value: any;
    operator: string;
    ignoreCase : boolean;
    name: string;
    filters : FilterDescriptor[]
  }

  export class SavedChartViewConfiguration
  {
    showUnassociated:boolean;
    removedValueIds:string[];
    showRemovedValues:boolean;
    forceDisableUnassociated:boolean;
  }


  export class SavedDiscoveredFilter  {
    id:number;
    name:string;
    savedFilterDto:Entities.SavedFilterDto;
    leftEntityDisplayTypeName:string;
    rightEntityDisplayTypeName:string;
    globalFilter:boolean;
    username:string;
    displayType:Entities.EFilterDisplayType;
  }


  export class SavedChart {
    name:string;
    chartDisplayType:string;
    chartType:string;
    mainEntityType:string;
    maximumNumberOfEntries:number;
    minimumCount:number;
    showOthers:boolean;
    savedFilterDto:Entities.SavedFilterDto;
    populationJsonUrl:string;
    populationSubsetJsonFilter:SavedChartViewConfiguration;
    showTabularView:boolean;
    sortType:string;
    id:number;
    hidden:boolean;
    owner: Users.DTOUser;
    globalFilter:boolean;

    static sortType_SIZE= 'SIZE';
    static chartType_FILES_BY_ENTITY= 'FILES_BY_ENTITY';
    static chartDisplayType_BAR_HORIZONTAL= 'BAR_HORIZONTAL';
    static chartDisplayType_PIE= 'PIE';
    static chartDisplayType_BAR_VERTICAL= 'BAR_VERTICAL';
  }
}

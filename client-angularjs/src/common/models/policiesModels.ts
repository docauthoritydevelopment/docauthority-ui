module Actions {
  export class DtoActionDefinition {
    id:string;
    name:string;
    javaMethodName:string;
    description:string;
    actionClass:string;

  }


  export class DtoActionExecutionTaskStatus {
    actionTriggerId:number;
    status:string;
    progressMark:number;
    progressMaxMark:number;
    triggerTime:number;
    type:string;
    userId:number;
    userName:string;
    scriptFolders:string


    static type_POLICY = 'POLICY';
    static type_IMMEDIATE="IMMEDIATE";

    static status_FINISHED_SUCCESSFULLY = 'FINISHED_SUCCESSFULLY';
    static status_NA="NA";
    static status_RUNNING="RUNNING";
    static status_FAILED="FAILED";
  }

  export class DtoExecuteActionRequest {
    actionDefinitionId:string;
    parameters:any;

  }
}

module Policy {

  export interface IPolicyFilterParamItem
  {
    id:string;
    type:EFilterParamType;
    values:any[];
  }

  export enum EFilterParamType
  {
    savedFilter,
    fileGroup,
    folder,
    file,
  }
  export enum EActionClass
  {
    sysFilter,
    entity,
    user,
    email,
    path,
    mediaLocation
  } //use it for strings: EActionClass[EActionClass.user]

  export class DtoPolicyObjectType {
    id:number;
    name:string;
    description:string;
    isAbstractType:boolean;
    parentId:number;
    parents:number[];
    actionClass:EActionClass;

    static Empty():DtoPolicyObjectType{
      var g = new DtoPolicyObjectType();
      g.id=1;g.name='c';g.description='d';g.parentId=2;g.actionClass=EActionClass.email;g.isAbstractType=true;
      return g;
    }
  }

  export class DtoPolicyLayerType {
    id:number;
    name:string;
    description:string;
    parentId:number;
    supportedActions:DtoPolicyActionTemplate[];

    static Empty():DtoPolicyLayerType{
      var g = new DtoPolicyLayerType();
      g.id=1;g.name='c';g.description='d';g.parentId=2;
      return g;
    }
  }

  export class DtoPolicyObjectItemBase  {
    id:string;
    name:string;
    policyObjectType:DtoPolicyObjectType;
    values:DtoPolicyObjectItemBaseValue<string>[];
    isSystemObject:boolean;


    static Empty():DtoPolicyObjectItemBase{
      var g = new DtoPolicyObjectItemBase();
      g.id='b';g.name='c'; g.isSystemObject=true;
      return g;
    }
  }

  export class DtoPolicyObjectItemBaseValue<T>  {
    active:boolean;
    item:ParamOption<T>; //string[];

  }



  export class DtoPolicyLayerItemBase {
    layerType:DtoPolicyLayerType;
    id:string;
    name:string;
    description:string;
    filter:DtoPolicyLayerParamItemBase[];
    actionTemplateDto:DtoPolicyActionTemplate;
    actionParams:DtoPolicyLayerParamItemBase[];


    static Empty():DtoPolicyLayerItemBase{
      var g = new DtoPolicyLayerItemBase();
   g.id='b';g.name='c';g.description='d';g.actionTemplateDto=DtoPolicyActionTemplate.Empty();
      return g;
    }
  }

  export class DtoPolicyActionTemplate {
    id:string;
    name:string;
    description:string;
    params:DtoPolicyActionTemplateParameter[];

     static Empty():DtoPolicyActionTemplate {
      var g = new DtoPolicyActionTemplate();
      g.id = 'b';
      g.name = 'c';
      g.description = 'd';
      g.params=[DtoPolicyActionTemplateParameter.Empty()];
      return g;
    }
  }

  export class DtoPolicyActionTemplateParameter {
    id:string;
    name:string;
    policyObjectTypes:DtoPolicyObjectType[];
    actionId:string;

    static Empty():DtoPolicyActionTemplateParameter {
      var g = new DtoPolicyActionTemplateParameter();
      g.id = 'b';
      g.name = 'c';
      g.policyObjectTypes=[DtoPolicyObjectType.Empty()];
      return g;
    }
  }

  export class DtoPolicyLayerParamItemBase {
    id:string;
    actionTemplateParameter:DtoPolicyActionTemplateParameter;
    values:DtoPolicyObjectItemBaseValue <DtoPolicyObjectItemBase>[];
  }


}

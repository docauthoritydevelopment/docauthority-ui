module GDPR {

  export class DtoErasureRequest {
    id: number;
    name: string;
    workflowDto: DtoWorkflow;
    personDetails: DtoPersonDetails;


    static Empty(): DtoErasureRequest {
      var g = new DtoErasureRequest();
      g.id = 1; (<any>g).workflowDto = 'b';  (<any>g).personDetails = 2;g.name = 'c';
      return g;
    }
  }

  export class DtoPersonDetails {
    personId: string;
    ssn: string;
    firstName: string;
    familyName: string;
    address: string;
    address2: string;
    email: string;

    static Empty(): DtoPersonDetails {
      var g = new DtoPersonDetails();
      g.personId = 'a'; g.ssn = 'b';  g.firstName = 'f'; g.familyName = 'g'; g.address = 'c';g.address2 = 'd';g.email = 'e';
      return g;
    }
  }

  export class DtoWorkflow {
    id: number;
    type: EWorkflowType;
    state: EWorkflowState;
    lastUpdatedDate: number;
    createdDate: number;
    lastUpdatedBy: string;
    createdBy: string;
    confirmedBy: string;


    static Empty(): DtoWorkflow {
      var g = new DtoWorkflow();
      g.id = 1; (<any>g).state = 'b';  g.lastUpdatedDate = 2; g.createdDate = 3; g.lastUpdatedBy = 'c';g.createdBy = 'd';g.confirmedBy = 'e';
      return g;
    }
  }

  export enum  EWorkflowType {
    ERASURE_REQUEST= <any>"ERASURE_REQUEST",

  }

  export enum  EWorkflowState {
    NEW= <any>"New",
    IN_PROGRESS= <any>"IN_PROGRESS",
    PENDING_CONFIRMATION= <any>"PENDING_CONFIRMATION",
    APPROVED= <any>"APPROVED",
    REJECTED= <any>"REJECTED",

  }
}

///<reference path='accountModels.ts'/>
'use strict';

module Entities {

  import DtoRootFolder = Operations.DtoRootFolder;
  import DTOUser = Users.DTOUser;
  import DtoRootFolderSharePermission = Operations.DtoRootFolderSharePermission;
  interface IQueryEntityResult {

  }
  interface IQueryEntityRequest {

  }
  export interface IQuerybleEntity {

  }
  export class EFileCategoryType
  {
    constructor(public value:string){
    }
    toString(){
      return this.value;
    }
    // values
    static businessID = new EFileCategoryType("BUSINESS_ID_FILTER");
    static custom = new EFileCategoryType("CUSTOM");

  }



  export class DTOQueryEntityRequest extends DTORequestBase implements IQueryEntityRequest{
    queryData:IQuerybleEntity;
    take:number;
    skip:number;
    page:number;
    pageSize:number;

  }
  export class  DTOQueryEntityResult<T> extends DTOResultBase implements IQueryEntityResult{
    content:T[];
    totalPages:number;
    numberOfElements:number;
    size:number;
    last:boolean;
    first:boolean;
    sort:any;
    remainingAggregatedCount:number;
    totalAggregatedCount:number;
  }

  export class DTOQueryEntityFIlesResult extends DTOResultBase implements IQueryEntityResult{
    content:IQuerybleEntity[];
    totalPages:number;
    totalElements:number;
    size:number;
    last:boolean;
    first:boolean;
    sort:any;
    numberOfElements:number

  }




  export class DTOAggregationCountFolder implements IQuerybleEntity
  {
    item: IQuerybleEntity//DTOFolderInfo;
    numOfDirectFiles:number;
    numOfAllFiles:number;

    static Empty():DTOAggregationCountFolder{
      var g = new DTOAggregationCountFolder();
      g.item='aa';g.numOfDirectFiles=1;g.numOfAllFiles=2;
      return g;
    }
  }

  export class DTOAggregationCountItem<T> implements IQuerybleEntity
  {
    item: T;
    count:number;
    count2:number;
    subtreeCount:number;
    subtreeCountUnfiltered:number;

    static Empty():DTOAggregationCountItem<any>{
      var g = new DTOAggregationCountItem<any>();
      g.item='a';g.count=1;g.count2=2;g.subtreeCount=3;g.subtreeCountUnfiltered=4;
      return g;
    }
  }

  export interface ITaggable extends IFunctionalRolesAssignee
  {
    id:any;
    fileTagDtos:Entities.DTOFileTag[];
    fileTagAssociations:DTOFileTagAssociation[];
    tagsAsFormattedString?:string;
  }
  export interface ITagsAuthorization extends ITaggable{
    assignTagsAuthorized:boolean;
    deleteTagsAuthorized:boolean;
  }

  export interface IBizListAssociatable extends IFunctionalRolesAssignee
  {
    id:any;
    associatedBizListItems:Entities.DTOSimpleBizListItem[];


  }
  export interface IBizListExtractable extends IFunctionalRolesAssignee
  {
    bizListExtractionSummary:DTOExtractionSummary[];
  }
  export interface IDocTypesAuthorization extends IDocTypesAssignee{
    assignDocTypesAuthorized:boolean;
    deleteDocTypesAuthorized:boolean;
  }

  export interface IDocTypesAssignee extends IFunctionalRolesAssignee
  {
    docTypeDtos:Entities.DTODocType[];
    docTypesAsFormattedString?:string;
  }

  export interface IDepartmentAuthorization extends IDepartmentAssignee{
    assignDepartmentAuthorized:boolean;
    deleteDepartmentAuthorized:boolean;
  }

  export interface IDepartmentAssignee extends IFunctionalRolesAssignee {
    departmentDto:Entities.DTODepartment;
    departmentAsFormattedString?:string;
  }

  export interface IFunctionalRolesAssignee
  {
    associatedFunctionalRoles:Users.DtoFunctionalRole[];
    functionalRolesAsFormattedString?:string;
  }

  export class DTOGroupDetails  {
    childrenGroups:DTOGroup[];
    siblingGroups:DTOGroup[];
    groupDto:DTOGroup;

  }
  export class DTONewJoinedGroupDto  {
    groupName:string;
    childrenGroupIds:string[];

  }
export class DtoRefinementRuleDto {
  id:number;
  subgroupId:string;
  rawAnalysisGroupId:string;
  subgroupName:string;
  active:boolean;
  priority:number;
  predicate: any
}
  export class DTOGroup implements IQuerybleEntity,ITaggable,IBizListAssociatable,IDocTypesAssignee, IFunctionalRolesAssignee, Users.IFunctionalItem {
    id:string;
    groupName:string;
    numOfFiles:number;
    firstMemberPath:string;
    firstMemberName:string;
    firstMemberId:number;
    numOfHitEntities:number;
    numOfHitFiles:number;
    fileTagDtos:Entities.DTOFileTag[];
    fileTagAssociations:DTOFileTagAssociation[];
    docTypeDtos:Entities.DTODocType[];
    associatedFunctionalRoles:Users.DtoFunctionalRole[];
    associatedBizListItems:DTOSimpleBizListItem[];
    groupType:EGroupType;
    parentGroupName:string;
    parentGroupId:string;
    rawAnalysisGroupId:string;
    hasSubgroups:boolean;
    groupProcessing:boolean;
    deleted:boolean;
    permittedOperations: Users.DtoSystemRole[];
    parentGroup: DTOGroup;


    static Empty():DTOGroup{
      var g = new DTOGroup();
      g.id='a';g.groupName='b';g.numOfFiles=-1;g.firstMemberName='c';g.firstMemberPath='d';g.numOfHitEntities=1;g.numOfHitFiles=2;g.firstMemberId=3;g.parentGroupName='e';
      g.fileTagDtos=[DTOFileTag.Empty()];g.fileTagAssociations=[DTOFileTagAssociation.Empty()];
      g.docTypeDtos=[DTODocType.Empty()]; g.associatedBizListItems=[DTOSimpleBizListItem.Empty()];
      return g;
    }
  }
  export enum  EGroupType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    ANALYSIS_GROUP= <any>"ANALYSIS_GROUP",
    USER_GROUP= <any>"USER_GROUP",
    SUB_GROUP= <any>"SUB_GROUP",
 }


  export class DTOMailInfo implements IQuerybleEntity {
    address: string;
    domain: string;
    mailAddressType: string;
    name: string;

    static Empty():DTOMailInfo{
      var g = new DTOMailInfo();
      g.address='b';g.domain='a';g.mailAddressType='c';g.name='d';
      return g;
    }
  }

  export class DTOFolderSharePermissionDto implements IQuerybleEntity {
    user:string;
    id:string;


    static Empty():DTOFolderSharePermissionDto{
      var g = new DTOFolderSharePermissionDto();
      g.user='a';
      g.id = 'b';
      return g;
    }
  }

  export class DTOFileUser implements IQuerybleEntity {
    user:string;


    static Empty():DTOFileUser{
      var g = new DTOFileUser();
       g.user='b';
      return g;
    }
  }

  export class DTOContent implements IQuerybleEntity {
    name:string;
    id:string;


    static Empty():DTOContent{
      var g = new DTOContent();
      g.name='a';
      g.id = 'b';
      return g;
    }
  }
  export class DTOExtension implements IQuerybleEntity {
    extension:string;


    static Empty():DTOExtension{
      var g = new DTOExtension();
      g.extension='b';
      return g;
    }
  }

  export class DTOOwner implements IQuerybleEntity {
    user:string;


    static Empty():DTOOwner{
      var g = new DTOOwner();
      g.user='b';
      return g;
    }
  }

  export class DTODepartment implements IQuerybleEntity {
    id:number;
    name:string;
    parentId:number;
    parentName:string;
    description:string;
    implicit:boolean;
    fullName:string;

    static Empty():DTODepartment {
      var g = new DTODepartment();
      g.id=111;g.name='b';g.parentId=222;g.parentName='c';g.description='d';g.implicit=true;g.fullName='e';
      return g;
    }
  }

  export class DTORegulation implements IQuerybleEntity {
    name:string;
    id: number;
    active:boolean;

    static Empty():DTORegulation{
      var g = new DTORegulation();
      g.name='b';
      g.id=0;
      return g;
    }
  }

  export enum  PatternType {
    REGEX= <any>"REGEX",
    PREDEFINED= <any>"PREDEFINED"
  }

  export class DTOTextSearchPattern implements IQuerybleEntity {
    name:string;
    id: number;
    pattern:string;
    validatorClass:string;
    patternType:PatternType;
    description:string;
    active:boolean;
    regulationIds:string[];
    categoryId:number;
    categoryName:string;
    subCategoryId:number;
    subCategoryName:string;

    static Empty():DTOTextSearchPattern{
      var g = new DTOTextSearchPattern();
      g.name='b';
      g.id=0;
      return g;
    }
  }



  export class DTOMetadata implements IQuerybleEntity {
    type:string;
    value: string;

    static Empty():DTOMetadata{
      var g = new DTOMetadata();
      g.type='b';
      g.value='c';
      return g;
    }
  }


  export class DTOLabel implements IQuerybleEntity {
    name:string;
    id: number;
    description:string;

    static Empty():DTOLabel{
      var g = new DTOLabel();
      g.name='b';
      g.id=0;
      return g;
    }
  }


  export class DTOSubtree implements IQuerybleEntity {
    id:string;
    type:string;
    name:string;
    docFolderId:string;
   folderName:string;
     folderPath:string;

    static Empty():DTOSubtree{
      var g = new DTOSubtree();
      g.id='b';g.type='a';g.name='c';g.docFolderId='d';g.folderName='e';g.folderPath='f';
      return g;
    }
  }

  export class DTOFileCategory implements IQuerybleEntity {
    fileCategoryId:string;
    fileCategoryName:string;
    fileCategoryType:EFileCategoryType;
    numOfFiles:number;
    firstMemberPath:string;
    firstMemberName:string;

    static Empty():DTOFileCategory{
      var g = new DTOFileCategory();
      g.fileCategoryType=EFileCategoryType.businessID;g.fileCategoryId='b';g.fileCategoryName='e';
      return g;
    }
  }


  export class DTOFileInfo implements IQuerybleEntity,ITaggable,IBizListAssociatable,IDocTypesAssignee,IDepartmentAssignee,IBizListExtractable,IFunctionalRolesAssignee , Users.IFunctionalItem {
    id:number;
    fileName:string;
    mailContainerId:number;
    mailboxGroup:string;
    madiaType:EMediaType;
    type:Dashboards.EFileType;
    itemType:Dashboards.EFileItemType;
    itemSubject:string;
    baseName:string;
    fsFileSize:number;
    numOfAttachments: number;
    fsLastModified:number;
    fsLastAccess:number;
    creationDate:number;
    fsLastAccessAsLong:number;
    fileTagDtos:Entities.DTOFileTag[];
    associatedFunctionalRoles:Users.DtoFunctionalRole[];
    fileTagAssociations:DTOFileTagAssociation[];
    docTypeDtos:Entities.DTODocType[];
    departmentDto:Entities.DTODepartment;
    recipients:DTOMailInfo[];
    sender:DTOMailInfo;
    sentDate:string;
    extension:Entities.EFileExtension;
    groupName:string;
    groupId:string;
    owner:string;
    contentMetadataDto:DtoContentMetadata;
    associatedBizListItems:DTOSimpleBizListItem[];
    bizListExtractionSummary:DTOExtractionSummary[];
    contentId:number;
    analyzeHint:EAnalyzeHint;
    permittedOperations: Users.DtoSystemRole[];
    searchPatternsCounting: DtoSearchPatternCounting[];
    daLabelDtos: DTOLabel[];

    static Empty():DTOFileInfo{
      var g = new DTOFileInfo();
      g.id=111;g.fileName='b';(<any>g).type='c';g.baseName='d';g.fsFileSize=222;g.fsLastModified=333;g.creationDate=4444;g.groupName ='e';g.groupId ='f';(<any>g).contentMetadataDto='g';
      (<any>g).docTypeDtos='h';(<any>g).recipientsAddresses='ra';(<any>g).senderAddress='sa';(<any>g).associatedBizListItems='i';(<any>g).bizListExtractionSummary='j';
      g.owner='k';(<any>g).analyzeHint='m';g.fsLastAccess=55;g.itemSubject ='n';(<any>g).madiaType ='o';g.mailboxGroup = 'p';(<any>g).departmentDto='q';
      return g;
    }
  }

  export class DtoSearchPatternCounting {
    patternId:number;
    patternName:string;
    patternCount:number;
    matches:number;
  }

  export class DTOWorkflowFileInfo extends DTOFileInfo{
    workflowFileData:DTOWorkflowFileData;
  }
  export class DTOWorkflowFileData {
    workflowId:number;
    fileAction: EFileAction;
    fileState: EFileState;
  }
  export enum EFileState {
    NONE= <any>"NONE",
    ACTION_PENDING_APPROVAL= <any>"ACTION_PENDING_APPROVAL",
    APPROVED= <any>"APPROVED",
    DISAPPROVED= <any>"DISAPPROVED",
  }
  export enum EFileAction {
    NEW= <any>"NEW",
    DO_NOTHING= <any>"DO_NOTHING",
    ARCHIVE= <any>"ARCHIVE",
    DELETE= <any>"DELETE",
    IRRELEVANT= <any>"IRRELEVANT",
  }

  export enum EAnalyzeHint{  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    NORMAL= <any>"NORMAL",
    EXCLUDED= <any>"EXCLUDED",
    MANUAL= <any>"MANUAL",

  }

  export enum EFolderType {
    SHARE= <any>"SHARE",
    ZIP= <any>"ZIP",
    EMAIL= <any>"EMAIL",
    PST= <any>"PST",
  }

  export class DtoContentMetadata  {
    contentId:number;
    fileCount:number;
    static Empty():DtoContentMetadata{
      var g = new DtoContentMetadata();
      g.contentId=1;g.fileCount=2;
      return g;
    }
  }
  export class DToAclData implements IQuerybleEntity {
    aclDeniedReadData:string[];
    aclDeniedWriteData:string[];
    aclReadData:string[];
    aclWriteData:string[];
    fileId:number;
  }

  export class DTOFileDetails implements IQuerybleEntity {
    fileDto:DTOFileInfo;
    aclDataDto:DToAclData;
    fileRootFolder:DtoRootFolder;
    fileOwner:string;
    fileProcessingState:string;
    ingestionError:Dashboards.DtoProcessingError;
    proposedTitles:string[];
    subject:string;
    title:string;
    author:string;
    template:string;

    static fileProcessingState_ANALYSED= 'ANALYSED';
    static fileProcessingState_INGESTED= 'INGESTED';

  }

  export class DtoFileHighlights {
    snippets:string[];
    locations:string[][];
    fileContentLastModifiedDate:number;
  }

  export class DTOFacetedSearchField implements IQuerybleEntity {
    fieldName:string;
    numberOfItems:number;
    range:boolean;
    values:DTOFacetedSearchFieldValue[];

  }
  export class DTOFacetedSearchFieldValue {
    value:string;
    numberOfItems:number;
  }

  export class DTOFolderInfo implements IQuerybleEntity,ITaggable,IBizListAssociatable,IDocTypesAssignee ,IDepartmentAssignee,IFunctionalRolesAssignee, Users.IFunctionalItem{
    id:number;
    name:string;
    mediaItemId:string;
    mailboxGroup:string;
    mediaType:EMediaType;
    path:string;
    realPath:string;
    depthFromRoot:number;
    parentFolderId:number;
    numOfSubFolders:number;
    numOfDirectFiles:number;
    numOfAllFiles:number;
    folderType: EFolderType;
    fileTagDtos:Entities.DTOFileTag[];
    fileTagAssociations:DTOFileTagAssociation[];
    docTypeDtos:Entities.DTODocType[];
    departmentDto:Entities.DTODepartment;
    associatedFunctionalRoles:Users.DtoFunctionalRole[];
    associatedBizListItems:Entities.DTOSimpleBizListItem[];
    rootFolderId:number;
    rootFolderLength:number;
    rootFolderNickName:string;
    realPathNoPrefix:string;
    permittedOperations: Users.DtoSystemRole[];
    rootFolderSharePermissions: DtoRootFolderSharePermission[];

    static Empty():DTOFolderInfo{
      var g = new DTOFolderInfo();
      g.id=111;g.name='b'; g.path='c';g.mediaItemId='d'; g.depthFromRoot=111;g.parentFolderId=222;g.numOfSubFolders=333;g.numOfDirectFiles=444;g.numOfAllFiles=555;(<any>g).fileTagDtos='e';
      (<any>g).docTypeDtos='f';(<any>g).associatedBizListItems='g';g.realPath='h';g.realPathNoPrefix='i';(<any>g).departmentDto='j';
      return g;
    }
  }

  export class DTOFileTagType implements IQuerybleEntity {
    id:number;
    name:string;
    system:boolean;
    hidden:boolean;
    tagItemsCount:number;
    description:string;
    sensitive:boolean;
    singleValueTag:boolean;

    static Empty():DTOFileTagType{
      var g = new DTOFileTagType();
      g.id=111;g.name='b';g.description='c';(<any>g).sensitive='d';(<any>g).system='e';(<any>g).hidden='f',(<any>g).singleValueTag='g';
      return g;
    }
  }

  export class DTODateRangePartition  implements IQuerybleEntity {
    id:number;
    name:string;
    description:string;
    dateRangeItemDtos:DTODateRangeItem[];
    continuousRanges:boolean;

    static Empty():DTODateRangePartition{
      var g = new DTODateRangePartition();
      g.id=111;g.name='b';g.description='c';(<any>g).dateRangeItemDtos=4;
      return g;
    }
  }


  export class DTODateRangeItem  implements IQuerybleEntity {
    id:number;
    name:string;
    start:DTODateRangePoint;
    end:DTODateRangePoint;
    totalCount:number;

        static Empty():DTODateRangeItem{
      var g = new DTODateRangeItem();
      g.id=111;g.name='b';(<any>g).start=4;(<any>g).end=5;g.totalCount=6;
      return g;
    }
  }

  export class DTODateRangePoint  implements IQuerybleEntity {
    id:number;
    absoluteTime:number;
    relativePeriod:ETimePeriod ;
    relativePeriodAmount:number;
    type:EDateRangePointType;

    static Empty():DTODateRangePoint{
      var g = new DTODateRangePoint();
      g.id=111;g.absoluteTime=2;g.relativePeriod=ETimePeriod.DAYS;g.relativePeriodAmount=3;g.type=EDateRangePointType.ABSOLUTE;
      return g;
    }

    static StartOfTime():DTODateRangePoint{
      var g = new DTODateRangePoint();
           g.relativePeriod=ETimePeriod.YEARS;g.relativePeriodAmount=99; g.type = EDateRangePointType.RELATIVE;
      return g;
    }

    static NowAbsolute():DTODateRangePoint{
      var g = new DTODateRangePoint();
      g.type=EDateRangePointType.ABSOLUTE;g.absoluteTime=Date.now();
      return g;
    }

    static IsStartOfTime(g:DTODateRangePoint):boolean{
      return g.relativePeriod==ETimePeriod.YEARS&&g.relativePeriodAmount==99;

    }
  }

  export enum  ETimePeriod {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    MINUTES= <any>"MINUTES",
    HOURS= <any>"HOURS",
    DAYS= <any>"DAYS",
    WEEKS= <any>"WEEKS",
    MONTHS= <any>"MONTHS",
    YEARS= <any>"YEARS",
  }

  export enum  EDateRangePointType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
    ABSOLUTE= <any>"ABSOLUTE",
    RELATIVE= <any>"RELATIVE",
    EVER= <any>"EVER",
    UNAVAILABLE= <any>"UNAVAILABLE"
  }

  export enum  EFileExtension {
    EML= <any>"eml",
  }

  export class DTODocType implements IQuerybleEntity {
    id:number;
    name:string;
    fileTagDtos:Entities.DTOFileTag[];
    uniqueName:string;
    parentId:number;
    parentUniqueName:string;  // Not recieved from server
    description:string;
    parents:number[];
    implicit:boolean;
    singleValue:boolean;
    fileTagTypeSettings:DTOFileTagTypeAndTags[];
    fullName:string;

    equals = function (o) {
      return this.id === o.id;
    };

    static Empty():DTODocType{
      var g = new DTODocType();
      g.id=111;g.name='b';g.uniqueName='c';g.parentId=222;g.description='d';g.parents=[2];g.implicit=true;g.parentUniqueName='e';(<any>g).singleValue='f';(<any>g).fileTagDtos='g';(<any>g).fileTagTypeSettings='h';(<any>g).fullName='i';
      return g;
    }
  }


  export class DTOFileTagTypeAndTags  implements IQuerybleEntity {
    fileTagTypeDto:DTOFileTagType ;
    fileTags:DTOFileTag[];

  }

  export class DTOFileTag implements IQuerybleEntity {
    id:number;
    name:string;
    type:DTOFileTagType;
    description:string;
    implicit:boolean;

    equals = function (o) {
      return this.id === o.id;
    };

    static Empty():DTOFileTag{
      var g = new DTOFileTag();
      g.id=111;g.name='b';(<any>g).type='c'; g.id=333;g.description='e';g.implicit=true;;
      return g;
    }
  }
  export class DTOFileTagAssociation implements IQuerybleEntity {
    associationTimeStamp:number;
    fileTagDto:DTOFileTag;
    fileTagSource:string;


    equals = function (o) {
      return this.id === o.id;
    };

    static Empty():DTOFileTagAssociation{
      var g = new DTOFileTagAssociation();
      g.associationTimeStamp=111;g.fileTagDto=DTOFileTag.Empty(); g.fileTagSource='cc';
      return g;
    }

    static fileTagSource_BUSINESS_ID = 'BUSINESS_ID';
    static fileTagSource_FOLDER_IMPLICIT="FOLDER_IMPLICIT";
    static fileTagSource_FOLDER_EXPLICIT="FOLDER_EXPLICIT";
    static fileTagSource_FILE="FILE";
  }

  export class DTODocTypeAssociation implements IQuerybleEntity {
    associationTimeStamp:number;
    docTypeDto:DTODocType;
    docTypeSource:string;


    equals = function (o) {
      return this.id === o.id;
    };

    static Empty():DTODocTypeAssociation{
      var g = new DTODocTypeAssociation();
      g.associationTimeStamp=111;g.docTypeDto=DTODocType.Empty(); g.docTypeSource='cc';
      return g;
    }

  }

  export class UserViewDataDto  {
    id:number;
    name:string;
    displayData:string;
    filter: SavedFilterDto;
    displayType:EFilterDisplayType;
    globalFilter:boolean;
    owner: DTOUser;
    scope: string;
  }


  export class SavedFilterDto  {
    id:number;
    name:string;
    rawFilter:string;
    filterDescriptor:ui.FilterDescriptor;
    globalFilter:boolean;
    scope: string;
  }

  export enum  EFilterDisplayType {
    CHART= <any>"CHART",
    DISCOVER= <any>"DISCOVER",
    DISCOVER_VIEW= <any>"DISCOVER_VIEW"
  }


  export class DTOBizListTemplateType implements IQuerybleEntity {

    name:string;
    description:string;

    static Empty():DTOBizListTemplateType{
      var g = new DTOBizListTemplateType();
      g.name='a';
      g.description='b';
      return g;
    }

  }

  export class DTOBizList implements IQuerybleEntity {
    id:number;
    name:string;
    description:string;
    bizListItemType:string ;
    template:DTOBizListTemplateType;
    lastSuccessfulRunDate:number;
    creationTimeStamp:number;
    lastUpdateTimeStamp:number;
    active:boolean;

    static Empty():DTOBizList{
      var g = new DTOBizList();
      g.name='a';
      g.id=2;
      g.lastSuccessfulRunDate=3;
      g.creationTimeStamp=4;
      g.lastUpdateTimeStamp=5;
      g.description='b';
      g.bizListItemType = DTOBizList.itemType_SIMPLE;
      (<any>g).active = 'c';
      return g;
    }

    static itemType_CONSUMERS = 'CONSUMERS';
    static itemType_CLIENTS="CLIENTS";
    static itemType_EMPLOYEES="EMPLOYEES";
    static itemType_PROJECTS_SERVICES="PROJECTS_SERVICES";
    static itemType_SIMPLE="SIMPLE";
  }

  export class DTOBizListSummaryInfo {
    bizListDto: DTOBizList;
    crawlRunDetailsDto: Operations.DtoCrawlRunDetails;
    itemsCount: number;
    activeItemsCount: number;
  }

  export class DtoClassificationProgress  {
     runStatus:Operations.ERunStatus
  }


  export class DTOExtractionSummary  {

    id:string;
    bizListItemType:string;
    bizListId:string;
    bizListExtractionDisplayName:string;
    sampleExtractions:DTOSimpleBizListItem[];
    count: number;

    static Empty():DTOExtractionSummary{
      var g = new DTOExtractionSummary();
      g.bizListItemType='a';
      g.id='c';
      g.bizListId='d';
      g.sampleExtractions=[DTOSimpleBizListItem.Empty()];
      g.count =3;
      return g;
    }

    static itemType_CONSUMERS = 'CONSUMERS';
    static itemType_CLIENTS="CLIENTS";
    static itemType_EMPLOYEES="EMPLOYEES";
    static itemType_PROJECTS_SERVICES="PROJECTS_SERVICES";
    static itemType_SIMPLE="SIMPLE";
  }
  export class DTOSimpleBizListItem implements IQuerybleEntity{

    id:string;
    businessId:string;
    name:string;
    bizListId: number;
    entityAliases:any[];
    state: string;
    implicit:boolean;
    type:string;
    aggregated:boolean;
    bizListExtractionDisplayName:string;


    static Empty():DTOSimpleBizListItem{
      var g = new DTOSimpleBizListItem();
      g.name='a';
      g.id='c';
      g.businessId='b';
      g.bizListId =3;
      (<any> g).entityAliases ='e'
      g.state = DTOSimpleBizListItem.state_ACTIVE;
      g.implicit = true;
      g.type = 'f';
      return g;
    }
    static state_ACTIVE = 'ACTIVE';
    static state_DELETED="DELETED";
    static state_ARCHIVED="ARCHIVED";
    static state_PENDING="PENDING";
  }
  export class DTOBizListItemAlias {
    name:string;
    id:number;
  }
  export class DTOBizListImportResult {

    resultDescription:string;
    importId:string;
    importedRowCount:number;
    duplicateRowCount:number;
    errorsRowCount:number;
  }

  export class DTOFileTypeCategory implements IQuerybleEntity {
    name:string;
    id:number;


    static Empty():DTOFileTypeCategory{
      var g = new DTOFileTypeCategory();
      g.name='a';
      g.id = 1;
      return g;
    }
  }


  export enum  FileSizeUnits {
    BYTE = 0,
    KB = 1,
    MB = 2,
    GB = 3
  }

  export class DTOFileSizePartition implements IQuerybleEntity {
    id : number;
    fromType : FileSizeUnits;
    toType : FileSizeUnits;
    fromValue : number;
    toValue : number;
    label:string;

    static Empty():DTOFileSizePartition{
      var g = new DTOFileSizePartition();
      g.label='a';
      g.id=1;
      g.toValue =2;
      return g;
    }
  }

}


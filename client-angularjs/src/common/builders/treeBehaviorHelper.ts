///<reference path='../../common/all.d.ts'/>
'use strict';



class TreeBehaviorHelper {

  static createTree<T> (scope, log:ILog,configuration,eventCommunicator:IEventCommunicator,  getDataUrlParams, fields:ui.IFieldDisplayProperties[],
                        treeItemTemplate,dataManipulationFn, hasChildren, getDataUrlBuilder?:(restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig,includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState)=>string) {

    log.debug('Init createGrid');

    if(!getDataUrlBuilder)
    {
      scope.getDataUrlBuilder = GridBehaviorHelper.getDataUrlBuilder;
    }
    else {
      scope.getDataUrlBuilder = getDataUrlBuilder;
    }
    if(scope.$parent && scope.$parent.$parent && scope.$parent.$parent.workflowId)
    {
      let origUrlBuilderFn =  scope.getDataUrlBuilder;
      scope.getDataUrlBuilder = function(...args) {
        let url = origUrlBuilderFn(...args);
        url += ((url.indexOf('?') === -1) ? '?' : '&') + "workflowId="+ scope.$parent.$parent.workflowId;;
        return url;
      };
    }
    var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName,scope.restrictedEntityId,scope.entityDisplayType, configuration,scope.includeSubEntityData,scope.fullyContainedFilterState);
    if(scope.restrictedEntityDisplayTypeName && scope.restrictedEntityId) {
      scope.lazyFirstLoad = false; //force load if restricted id was already set.
    }

    scope.contextName = scope.entityDisplayType && scope.entityDisplayType instanceof EntityDisplayTypes? EFilterContextNames.convertEntityDisplayType(scope.entityDisplayType):null;

    scope.onDataCompleteFn = function(pageSize,pageSizeReduced)
    {

    };
    var manipulateReadRequest = function(params)
    {
      if(scope.findId)
      {
        params['selectedItemId']=scope.findId;
        log.debug('Added selectedItemId to request: '+scope.findId);
        scope.findId=null;
      }
      return params;
    }
   var onErrorReadResponse = function(status)
    {
      if(status == 400)
      {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxSetState, {
          action: 'userSearchInputError',

        });
      }
    }
    var onExpandNode = function(nodeElement)
    {
      scope.expanding = true;
    }





    scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers(scope);
    });

    scope.handleMultiSelectionChangeFn = function() {
      var data=[];
      var element = scope.element;
      if(scope.selectedItem) //consider only first selected item
      {
        data.push(scope.selectedItem);
      }
      if(element ) {
        var multiselected = elementOptions.getMultiSelectedItems(scope.element);
        data = data.concat(multiselected);
      }
      scope.selectedItems=data;
      if(scope.itemsSelected) {
        scope.itemsSelected(scope.selectedItems);
      }
      scope.showSelectedRowsPath();


    };


    scope.showSelectedRowsPath = function()
    {
      function getParentRow(row)
      {
        try {
          var treeview = treeElement.data("kendoTreeView");
          var parent = treeview.parent(row);
          return parent && parent.find('.k-in')[0];
        }
        catch(e)//kendo excption Cannot read property 'className' of undefined
        {
          log.debug(e);
          return null;
        }

      }
      var treeElement = scope.element;
      if(treeElement) {
        treeElement.find('.k-in').removeClass("k-state-selected-parent");
        var treeview = treeElement.data("kendoTreeView");
        if(treeview && treeview.select().length>0) {
          var parentRow = getParentRow(treeview.select());
          while (parentRow) {
            $(parentRow).addClass("k-state-selected-parent");
            parentRow = getParentRow($(parentRow));
          }
        }


      }
    };

    var treeBuilder: ui.ITreeOptionsBuilder<T> = new KendoTreeOptionsBuilder<T>();
    var elementOptions  = treeBuilder.createTree<T>( scope.elementName,url,
        getDataUrlParams,fields,   configuration.max_data_records_per_request, hasChildren,dataManipulationFn,!scope.lazyFirstLoad ? true : <boolean>!scope.lazyFirstLoad,treeItemTemplate,
        scope.filterData? scope.filterData.filter:null,manipulateReadRequest,scope.handleMultiSelectionChangeFn,onErrorReadResponse,onExpandNode);

    scope.mainTreeOptions = elementOptions.treeSettings;
    return elementOptions;

  }

  static connect(scope,timeout,$window,log:ILog,configuration:IConfig,elementOptions:ui.ITreeHelper,onSelectedItemChanged,element,doNotExpandAndSelectFirstByDefault?:boolean,marginBottom?:number) {
    scope.dateTime = Date.now();
    scope.element = element;



    resizeTree(false);

    var isReadyForReload = function() {
      var element = scope.element;
      var requireRestrictedEntityId = (!scope.restrictedEntityDisplayTypeName ||  (scope.restrictedEntityId&&scope.restrictedEntityDisplayTypeName));
      var requireEntityTypeId = (!scope.requireEntityTypeId ||  (scope.entityTypeId&&scope.requireEntityTypeId));
      return element &&  requireRestrictedEntityId && requireEntityTypeId;
    }

    scope.$watch(function () {   return scope.restrictedEntityId; }, function (value) {

      var element = scope.element;
      if (isReadyForReload()) {
        var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState);
        if(url) {
          elementOptions.refresh(element, url, null);
        }
      }

    });

    scope.$watch(function () { return scope.entityTypeId; }, function (value) {
      var element = scope.element;
      if(isReadyForReload()) {
        var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState);
        if(url) {
          elementOptions.refresh(element, url, null);
        }
      }
    });

    scope.getCurrentUrl = function () {
      var tree = scope.element;
      if (tree) {
        return elementOptions.getCurrentUrl(tree);
      }
      return null;
    };

    scope.getCurrentFilter = function ():ui.IDisplayElementFilterData {
      return scope.filterData;
    };

    scope.$watch(function () { return scope.filterData; }, function (value,oldValue) {
      var element = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(element ) {

        log.debug(' filter was requested: ' + JSON.stringify(scope.filterData));
        elementOptions.filterKendoFormat(element, scope.filterData?(<ui.IDisplayElementFilterData>scope.filterData).filter:null);

      }
      else if(!element&& scope.filterData) { //filter was set befor the grid was created
        log.debug(' filter was requested: Waiting for tree to initialized');
      }
    });

    scope.reloadDataToTree = function (itemId,dataItem) {
      //var element =$element;
      //if(element[0]) {
      //  if(dataItem&&dataItem.item) {
      //    pathToExpandAndSelect = dataItem.item.path;
      //    log.debug('tree reloads data upon item path: ' + pathToExpandAndSelect);
      //  }
      //  else {
      //    pathToExpandAndSelect = $scope.foldersSelectedItem? $scope.foldersSelectedItem.item.path:null;
      //  }
      //  _this.elementOptions.refresh(element,null,null);
      //}
    };

    scope.$watch(function () { return scope.includeSubEntityData; }, function (value,oldValue) {
      var element =scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(element) {
        var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration,scope.includeSubEntityData,scope.fullyContainedFilterState);
        elementOptions.refresh(element, url, null);
      }
    });
    var firstDataBound=true;

    scope.onDataBound = function(e)
    {
      var  scrollIntoView = false; //Inorder not to scroll to seleceted node when user expand node manually
      if(scope.changeSelectedItem&&scope.changeSelectedItem.trim()!='') //first load when reading changeSelectedItem =>  rowIdsToSelect =null
      {
     //   pathToExpandAndSelect = scope.changeSelectedItem;
        scope.selectedItem=null;
        var pathToExpandFound = scope.findByPath(scope.changeSelectedItem);
        if(expandedAndSelected /*pathToExpandFound && pathToExpandFound.exactMatch*/) {
          scope.changeSelectedItem = null;
        }

      }

      if(! scope.expanding && !pathToExpandAndSelect && scope.selectedItem) //is dataBound is triggered due to user expand node, do not look for the selected item
      {
        pathToExpandAndSelect =scope.getFindByPathValue ? scope.getFindByPathValue( scope.selectedItem):null;
      }
      var pathToExpandFound;
      if(pathToExpandAndSelect) {
        scope.selectedItem=null;
        pathToExpandFound = scope.findByPath(pathToExpandAndSelect);
        scrollIntoView=true;

      }
      else if(!expandedAndSelected&& firstDataBound  && !scope.restrictedEntityId && !doNotExpandAndSelectFirstByDefault)
      {
        firstDataBound = false;
        selectFirstNode();
       // timeout(expandFirstNode);
      }
      else if (scope.selectedItem == null && !doNotExpandAndSelectFirstByDefault) {
        selectFirstNode();
      }

      scope.expanding = false;
      resizeTree(scrollIntoView);
      scope.element.on('click','.dropdown-toggle',scope.selectItemIfNeeded);
      hookHoverForRowIcon();
      scope.showSelectedRowsPath();
      scope.initialized=true;
      if(scope.localDataBound) {
        scope.localDataBound();
      }
    };

    var pathToExpandAndSelect=null;
    var expandedAndSelected;
    scope.findByPath = function(pathToFind:string)
    {

      var element =scope.element;
      if(element&&pathToFind && pathToFind.trim()!='') {
        pathToFind = pathToFind.trim().toLowerCase();
        log.debug(' path was requested: ' + pathToFind);
        let isFilePath =  pathToFind.lastIndexOf('.')>-1 && (pathToFind.lastIndexOf('.') + 5 >= pathToFind.length);
        if(isFilePath && !_.endsWith(pathToFind,'.eml') && !_.endsWith(pathToFind,'*')){ //pst* zip*
          pathToFind = pathToFind.substring(0,pathToFind.lastIndexOf(getPathDelimiter(pathToFind)));
        }
        if(pathToFind[pathToFind.length-1]!='/' && pathToFind[pathToFind.length-1]!='\\')  //path to find need to end with delimeter, otherwise \pst1 will be caught as \pst
        {
          pathToFind+=getPathDelimiter(pathToFind);
        }

        var shouldQueueItemToDataBind =  elementOptions.expandPath(element,pathToFind,isInPathFun,!doNotExpandAndSelectFirstByDefault);
        if(shouldQueueItemToDataBind && !shouldQueueItemToDataBind.exactMatch)
        {
          pathToExpandAndSelect = pathToFind;
        }
        else
        {
          pathToExpandAndSelect = null;
          expandedAndSelected = true;
          var scrollIntoView = true;
          resizeTree(scrollIntoView);
        }

      }
      function getPathDelimiter(path){
        if(path.indexOf('\\')>=0)
        {
         return '\\';
        }
        return '/';
      }
      function isInPathFun (dataItem,pathInput:string):any
      {

        var pathStr = scope.getFindByPathValue ? scope.getFindByPathValue(dataItem): dataItem.fullName.toLowerCase(); //create copy of path str
        if(pathStr[pathStr.length-1]!='/' && pathStr[pathStr.length-1]!='\\')
        {
          pathStr+= getPathDelimiter(pathStr);
        }
        var inPath = pathInput.indexOf(pathStr) == 0;//starts with
        if(!inPath)
        {
          return false;
        }
        return {inPath:true,exactMatch:pathInput==pathStr}

      }

    };
    var hookHoverForRowIcon = function()
    {
      var treeElement = scope.element;
      if(treeElement) {
        var gridCell = treeElement.find('.k-in');
        gridCell.hover(
            function () {
              $(this).find('i').removeClass("ng-hide");
            },
            function () {
              $(this).find('i').addClass("no-hide");
            }
        );
        gridCell.hover(
          function () {
            $(this).find('i').removeClass("no-visible");
          },
          function () {
            $(this).find('i').addClass("no-visible");
          }
        );
      }
    };

    function resizeTree(scrollIntoView:boolean) {

      timeout(function() {
        var treeElement = scope.element;
        if(treeElement) {
          elementOptions.resizeTree(treeElement,marginBottom);
          if (scrollIntoView) {
            elementOptions.scrollSelectedNodeIntoView(treeElement);
          }
        }
      });
    }
    var w = angular.element($window);
    w.bind('resize', function () {
      scope.$apply(function(){resizeTree(true)});
    });

    scope.selectItemIfNeeded=function(e)
    {
     var target = e.toElement || e.relatedTarget || e.target; //support Chrome/FF/IE
      if(!target)
      {
        return;
      }
      var theClickedRow = $(target).closest('.k-in');
      // if(e.ctrlKey || e.shiftKey || !(theClickedRow.hasClass('k-state-selected') || theClickedRow.hasClass('multi-state-selected')))
      if(!(theClickedRow.hasClass('k-state-selected') || theClickedRow.hasClass('multi-state-selected')))
      {
        theClickedRow.click();
      }
      // Indicate to the tree selection handler that a menu is about to open and default action should be ignored
      theClickedRow.addClass('da-menu-open');
    }

    scope.handleSelectionChange = function(data, dataItem, columns) {
      if(data) //consider only first selected item
      {
        scope.selectedItem = data;
        onSelectedItemChanged();
      }
      else
      {
        scope.selectedItem=null;
        if( scope.itemSelected) {
          scope.itemSelected(null, null, null);
        }
      }

      scope.handleMultiSelectionChangeFn();
    };

    scope.refreshData=function(itemId)
    {
      var element = scope.element;
      if(element) {
        log.debug('tree refresh data');
        elementOptions.reloadData(element);

      }
    };

    var selectFirstNode=function()
    {
      var tree = scope.element;
      if(tree)
      {
        elementOptions.selectFirstNode(tree);
      }
    };
    var expandFirstNode=function()
    {
      var tree = scope.element;
      if(tree)
      {
        elementOptions.expandFirstNode(tree);
      }
    };
  }

}

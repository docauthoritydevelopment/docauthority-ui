'use strict';

//--------------------------------------------------------------------------------------
// inspired by: https://github.com/Intellipharm/angular-tooltips
//--------------------------------------------------------------------------------------

var TooltipBuilder = (function () {

  var init = function () {

    let selector = `
    a[title], 
    a[tooltip], 
    a[bubble],
    span[title], 
    span[tooltip], 
    span[bubble],
    div[title],
    div[tooltip],
    div[bubble], 
    button[title],
    button[tooltip],
    button[bubble]`;

    $(document).on("mouseenter", selector, (e) =>{
      removeTooltip();
      showTooltip(e);
    });
  };

  //-------------------------------------------

  var showTooltip = function (e) {

    var elm = $(e.target);
    var title = elm.attr("title") || elm.attr("tooltip") || elm.attr("bubble");

    if(!title) {
      elm = elm.closest("[title], [tooltip], [bubble]");
      title = elm.attr("title") || elm.attr("tooltip") || elm.attr("bubble");
    }
    if (title) {
      createTooltip(e, elm, title);

      elm.removeAttr('ng-attr-title');
      elm.removeAttr('title').removeAttr('tooltip');
      elm.attr({'bubble' : title});

      elm.on("mouseleave click", () =>{
        removeTooltip();
      });
    }
  };

  //-------------------------------------------

  var removeTooltip = function(){
    if(createTooltip) {
      createTooltip.cancel();
    }
    $(document).find('body').find('.da-tooltip').remove();
  };

  //-------------------------------------------

  var createTooltip = _.debounce(function (e, elm, title) {

    var tooltipElement = $('<div>').addClass('da-tooltip da-tooltip-fade-in');

    $(document).find('body').append(tooltipElement);
    tooltipElement.html(title);

    var pos = calculatePosition(e, elm, tooltipElement, getDirection(elm));
    tooltipElement.addClass('da-tooltip-' + pos.dir).css({
      top: pos.top,
      left: pos.left
    });
  },400);

  //----------------------------------------------

  var getDirection = function (elm) {
    return elm.attr('tooltip-direction') || elm.attr('title-direction') || 'top';
  };

  //----------------------------------------------

  var calculatePosition = function (e, elm, tooltip, direction) {

    var tooltipBounding = tooltip[0].getBoundingClientRect();
    var elBounding = elm[0].getBoundingClientRect();
    var scrollLeft = window.scrollX || document.documentElement.scrollLeft;
    var scrollTop = window.scrollY || document.documentElement.scrollTop;
    var arrow_padding = 12;
    var pos = {left: 0, top: 0};
    var newDirection = null;

    // calculate the left position
    if (stringStartsWith(direction, 'left')) {
      pos.left = elBounding.left - tooltipBounding.width - (arrow_padding / 2) + scrollLeft;
    } else if (stringStartsWith(direction, 'right')) {
      pos.left = elBounding.left + elBounding.width + (arrow_padding / 2) + scrollLeft;
    } else if (stringContains(direction, 'left')) {
      pos.left = elBounding.left - tooltipBounding.width + arrow_padding + scrollLeft;
    } else if (stringContains(direction, 'right')) {
      pos.left = elBounding.left + elBounding.width - arrow_padding + scrollLeft;
    } else {
      pos.left = elBounding.left + (elBounding.width / 2) - (tooltipBounding.width / 2) + scrollLeft;
    }

    // calculate the top position
    if (stringStartsWith(direction, 'top')) {
      pos.top = elBounding.top - tooltipBounding.height - (arrow_padding / 2) + scrollTop;
    } else if (stringStartsWith(direction, 'bottom')) {
      pos.top = elBounding.top + elBounding.height + (arrow_padding / 2) + scrollTop;
    } else if (stringContains(direction, 'top')) {
      pos.top = elBounding.top - tooltipBounding.height + arrow_padding + scrollTop;
    } else if (stringContains(direction, 'bottom')) {
      pos.top = elBounding.top + elBounding.height - arrow_padding + scrollTop;
    } else {
      pos.top = elBounding.top + (elBounding.height / 2) - (tooltipBounding.height / 2) + scrollTop;
    }

    if (pos.left < scrollLeft) {
      newDirection = direction.replace('left', 'right');
    } else if ((pos.left + tooltipBounding.width) > (window.innerWidth + scrollLeft)) {
      newDirection = direction.replace('right', 'left');
    }

    if (pos.top < scrollTop) {
      newDirection = direction.replace('top', 'bottom');
    } else if ((pos.top + tooltipBounding.height) > (window.innerHeight + scrollTop)) {
      newDirection = direction.replace('bottom', 'top');
    }

    if (newDirection) {
      //return calculatePosition(e, elm, tooltip, newDirection);
    }

    return {
      top: pos.top,
      left: pos.left,
      dir: direction
    };
  };

  //----------------------------------------------

  var stringStartsWith = function (searchString, findString) {
    return searchString.substr(0, findString.length) === findString;
  };

  //----------------------------------------------

  var stringContains = function (searchString, findString) {
    return searchString.indexOf(findString) !== -1;
  };

  init();
}());

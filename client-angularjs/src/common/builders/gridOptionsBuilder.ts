///<reference path='../all.d.ts'/>


'use strict';
declare var autosize:any;

class KendoGridOptionsBuilder<T> implements  ui.IGridOptionsBuilder<T>
{

     createGrid<T>(url:string,urlParams,
                                  fields:ui.IFieldDisplayProperties[] ,
                                  maxRecordsPerRequest:number,
                                  dataManipulationFn:(data: T[])=> T[],
                                  totalAggregatedCountFn:(totalAggregatedCount,pageNumber:number,data: T[])=> void,
                                   autoBind: boolean,
                                  rowTemplate?:string,updateUrl?:string,filterData?:any,
                   onDataCompleteFn?:(pageSize:number,pageSizeReduced:boolean,requestUrl?:string)=>void,
                   manipulateReadRequestFn?:(queryParams: any)=>void,onErrorReadResponseFn?:(status,url,error: any)=>void,totalElementsIsReturned?:boolean,onExcelPreparationsFn?:(workbook: any)=>void)
      {

        var lastSelectKendoTr:any  = null;
        var options:KendoGridViewOptions = new KendoGridViewOptions();

        options.gridDisplaySettings = <kendo.ui.GridOptions> {

          autoBind: autoBind,
          columnMenu: true,
          sortable: false,
          pageable: <kendo.ui.GridPageable>{
            numeric:false,  //buttons for with page numbers
            pageSizes:false,
            buttonCount: 5,
            pageSize:30,
            input:true,
            info:true,
            refresh: true,
            messages: {
              display: "Showing {0}-{1} <span class='of'>from {2}</span> ",
              of: "<span class='of'>/ {0}</span>"
            }
          },
         //  detailTemplate: '<div><h2>File details</h2><p>File name</p></div>',
       //   detailInit: detailInit,
          reorderable: true,
          resizable: true,
          columnResize: function(e) {

          },
           filterable: {
            extra: false,
            operators: {
              string: {
                contains:"Contains",
                startswith: "Starts with",
                eq: "Is equal to",
                neq: "Is not equal to"
              }
            }
          },
          //filterable: {
          //  mode: "row"
          //},
          selectable: 'multiple, row',

          allowCopy: false,
          rowTemplate: rowTemplate,
       //   height: $(window).innerHeight() - 256,
          scrollable:true,
          editable: false,
          edit:function(e)
          {



          },
          excelExport: function(e) { //Color the Alternating Rows
            function colorRow(row,color)
            {
              for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex ++) {
                row.cells[cellIndex].background = color;
              }
            }
            var sheet = e.workbook.sheets[0];
            if((<any>e.workbook.sheets[0].rows[0]).type =="header")
            {
              var headerRow = e.workbook.sheets[0].rows[0];
              var optionsHeader = (<ui.IFieldDisplayProperties[]>e.sender.options.columns).filter(h=>(<ui.IFieldDisplayProperties>h).displayed!=false);
              for (var cellIndex = 0; cellIndex < headerRow.cells.length; cellIndex ++) {
                headerRow.cells[cellIndex].value = optionsHeader[cellIndex].exportTitle?optionsHeader[cellIndex].exportTitle:headerRow.cells[cellIndex].value;
              }

            }
            for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
              if (rowIndex % 2 == 0) {
                var row = sheet.rows[rowIndex];
                colorRow(row,"#aabbcc");
              }
            }
            if(onExcelPreparationsFn) {
              onExcelPreparationsFn(e.workbook);
            }
          },

          // scrollable: {
          ////  virtual: true
          //}

        };


        options.gridExcel =  {
          allPages: true,
          fileName: 'Groups.xlsx',
          filterable:true
        };
        options.gridPdf =  {
          allPages: true,
            fileName: 'Groups.pdf',
            creator: 'DocAuthority',
            landscape: true,
            margin: {left: 36, right: 36, top: 36, bottom: 36},    // 0.5" margin
          subject: 'DocAuthority report'
        };

        function getQueryVariable(url,variable) {
          var query = url;
          var vars = query.split('&');
          for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
              return decodeURIComponent(pair[1]);
            }
          }
        }

        var transportCompleteHandler = function(jqXHR, statusText,xx,x) {
          if (statusText === "success") {
            var requestUrl = this.url;
            var pageSize = getQueryVariable(this.url,'pageSize');
            var pageSizeReduced = getQueryVariable(this.url,'pageSizeReduced');


              if(onDataCompleteFn)
              {
                jqXHR.success(function(e){

                      onDataCompleteFn( parseInt(pageSize),pageSizeReduced=='true',requestUrl);

                });


            }
            return;
          }
          //if (jqXHR.status == 200) {
          //
          //}
          if (jqXHR.status == 500 || jqXHR.status == 404) {

            if(onErrorReadResponseFn) {
              onErrorReadResponseFn(jqXHR.status,jqXHR.url,jqXHR.responseJSON.message);
            }
            console.error('WOW NO \n\n server error');
          }
          else if (jqXHR.status == 502) {
            console.error('Communication error');
          }
          else if (jqXHR.status == 400) {
            if(onErrorReadResponseFn) {
              onErrorReadResponseFn(jqXHR.status,jqXHR.url,jqXHR.responseJSON.message);
            }

            console.error('Bad request/search: ', jqXHR.responseJSON.message);
          }

        };
        options.dataSource = <any>{
          pageSize: 30,
          autoSync: false,
          serverPaging: true,
          serverSorting: true,
          serverFiltering: true,
          filter:filterData,
          batch: false, //If set to true the data source will batch CRUD operation requests
          transport: {
            read:{
              url:url,
              beforeSend: function(jqXHR, settings) {
                jqXHR.url = settings.url;
                var auth = 'FREE-PASS';
                jqXHR.setRequestHeader('DA-XSRF', auth);
                jqXHR.timeout = 360000;  //6 minutes in milliseconds
              },

             data: function (e)
                  {

                    if(e.pageSize>maxRecordsPerRequest)//limit max amount of files
                    {
                      e.pageSize =maxRecordsPerRequest;
                      e.pageSizeReduced =true;

                    }
                    if(e.pageSize === maxRecordsPerRequest)
                    {
                      e.export = true;
                    }
                    if(e.take>maxRecordsPerRequest)//limit max amount of files
                    {
                      e.take =maxRecordsPerRequest;
                      e.pageSizeReduced =true;

                    }
                    if(e.filter !=undefined) //for column filter
                    {
                      iterate(e.filter,function(obj,property){
                          if(obj[property] && obj[property].toString().indexOf('item.')==0)
                          {
                            obj[property] = obj[property].substr(5);

                          }
                      })
                    }

                    var p = urlParams?urlParams(e):null;
                    if(p) {
                      e = angular.extend(e, p);
                    }
                    if(manipulateReadRequestFn)
                    {
                      return manipulateReadRequestFn(e);
                    }
                      return e;
                  },
              complete: transportCompleteHandler
            },

            update: {
              url: updateUrl?updateUrl:"",
              data: function (e)
              {
                if(e.item !=undefined)
                {

                  e = e.item;
                  delete e.item;
                }
                return e;
              },
            //  data:urlParams || {},
              dataType: 'json',
              type: 'POST',
              complete: transportCompleteHandler

            },


            parameterMap: function (data /*, type*/) {
              //Convert complex objects in request such as sort and filter to json.
              var res = {};
              for (var f in data) {
                res[f] = (typeof data[f] === 'object') ? kendo.stringify(data[f]) : data[f];
              }
              return res;
            },

          },
          error: function(e) {
          }
        };

        function iterate (obj,fn) {
          for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
              if (typeof obj[property] == "object") {
                iterate(obj[property], fn);
              }
              else {
                fn(obj,property);
              }
            }
          }
        }
        var schemaFields = fields.map(function(field:ui.IFieldDisplayProperties):kendo.data.DataSourceSchemaModelField
         {
           return {
             field: field.fieldName,
             type: field.type.toString(),
             editable: field.editable,
             nullable: field.nullable,
             parse: field.parse,

           }
      } );
        var kendofields = {};
        schemaFields.forEach(f=>kendofields[f.field] = f);
      var knownTotalElementsHandlerFn =  function (response) {
        if (response !== Object(response))  {  //incase the result is primitive value
          if (response == 0 ){
            return 0;
          }
          return 1;
        }
        return response.totalElements; // total is returned in the "total" field of the response
      }
       var unknownTotalElementsHandlerFn =  function(response){
          var pageSize = response.pageSize?response.pageSize:response.size;
          var pageNumber = response.pageNumber?response.pageNumber: response.totalPages;
          if(response.last)//pageSize&& pageSize>response.numberOfElements)
          {
            return pageNumber*pageSize;
          }
          return response.pageSize*2200; //must be set inorder that next page will be enabled and for excel export records limit reached
        };
        options.dataSchema = {
          data: function (response){
            var data = KendoGridOptionsBuilder.setResponseData<T>(response,totalAggregatedCountFn,dataManipulationFn);
            return data;
          },
          total:totalElementsIsReturned? knownTotalElementsHandlerFn:unknownTotalElementsHandlerFn,
          model: {
            id:getPrimaryKey(),
            fields: schemaFields
          }
        };

        function getPrimaryKey()
        {
          var p = fields.filter(function (f) { return f.isPrimaryKey; });
          return p[0]!=null?p[0].fieldName:'id'
        }

        var requireElipsis = function(isEllipsis:boolean,centerText:boolean,noWrap,ddlInside:boolean)
        {
          return isEllipsis||centerText||noWrap||ddlInside?{
            class: (isEllipsis?"ellipsis":'')+(centerText?' centerText':'')+(noWrap?' noWrap':'')+(ddlInside?' ddl-cell':''),
          }:null;
        }
        options.columns =[];
        fields.forEach(field =>
        {
          if(field.displayed) {
            options.columns.push( <kendo.ui.GridColumn> {
              field: field.fieldName,
              title: field.title,
              template: field.template,
              sortable: field.sortable,
              filterable: field.filterable,
              format:field.format,
              validation:field.validation,
              editor:field.editor,
              width:field.width,
              attributes: requireElipsis(field.ellipsis,field.centerText,field.noWrapContent,field.ddlInside),
              headerTemplate: '<span  title="'+field.title+'">'+field.title+'</span>',
              exportTitle:field.exportTitle //The title for excel downloaded file header
            })
          }
        } );

        var result = new KendoGridHelper(options.getOptions());

        return result;

      }
     static setResponseData=function<T>(response,totalAggregatedCountFn,dataManipulationFn)
     {
       var pageNumber = response.pageNumber!=null?response.pageNumber:response.number!=null?response.number+1:null; //!=null is needed cause response.numer may be 0 and just response.number? returns false
       //if(response.content&& response.content[0] &&(<Entities.DTOAggregationCountItem>response.content[0]).item)
       //{
       //  response.content = response.content.map(function (c) {
       //      angular.extend(c,(<Entities.DTOAggregationCountItem>c).item);
       //      delete(c.item);
       //  });
       //}
       var data = response && response.content?response.content:response;
       if(totalAggregatedCountFn)
       {
         var totalElements;
         if (response !== Object(response)) {  //in case the result is primitive value
           totalElements = 1;
           pageNumber = 1;
         }
         else if(response && response.content) {
           totalElements = (<any>response).totalElements;
         }
         else {
           totalElements = Array.isArray(response)? response.length:null;
           pageNumber = 1;
         }

         totalAggregatedCountFn(totalElements,pageNumber,data);
       }
       if(dataManipulationFn!=null) {

         return dataManipulationFn(<T[]>data)
       }
       return data;
     }
}

class KendoGridHelper implements ui.IGridHelper{
  constructor(public gridSettings)
  {
    this.gridSettings = gridSettings;
  }
  selectFirstRow(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
     var row =  gridData.items()[0];
   //   var row = gridElement.find('tr:first');
     // if(!gridData.select()) {
        gridData.select(row);
    //  }
    }
  }
  getSelectedRowsUid(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var selectedRow = gridData.select();
      if(selectedRow[0]) {
        //get data-uidws of all selected rows
       return selectedRow.map(function(){return $(this).attr("data-uid");}).get();
      }
    }
    return null;
  }
  getCurrentUrl(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var url = gridData.dataSource.transport.options.read.url;
      return url;
    }
    return null;
  }
  getCurrentFilter(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {

      if(gridData.dataSource.filter())
      {
         return gridData.dataSource.filter();
      }
    }
    return null;
  }
  unselectAll(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      gridData.clearSelection();
    }
   }
  selectRowByUid(gridElement:any,rowUidToSelect)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var newRow = gridData.table.find('tr[data-uid="' + rowUidToSelect + '"]');
      //   var row = gridElement.find('tr:first');
      if(newRow[0]) {
        gridData.select(newRow);
        return true;
      }
    }
    return false;
  }
  selectRowsByUid(gridElement:any,rowUidsToSelect:string[])
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData && rowUidsToSelect&& rowUidsToSelect[0]) {
      //var selected =   this.selectRowByUid(gridElement,rowUidsToSelect[0]);
      var selected;
      //rowUidsToSelect.splice(0,1)
      //rowUidsToSelect.forEach(rowUid=>
      //{
      //  var newRow = gridData.table.find('tr[data-uid="' + rowUid + '"]');
      //  if(newRow[0]) {
      //    newRow.addClass('k-state-selected');
      //    selected=true;
      //  }
      //})
      var selector = rowUidsToSelect.map(uid=>"tr[data-uid='" + uid + "']").join(',');
      gridData.select(selector);
      return selected;
    }
    return false;
  }

  resizeGrid(gridElement:any,marginBottom:number)
  {
    marginBottom=marginBottom?marginBottom:20;
    var dataArea = gridElement.find(".k-grid-content");
    var pager = gridElement.find(".k-grid-pager");
    var header = gridElement.find(".k-grid-header");
    var otherGridElements = gridElement.children().not(".k-grid-content"); //This is anything ather than the Grid iteslf like header, commands, etc
    var otherGridElementsHeight=0, otherElementsHeight=0;

    //Calcualte all Grid elements height
    otherGridElements.each((i,o)=>
    {
      otherGridElementsHeight += $(o).outerHeight(true);
    });

    var mainBody = $(".main-body-grid");
    var top = mainBody.offset().top;
    var modalBody=gridElement.closest(".modal-body")[0];
    if(modalBody){
      mainBody= $(modalBody);
   //   top=gridElement.offset().top;
   //   let dialogMinHeight = $(modalBody).outerHeight(true);
   //   let dialogHeight = $(modalBody).height();
    //  let modalFooter = $(modalBody).parent().find('.modal-footer')[0];
   //   otherElementsHeight = $(modalFooter).outerHeight(true)+((dialogMinHeight-dialogHeight)/2) ; //for 10px*2 padding
    }

    var mainBodyBottom = mainBody.offset().top + mainBody.height();
    var calculatedHeight = mainBodyBottom - gridElement.offset().top -  otherGridElementsHeight - otherElementsHeight;

    //set grid height
    $(dataArea).height(calculatedHeight- marginBottom );
    gridElement.height(calculatedHeight+ otherGridElementsHeight- marginBottom );
  }
  scrollSelectedRowIntoView(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData)
    {
      var selectedRow =gridElement.find(".k-grid-content .k-state-selected");
      if(selectedRow[0] )
      {
        var inView =  isElementInView(selectedRow);
        if(inView<0)
        {
          var totalScrollNeeded=0;
          var content = gridElement.select(".k-grid-content");
          if(selectedRow.offset().top<content.offset().top)
          {
            totalScrollNeeded= selectedRow.offset().top-content.offset().top;
          }
          else {

            totalScrollNeeded = inView * -1 + gridElement.find(".k-grid-content").scrollTop() + selectedRow.height() + 10;
          }
          gridElement.find(".k-grid-content").scrollTop(totalScrollNeeded);
          // content.scrollTop(inView*-1); //scroll the
        }
      }
    }

    function isElementInView(elem){
      var content = gridElement.select(".k-grid-content");
      var val = (content.offset().top+content.height())-( elem.offset().top+ elem.height());
      if(elem.offset().top<content.offset().top)
      {
        return elem.offset().top-content.offset().top;
      }
      return val ;
    }
  }
  setData(gridElement:any,data:any[])
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      gridData.dataSource.data(data);
    }

  }
  getData(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      return gridData.dataSource.data();
    }

  }
  getRowDataByUid(gridElement:any,uid:string)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var item = gridData.dataSource.data().filter(d=>d.uid==uid)[0];
      //   var row = gridElement.find('tr:first');
      return item;
    }
    return null;
  }

  getSelectedRowsDataItems(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var data = gridData.dataItems();
      var selectedUIDs = gridData.select();
      if (data && data.length > 0 && selectedUIDs && selectedUIDs[0]) {

        var items = [];
        for (var i = 0; i < selectedUIDs.length; i++) {
          var rowUid = $(selectedUIDs[i]).attr('data-uid');
          var selectedRowitem = data.filter(d=>d.uid == rowUid)[0];
          if(selectedRowitem) {
            items.push(selectedRowitem);
          }
        }
        return items;
      }
    }
      return null;
   }

  selectRowByDataItem(gridElement:any,dataItemIds:number[])
  {
    var result = false;
    var gridData = gridElement.data("kendoGrid");
    if(gridData ) {

      var view = gridData.dataSource.view();
      if (view && dataItemIds && dataItemIds[0]) {
        for (var i = 0; i < dataItemIds.length; i++) {
          var row = view.filter(v=>v.id==dataItemIds[i])[0];
          if (row) {
            gridData.select($(gridElement).find("tr[data-uid='" + row.uid + "']"));
            result = true;
          }
        }

      }
    }
    return result;
  }
  selectPrevRow(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData ) {
       var dataSource = gridData.dataSource;
       var selected = this.getSelectedRowsDataItems(gridElement);
       var  index = dataSource.indexOf(selected[0]);
       var modelToSelect = dataSource.data()[(index-1)>0?(index-1):selected.length<dataSource.data().length?selected.length:0];
      this.unselectAll(gridElement);
      return this.selectRowByDataItem(gridElement,[modelToSelect.id]);
    }
    return false;
  }
  setMultipleRowSelection(gridElement:any,enabled:boolean)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      if(enabled) {
        gridData.selectable = "multiple, row";
      }
      else
      {
        gridData.selectable = "row";
      }
    }
  }
  editCellMode = function(gridElement:any,targetElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var  cell = $(targetElement).closest('td');/* you have to find cell containing check box*/
      gridData.editCell(cell);
    }
  }

  closeEditCellMode = function(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      gridData.closeCell();
    }
  }

  registerToFocusEvent(gridElement:any){
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      var lastNavigateTr:any  = null;
      gridData.bind("navigate", function (evt) {
        if (lastNavigateTr) {
          lastNavigateTr.removeClass("navigate-kendo-grid-tr");
        }
        lastNavigateTr = evt.element.closest("tr");
        lastNavigateTr.addClass("navigate-kendo-grid-tr")
      });
    }
  }
  getPageNumber(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      return gridData.dataSource.page();
    }
    return null;
  }
  sort(gridElement:any,sortField:string,descDirection:boolean)
  {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      // get currently applied filters from the Grid.
      var sort = [];

      sort.push({field: sortField, dir: descDirection?'desc':'asc'});
      gridData.dataSource.sort(sort);
    }
  }
  filter(gridElement:any,filterField:string, filterValue:any,fieldType:string,operator?:EFilterOperators){


    // get the kendoGrid element.
    var gridData = gridElement.data("kendoGrid");
    if(gridData)
    {

      if(fieldType == 'number')
      {
        filterValue =parseFloat(filterValue);
      }
      var currentFilters =[];
      //filter={"logic":"and","filters":[{"field":"numOfFiles","operator":"eq","value":"15"}
      currentFilters.push({
        field: filterField,
        operator: operator?operator.value.toString(): "contains",
        value: filterValue
      });

        gridData.dataSource.filter(currentFilters);

    }
  }
  filterKendoFormat(gridElement:any,filter:ui.IKendoFilter) {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      if(gridData.dataSource.transport.options&&gridData.dataSource.transport.options.read.url) {
        gridData.dataSource.filter(<any>filter);
      }
    }

  }
  setFilterKendoFormatToDataSource(dataSource:any,filter:ui.IKendoFilter) {
    if(dataSource) {
      if(dataSource.transport.options&&dataSource.transport.options.read.url) {
        dataSource.filter(<any>filter);
      }
    }
  }
  hidePageButtonsIfNeeded(gridElement:any) {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      var configPageSize =gridData.dataSource.pageSize();
      var actualPageSize =gridData.dataItems().length;
      if(  gridData.dataSource.page()==1)
      {
        gridData.pager.element.find('[title="Previous"]').css("visibility", "hidden");
      }
      else
      {
        gridData.pager.element.find('[title="Previous"]').css("visibility", "visible");
      }
      if(actualPageSize<configPageSize) {
        gridData.pager.element.find('[title="Next"]').css("visibility", "hidden");
      }
      else
      {
        gridData.pager.element.find('[title="Next"]').css("visibility", "visible");
      }
    }
  }

  saveAsExcel(gridElement:any,fileName:string) {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      gridData.options.excel.fileName=fileName;
      gridData.saveAsExcel();
    }
  }

  saveAsPdf(gridElement:any,fileName:string) {
    var gridData = gridElement.data("kendoGrid");
    if(gridData) {
      gridData.options.pdf.fileName=fileName;
      gridData.saveAsPDF();
    }
  }

  refreshTemplate(gridElement:any,template:any) {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      gridData.options.rowTemplate = template;
    }
  }
  loadData(gridElement:any,url,pageNumber:number,filter,isClientFilter,sortField,descDirection) {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {

      if(!url||url=='')
      {
        gridData.dataSource.transport.options.read.url = url;
        gridData.dataSource.data([]);
      }
      else {
        gridData.options.autoBind=true;
        gridData.dataSource.transport.options.read.url = url;
  //     gridData.dataSource.page(pageNumber); //reset page numbers
        let params = {
          page: pageNumber,
          pageSize: gridData.getOptions().dataSource.pageSize,
          take:gridData.getOptions().dataSource.pageSize,
        };
        if(sortField) {
          params['sort'] = {field: sortField, dir: descDirection ? 'desc' : 'asc'};
        }
        if(filter) {
          params['filter'] = filter;
        }
      //  console.log('111params' +url+' '+ JSON.stringify(params));
        gridData.dataSource.query(params);
      }
     // gridElement.data("kendoGrid").dataSource.read();

    }
  }
  reloadData(gridElement:any) {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      //if( gridData.options.autoBind) {
      if(gridData.dataSource.options.transport.read.url) {
        gridData.dataSource.read();
      }
      //}
     // gridData.dataSource.page(1); //reset page numbers
    }
  }
  clear(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      if(gridData.dataSource.data().length>0) {
        gridData.clearSelection();
        gridData.dataSource.data([]);
        gridData.dataSource.transport.options.read.url=null;
      }
    }
  }
  refreshDisplay(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      gridData.refresh();
    }
  }
  setPage(gridElement:any, pageNumber:number)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
     gridData.dataSource.page(pageNumber);

    }
  }
  getTotalElementsCount(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      return gridData.dataSource.total();

    }
    return null;
  }

  nextPage(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      var currentPage = gridData.dataSource.page();
      gridData.dataSource.page(currentPage+1);
    }
  }

  prevPage(gridElement:any)
  {
    var gridData = gridElement.data("kendoGrid");
    if (gridData) {
      var currentPage = gridData.dataSource.page();
      gridData.dataSource.page(currentPage-1);
    }
  }

  setOnlyOnePageStateClass = function(gridElement,totalElements,configuration,entityDisplayType: EntityDisplayTypes) {
    gridElement.removeClass('one-page-only');
    gridElement.removeClass('one-page-only-show-count');
    var gridData = gridElement.data("kendoGrid");
    if (gridData && gridData.dataSource) {
      var configPageSize = gridData.dataSource.pageSize();
      var onlyOnePage = configPageSize - totalElements >= 0;
      if (onlyOnePage) {
        gridElement.addClass('one-page-only');
      }
      else if (entityDisplayType != EntityDisplayTypes.files &&  totalElements > configuration['facet-count-hll.lower-bound']) {
        gridElement.addClass('one-page-only-show-count');
      }
    }
  }
}



class KendoGridViewOptions {
  dataSource:any;//kendo.data.DataSourceOptions;
  dataSchema:any;
  columns: kendo.ui.GridColumn[];
  gridDisplaySettings:kendo.ui.GridOptions;
  gridPdf:kendo.ui.GridPdf;
  gridExcel:kendo.ui.GridExcel;
  toolbar:kendo.ui.GridToolbarItem[];

  getOptions():kendo.ui.GridOptions {
    this.gridDisplaySettings.columns = this.columns;
    this.gridDisplaySettings.toolbar = this.toolbar;
    this.gridDisplaySettings.excel = this.gridExcel;
    this.gridDisplaySettings.pdf = this.gridPdf;
    this.gridDisplaySettings.dataSource = this.dataSource;
    this.gridDisplaySettings.dataSource.schema= this.dataSchema;
    this.gridDisplaySettings.navigatable = true;
    return this.gridDisplaySettings;

  }

}

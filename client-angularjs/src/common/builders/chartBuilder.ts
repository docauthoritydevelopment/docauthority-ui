///<reference path='../all.d.ts'/>

'use strict';
angular.module('ui.builder.charts', [
])
.factory('chartsBuilder', function(propertiesUtils) {
  var myFactory:ui.IChartOptionsBuilder = <ui.IChartOptionsBuilder>{};

  myFactory.createPie = function(histogramData,chartTitle:string,
                                      fields:ui.IChartFieldDisplayProperties[],
                                      seriesColors:string[]) :ui.IChartHelper{
    return KendoChartOptionsBuilder.createPie(histogramData,chartTitle,fields,seriesColors);
  };

  myFactory.createBarChart = function(histogramData,chartTitle:string,
                                      fields:ui.IChartFieldDisplayProperties[],
                                      gap) :ui.IChartHelper{
    return KendoChartOptionsBuilder.createBarChart(histogramData,chartTitle,fields,gap);
  };
  myFactory.createColumnChart = function(stackedHistogramData,chartTitle:string,
                                         fields:ui.IChartFieldDisplayProperties[],
                                         seriesColors:string[]) :ui.IChartHelper{
    return KendoChartOptionsBuilder.createColumnChart(stackedHistogramData,chartTitle,fields,seriesColors);
  };
  myFactory.createStackedBarChart = function(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                                             fields:ui.IChartFieldDisplayProperties[],
                                             seriesColors:string[]):ui.IChartHelper{
    return KendoChartOptionsBuilder.createStackedBarChart(stackedHistogramData,chartTitle,fields,seriesColors);
  };


  return  myFactory
});

class KendoChartOptionsBuilder
//implements  ui.IChartOptionsBuilder
{

 static createPie(histogramData,chartTitle:string,
                fields:ui.IChartFieldDisplayProperties[],
                seriesColors:string[]):ui.IChartHelper

  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var categoryPercentageLabelTemplate = "#= category #\n#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) # ";
    var categoryLabelTemplate = "#= category #\n #= kendo.toString(value, \"N0\")#";
    var tooltipTemplate =tooltipField? "# if (dataItem."+tooltipField.fieldName+"!=null && dataItem."+tooltipField.fieldName+"!='') {# " +
  "#: dataItem."+tooltipField.fieldName+" # # } else {# #= category #<br />#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) #  #}#" :null;
    var mainOptions = {
      //chartArea: {
      //  width: 420,
      //  height: 420
      //},
        dataSource: {
          data: histogramData,
          sort: {
            field: seriesField.fieldName,
            dir: seriesField.dir
          }
        },
        //    seriesClick: onSeriesClick,
        title: {
          visible:false,
          text: chartTitle
        },
        legend: {
          visible:false,
          position: "top"
        },
        seriesDefaults: {
          type: "pie",
          startAngle: 315,
          labels: {
            visible: true,

            template: categoryPercentageLabelTemplate,
            background: "transparent",
            border: {
              color: "transparent",
              width: 0
            },
          }
        },
        seriesColors:seriesColors,
        transitions: true,
        series: [{
          field: seriesField.fieldName,
          name: seriesField.title,
          categoryField: categoryField.fieldName,
          labels: {
            visible: true,
            color: function(e){
              var theItem = e.series.data.filter(s=> e.dataItem.uid == s.uid)[0];
              return theItem&& theItem.labelUserColor?theItem.labelUserColor:'black';
            },
            font: function(e){
              var theItem = e.series.data.filter(s=> e.dataItem.uid == s.uid)[0];
              return theItem&& theItem.labelUserColor?'16px':null;
            }
          }

        }],
        categoryAxis: {
          line: {
            visible: false
          },
          field: categoryField.fieldName,
          labels: {
            //rotation: -90
            format: categoryField.format,
          },
          majorGridLines: {
            visible: false
          }
        },
        valueAxis: {
          visible:false,
          labels: {
            format: seriesField.format
          },
          //  majorUnit: 100,
          line: {
            visible: false
          }
        },
        tooltip: {
          visible: true,
          color: "white",
          // format: "{0}%",
          template:tooltipTemplate
        }
      };

    var result = new KendoChartHelper(mainOptions);
    return result;

    };

  static createBarChart(histogramData,chartTitle:string,
            fields:ui.IChartFieldDisplayProperties[],
            gap):ui.IChartHelper
  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var categoryPercentageLabelTemplate = "#= category #\n#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) # ";
    var categoryLabelTemplate = "#= category #\n #= kendo.toString(value, \"N0\")#";
    var tooltipTemplate =tooltipField? "# if (dataItem."+tooltipField.fieldName+"!=null && dataItem."+tooltipField.fieldName+"!='') {# " +
    "#: dataItem."+tooltipField.fieldName+" # # } else {# #= category #<br />#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) #  #}#"
        :null;
    var mainOptions = {
      dataBound: function (e) {
      },
      dataSource: {
        data: histogramData,

        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:false,
        position: "top"
      },
      seriesDefaults: {
        type: "bar",
        gap: gap,
        color:"#4F6990",
        labels: {
          visible: true,
          format: seriesField.format,
          background: "transparent"
        }
      },
      series: [{
        field: seriesField.fieldName,
        name: seriesField.title,
        colorField: "userColor",
        labels: {
          visible: true,
          color: function(e){
            return e.index&& e.series.data[e.index].labelUserColor?e.series.data[e.index].labelUserColor:'#000';
          },
          font: function(e){
            return e.index&& e.series.data[e.index].labelUserColor?'16px':null;
          }
        }
       }],
      categoryAxis: {
        line: {
          visible: false
        },
        field: categoryField.fieldName,
        labels: {
          visible: true,
          //visual: function(e) {
          //  // Build an HTML fragment and append it to the body
          //  var html = $('<div><b>' + e.text + '</b></div>')
          //      .appendTo(document.body);
          //
          //  // Create an empty group that will hold the rendered label
          //  var visual = new kendo.drawing.Group();
          //
          //  // Store a reference to the target rectangle, see below
          //  var rect = e.rect;
          //
          //  kendo.drawing.drawDOM(html)
          //      .done(function(group) {
          //        // Clean-up HTML fragment
          //        html.remove();
          //
          //        // Center the label using Layout
          //        var layout = new kendo.drawing.Layout(rect, {
          //          justifyContent: "center"
          //        });
          //        layout.append(group);
          //        layout.reflow();
          //
          //        // Render the content
          //        visual.append(layout);
          //      });
          //
          //  return visual;
          //}
        },
        majorGridLines: {
          visible: false
        }
      },
      valueAxis: {
        visible:false,
        labels: {
          format: categoryField.format
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
    //    format: "N0",
        color: "white",
        template:tooltipTemplate
      },

    };

    var result = new KendoChartHelper(mainOptions);
    return result;

  };

  static createColumnChart(stackedHistogramData,chartTitle:string,
                    fields:ui.IChartFieldDisplayProperties[],
                    seriesColors:string[]):ui.IChartHelper
  {
    var mainOptions = {
      dataSource: {
        data: stackedHistogramData,
        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:true,
        position: "top"
      },
      seriesDefaults: {
        type: "column",
        //   stack: true,
        //   gap:0.2,

        labels: {
          visible: true,
          position:'center',
          format: "N0",
          background: "transparent",

        }
      },
      series:stackedHistogramData.series,
      valueAxis: {
        visible:false,
        labels: {
          format: "N0"
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      categoryAxis: {
        categories:stackedHistogramData.categories,
        majorGridLines: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
        template: "#= series.name #: #= value #"
      }
    };

    var result = new KendoChartHelper(mainOptions);
    return result;
  };

  static createStackedBarChart(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                 fields:ui.IChartFieldDisplayProperties[],
                 seriesColors:string[]):ui.IChartHelper
  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var categoryPercentageLabelTemplate = "#= category #\n#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) # ";
    var categoryLabelTemplate = "#= category #\n #= kendo.toString(value, \"N0\")#";
    var tooltipTemplate =tooltipField? "# if (dataItem."+tooltipField.fieldName+"!=null && dataItem."+tooltipField.fieldName+"!='') {# " +
    "#: dataItem."+tooltipField.fieldName+" # # } else {# #= category #<br />#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) #  #}#"
        :null;
    var mainOptions = {
      dataSource: {
        data: stackedHistogramData,
        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:true,
        position: "top",

      },
      seriesDefaults: {
        type: "bar",
        stack: true,
        //stack: {
        //  type: "100%"
        //},

        gap:0.2,

        labels: {
          visible: true,
          position:'center',
          rotation: 90,
          format: seriesField.format,
          background: "transparent",
          template:'# if(value>0){# #= value # # } #'

        }
      },
      series:stackedHistogramData.series,
      valueAxis: {
        min: 0,
        visible:false,
        labels: {
          format: categoryField.format,
          visible:true
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      categoryAxis: {
        labels: {
          visible:true
        },
        categories:stackedHistogramData.categories.map(c=>c.name),
        majorGridLines: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
        template: "#= series.name #: #= value #"
      }
    };

    var result = new KendoChartHelper(mainOptions);
    return result;

  };
};

class KendoChartHelper implements ui.IChartHelper{
  constructor(public chartSettings)
  {

  }
  setTitle(chartElement:any,title:string) {
    var chartView = chartElement.getKendoChart();
    if (chartView) {
      chartView.options.title.text =title;
    }
  }
  showTitle(chartElement:any,show:boolean)
  {
    var chartView = chartElement.getKendoChart();
    if (chartView) {
      chartView.options.title.visible=show;
      chartView.options.transitions = false;
      chartView.redraw();
      chartView.options.transitions = true;

    }
  }

  saveAsExcel(chartElement:any,fileName:string) {
    var chartView = chartElement.getKendoChart();
    if (chartView) {
      chartView.options.excel.fileName=fileName;
      chartView.saveAsExcel();
    }
  }

  saveAsPdf(chartElement:any,fileName:string,successFn) {
    var chartView = chartElement.getKendoChart();
    if (chartView) {
      chartView.options.pdf.fileName=fileName;
      chartView.exportPDF({ paperSize: "auto", margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" } }).done(function(data) {
        kendo.saveAs({
          dataURI: data,
          fileName: fileName,
        });
        if(successFn)
        {
          successFn();
        }
      });
    }
  }

  saveAsImage(chartElement:any,fileName:string,successFn) {
    var chartView = chartElement.getKendoChart();
    if (chartView) {
      chartView.exportImage().done(function(data) {
        kendo.saveAs({
          dataURI: data,
          fileName: fileName,
        });
        if(successFn)
        {
          successFn();
        }
      });
    }
  }
}

class KendoChartDataBuilder
{
  static castToStackedData=function(categories:any[],series:any[],seriesData:Dashboards.DtoItemCounter<any>[],findSeriesValueByFun:(seriesData,uniqueSeries,uniqueCat)=>number, propertiesUtils): ui.IStackedChartData {
    var uniqueCategories = <any[]>propertiesUtils.unique(categories);
    var uniqueCategoriesCast = uniqueCategories.map(c=><ui.ICategoryChartData>{name:c});
    var uniqueSerieses = <any[]>propertiesUtils.unique(series);
    var seriesCast:ui.ISeriesChartData[] = [];
    var seriesValues:number[] = [];
    uniqueSerieses.forEach(uniqueSeries=> {
      seriesValues = [];
      uniqueCategories.forEach(uniqueCat=> {
        var seriesValue = findSeriesValueByFun(seriesData, uniqueSeries, uniqueCat);
        seriesValues.push(seriesValue)
      });
      seriesCast.push(<ui.ISeriesChartData>{name: uniqueSeries, data: seriesValues});
    });

    var stackedHistogramData = <ui.IStackedChartData>{categories: uniqueCategoriesCast, series: seriesCast};
    return stackedHistogramData;
  }
}

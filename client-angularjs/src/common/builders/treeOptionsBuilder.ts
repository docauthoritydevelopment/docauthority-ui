///<reference path='../all.d.ts'/>

'use strict';


class KendoTreeOptionsBuilder<T> implements  ui.ITreeOptionsBuilder<T>
{

     createTree<T>(elementName:string,url:string,urlParams,
                                  fields:ui.IHierarchicalFieldDisplayProperties[],
                                  maxRecordsPerRequest:number,
                                  hasChildrenFn:(data: T)=> boolean,
                                   dataManipulationFn:(data: T[])=> T[],
                                  autoBind: boolean,
                                  itemTemplate?:string,
                                  filterData?:any,
                   manipulateReadRequestFn?:(queryParams: any)=>void,
                   handleMultiSelectionChangeFn?:any,onErrorReadResponseFn?:(status,url,error: any)=>void,
                    onExpandNodeFn?:(nodeElement: any)=>void)
      {

        var options:KendoTreeViewOptions = new KendoTreeViewOptions();

        options.treeDisplaySettings = <kendo.ui.TreeViewOptions>{
          autoBind: autoBind,
         autoScroll: true,
     //     name: 'tree',
          animation: {
            collapse: {
              duration: 500,
              effects : "fadeOut"
            },
            expand  : {
              duration: 500,
              effects : "fadeIn"
            }
          },
          //
          //checkboxes: {
          //  checkChildren: true
          //},
      //  dataImageUrlField?: string;

     //   dataSpriteCssClassField?: string;

      //  dataUrlField:  'jjj',
      //  dragAndDrop: true,
          expand: function(e) {
            onExpandNodeFn(e.node);
          },
        loadOnDemand: true,
          messages: {
            loading: "loading Folders..."
          },
          template: itemTemplate,
          select: function(e)
          {
            var selectedItemIsAlreadySelected = $(e.node).find('.k-in.multi-state-selected')[0];
            var ctrlKeyIsPressed = $(e.node).find('.k-in').closest('.ctrlKey')[0];
            if ($(e.node).find('.k-in').hasClass('da-menu-open'))
            {
              // Menu is about to be opened: remove indication and ignore default action
              $(e.node).find('.k-in').removeClass('da-menu-open');
              e.preventDefault();
            }
            else if($(e.node).find('.dropdown-wrapper.open').length > 0)
            {
              // Menu is opened: ignore default action
              e.preventDefault();
            }
            else if(!ctrlKeyIsPressed)
            {
              $('.k-in').removeClass('multi-state-selected');
            }
            else if(selectedItemIsAlreadySelected)
            {
              $(e.node).find('.k-in').removeClass('multi-state-selected');
              e.preventDefault();
            }
            else {
              $($(e.node).find('.k-in')[0]).addClass('multi-state-selected');
              e.preventDefault();
              if(handleMultiSelectionChangeFn) {
                handleMultiSelectionChangeFn(); //calling on select manually cause prevent to call it when ctrl is pressed
              }
            }

          },
          //checkboxes: {
          ////  checkChildren: true,
          //  template: "<input type='checkbox' name='checkedFiles[#= item.id #]' value='true' />"
          //},

         // check: handleCheckboxSelectionChangeFn,
        };

        var transportCompleteHandler = function(jqXHR, statusText) {
          if (statusText === "success") {
            return;
          }
          //if (jqXHR.status == 200) {
          //
          //}
          if (jqXHR.status == 500 || jqXHR.status == 404) {
            console.error('WOW NO \n\n server error');
          }
          else if (jqXHR.status == 502) {
            console.error('Communication error');
          }
          else if (jqXHR.status == 400) {
            if(onErrorReadResponseFn) {
              onErrorReadResponseFn(jqXHR.status,jqXHR.url,jqXHR.responseJSON.message);
            }
            console.error('Bad request/search: ' + jqXHR.responseJSON.message);
          }

        };
        options.dataSource =<any> {
        //    filter:filterData,
            transport: {
              read: {
                url: url,
                beforeSend: function(jqXHR, settings) {
                  jqXHR.url = settings.url;
                  var auth = 'FREE-PASS';
                  jqXHR.timeout = 680000;  //in milliseconds
                  jqXHR.setRequestHeader('DA-XSRF', auth);
                },
             //   data: urlParams || {},
                data: function (e)
                {
                  if(e['id'])
                  {
                   var idInUrl = parseQuery(this.url)['id'];
                    if(idInUrl) {
                      this.url = this.url.replace('id=' +idInUrl,''); //remove id query param that there will not be two
                    }
                  }
                  if(e.pageSize>maxRecordsPerRequest)//limit max amount of files
                  {
                    e.pageSize =maxRecordsPerRequest;
                    e.pageSizeReduced =true;

                  }
                  if(e.take>maxRecordsPerRequest)//limit max amount of files
                  {
                    e.take =maxRecordsPerRequest;
                    e.pageSizeReduced =true;

                  }
                  var params = urlParams?urlParams(e):null;
                  //ugly fix for when expanding tree after changing url, url is the prev one
                  if ($('.'+elementName).data("kendoTreeView")) {
                    this.url= (<any>$('.'+elementName).data("kendoTreeView").dataSource).transport.options.read.url;
                    var myUrlParams = (<any>$('.'+elementName).data("kendoTreeView").dataSource).transport.options.read.myUrlParams;
                    params = angular.extend(params,myUrlParams);
                  }
                  if(e['item.id'] !=undefined) //for item.id convert to id
                  {
                    e['id'] = e['item.id'];
                    delete e['item.id'];
                  }

                  if(params) {
                    params = angular.extend(e, params);
                  }
                  if(manipulateReadRequestFn)
                  {
                    return manipulateReadRequestFn(e);
                  }
                  return params;
                },
                error: function (xhr, error) {
                  console.debug(xhr); console.debug(error);

                },
                complete: transportCompleteHandler
              },

            },



      }
        function iterate (obj,fn) {
          for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
              if (typeof obj[property] == "object") {
                iterate(obj[property], fn);
              }
              else {
                fn(obj,property);
              }
            }
          }
        }
        var schemaFields = fields.map(function(field:ui.IFieldDisplayProperties):kendo.data.DataSourceSchemaModelField
         {
           return {
             field: field.fieldName,
             type: field.type.toString(),
             editable: field.editable,
             nullable: field.nullable,
             parse: field.parse
           }
      } );


        options.dataSchema = <kendo.data.HierarchicalDataSourceSchema>{
          data: function (response){ //he configuration used to parse the remote service response.
            if(dataManipulationFn!=null) {
              return dataManipulationFn(response&&response.content?response.content:response)
            }
            if(response.content) {
              return response.content;
            }
            return response;
          },
          //total: 'totalElements',
          model: {
            id:getPrimaryKey(),
        //    fields: schemaFields,
            hasChildren:hasChildrenFn
          }
        }
        function parseQuery(str) {
          return (str).replace('?','&').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        };
        function getPrimaryKey()
        {
          var p = fields.filter(function (f:ui.IFieldDisplayProperties) { return f.isPrimaryKey; });
          return p[0]!=null?p[0].fieldName:'id'
        }
        options.dataTextField =[];
        fields.forEach(field =>
        {
          if(field.displayed) {
            options.dataTextField.push( field.fieldName);
          }
        } );


        var result = new KendoTreeHelper(options.getOptions());
       return result;

      }





  };

class KendoTreeHelper implements ui.ITreeHelper{
  constructor(public treeSettings)
  {
    this.treeSettings = treeSettings;
  }
  refresh(treeElement:any,url:any,urlParams:any) {
    var treeview = treeElement.data("kendoTreeView")
    if (treeview) {
      treeview.options.autoBind=true;
      if(url) {
        treeview.dataSource.transport.options.read.url = url;
        treeview.dataSource.transport.options.read.myUrl = url;

      }
      treeview.dataSource.transport.options.read.myUrlParams = urlParams; //workaround - url is not actually refreshed
      treeview.dataSource.read();
    }

  }
  reloadData(treeElement:any) {
    var treeview = treeElement.data("kendoTreeView")
    if (treeview) {

      treeview.dataSource.read();
    }
  }
  getMultiSelectedItems(treeElement)
  {
    var dataItems=[];
    var selectedIds = treeElement.find('.multi-state-selected');
    var treeview = treeElement.data("kendoTreeView")
    if (treeview&&selectedIds&&selectedIds[0]) {
      selectedIds.toArray().forEach(function (id)
          {
            var item = treeview.dataItem(id)
            dataItems.push(item);
          }
      );
    }
    return dataItems;
  }
  getCheckedItems(treeElement)
  {
    var treeview = treeElement.data("kendoTreeView")
    if (treeview) {
      var checkedNodes=[];
      checkedNodeIds(treeview.dataSource.view(), checkedNodes);
      return checkedNodes;
    }
    function checkedNodeIds(nodes, checkedNodes) {
      for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
          checkedNodes.push(nodes[i]);
        }

        if (nodes[i].hasChildren) {
          checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
      }
    }
  }
  getCurrentUrl(treeElement:any)
  {
    var treeview = treeElement.data("kendoTreeView");
    if(treeview) {
      return treeview.dataSource.transport.options.read.url;
    }
    return null;
  }
  getCurrentFilter(gridElement:any)
  {
    var treeview = gridElement.data("kendoTreeView");
    if(treeview) {

      if(treeview.dataSource.filter())
      {
        return treeview.dataSource.filter();
      }
    }
    return null;
  }
  expandFirstNode(treeElement:any) {
    var treeview = treeElement.data("kendoTreeView")
    if (treeview && treeview.dataItems()[0]) {
      var isFirstNode = treeview.dataItems()[0].children._data.length == 0;
      if (isFirstNode) {
        treeview.expand(".k-item:first");
      }
    }
  }
  expandPath(treeElement:any,text:string,isInPathFun:(dataItem,pathItem)=>any,expandSelected:boolean):any {
    var treeview = treeElement.data("kendoTreeView");
    if (treeview && treeview.dataItems()[0]) {
      var items = treeview.dataItems();
      var found = true;
      var exactMatch = false;
      var dataSource = treeview.dataSource;
      while (items && found && !exactMatch) {
        for (var i = 0; i < items.length; i++) {
          found = false;

          var inPath = isInPathFun(items[i], text);
          var id = items[i].id;
          var dataItem = dataSource.get(id);
          var node = treeview.findByUid(dataItem.uid);
          if (inPath) {

            found = true;
            if ((<any>inPath).exactMatch) {
              treeview.select(node);
             if(expandSelected)
             {
            //   treeview.expand(node);
             }
              exactMatch = true;
            }
            else {
              treeview.expand(node);
              items = items[i].children._data; //children alrea
            }
            break;
          }
          else
          {
          //  treeview.collapse(node);
          }
        }

      }
      if (found) {
        return {found:true,exactMatch:exactMatch};
      }
    }
    return false;
  }

  selectFirstNode(treeElement:any)
  {
   var treeview= treeElement.data("kendoTreeView");
    if (treeview && treeview.dataItems()[0]) {
       treeview.select(".k-item:first");
    }
  }

  unselectAll(treeElement:any)
  {
    var treeview= treeElement.data("kendoTreeView");
    if (treeview && treeview.dataItems()[0]) {
       treeview.select(null);
    }
}

  filterKendoFormat(treeElement:any,filter:any) {
    var treeview= treeElement.data("kendoTreeView");
    if (treeview) {
      this.refresh(treeElement,null,{filter:JSON.stringify(filter)});
    }

  }
  resizeTree(treeElement:any,marginBottom:number)
  {
    marginBottom=marginBottom?marginBottom:20;
    var mainBody = $(".main-body-grid");

    var otherElementsHeight=0;
    var modalBody=treeElement.closest(".modal-body")[0];
    if(modalBody){
      mainBody= $(modalBody);
      // top=treeElement.offset().top;
      // let dialogMinHeight = $(modalBody).outerHeight(true);
      // let dialogHeight = $(modalBody).height();
      // let modalFooter = $(modalBody).parent().find('.modal-footer')[0];
      // otherElementsHeight = $(modalFooter).outerHeight(true)+((dialogMinHeight-dialogHeight)/2) ; //for 10px*2 padding
    }

    var mainBodyBottom = mainBody.offset().top + mainBody.height();
    var calculatedHeight = mainBodyBottom - treeElement.offset().top - otherElementsHeight ;
    treeElement.height(calculatedHeight- marginBottom);
  }
  scrollSelectedNodeIntoView(treeElement:any)
  {
    var treeview= treeElement.data("kendoTreeView");
    if (treeview) {
      {
        var selectedRow = treeElement.find(".k-in.k-state-selected");
        if (selectedRow[0]) {
          var inView = isElementInView(selectedRow);
          if (inView < 0) {
            var totalScrollNeeded = 0;
            var content = treeElement;
            if (selectedRow.offset().top < content.offset().top) {
              totalScrollNeeded = selectedRow.offset().top - content.offset().top;
            }
            else {

              totalScrollNeeded = inView * -1 + treeElement.scrollTop() + selectedRow.height() + 10;
            }
            treeElement.scrollTop(totalScrollNeeded);
            // content.scrollTop(inView*-1); //scroll the
          }
        }
      }
    }
    function isElementInView(elem){
      var content = treeElement;
      var val = (content.offset().top+content.height())-( elem.offset().top+ elem.height());
      if(elem.offset().top<content.offset().top)
      {
        return elem.offset().top-content.offset().top;
      }
      return val ;
    }
  }
  refreshDisplay(treeElement:any)
  {
    var treeview = treeElement.data("kendoTreeView");
    if (treeview) {
      treeview.refresh();
    }
  }

}



class KendoTreeViewOptions {
  dataSource:any;//kendo.data.HierarchicalDataSourceOptions;
  dataSchema:kendo.data.HierarchicalDataSourceOptions;
   treeDisplaySettings:kendo.ui.TreeViewOptions;
  dataTextField:string[];


  getOptions():kendo.ui.TreeViewOptions {

    this.treeDisplaySettings.dataSource = this.dataSource;
    this.treeDisplaySettings.dataSource.schema= this.dataSchema;
    this.treeDisplaySettings.dataTextField = this.dataTextField;
    return this.treeDisplaySettings;

  }

}

'use strict';

var DropdownAdapter = (function () {

    var dropdownMenu, target, resizeTimer;

    var hideDropDown = function(){
      if (dropdownMenu && target) {
        target.append(dropdownMenu.detach());
        dropdownMenu.hide();
      }
      target = null;
      dropdownMenu = null;
    };

    //------------------------------------------------

    var fixLeft = function(dropdownMenu, target) {

      let deep = 0;

      let calcDeep = function(_target, level) {
        var dropdownMenuCnt = _target.find('> .dropdown-submenu:not(.ng-hide)');

        if(dropdownMenuCnt.length > 0) {
          deep = Math.max(deep, level+1);
        }
        for(let i=0; i<dropdownMenuCnt.length; i++) {
          calcDeep($(dropdownMenuCnt[i]).find('.dropdown-menu:first'), level+1);
        }
      };

      calcDeep(dropdownMenu, 0);

      let eOffset = target.offset();
      let left = eOffset.left;
      let totalDropdownWidth = (deep + 1) * dropdownMenu.width();
      let scrollLeft = window.scrollX || document.documentElement.scrollLeft;
      let winWidth = scrollLeft + $(window).width();
      let leftPlusElmWidth = left + totalDropdownWidth;

      dropdownMenu.find('.dropdown-submenu').removeClass('pull-left');

      if(leftPlusElmWidth >= winWidth || dropdownMenu.hasClass('rtl-dir')) {
        dropdownMenu.find('.dropdown-submenu').addClass('pull-left');
        dropdownMenu.css({
          'left': left + target.outerWidth() - dropdownMenu.width(),
        });
      } else {
        dropdownMenu.css({
          'left': eOffset.left
        });
      }
    };

    //------------------------------------------------

    var fixTop = function(dropdownMenu, target) {

      let shrinkHeight = target.hasClass("shrink-if-no-space");

      if(shrinkHeight) {
        dropdownMenu.css({
          'height': 'auto'
        });
      }

      let eOffset = target.offset();
      let scrollTop = window.scrollY || document.documentElement.scrollTop;
      let top = eOffset.top + target.outerHeight();
      let winHeight = scrollTop + $(window).height();
      let dropdownHeight = dropdownMenu.height();

      if(top + dropdownHeight >= winHeight) {
        if(shrinkHeight) {
          dropdownMenu.css({
            'height': winHeight - top - 25,
            'overflow':'auto'
          });
        }
        else if(target.hasClass("pin-to-top")){
          top = 0;
        }
        else {
          top = eOffset.top - dropdownMenu.height();
        }
      }
      dropdownMenu.css({
        'top': top
      });

    };


    //==================================================
    // dropdown events
    //==================================================

    $(window).on('show.bs.dropdown', function (e) {

      hideDropDown();

      if(!$(e.target).hasClass("no-fix")) {
        target = $(e.target);
        dropdownMenu = target.find('.dropdown-menu:first');

        fixLeft(dropdownMenu, target);
        fixTop(dropdownMenu, target);

        $('body').append(dropdownMenu.detach());

        dropdownMenu.css({
          'display': 'block',
          'zIndex': 100000,
          'right': 'auto'
        });
      }
    });

    //------------------------------------------------

    $(window).on('click', function (e) {
      let showAnyway = dropdownMenu && dropdownMenu.hasClass('show-anyway');
      if(!showAnyway) {
        hideDropDown();
      }
    });

    //------------------------------------------------

    $(window).on('resize', function (e) {
      if (dropdownMenu) {
        dropdownMenu.hide();
      }
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        if(target) {
          target.click();
        }
      }, 100);
    });
}());

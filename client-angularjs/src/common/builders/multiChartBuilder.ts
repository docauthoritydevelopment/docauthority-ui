///<reference path='../all.d.ts'/>

'use strict';
angular.module('ui.builder.charts.multi', [
    ])
    .factory('multiChartsBuilder', function(propertiesUtils) {
      var myFactory:ui.IChartOptionsBuilder = <ui.IChartOptionsBuilder>{};

      myFactory.createPie = function(histogramData,chartTitle:string,
                                     fields:ui.IChartFieldDisplayProperties[],
                                     seriesColors:string[]) :ui.IChartHelper{
        return KendoChartOptionsBuilder2.createPie(histogramData,chartTitle,fields,seriesColors);
      };

      myFactory.createBarChart = function(histogramData,chartTitle:string,
                                          fields:ui.IChartFieldDisplayProperties[],
                                          gap) :ui.IChartHelper{
        return KendoChartOptionsBuilder2.createBarChart(histogramData,chartTitle,fields,gap);
      };
      myFactory.createColumnChart = function(stackedHistogramData,chartTitle:string,
                                             fields:ui.IChartFieldDisplayProperties[],
                                             seriesColors:string[]) :ui.IChartHelper{
        return KendoChartOptionsBuilder2.createColumnChart(stackedHistogramData,chartTitle,fields,seriesColors);
      };
      myFactory.createStackedBarChart = function(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                                                 fields:ui.IChartFieldDisplayProperties[],
                                                 seriesColors:string[]):ui.IChartHelper{
        return KendoChartOptionsBuilder2.createStackedBarChart(stackedHistogramData,chartTitle,fields,seriesColors);
      };


      return  myFactory
    });

class KendoChartOptionsBuilder2
//implements  ui.IChartOptionsBuilder
{

  static createPie(histogramData,chartTitle:string,
                   fields:ui.IChartFieldDisplayProperties[],
                   seriesColors:string[]):ui.IChartHelper

  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var categoryPercentageLabelTemplate = "#= category #\n#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) # ";
    var categoryLabelTemplate = "#= category #\n #= kendo.toString(value, \"N0\")#";
    var tooltipTemplate =tooltipField? "# if (dataItem."+tooltipField.fieldName+"!=null && dataItem."+tooltipField.fieldName+"!='') {# " +
    "#: dataItem."+tooltipField.fieldName+" # # } else {# #= category #<br />#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) #  #}#" :null;
    var mainOptions = {
      //chartArea: {
      //  width: 420,
      //  height: 420
      //},
      dataSource: {
        data: histogramData,
        sort: {
          field: seriesField.fieldName,
          dir: seriesField.dir
        }
      },
      //    seriesClick: onSeriesClick,
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:false,
        position: "top"
      },
      seriesDefaults: {
        type: "pie",
        startAngle: 315,
        labels: {
          visible: true,

          template: categoryPercentageLabelTemplate,
          background: "transparent",
          border: {
            color: "transparent",
            width: 0
          },
        }
      },
      seriesColors:seriesColors,
      transitions: true,
      series: [{
        field: seriesField.fieldName,
        name: seriesField.title,
        categoryField: categoryField.fieldName,
        labels: {
          visible: true,
          color: function(e){
            var theItem = e.series.data.filter(s=> e.dataItem.uid == s.uid)[0];
            return theItem&& theItem.labelUserColor?theItem.labelUserColor:'black';
          },
          font: function(e){
            var theItem = e.series.data.filter(s=> e.dataItem.uid == s.uid)[0];
            return theItem&& theItem.labelUserColor?'16px':null;
          }
        }

      }],
      categoryAxis: {
        line: {
          visible: false
        },
        field: categoryField.fieldName,
        labels: {
          //rotation: -90
          format: categoryField.format,
        },
        majorGridLines: {
          visible: false
        }
      },
      valueAxis: {
        visible:false,
        labels: {
          format: seriesField.format
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
        color: "white",
        // format: "{0}%",
        template:tooltipTemplate
      }
    };

    var result = new KendoChartHelper(mainOptions);
    return result;

  };

  static createBarChart(histogramData,chartTitle:string,
                        fields:ui.IChartFieldDisplayProperties[],
                        gap):ui.IChartHelper
  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var categoryPercentageLabelTemplate = "#= category #\n#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) # ";
    var categoryLabelTemplate = "#= category #\n #= kendo.toString(value, \"N0\")#";
    var tooltipTemplate =tooltipField? "# if (dataItem."+tooltipField.fieldName+"!=null && dataItem."+tooltipField.fieldName+"!='') {# " +
    "#: dataItem."+tooltipField.fieldName+" # # } else {# #= category #<br />#= kendo.toString(value, \"N0\")# , #= kendo.format('{0:P}', percentage) #  #}#"
        :null;
    var mainOptions = {
      dataBound: function (e) {
      },
      dataSource: {
        data: histogramData,

        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:false,
        position: "top"
      },
      seriesDefaults: {
        type: "bar",
        gap: gap,
        color:"#4F6990",
        labels: {
          visible: true,
          format: seriesField.format,
          background: "transparent"
        }
      },
      series: [{
        field: seriesField.fieldName,
        name: seriesField.title,
        colorField: "userColor",
        labels: {
          visible: true,
          color: function(e){
            return e.index&& e.series.data[e.index].labelUserColor?e.series.data[e.index].labelUserColor:'#000';
          },
          font: function(e){
            return e.index&& e.series.data[e.index].labelUserColor?'16px':null;
          }
        }
      }],
      categoryAxis: {
        line: {
          visible: false
        },
        field: categoryField.fieldName,
        labels: {
          visible: true,

        },
        majorGridLines: {
          visible: false
        }
      },
      valueAxis: {
        visible:false,
        labels: {
          format: categoryField.format
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
        format: "N0",
        color: "white",
      },

    };

    var result = new KendoChartHelper(mainOptions);
    return result;

  };

  static createColumnChart(stackedHistogramData,chartTitle:string,
                           fields:ui.IChartFieldDisplayProperties[],
                           seriesColors:string[]):ui.IChartHelper
  {
    var mainOptions = {
      dataSource: {
        data: stackedHistogramData,
        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:true,
        position: "top"
      },
      seriesDefaults: {
        type: "column",
        //   stack: true,
        //   gap:0.2,

        labels: {
          visible: true,
          position:'center',
          format: "N0",
          background: "transparent",

        }
      },
      series:stackedHistogramData.series,
      valueAxis: {
        visible:false,
        labels: {
          format: "N0"
        },
        //  majorUnit: 100,
        line: {
          visible: false
        }
      },
      categoryAxis: {
        categories:stackedHistogramData.categories,
        majorGridLines: {
          visible: false
        }
      },
      tooltip: {
        visible: true,
        template: "#= series.name #: #= value #"
      }
    };

    var result = new KendoChartHelper(mainOptions);
    return result;
  };

  static createStackedBarChart(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                               fields:ui.IChartFieldDisplayProperties[],
                               seriesColors:string[]):ui.IChartHelper
  {
    var seriesField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isSeriesField; })[0];
    var categoryField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isCategoryField; })[0];
    var tooltipField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isToolTipField; })[0];
    var valueField:ui.IChartFieldDisplayProperties =  fields.filter(function (f) { return f.isValueField; })[0];

    var mainOptions = {
      dataSource: {
        data: stackedHistogramData,
        //sort: {
        //  field: "name", //'10-100'
        //  dir: "asc"
        //}
      },
      title: {
        visible:false,
        text: chartTitle
      },
      legend: {
        visible:true,
        position: "top",

      },
      seriesDefaults: {
        type: "bar",
        stack: true,
        //stack: {
        //  type: "100%"
        //},

        gap:0.2,
        spacing: 0.1,
        labels: {
          visible: true,
          position:'center',
          rotation: 90,
          format: seriesField.format,
          background: "transparent",
       //   template:'# if(value>0){# #= value # # } #',

        },

      },
      series:stackedHistogramData.series,
      valueAxis: {
        min: 0,
        visible:true,
        labels: {
          format: seriesField?seriesField.format:valueField.format,
          visible:true
        },
        //  majorUnit: 100,
        line: {
          visible: false
        },

      },
      categoryAxis: {
      //  maxDateGroups:30,
        //baseUnitStep: "auto",
        //baseUnit: "fit",
        //autoBaseUnitSteps: {
        //  days: [3]
        //},
        labels: {
          visible:true,
          //dateFormats: {
          //  days:"M-d"
          //},
          //  format:categoryField.format

        },
        //  type: "date",
        categories:stackedHistogramData.categories.map(c=>c.name),
        majorGridLines: {
          visible: true
        },
        title:{
         // text:'ddf43434dffff'
        }
      },
      tooltip: {
        color:'#fff',
        visible: true,
        format: seriesField?seriesField.format:valueField.format,
        template: "#= series.name #: #= kendo.toString(value, '"+seriesField.format+"') #"
      }
    };

    var result = new KendoChartHelper(mainOptions);
    return result;


  };
};


class KendoChartDataBuilder2
{
  static castToStackedData=function(categories:any[],series:any[],seriesData:Dashboards.DtoItemCounter<any>[],findSeriesValueByFun:(seriesData,uniqueSeries,uniqueCat)=>number, propertiesUtils) {
    var uniqueCategories = <any[]>propertiesUtils.unique(categories);
    var uniqueSerieses = <any[]>propertiesUtils.unique(series);
    var series = [];
    var seriesValues:number[] = [];
    uniqueSerieses.forEach(uniqueSeries=> {
      seriesValues = [];
      uniqueCategories.forEach(uniqueCat=> {
        var seriesValue = findSeriesValueByFun(seriesData, uniqueSeries, uniqueCat);
        seriesValues.push(seriesValue)
      });
      series.push({name: uniqueSeries, data: seriesValues});
    });

    var stackedHistogramData = {categories: uniqueCategories, series: series};
    return stackedHistogramData;
  }
}

// ///<reference path='../all.d.ts'/>
//
// 'use strict';
// //
// //{"facets":[
// //  { "name":"numOfFiles",
// //    "type":"number",
// //    "slider": true,
// //    "fields":
// //      [
// //        {"value":"4","size": "104"},
// //        {"value":"56","size": "140"},
// //        {"value":"198","size": "130"},
// //        {"value":"3","size": "1540"}
// //
// //      ]
// //  },
// //  { "name":"entities",
// //    "type":"string",
// //    "slider": true,
// //    "fields":
// //      [
// //        {"value":"Customers","size": "104"},
// //        {"value":"Departments","size": "140"},
// //        {"value":"Employess","size": "130"},
// //        {"value":"Vendors","size": "1540"}
// //
// //      ]
// //  }
// //]
// //}
//
//
//
// module ui {
//   export class DAFacetedSearchOptionsBuilder implements ui.IFacetedSearchOptionsBuilder {
//
//     createFacetSearch(fields:ui.IFacetedSearchFieldDisplayProperties[],
//                       data:Entities.DTOFacetedSearchField[]) {
//
//       var facets:any = [];
//       fields.forEach(theField => {
//         var theData = data.filter(d=> d.fieldName == theField.fieldName)[0];
//         if (theData) {
//           var newField = this.createField(theData, theField);
//
//           facets.push(newField);
//         }
//       });
//
//
//       return facets;
//
//     }
//
//     private createField(theData:Entities.DTOFacetedSearchField, theField:ui.IFacetedSearchFieldDisplayProperties) {
//       var newField:ui.IFacetedSearchField =
//       {
//         fieldName: theField.fieldName,
//         title: theField.fieldName,
//         scrollbar: theField.scrollbar,
//         type: theField.type,
//         fieldType:theField.fieldType,
//         range: theData.range,
//         fields: []
//       };
//       this.setFieldsFromData(theData, newField);
//       return newField;
//     }
//
//     private setFieldsFromData(theData:Entities.DTOFacetedSearchField, newField:ui.IFacetedSearchField) {
//       theData.values.forEach(v=> {
//         newField.fields.push(< ui.IFacetedSearchFieldValue>
//           {
//             value: v.value,
//             numberOfItems: v.numberOfItems
//           }
//         );
//       })
//     }
//
//
//   }
//   ;
// }
//

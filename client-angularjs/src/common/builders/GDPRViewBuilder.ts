  ///<reference path='../../common/all.d.ts'/>

  'use strict';

angular.module('ui.builder.html.GDPR', [
])
    .factory('GDPRSplitViewHtmlPartsFactory', function() {
      var myFactory=<any>{};

      myFactory.createLeft=function(entityDisplayTypes:EntityDisplayTypes,isRestrictedEntityTypeId:boolean,context?:string)
      {
        if(entityDisplayTypes == EntityDisplayTypes.folders) {
          var type = (context?context+'-':'')+'folders-tree';
          var foldersTree = '<'+type+' style="overflow:auto;"  item-selected="onSelectLeft"   get-current-url="getLeftUrl" get-current-filter="getLeftFilter" change-selected-item="changeSelectedItemLeft" ' +
                  'reload-data-to-tree="reloadDataLeft" entity-type-id="leftEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" find-id="findIdLeft"  refresh-data="refreshDataLeft" items-selected="onSelectItemsLeft" sort-data="sortLeft" find-by-path="findByLeft" ' +
                  ' filter-data="filterLeft"  lazy-first-load="lazyFirstLoadLeft"></'+type+'>'
              ;
          return foldersTree;
        }
        var elementType=entityDisplayTypes == EntityDisplayTypes.doc_types?"tree-list": "grid";
        var type =(context?context+'-':'')+ entityDisplayTypes.toString().replace('_','-')+'-'+elementType;

        var  grid = '<'+type+' style="height:100%"  item-selected="onSelectLeft" items-selected="onSelectItemsLeft" template-view="!changeGridView"' +
            ' filter-data="filterLeft" entity-type-id="leftEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" sort-data="sortLeft" reload-data-to-grid="reloadDataLeft" refresh-data="refreshDataLeft" ' +
            'get-current-url="getLeftUrl" get-current-filter="getLeftFilter" unselect-all="unselectLeft"  lazy-first-load="lazyFirstLoadLeft" local-data-bound="onLocalDataBound"' +
            ' export-to-pdf="exportLeftToPdf" find-id="findIdLeft" export-to-excel="exportLeftToExcel"  page-changed="pageLeftChanged" change-page="changeLeftPage" change-selected-item="changeSelectedItemLeft"   process-export-finished="exportLeftToExcelCompleted">' +
            ' </'+type+'>';

        return grid;
      }

      myFactory.createRight=function(entityDisplayTypes:EntityDisplayTypes,
                                     leftType:EntityDisplayTypes,isRestrictedEntityTypeId:boolean,context?:string) {

        if(entityDisplayTypes == EntityDisplayTypes.folders) {
          var type = (context?context+'_':'')+'folders-tree';
          var foldersTree = '<'+type+' style="overflow:auto;"  restrictedEntityDisplayTypeName="'+leftType+'" reload-data-to-tree="reloadDataRight" refresh-data="refreshDataRight" include-sub-entity-data="includeSubEntityDataLeft" ' +
              'get-current-url="getRightUrl" entity-type-id="rightEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" get-current-filter="getRightFilter" change-selected-item="changeSelectedItemRight" fully-contained-filter-state="includeFullyContainedDataLeft" ' +
              'item-selected="onSelectRight" find-id="findIdRight" items-selected="onSelectItemsRight" lazy-first-load="lazyFirstLoadRight"  restricted-entity-id="selectedLeftId" find-by-path="findByRight"></folders-tree>';


          return foldersTree;
        }
        var elementType=entityDisplayTypes == EntityDisplayTypes.doc_types?"tree-list": "grid";
        var type = (context?context+'_':'')+entityDisplayTypes.toString().replace('_','-')+'-'+elementType;

        var grid = '<'+type+' style="height:100%" restrictedEntityDisplayTypeName="'+leftType+'" restricted-entity-id="selectedLeftId" item-selected="onSelectRight"  template-view="!changeGridView" items-selected="onSelectItemsRight"' +
            ' filter-data="filterRight" entity-type-id="rightEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" collapsed-data="collapsedDataRight"   sort-data="sortRight" lazy-first-load="lazyFirstLoadRight" total-elements="totalElementsRight"  '
            +' export-to-pdf="exportRightToPdf" export-to-excel="exportRightToExcel" include-sub-entity-data="includeSubEntityDataLeft"  fully-contained-filter-state="includeFullyContainedDataLeft" get-current-url="getRightUrl" get-current-filter="getRightFilter" ' +
            'reload-data-to-grid="reloadDataRight" find-id="findIdRight" refresh-data="refreshDataRight" page-changed="pageRightChanged" change-page="changeRightPage" change-selected-item="changeSelectedItemRight" process-export-finished="exportRightToExcelCompleted" >' +
            ' </'+type+'>';

        return grid;
      }

      return  myFactory
    })
    .factory('GDPRViewBuilderHelper', function($filter,configuration:IConfig,fileTagsResource:IFileTagsResource,$q,userSettings:IUserSettingsService) {


      var recToAppliedTags={},appliedToRecTags={};
      var tagIconsById ={},displayedTagTypeIds=[];
      var fileIrrelevantTagId,fileRelevantTagId;
      var workflowTagTypeId,workflowStateTagTypeId,confidenceLevelTagTypeId,actionTagTypeId,recommedationTagTypeId,relevancyTagTypeId;

      function prepareGDPRConfigurationFromFile(GDPRRawConfig:IConfig, successCallback,errorCallback) {
        let tagTypeNameToTagTypeDic={};
        let localConfig = GDPRRawConfig?GDPRRawConfig:configuration;
        let getTagTypeIdBy = function(configuredString:string)
        {
          if (configuredString) {
            var tagType = tagTypeNameToTagTypeDic[configuredString.toLowerCase()];
            return tagType.id;
          }
          return null;
        }
        let setConfigToServer = function()
        {
          let GDPRRawConfig = {
              GDPR_workflow_tagType_id:configuration.GDPR_workflow_tagType_id,
              GDPR_recommended_action_tagType_id:configuration.GDPR_recommended_action_tagType_id,
              GDPR_confidence_level_tagType_id:configuration.GDPR_confidence_level_tagType_id,
              GDPR_action_tagType_id:configuration.GDPR_action_tagType_id,
              GDPR_workflow_state_tagType_id:configuration.GDPR_workflow_state_tagType_id,
              GDPR_file_relevancy_tagType_id:configuration.GDPR_file_relevancy_tagType_id,
              GDPR_action_tag_id_icon:configuration.GDPR_action_tag_id_icon,
              GDPR_confidence_tag_id_icon:configuration.GDPR_confidence_tag_id_icon,
              GDPR_filter_panel_tagType_Ids:configuration.GDPR_filter_panel_tagType_Ids,
              GDPR_file_relevant_tag_id:configuration.GDPR_file_relevant_tag_id,
              GDPR_file_irrelevant_tag_id:configuration.GDPR_file_irrelevant_tag_id,
          }

            let GDPRConfig = {
              fileRelevantTagId: fileRelevantTagId,
              fileIrrelevantTagId: fileIrrelevantTagId,
              displayedTagTypeIds: displayedTagTypeIds,
              tagIconsById: tagIconsById,
              recToAppliedTags: recToAppliedTags,
              appliedToRecTags: appliedToRecTags,
              workflowTagTypeId: workflowTagTypeId,
              confidenceLevelTagTypeId: confidenceLevelTagTypeId,
              recommedationTagTypeId: recommedationTagTypeId,
              actionTagTypeId: actionTagTypeId,
              relevancyTagTypeId: relevancyTagTypeId,
              workflowStateTagTypeId: workflowStateTagTypeId,
            };
            GDPRConfig['raw'] = GDPRRawConfig;

          userSettings.setGDPRTagsConfig(GDPRConfig);
        }

        fileTagsResource.getAllTags(function (fileTagLists:Entities.DTOFileTagTypeAndTags[]) {

            fileTagLists.forEach(f=> {
              tagTypeNameToTagTypeDic[f.fileTagTypeDto.name.toLowerCase()] = f.fileTagTypeDto;
            });
            let actionTagTypeID = getTagTypeIdBy(localConfig.GDPR_action_tagType_id);
            let recommandedTagTypeID = getTagTypeIdBy(localConfig.GDPR_recommended_action_tagType_id);
            let actionTags = fileTagLists.filter(f=> f.fileTagTypeDto.id == actionTagTypeID)[0].fileTags;
            let recommandedTags = fileTagLists.filter(f=> f.fileTagTypeDto.id == recommandedTagTypeID)[0].fileTags;
            let actionTagIcons = JSON.parse(localConfig.GDPR_action_tag_id_icon);
            actionTags.forEach(a=> {
              var rec = recommandedTags.filter(r=>r.name.toLowerCase() == a.name.toLowerCase())[0]
              recToAppliedTags[rec.id] = a.id;
              appliedToRecTags[a.id] = rec.id;
              tagIconsById[a.id] = actionTagIcons[a.name.toLowerCase()];
            });
            let confidenceTagIcons = JSON.parse(localConfig.GDPR_confidence_tag_id_icon);
            let confidenceTagTypeID = getTagTypeIdBy(localConfig.GDPR_confidence_level_tagType_id);
            let confidenceTags = fileTagLists.filter(f=> f.fileTagTypeDto.id == confidenceTagTypeID)[0].fileTags;
            confidenceTags.forEach(a=> {
              tagIconsById[a.id] = confidenceTagIcons[a.name.toLowerCase()];
            });

            let filterPanelTagTypeNames = JSON.parse(localConfig.GDPR_filter_panel_tagType_Ids);

            filterPanelTagTypeNames.forEach(a=> {
              let tagType = tagTypeNameToTagTypeDic[a.toLowerCase()];
              displayedTagTypeIds.push(tagType.id);
            });

            let relevancyTagTypId = getTagTypeIdBy(localConfig.GDPR_file_relevancy_tagType_id);
            let relevancyTags = fileTagLists.filter(f=> f.fileTagTypeDto.id == relevancyTagTypId)[0].fileTags;
            fileIrrelevantTagId = relevancyTags.filter(f=> f.name.toLowerCase() == localConfig.GDPR_file_irrelevant_tag_id.toLowerCase())[0].id;
            fileRelevantTagId = relevancyTags.filter(f=> f.name.toLowerCase() == localConfig.GDPR_file_relevant_tag_id.toLowerCase())[0].id;

            workflowTagTypeId=getTagTypeIdBy(configuration.GDPR_workflow_tagType_id);
            confidenceLevelTagTypeId= getTagTypeIdBy(configuration.GDPR_confidence_level_tagType_id);
            recommedationTagTypeId= getTagTypeIdBy(configuration.GDPR_recommended_action_tagType_id);
            actionTagTypeId= getTagTypeIdBy(configuration.GDPR_action_tagType_id);
            relevancyTagTypeId= getTagTypeIdBy(configuration.GDPR_file_relevancy_tagType_id);
            workflowStateTagTypeId= getTagTypeIdBy(configuration.GDPR_workflow_state_tagType_id);

            setConfigToServer();
            successCallback();
          },
          function () {
            errorCallback();
          });

      }
      var myFactory=<ui.IGDPRViewBuilderHelper>{};
      myFactory.initTagTypes = function()
      {
        if (!fileRelevantTagId) {
          let defer = $q.defer();

          userSettings.getGDPRTagsConfig(function(GDPRConfig){
            if(GDPRConfig && GDPRConfig.fileRelevantTagId)
            {
                fileRelevantTagId = GDPRConfig.fileRelevantTagId;
                fileIrrelevantTagId = GDPRConfig.fileIrrelevantTagId;
                displayedTagTypeIds = GDPRConfig.displayedTagTypeIds;
                tagIconsById = GDPRConfig.tagIconsById;
                recToAppliedTags = GDPRConfig.recToAppliedTags;
                appliedToRecTags = GDPRConfig.appliedToRecTags;
                workflowTagTypeId = GDPRConfig.workflowTagTypeId;
                confidenceLevelTagTypeId = GDPRConfig.confidenceLevelTagTypeId;
                recommedationTagTypeId = GDPRConfig.recommedationTagTypeId;
                actionTagTypeId = GDPRConfig.actionTagTypeId;
                relevancyTagTypeId = GDPRConfig.relevancyTagTypeId;
                workflowStateTagTypeId = GDPRConfig.workflowStateTagTypeId;

                defer.resolve('Set config from DB');
            }
            else {
              prepareGDPRConfigurationFromFile(GDPRConfig?GDPRConfig.raw:null,
                function(){ defer.resolve('Initialize config from config file');},
                function(){ defer.reject('Oops... something went wrong');});
            }
          });
          return defer.promise; //promise to initialize config
        }
        else { //configuration already initialized
          return true;
        }
      }
      myFactory.getConfidenceLevelTagTypeId = function()
      {
        return confidenceLevelTagTypeId;
      }
      myFactory.getRecommedationTagTypeId = function()
      {
        return recommedationTagTypeId;
      }
      myFactory.getActionTagTypeId = function()
      {
        return actionTagTypeId;
      }
      myFactory.getWorkflowTypeId = function()
      {
        return workflowTagTypeId;
      }
      myFactory.getWorkflowStateTypeId = function()
      {
        return workflowStateTagTypeId;
      }
      myFactory.getRecToAppliedTags = function()
      {
            return recToAppliedTags;
      }
      myFactory.getAppliedToRecTags = function()
      {
        return appliedToRecTags;
      }
      myFactory.getTagIconsById = function()
      {
        return tagIconsById;
      }
      myFactory.getDisplayedFilterPanelTagTypeIds = function()
      {
        return displayedTagTypeIds;
      }
      myFactory.getFileRelevantTagId = function()
      {
        return fileRelevantTagId;
      }
      myFactory.getFileIrrelevantTagId = function()
      {
        return fileIrrelevantTagId;
      }
      myFactory.parseFilesResolveStatus = function (taggableItem:Entities.ITaggable,fileTagDtos:Entities.DTOAggregationCountItem< Entities.DTOFileTag>[]) {
        if(fileTagDtos &&fileTagDtos[0]) {

          (<any>taggableItem).progressState =(fileTagDtos.map(f=>f.count).reduce(function(a, b){ return a + b; })/(<any>taggableItem['allGDPRFiles']))*100 ;
          (<any>taggableItem).progressState=(<any>taggableItem).progressState<=100?(<any>taggableItem).progressState:100;
          var tagList =       (<any>taggableItem).fileTagDtos.map(t =>  t.name +'('+ t.count+')').join('; ') ;
          (<any> taggableItem).progressStateAsFormattedString = tagList;
          return taggableItem;
        }
        else {
          (<any>taggableItem).progressState  =Math.floor( Math.random()*100);
        }
        return null;
      };

      myFactory.parseTagsLeaveOnlyRelevantTags = function (taggableItem:Entities.ITaggable) {

        var tagTypeIdsToExclude = [ actionTagTypeId,
          confidenceLevelTagTypeId,  relevancyTagTypeId,  recommedationTagTypeId  ,workflowTagTypeId]
        if(taggableItem.fileTagDtos) {
          taggableItem.fileTagDtos = taggableItem.fileTagDtos.filter(f=>!tagTypeIdsToExclude.filter(e=> e==f.type.id)[0] );
          return taggableItem;
        }

          return null;
      };

      myFactory.setFilesResolveStatusTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {
        tagsField.template =
            '<div style="display: inline-block;">'+ 'Progress #: item.progressState #%'+
         ' <div class="simple-progress-container" style="margin-top: 10px;"><div class="simple-progressbar" style="width:#: item.progressState #%"></div></div></div>'//+
            //'<div style="display: block;"><i class="btn btn-xs btn-flat ng-hide1" ng-click="open(dataItem)" title="Open"><span class="fa fa-sign-in" > </span>&nbsp;Open group</i></div>' ;


        scope.open= function(dataItem) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGDPRGridProgressUserAction, {
            action: 'open',
            itemId: dataItem.id,

          });
        }
        var title='Completion rate';
        var progerssBar =   ' <div class="simple-progress-container" style="margin-top: 10px;"><div class="simple-progressbar" style="width:#: item.progressState #%"></div></div>';
        var template =  '<div class="title-third"  style1="line-height: 26px;"><span translate="{{\''+title+'\'}}"></span>:</div>' +
          '<div class="title-secondary"  title="Resolve Progress">'+progerssBar+'</div>';
        tagsField.template =  "<div class='centerText' >"+template+"</div>";
        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }
      };
      myFactory.parseGDPRRecommendation = function (taggableItem:Entities.ITaggable) {
        if(taggableItem.fileTagDtos) {
          var appliedToRecTags =myFactory.getAppliedToRecTags();

          (<any>taggableItem).GPDRActions = taggableItem.fileTagDtos.filter(f=>f.type.id == actionTagTypeId && !f.implicit);
          if(!(<any>taggableItem).GPDRActions[0]) {
            (<any>taggableItem).GPDRActions = taggableItem.fileTagDtos.filter(f=>f.type.id == actionTagTypeId);
          }
          if((<any>taggableItem).GPDRActions[0]) {
            (<any>taggableItem).GPDRActions = (<any>taggableItem).GPDRActions.slice((<any>taggableItem).GPDRActions.length - 1);
          }
          (<any>taggableItem).GPDRRecommendedActions = taggableItem.fileTagDtos.filter(f=> f.type.id == recommedationTagTypeId );
          (<any>taggableItem).GPDRRecommendedActionsNotApplyed = taggableItem.fileTagDtos.filter(f=>{
                 return f.type.id == recommedationTagTypeId&&   !(<any>taggableItem).GPDRActions.filter(a=>f.id == appliedToRecTags[a.id])[0];
                });

          var tagList =    (<any>taggableItem).GPDRRecommendedActionsNotApplyed ?  ' Recommended actions:'+  (<any>taggableItem).GPDRRecommendedActionsNotApplyed.map(t => (<Entities.DTOFileTag>t).name ).join('; ') : '';
          (<any> taggableItem).resolvedActionTagsAsFormattedString =   (<any>taggableItem).GPDRActions ?   ' Resolved actions:'+ (<any>taggableItem).GPDRActions.map(t => (<Entities.DTOFileTag>t).name ).join('; ') : '';
          (<any> taggableItem).GPDRRecommendedActionsNotApplyedTagsAsFormattedString = tagList;
          return taggableItem;
        }
        return null;
      };
      myFactory.setGDPRRecommendationTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {

        var tagNameTemplate= "<small>#: item.GPDRRecommendedActionsNotApplyed[i].name # <i class='btn btn-flat btn-xs ng-hide' ng-click='applyGDPRRecommendation(dataItem,#: item.GPDRRecommendedActionsNotApplyed[i].id #)'>Apply</i></small>";
        var tagsTooltip ='<ul class="dropdown-menu"><li><a>Apply for High confidence files only</a></li>' +
            '<li><a>Apply for Medium confidence files only</a></li>' +
            '<li><a>Apply for Low confidence files only</a></li></ul>';

        var closeAllDdls=function()
        {
          $('html').trigger('click'); //to close all ddls
        };

        scope.applyGDPRRecommendation= function(dataItem:any,tagIdToApply) {
          var tags = dataItem.item?(<any>dataItem.item).GPDRRecommendedActionsNotApplyed:<any>dataItem.GPDRRecommendedActionsNotApplyed;
          var tagToApply:Entities.DTOFileTag = tags.filter(t=> t.id==tagIdToApply)[0];
          closeAllDdls();
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGDPRGridTagsUserAction, {
            action: 'applyRecommendedAction',
            items: [dataItem],
            tagToApply:tagToApply,
            displayType:scope.entityDisplayType
          });
        }

        tagsField.template = '#if (  item.GPDRRecommendedActionsNotApplyed && item.GPDRRecommendedActionsNotApplyed.length>0) {# <div title="Recommendations not yet applied" class="dropdown dropdown-wrapper  " >' +
            '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title"> <small>Recommendation:</small> ' +
            ' # for (var i=0; i < item.GPDRRecommendedActionsNotApplyed.length; i++) { # ' +
                tagNameTemplate+ ' # } #    ' +
            '</a></span>' + tagsTooltip+'</div># } #';

        var title = 'Action recommendation';
        var firstRecommendation =  '#if (  item.GPDRRecommendedActionsNotApplyed && item.GPDRRecommendedActionsNotApplyed.length>0) {# <span>#: item.GPDRRecommendedActionsNotApplyed[0].name # </span> # } #  ';
        tagsField.template = '<div class="centerText"><div class="title-third "  ><span translate="{{\''+title+'\'}}"></span>:</div>' +
          '<div class="title-secondary"  title="Recommendation">'+firstRecommendation+'</div></div>';


        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }
      };
      myFactory.setGDPRActionTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {

        var tagNameTemplate= "<span class='title-primary'><span class='{{getTagIcon(#: item.GPDRActions[i].id #)}}'></span> #: item.GPDRActions[i].name # </span>";
        var tagsTooltip ='<ul class="dropdown-menu"><li> #if (  !item.GPDRActions[i].implicit) {#<a ng-click="removeActionTag(dataItem, #: item.GPDRActions[i].id #  )"><span class="fa fa-times " ></span> Remove</a> #} else {# <a>Cannot be deleted </a># } #</li></ul>';

        var closeAllDdls=function()
        {
          $('html').trigger('click'); //to close all ddls
        };

        scope.getTagIcon = function(tagID)
        {
          var tagIconsById = myFactory.getTagIconsById();
          return tagIconsById[tagID];
        }
        scope.removeActionTag= function(dataItem,tagIDToRemove:number) {
          var tags = dataItem.item?(<any>dataItem.item).GPDRActions:<any>dataItem.GPDRActions;
         var fileTagToRemove:Entities.DTOFileTag = tags.filter(t=> t.id==tagIDToRemove)[0];
          closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGDPRGridTagsUserAction, {
            action: 'removeActionTag',
            item: dataItem,
            tagToRemove: fileTagToRemove,
            displayType: scope.entityDisplayType
          });
        }

        tagsField.template = '#if (  item.GPDRActions && item.GPDRActions[0]) {#'+
            ' # for (var i=0; i < item.GPDRActions.length; i++) { # ' +
                  '<span title="Applied GDPR Actions" class="dropdown dropdown-wrapper  " >' +
                   '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +
                    tagNameTemplate+ '    ' +
                    '<i class="fa fa-caret-down ng-hide" style="position: absolute;top: 25%;RIGHT: -14px;" ></i></a></span>' + tagsTooltip+'</span># } # # } #';

        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }
      };

      myFactory.parseConfidenceLevel = function (taggableItem:Entities.ITaggable) {
        if(taggableItem.fileTagDtos) {

          (<any>taggableItem).confidenceLevelTag = taggableItem.fileTagDtos.filter(f=>f.type.id == confidenceLevelTagTypeId)[0];

          return taggableItem;
        }
        return null;
      };
      myFactory.setConfidenceLevelTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {
        tagsField.template = '#if (  item.confidenceLevelTag) {# <span  class="confidence-level {{dataItem.item.confidenceLevelTag.name}}"  > ' +
        '<span class="{{getConfidenceIcon(dataItem.item.confidenceLevelTag)}}  " title="How much confidence there is that the search person is in this file " ></span> #: item.confidenceLevelTag.name #</span> #}#';

        scope.getConfidenceIcon = function(tag:Entities.DTOFileTag)
        {
            return tagIconsById[tag.id];
        }

        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }
      };
      myFactory.parseConfidenceLevelCounters = function (taggableItem:Entities.ITaggable,fileTagDtos:Entities.DTOAggregationCountItem< Entities.DTOFileTag>[]) {
        if(fileTagDtos) {

          (<any>taggableItem).confidenceLevelTags = fileTagDtos.filter(f=>f.item.type.id ==confidenceLevelTagTypeId).map(t=>{
            return {name:(<any>t).item.name,id:(<any>t).item.id,count: (<any>t).count}
          });

          var tagList =    (<any>taggableItem).confidenceLevelTags ?    (<any>taggableItem).confidenceLevelTags.map(t =>  t.name +'('+ t.count+')').join('; ') : '';
          (<any> taggableItem).confidenceLevelTagsAsFormattedString = tagList;
          return taggableItem;
        }
        return null;
      };

      myFactory.setConfidenceLevelCountersTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {
        var tagIconTemplate = '<span  ng-repeat="tag in dataItem.item.confidenceLevelTags" class="confidence-level {{tag.name}}"><span class="{{getConfidenceIcon(tag)}}  " title="How much confidence there is that the search person is in this file " ></span>&nbsp;{{tag.count}}&nbsp;&nbsp; </span>' ;
        var tagsTooltip ='<ul class="dropdown-menu"><li><a>Empty</a></li></ul>';

        scope.getConfidenceIcon = function(tag:Entities.DTOFileTag)
        {
          return tagIconsById[tag.id];
        }

        tagsField.template = '<span ng-if="dataItem.item.confidenceLevelTags"><span title="#: item.confidenceLevelTagsAsFormattedString #" class="dropdown dropdown-wrapper" >' +
            '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +

            tagIconTemplate+
            '<i class1="fa fa-caret-down ng-hide"  ></i></a></span>' + tagsTooltip+'</span></span>';

        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }

      };
      myFactory.parseIrrelevant = function (taggableItem:Entities.ITaggable) {
        if(taggableItem.fileTagDtos) {
          var relevancyTags = taggableItem.fileTagDtos.filter(t=> t.type.id == relevancyTagTypeId);
          if(relevancyTags[0])
          {
            var hasRelevantTag = relevancyTags.filter(t=>t.id ==myFactory.getFileRelevantTagId())[0];
            taggableItem['irrelevant'] = !hasRelevantTag;
          };

          return taggableItem;
        }
        return null;
      };
      myFactory.setIrrelevantTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
      {
        var tagIconTemplate= "<span ng-if='!dataItem.item.irrelevant' title='Remove this item from search results, mark as irrelevant'> <i class='btn btn-flat btn-xs ng-hide fa fa-times' ng-click='toggleRelevancy(dataItem )'></i></span>";
        tagIconTemplate+= "<span ng-if='dataItem.item.irrelevant' title='Restore this item files to search results'> <i class='btn btn-primary btn-xs ng-hide' ng-click='toggleRelevancy(dataItem )'>+ Restore</i></span>";


        var tagsTooltip = "<ul class='dropdown-menu' >"+

            " <li><a  ng-click='toggleRelevancy(dataItem)'>  '# if (  item.irrelevant ) {# Set as relevant #} else { # Remove from search # } #</a></li>"+
              //"<li><a  title='filter by this relevancy state' ng-click='filterByTag($event, dataItem , #: item.fileTagDtos[i].id #)'><span class='fa fa-filter '></span>  Filter by this</a></li>"+
            "</ul> ";

        var closeAllDdls=function()
        {
          $('html').trigger('click'); //to close all ddls
        };
        scope.toggleRelevancy = function(dataItem:any) {
          closeAllDdls();
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGDPRGridTagsUserAction, {
            action: 'toggleRelevancy',
            items: [dataItem],
            displayType: scope.entityDisplayType
          });
        };
        tagsField.template =  //'<span ng-if="dataItem.item.irrelevant" ><span class="fa fa-ban"></span> Removed</span>'+
            ' <span title="File marked as unrellevant for this search" class="" >' +
            '<span ><a class="ddl-no-title">' +tagIconTemplate+

            '<i class1="fa fa-caret-down ng-hide"  ></i></a></span></span>';


        if(noAggragetedDTo) {
          tagsField.template = tagsField.template.replace(/item./g, '');
        }
      };

      myFactory.getFilesAndFilterTemplate = function (numOfFilesFieldName,numOfFilteredFilesFieldName,inFolderPercentageFieldName)
      {

        var numOfFilesNewTemplate = numOfFilesFieldName? '#=' + numOfFilesFieldName+'#': '#=' + numOfFilteredFilesFieldName+'#';

        var filterTemplate=
          '<span style="font-size:12px" title="Total files: '+numOfFilesNewTemplate+ '"><span ng-if="(filterData&&filterData.filter)">' +
          '</span>'+ numOfFilesNewTemplate+ ' files</span> ';

        if(inFolderPercentageFieldName) {
          var percent = inFolderPercentageFieldName? '<span style="white-space: nowrap" ng-if="(filterData&&filterData.filter)||restrictedEntityId">#: kendo.toString('+inFolderPercentageFieldName+', "n0")# %</span>':'';
          filterTemplate += "<span style='line-height: 10px;'><small style='white-space: nowrap;'   class='fa fa-filter'></small><small style='white-space: nowrap'> " + percent + "</small></span>";
        }
        return filterTemplate;
      };
      return  myFactory
    });

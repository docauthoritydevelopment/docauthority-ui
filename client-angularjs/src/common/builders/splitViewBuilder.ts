///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('ui.builder.html.splitView', [])
  .factory('splitViewHtmlPartsFactory', function() {
    var myFactory=<any>{};

    myFactory.createLeft=function(entityDisplayTypes:EntityDisplayTypes,isRestrictedEntityTypeId:boolean) {
      if(entityDisplayTypes == EntityDisplayTypes.folders) {
        var foldersTree = '<folders-tree style="overflow:auto;" item-selected="onSelectLeft"   get-current-url="getLeftUrl" get-current-filter="getLeftFilter" change-selected-item="changeSelectedItemLeft" ' +
            'reload-data-to-tree="reloadDataLeft" entity-type-id="leftEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" find-id="findIdLeft"  refresh-data="refreshDataLeft" items-selected="onSelectItemsLeft" sort-data="sortLeft" find-by-path="findByLeft" ' +
            ' filter-data="filterLeft" filter-by-search-text="leftSearchText"  lazy-first-load="lazyFirstLoadLeft"></folders-tree>';
        return foldersTree;
      }
      var elementType = "grid";

      if (entityDisplayTypes == EntityDisplayTypes.doc_types || entityDisplayTypes == EntityDisplayTypes.departments || entityDisplayTypes == EntityDisplayTypes.matters) {
        elementType = "tree-list";
      }
      else if (entityDisplayTypes == EntityDisplayTypes.departments_flat) {
        entityDisplayTypes = EntityDisplayTypes.departments;
      }
      else if (entityDisplayTypes == EntityDisplayTypes.matters_flat) {
        entityDisplayTypes = EntityDisplayTypes.matters;
      }

      var type = entityDisplayTypes.toString().replace('_','-')+'-'+elementType;

      var  grid = '<'+type+' style="height:100%"  item-selected="onSelectLeft" items-selected="onSelectItemsLeft" template-view="!changeGridView" total-elements="totalElementsLeft" ' +
        ' filter-data="filterLeft" initial-load-params="initialLoadParamsLeft" entity-type-id="leftEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" sort-data="sortLeft" reload-data-to-grid="reloadDataLeft" refresh-data="refreshDataLeft" ' +
          'get-current-url="getLeftUrl" get-current-filter="getLeftFilter" unselect-all="unselectLeft"  lazy-first-load="lazyFirstLoadLeft" local-data-bound="onLocalDataBound"' +
          ' export-to-pdf="exportLeftToPdf" filter-by-search-text="leftSearchText"  find-id="findIdLeft" export-to-excel="exportLeftToExcel"  page-changed="pageLeftChanged" change-page="changeLeftPage" change-selected-item="changeSelectedItemLeft"   process-export-finished="exportLeftToExcelCompleted">' +
      ' </'+type+'>';

      return grid;
    };

    myFactory.createRight=function(entityDisplayTypes:EntityDisplayTypes,leftType:EntityDisplayTypes,isRestrictedEntityTypeId:boolean) {
      if(entityDisplayTypes == EntityDisplayTypes.folders) {
        var foldersTree = '<folders-tree style="overflow:auto;"  restrictedEntityDisplayTypeName="'+leftType+'" reload-data-to-tree="reloadDataRight" refresh-data="refreshDataRight" include-sub-entity-data="includeSubEntityDataLeft" ' +
          ' filter-data="filterRight" get-current-url="getRightUrl" entity-type-id="rightEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" get-current-filter="getRightFilter" change-selected-item="changeSelectedItemRight" fully-contained-filter-state="includeFullyContainedDataLeft" ' +
            'item-selected="onSelectRight" find-id="findIdRight" filter-by-search-text="rightSearchText" items-selected="onSelectItemsRight" lazy-first-load="lazyFirstLoadRight"  restricted-entity-id="selectedLeftId" find-by-path="findByRight"></folders-tree>';

        return foldersTree;
      }
      var elementType = "grid";
      if (entityDisplayTypes == EntityDisplayTypes.doc_types || entityDisplayTypes == EntityDisplayTypes.departments || entityDisplayTypes == EntityDisplayTypes.matters) {
        elementType = "tree-list";
      }
      else if (entityDisplayTypes == EntityDisplayTypes.departments_flat) {
        entityDisplayTypes = EntityDisplayTypes.departments;
      }
      else if (entityDisplayTypes == EntityDisplayTypes.matters_flat) {
        entityDisplayTypes = EntityDisplayTypes.matters;
      }

      var type = entityDisplayTypes.toString().replace('_','-')+'-'+elementType;

      var grid = '<'+type+' style="height:100%" restrictedEntityDisplayTypeName="'+leftType+'" restricted-entity-id="selectedLeftId" item-selected="onSelectRight"  template-view="!changeGridView" items-selected="onSelectItemsRight"' +
          ' filter-data="filterRight" entity-type-id="rightEntityTypeId" restricted-entity-type-id="'+isRestrictedEntityTypeId+'" initial-load-params="initialLoadParamsRight" collapsed-data="collapsedDataRight"   sort-data="sortRight" lazy-first-load="lazyFirstLoadRight" total-elements="totalElementsRight"  '
          +' export-to-pdf="exportRightToPdf" export-to-excel="exportRightToExcel" include-sub-entity-data="includeSubEntityDataLeft"  fully-contained-filter-state="includeFullyContainedDataLeft" get-current-url="getRightUrl" get-current-filter="getRightFilter" ' +
          'reload-data-to-grid="reloadDataRight" filter-by-search-text="rightSearchText"  find-id="findIdRight" refresh-data="refreshDataRight" page-changed="pageRightChanged" change-page="changeRightPage" change-selected-item="changeSelectedItemRight" process-export-finished="exportRightToExcelCompleted" >' +
          ' </'+type+'>';

      return grid;
    };

    return  myFactory;
  })

  .factory('splitViewBuilderHelper', function($filter,configuration:IConfig,userSettings:IUserSettingsService,propertiesUtils,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
    var myFactory=<ui.ISplitViewBuilderHelper>{};

    myFactory.parseMarkedItems = function (markableItems:any[],entityDisplayType:EntityDisplayTypes,element,gridOptions:ui.IGridHelper) {
      if(markableItems&& markableItems.length>0) {
        userSettings.getRecentlyUsedMarkedItems(function (savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[]) {
          if (savedActiveRecentlyUsedMarkedItems && savedActiveRecentlyUsedMarkedItems.length > 0) {
            var markedItemForEntityType = savedActiveRecentlyUsedMarkedItems.filter(r=>entityDisplayType.equals(r.entity));
            var savedActiveRecentlyUsedMarkedItemsDic = {};
            savedActiveRecentlyUsedMarkedItems.forEach(type=> savedActiveRecentlyUsedMarkedItemsDic[type.id] = type);
            if(markedItemForEntityType.length>0)
            {
               var data = gridOptions.getData(element);
              markableItems = data || [];

              markableItems.forEach(item=> {
               var id=  item['item.id']?item['item.id']:item['id'];
                (<any>item).marked = savedActiveRecentlyUsedMarkedItemsDic[id]!=null;
              });
            }
            return markableItems;
          }
        });
      }
    };

    myFactory.getMarkedItemTemplate = function (scope,eventCommunicator) {
      var markedItemIcon=  ' <span ng-if="dataItem.marked" class="'+configuration.icon_markedItem+' tag-3" ng-click="removeMarkedItem(dataItem)" bubble="Remove marking"> </span>';

      scope.removeMarkedItem = function(dataItem) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridMarkItemUserAction, {
          action: 'removeMarkItem',
          id: dataItem.id,
          entityDisplayType:scope.entityDisplayType,
        });
      };

     return markedItemIcon;
    };

    myFactory.parseTags = function (taggableItem:Entities.ITaggable) {
      if(taggableItem.fileTagDtos) {
        var assosFuncRoleIds = taggableItem.associatedFunctionalRoles.map(f=>f.id);
        var authorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagAssociation],assosFuncRoleIds);
        if (authorized ||((<any>taggableItem).deleteTagsAuthorized || (<any>taggableItem).assignTagsAuthorized) ) { //incase of parsed after already parsed, funcRole may not exists any more
          taggableItem.fileTagDtos.sort((t1, t2)=> {
            if (t1.type.name > t2.type.name) {
              return 1;
            }
            if (t1.type.name < t2.type.name) {
              return -1;
            }
            return 0;
          });

          taggableItem.fileTagDtos.forEach(t=> {
            (<any>t).styleId = myFactory.getTagStyleId(t);
            (<any>t).isExplicit = !t.implicit;
          });

          var tagList = taggableItem.fileTagDtos ? taggableItem.fileTagDtos.map(t => t.type.name + ':' + t.name + ((<any>t).isExplicit ? '' : ' (implicit)')).join('; ') : '';
          taggableItem.tagsAsFormattedString = tagList;
          if( !(<any>taggableItem).deleteTagsAuthorized && !(<any>taggableItem).assignTagsAuthorized) {
            (<any>taggableItem).deleteTagsAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignTag], assosFuncRoleIds);
            (<any>taggableItem).assignTagsAuthorized = (<any>taggableItem).deleteTagsAuthorized;
          }
          return taggableItem;
        }
        else {
          taggableItem.fileTagDtos=[];
          taggableItem.tagsAsFormattedString ="";
        }
      }
      return null;
    };



    myFactory.getMetadataStyleId = function(typeIndex:number) {
      var colors = parseInt(configuration.max_tags_colors);
      return typeIndex%colors;
    }

    myFactory.getTagStyleId = function(tag:Entities.DTOFileTag) {
      var colors = parseInt(configuration.max_tags_colors);
      if(tag.type.id) {
        return tag.type.id%colors;
      }
      return tag.type.id;
    };

    myFactory.getTagTypeStyleId = function(tag:Entities.DTOFileTagType) {
      var colors = parseInt(configuration.max_tags_colors);
      if (tag.id) {
        return tag.id % colors;
      }
      return tag.id;
    };

    myFactory.getDocTypeStyleId = function(docType) {
      var colors = parseInt(configuration.max_docTypes_colors);
      if(docType.parents&& docType.parents.length>0) {
        var result = docType.parents[0]%colors+1;
        return result;
      }
      return docType.id%colors+1;
    };

    myFactory.getDocTypeDepth = function(docType) {
      if (docType.parents && docType.parents.length > 0) {
        return docType.parents.length;
      }
      return 0;
    };

    myFactory.getDepartmentStyleId = function(department:Entities.DTODepartment) {
      var colors = parseInt(configuration.max_departments_colors);
      if(department.parentId) {
        return department.parentId%colors+1;
      }
      return department.id%colors+1;
    };

    myFactory.getDepartmentDepth = function(department) {
      if (department.parentId) {
        return 1;
      }
      return 0;
    };

    myFactory.getFunctionalRoleStyleId = function(funcRoleId:number) {
      var colors = parseInt(configuration.max_tags_colors);
      return funcRoleId % colors;
    };

    myFactory.parseFunctionalRoles = function (assigneeItem:Entities.IFunctionalRolesAssignee) {
      if (assigneeItem.associatedFunctionalRoles) {
        var authorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation]);
        if (authorized) {
          assigneeItem.associatedFunctionalRoles.sort( propertiesUtils.sortAlphbeticFun('name'));

          assigneeItem.associatedFunctionalRoles.forEach(t=> {
            (<any>t).isExplicit = !t.implicit;
            (<any>t).styleId = myFactory.getFunctionalRoleStyleId(t.id);
          });
          var tagList = assigneeItem.associatedFunctionalRoles ? assigneeItem.associatedFunctionalRoles.map(t =>  t.name + ((<any>t).isExplicit ? '' : ' (implicit)')).join('; ') : '';
          assigneeItem.functionalRolesAsFormattedString = tagList;
          (<any>assigneeItem).deleteFuncRolesAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignDataRole]);
          (<any>assigneeItem).assignFuncRolesAuthorized = (<any>assigneeItem).deleteFuncRolesAuthorized;
          return assigneeItem;
        }
        else {
          assigneeItem.associatedFunctionalRoles=[];
          assigneeItem.functionalRolesAsFormattedString ='';
        }
      }
      return null;
    };


    myFactory.parseDocTypes = function (assigneeItem:Entities.IDocTypesAuthorization) {
      if(assigneeItem.docTypeDtos) {
        var viewAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeAssociation],assigneeItem.associatedFunctionalRoles.map(f=>f.id));
        if (viewAuthorized || (assigneeItem.deleteDocTypesAuthorized || assigneeItem.assignDocTypesAuthorized)) {
          assigneeItem.docTypeDtos.sort((t1, t2)=> {
            if (t1.name > t2.name) {
              return 1;
            }
            if (t1.name < t2.name) {
              return -1;
            }
            return 0;
          });

          assigneeItem.docTypeDtos.forEach(t=> {
              (<any>t).isExplicit = !t.implicit;
              (<any>t).styleId = myFactory.getDocTypeStyleId(t);
              (<any>t).depth = myFactory.getDocTypeDepth(t);
            }
          );
          var tagList = assigneeItem.docTypeDtos ? assigneeItem.docTypeDtos.map(t => t.fullName + ((<any>t).isExplicit ? '' : ' (implicit)')).join('; ') : '';
          assigneeItem.docTypesAsFormattedString = tagList;
          if( !assigneeItem.deleteDocTypesAuthorized && !assigneeItem.assignDocTypesAuthorized) {
            assigneeItem.deleteDocTypesAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignDocType], assigneeItem.associatedFunctionalRoles.map(f => f.id));
            assigneeItem.assignDocTypesAuthorized = assigneeItem.deleteDocTypesAuthorized;
          }
          return assigneeItem;
        }
        else {
          assigneeItem.docTypeDtos=[];
          assigneeItem.docTypesAsFormattedString='';
        }
      }
      return null;
    };

    myFactory.parseDepartment = function (assigneeItem:Entities.IDepartmentAuthorization) {
      var viewAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentAssociation],assigneeItem.associatedFunctionalRoles.map(f=>f.id));
      if (viewAuthorized || (assigneeItem.deleteDepartmentAuthorized || assigneeItem.assignDepartmentAuthorized)) {
        if (assigneeItem.departmentDto) {
          (<any>assigneeItem.departmentDto).isExplicit = !assigneeItem.departmentDto.implicit;
          (<any>assigneeItem.departmentDto).styleId = myFactory.getDepartmentStyleId(assigneeItem.departmentDto);
          (<any>assigneeItem.departmentDto).depth = myFactory.getDepartmentDepth(assigneeItem.departmentDto);
        }
        else {
          assigneeItem.departmentDto = null;
        }

        assigneeItem.departmentAsFormattedString = assigneeItem.departmentDto ? ((<any>assigneeItem.departmentDto).isExplicit ?  assigneeItem.departmentDto.fullName : assigneeItem.departmentDto.fullName + ' (implicit)'): currentUserDetailsProvider.isInstallationModeLegal() ? 'Add Matter' : 'Add Department';

        if( !assigneeItem.deleteDepartmentAuthorized && !assigneeItem.assignDepartmentAuthorized) {
          assigneeItem.deleteDepartmentAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignDepartment], assigneeItem.associatedFunctionalRoles.map(f => f.id));
          assigneeItem.assignDepartmentAuthorized = assigneeItem.deleteDepartmentAuthorized;
        }
        return assigneeItem;
      }
      else {
        assigneeItem.departmentDto=null;
        assigneeItem.departmentAsFormattedString='';
      }
    };

    myFactory.parseAssociatedBizListItems = function (assigneeItem:Entities.IBizListAssociatable) {
      if(assigneeItem.associatedBizListItems) {
        var authorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],assigneeItem.associatedFunctionalRoles.map(f=>f.id));
        if (authorized) {
          assigneeItem.associatedBizListItems.sort((t1, t2)=> {
            if (t1.name > t2.name) {
              return 1;
            }
            if (t1.name < t2.name) {
              return -1;
            }
            return 0;
          });
          var getAssociationStateText = function (association:Entities.DTOSimpleBizListItem) {
            if (association.state == Entities.DTOSimpleBizListItem.state_ACTIVE) {
              return 'Active';
            }
            else if (association.state == Entities.DTOSimpleBizListItem.state_ARCHIVED) {
              return 'Archived';
            }
            else if (association.state == Entities.DTOSimpleBizListItem.state_PENDING) {
              return 'Pending';
            }
            else if (association.state == Entities.DTOSimpleBizListItem.state_DELETED) {
              return 'Deleted';
            }
            return '';
          };
          assigneeItem.associatedBizListItems.forEach(t=> {
              (<any>t).isExplicit = !t.implicit;
            }
          );

          var tagList = assigneeItem.associatedBizListItems ? assigneeItem.associatedBizListItems.map(t => t.name + ((<any>t).isExplicit ? '' : ' (implicit)') + (getAssociationStateText(t))).join('; ') : '';
          (<any>assigneeItem).associatedBizListAsFormattedString = tagList;
          (<any>assigneeItem).deleteBizListAuthorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignBizList],assigneeItem.associatedFunctionalRoles.map(f=>f.id));
          (<any>assigneeItem).assignBizListAuthorized = (<any>assigneeItem).deleteBizListAuthorized;
          return assigneeItem;
        }
        else {
          assigneeItem.associatedBizListItems=[];
          (<any>assigneeItem).associatedBizListAsFormattedString='';
        }
      }
      return null;
    };

    myFactory.parseExtractedBizListItems = function (assigneeItem:Entities.IBizListExtractable,bizListDic) {
      if(assigneeItem.bizListExtractionSummary && assigneeItem.bizListExtractionSummary[0]) {
        assigneeItem.bizListExtractionSummary.forEach((t) => {
          for (let grow = 0 ; grow< t.sampleExtractions.length ; grow++) {
            let sample = t.sampleExtractions[grow];
            sample.bizListExtractionDisplayName = $filter('translate')(t.bizListItemType) + '>' + (bizListDic[t.bizListId] ? bizListDic[t.bizListId].name : '') + '(' + t.count + ')'
          }
        })
        var authorized = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewBizListAssociation],assigneeItem.associatedFunctionalRoles.map(f=>f.id));
        if (authorized) {
          var totalCounter = 0;

          assigneeItem.bizListExtractionSummary.forEach(e=> {
            totalCounter += e.count;

          });
          assigneeItem.bizListExtractionSummary.sort((t1, t2)=> {
            if (t1.count > t2.count) {
              return 1;
            }
            if (t1.count < t2.count) {
              return -1;
            }
            return 0;
          });

          var tagList = assigneeItem.bizListExtractionSummary ? assigneeItem.bizListExtractionSummary.map(t =>
            $filter('translate')(t.bizListItemType) + '>' + ( bizListDic[t.bizListId] ? bizListDic[t.bizListId].name : '') + '(' + t.count + ')'
          ).join('; ') : '';

          (<any>assigneeItem).bizListExtractionSummaryAsFormattedString = tagList;
          return assigneeItem;
        }
        else {
          assigneeItem.bizListExtractionSummary=[];
          (<any>assigneeItem).bizListExtractionSummaryAsFormattedString ='';
        }
      }
      return null;
    };

    myFactory.setFunctionalRolesTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean) {
      var tagIconTemplate = "<span class='tag-icon tag tag-#: item.associatedFunctionalRoles[i].styleId #' bubble='#: item.functionalRolesAsFormattedString #'><span class='"+configuration.icon_functionalRole+"#: item.associatedFunctionalRoles[i].isExplicit?'':'-o' #'></span></span>";
      var tagNameTemplate= "<span  class='tag-name tag label label-resized1 label-o label-tag-#: item.associatedFunctionalRoles[i].styleId #' bubble='#: item.functionalRolesAsFormattedString #'>"+tagIconTemplate+" #: item.associatedFunctionalRoles[i].name #</span>";

      var tagsTooltip = `#if (  item.associatedFunctionalRoles && item.associatedFunctionalRoles[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons'  aria-labelledby='split-button' > ` +
        "# for (var i =0; i < item.associatedFunctionalRoles.length; i++) { # " +
        " <li bubble='#: item.associatedFunctionalRoles[i].name #' style='position:relative;'><a class='no-linkable ' >" +
        tagIconTemplate+'&nbsp;'+
        "<span bubble='#: item.associatedFunctionalRoles[i].name #' class='tag label label#: item.associatedFunctionalRoles[i].isExplicit?'':'-o' # label-tag-#: item.associatedFunctionalRoles[i].styleId #'> #: item.associatedFunctionalRoles[i].name #</span>&nbsp;"+
        "<span class=''>" +
        "#if (  item.deleteFuncRolesAuthorized) {#"+
            "<span class='btn btn-link btn-xs ' title='Remove this functional role' ng-if='#: item.associatedFunctionalRoles[i].isExplicit #' ng-click='removeFunctionalRole($event, dataItem , #: item.associatedFunctionalRoles[i].id #)' >" +
            "<span class='fa fa-trash-o '></span></span>#}#"+

        "<span class='btn btn-link btn-xs ' title='filter by this functional role' ng-click='filterByFunctionalRole($event, dataItem , #: item.associatedFunctionalRoles[i].id #)' >" +
        "<span class='fa fa-filter '></span></span>" +
        "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls = function() {
        $('html').trigger('click'); //to close all ddls
      };
      scope.removeFunctionalRole = function(event,dataItem:any,tagItemId:number) {
        closeAllDdls();
        var tags:Entities.DTOFileTag[] = dataItem.item ? (dataItem.item).associatedFunctionalRoles : dataItem.associatedFunctionalRoles;

        var tagItem = tags.filter(t=>(<any>t).id == tagItemId)[0];
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFunctionalRolesAction, {
          action: 'removeRole',
          item: dataItem,
          tagToRemove: tagItem,
          displayType: scope.entityDisplayType
        });
      };

      scope.filterByFunctionalRole = function(event,dataItem:any,roleItemId:number) {
        closeAllDdls();
        var tags:Entities.DTOFileTag[] = dataItem.item ? (dataItem.item).associatedFunctionalRoles : dataItem.associatedFunctionalRoles;

        var roleItem = tags.filter(t=>(<any>t).id == roleItemId)[0];
        if ( roleItem) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
            action: 'doFunctionalRolesFilter',
            values: [roleItem],
          });
        }
      };

      var position = doNotHaveContainer ? '' : 'right-position';
      tagsField.template = '#if (  item.associatedFunctionalRoles && item.associatedFunctionalRoles[0]) {# <span bubble="#: item.functionalRolesAsFormattedString #" class="dropdown dropdown-wrapper  size-#: item.associatedFunctionalRoles.length #" >' +
        '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +
        ' # for (var i=0; i < item.associatedFunctionalRoles.length; i++) { # ' +
        tagIconTemplate+tagNameTemplate+ ' # } #    ' +
        '<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></a></span>' + tagsTooltip+'</span># } #';

      if(noAggragetedDTo) {
        tagsField.template = tagsField.template.replace(/item./g, '');
      }
    };

    myFactory.setTagsTemplate2 = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean) {
      var tagIconTemplate = "<span class='tag-icon tag tag-#: item.fileTagDtos[i].styleId # ' bubble='#: item.tagsAsFormattedString #'><span class='"+configuration.icon_tag+"#: item.fileTagDtos[i].isExplicit?'':'-o' #'></span></span>";
      var tagNameTemplate= "<span  class='tag-name tag label label-resized1 label-o   label-tag-#: item.fileTagDtos[i].styleId # ' bubble='#: item.tagsAsFormattedString #'>"+tagIconTemplate+" #: item.fileTagDtos[i].name #</span>";

      var tagsTooltip = `#if (  item.fileTagDtos && item.fileTagDtos[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons'  aria-labelledby='split-button' > # for (var i =0; i < item.fileTagDtos.length; i++) { # ` +
          " <li style='position:relative;'><a class='no-linkable ' >" +
          tagIconTemplate+'&nbsp;'+
           "<span title='  #: item.fileTagDtos[i].type.name #> #: item.fileTagDtos[i].name #'  class='tag label label#: item.fileTagDtos[i].isExplicit?'':'-o' # label-tag-#: item.fileTagDtos[i].styleId #'> #: item.fileTagDtos[i].name #</span>&nbsp;"+
          "<span class=''>" +
        "#if (  item.deleteTagsAuthorized) {#"+
          "<span class='btn btn-link btn-xs ' title='Remove this tag' ng-if='#: item.fileTagDtos[i].isExplicit #' ng-click='removeTag($event, dataItem , #: item.fileTagDtos[i].id #)' >" +
          "<span class='fa fa-trash-o '></span></span>#}#"+
          "<span class='btn btn-link btn-xs ' title='filter by this tag' ng-click='filterByTag($event, dataItem , #: item.fileTagDtos[i].id #)' >" +
          "<span class='fa fa-filter '></span></span>" +
           "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls = function() {
        $('html').trigger('click'); //to close all ddls
      };

      scope.removeTag = function(event,dataItem:any,tagItemId:number) {
        closeAllDdls();
        var tags:Entities.DTOFileTag[] = dataItem.item ? (<Entities.ITaggable>(dataItem.item)).fileTagDtos : (<Entities.ITaggable>dataItem).fileTagDtos;

        var tagItem = tags.filter(t=>(<any>t).id == tagItemId)[0];
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridTagsUserAction, {
          action: 'removeTag',
          item: dataItem,
          tagToRemove: tagItem,
          displayType: scope.entityDisplayType
        });
      };

      scope.filterByTag = function(event,dataItem:any,tagItemId:number) {
        closeAllDdls();
        var tags:Entities.DTOFileTag[] = dataItem.item ? (<Entities.ITaggable>(dataItem.item)).fileTagDtos : (<Entities.ITaggable>dataItem).fileTagDtos;
        var tagItem = tags.filter(t=>(<any>t).id == tagItemId)[0];
        if ( tagItem) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
            action: 'doTagFilter',
            values: [tagItem],
          });
        }
      };

      var position = doNotHaveContainer ? '' : 'right-position';
      tagsField.template = '#if (  item.fileTagDtos && item.fileTagDtos[0]) {# <span bubble="#: item.tagsAsFormattedString #" class="dropdown dropdown-wrapper  size-#: item.fileTagDtos.length #" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +
          ' # for (var i=0; i < item.fileTagDtos.length; i++) { # ' +
           tagIconTemplate+tagNameTemplate+ ' # } #    ' +
          '<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></a></span>' + tagsTooltip+'</span># } #';

      if(noAggragetedDTo) {
        tagsField.template = tagsField.template.replace(/item./g, '');
      }
    };

    myFactory.setSearchPatternsTemplate2 = function (scope,patternsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
    {
      var patternsTooltip = `#if (  item.searchPatternsCounting && item.searchPatternsCounting[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons'  aria-labelledby='split-button' style="overflow-y: auto;max-height: 300px"> # for (var i =0; i < item.searchPatternsCounting.length; i++) { # ` +
        "<li style='position:relative;'><a class='no-linkable ' >" +
        "<span class='searchPattern-count'>#: item.searchPatternsCounting[i].patternCount #</span>" +
        "<span class='searchPattern-icon' >#if( !item.searchPatternsCounting[i].multi ) {# <span class='"+configuration.icon_pattern+"'></span>#} else {# <span class='"+configuration.icon_pattern_multi+"'></span> #}#</span>" +'&nbsp;'+
        "<span title='#: item.searchPatternsCounting[i].patternName #'  class=''> #: item.searchPatternsCounting[i].patternName #</span>&nbsp;"+
        "<span class=''>" +
        "<span class='btn btn-link btn-xs ' title='filter by this pattern' ng-click='filterByPattern($event, dataItem , #: item.searchPatternsCounting[i].patternId #,#: item.searchPatternsCounting[i].multi #)' >" +
        "<span class='fa fa-filter '></span></span>" +
        "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      };

      scope.filterByPattern = function(event,dataItem:any,patternId:number,multi:boolean) {
        let patternName:string = dataItem.searchPatternsCounting.filter(patternId => patternId == patternId)[0].patternName;
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
          action: (multi ? 'doSearchPatternMultiFilter' : 'doSearchPatternFilter'),
          id: patternId,
          name: patternName,
        });
      };

      patternsField.template = '#if (  item.searchPatternsCounting && item.searchPatternsCounting[0]) { # <span class="dropdown dropdown-wrapper " >' +
        '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +
        "<span class='searchPattern-icon' >#if( item.searchPatternsCounting.length == 1 ) {# <span bubble='Pattern:  #: item.searchPatternsCounting[0].patternName #' class='"+configuration.icon_pattern+"'></span>#} else {# <span bubble='Multiple patterns ( #: item.searchPatternsCounting.length  # )'  class='"+configuration.icon_pattern_multi+"'></span> #}#</span>" +
        '&nbsp;<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></a></span>' + patternsTooltip+'</span># } #';

      if(noAggragetedDTo) {
        patternsField.template = patternsField.template.replace(/item./g, '');
      }
    };


    myFactory.setLabelsTemplate2 = function (scope,labelsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean)
    {
      var labelTooltip = `#if (  item.daLabelDtos && item.daLabelDtos[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons'  aria-labelledby='split-button' style="overflow-y: auto;max-height: 300px"> # for (var i =0; i < item.daLabelDtos.length; i++) { # ` +
        "<li style='position:relative;'><a class='no-linkable ' >" +
        "<span class='label-icon' ><span class='"+configuration.icon_label+"'></span></span>" +'&nbsp;'+
        "<span title='#: item.daLabelDtos[i].name #'  class=''> #: item.daLabelDtos[i].name #</span>&nbsp;"+
        "<span class=''>" +
        "<span class='btn btn-link btn-xs ' title='filter by this label' ng-click='filterByLabel($event, dataItem , #: item.daLabelDtos[i].id #)' >" +
        "<span class='fa fa-filter '></span></span>" +
        "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      };

      scope.filterByLabel = function(event,dataItem:any,labelId:number) {
        let labelName:string = dataItem.daLabelDtos.filter(labelDto => labelDto.id == labelId)[0].name;
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
          action: 'doLabelFilter',
          id: labelId,
          name: labelName,
        });
      };

      labelsField.template = '#if (item.daLabelDtos && item.daLabelDtos[0]) { # <span class="dropdown dropdown-wrapper " >' +
        '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><a class="ddl-no-title">' +
        "<span class='label-icon' style='padding-right:0px' ><span class='"+configuration.icon_label+"'></span></span>" +
        '&nbsp;<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></a></span>' + labelTooltip+'</span>#}#';

      if(noAggragetedDTo) {
        labelsField.template = labelsField.template.replace(/item./g, '');
      }
    };


    myFactory.setDocTypeTemplate2 = function (scope,tagsField,eventCommunicator,dialogs,doNotHaveContainer=false,noAggragetedDTo:boolean = false , withoutDropDown: boolean = false) {
      var tagIconTemplate = "<span style='vertical-align: top;' class='docType-icon tag tag-#: item.docTypeDtos[i].styleId # opacity-depth-#: item.docTypeDtos[i].depth #' ' " +
          " bubble='#: item.docTypesAsFormattedString #'><span class='fa fa-#: item.docTypeDtos[i].isExplicit?'bookmark':'bookmark-o' #'></span></span>";
      var tagNameTemplate= "<span style='padding-top:0px' class='  tag label title-secondary label-resized label11#: item.docTypeDtos[i].isExplicit?'':'-o' #   tag-#: item.docTypeDtos[i].styleId # opacity-depth-#: item.docTypeDtos[i].depth #'" +
          " bubble='#: item.docTypesAsFormattedString #'>&nbsp;#: item.docTypeDtos[i].name #</span>";

      var tagsTooltip = `#if (  item.docTypeDtos && item.docTypeDtos[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons' aria-labelledby='split-button' >` +
          " # for (var i =0; i < item.docTypeDtos.length; i++) { # " +
          " <li style='position:relative;'><a class='no-linkable' >" +
          tagIconTemplate +
          "&nbsp <span  class='tag label label#: item.docTypeDtos[i].isExplicit?'':'-o' # label-tag-#: item.docTypeDtos[i].styleId #  opacity-depth-#: item.docTypeDtos[i].depth #' " +

          "> #: item.docTypeDtos[i].name #</span>"+
          "<span class=''>" +
        "#if (  item.deleteDocTypesAuthorized) {#"+
          "<span class='btn btn-link btn-xs ' title='Remove this DocType' ng-if='#: item.docTypeDtos[i].isExplicit #' " +
          "ng-click='removeDocType($event, dataItem , #: item.docTypeDtos[i].id #)' ><span class='fa fa-trash-o '></span></span>#}#" +
          "<span class='btn btn-link btn-xs ' title='Filter by this DocType'  " +
          "ng-click='filterByDocType($event, dataItem , #: item.docTypeDtos[i].id #)' ><span class='fa fa-filter '></span></span>" +
          "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls = function() {
        $('html').trigger('click'); //to close all ddls
      };

      scope.openDocType = function(dataItem) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDocTypesUserAction, {
            action: 'setDocType',
            item: dataItem,
          });
      };

      scope.removeDocType = function(event,dataItem:any,docTypeItemId:number) {
        closeAllDdls();
        var tags:Entities.DTODocType[] = dataItem.item ? (<Entities.IDocTypesAssignee>(dataItem.item)).docTypeDtos : (<Entities.IDocTypesAssignee>dataItem).docTypeDtos;

        var docTypeItem = tags.filter(t=>(<any>t).id == docTypeItemId)[0];
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDocTypesUserAction, {
          action: 'removeDocType',
          item: dataItem,
          docTypeToRemove: docTypeItem,
          displayType: scope.entityDisplayType
        });
      };

      scope.filterByDocType = function(event,dataItem:any,docTypeItemId:number) {
        closeAllDdls();
        var tags:Entities.DTODocType[] = dataItem.item ? (<Entities.IDocTypesAssignee>(dataItem.item)).docTypeDtos : (<Entities.IDocTypesAssignee>dataItem).docTypeDtos;

        var docTypeItem = tags.filter(t=>(<any>t).id == docTypeItemId)[0];
        if ( docTypeItem) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
            action: 'doDocTypeFilter',
            values: [docTypeItem],
          });
        }
      };

      var position = doNotHaveContainer ? '' : 'right-position';
      if (!withoutDropDown) {
        tagsField.template = '#if (  item.docTypeDtos && item.docTypeDtos[0]) {# <span bubble="#: item.docTypesAsFormattedString #"  class="dropdown dropdown-wrapper size-#: item.docTypeDtos.length #" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><span class="ddl-no-title">' +
          ' # for (var i=0; i < item.docTypeDtos.length; i++) { # ' +
          tagIconTemplate  + ' # } #  ' +
          '<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></span></span>' + tagsTooltip + '</span> # } #';
      }
      else {
        tagsField.template = '#if (  item.docTypeDtos && item.docTypeDtos[0]) {# <span bubble="#: item.docTypesAsFormattedString #"  class="dropdown dropdown-wrapper  size-#: item.docTypeDtos.length #" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><span class="ddl-no-title" style="vertical-align: middle;">' +
          ' # for (var i=0; i < item.docTypeDtos.length; i++) { # ' +
          tagIconTemplate + tagNameTemplate  + ' # } #  ' +
          '</span><i class="fa fa-caret-down ng-hide1 no-visible"  ></i></span>' +


          '<ul class="dropdown-menu" > '+
          '#if (   item.assignDocTypesAuthorized ) {#'+
          '<li><a class="btn btn-link btn-xs" ng-click="openDocType(dataItem)"><span class="fa fa-pencil"></span> Change DocType</a></li># } #' +
          '<li><a  ng-click="filterByDocType($event, dataItem , #: item.docTypeDtos[0].id #)"><span class="fa fa-filter"></span>  Filter by this DocType</a></li>'+
          '#if (   item.deleteDocTypesAuthorized && item.docTypeDtos[0].isExplicit) {#  <li><a  ng-click="removeDocType($event, dataItem , #: item.docTypeDtos[0].id #)"><span class="fa fa-trash-o"></span> Clear DocType</a></li> #}#'+
          '</ul>' +

          '</span> # } #' +
          '#if ( item.assignDocTypesAuthorized && ( !item.docTypeDtos ||  item.docTypeDtos.length == 0 )) {#  <i class="btn btn-xs btn-flat no-visible btn-association"  title="Add DocType" ng-click="openDocType(dataItem)"><span class="fa fa-caret-right"> </span><span class="fa fa-bookmark"> </span> <span style="font-style: normal"> Add DocType</span> </i> # } #';
      }

      if(noAggragetedDTo) {
        tagsField.template = tagsField.template.replace(/item./g, '');
      }
    };

    myFactory.setDepartmentTemplate = function (scope,tagField,eventCommunicator,dialogs,doNotHaveContainer=false,noAggragetedDTo:boolean = false , withoutDropDown: boolean = false) {

      var iconTemplate = "<span style='vertical-align: top;' class='department-icon tag tag-#: item.departmentDto.styleId # opacity-depth-#: item.departmentDto.depth #'  " +
        " bubble='#: item.departmentAsFormattedString #'><span ng-if='isInstallationModeStandard()' class='fa fa-#: item.departmentDto.isExplicit?'building':'building-o' #'></span><span ng-if='isInstallationModeLegal()' class='#: item.departmentDto.isExplicit?'icon-da-special-matter-icon':'icon-da-special-matter-icon-o' #'></span></span>";
      var nameTemplate= "<span style='padding-top:0px; margin-left: -2px' class='  tag label title-secondary label-resized label11#: item.departmentDto.isExplicit?'':'-o' #   tag-#: item.departmentDto.styleId # opacity-depth-#: item.departmentDto.depth #'" +
        " bubble='#: item.departmentAsFormattedString #'>&nbsp;#: item.departmentDto.name #</span>";

      var tooltip = `#if (item.departmentDto) {# <ul class='dropdown-menu pull-right labels-list-two-buttons' aria-labelledby='split-button' >` +
        " <li style='position:relative;'><a class='no-linkable' >" +
        iconTemplate +
        "&nbsp <span  class='tag label label#: item.departmentDto.isExplicit?'':'-o' # label-tag-#: item.departmentDto.styleId #  opacity-depth-#: item.departmentDto.depth #' " +
        " bubble='#: item.departmentAsFormattedString #'> #: item.departmentDto.name #</span>"+
        "<span class=''>" +
        "#if (  item.deleteDepartmentAuthorized) {#"+
        "<span class='btn btn-link btn-xs ' title='Remove this Department' ng-if='#: item.departmentDto.isExplicit # && isInstallationModeStandard()' " +
        "ng-click='removeDepartment($event, dataItem)' ><span class='fa fa-trash-o '></span></span>" +
        "<span class='btn btn-link btn-xs ' title='Remove this Matter' ng-if='#: item.departmentDto.isExplicit # && isInstallationModeLegal()' " +
        "ng-click='removeDepartment($event, dataItem)' ><span class='fa fa-trash-o '></span></span>#}#" +
        "<span class='btn btn-link btn-xs ' title='Filter by this Department' ng-if='isInstallationModeStandard()' " +
        "ng-click='filterByDepartment($event, dataItem, true)' ><span class='fa fa-filter '></span></span>" +
        "<span class='btn btn-link btn-xs ' title='Filter by this Matter' ng-if='isInstallationModeLegal()' " +
        "ng-click='filterByDepartment($event, dataItem, true)' ><span class='fa fa-filter '></span></span>" +
        "</span></a></li> </ul> # } #";

      var closeAllDdls = function() {
        $('html').trigger('click'); //to close all ddls
      };

      scope.openDepartment = function(dataItem) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDepartmentMenuUserAction, {
          action: 'setDepartment',
          item: dataItem,
        });
      };

      scope.removeDepartment = function(event,dataItem:any) {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDepartmentMenuUserAction, {
          action: 'removeDepartment',
          item: dataItem,
          departmentToRemove: (<Entities.IDepartmentAssignee>(dataItem.item)).departmentDto,
          displayType: scope.entityDisplayType
        });
      };

      scope.filterByDepartment = function(event,dataItem:any,isSubtreeFilter:boolean) {
        closeAllDdls();

        var item:Entities.DTODepartment = dataItem.item ? (<Entities.IDepartmentAssignee>(dataItem.item)).departmentDto : (<Entities.IDepartmentAssignee>dataItem).departmentDto;

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDepartmentMenuUserAction, {
          action: 'doDepartmentFilter',
          values: [item],
          isSubtreeFilter: isSubtreeFilter
        });
      };

      scope.openMatter = function(dataItem) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMatterMenuUserAction, {
          action: 'setMatter',
          item: dataItem,
        });
      };

      scope.removeMatter = function(event,dataItem:any) {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMatterMenuUserAction, {
          action: 'removeMatter',
          item: dataItem,
          matterToRemove: (<Entities.IDepartmentAssignee>(dataItem.item)).departmentDto,
          displayType: scope.entityDisplayType
        });
      };

      scope.filterByMatter = function(event,dataItem:any,isSubtreeFilter:boolean) {
        closeAllDdls();

        var item:Entities.DTODepartment = dataItem.item ? (<Entities.IDepartmentAssignee>(dataItem.item)).departmentDto : (<Entities.IDepartmentAssignee>dataItem).departmentDto;

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMatterMenuUserAction, {
          action: 'doMatterFilter',
          values: [item],
          isSubtreeFilter: isSubtreeFilter
        });
      };

      scope.isInstallationModeStandard = function() {
        return currentUserDetailsProvider.isInstallationModeStandard();
      };

      scope.isInstallationModeLegal = function() {
        return currentUserDetailsProvider.isInstallationModeLegal();
      };

      var position = doNotHaveContainer ? '' : 'right-position';
      if (!withoutDropDown) {
        tagField.template = '#if (item.departmentDto!=null) {# <span bubble="#: item.departmentAsFormattedString #"  class="dropdown dropdown-wrapper size-1" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><span class="ddl-no-title">' +
          iconTemplate  +
          '<i class="fa fa-caret-down ng-hide1 no-visible"></i></span></span>' + tooltip + '</span> # } #';
      }
      else {
        tagField.template = '#if (item.departmentDto) {# <span bubble="#: item.departmentAsFormattedString #"  class="dropdown dropdown-wrapper size-1" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><span class="ddl-no-title" style="vertical-align: middle;">' +
          iconTemplate + nameTemplate  +
          '</span><i class="fa fa-caret-down ng-hide1 no-visible"  ></i></span>' +
          '<ul class="dropdown-menu" > '+
          '#if (   item.assignDepartmentAuthorized ) {#'+
          '<li><a class="btn btn-link btn-xs" ng-click="openDepartment(dataItem)" ng-if="isInstallationModeStandard()"><span class="fa fa-pencil"></span> Change Department</a><a class="btn btn-link btn-xs" ng-click="openMatter(dataItem)" ng-if="isInstallationModeLegal()"><span class="fa fa-pencil"></span> Change Matter</a></li># } #' +
          '<li><a  ng-click="filterByDepartment($event, dataItem, true)" ng-if="isInstallationModeStandard()"><span class="fa fa-filter"></span>  Filter by this Department</a><a  ng-click="filterByMatter($event, dataItem)" ng-if="isInstallationModeLegal()"><span class="fa fa-filter"></span>  Filter by this Matter</a></li>'+
          '#if (   item.deleteDepartmentAuthorized && item.departmentDto.isExplicit) {#  <li><a  ng-click="removeDepartment($event, dataItem)" ng-if="isInstallationModeStandard()"><span class="fa fa-trash-o"></span> Clear Department</a><a  ng-click="removeMatter($event, dataItem)" ng-if="isInstallationModeLegal()"><span class="fa fa-trash-o"></span> Clear Matter</a></li> #}#'+
          '</ul>' +
          '</span> # } #' +
          '#if ( item.assignDepartmentAuthorized &&  !item.departmentDto ) {#  <i class="btn btn-xs btn-flat no-visible btn-association" ng-if="isInstallationModeStandard()" title="Add Department" ng-click="openDepartment(dataItem)"><span class="fa fa-caret-right"> </span><span class="fa fa-building"></span><span style="font-style: normal;margin-left: -1px"> Add Department</span></i><i class="btn btn-xs btn-flat no-visible btn-association" ng-if="isInstallationModeLegal()" title="Add Matter" ng-click="openMatter(dataItem)"><span class="fa fa-caret-right"> </span><span class="icon-da-special-matter-icon"></span><span style="font-style: normal;margin-left: -2px"> Add Matter</span></i> # } #';
      }

      if(noAggragetedDTo) {
        tagField.template = tagField.template.replace(/item\./g, '');
      }
    };

    myFactory.setAssociatedBizListItemsTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean) {
      var tagIconTemplate = "<span class='tag tag-#: item.associatedBizListItems[i].bizListId # ' " +
          " bubble='#: item.associatedBizListAsFormattedString #'><span class='"+configuration.icon_bizListItemClients+"#: item.associatedBizListItems[i].isExplicit?'':'-o' #'></span></span>";

      var tagsTooltip = `#if (  item.associatedBizListItems && item.associatedBizListItems[0]) {# <ul class='dropdown-menu pull-right labels-list-two-buttons' aria-labelledby='split-button' >` +
          " # for (var i =0; i < item.associatedBizListItems.length; i++) { # " +
          " <li style='position:relative;'><a class='no-linkable' >" +
          tagIconTemplate +
          "&nbsp <span  class='tag label label#: item.associatedBizListItems[i].isExplicit?'':'-o'# label-tag-#: item.associatedBizListItems[i].bizListId #  ' " +

          "> #: item.associatedBizListItems[i].name #</span>&nbsp; "+
          "<span class=''>" +
        "#if (  item.deleteBizListAuthorized) {#"+
          "<span class='btn btn-link btn-xs ' title='Remove this Biz list item association' ng-if='#: item.associatedBizListItems[i].isExplicit #' " +
          "ng-click='removeBizListItemAssociation($event, dataItem ,\"#: item.associatedBizListItems[i].bizListId #\", \"#: item.associatedBizListItems[i].id #\")' ><span class='fa fa-trash-o '></span></span>#}#"+
          "<span class='btn btn-link btn-xs ' title='Filter this Biz list item association' " +
          "ng-click='filterByBizListItemAssociation($event, dataItem ,\"#: item.associatedBizListItems[i].id #\", \"#: item.associatedBizListItems[i].name #\")' ><span class='fa fa-filter '></span></span>" +
          "</span></a></li>  # } # </ul> # } #";

      var closeAllDdls=function() {
        $('html').trigger('click'); //to close all ddls
      };

      scope.removeBizListItemAssociation = function(event,dataItem:any,bizListId,bizListItemId:number) {
        closeAllDdls();
        // var tags:Entities.DTOSimpleBizListItem[] = dataItem.item ? (<Entities.IBizListAssociatable>(dataItem.item)).associatedBizListItems : (<Entities.IBizListAssociatable>dataItem).associatedBizListItems;
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridBizListUserAction, {
          action: 'removeBizListItemAssociation',
          selectedDataItem: dataItem,
          bizListItemId: bizListItemId,
          displayType: scope.entityDisplayType,
          bizListId:bizListId
        });
      };

      scope.filterByBizListItemAssociation = function(event,dataItem:any,bizListItemId:number,bizListItemName:number){
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
          action: 'doBizAssociationFilter',
          id: bizListItemId,
          name: bizListItemName,
        });
      };

      var position = doNotHaveContainer ? '' : 'right-position';

      tagsField.template = '#if (  item.associatedBizListItems && item.associatedBizListItems[0]) {# <span bubble="#: item.associatedBizListAsFormattedString #" class="dropdown dropdown-wrapper" >' +
          '<span class="btn-dropdown dropdown-toggle" data-toggle="dropdown"><span class="ddl-no-title">' +
          ' # for (var i=0; i < item.associatedBizListItems.length; i++) { # ' +
         tagIconTemplate+ ' # } #  ' +
          '<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></span></span>'+ tagsTooltip+'</span> # } #';

      if(noAggragetedDTo) {
        tagsField.template = tagsField.template.replace(/item./g, '');
      }
    };

    myFactory.setExtractedBizListItemsTemplate = function (scope,tagsField,eventCommunicator,doNotHaveContainer=false,noAggragetedDTo?:boolean) {
      var tagIconTemplate = '<span class="tag tag-#: item.bizListExtractionSummary[i].bizListId # " ' +
          ' bubble="#: item.bizListExtractionSummaryAsFormattedString #" style="white-space: nowrap">' +
                    "<span class={{bizListTypeIcon('#: item.bizListExtractionSummary[i].bizListItemType #')}}></span></span>";

      var tagsTooltip = "#if (  item.bizListExtractionSummary[i] && item.bizListExtractionSummary[i].sampleExtractions[0])" +
          " {#" +
          " <ul class='dropdown-menu pull-right labels-list-one-button' aria-labelledby='split-button' >" +

            " # for (var j =0; j <  item.bizListExtractionSummary[i].sampleExtractions.length; j++) { # " +
               " <li style='position:relative;'><a class='no-linkable' >" +
            tagIconTemplate +
            "&nbsp <span title='#: item.bizListExtractionSummary[i].sampleExtractions[j].bizListExtractionDisplayName #'  class='tag label label-tag-#: item.bizListExtractionSummary[i].bizListId #  ' >" +
          " #: item.bizListExtractionSummary[i].sampleExtractions[j].bizListExtractionDisplayName  #"+
          "</span>"+
          "<span class='btn btn-link btn-xs  ' title='Filter this Biz list item association' " +
          "ng-click='filterByBizListItemExtraction($event, dataItem ,\"#: item.bizListExtractionSummary[i].bizListItemType #\",\"#:  item.bizListExtractionSummary[i].sampleExtractions[j].bizListId #\",\"#:  item.bizListExtractionSummary[i].sampleExtractions[j].id #\", \"#:  item.bizListExtractionSummary[i].sampleExtractions[j].bizListExtractionDisplayName #\")' >" +
          "<span class='fa fa-filter '></span></span></a></li>"+
             "# } #" +
               "#if (  item.bizListExtractionSummary[i].sampleExtractions.length< item.bizListExtractionSummary[i].count){ # "+
                  "<li><a>More..   (#: kendo.toString(item.bizListExtractionSummary[i].count-item.bizListExtractionSummary[i].sampleExtractions.length,'n0') #)</a></li>"+
              " # } #"+
          " </ul> # } #";

      scope.bizListTypeIcon = function(listTypeName) {
        return myFactory.bizListTypeIcon(listTypeName);
      };

      var closeAllDdls = function() {
        $('html').trigger('click'); //to close all ddls
      };

      scope.filterByBizListItemExtraction = function(event,dataItem:any,bizListItemType,bizListItemId:number,ruleId:number,bizListItemName:number) {
        closeAllDdls();
        if (bizListItemType == Entities.DTOExtractionSummary.itemType_CONSUMERS) {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
            action: 'doExtractionRuleFilter',
            id: bizListItemId,
            name: bizListItemName,
          });
        }
        else {
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
            action: 'doBizExtractionFilter',
            id: ruleId,
            name: bizListItemName,
          });
        }
      };

      var position=doNotHaveContainer?'':'right-position';

      tagsField.template = '#if (  item.bizListExtractionSummary && item.bizListExtractionSummary[0]) {#'+
             ' # for (var i=0; i < item.bizListExtractionSummary.length; i++) { # ' +

              '<span  class="dropdown dropdown-wrapper" ><span class="btn-dropdown dropdown-toggle" data-toggle="dropdown">' +

               '<a>'+'<small  class="tag tag-#: item.bizListExtractionSummary[i].bizListId # "  style=" vertical-align: top;padding: 0;margin-right: -2px;padding-left: 2px;">#: item.bizListExtractionSummary[i].count #</small> '+ tagIconTemplate+

               '<i class="fa fa-caret-down ng-hide1 no-visible"  ></i></a>'+
              '</span> '+

            tagsTooltip+
          '</span> ' +
          '# } #  # } #';
        if(noAggragetedDTo) {
        tagsField.template = tagsField.template.replace(/item./g, '');
      }
    };

    myFactory.bizListTypeIcon= function(listTypeName) {
      if(listTypeName == Entities.DTOBizList.itemType_CLIENTS)
        return configuration.icon_bizListItemClientExtraction;
      if(listTypeName == Entities.DTOBizList.itemType_CONSUMERS)
        return configuration.icon_bizListItemConsumerExtraction;
    };

    myFactory.getFilesAndFilterTemplate = function (numOfFilesFieldName,numOfFilteredFilesFieldName,inFolderPercentageFieldName) {
      // var numOfFilteredFilesNewTemplate =numOfFilteredFilesFieldName?'#if ( '+ numOfFilteredFilesFieldName+'>=0) {#' +
      // ' <span ng-if="(filterData&&filterData.filter)||restrictedEntityId"> #: '+numOfFilteredFilesFieldName+'#'+
      // '</span> #}#':'';
      // var numOfFilesNewTemplate = numOfFilesFieldName? '#=' + numOfFilesFieldName+'#': '';
      var filterTemplate = '';
      if(numOfFilesFieldName) {
         filterTemplate =
          `<span style="font-size:12px;text-overflow: ellipsis;white-space: nowrap;display:inline-block;overflow: hidden" >
               <span ng-if="(filterData&&filterData.filter)||restrictedEntityId" bubble="Filtered files: #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #">
                <small style="white-space: nowrap;"  class="fa fa-filter"></small>&nbsp;
                #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #
                 #if (  ${ numOfFilesFieldName}>=0) {# / #}# 
               </span>
              #if (  ${ numOfFilesFieldName}>=0) {# <span bubble="All files:#: kendo.toString( ${numOfFilesFieldName}, 'n0') # ">#: kendo.toString( ${numOfFilesFieldName}, 'n0') #  </span> #}#  files
              </span>
               `;

        if (inFolderPercentageFieldName) {
          var percent = inFolderPercentageFieldName ? '<span style="white-space: nowrap" ng-if="(filterData&&filterData.filter)||restrictedEntityId">#: kendo.toString(' + inFolderPercentageFieldName + ', "n0")# %</span>' : '';
          filterTemplate += "<span style='line-height: 10px;display:block;'><small style='white-space: nowrap'> &nbsp;&nbsp;" +
            percent + "</small></span>";
        }
      }
      else{
        filterTemplate =
          `<span style="font-size:12px;text-overflow: ellipsis;white-space: nowrap;" >
               <span ng-if="(filterData&&filterData.filter)||restrictedEntityId" bubble="Filtered files: #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #  ">
                <small style="white-space: nowrap;"  class="fa fa-filter"></small>&nbsp;
               </span>
               <span bubble="All files: #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') # ">#: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #  </span>   files
              </span>
               `;
      }
      return filterTemplate;
    };

    myFactory.parseFilesAndFilterTemplate = function(entity:Entities.IFunctionalRolesAssignee) {
   //   (<any>entity).viewFilesUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewFiles]);
    };

    myFactory.getFilesAndFilterTemplateFlat = function (numOfFilesFieldName,numOfFilteredFilesFieldName,percentageFieldName,tooltip?:string) {
      tooltip = tooltip?tooltip:'All files';

      var filterTemplate = '';
      if(numOfFilesFieldName) {
        filterTemplate =
          `<span style="font-size:12px;white-space: nowrap;" >
               <span bubble="User filtered files:   #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #">
                <small  ng-if=" (filterData&&filterData.filter)||restrictedEntityId" style="white-space: nowrap;margin-right: 3px;"  class="fa fa-filter"></small>
                 #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #
                  #if (  ${ numOfFilesFieldName}>=0 && ${numOfFilteredFilesFieldName}>=0 ) {# / #}# 
               </span>
              #if (  ${ numOfFilesFieldName}>=0) {# <span bubble="${tooltip}: #: kendo.toString( ${numOfFilesFieldName}, 'n0') #">#: kendo.toString( ${numOfFilesFieldName}, 'n0') # </span> #}#  files
              </span>
               `;

        if (percentageFieldName) {
          var percent = ` #if (  ${ numOfFilesFieldName}>=0 && ${numOfFilteredFilesFieldName}>=0 ) {#
              <small style="line-height: 10px;display: inline-block;width: 25px;text-align: right">#: kendo.toString(${percentageFieldName}, "n0")# %</small>&nbsp;&nbsp;#}# `;
          filterTemplate = '<span style="white-space: nowrap;display: inline-block">'+ percent + filterTemplate+'</span>';
        }
      }
      else{
        filterTemplate =
          `<span style="font-size:12px;white-space: nowrap;" >
               <span ng-if="(filterData&&filterData.filter)||restrictedEntityId" bubble="User filtered files: #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #">
                <small style="white-space: nowrap;"  class="fa fa-filter"></small>&nbsp;
               </span>
               <span bubble="${tooltip}: #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') #"> #: kendo.toString( ${numOfFilteredFilesFieldName}, 'n0') # </span>   files
              </span>
               `;
      }
      return filterTemplate;
    };

    myFactory.getFileTypesTemplate = function (processedExcelFiles,processedPdfFiles,processedWordFiles,processedOtherFiles) {
     var excel = '<span  ng-if=" dataItem.'+processedExcelFiles+'>0" class=""  bubble=" MS-Excel processed files : {{dataItem.'+processedExcelFiles+'|number}}" style="margin-left: 3px;display: inline-block">' +
          ' <span class="fileType fa fa-file-excel-o " style="font-size: 14px;"></span><span class=""  translate="{{dataItem.'+processedExcelFiles+'|number}}"></span></span>  ';
      var pdf = '<span  ng-if=" dataItem.'+processedPdfFiles+'>0" class=" " bubble="Pdf processed files : {{dataItem.'+processedPdfFiles+'|number}}" style="margin-left: 3px;display: inline-block">' +
          ' <span  class="fileType fa fa-file-pdf-o " style="font-size: 14px;"></span> <span class="" translate="{{dataItem.'+processedPdfFiles+'|number}}"></span></span>  ';
      var word = '<span  ng-if=" dataItem.'+processedWordFiles+'>0" bubble="MS-Word processed files : {{dataItem.'+processedWordFiles+'|number}}" class=" " style="margin-left: 3px;display: inline-block">' +
          ' <span  class="fileType fa fa-file-word-o " style="font-size: 14px;"></span> <span class=""  translate="{{dataItem.'+processedWordFiles+'|number}}"></span></span>  ';
      var other = '<span  ng-if=" dataItem.'+processedOtherFiles+'>0" class=" "  bubble="Other processed files : {{dataItem.'+processedOtherFiles+'|number}}" style="margin-left: 3px;display: inline-block">' +
          ' <span class="fileType fa fa-file-o " style="font-size: 14px;"></span><span class="" translate="{{dataItem.'+processedOtherFiles+'|number}}"> </span></span>  ';

      return excel.concat(pdf).concat(word).concat(other);
    };

    myFactory.getFileProcessingErrorsTemplate = function (scanErrors,scanProcessingErrors) {
      var scanErrorsTemplate = '<div  ng-if=" dataItem.'+scanErrors+'>0"> scan:' +
          ' <span bubble="Scan error files" translate="{{dataItem.'+scanErrors+'|number}}"></span> </div>  ';
      var scanProcessingErrorsTemplate = '<div  ng-if=" dataItem.'+scanProcessingErrors+'>0">ingest:' +
          ' <span bubble="Scan ingested error files" translate=" {{dataItem.'+scanProcessingErrors+'|number}}"></span> </div>  ';


      return "<div class='table-row title-third' ng-if='dataItem."+scanErrors+">0 || dataItem."+scanProcessingErrors+">0' > <div class='table-cell' style='vertical-align: middle; padding-right: 10px;'>errors:</div><div class='table-cell'>"+scanErrorsTemplate.concat(scanProcessingErrorsTemplate)+'</div></div>';
    };

    myFactory.getLastRunTimingTemplate=function(lastRunStartTime,lastRunElapsedTime,lastRunEndTime,isRunning) {
      var lastRunStartTimeTemplate= "<span title='Last run start time'> {{ dataItem."+lastRunStartTime+" |date:\'"+configuration.dateTimeFormat+"\' }}</span>";
      var lastRunElapsedTimeTemplate='<div ng-if="!dataItem.'+isRunning+'"  translate=" {{\'(\'+dataItem.'+lastRunElapsedTime+'+\')\'}}" bubble="Ended at {{dataItem.'+lastRunEndTime+'| date:\''+configuration.dateTimeFormat+' \'}}"></div>';
      var lastRunElapsedTimeTemplateRunning='<div ng-if="dataItem.'+isRunning+'"  translate=" {{\'(\'+dataItem.'+lastRunElapsedTime+'+\')\'}}" bubble="Running for {{dataItem.'+lastRunElapsedTime+'}}"></div>';


      return "<div class='title-secondary centerText' ng-if='dataItem."+lastRunElapsedTime+"'>"+lastRunStartTimeTemplate.concat(lastRunElapsedTimeTemplate).concat(lastRunElapsedTimeTemplateRunning)+'</div>';
    };

    myFactory.getStatusTemplate =function(status,isEndedSuccessfully,isEndedWithError,isPaused,translatePrefix='') {
     var template='<div class="title-secondary"><span  translate="{{translatePrefix+dataItem.'+status+'}}"></span>&nbsp;'+
      '<span ng-show="dataItem.'+isEndedSuccessfully+'" class="fa fa-check primary " bubble="Last processing ended successfully"></span>' +
      '<span ng-show="dataItem.'+isEndedWithError+'" class="fa fa-exclamation-triangle notice icon-item" bubble="Last processing failed"></span>'
      '<span ng-show="dataItem.'+isPaused+'" class="fa fa-pause  icon-item" bubble="Process paused"></span></div>'

      return "<div class='centerText'><div class='title-third' >Status</div>".concat(template)+'</div>';
    };

    myFactory.getToggleButtonTemplate =function(active,titleText,onClickCallBack,disabledField?,hoverTitle?) {
      let hoverText = !_.isEmpty(hoverTitle) ? hoverTitle : titleText;

      var rescanActiveTemplate = '<div class="title-third ">'+titleText+'</div>' +
          '<div class="title-secondary"><a class="active-toggle-btn" ng-click="'+onClickCallBack+'" >'+
          '<span ng-show="dataItem.'+active+'"  bubble="'+hoverText+' is active"><span class="fa fa-toggle-on icon-item " ></span> </span>' +
          '<span ng-show="!dataItem.'+active+'" bubble="'+hoverText+' is not active" class="notice"><span class="fa fa-toggle-off notice icon-item" ></span> </span>'+
          '</a></div>';
      return "<div class='centerText' "+(disabledField?"ng-class='{disabled:dataItem."+disabledField+"}":'')+ "'>"+rescanActiveTemplate+"</div>";
    };

    myFactory.getCountTemplate =function(countFiledName,title,titleClass?,translatePrefix='',countFiledNameTooltip=null) {
      countFiledNameTooltip = countFiledNameTooltip?countFiledNameTooltip:countFiledName;
      var template = '<div class="title-third '+(titleClass?titleClass:"")+'" ><span translate="{{\''+title+'\'}}"></span></div>' +
          '<div class="title-secondary"  translate="{{translatePrefix+dataItem.'+countFiledName+'}}" bubble="{{dataItem.'+countFiledNameTooltip  +'}}"></div>';
      return "<div class='centerText'>"+template+"</div>";
    };

    myFactory.getTitledTemplate =function(innerTemplate,title,titleClass?,translatePrefix='',countFiledNameTooltip=null) {
      countFiledNameTooltip = countFiledNameTooltip?countFiledNameTooltip:innerTemplate;
      var template = '<div class="title-third '+(titleClass?titleClass:"")+'"  ><span translate="{{\''+title+'\'}}"></span>:</div>' +
          '<div class="title-secondary ellipsis"  translate="{{translatePrefix+dataItem.'+innerTemplate+'}}" bubble="{{dataItem.'+countFiledNameTooltip  +'}}"></div>';
      return "<div class='centerText'>"+template+"</div>";
    };

    myFactory.getFileWorklowDataTemplate= function() {
      var template = '<div class="title-third "  ><span>Action</span>: <span class="{{getFileStateIcon(dataItem)}}" style="margin-left: 2px;"></span></div>' +
            '<div>' +
        '<span class="btn label label-tag-{{dataItem.workflowFileData.fileAction}}" translate="{{dataItem.workflowFileData.fileAction}}"></span></div>';
      return "<div class='centerText'>"+template+"</div>";
    };


    myFactory.getFilesNumPercentage = function(filteredNumber:number , totalNumber : number):number  {
      if (filteredNumber==0 && totalNumber ==0 ) {
        return 100;
      }
      if (totalNumber == -1 ) {
        return null;
      }
      return Math.floor(filteredNumber / totalNumber * 100) == 0 ?  Math.ceil(filteredNumber / totalNumber * 100) : Math.floor(filteredNumber / totalNumber * 100);
    }

    return  myFactory;
  });



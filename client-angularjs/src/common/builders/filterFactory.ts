///<reference path='../../common/all.d.ts'/>

'use strict';
angular.module('ui.builder.filters', [
])
    .factory('filterFactory', function(configuration:IConfig,propertiesUtils, currentUserDetailsProvider:ICurrentUserDetailsProvider) {
      var myFactory:ui.IFilterFactory = <ui.IFilterFactory>{};

      var filterNames = {};
      filterNames[configuration.contentFilterFieldName] = 'Content';
      filterNames[configuration.labelFieldName] = 'Label';
      filterNames[configuration.sharePermissionFieldName] = 'Read share permissions';
      filterNames[configuration.noneMailFilterFieldName] = 'EMail';
      filterNames[configuration.openAccessRightsFilterFieldName] = 'Access';
      filterNames[configuration.tagFilterFieldName] = 'Tag';
      filterNames[configuration.tagTypeMismatchFilterFieldName] = 'Mismatch';
      filterNames[configuration.tagTypeFilterFieldName] = 'Tag type';
      filterNames[configuration.subFolderFilterFieldName] = 'Folder subtree';
      filterNames[configuration.departmentFilterFieldName] = 'Department';
      filterNames[configuration.subDepartmentFilterFieldName] = 'Department subtree';
      //filterNames[configuration.matterFilterFieldName] = 'Matter';
      //filterNames[configuration.subMatterFilterFieldName] = 'Matter subtree';
      filterNames[configuration.docTypeFilterFieldName] = 'DocType';
      filterNames[configuration.subDocTypeNameFilterFieldName] = 'DocType subtree';
      filterNames[configuration.subDocTypesFilterFieldName] = 'DocType subtree';
      filterNames[configuration.fileSpecificProcessingStateFilterFieldName] = 'File current state';
      filterNames[configuration.fileTagTypeNameFilterFieldName] = 'Tag type';
      filterNames[configuration.bizListItemAssociationFilterFieldName] = 'Association';
      filterNames[configuration.ungroupedFilterFieldName] = 'Un-grouped';
      filterNames[configuration.functionalRoleFilterFieldName] = 'Data role';
      filterNames[configuration.bizListItemAssociationTypeFilterFieldName] = 'Association all';
      filterNames[configuration.bizListItemExtractionFilterFieldName] = 'Extraction';
      filterNames[configuration.fileExtensionFilterFieldName] ='File extension';
      filterNames[configuration.fileTypeCategoryNameFilterFieldName] ='File type';
      filterNames[configuration.fileTypeCategoryFilterFieldName] ='File type';
      filterNames[configuration.fileSizeFilterFieldName] ='File size';
      filterNames[configuration.regulationFilterFieldName] ='Regulation';
      filterNames[configuration.regulationNameFilterFieldName] ='Regulation';
      filterNames[configuration.metadataTypeFilterFieldName] ='File metadata type';
      filterNames[configuration.metadataFilterFieldName] ='File metadata';
      filterNames[configuration.patternFilterFieldName] ='Pattern';
      filterNames[configuration.patternNameFilterFieldName] ='Pattern';
      filterNames[configuration.patternMultiFilterFieldName] ='Multi pattern';
      filterNames[configuration.patternManyNameFilterFieldName] ='Multi pattern';
      filterNames[configuration.bizListItemExtractionTypeFilterFieldName] ='Extraction all';
      filterNames[configuration.bizListItemExtractionRuleFilterFieldName] ='Extraction rule';
      filterNames[configuration.fileDuplicationsFilterFieldName] ='File copies';
      filterNames[configuration.fileCreatedDatesFilterFieldName] ='File created date';
      filterNames[configuration.fileModifiedDatesFilterFieldName] ='File modified date';
      filterNames[configuration.fileAccessedDatesFilterFieldName] ='File accessed date';
      filterNames[configuration.rootFolderNameLike] ='Rootfolder';
      filterNames[configuration.mediaTypeFilterFieldName] ='Media';
      filterNames[configuration.mailFilterFieldName] ='EMail';
      filterNames[configuration.senderAddressFilterFieldName] ='From';
      filterNames[configuration.recipientAddressFilterFieldName] ='To';
      filterNames[configuration.senderDomainFilterFieldName] ='From domain';
      filterNames[configuration.recipientAddressFilterFieldName] ='To domain';

      myFactory.searchTextAbbreviationsDic={
        'group:':EntityDisplayTypes.groups,
        'acl:':EntityDisplayTypes.acl_reads,
        'aclr:':EntityDisplayTypes.acl_reads,
        'aclw:':EntityDisplayTypes.acl_writes,
        'file:':EntityDisplayTypes.files,
        'extension:':EntityDisplayTypes.extensions,
        'ext:':EntityDisplayTypes.extensions,
        'reg:':EntityDisplayTypes.regulations,
        'regulation:':EntityDisplayTypes.regulations,
        'pattern:':EntityDisplayTypes.patterns,
        'patternmulti:':EntityDisplayTypes.patterns_multi,
        'multipattern:':EntityDisplayTypes.patterns_multi,
        'filetypecategory:':EntityDisplayTypes.file_type_categories,
        'filetype:':EntityDisplayTypes.file_type_categories,
        'dep:':EntityDisplayTypes.departments,
        'department:':EntityDisplayTypes.departments,
        'doc:':EntityDisplayTypes.doc_types,
        'doctype:':EntityDisplayTypes.doc_types,
        'tagtype:':EntityDisplayTypes.tag_types,
        'mediatype:':EntityDisplayTypes.media_type,
        'media:':EntityDisplayTypes.media_type,
        'from:':EntityDisplayTypes.email_sender_address,
        'to:':EntityDisplayTypes.email_recipient_address,
        'fromdomain:':EntityDisplayTypes.email_sender_domain,
        'todomain:':EntityDisplayTypes.email_recipient_domains,
        'rootfolder:':EntityDisplayTypes.root_folders,
        'rf:':EntityDisplayTypes.root_folders,
        'owner:':EntityDisplayTypes.owners,
        'readsharepermission:':EntityDisplayTypes.share_permissions_allow_read,
        'sharepermission:':EntityDisplayTypes.share_permissions_allow_read,
        'rsp:':EntityDisplayTypes.share_permissions_allow_read,
        'folder:':EntityDisplayTypes.folders,
      };

      myFactory.createFilterByProperties=function(): (value) => ICriteriaComponent
      {

        var displayedFilterType = EUserSearchMode.properties.toString();

        var dtoType = Entities.DTOOwner.Empty();
        var dtoType1 = Entities.DTOGroup.Empty();
        var dtoType2 = Entities.DTOFolderInfo.Empty();
        var dtoType3 = Entities.DTOFileInfo.Empty();
        var dtoType7 = Entities.DTOFileUser.Empty();
        var dtoType5 = Entities.DTOFileUser.Empty();

        return function (value):ICriteriaComponent {
          var originalCriteriaOperationId=propertiesUtils.creatUuid();
          var criterias =
              [new SingleCriteria(EFilterContextNames.group, propertiesUtils.propName(dtoType1, dtoType1.groupName), value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
                , new SingleCriteria(EFilterContextNames.owner, propertiesUtils.propName(dtoType, dtoType.user), value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
                , new SingleCriteria(EFilterContextNames.file, propertiesUtils.propName(dtoType3, dtoType3.itemSubject), value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
                , new SingleCriteria(EFilterContextNames.file, 'fullName', value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
                , new SingleCriteria(EFilterContextNames.acl_read, propertiesUtils.propName(dtoType5, dtoType5.user), value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
                , new SingleCriteria(EFilterContextNames.acl_write, propertiesUtils.propName(dtoType7, dtoType7.user), value, EFilterOperators.contains, value, displayedFilterType,originalCriteriaOperationId)
              ];  //shaul
          return new OrCriteria(criterias);

        }
      }

      myFactory.createFilterBySearchText=function(entityDisplayTypes:EntityDisplayTypes):(value) => ICriteriaComponent {
        var originalCriteriaOperationId=propertiesUtils.creatUuid();
        if (entityDisplayTypes == EntityDisplayTypes.groups) {
          var dtoType = Entities.DTOGroup.Empty();
          return function (value): ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.group, propertiesUtils.propName(dtoType, dtoType.groupName), value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.acl_reads) {
          var dtoType5 = Entities.DTOFileUser.Empty();
          return function (value): ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.acl_read, propertiesUtils.propName(dtoType5, dtoType5.user), value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.acl_writes) {
          var dtoType6 = Entities.DTOFileUser.Empty();
          return function (value): ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.acl_write, propertiesUtils.propName(dtoType6, dtoType6.user), value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.share_permissions_allow_read) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.sharePermissionFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.owners) {
          var dtoTypeO = Entities.DTOOwner.Empty();
          return function (value): ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.owner, propertiesUtils.propName(dtoTypeO, dtoTypeO.user), value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.email_sender_address) {
          return function (value): ICriteriaComponent {
            return new SingleCriteria(null, configuration.senderAddressFilterFieldName, value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.email_recipient_address) {
          return function (value): ICriteriaComponent {
            return new SingleCriteria(null, configuration.recipientAddressFilterFieldName, value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.email_recipient_domains) {
          return function (value): ICriteriaComponent {
            return new SingleCriteria(null, configuration.recipientDomainFilterFieldName, value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.email_sender_domain) {
          return function (value): ICriteriaComponent {
            return new SingleCriteria(null, configuration.senderDomainFilterFieldName, value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.root_folders) {
          return function (value): ICriteriaComponent {
            return new SingleCriteria(null, configuration.rootFolderNameLike, value, EFilterOperators.contains, value,null,originalCriteriaOperationId);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.extensions) {
          var dtoExtension =  Entities.DTOExtension.Empty();
          return function(value):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileExtensionFilterFieldName ,value,EFilterOperators.equals,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.patterns) {
          var dTORegulation =  Entities.DTORegulation.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.patternNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.patterns_multi) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.patternManyNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_type_categories) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileTypeCategoryNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.media_type) {

          return function(value,name=null):ICriteriaComponent {
            if(value.toLowerCase()=='sp' || value.toLowerCase()=='sharepoint'){
              value = Operations.EMediaType.SHARE_POINT
            }
            else if(value.toLowerCase()=='fs'||value.toLowerCase()=='fileShare'){
              value = Operations.EMediaType.FILE_SHARE
            }
            else if(value.toLowerCase()=='od'||value.toLowerCase()=='onedrive'){
              value = Operations.EMediaType.ONE_DRIVE
            }
            else if(value.toLowerCase()=='ex'||value.toLowerCase()=='exchange'){
              value = Operations.EMediaType.EXCHANGE365
            }
            else if(value.toLowerCase()=='box'){
              value = Operations.EMediaType.BOX
            }
            return  new SingleCriteria(null, configuration.mediaTypeFilterFieldName,value,EFilterOperators.equals,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.files) {
          var dtoType3 =  Entities.DTOFileInfo.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.file, propertiesUtils.propName(dtoType3, dtoType3.fileName), value, EFilterOperators.contains,value,null,originalCriteriaOperationId);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.folders || entityDisplayTypes == EntityDisplayTypes.folders_flat ) {
          var dtoType3 =  Entities.DTOFileInfo.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.file, 'fullName', value, EFilterOperators.contains,value,null,originalCriteriaOperationId);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.regulations) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.regulationNameFilterFieldName,value,EFilterOperators.equals,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.tag_types) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileTagTypeNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.doc_types) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.subDocTypeNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.departments) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.subDepartmentNameFilterFieldName,value,EFilterOperators.contains,value,null,originalCriteriaOperationId );
          }
        }
      }

      myFactory.createFilterByID=function(entityDisplayTypes:EntityDisplayTypes,displayedFilterType?):(value,name) => ICriteriaComponent
      {

        if(entityDisplayTypes == EntityDisplayTypes.owners) {
          var dtoType =  Entities.DTOOwner.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(EFilterContextNames.owner, propertiesUtils.propName(dtoType, dtoType.user),value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.labels) {
          var dtoLabel =  Entities.DTOLabel.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.labelFilterFieldName ,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.share_permissions_allow_read) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.sharePermissionFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.contents) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileContentFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.extensions) {
          var dtoExtension =  Entities.DTOExtension.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileExtensionFilterFieldName ,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.regulations) {
          var dTORegulation =  Entities.DTORegulation.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.regulationFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.metadatas) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.metadataFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.metadatas_type) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.metadataTypeFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.patterns) {
          var dTORegulation =  Entities.DTORegulation.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.patternFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.patterns_multi) {
          var dTORegulation =  Entities.DTORegulation.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.patternMultiFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_type_categories) {
          var dTOFileTypeCategory =  Entities.DTOFileTypeCategory.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileTypeCategoryFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_sizes) {
          var dTOFileSizePartition =  Entities.DTOFileSizePartition.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.fileSizeFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.groups) {
          var dtoType1 =  Entities.DTOGroup.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.group, propertiesUtils.propName(dtoType1, dtoType1.id), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.folders ||entityDisplayTypes == EntityDisplayTypes.folders_flat ) {
          var dtoType2 = Entities.DTOFolderInfo.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.folder, propertiesUtils.propName(dtoType2, dtoType2.id), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.files) {
          var dtoType3 =  Entities.DTOFileInfo.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.file, propertiesUtils.propName(dtoType3, dtoType3.id), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.email_recipient_address  ) {
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(null, configuration.recipientAddressFilterFieldName, value.toLowerCase(), EFilterOperators.equals,name,displayedFilterType);
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.email_sender_address  ) {
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(null, configuration.senderAddressFilterFieldName, value.toLowerCase(), EFilterOperators.equals,name,displayedFilterType);
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.email_recipient_domains  ) {
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(null, configuration.recipientDomainFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.email_sender_domain  ) {
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(null, configuration.senderDomainFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }

        if(entityDisplayTypes == EntityDisplayTypes.acl_reads  ) {
          var dtoType5 =  Entities.DTOFileUser.Empty();
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(EFilterContextNames.acl_read, propertiesUtils.propName(dtoType5, dtoType5.user), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.acl_writes ) {
          var dtoType7 =  Entities.DTOFileUser.Empty();
          return function(value,name=null) :ICriteriaComponent{
            return new SingleCriteria(EFilterContextNames.acl_write, propertiesUtils.propName(dtoType7, dtoType7.user), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.tags) {
          var dtoType8 =  Entities.DTOFileTag.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null, configuration.tagFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.tag_types) {
          var dtoType9 =  Entities.DTOFileTagType.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null, configuration.tagTypeFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.root_folders) {
          var dtoType10 =  Entities.DTOFolderInfo.Empty();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(EFilterContextNames.root_folder, propertiesUtils.propName(dtoType10, dtoType10.id), value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.doc_types) {

          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null, configuration.docTypeFilterFieldName,value, EFilterOperators.equals, name,displayedFilterType);
            //  return new SingleCriteria(EFilterContextNames.doc_type, propertiesUtils.propName(dtoType11, dtoType11.id), value, EFilterOperators.equals,name);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.bizlistItems_clients) {
          var dtoType12=  Entities.DTOSimpleBizListItem.Empty();
          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.bizList_item_clients.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null, configuration.bizListItemAssociationFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.bizlistItem_clients_extractions) {
          var dtoType13 =  Entities.DTOSimpleBizListItem.Empty();
          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.bizList_item_clients_extraction.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemExtractionFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.bizlistItem_consumers_extraction_rules) {

          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.bizList_item_consumers_extraction_rule.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemExtractionRuleFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_created_dates) {
         var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.file_created_date.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.fileCreatedDatesFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_modified_dates) {
          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.file_modified_date.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.fileModifiedDatesFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.file_accessed_dates) {
          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.file_created_date.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.fileAccessedDatesFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.functional_roles) {
          var displayedFilterType=displayedFilterType?displayedFilterType:EFilterContextNames.functional_role.toString();
          return function(value,name=null):ICriteriaComponent {
            return new SingleCriteria(null,configuration.functionalRoleFilterFieldName, value, EFilterOperators.equals,name,displayedFilterType);
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.departments) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.subDepartmentFilterFieldName ,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.departments_flat) {
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.departmentFilterFieldName ,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.matters) {
          var dtoMatterType =  Entities.DTODepartment.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.subDepartmentFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
        if(entityDisplayTypes == EntityDisplayTypes.matters_flat) {
          var dtoMatterType =  Entities.DTODepartment.Empty();
          return function(value,name=null):ICriteriaComponent {
            return  new SingleCriteria(null, configuration.departmentFilterFieldName,value,EFilterOperators.equals,name,displayedFilterType );
          }
        }
      };

      myFactory.createContentSearch=function() {
        var displayedFilterType=EUserSearchMode.content.toString();
        return function(value:string) {
          var originalCriteriaOperationId=propertiesUtils.creatUuid();
          //return {fieldName: FilterFactory.contentFilterFieldName, value: value, operator: EFilterOperators.contains};
          return new SingleCriteria(null,configuration.contentFilterFieldName, value, EFilterOperators.contains,value,displayedFilterType,originalCriteriaOperationId);
        }
      };
      myFactory.isInvertedFilterEnabled=function(criteria:SingleCriteria){
        return criteria &&
          ((criteria.operator != EFilterOperators.contains) || (criteria.operator == EFilterOperators.contains && criteria.fieldName !== configuration.contentFilterFieldName))&&
          (criteria.fieldName !== configuration.noneMailFilterFieldName) &&
          (criteria.fieldName !== configuration.mailFilterFieldName);
      };
      myFactory.createTagFilter=function(){
        var displayedFilterType=EFilterContextNames.tag.toString();
        return function(tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators):ICriteriaComponent {
          if(tags && tags.length>0) {
            tags = angular.copy(tags);
            var first = new SingleCriteria(null, configuration.tagFilterFieldName, tags[0].id, EFilterOperators.equals, tags[0].name,displayedFilterType);
            tags.splice(0, 1);
            var criterias = [];
            tags.forEach(t=>criterias.push(new SingleCriteria(null, configuration.tagFilterFieldName, t.id, EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createExplicitTagAssociationFilter=function(){
        var displayedFilterType=EFilterContextNames.tag.toString();
        return function(tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators):ICriteriaComponent {
          if(tags && tags.length>0) {
            tags = angular.copy(tags);
            var first = new SingleCriteria(null, configuration.explicitTagAssociation, tags[0].id, EFilterOperators.equals, tags[0].name,displayedFilterType);
            tags.splice(0, 1);
            var criterias = [];
            tags.forEach(t=>criterias.push(new SingleCriteria(null, configuration.explicitTagAssociation, t.id, EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createMailFilter=function() {
        return function (value:number, displayText:string) {
          return new SingleCriteria(null, configuration.mailFilterFieldName, value, EFilterOperators.contains, displayText, displayText);
        }
      };


      myFactory.createTagTypeFilter=function(){
        var displayedFilterType=EFilterContextNames.tag_type.toString();
        return function(tags:Entities.DTOFileTagType[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators):ICriteriaComponent {
          if(tags && tags.length>0) {
            tags = angular.copy(tags);
            var first = new SingleCriteria(null, configuration.tagTypeFilterFieldName, tags[0].id, operator?operator:EFilterOperators.equals, tags[0].name,displayedFilterType);
            tags.splice(0, 1);
            var criterias = [];
            tags.forEach(t=>criterias.push(new SingleCriteria(null, configuration.tagTypeFilterFieldName, t.id, operator?operator:EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createExplicitTagTypeAssociationFilter=function(){
        var displayedFilterType=EFilterContextNames.tag_type.toString();
        return function(tags:Entities.DTOFileTagType[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators):ICriteriaComponent {
          if(tags && tags.length>0) {
            tags = angular.copy(tags);
            var first = new SingleCriteria(null, configuration.explicitTagTypeAssociation, tags[0].id, angular.isDefined(operator)?operator:EFilterOperators.equals, tags[0].name,displayedFilterType);
            tags.splice(0, 1);
            var criterias = [];
            tags.forEach(t=>criterias.push(new SingleCriteria(null, configuration.explicitTagTypeAssociation, t.id, angular.isDefined(operator)?operator:EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createDocTypeFilter=function(){
        var displayedFilterType=EFilterContextNames.doc_type.toString();
        return function(docTypes:Entities.DTODocType[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean , filterOperation? ):ICriteriaComponent {
          if(docTypes && docTypes.length>0) {
            docTypes = angular.copy(docTypes);
            var first = new SingleCriteria(null,isSubtreeFilter?configuration.subDocTypesFilterFieldName: configuration.docTypeFilterFieldName, docTypes[0].id, filterOperation ? filterOperation : EFilterOperators.equals, docTypes[0].name,displayedFilterType);
            docTypes.splice(0, 1);
            var criterias = [];
            docTypes.forEach(t=>criterias.push(new SingleCriteria(null, isSubtreeFilter?configuration.subDocTypesFilterFieldName: configuration.docTypeFilterFieldName, t.id, filterOperation ? filterOperation : EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createBizListItemAssociationTypeFilter=function(){
        var displayedFilterType=EFilterContextNames.bizList_item_clients.toString();
        return function(bizListType):ICriteriaComponent {
          if(bizListType) {
            var first = new SingleCriteria(null, configuration.bizListItemAssociationTypeFilterFieldName,bizListType, EFilterOperators.equals,bizListType,displayedFilterType);
            return first;
          }
        }
      };

      myFactory.createBizListItemExtractionTypeFilter=function(displayedFilterType){

        return function(bizListType):ICriteriaComponent {
          if(bizListType) {
            var first = new SingleCriteria(null, configuration.bizListItemExtractionTypeFilterFieldName,bizListType, EFilterOperators.equals,bizListType,displayedFilterType);
            return first;
          }
        }
      };

      myFactory.createSubDocTypesFilter=function(){
        var displayedFilterType='SubDocType';
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            return new SingleCriteria(null, configuration.subDocTypesFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
          }
        }
      };

      myFactory.createSubFolderFilter=function(){
        var displayedFilterType='Subfolder';
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            return new SingleCriteria(null, configuration.subFolderFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
          }
        }
      };

      myFactory.createDepartmentFilter=function(){
        var displayedFilterType=EFilterContextNames.department.toString();
        return function(department:Entities.DTODepartment,criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ):ICriteriaComponent {
          if(department) {
            department = angular.copy(department);
            return new SingleCriteria(null,isSubtreeFilter?configuration.subDepartmentFilterFieldName:configuration.departmentFilterFieldName, department.id, filterOperation ? filterOperation : EFilterOperators.equals, department.name,displayedFilterType);
          }
        }
      };

      myFactory.createDepartmentsFilter=function(){
        var displayedFilterType=EFilterContextNames.department.toString();
        return function(departments:Entities.DTODepartment[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ):ICriteriaComponent {
          if(departments && departments.length > 0) {
            departments = angular.copy(departments);
            var first = new SingleCriteria(null, isSubtreeFilter?configuration.subDepartmentFilterFieldName:configuration.departmentFilterFieldName, departments[0].id, filterOperation ? filterOperation : EFilterOperators.equals, departments[0].name,displayedFilterType);
            departments.splice(0, 1);
            var criterias = [];
            departments.forEach(t=>criterias.push(new SingleCriteria(null, isSubtreeFilter?configuration.subDepartmentFilterFieldName:configuration.departmentFilterFieldName, t.id, filterOperation ? filterOperation : EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createSubDepartmentFilter=function(){
        var displayedFilterType='SubDepartment';
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            //return new SingleCriteria(null, configuration.subDepartmentFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
            return new SingleCriteria(null, configuration.subDepartmentFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
          }
        }
      };

      myFactory.createMatterFilter=function(){
        var displayedFilterType=EFilterContextNames.matter.toString();
        return function(matter:Entities.DTODepartment,criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ):ICriteriaComponent {
          if(matter) {
            matter = angular.copy(matter);
            return new SingleCriteria(null,isSubtreeFilter?configuration.subMatterFilterFieldName:configuration.matterFilterFieldName, matter.id, filterOperation ? filterOperation : EFilterOperators.equals, matter.name,displayedFilterType);
          }
        }
      };

      myFactory.createMattersFilter=function(){
        var displayedFilterType=EFilterContextNames.matter.toString();
        return function(matters:Entities.DTODepartment[],criteriaOperation:ECriteriaOperators, isSubtreeFilter?:boolean, filterOperation? ):ICriteriaComponent {
          if(matters && matters.length > 0) {
            matters = angular.copy(matters);
            var first = new SingleCriteria(null, isSubtreeFilter?configuration.subMatterFilterFieldName:configuration.matterFilterFieldName, matters[0].id, filterOperation ? filterOperation : EFilterOperators.equals, matters[0].name,displayedFilterType);
            matters.splice(0, 1);
            var criterias = [];
            matters.forEach(t=>criterias.push(new SingleCriteria(null, isSubtreeFilter?configuration.subMatterFilterFieldName:configuration.matterFilterFieldName, t.id, filterOperation ? filterOperation : EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createSubMatterFilter=function(){
        var displayedFilterType='SubMatter';
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            //return new SingleCriteria(null, configuration.subDepartmentFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
            return new SingleCriteria(null, configuration.subMatterFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
          }
        }
      };

      myFactory.createUnassociatedFilter=function(entityDisplayTypes:EntityDisplayTypes,displayedFilterType?):() => ICriteriaComponent {
        var unassociatedText = myFactory.getUnassociatedText(entityDisplayTypes);
        if (entityDisplayTypes == EntityDisplayTypes.groups ) {
          var dtoType1 =  Entities.DTOGroup.Empty();
          return function ():ICriteriaComponent {

            return new SingleCriteria(EFilterContextNames.group, propertiesUtils.propName(dtoType1, dtoType1.id), '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.bizlistItem_clients_extractions  ) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemExtractionFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.bizlistItems_clients  ) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemAssociationFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.bizlistItem_consumers_extraction_rules  ) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemExtractionRuleFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.bizlistItem_clients_extractions  ) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.bizListItemExtractionFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.tags) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.tagFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.tag_types) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.tagTypeFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.doc_types) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.docTypeFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.departments) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.subDepartmentFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.departments_flat) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.departmentFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.matters) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.subMatterFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
        if (entityDisplayTypes == EntityDisplayTypes.matters_flat) {
          return function ():ICriteriaComponent {
            return new SingleCriteria(null,configuration.matterFilterFieldName, '*', EFilterOperators.notEquals, unassociatedText, displayedFilterType);
          }
        }
      };


      myFactory.createFileDuplicationsFilter=function(){
        var displayedFilterType='Duplications';
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            return new SingleCriteria(null, configuration.fileDuplicationsFilterFieldName, id, EFilterOperators.equals, name,displayedFilterType);
          }
        }
      };

      myFactory.createDateRangeFilter=function(){
        var displayedFilterType='Date range';
        return function(dateRanges:Entities.DTODateRangeItem[],criteriaOperation:ECriteriaOperators,filterFieldType,operator?:EFilterOperators):ICriteriaComponent {
          if(dateRanges && dateRanges.length>0) {
            dateRanges = angular.copy(dateRanges);
            var first = new SingleCriteria(null, filterFieldType,dateRanges[0],operator?operator: EFilterOperators.equals, dateRanges[0].name,displayedFilterType);
            dateRanges.splice(0, 1);
            var criterias = [];
            dateRanges.forEach(t=>criterias.push(new SingleCriteria(null,filterFieldType, t, operator?operator: EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

      myFactory.createRootFolderNameLikeFilter=function(){
        var displayedFilterType='Rootfolder search';
        return function(textSearch:string):ICriteriaComponent {
          if(textSearch && textSearch.trim()!='') {
           return new SingleCriteria(null, configuration.rootFolderNameLike,textSearch, EFilterOperators.equals, textSearch,displayedFilterType);
          }
        }
      };

      myFactory.createAllByFieldFilter=function(){
        return function(fieldName:string,filterOperator:EFilterOperators,displayText:string):ICriteriaComponent {
            return new SingleCriteria(null, fieldName,'*', filterOperator, displayText);
        }
      };

      myFactory.createFunctionalRoleFilter=function(){
        return function(funRoles:Users.DtoFunctionalRole[],criteriaOperation:ECriteriaOperators,operator?:EFilterOperators):ICriteriaComponent {
          var filterFieldType = "functionalRole";
          if(funRoles && funRoles.length>0) {
            funRoles = angular.copy(funRoles);
            var first = new SingleCriteria(null, configuration.functionalRoleFilterFieldName,funRoles[0].id,operator?operator: EFilterOperators.equals, funRoles[0].name,filterFieldType);
            funRoles.splice(0, 1);
            var criterias = [];
            funRoles.forEach(t=>criterias.push(new SingleCriteria(null, configuration.functionalRoleFilterFieldName, t.id, operator?operator: EFilterOperators.equals,t.name,filterFieldType)));
            if(criterias.length==0) {
              return first;
            }
            if(criteriaOperation == ECriteriaOperators.and) {
              return new AndCriteria([first], criterias);
            }
            return new OrCriteria([first], criterias);
          }
        }
      };

       myFactory.createTagTypeMismatchFilter=function(){
        var displayedFilterType=EFilterContextNames.tag_type.toString()+' mismatch';
        return function(tags:Entities.DTOFileTagType[]):ICriteriaComponent {
          if(tags && tags.length>0) {
            tags = angular.copy(tags);
            var first = new SingleCriteria(null, configuration.tagTypeMismatchFilterFieldName, tags[0].id, EFilterOperators.equals, tags[0].name,displayedFilterType);
            tags.splice(0, 1);
            var criterias = [];
            tags.forEach(t=>criterias.push(new SingleCriteria(null, configuration.tagTypeMismatchFilterFieldName, t.id, EFilterOperators.equals,t.name,displayedFilterType)));
            if(criterias.length==0) {
              return first;
            }

            return new OrCriteria([first], criterias);
          }
        }
      };


      myFactory.createBizListItemStateFilter=function(state){
        return function():ICriteriaComponent {
          var dtoType10 =  Entities.DTOSimpleBizListItem.Empty();
          var first = new SingleCriteria(null,  propertiesUtils.propName(dtoType10, dtoType10.state),state, EFilterOperators.equals);
          return first;

        }
      };
      myFactory.createFilterByUngrouped=function(){
        var dtoType1 =  Entities.DTOGroup.Empty();
        return  new SingleCriteria(null, configuration.ungroupedFilterFieldName,'*',EFilterOperators.equals,'Analyzed documents','Ungrouped');
        //return new SingleCriteria(null, configuration.ungroupedFilterFieldName, '', EFilterOperators.equals, 'Ungrouped');

        //  return new SingleCriteria(EFilterContextNames.group, propertiesUtils.propName(dtoType1, dtoType1.id), '*', EFilterOperators.notEquals,myFactory.getUnassociatedText(EntityDisplayTypes.groups),null);
      };
      myFactory.createFilterByExpiredUser=function(){
        return  new SingleCriteria(null, configuration.expiredUserFilterFieldName,'*',EFilterOperators.equals,'Expired user','Acl R/W');
      };
      myFactory.createFilterBySpecificProcessingState=function(state){
        return  new SingleCriteria(null, configuration.fileSpecificProcessingStateFilterFieldName,state,EFilterOperators.equals,state);
      };
      myFactory.createFilterByGeneralProcessingState=function(state){
        return  new SingleCriteria(null, configuration.fileGeneralProcessingStateFilterFieldName,state,EFilterOperators.equals,state);
      };
      myFactory.createFilterByGroupDetachedFiles=function(){
        var dtoType1 =  Entities.DTOFileInfo.Empty();
        return new SingleCriteria(EFilterContextNames.file, propertiesUtils.propName(dtoType1, dtoType1.analyzeHint), Entities.EAnalyzeHint.EXCLUDED, EFilterOperators.equals,'Detached',null);
      };
      myFactory.createFilterByNoneMailRelatedDataFiles=function(){
        return new SingleCriteria(null, configuration.noneMailFilterFieldName, false, EFilterOperators.equals, 'Without email related');
      };
      myFactory.createFilterByOpenAccessRights=function(){
        return new SingleCriteria(null, configuration.openAccessRightsFilterFieldName, '', EFilterOperators.equals, 'With open access rights');
      };

      myFactory.createSearchPatternsItemStateFilter=function(state){
        return function():ICriteriaComponent {
          var dtoType10 =  Operations.DTOSearchPattern.Empty();
          var first = new SingleCriteria(null,  propertiesUtils.propName(dtoType10, dtoType10.active),state, EFilterOperators.equals);
          return first;

        }
      };

      myFactory.createFullyContainedFilterById=function(isFullyContained:boolean, from:EFilterContextNames,to:EFilterContextNames,includeSubEntityDataLeft?:boolean,collapsedDataRight?:boolean){
        var fromName =from.toString()+'.id';
        if(from == EFilterContextNames.folder)
        {
          fromName = includeSubEntityDataLeft?configuration.subFolderFilterFieldName:fromName;
        }
        return function(id,name=null):ICriteriaComponent {
          if(id ) {
            return new SingleCriteria(null, to+'.'+configuration.fullyConatinedFilterFieldName+'.'+fromName, id,isFullyContained? EFilterOperators.equals:EFilterOperators.notEquals,
                name,to+' '+myFactory.isFullyContainedText(isFullyContained)+' in '+from);
          }
        }
      };
      myFactory.isFullyContainedText = function(isContained:boolean)
      {
        return this.getFullyContainedText(isContained?EFullyContainedFilterState.fullyContained:EFullyContainedFilterState.notContained);
      };
      myFactory.getFullyContainedText = function(val:EFullyContainedFilterState)
      {
        if(val== EFullyContainedFilterState.all)
        {
          return 'all';
        }
        else if(val==EFullyContainedFilterState.notContained)
        {
          return 'not contained';
        }
        else {
          return 'fully contained';
        }
      };
      myFactory.getUnassociatedText = function(entityDisplayType:EntityDisplayTypes) {
        if(entityDisplayType == EntityDisplayTypes.groups)
        {
         return 'Un-grouped';
        }
        return 'Unassociated';
      };




      myFactory.getFilterReportDisplayedName = function(tag:SingleCriteria) {

        if (filterNames[tag.fieldName]) {
          return filterNames[tag.fieldName];
        }

        if (tag.fieldName == configuration.departmentFilterFieldName) {
          if (currentUserDetailsProvider.isInstallationModeStandard()) {
            return 'Department';
          }
          else if (currentUserDetailsProvider.isInstallationModeLegal()) {
            return 'Matter';
          }
          return 'Department';
        }
        if (tag.fieldName == configuration.subDepartmentFilterFieldName) {
          if (currentUserDetailsProvider.isInstallationModeStandard()) {
            return 'Department subtree';
          }
          else if (currentUserDetailsProvider.isInstallationModeLegal()) {
            return 'Matter subtree';
          }
          return 'Department subtree';
        }
        if (tag.fieldName == configuration.subDepartmentNameFilterFieldName) {
          if (currentUserDetailsProvider.isInstallationModeStandard()) {
            return 'Department subtree';
          }
          else if (currentUserDetailsProvider.isInstallationModeLegal()) {
            return 'Matter subtree';
          }
          return 'Department subtree';
        }

        if (tag.fieldName.indexOf('.'+configuration.fullyConatinedFilterFieldName+'.')>0) {
          return 'Fully '+(tag.operator==EFilterOperators.equals?'':'not ')+ 'contained in';
        }

        if (tag.contextName) {
          return tag.contextName.toString();
        }
        return '';
      };

      myFactory.getFilterDataDisplayedText=function(filterData:FilterData):KeyValuePair[]
      {
        var result:KeyValuePair[]=[];

        var pagefilterTags = filterData.getPageFilter() ?filterData.getPageFilter().getAllCriteriaLeafs() : [];
        var pageRestricationFilterTags = filterData.getPageRestrictionFilter() ? filterData.getPageRestrictionFilter().getAllCriteriaLeafs() : [];
        var pageUserFilterTags = filterData.getPageUserSearchFilters() ? filterData.getPageUserSearchFilters().getAllUniqueCriteriaOperationLeafs(propertiesUtils) : [];
        var userFilterTag=filterData.getUserSearchFilter() ? filterData.getUserSearchFilter().getAllCriteriaLeafs() : [];

        pagefilterTags.forEach(tag =>      {
          result.push(new KeyValuePair(myFactory.getFilterReportDisplayedName(tag), tag.displayedText));
        });
        pageRestricationFilterTags.forEach(tag =>      {
          result.push(new KeyValuePair( myFactory.getFilterReportDisplayedName(tag),  tag.displayedText));
        });
        pageUserFilterTags.forEach(tag =>      {
          result.push(new KeyValuePair( tag.displayedFilterType,  tag.displayedText));
        });
        userFilterTag.forEach(tag =>      {
          result.push(new KeyValuePair(myFactory.getFilterReportDisplayedName(tag), tag.displayedText));
        });

        return result;
      };


      return  myFactory
    });

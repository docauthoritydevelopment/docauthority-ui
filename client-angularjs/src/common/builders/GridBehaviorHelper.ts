///<reference path='../../common/all.d.ts'/>
'use strict';



class GridBehaviorHelper {

  static createGrid<T> (scope, log:ILog,configuration,eventCommunicator:IEventCommunicator, userSettings:IUserSettingsService, getDataUrlParams, fields:ui.IFieldDisplayProperties[],
                        dataManipulationFn, rowTemplate, stateParams?, updateUrl?:string,
                        getDataUrlBuilder?:(restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig, includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState,collapsedData:boolean)=>string
                        ,totalElementsIsReturned?:boolean,fixHeight?:string) {
    log.debug('Init createGrid');

    var showGrid = false;//!scope.templateView;

     var gridBuilder:ui.IGridOptionsBuilder<T> = new KendoGridOptionsBuilder<T>();
     if (!getDataUrlBuilder) {
       scope.getDataUrlBuilder = GridBehaviorHelper.getDataUrlBuilder;
     }
     else {
       scope.getDataUrlBuilder = getDataUrlBuilder;
     }
    if(scope.$parent && scope.$parent.$parent && scope.$parent.$parent.workflowId)
    {
      let origUrlBuilderFn =  scope.getDataUrlBuilder;
      scope.getDataUrlBuilder = function(...args) {
          let url = origUrlBuilderFn(...args);
          url += ((url.indexOf('?') === -1) ? '?' : '&') + "workflowId="+ scope.$parent.$parent.workflowId;;
          return url;
       };
    }

     var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState);
     //if (scope.restrictedEntityDisplayTypeName && scope.restrictedEntityId) {
     //  scope.lazyFirstLoad = false; //force load if restricted id was already set.
     //}
     scope.dataManipulationFn = dataManipulationFn;
     scope.contextName = scope.entityDisplayType && scope.entityDisplayType instanceof EntityDisplayTypes ? EFilterContextNames.convertEntityDisplayType(scope.entityDisplayType) : null;
     scope.onDataCompleteFn = function (pageSize, pageSizeReduced, requestUrl) {
       scope.lastRequestUrl = requestUrl;
       var totalElementsCount = scope.elementOptions.getTotalElementsCount(scope.element);
       var pagingSupported = scope.elementOptions.getPageNumber(scope.element) != null;
       if (  scope.processExportFinished&&(pageSizeReduced || totalElementsCount == pageSize || !pagingSupported))
         scope.$apply(function () {
          scope.processExportFinished(pageSizeReduced);
         });

     };


     scope.getDirtyItems = function () {
       var grid = scope.element;
       var gridData = grid.data("kendoGrid");
       if (gridData) {
         var data = gridData.dataSource.data();
         var dirtyItems = data.filter(d=>d.dirty == true);
         return dirtyItems;
       }
       return null;
     }
     scope.addItem = function (dataItem) {
       dataItem.dirty = true;
       var grid = scope.element;
       var gridData = grid.data("kendoGrid");
       if (gridData) {
         gridData.dataSource.add(dataItem);

       }
       return null;
     }
     scope.manipulateReadRequest = function (params) {
       if (scope.findId) {
         params['selectedItemId'] = scope.findId;
         scope.changeSelectedItem = scope.findId;
         log.debug('Added selectedItemId to request: ' + scope.findId);
         scope.findId = null;
       }
       return params;
     }
     scope.onErrorReadResponse = function (status, url, error:any) {
       if (status == 400) {
         eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxSetState, {
           action: 'userSearchInputError',

         });
       }
       if (status == 500 || status == 404 || status == 400) {
         gridOptions.clear(scope.element);
       }
     }


     scope.$on("$destroy", function () {
       eventCommunicator.unregisterAllHandlers(scope);
     });
     scope.totalAggregatedCountFn = function (totalAggregatedCount:number, pageNumber:number, data:any[]) {
       log.debug(scope.elementName + ' total elements ' + totalAggregatedCount);

       scope.totalElements = totalAggregatedCount;
       var grid = scope.element;
       if (grid) {
         var gridPageNumber = scope.elementOptions.getPageNumber(grid);
         if (pageNumber && pageNumber != gridPageNumber && gridPageNumber != null) {
           data = null; //in findID case response page may be different then the requested page
           scope.dirty = true;
           scope.elementOptions.setPage(grid, pageNumber);
         }
       }
     }
     var addionalSheetToExcel;
     scope.addSheetToExcel = function (newSheet) {
       addionalSheetToExcel = newSheet;
     }
     var onExportToExcelPreparations = function (workbook) {
       if (addionalSheetToExcel) {
         workbook.sheets.push(addionalSheetToExcel);
         addionalSheetToExcel = null;
       }
       return workbook;
     };

   !scope.isTreeList&&(rowTemplate!=''&&rowTemplate!=null)? fields.forEach(f=> delete f.width):null;

     var gridOptions:ui.IGridHelper = gridBuilder.createGrid(url, getDataUrlParams, fields, configuration.max_data_records_per_request, dataManipulationFn, scope.totalAggregatedCountFn,
         !scope.lazyFirstLoad ? true : <boolean>!scope.lazyFirstLoad, showGrid ? null : rowTemplate, updateUrl,
         scope.filterData ? (<ui.IDisplayElementFilterData>scope.filterData).filter : null, scope.onDataCompleteFn, scope.manipulateReadRequest, scope.onErrorReadResponse,
         totalElementsIsReturned, onExportToExcelPreparations);
     scope.elementOptions = gridOptions;
     scope.totalElementsIsReturned = totalElementsIsReturned;
     if (fixHeight) {
       scope.elementOptions.gridSettings.height = fixHeight;
     }

     scope.mainGridOptions = scope.elementOptions.gridSettings;


     return scope.elementOptions;
  }

  static createTreeList<T> (scope, log:ILog,configuration,eventCommunicator:IEventCommunicator,  userSettings:IUserSettingsService, getDataUrlParams, fields:ui.IFieldDisplayProperties[],
                        dataManipulationFn, rowTemplate, stateParams?, updateUrl?:string,
                        getDataUrlBuilder?:(restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig, includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState,collapsedData:boolean)=>string
      ,totalElementsIsReturned?:boolean,fixHeight?:string) {


    scope.isTreeList = true;
    var gridOptions=GridBehaviorHelper.createGrid<T>(scope, log,configuration,eventCommunicator, userSettings, getDataUrlParams, fields,
        dataManipulationFn, rowTemplate, stateParams, updateUrl,
        getDataUrlBuilder
        ,totalElementsIsReturned,fixHeight);
    var onCollapse = function(e)
    {

    }
    var onExpand = function(e)
    {

    }


    var treeBuilder: ui.ITreeListOptionsBuilder<T> = new KendoTreeListOptionsBuilder<T>();
    scope.elementOptions  = treeBuilder.createFromGridOptions<T>(gridOptions.gridSettings,fields,scope.filterData,onCollapse,onExpand);

    scope.mainTreeListOptions = scope.elementOptions.gridSettings;


    return scope.elementOptions;

  }

  static getDataUrlBuilder = function (restrictedEntityDisplayTypeName:string,restrictedEntityId:any,entityDisplayType:EntityDisplayTypes, configuration:IConfig,
                                       includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState,collapsedData:boolean) {

    if (restrictedEntityDisplayTypeName && restrictedEntityId) {
      var currentRestricted = restrictedEntityDisplayTypeName;
      if (restrictedEntityDisplayTypeName == EntityDisplayTypes.folders_flat.toString()) {
        currentRestricted = EntityDisplayTypes.folders.toString();
      }
      else if (restrictedEntityDisplayTypeName == EntityDisplayTypes.departments_flat.toString()) {
        currentRestricted = EntityDisplayTypes.departments.toString();
      }
      else if (restrictedEntityDisplayTypeName == EntityDisplayTypes.matters_flat.toString()) {
        currentRestricted = EntityDisplayTypes.matters.toString();
      }
      //var currentRestricted = restrictedEntityDisplayTypeName == EntityDisplayTypes.folders_flat.toString() ? EntityDisplayTypes.folders.toString() : restrictedEntityDisplayTypeName; //no watch for folder_flat_info_id
      var restrictedEntityDisplayType = new EntityDisplayTypes(currentRestricted);

      return GridBehaviorHelper.getDataUrlBuilder2(entityDisplayType, restrictedEntityDisplayType, restrictedEntityId, configuration,includeSubEntityData,fullyContained,collapsedData);
    }
    return GridBehaviorHelper.getDataUrlBuilder2(entityDisplayType, null, null, configuration,includeSubEntityData,fullyContained,collapsedData);

  };
  static getDataUrlBuilder2 = function (entityDisplayType:EntityDisplayTypes,restrictedEntityDisplayType:EntityDisplayTypes,restrictedEntityId, configuration,
                                        includeSubEntityData:boolean,fullyContained:EFullyContainedFilterState,collapsedData:boolean)
  {
    try
    {
       if(restrictedEntityId &&restrictedEntityDisplayType)
      {
        var restrictedEntityDisplayTypeName = restrictedEntityDisplayType.toString();
        if(includeSubEntityData)
        {
          if(restrictedEntityDisplayType.equals(EntityDisplayTypes.folders)||restrictedEntityDisplayType.equals(EntityDisplayTypes.folders_flat)) {
            restrictedEntityDisplayTypeName = 'subfolders';
          }
          else if(restrictedEntityDisplayType.equals(EntityDisplayTypes.departments)||restrictedEntityDisplayType.equals(EntityDisplayTypes.departments_flat)) {
            restrictedEntityDisplayTypeName = 'subdepartments';
          }
          else if(restrictedEntityDisplayType.equals(EntityDisplayTypes.matters)||restrictedEntityDisplayType.equals(EntityDisplayTypes.matters_flat)) {
            restrictedEntityDisplayTypeName = 'submatters';
          }
          else if(restrictedEntityDisplayType.equals(EntityDisplayTypes.doc_types))
          {
            restrictedEntityDisplayTypeName = 'subdoc_types';
          }
        }

        let url = configuration[entityDisplayType+'_url'];
        if(!restrictedEntityDisplayType.equals(EntityDisplayTypes.all_files) ) {
          let baseFilterValuePart = 'baseFilterValue=' + encodeURIComponent(restrictedEntityId);
          let baseFilterFieldPart = 'baseFilterField=' + configuration[restrictedEntityDisplayTypeName + '_entityNameId'];
          if (url.indexOf("?")==-1) {
            url = url.concat('?');
          }
          else{
            url = url.concat('&');
          }
           url = url.concat(baseFilterValuePart, '&' + baseFilterFieldPart);
        }
        if(collapsedData && entityDisplayType.equals(EntityDisplayTypes.files)) {
          if (url.indexOf("?")==-1) {
            url = url.concat('?');
          }
          else{
            url = url.concat('&');
          }
          url = url.concat('collapseDuplicates=true');
        }
        else if(entityDisplayType.equals(EntityDisplayTypes.groups) && (restrictedEntityDisplayType.equals(EntityDisplayTypes.folders)||restrictedEntityDisplayType.equals(EntityDisplayTypes.folders_flat))) {
          let fullyContainedPart = !fullyContained||fullyContained==EFullyContainedFilterState.all?''
              :fullyContained==EFullyContainedFilterState.notContained?'fullyContained=false':'fullyContained=true';
          url =  url.concat('&'+fullyContainedPart);
        }

        return url;
      }
      return configuration[entityDisplayType+'_url'];//+'?'+fullyContainedPart;//+pageIDPart;
    }
   catch(e)
   {
     console.error('No url: '+restrictedEntityDisplayType.toString().substr(0, restrictedEntityDisplayType.toString().length - 1) + '_' + entityDisplayType + '_url');
   }
    return null;

  };

  static connect(scope,timeout,$window,log:ILog,configuration:IConfig,gridOptions:ui.IGridHelper,rowTemplate,fields:ui.IFieldDisplayProperties[],
                 onSelectedItemChanged,element,doNotSelectFirstRowByDefault?:boolean,marginBottom?:number,suppressResizeGrid?:boolean,
                 createAdditionalExcelSheets?:(successFunction:(newSheet)=>void)=>any, forceSelectFirstRowByDefault?:boolean)
  {
    scope.dateTime = Date.now();
    scope.element = element;
    var  rowTemplate= rowTemplate;
    resizeGrid();

    var isReadyForReload = function() {
      var grid = scope.element;
      if(scope.initialLoadParams){
        if( scope.initialLoadParams.includeSubEntityData!=null && scope.includeSubEntityData !=  scope.initialLoadParams.includeSubEntityData){
          return false;
        }
        // if(scope.fullyContainedFilter !=  scope.initialLoadParams.fullyContainedFilter){
        //   return false;
        // }
        if(scope.initialLoadParams.collapsedData!=null && scope.collapsedData !=  scope.initialLoadParams.collapsedData){
          return false;
        }
        if((scope.filterData != null &&  !scope.initialLoadParams.filterData) || //XOR
          (scope.filterData == null &&  scope.initialLoadParams.filterData)){
          return false;
        }
      }
      var requireRestrictedEntityId = (!scope.restrictedEntityDisplayTypeName ||  (scope.restrictedEntityId&&scope.restrictedEntityDisplayTypeName));
      var restrictedEntityTypeId = (!scope.restrictedEntityTypeId ||  (scope.entityTypeId&&scope.restrictedEntityTypeId));
      let isReady = grid &&  requireRestrictedEntityId &&restrictedEntityTypeId;
      if(isReady){
        scope.initialLoadParams=null;
      }
      return isReady;
    }
    scope.$watch(function () { return scope.restrictedEntityId; }, function (value,oldValue) {
      //rebing grid
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(isReadyForReload()) {
        var url =scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName,scope.restrictedEntityId,scope.entityDisplayType, configuration,scope.includeSubEntityData,scope.fullyContainedFilterState,scope.collapsedData);
        if(url) {
          loadGrid(grid, url, 1);
          //gridOptions.loadData(grid, url, 1);
        }
      }
      else if(scope.restrictedEntityDisplayTypeName && !scope.restrictedEntityId)
      {
        scope.clear();
      }

    });
    scope.clear=function()
    {
      var grid = scope.element;
      if(grid && gridOptions) {
        gridOptions.clear(grid);
      }
      scope.totalElements=null;
    }
    scope.$watch(function () { return scope.entityTypeId; }, function (value, oldValue) {
      //rebing grid
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(isReadyForReload()) {
        var url =scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName,scope.restrictedEntityId,scope.entityDisplayType, configuration,scope.includeSubEntityData,scope.fullyContainedFilterState,scope.collapsedData);
        if(url) {
          loadGrid(grid, url, 1);
        //  gridOptions.loadData(grid, url, 1);
        }
      }
    });
    scope.$watch(function () { return scope.restrictedEntityTypeId; }, function (value,oldValue) {
      //rebing grid
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(isReadyForReload()) {
        var url =scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName,scope.restrictedEntityId,scope.entityDisplayType, configuration,scope.includeSubEntityData,scope.fullyContainedFilterState,scope.collapsedData);
        if(url) {
          loadGrid(grid, url, 1);
          //gridOptions.loadData(grid, url, 1);
        }
      }
    });


  scope.getCurrentUrl = function()
    {
      var grid = scope.element;
      if(grid) {
        return gridOptions.getCurrentUrl(grid);
      }
      return null;
    };

    scope.getCurrentFilter = function():ui.IDisplayElementFilterData
    {
      return scope.filterData;
    };

    scope.selectItemIfNeeded=function(e, p)
    {
      var target = e.toElement || e.relatedTarget || e.target; //support Chrome/FF/IE
      if(!target)
      {
        return;
      }
      var theClickedRow = $(target).closest('tr');
      if(e.ctrlKey || e.shiftKey || !theClickedRow.hasClass('k-state-selected'))
      {
        theClickedRow.click(); //select row when ddl is opened. In case of multi select do no do that inorder not to lose the multi selection
      }

    }

    scope.handleSelectionChange = function(data) {
      scope.element.find('tr').removeClass('multi-state-selected');
      if(data && data.length==1) //consider only first selected item
      {
        scope.selectedItem=data[0];
        onSelectedItemChanged();
        var row = scope.element.find("tr[data-uid='" + data[0].uid + "']");
        scope.element.find('tr').removeClass('first-state-selected');
        row.addClass('first-state-selected');
      }
      else if(!data|| data.length==0)
      {
        scope.selectedItem=null;
        if( scope.itemSelected) {
          scope.itemSelected(null, null, null);
        }
      }
      else { //more then one item selected
        for(var i=0; i<data.length;i++) {
          var row = scope.element.find("tr[data-uid='" + data[i].uid + "']");
          if(!row.hasClass('first-state-selected'))
          {
            row.addClass('multi-state-selected');
          }

        }

      }
      scope.selectedItems=data;
      if(scope.itemsSelected) {
        scope.itemsSelected(scope.selectedItems);
      }
      var grid = scope.element;
      if(grid) {
        //       gridOptions.closeEditCellMode(grid);
      }

      scope.showSelectedRowsPath();
    };



    if(element) {
      element.on('$destroy', function () {
        log.debug('44444444444 destroy '+scope.elementName+'  DOM 444444444444444444444444444444 ID: '+scope.dateTime);
        scope.$destroy();
      });
    }
    scope.$on("$destroy", function() {
      log.debug('555555555555 destroy '+scope.elementName+' scope 5555555555555555555 ID: '+scope.dateTime);
    });
    scope.$watch(function () { return scope.exportToExcel; }, function (value) {
        excelExport();
     });

    scope.$watch(function () { return scope.templateView; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid&& scope.templateView!=null) {
        gridOptions.refreshTemplate(grid, scope.templateView?rowTemplate:null); //not working currently with kendo. need to recreate grid
      }
    });

    var excelExport = function()
    {
      var grid = scope.element;
      if(grid && scope.exportToExcel) {
        if(createAdditionalExcelSheets)
        {
          try {
            createAdditionalExcelSheets(
                function (newSheet) {
                  scope.addSheetToExcel(newSheet);
                  gridOptions.saveAsExcel(grid, scope.exportToExcel.fileName);
                }
            );
          }
          catch(e)
          {
            gridOptions.saveAsExcel(grid, scope.exportToExcel.fileName);
          }
        }
        else {
          gridOptions.saveAsExcel(grid, scope.exportToExcel.fileName);
        }
      }
    };

    scope.$watch(function () { return scope.exportToPdf; }, function (value) {
      var grid = scope.element;
      if(grid&& scope.exportToPdf) {
        gridOptions.saveAsPdf(grid, scope.exportToPdf.fileName);
      }
    });
    scope.$watch(function () { return scope.includeSubEntityData; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid) {
        if (isReadyForReload()) {
          var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
          if (url) {
            loadGrid(grid, url, 1);
            //  gridOptions.loadData(grid, url, 1);
          }
        }
      }
     });
    function loadGrid(grid,url,page){
      gridOptions.loadData(grid, url, page,
        scope.filterData ? (<ui.IDisplayElementFilterData>scope.filterData).filter : null, scope.filterData ? scope.filterData.isClientFilter : null,
        scope.sortData ? <any>(scope.sortData).fieldName : null, scope.sortData ? scope.sortData.descDirection : null);
    //  gridOptions.sort(grid, <any>(scope.sortData).fieldName,scope.sortData.descDirection)
    //  gridOptions.filterKendoFormat(grid, scope.filterData?(<ui.IDisplayElementFilterData>scope.filterData).filter:null,scope.filterData?scope.filterData.isClientFilter:null);
    }

    scope.$watch(function () { return scope.fullyContainedFilterState; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid) {
        if (isReadyForReload()) {
          var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
          if (url) {
            loadGrid(grid, url, 1);
            //gridOptions.loadData(grid, url, 1);
          }
        }
      }
    });
    scope.$watch(function () { return scope.collapsedData; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid) {
        if (isReadyForReload()) {
          var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
          if (url) {
            loadGrid(grid, url, 1);
            // gridOptions.loadData(grid, url, 1);
          }
        }
      }
    });


    function resizeGrid() {
      timeout(function() {
        var grid = scope.element;
        if(grid[0] ) {
          if(!suppressResizeGrid) {
            gridOptions.resizeGrid(grid, marginBottom );
          }
          gridOptions.scrollSelectedRowIntoView(grid);
        }
      });
    }
    var w = angular.element($window);
    w.bind('resize', function () {
      scope.$apply(resizeGrid);
    });
    //scope.$watch(function () { return $window.innerHeight; }, function (value) {
    //  resizeGrid();
    //});

    scope.$watch(function () { return scope.sortData; }, function (value, oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid&& scope.sortData) {
        log.debug('groups sort was requested: '+(scope.sortData).fieldName+'desc direction: '+scope.sortData.descDirection);
        loadGrid(grid,gridOptions.getCurrentUrl(grid),1);
       // gridOptions.sort(grid, <any>(scope.sortData).fieldName,scope.sortData.descDirection)
      }

    });


    scope.$watch(function () { return scope.filterData; }, function (value,oldValue) {
      var grid = scope.element;
      if((value==oldValue && !value)||(value && oldValue && angular.toJson(value.filter)==angular.toJson(oldValue.filter)))
      {
        return;
      }
      if(grid) {
        loadGrid(grid, gridOptions.getCurrentUrl(grid), 1);
         //gridOptions.filterKendoFormat(grid, scope.filterData?(<ui.IDisplayElementFilterData>scope.filterData).filter:null,scope.filterData?scope.filterData.isClientFilter:null);
      }
      else if(!grid&& scope.filterData) { //fiter was set befor the grid was created
        log.debug(' filter was requested: Waiting for grid to initialized');
      }


    });
  scope.$watch(function () { return scope.filterBySearchText; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
    if(grid) {
      if (isReadyForReload()) {
        var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
        if (url) {
          loadGrid(grid, url, 1);
          //  gridOptions.loadData(grid, url, 1);
        }
      }
    }
    });
    scope.$watch(function () { return scope.filterBySearchDateTime; }, function (value,oldValue) {
      var grid = scope.element;
      if(value==oldValue && !value)
      {
        return;
      }
      if(grid) {
        if (isReadyForReload()) {
          var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
          if (url) {
            loadGrid(grid, url, 1);
            //  gridOptions.loadData(grid, url, 1);
          }
        }
      }
    });

    var rowIdsToSelect=[];
    //refresh display from local dataSource data
    scope.reloadDataToGrid = function(itemIdsToSelect:number[])
    {
      var grid = scope.element;
        if(grid) {

          if(itemIdsToSelect)
          {
            rowIdsToSelect =itemIdsToSelect;
          }
          else {
       //     rowIdsToSelect = scope.selectedItems? scope.selectedItems.map(i=>i.id):[];
          }
          gridOptions.refreshDisplay(grid);

        }
    };
    //Read new request
    scope.refreshData = function(itemIdsToSelect:number[],findId?:number, scrollToSelected?:boolean)
    {
      var grid = scope.element;
      if(grid) {
        if (findId) {
          scope.findId = findId;
        }
        else if (itemIdsToSelect) {
          rowIdsToSelect = itemIdsToSelect;
        }
        else {
        //  rowIdsToSelect = scope.selectedItems ? scope.selectedItems.map(i=>i.id) : [];
        }
        gridOptions.reloadData(grid,function(){
          if (scrollToSelected) {
            timeout(function () {
              gridOptions.scrollSelectedRowIntoView(grid);
            });
          }
        });
       }
    };

    scope.refreshDataSilently = function(itemIdsToSelect:number[])
    {
      var grid = scope.element;
      grid.addClass('silent-loading');
      if(scope.getCurrentUrl()==null) //first load , no url yet
      {
        var url = scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName, scope.restrictedEntityId, scope.entityDisplayType, configuration, scope.includeSubEntityData, scope.fullyContainedFilterState, scope.collapsedData);
        if (url) {
          loadGrid(grid, url, 1);
        //  gridOptions.loadData(grid, url, 1);
        }
      }
      else {
        scope.refreshData();
      }

    };

    var getFieldType=function(fieldname:string)
    {
      var theField = fields.filter(f=> f.fieldName.indexOf(fieldname) > -1 )[0];
      return theField!=null?theField.type:'';
    };
    scope.unselectAll=function()
    {
      var grid = scope.element;
      if(grid) {

        rowIdsToSelect = [];
        gridOptions.unselectAll(grid);

      }
    }
    scope.dataBinding = function() //occures before dataBound
    {
   //   scope.onDataBindCompleted();

    };
    var selectedItemId;
    var firstDataBound=true;
    scope.onDataBound = function (e) {
      scope.templateView==null||(scope.templateView&&rowTemplate=='')? scope.element.addClass('no-template-row'):null; //for hiding table header only when grid actuallty has template row

      if(scope.dirty==true)
      {
        scope.dirty=false;
        return;
      }

      var grid = scope.element;

      gridOptions.registerToFocusEvent(grid);
      if (scope.totalElementsIsReturned || scope.totalElements > configuration['facet-count-hll.lower-bound']) {
        gridOptions.setOnlyOnePageStateClass(grid, scope.totalElements, configuration, scope.entityDisplayType); //for show/hide pager buttons
      }

      grid.removeClass('silent-loading');
      if(scope.totalElementsIsReturned) {
        // var grid = scope.element;
        grid.addClass('include-total-element');
      }
      if(scope.changePage)
      {
        var pageNumber = gridOptions.getPageNumber(grid);
        if(pageNumber && scope.changePage!=pageNumber) {
          var url =scope.getDataUrlBuilder(scope.restrictedEntityDisplayTypeName,scope.restrictedEntityId,scope.entityDisplayType, configuration,scope.includeSubEntityData ,scope.collapsedData);
          if(url) {
            loadGrid(grid, url, scope.changePage);
            //gridOptions.loadData(grid, url, scope.changePage);
          }
          //thread is back but page is async loaded
          return;
        }
      }
      scope.changePage = false;
      setPageNumberToUrl(e); //must be before selection change
      var rowsAreSelected = false;

      var needToScroll, scrollAfterLocalDataBound;
      if(scope.changeSelectedItem&&scope.changeSelectedItem!='') //first load when reading changeSelectedItem =>  rowIdsToSelect =null
      {
        selectedItemId= scope.changeSelectedItem;
        rowIdsToSelect=[scope.changeSelectedItem];
        scope.changeSelectedItem = null;
        scope.selectedItem=null;
        needToScroll=true;
      }

      if(scope.selectedItem) {
        selectedItemId = scope.selectedItem.id;
        rowIdsToSelect = rowIdsToSelect && rowIdsToSelect[0]?rowIdsToSelect:scope.selectedItems?scope.selectedItems.map(s=>(<any>s).id):null; //select newly requested items or forme ones
        //if (rowIdsToSelect && rowIdsToSelect[0]) {
        //  var selectedItemIsStillValidIndex = rowIdsToSelect.indexOf(scope.selectedItem.id);
        //  if (selectedItemIsStillValidIndex > -1) {
        //    rowIdsToSelect.splice(selectedItemIsStillValidIndex, 1);
        //  }
        //}

      }
      gridOptions.unselectAll(grid);
      var  rowsAreSelected:boolean=false;;
      if(rowIdsToSelect) {
        rowsAreSelected=  gridOptions.selectRowByDataItem(grid, rowIdsToSelect);

      }
      else if(selectedItemId) {
        rowsAreSelected = gridOptions.selectRowByDataItem(grid, [selectedItemId]);
      }

      rowIdsToSelect = null;
      selectedItemId=null;


      if ((!rowsAreSelected && !scope.restrictedEntityId &&!doNotSelectFirstRowByDefault) || (!rowsAreSelected && forceSelectFirstRowByDefault)) {
        selectFirstRow();
      }

      if(firstDataBound) //to solve bug in treelist height becomes over sized after first bind
      {
        resizeGrid();
        firstDataBound=false;
      }
      //     resizeGrid(); causes jumps for treelist in modal dialog

      hookHoverForRowIcon();
      scope.showSelectedRowsPath();
      //    hidePageButtonsIfNeeded ();
      if(scope.localDataBound) {
        scrollAfterLocalDataBound = scope.localDataBound();
      }
      scope.element.on('click','.dropdown-toggle',scope.selectItemIfNeeded);
      if(needToScroll || scrollAfterLocalDataBound) {
        timeout(function () {
          gridOptions.scrollSelectedRowIntoView(grid);
        });
      }
        //   scrollSelectedIntoView();
     };


    var hidePageButtonsIfNeeded = function()
    {
      var grid = scope.element;
      if(grid) {
        gridOptions.hidePageButtonsIfNeeded(grid);
      }
    };

    var hookHoverForRowIcon = function()
    {
      scope.element.on("mouseenter mouseleave",'.k-grid-content tr',function (e) {
        if(e.type=='mouseover' || e.type=='mouseenter') {
          $(this).find('i').removeClass("ng-hide");
        }
        else {
          $(this).find('i').addClass("no-hide");
        }
      });

      scope.element.on("mouseenter mouseleave",'.k-grid-content tr td',function (e) {
        if(e.type=='mouseover' || e.type=='mouseenter') {
          $(this).find('i').removeClass("no-visible");
        }
        else {
          $(this).find('i').addClass("no-visible");
        }
      });
      //var gridCell = scope.element.find('td');
      //gridCell.hover(
      //  function () {
      //    $(this).find('i').removeClass("ng-hide");
      //  },
      //  function() {
      //    $(this).find('i').addClass("ng-hide");
      //  }
      //);
    };

    var selectFirstRow=function()
    {
      var grid = scope.element;
      if(grid)
      {
        gridOptions.selectFirstRow(grid);
      }

    };

    scope.showSelectedRowsPath = function()
    {
      function getParentRow(rowElement) {
        var gridData = gridElement.data("kendoTreeList");
        var dataUid=$(rowElement).attr('data-uid');
        var dataItem = gridOptions.getRowDataByUid(gridElement,dataUid);
        var parentId = dataItem.parentId;
        if(parentId)
        {
          var parentUid = gridOptions.getData(gridElement).filter(d=>d.id == parentId)[0].uid;
          var parentRow = gridData.table.find('tr[data-uid="' + parentUid + '"]');
          return parentRow;
        }
        return null;

      }
      var gridElement = scope.element;
      if(gridElement) {
        var gridData = gridElement.data("kendoTreeList");
        if (gridData) {
          // var dataUid=$(gridData.select()).attr('data-uid');
          // var selectedElement:Entities.DTOGroup = gridOptions.getRowDataByUid(gridElement,dataUid);
         gridElement.find('.k-grid-content tr').removeClass("k-state-selected-parent");

          if(gridData.select().length>0) {
            var parentRow = getParentRow(gridData.select());
            while (parentRow) {
              $(parentRow).addClass("k-state-selected-parent");
              parentRow = getParentRow($(parentRow));
            }
          }


         }
      }
    };

    var setPageNumberToUrl = function(e)
    {
      var grid = scope.element;
      if(grid)
      {
        var page = gridOptions.getPageNumber(grid);
       if(scope.pageChanged)
       {
         scope.pageChanged(page);
       }
      }

      //if (pager) {
      //  pager.bind('change', function () {
      //    // set browser url param; use a single browser history location while inside controller
      //    $location.search({page: pager.page()});
      //    $location.replace();
      //  });
      //}
    };


  }



}

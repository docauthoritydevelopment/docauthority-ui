///<reference path='../all.d.ts'/>

'use strict';
angular.module('ui.builder.highcharts', [
])
  .factory('highChartsBuilder', function(propertiesUtils) {
    var myFactory:ui.IChartOptionsBuilder = <ui.IChartOptionsBuilder>{};

    myFactory.createBarChart = function(histogramData,chartTitle:string,
                                        fields:ui.IChartFieldDisplayProperties[],showTotalCount:boolean) :ui.IChartHelper{
      return HighChartOptionsBuilder.createBarChart(histogramData,chartTitle,fields,showTotalCount);
    };

    myFactory.createPie = function(histogramData,chartTitle:string,
                                   fields:ui.IChartFieldDisplayProperties[],
                                   seriesColors:string[]) :ui.IChartHelper{
      return HighChartOptionsBuilder.createPie(histogramData,chartTitle,fields,seriesColors);
    };

    myFactory.createStackedBar100Chart =  function (histogramData, chartTitle, fields) {
      return HighChartOptionsBuilder.createStackedBar100Chart(histogramData, chartTitle, fields);
    };

    return  myFactory
  });

class HighChartOptionsBuilder
//implements  ui.IChartOptionsBuilder
{
  static createPie(histogramData,chartTitle,fields,seriesColors): any {

    let valuesList = [];
    for (let count = 0; count < histogramData.length; count++) {
      valuesList.push({
        name : histogramData[count].name + '----' + ( histogramData[count].description ? histogramData[count].description : ''),
        y: histogramData[count].counter,
        sliced : histogramData[count].selected,
        extraInfo: histogramData[count].extraInfo
      });
    }
    return {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      tooltip: {
        formatter: function () {
          let values = this.key.split("----");
          let tooltip  = `<b> ${values[0]} </b>: <b> ${this.y} (${Number(this.point.percentage).toFixed(1)}%)</b>`;
          if(this.point.extraInfo) {
            tooltip += HighChartOptionsBuilder.addExtraInfoHtml(this.point.extraInfo);
          }
          return tooltip;
        },
        positioner: function (labelWidth, labelHeight, point) {
          var actualXLocation: number = point.plotX ;
          var actualYLocation: number = point.plotY ;

          var tooltipX, tooltipY;
          tooltipX = actualXLocation < this.chart.plotSizeX/2 ?  actualXLocation -  30 - labelWidth : actualXLocation + 40;
          if (tooltipX< 0 ) {
            tooltipX= 0 ;
          }
          if (tooltipX > this.chart.plotSizeX - labelWidth ) {
            tooltipX= this.chart.plotSizeX - labelWidth;
          }

          tooltipY = actualYLocation < this.chart.plotSizeY/2 ?  actualYLocation -  40 : actualYLocation + 30;
          if (tooltipY< 0 ) {
            tooltipY =  actualYLocation + 30
          }

          if (tooltipY > this.chart.plotSizeY - labelHeight) {
            tooltipY = actualYLocation -  40;
          }


          return {
            x: tooltipX,
            y: tooltipY
          };
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            padding: 0,
            enabled: true,
            style : {
              fontSize:'10px',
              fontWeight: 'normal'
            },
            formatter : function () {
              var values = this.key.split("----");
              var theText = values[0] +'<br><b>' + this.y + ',   ' + Number(this.point.percentage).toFixed(1) +'%</b>';
              if(this.point.extraInfo) {
                theText += HighChartOptionsBuilder.addExtraInfoHtml(this.point.extraInfo);
              }
              return theText;
            }
          }
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: '',
        data: valuesList
      }]
    };
  };

  static createStackedBar100Chart(histogramData, chartTitle: string,
                                  fields: ui.IChartFieldDisplayProperties[]): any {

    let colorsArray = ['#A0A700','#B52E31']
    let categoryList = [];
    let valuesList = [];
    for (let count = histogramData.series.length-1; count >= 0 ; count--) {
      valuesList.push({
        name : histogramData.series[count].name,
        data : histogramData.series[count].data,
        color : colorsArray[count],
        legendIndex: count
      });
    }
    return {
      chart: {
        type: 'bar',
        spacing: [0,10,0,10],
        marginTop: 25,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      yAxis: {
        min: 0,
        visible: false,
        minPadding: 0,
        maxPadding: 0
      },
      xAxis: {
        visible: false,
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        formatter: function () {
          return '<b>' + this.series.name +
            '</b>: <b>' + this.y + '</b>';
        }
      },
      plotOptions: {
        bar: {
          dataLabels : {
            enabled : true,
            inside: true
          }
        },
        series: {
          stacking: 'percent'
        }
      },
      legend: {
        enabled: true,
        verticalAlign: 'top',
        y : -5
      },
      credits: {
        enabled: false
      },
      series: valuesList
    };
  };

  static createBarChart(histogramData, chartTitle: string,
                        fields: ui.IChartFieldDisplayProperties[],showTotalCount): any  {

    let categoryList = [];
    let valuesList = [];
    let totalValuesList = [];
    for (let count = 0 ; count < histogramData.length ; count++) {
      categoryList.push(histogramData[count].name + '----' + ( histogramData[count].description ? histogramData[count].description : ''));
      valuesList.push(
        {
          y: histogramData[count].counter,
          color  : histogramData[count].selected  ? '#1ab394' : '#4F6990',
          extraInfo: histogramData[count].extraInfo
        });
      if (histogramData[count].totalCounter) {
        totalValuesList.push({
          y: histogramData[count].totalCounter,
          color  : '#aaaaaa'
        })
      }
    }
    let seriesVal = [{
      data: valuesList,
      color : "#4F6990",
      name : 'Filtered files number'
    }];
    if (showTotalCount && totalValuesList.length > 0 ) {
      seriesVal.push({
        data : totalValuesList,
        color : "#aaaaaa",
        name : 'Total files number'
      })
    }
    return {
      chart: {
        type: 'bar',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: categoryList,
        labels : {
          useHTML : false,
          formatter: function() {
            // split using -
            var values = this.value.split("----");
            return values[0];
          },
        },
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        backgroundColor: "#FFFFFF",
        formatter: function () {
          let values = this.x.split("----");
          let tooltip  = `<b> ${values[0]}</b>: <b> ${this.y} </b>`;
          if(this.point.extraInfo) {
            tooltip += HighChartOptionsBuilder.addExtraInfoHtml(this.point.extraInfo);
          }
          return tooltip;
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
            color: valuesList.length > 20 ? 'black' : null,
            formatter: function () {
              let lbl = `${this.y}`;
              if(this.point.extraInfo) {
                lbl += HighChartOptionsBuilder.addExtraInfoHtml(this.point.extraInfo);
              }
              return lbl;
            }
          },
          maxPointWidth: 30
        }
      },
      legend: {
        enabled : seriesVal.length > 1,
        borderWidth: 0,
        shadow: false
      },
      credits: {
        enabled: false
      },
      series: seriesVal
    };

  };


  static addExtraInfoHtml(extraInfo) {
    let html = '';

    extraInfo = extraInfo || {};

    if(!_.isEmpty(extraInfo.risk)) {
      html +=  `
        <span>
           <span>  Risk:</span>
           <span style="color: red; font-weight: 600">${extraInfo.risk}</span> 
        </span>`
    }
    if(!_.isEmpty(extraInfo.value)) {
      html +=  `
        <span>
           <span>  Value:</span>
           <span style="color: green; font-weight: 600">${extraInfo.value}</span> 
        </span>`
    }

    return html;
  }
}

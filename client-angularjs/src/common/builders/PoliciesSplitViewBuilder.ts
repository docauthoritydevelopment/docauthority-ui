class PoliciesSplitViewHtmlPartsFactory {
  static createViewNavOptions( tabType:EPolicyTabType,expandOnSelection?:boolean,enableRestrictNavId?:boolean)
  {
    var elementType="tree-list";
    var type = tabType.toString().replace('_','-')+'-nav-bar-'+elementType;
    var restrictNavIdPart = enableRestrictNavId?'restricted-entity-ids="restrictNavIds"':'';
    var  grid = '<'+type+' style="height:100%"  item-selected="onSelectNav" items-selected="onSelectItemsNav" '+restrictNavIdPart+' expand-on-selection="'+expandOnSelection+'"' +
        ' filter-data="filterNav" sort-data="sortNav" reload-data-to-grid="reloadDataNav" refresh-data="refreshDataNav" ' +
        'get-current-url="getNavUrl" get-current-filter="getNavFilter" unselect-all="unselectNav"  lazy-first-load="lazyFirstLoadNav"' +
        ' export-to-pdf="exportNavToPdf" find-id="findIdNav" export-to-excel="exportNavToExcel"  page-changed="pageNavChanged" change-page="changeNavPage"' +
        ' change-selected-item="changeSelectedNavId"   process-export-finished="exportNavToExcelCompleted" >' +
        ' </'+type+'>';

    return grid;
  }


  static createMainView(tabType:EPolicyTabType) {

   var elementType="grid";
    var tabPart = tabType==EPolicyTabType.objects?'object':'layer';
   var type = 'policy-'+tabPart+'-items-'+elementType;

    var  grid = '<'+type+' style="height:100%" restrictedEntityDisplayTypeName="" restricted-entity-id="selectedNavId" restricted-entity-item="selectedNavItem" item-selected="onSelectMainView" items-selected="onSelectItemsMainView"' +
        ' filter-data="filterMainView" sort-data="sortMainView" reload-data-to-grid="reloadDataMainView" refresh-data="refreshDataMainView" ' +
        'get-current-url="getMainViewUrl" get-current-filter="getMainViewFilter" unselect-all="unselectMainView"  lazy-first-load="lazyFirstLoadMainView"' +
        ' export-to-pdf="exportMainViewToPdf" find-id="findIdMainView" export-to-excel="exportMainViewToExcel"  page-changed="pageMainViewChanged"' +
        ' change-page="changeMainViewPage" change-selected-item="changeSelectedItemMainView"   process-export-finished="exportMainViewToExcelCompleted">' +
        ' </'+type+'>';

    return grid;
  }

}
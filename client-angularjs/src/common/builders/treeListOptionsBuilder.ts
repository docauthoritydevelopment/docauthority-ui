///<reference path='../all.d.ts'/>

'use strict';


class KendoTreeListOptionsBuilder<T> implements  ui.ITreeListOptionsBuilder<T>
{

  createFromGridOptions<T>(gridOptions:kendo.ui.GridOptions,fields,filterData,onCollapseFn,onExpandFn)
  {

    var options:KendoTreeListOptions = new KendoTreeListOptions();
    options.treeDisplaySettings = <any>gridOptions;
    angular.extend(options.treeDisplaySettings,
        {
          expand:onExpandFn,
          collapse:() => {
            $('html').trigger('click'); //to close all ddls
            if(onCollapseFn) {
              onCollapseFn();
            }
          }
        })

    angular.extend(options.treeDisplaySettings.dataSource.schema.model,
        {
            parentId: getParentIdField(),
        })

    function getParentIdField()
    {
      var p = fields.filter(function (f:ui.IHierarchicalFieldDisplayProperties) { return f.isParentId; });
      return p[0]!=null?p[0].fieldName:'';
    }


      angular.extend( options.treeDisplaySettings.columns[0],
          {
            expandable: true
          });
    (<any>options.treeDisplaySettings).pageable=false;
    options.treeDisplaySettings.dataSource.serverPaging=false;
    delete  options.treeDisplaySettings.dataSource.pageSize;




    var result = new KendoTreeListHelper(options.treeDisplaySettings);
   /// (<any>result).filterData = filterData; //my filter mechanishem since treelist filter doesnt work
    return result;

  }

  create<T>(elementName:string,url:string,urlParams,
                    fields:ui.IHierarchicalFieldDisplayProperties[],
                    maxRecordsPerRequest:number,
                    hasChildrenFn:(data:T)=> boolean,
                    dataManipulationFn:(data: T[])=> T[],
                    autoBind: boolean,
                    itemTemplate?:string,
                    filterData?:any,onDataCompleteFn?,onExcelPreparationsFn?,onCollapseFn?,onExpandFn?,onErrorReadResponseFn?:(status,url,error: any)=>void
  )
  {

    var options:KendoTreeListOptions = new KendoTreeListOptions();

    options.treeDisplaySettings = <kendo.ui.TreeListOptions>{
      autoBind: autoBind,

      animation: {
        collapse: {
          duration: 200,
          effects : "fadeOut"
        },
        expand  : {
          duration: 200,
          effects : "fadeIn"
        }
      },
   //  dragAndDrop: true,
   //   editable: {
   //     move: true
   //   },
      expand:onExpandFn,
      collapse:onCollapseFn,
      filterable: true,
      selectable: 'multiple, row',
      //height: $(window).innerHeight() - 256,
      scrollable:true,
      allowCopy: true,
      reorderable: true,
      resizable: true,
     //messages: {
     //   loading: "loading Folders..."
     // },
      //template: itemTemplate
      excelExport: function(e) { //Color the Alternating Rows
        var sheet = e.workbook.sheets[0];
        for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
          if (rowIndex % 2 == 0) {
            var row = sheet.rows[rowIndex];
            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex ++) {
              row.cells[cellIndex].background = "#aabbcc";
            }
          }
        }
        if(onExcelPreparationsFn) {
          onExcelPreparationsFn(e.workbook);
        }
        if(onDataCompleteFn)
        {
          onDataCompleteFn(false);
        }
      },
    };

    var transportCompleteHandler = function(jqXHR, statusText) {
      if (statusText === "success") {
         return;
      }
      //if (jqXHR.status == 200) {
      //
      //}
      if (jqXHR.status == 500 || jqXHR.status == 404) {
        console.error('WOW NO \n\n server error');
      }
      else if (jqXHR.status == 502) {
        console.error('Communication error');
      }
      else if (jqXHR.status == 400) {
        if(onErrorReadResponseFn) {
          onErrorReadResponseFn(jqXHR.status,jqXHR.url,jqXHR.responseJSON.message);
        }
        console.error('Bad request/search: ' + jqXHR.responseJSON.message);
      }

    };

    options.dataSource =<any>({
  //    filter:filterData,
      transport: {
        read: {
          url: url,
          beforeSend: function(jqXHR, settings) {
            jqXHR.url = settings.url;
            var auth = 'FREE-PASS';
            jqXHR.timeout =680000;  //in milliseconds
            jqXHR.setRequestHeader('DA-XSRF', auth);
          },
          //   data: urlParams || {},
          data: function (e)
          {
            if(e.pageSize>maxRecordsPerRequest)//limit max amount of files
            {
              e.pageSize =maxRecordsPerRequest;
              e.pageSizeReduced =true;
            }
            if(e.pageSize === maxRecordsPerRequest)
            {
              e.export = true;
            }
            if(e.take>maxRecordsPerRequest)//limit max amount of files
            {
              e.take =maxRecordsPerRequest;
              e.pageSizeReduced =true;
            }
            var params = urlParams?urlParams(e):null;

            if(e['item.id'] !=undefined) //for item.id convert to id
            {
              e['id'] = e['item.id'];
              delete e['item.id'];
            }

            if(params) {
              params = angular.extend(e, params);
            }
            return params;
          },
          error: function (xhr, error) {
            console.debug(xhr); console.debug(error);

          },
          complete: transportCompleteHandler
        },

      },
    });

    var schemaFields = fields.map(function(field:ui.IFieldDisplayProperties):kendo.data.DataSourceSchemaModelField
    {
      return {
        field: field.fieldName,
        type: field.type.toString(),
        editable: field.editable,
        nullable: field.nullable,
        parse: field.parse,

      }
    } );


    options.dataSchema = <any>{
      data: function (response){ //he configuration used to parse the remote service response.
        if(dataManipulationFn!=null) {
          if(response.content) {
            return dataManipulationFn((response.content))
          }
          return dataManipulationFn((response))
        }
        if(response.content) {
          return response.content;
        }
        return response;
      },
      //total: 'totalElements',
      model: {
        id:getPrimaryKey(),
        //  parentId: "ReportsTo",
        fields: schemaFields,
        parentId: getParentIdField(),
       // hasChildren:true
      }
    }

    function getPrimaryKey()
    {
      var p = fields.filter(function (f:ui.IFieldDisplayProperties) { return f.isPrimaryKey; });
      return p[0]!=null?p[0].fieldName:'id'
    }
    function getParentIdField()
    {
      var p = fields.filter(function (f:ui.IHierarchicalFieldDisplayProperties) { return f.isParentId; });
      return p[0]!=null?p[0].fieldName:'';
    }

    options.columns =[];
    fields.forEach(field =>
    {
      if(field.displayed) {
        options.columns.push( <kendo.ui.TreeListColumn> {
          field: field.fieldName,
          title: field.title,
          template: field.template,
          sortable: field.sortable,
          filterable: field.filterable,
          format:field.format,
          validation:field.validation,
          editor:field.editor,
          width:field.width,
          expandable: field.expandable,
          resizable:true,
          headerTemplate: '<span  title="'+field.title.replace('#','\\\\#')+'">'+field.title.replace('#','\\\\#')+'</span>'
        })
      }
    } );
    var result = new KendoTreeListHelper(options.treeDisplaySettings);
    (<any>result).filterData = filterData; //my filter mechanishem since treelist filter doesnt work
    return result;

  }





};

class KendoTreeListHelper implements ui.IGridHelper{
  constructor(public gridSettings)
  {
    this.gridSettings = gridSettings;
  }
  selectFirstRow(gridElement:any)
  {
    var treelist = gridElement.data("kendoTreeList");
    if(treelist) {
      var row =  treelist.items()[0];
      //   var row = gridElement.find('tr:first');
      // if(!gridData.select()) {
      var rowIndex = 0;
      var row = treelist.content.find("tr:visible").eq(rowIndex);
      treelist.select(row);
      //  }
    }
  }
  getSelectedRowsUid(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var selectedRow = gridData.select();
      if(selectedRow[0]) {
        //get data-uidws of all selected rows
        return selectedRow.map(function(){return $(this).attr("data-uid");}).get();
      }
    }
    return null;
  }
  getCurrentUrl(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var url = gridData.dataSource.transport.options.read.url;
      return url;
    }
    return null;
  }
  getCurrentFilter(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {

      if(gridData.dataSource.filter())
      {
        return gridData.dataSource.filter();
      }
    }
    return null;
  }
  unselectAll(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      gridData.clearSelection();
    }
  }
  selectRowByUid(gridElement:any,rowUidToSelect)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var newRow = gridData.content.find('tr[data-uid="' + rowUidToSelect + '"]');
      //   var row = gridElement.find('tr:first');
      if(newRow[0]) {
        gridData.select(newRow);
        return true;
      }
    }
    return false;
  }
  selectRowsByUid(gridElement:any,rowUidsToSelect:string[])
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData && rowUidsToSelect&& rowUidsToSelect[0]) {
      var selected =   this.selectRowByUid(gridElement,rowUidsToSelect[0]);
      rowUidsToSelect.splice(0,1);
      rowUidsToSelect.forEach(rowUid=>
      {
        var newRow = gridData.content.find('tr[data-uid="' + rowUid + '"]');
        if(newRow[0]) {
          newRow.addClass('k-state-selected');
          selected=true;
        }
      })
      return selected;
    }
    return false;
  }
  resizeGrid(gridElement:any,marginBottom:number)
  {
    marginBottom=marginBottom?marginBottom:20;
    var dataArea = gridElement.find(".k-grid-content");
    var pager = gridElement.find(".k-grid-pager");
    var header = gridElement.find(".k-grid-header");
    var otherGridElements = gridElement.children().not(".k-grid-content"); //This is anything ather than the Grid iteslf like header, commands, etc
    var otherGridElementsHeight=0, otherElementsHeight=0;

  //  Calcualte all Grid elements height
    otherGridElements.each((i,o)=>
    {
      otherGridElementsHeight += $(o).outerHeight(true);
    });

    var mainBody = $(".main-body-grid");
    var top = mainBody.offset().top;
    var modalBody=gridElement.closest(".modal-body")[0];
    if(modalBody){
      mainBody= $(modalBody);
    //  top=modalBody.offset().top;
    //  let dialogMinHeight = $(modalBody).outerHeight(true);
   //   let dialogHeight = $(modalBody).height();
   //   let modalFooter = $(modalBody).parent().find('.modal-footer')[0];
   //   otherElementsHeight = $(modalFooter).outerHeight(true)+((dialogMinHeight-dialogHeight)/2) ; //for 10px*2 padding
    }

    var mainBodyBottom = mainBody.offset().top + mainBody.height();
    var calculatedHeight = mainBodyBottom - gridElement.offset().top -  otherGridElementsHeight - otherElementsHeight;

    //set grid height
    $(dataArea).height(calculatedHeight- marginBottom );
    gridElement.height(calculatedHeight+ otherGridElementsHeight- marginBottom );
  }
  scrollSelectedRowIntoView(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData)
    {
      var selectedRow =gridElement.find(".k-grid-content .k-state-selected");
      if(selectedRow[0] )
      {
        var inView =  isElementInView(selectedRow);
        if(inView<0)
        {
          var totalScrollNeeded=0;
          var content = gridElement.select(".k-grid-content");
          if(selectedRow.offset().top<content.offset().top)
          {
            totalScrollNeeded= selectedRow.offset().top-content.offset().top;
          }
          else {

            totalScrollNeeded = inView * -1 + gridElement.find(".k-grid-content").scrollTop() + selectedRow.height() + 10;
          }
          gridElement.find(".k-grid-content").scrollTop(totalScrollNeeded);
          // content.scrollTop(inView*-1); //scroll the
        }
      }
    }

    function isElementInView(elem){
      var content = gridElement.select(".k-grid-content");
      var val = (content.offset().top+content.height())-( elem.offset().top+ elem.height());
      if(elem.offset().top<content.offset().top)
      {
        return elem.offset().top-content.offset().top;
      }
      return val ;
    }
  }
  setData(gridElement:any,data:any[])
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      gridData.dataSource.data(data)
    }

  }
  getData(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      return gridData.dataSource.data();
    }

  }
  getRowDataByUid(gridElement:any,uid:string)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var item = gridData.dataSource.data().filter(d=>d.uid==uid)[0];
      //   var row = gridElement.find('tr:first');
      return item;
    }
    return null;
  }

  getSelectedRowsDataItems(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var data = gridData.dataItems();
      var selectedUIDs = gridData.select();
      if (data && data.length > 0 && selectedUIDs && selectedUIDs[0]) {

        var items = [];
        for (var i = 0; i < selectedUIDs.length; i++) {
          var rowUid = $(selectedUIDs[i]).attr('data-uid');
          var selectedRowitem = data.filter(d=>d.uid == rowUid)[0];
          if(selectedRowitem) {
            items.push(selectedRowitem);
          }
        }
        return items;
      }
    }
    return null;
  }

  expandFirstItem(gridElement:any)
  {
    var treeList = gridElement.data("kendoTreeList");
    if(treeList ) {
      var row = treeList.content.find("tr")[0];

      treeList.expand(row);
    }
  }
  selectRowByDataItem(gridElement:any,dataItemIds:number[])
  {
    var _this=this;
    var result:any = false;
    var gridData = gridElement.data("kendoTreeList");
    if(gridData ) {
      var toExpandData;
      var view = gridData.dataSource.view();
      if (view && dataItemIds && dataItemIds[0]) {
        for (var i = 0, row; i < dataItemIds.length; i++) {
         _.some(view,  (v) => {
           if (v.id == dataItemIds[i]) {
             row = v;
             return true;
           }
         });
          if (row) {
            var parents = row.parents?row.parents:row.item.parents;
            var expanded=false;
            for (var j = 0; j < parents.length; j++) {
             var parentId = parents[j];

              var prow = view.filter(v=>v.id==parentId)[0];
              if(parentId==dataItemIds[i])
              {
                var trToSelect = $(gridElement).find("tr[data-uid='" + row.uid + "']");
                gridData.select(trToSelect);
                result = true;
              }
              if(prow&&!prow.expanded ) {

                var trTo = $(gridElement).find("tr[data-uid='" + prow.uid + "']");
                toExpandData = trTo;
              //  gridData.expand(trTo);
                expanded = true;
               result='na';
                break;;
              }
              else if(!prow)
              {
              }
            }

          }
        }

      }
    }
    if(toExpandData)
    {
      gridData.expand(toExpandData);
      _this.selectRowByDataItem(gridElement,dataItemIds);
    }
    return result;
  }

  selectPrevRow(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData ) {
      var dataSource = gridData.dataSource;
      var selected = this.getSelectedRowsDataItems(gridElement);
      var  index = dataSource.indexOf(selected[0]);
      var modelToSelect = dataSource.data()[(index-1)>=0?(index-1):selected.length<dataSource.data().length?selected.length:0];
      this.unselectAll(gridElement);
      return this.selectRowByDataItem(gridElement,[modelToSelect.id]);
    }
    return false;
  }
  setMultipleRowSelection(gridElement:any,enabled:boolean)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      if(enabled) {
        gridData.selectable = "multiple, row";
      }
      else
      {
        gridData.selectable = "row";
      }
    }
  }
  editCellMode = function(gridElement:any,targetElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var  cell = $(targetElement).closest('td');/* you have to find cell containing check box*/
      gridData.editCell(cell);
    }
  }

  closeEditCellMode = function(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      gridData.closeCell();
    }
  }

  getPageNumber(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      return gridData.dataSource.page(); //null if no paging
    }
    return null;
  }
  sort(gridElement:any,sortField:string,descDirection:boolean)
  {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      // get currently applied filters from the Grid.
      var sort = [];

      sort.push({field: sortField, dir: descDirection?'desc':'asc'});
      gridData.dataSource.sort(sort);
    }
  }
  filter(gridElement:any,filterField:string, filterValue:any,fieldType:string,operator?:EFilterOperators){


    // get the kendoGrid element.
    var gridData = gridElement.data("kendoTreeList");
    if(gridData)
    {

      if(fieldType == 'number')
      {
        filterValue =parseFloat(filterValue);
      }
      var currentFilters =[];
      //filter={"logic":"and","filters":[{"field":"numOfFiles","operator":"eq","value":"15"}
      currentFilters.push({
        field: filterField,
        operator: operator?operator.value.toString(): "contains",
        value: filterValue
      });

      gridData.dataSource.query({ filter: currentFilters });

    }
  }


  hidePageButtonsIfNeeded(gridElement:any) {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      var configPageSize =gridData.dataSource.pageSize();
      var actualPageSize =gridData.dataItems().length;
      if(  gridData.dataSource.page()==1)
      {
        gridData.pager.element.find('[title="Previous"]').css("visibility", "hidden");
      }
      else
      {
        gridData.pager.element.find('[title="Previous"]').css("visibility", "visible");
      }
      if(actualPageSize<configPageSize) {
        gridData.pager.element.find('[title="Next"]').css("visibility", "hidden");
      }
      else
      {
        gridData.pager.element.find('[title="Next"]').css("visibility", "visible");
      }
    }
  }

  saveAsExcel(gridElement:any,fileName:string) {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      gridData.options.excel.fileName=fileName;
      gridData.saveAsExcel();
    }
  }

  saveAsPdf(gridElement:any,fileName:string) {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      gridData.options.pdf.fileName=fileName;
      gridData.saveAsPDF();
    }
  }

  refreshTemplate(gridElement:any,template:any) {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      gridData.options.rowTemplate = template;
    }
  }
  loadData(gridElement:any,url,pageNumber:number,filter,isClientFilter,sortField,descDirection) {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData &&  gridData.dataSource.transport.options) {


      gridData.options.autoBind=true;
      gridData.dataSource.transport.options.read.url=url;
      this.setFilterKendoFormatToDataSource(gridData.dataSource,filter);

 //     gridData.dataSource.read();

      }

    }


  setFilterKendoFormatToDataSource(dataSource:any,filter:ui.IKendoFilter) {
    if(dataSource) {
      if(dataSource.transport.options&&dataSource.transport.options.read.url) {
        dataSource.filter(<any>filter);
      }
    }
  }
  filterKendoFormat(gridElement:any,filter:ui.IKendoFilter,isClientFilter?:boolean) {
    var gridData = gridElement.data("kendoTreeList");
    if(gridData) {
      //  var filterExp = { logic: "and", filters: [filter] };
      //  gridData.dataSource.filter(<any>filterExp);
      (<any>this).filterData=filter;
      if(isClientFilter) {
        gridData.dataSource.filter({});
        gridData.dataSource.filter(filter);
      }
      else {
        this.setFilterKendoFormatToDataSource(gridData.dataSource, filter);
        gridData.dataSource.read();
      }
    }


  }
  reloadData(gridElement:any, finishFunc?:any) {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      gridData.dataSource.read().then(function() {
        if (finishFunc) {
          finishFunc();
        }
      });
      // gridData.dataSource.page(1); //reset page numbers
    }
  }
  clear(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      gridData.dataSource.data([ ]);
    }
  }
  refreshDisplay(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      gridData.refresh();
    }
  }
  nextPage(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      var currentPage = gridData.dataSource.page();
      gridData.dataSource.page(currentPage+1);
    }
  }

  prevPage(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      var currentPage = gridData.dataSource.page();
      gridData.dataSource.page(currentPage-1);
    }
  }
  registerToFocusEvent(gridElement:any){
    var gridData = gridElement.data("kendoTreeView");
    if (gridData) {
      gridData.bind("navigate", function () {
        alert(22);
      });
    }
  }

  setPage(gridElement:any, pageNumber:number)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      gridData.dataSource.page(pageNumber);

    }
  }
  getTotalElementsCount(gridElement:any)
  {
    var gridData = gridElement.data("kendoTreeList");
    if (gridData) {
      return gridData.dataSource.total();

    }
    return null;
  }
  setOnlyOnePageStateClass = function(gridElement,number,configuration, entityDisplayType: EntityDisplayTypes)
  {

  }
}




class KendoTreeListOptions  {
  columns: kendo.ui.TreeListColumn[];
  dataSource:kendo.data.DataSourceOptions;
  dataSchema:kendo.data.DataSourceSchemaModelWithFieldsArray;
  treeDisplaySettings:kendo.ui.TreeListOptions;

  getOptions():kendo.ui.TreeListOptions {
    this.treeDisplaySettings.columns = this.columns;
    this.treeDisplaySettings.dataSource = this.dataSource;
    this.treeDisplaySettings.dataSource.schema= this.dataSchema;
    return this.treeDisplaySettings;

  }

}

///<reference path='..\all.d.ts'/>
'use strict';

interface ISaveFileBuilder {

  saveAsExcelOneSheet(fields:ui.IFieldDisplayPropertiesBase[] ,data,fileNameNoExstention:string,sheetTitle:string): void;
  saveAsTxt(data:string[],fileNameNoExstention:string): void;
  saveSeriesCategoriesChartAsExcel(fields:ui.IChartFieldDisplayProperties[],data,fileNameNoExstention:string,sheetTitle:string): void;
  saveAsExcelMultipleSheets(sheets:any[], fileNameNoExstention): void;
  saveSVGAsPdf(htmlElement,fileNameNoExtention:string): void;
  saveSVGAsImage(htmlElement,fileNameNoExtention:string): void;
  saveSVGAsBigImage(htmlElement,fileNameNoExtention:string): void;
  createSheet(fields, preparedRows,title):any;
  preparSheetRows(fields:ui.IFieldDisplayPropertiesBase[], data:any[]):any;

}


angular.module('ui.builder.saveAsFile', [
    ])
    .factory('saveFilesBuilder', function($filter,configuration) {
      var myFactory:ISaveFileBuilder = <ISaveFileBuilder>{};

      myFactory.saveSVGAsBigImage = function(sourceSVG,fileNameNoExtention)
      {
        var targetCanvas:any = document.createElement('canvas');
        document.body.appendChild(targetCanvas);
        targetCanvas=$(targetCanvas);
        var svgSize = sourceSVG[0].getBoundingClientRect();
        targetCanvas[0].width = svgSize.width;
        targetCanvas[0].height =svgSize.height;
        var svg_xml = (new XMLSerializer()).serializeToString(sourceSVG[0]);
        var ctx = targetCanvas[0].getContext('2d');
        var img = new Image();
        img.src = "data:image/svg+xml;base64," + btoa(svg_xml);
        img.onload = function() {
          ctx.drawImage(img, 0, 0);

          var newLink = document.createElement('a');
          newLink['download'] = fileNameNoExtention +".png";
          newLink.target = '_blank';
          targetCanvas[0].toBlob(function(blob) {
            newLink.href = URL.createObjectURL(blob);
            newLink.style.display = 'none';
            document.body.appendChild(newLink);
            newLink.click();
            targetCanvas.remove();
          },"image/png");
        }
      }


      myFactory.saveSVGAsImage = function(sourceSVG,fileNameNoExtention)
      {
        var targetCanvas:any = document.createElement('canvas');
        document.body.appendChild(targetCanvas);
        targetCanvas=$(targetCanvas);
        var svgSize = sourceSVG[0].getBoundingClientRect();
        targetCanvas[0].width = svgSize.width;
        targetCanvas[0].height =svgSize.height;
        var svg_xml = (new XMLSerializer()).serializeToString(sourceSVG[0]);
        var ctx = targetCanvas[0].getContext('2d');
        var img = new Image();
        img.src = "data:image/svg+xml;base64," + btoa(svg_xml);
        img.onload = function() {
          // after this, Canvas’ origin-clean is DIRTY

          ctx.drawImage(img, 0, 0);

          var canvasdata =  targetCanvas[0].toDataURL("image/png");
          var a = document.createElement('a');
          a.href = canvasdata;
          a.target = '_blank';
          (<any>a).download = fileNameNoExtention;
          a.style.display = 'none';
          document.body.appendChild(a);
          a.click();
          targetCanvas.remove();
        }
      }

      myFactory.saveSVGAsPdf = function(sourceSVG,fileNameNoExtention:string)
      {

          sourceSVG[0].style.background='#fff';
          var targetCanvas:any = document.createElement('canvas');
          document.body.appendChild(targetCanvas);
          targetCanvas=$(targetCanvas);
          var svgSize = sourceSVG[0].getBoundingClientRect();
          targetCanvas[0].width = svgSize.width;
          targetCanvas[0].height =svgSize.height;
          var svg_xml = (new XMLSerializer()).serializeToString(sourceSVG[0]);
          var ctx = targetCanvas[0].getContext('2d');

          var img = new Image();

          img.src = "data:image/svg+xml;base64," + btoa(svg_xml);

          img.onload = function() {
            // after this, Canvas’ origin-clean is DIRTY

            ctx.drawImage(img, 0, 0);
            kendo.drawing.drawDOM(targetCanvas)
                .then(function(group) {
                  // Render the result as a PDF file
                  return kendo.drawing.exportPDF(group, <any>{
                    paperSize: "auto",
                    margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
                  });
                })
                .done(function(data) {
                  // Save the PDF file
                  kendo.saveAs({
                    dataURI: data,
                    fileName: fileNameNoExtention+".pdf",
                  });

                });
            targetCanvas.remove();
          }

      }
      myFactory.createSheet=function(fields, rows,title)
      {
        var columns = [];
        fields.forEach(f=> {
          columns.push({autoWidth: true})
        });
        var sheet = {
          columns: [
            {autoWidth: true},
            {autoWidth: true},

          ],
          title: title.substr(0,30),
          rows: rows
        }
        return sheet;
      }

      myFactory.preparSheetRows = function(fields:ui.IFieldDisplayPropertiesBase[] ,data) {
        fields = fields.filter(f=>typeof (<ui.IFieldDisplayProperties>f).displayed == 'undefined' || (<ui.IFieldDisplayProperties>f).displayed);
        if (fields && fields.length > 0) {
          var rows = [], cells = [];
          fields.forEach(f=> {
            cells.push({value: f.title})
          });
          rows.push({ //columns header

            cells: cells
          });

          for (var i = 0; i < data.length; i++) {
            var cells = [];
            fields.forEach(f=> {
              var value;
              if(f.fieldName.split) {
                var props =f.fieldName.split('.');

                if (props.length == 1) {
                  value = data[i][f.fieldName];
                }
                else {
                  value = data[i]; //for cases like item.id
                  props.forEach(p=>value = value[p]);
                }
              }
              else {
                value = data[i][f.fieldName];
              }
              if (value && f.type == EFieldTypes.type_date) {
                var valueDate = $filter('asDate')(value);
                value = $filter('date')(valueDate, configuration.dateFormat);
              }
              else if (value && f.type == EFieldTypes.type_dateTime) {
                var valueDate = $filter('asDate')(value);
                value = $filter('date')(valueDate, configuration.dateTimeFormat);
              }

              cells.push({value: value})
            });
            rows.push({
              cells: cells
            })

          }

        }
        return rows;
      }
      var createAndSaveExcelWorkBook = function(sheets:any[], fileNameNoExstention)
      {
        var workbook = new kendo.ooxml.Workbook({
          sheets: sheets
        });
        kendo.saveAs({
          dataURI: workbook.toDataURL(),
          fileName: fileNameNoExstention + '.xlsx'
        });
      }

      myFactory.saveAsExcelOneSheet = function(fields:ui.IFieldDisplayPropertiesBase[] ,data,fileNameNoExstention:string,sheetTitle:string) {
        var rows = myFactory.preparSheetRows(fields, data);
        var sheet =  myFactory.createSheet(fields, rows, sheetTitle);
        createAndSaveExcelWorkBook([sheet], fileNameNoExstention);


      };
      myFactory.saveAsTxt = function(data:string[],fileNameNoExstention:string) {
        var blob = new Blob(data, {type: "text/plain;charset=utf-8"});
        kendo.saveAs({
          dataURI: blob,
          fileName: fileNameNoExstention+".txt"
        });


      };

      myFactory.saveAsExcelMultipleSheets = function(sheets:any[], fileNameNoExstention)
      {
        createAndSaveExcelWorkBook(sheets, fileNameNoExstention);

      };

      myFactory.saveSeriesCategoriesChartAsExcel = function(fields:ui.IChartFieldDisplayProperties[],data:ui.IStackedChartData,fileNameNoExstention:string,sheetTitle:string)
      {
        var rows=[];
       var categoryField =  fields.filter(f=>f.isCategoryField)[0];
       var categoryDataField =  fields.filter(f=>f.isCategoryDataField)[0];
        var titleRow =  [{value: categoryField?categoryField.title:'Name'}];
        categoryDataField? titleRow.push({value: categoryDataField.title}):null;
        for (var i = 0; i < data.series.length; i++) {

          titleRow.push({value: data.series[i].name});

        }
        rows.push({cells:titleRow}); //columns header

        for (var cat = 0; cat < data.categories.length; cat++) {
          var newRow=[ {value: data.categories[cat].name}];
          categoryDataField? newRow.push({value:data.categories[cat].data}):null;
          for (var i = 0; i <data.series.length; i++) {
            var counter = data.series[i].data[cat];
            newRow.push({value:counter});
          }
          rows.push({cells:newRow});

        }
        var sheet =  myFactory.createSheet(fields, rows,sheetTitle);
        createAndSaveExcelWorkBook([sheet],fileNameNoExstention);
      };

      return  myFactory
    });


///<reference path='../all.d.ts'/>

'use strict';
angular.module('ui.builder.charts.multi.highchart', [
    ])
    .factory('multiHighChartsBuilder', function(propertiesUtils) {
      var myFactory:ui.IChartOptionsBuilder = <ui.IChartOptionsBuilder>{};

      myFactory.createPie = function(histogramData,chartTitle:string,
                                     fields:ui.IChartFieldDisplayProperties[],
                                     seriesColors:string[]) :ui.IChartHelper{
        return HighChartOptionsBuilder2.createPie(histogramData,chartTitle,fields,seriesColors);
      };

      myFactory.createBarChart = function(histogramData,chartTitle:string,
                                          fields:ui.IChartFieldDisplayProperties[],
                                          gap) :ui.IChartHelper{
        return HighChartOptionsBuilder2.createBarChart(histogramData,chartTitle,fields,gap);
      };


      myFactory.createLineChart = function(stackedHistogramData,chartTitle:string,
                                             fields:ui.IChartFieldDisplayProperties[],
                                             seriesColors:string[]) :ui.IChartHelper{
        return HighChartOptionsBuilder2.createLineChart(stackedHistogramData,chartTitle,fields,seriesColors);
      };

      myFactory.createStackedBarChart = function(stackedHistogramData:ui.IStackedChartData,chartTitle:string,
                                                 fields:ui.IChartFieldDisplayProperties[],
                                                 seriesColors:string[]):ui.IChartHelper{
        return HighChartOptionsBuilder2.createStackedBarChart(stackedHistogramData,chartTitle,fields,seriesColors);
      };


      return  myFactory
    });

class HighChartOptionsBuilder2
//implements  ui.IChartOptionsBuilder
{

  static defaultChartColors  =  ["rgb(255, 141, 0)",   "#EB742D", "#954111", "#5A5A5A", "#243D6F", "#73A7D9"]
  static createPie(histogramData, chartTitle, fields, seriesColors): any {

    let valuesList = [];
    for (let count = 0; count < histogramData.length; count++) {
      valuesList.push({
        name: histogramData[count].name,
        y: histogramData[count].counter,
        color: seriesColors[count]
      });
    }
    return {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      tooltip: {
        formatter: function () {
          return '<b>' + this.key + '</b>: <b>' + this.y + ' (' + Number(this.point.percentage).toFixed(1) + '%)</b>';
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true
          }
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: '',
        data: valuesList
      }]
    };
  };

  static createBarChart(histogramData, chartTitle: string,
                        fields: ui.IChartFieldDisplayProperties[],
                        gap): any {
    let categoryList = [];
    let valuesList = [];
    if (histogramData.categories) {
      for (let count = 0; count < histogramData.categories.length; count++) {
        categoryList.push(histogramData.categories[count].name);
      }
    }

    if (histogramData.series) {
      for (let count = 0; count < histogramData.series.length; count++) {
        valuesList.push({
          name: histogramData.series[count].name,
          data: histogramData.series[count].data,
          type: histogramData.series[count].name == 'total' ? 'spline' : 'column',
          color: histogramData.series[count].name == 'total' ? "#26bbdd" : (this.defaultChartColors && this.defaultChartColors.length > count ? this.defaultChartColors[count] : null)
        })
      }
    }

    return {

      chart: {
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      title: {
        text: ''
      },

      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories: categoryList,
        gridLineWidth: 1
      },

      yAxis: {
        title: {
          text: 'Files'
        }
      },
      tooltip: {
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        column: {
          dataLabels : {
            inside : true,
            rotation: -90,
            align: 'right'
          }
        },
        series:{
          dataLabels: {
            style : {
              fontSize: '10px'
            }
          },
          point:{
            events:{
              click: function(){
                var e = !this.dataLabels || !this.dataLabels.enabled ? true: false;
                this.update({
                  dataLabels:{
                    enabled: e
                  }
                });
              }
            }
          }
        }
      },
      series: valuesList,
    };

  };

  static createLineChart(histogramData, chartTitle: string,
                           fields: ui.IChartFieldDisplayProperties[],
                           seriesColors: string[]): any {
    let categoryList = [];
    if (histogramData.categories) {
      for (let count = 0; count < histogramData.categories.length; count++) {
        categoryList.push(histogramData.categories[count].name);
      }
    }

    let valuesList = [];
    if (histogramData.series) {
      for (let count = 0; count < histogramData.series.length; count++) {
        valuesList.push({
          name: histogramData.series[count].name,
          data: histogramData.series[count].data,
          color: histogramData.series[count].name == 'total' ? "#26bbdd" : (this.defaultChartColors && this.defaultChartColors.length > count ? this.defaultChartColors[count] : null)
        })
      }
    }
    return {

      chart : {
        type: 'line',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          }
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },

      xAxis: {
        categories: categoryList,
        gridLineWidth: 1
      },
      tooltip: {
      },
      yAxis: {
        title: {
          text: 'Files'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      plotOptions: {
        series:{
          dataLabels: {
            style : {
              fontSize: '10px'
            }
          },
          point:{
            events:{
              click: function(){
                var e = !this.dataLabels || !this.dataLabels.enabled ? true: false;
                this.update({
                  dataLabels:{
                    enabled: e
                  }
                });
              }
            }
          }
        }
      },
      credits: {
        enabled: false
      },
      series: valuesList,
    };
  };

  static createStackedBarChart(histogramData: ui.IStackedChartData, chartTitle: string,
                               fields: ui.IChartFieldDisplayProperties[],
                               seriesColors: string[]): any {
    let categoryList = [];
    let valuesList = [];
    if (histogramData.categories) {
      for (let count = 0; count < histogramData.categories.length; count++) {
        categoryList.push(histogramData.categories[count].name);
      }
    }

    if (histogramData.series) {
      for (let count = 0; count < histogramData.series.length; count++) {
        valuesList.push({
          name: histogramData.series[count].name,
          data: histogramData.series[count].data,
          type: histogramData.series[count].name == 'total' ? 'spline' : 'column',
          color: histogramData.series[count].name == 'total' ? "#26bbdd" : (this.defaultChartColors && this.defaultChartColors.length > count ? this.defaultChartColors[count] : null)
        })
      }
    }

    return {

      chart: {
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      title: {
        text: ''
      },

      exporting: {
        chartOptions: {
          title: {
            text: chartTitle
          },
          plotOptions: {
            series: {
              dataLabels: {
                style : {
                  fontSize: '10px'
                }
              }
            }
          }
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories: categoryList,
        gridLineWidth: 1
      },

      yAxis: {
        title: {
          text: 'Files'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },

      plotOptions: {
        column: {
          stacking: 'normal'
        },
        series:{
          dataLabels: {
            enabled: true,
            style : {
              fontSize: '10px'
            }
          },
          point:{
            events:{
              click: function(){
                var e = !this.dataLabels || !this.dataLabels.enabled ? true: false;
                this.update({
                  dataLabels:{
                    enabled: e
                  }
                });
              }
            }
          }
        }
      },
      tooltip: {
      },
      credits: {
        enabled: false
      },
      series: valuesList,
    };
  };
};

///<reference path='../../common/all.d.ts'/>
'use strict';


angular.module('directives.pageable.items.menu', [

    ])
    .controller('pageableCheckableItemsManageMenuCtrl', function ($scope,$element,Logger:ILogger,fileTagsResource:IFileTagsResource,fileTagResource,propertiesUtils,
                                                                  eventCommunicator:IEventCommunicator,dialogs, $q,localStorage:IUserStorageService) {
      var log:ILog = Logger.getInstance('pageableCheckableItemsManageMenuCtrl');


      $scope.init = function () {

      };


      $scope.openNewItemDialog = function()
      {
        var dlg = dialogs.create(
            'common/directives/dialogs/genericDialogs.tpl.html',
            'openSingleInputDialogCtrl',
            {bodyText:'Add new item',title:'Add new item',placeholderText:'...'},
            null);

        dlg.result.then(function(newItem){
          $scope.addItem($scope.objectId,{active:true,item:newItem});

          $scope.refreshData();
        },function(){

        });

      }



      $scope.openEditItemDialog = function() {
        if($scope.selectedItemsToExecuteOn&&$scope.selectedItemsToExecuteOn.length==1) {
          var itemToEdit = (<any>$scope.selectedItemsToExecuteOn[0]);
          var dlg = dialogs.create(
              'common/directives/dialogs/genericDialogs.tpl.html',
              'openSingleInputDialogCtrl',
              {bodyText: 'Add new item', title: 'Add new item', placeholderText: '...', initialValue: itemToEdit.item},
              null);

          dlg.result.then(function (newItem) {
            $scope.editItem($scope.objectId,{active:true,item:newItem});
            $scope.refreshData();
          }, function () {

          });
        }
      }




      $scope.openDeleteItemDialog = function () {
        if($scope.selectedItemsToExecuteOn && $scope.selectedItemsToExecuteOn.length>0) {
          var dlg = dialogs.confirm('Confirmation', 'Are you sure you want do delete '+$scope.selectedItemsToExecuteOn.length+' items?', null);
          dlg.result.then(function (btn) { //user confirmed deletion
            $scope.selectedItemsToExecuteOn.forEach( s=> {
              $scope.deleteItem($scope.objectId, s);
            });

          }, function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
        }

      }





    })
    .directive('pageableCheckableItemsManageMenu',
        function () {
          return {
            template:
            '<div> <button class="btn btn-flat btn-xs" ng-click="openNewItemDialog()"><i class="fa fa-plus"></i>&nbsp;Add new</button>' +
            '<button class="btn btn-link btn-sm ng-hide "  ng-class="{disabled:((!selectedItemsToExecuteOn)||selectedItemsToExecuteOn.length==0||selectedItemsToExecuteOn.length>1)}" ng-click="openEditItemDialog()"><i class="fa fa-pencil"></i>&nbsp;Edit</button>' +
            '<button class="btn btn-link btn-sm" ng-class="{disabled:((!selectedItemsToExecuteOn)||selectedItemsToExecuteOn.length==0)}" ng-click="openDeleteItemDialog()"><i class="fa fa-trash-o"></i>&nbsp;Delete</button>' +
            '</div>',
            controller: 'pageableCheckableItemsManageMenuCtrl',

            replace:true,
            scope:{
              'refreshData':'=',
              'selectedItemsToExecuteOn':'=',
              'addItem':'=',
              'editItem':'=',
              'deleteItem':'=',
              'objectId':'=',

            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }
    )
;

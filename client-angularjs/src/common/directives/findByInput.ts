///<reference path='../../common/all.d.ts'/>
'use strict';
angular.module('directives.findBy', [])
    .controller('findByInputCtrl', function ($scope, $element, Logger:ILogger) {
      var log:ILog = Logger.getInstance('findByInputCtrl');



      $scope.toggled=function(open)
      {
        $scope.focusFindBy=open;
      }

      $scope.clear = function()
      {
        $scope.findByText='';
        $scope.focusFindBy=true;
      }
    })
    .directive('findByInput',
        function () {
          return {
            // restrict: 'E',
            template: ' <span class="dropup" dropdown on-toggle="toggled(open)"  >' +
                '<a class="btn btn-link btn-xs dropdown-toggle " title="{{tooltipText}}" dropdown-toggle  ><i class="fa fa-map-marker"></i>&nbsp;{{text}}</a>' +
                '<ul class="dropdown-menu" role="menu" aria-labelledby="split-button"  style="max-width: 300px;"><li style="padding: 1px;">' +
                '<div  style="position: relative;"><input  type="text" class="form-control has-right-icon" focus-if="focusFindBy" ng-click="$event.stopPropagation();" style="width: 300px" ng-model="findByText" ' +
                             ' ng-enter="findBy(findByText)" ng-keyup="findBy(findByText)" placeholder="{{placeholder}}" spellcheck="false" />' +
                '<span class="btn btn-link top-right-position" title="Clear text" ng-click="clear();$event.stopPropagation();"><i class="fa fa-times"></i></span></div>' +
            '</li></ul></span>',
            scope:{
              tooltipText: '@',
              text: '@',
              placeholder:'@',
              findBy:'='
            },
            controller: 'findByInputCtrl',


          };
        }
    );

///<reference path='../../all.d.ts'/>
'use strict';

interface IChooseCustomerDataCenterScope extends ng.IScope {
  selectedDataCenter: Operations.DtoCustomerDataCenter;
  selectedDataCenterSubdisplay: string;
  saveLastSelected() ;

}
interface IChooseCustomerDataCenterScopeInput  {

  getLastSelectedFn(successFunction: (id:number) => void ):void;
  setLastSelectedFn(id:number):void;
}
interface IChooseCustomerDataCenterController extends IChooseInputParamController<Operations.DtoCustomerDataCenter>  {

}

interface IChooseInputParamController<T>  {
  createDataCenterParamOption(dataCenter:T);
  openNewDataCenterDialog(successFunction,cancelFunction)
  loadDataCenterOptionItems(itemsOptions:ParamOption<T>[]);
  onDataCenterItemSelected(activeItem:ParamActiveItem<T>);
}

class ChooseCustomerDataCenterController  implements IChooseCustomerDataCenterController, IChooseCustomerDataCenterScopeInput{

  selectedDataCenter: Operations.DtoCustomerDataCenter;
  selectedDataCenterSubdisplay: string;
  getLastSelectedFn: (successFunction: (id:number) => void )=>void;
  setLastSelectedFn:(id:number)=>void;


  private dataCenterSavedSelectedItems:ParamActiveItem<Operations.DtoCustomerDataCenter>[];

  static $inject = ['$scope', 'customerDataCenterResource', 'configuration','dialogs','eventCommunicator','i18nNotifications','userSettings'];
  constructor(isolateScope:IChooseCustomerDataCenterScope,protected customerDataCenterResource,protected configuration:IConfig,protected dialogs,protected eventCommunicator:IEventCommunicator,protected i18nNotifications:II18nNotifications,protected userSettings:IUserSettingsService) {

    this.setSavedSelectedDataCenter(this.selectedDataCenter);

  }

  private setSavedSelectedDataCenter=(dataCenter:Operations.DtoCustomerDataCenter)=> {

    if(dataCenter && dataCenter.id) {

      this.dataCenterSavedSelectedItems = [this.createDataCenterNewSelectedItem(dataCenter)];

    }
    else if( this.getLastSelectedFn) {
       this.do.apply(this,null);

    }
    else{
      this.dataCenterSavedSelectedItems=[]; //inorder to display the input box
    }

  }

  private do(){
    this.getLastSelectedFn(
      function (centerId) {
        this.initSelectedDataCenterById(centerId);
      }.bind(this)
    );
  }
  private createDataCenterNewSelectedItem(dataCenter:Operations.DtoCustomerDataCenter):ParamActiveItem<Operations.DtoCustomerDataCenter> {
    var newItem = this.createDataCenterParamOption(dataCenter);
    var paramActiveItem = new ParamActiveItem<Operations.DtoCustomerDataCenter>();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  }

  public createDataCenterParamOption(dataCenter:Operations.DtoCustomerDataCenter):ParamOption<Operations.DtoCustomerDataCenter> {
    var newItem = new ParamOption<Operations.DtoCustomerDataCenter>();
    newItem.id = dataCenter.id;
    newItem.display = dataCenter.name;
    var descriptionPart = dataCenter.description ? dataCenter.description : ' ';
    var locationPart = dataCenter.location ? dataCenter.location : ' ';
    newItem.subdisplay = locationPart + ' ' + descriptionPart;
    newItem.value = dataCenter;
    return newItem;
  }

  private initSelectedDataCenterById (centerId) {
    if (centerId) {
      this.customerDataCenterResource.getDataCenter(centerId, function (dataCenter:Operations.DtoCustomerDataCenter) {
        if (dataCenter) {

         this.setSavedSelectedDataCenter(dataCenter);
        }
      }.bind(this), function () {
      })
    }
    else {
      this.customerDataCenterResource.getDataCenters(function (dataCenters:Operations.DtoCustomerDataCenter[]) {
          if (dataCenters && dataCenters.length > 0) {
            var defaultDataCenter = dataCenters.filter(d=>d.defaultDataCenter);
            this.setSavedSelectedDataCenter(defaultDataCenter[0]);
          }
        }.bind(this) //keep the context after async call
        , function () {
        })
    }
  }

  public loadDataCenterOptionItems=(itemsOptions:ParamOption<Operations.DtoCustomerDataCenter>[])=> {  //'this' here would be the optionsParam directive scope without the =>.
    let _local = this;
    _local.customerDataCenterResource.getDataCenters(function (dataCenters:Operations.DtoCustomerDataCenter[]) {
        if (dataCenters) {
          dataCenters.forEach(dataCenters=> {
            var newItem = _local.createDataCenterParamOption(dataCenters);
            itemsOptions.push(newItem);
          })
        }
      }
      , _local.onError)
  }

  public onDataCenterItemSelected=(dataCenter:ParamActiveItem<Operations.DtoCustomerDataCenter>)=>
  {
    this.selectedDataCenter = dataCenter.item.value;
  //  (<Operations.RootFolderDto>$scope.rootFolderItem).customerDataCenterDto  =(<Operations.DtoCustomerDataCenter>dataCenter.item.value);
    this.selectedDataCenterSubdisplay  =dataCenter.item.subdisplay;

  }

  public openNewDataCenterDialog=(successFunction,cancelFunction)=> {
    var dlg = this.dialogs.create(
      'common/directives/operations/dialogs/newDataCenterDialog.tpl.html',
      'newDataCenterItemDialogCtrl',
      {createNew: false},
      {size: 'md', windowClass: 'offsetDialog'});

    dlg.result.then(function (dataCenter:Operations.DtoCustomerDataCenter) {
      successFunction(dataCenter);
    }, function () {
      cancelFunction ? cancelFunction() : null;
    });
  }

  public saveLastSelected = () => //otherwise this would be the rootfolder dialog. Function is not declared on prototype this way
  {
    if(this.selectedDataCenter) {
      this.setLastSelectedFn(this.selectedDataCenter.id);
    }
  }


  private onError() {
    this.i18nNotifications.onErrorInformUser();
  }

}
var chooseDataCenter = function() {
  return {
    bindToController: {  //<IChooseCustomerDataCenterScope>  <IChooseCustomerDataCenterScopeInput>
      selectedDataCenter:'=',
      selectedDataCenterSubdisplay:'=',
      saveLastSelected:'=',
      getLastSelectedFn:'=',
      setLastSelectedFn:'=',
    },
    scope:{},
    templateUrl:'/common/directives/operations/customerDataCenterSelector.tpl.html',
    replace:true,
    controllerAs:'choose_dc',
    controller:ChooseCustomerDataCenterController
  }
}

angular.module('directives.operations.dataCenter', [
  ])
  .directive('chooseDataCenter', chooseDataCenter)


///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.microsoft.new', [

    ])
    .controller('newMicrosoftConnectionItemDialogCtrl',
        function ($scope, $uibModalInstance, data, dialogs, sharePointConnectionResource:IMediaTypeConnectionResource<Operations.DtoSharePointConnectionDetails>,
                  oneDriveConnectionResource:IMediaTypeConnectionResource<Operations.DtoOneDriveConnectionDetails>,$filter,
                  eventCommunicator:IEventCommunicator) {

          const psaudoPassword = '11111111';
          var init=function(mediaType:Operations.EMediaType) {

            $scope.connection = <Operations.DtoSharePointConnectionDetails>(data.connection);
            if ($scope.connection) {
                $scope.editMode = true;
                if ((<Operations.DtoSharePointConnectionDetails>$scope.connection).sharePointConnectionParametersDto) {
                  (<Operations.DtoSharePointConnectionDetails>$scope.connection).sharePointConnectionParametersDto.password = psaudoPassword;
                }
            }
            else {
              var connection:Operations.MicrosoftConnectionDetailsDtoBase;
              if(mediaType == Operations.EMediaType.SHARE_POINT) {
                connection = new Operations.DtoSharePointConnectionDetails();
                connection.sharePointConnectionParametersDto = new Operations.DtoSharePointConnectionParameters();
              }
              else if(mediaType == Operations.EMediaType.ONE_DRIVE) {
                connection = new Operations.DtoOneDriveConnectionDetails();
                connection.sharePointConnectionParametersDto = new Operations.DtoSharePointConnectionParameters();
              }
              $scope.connection =connection;

            }
            $scope.setActiveTab( 'general');
          }

          $scope.$watch('selectedDataCenter', function(value) {
            $scope.testConnectionInProgress = false;
          });

          $scope.setActiveTab=function(tabName:string)
          {
            $scope.activeTab=tabName;
          }
          var getResource = function(){
            return  $scope.connection.mediaType == Operations.EMediaType.ONE_DRIVE? oneDriveConnectionResource:sharePointConnectionResource;
          }
          init(data.mediaType);

          var lastRequestTime = null;

          $scope.testConnection = function(){
            $scope.connectivityTest = null;
            $scope.failureMessage = null;
            if ($scope.myForm.$valid) {
              let currReqTime = (new Date()).getTime();
              lastRequestTime = currReqTime;
              $scope.testConnectionInProgress = true;
              if( $scope.editMode)
              {
                const copyOfCon =  _.cloneDeep($scope.connection);
                (<Operations.DtoSharePointConnectionDetails>copyOfCon).sharePointConnectionParametersDto.password =
                  (<Operations.DtoSharePointConnectionDetails>copyOfCon).sharePointConnectionParametersDto.password == psaudoPassword
                  ? null:(<Operations.DtoSharePointConnectionDetails>copyOfCon).sharePointConnectionParametersDto.password ;
                getResource().testConnectionWithDatacenterWithSavedConnection($scope.connection.id,copyOfCon.sharePointConnectionParametersDto, $scope.selectedDataCenter.id,
                  function (success: boolean, failureMessage) {
                    if (currReqTime == lastRequestTime) {
                      $scope.testConnectionInProgress = false;
                      $scope.connectivityTest = success;
                      $scope.failureMessage = failureMessage;
                    }
                  }
                  , function(){
                    if (currReqTime == lastRequestTime) {
                      onTestConnectionError();
                    }
                  })
              }
              else {
                getResource().testConnectionWithDatacenter($scope.connection.sharePointConnectionParametersDto, $scope.selectedDataCenter.id,
                  function (success: boolean, failureMessage) {
                    if (currReqTime == lastRequestTime) {
                      $scope.testConnectionInProgress = false;
                      $scope.connectivityTest = success;
                      $scope.failureMessage = failureMessage;
                    }
                  }
                  ,function(){
                  if (currReqTime == lastRequestTime) {
                    onTestConnectionError();
                  }
                })
              }
            }
          };
          var onTestConnectionError  = () =>
          {
            $scope.testConnectionInProgress = false;
            dialogs.error('Error','Sorry, failed to test connection via datacenter '+(<Operations.DtoCustomerDataCenter>$scope.selectedDataCenter).name);
          }
          $scope.cancel = function(){
            $uibModalInstance.dismiss('Canceled');
          }; // end cancel

          $scope.save = function() {
            $scope.submitted = true;
            if ($scope.myForm.$valid) {
              if ($scope.editMode) {
               if((<Operations.DtoSharePointConnectionDetails>$scope.connection).sharePointConnectionParametersDto.password == psaudoPassword)
               {
                 (<Operations.DtoSharePointConnectionDetails>$scope.connection).sharePointConnectionParametersDto.password =null;
               }
                $uibModalInstance.close($scope.connection);
              }
              else {
                (<Operations.MicrosoftConnectionDetailsDtoBase>$scope.connection).name = !(<Operations.MicrosoftConnectionDetailsDtoBase>$scope.connection).name ||
                (<Operations.MicrosoftConnectionDetailsDtoBase>$scope.connection).name.trim()==''?null: (<Operations.MicrosoftConnectionDetailsDtoBase>$scope.connection).name;

                getResource().addNewConnection($scope.connection, function (newConnection:Operations.MicrosoftConnectionDetailsDtoBase) {
                      $uibModalInstance.close(newConnection);
                    }
                    , function (error) {
                      if (error.status == 409) {
                        dialogs.error('Error', 'Same connection configuration already exists.');
                      }
                      else {
                        onError();
                      }

                      //  $scope.cancel();

                    })
              }

            }
          };

          $scope.hitEnter = function(evt){
            if(angular.equals(evt.keyCode,13))
              $scope.save();
          };

          $scope.$on("$destroy", function() {
            eventCommunicator.unregisterAllHandlers($scope);
          });
          var onError = function()
          {
            dialogs.error('Error','Sorry, an error occurred.');
          }

        })



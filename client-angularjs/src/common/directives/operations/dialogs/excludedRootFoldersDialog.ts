///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.rootFolders.excludedFolders', [
      'folders.server',
      'operations.rootFolders.excludedFolders'
    ])
    .controller('excludedFoldersDialogCtrl', function ($scope, $uibModalInstance, data, dialogs, rootFoldersResource:IRootFoldersResource) {


      $scope.rootFolder = <Operations.DtoRootFolder>data.rootFolder;
      $scope.rootFolderId = data.rootFolder? data.rootFolder.id:null;
      $scope.selectedFolders = data.unSavedExcludedFolders ? data.unSavedExcludedFolders:[];
      $scope.lazyFirstLoad=false;
      $scope.readOnlyMode=data.readOnlyMode;
      $scope.title =  $scope.readOnlyMode?'View excluded folder rules of '+$scope.rootFolder.path:' Manage excluded folder rules of '+$scope.rootFolder.path;

      $scope.cancel = function(){
        $uibModalInstance.dismiss();
      };

      $scope.onSelectItem=function(id,path){
        $scope.selectedExcludedFolderId = id;
        $scope.selectedExcludedFolderPath = path;
      }
      $scope.onSelectItems = function (data) {
        $scope.excludedFoldersSelectedItems = data;
      };
      $scope.reloadGridData = function()
      {
        $scope.reloadDataListItems();
      }
      $scope.removeSelectedItem = function(selectedItemToRemove)
      {
        var index =$scope.selectedFolders.indexOf(selectedItemToRemove);
        $scope.selectedFolders.splice(index,1);
      }

      $scope.openBrowseServerFoldersDialog = function()
      {
        var dlg = dialogs.create(
            'common/directives/dialogs/folderBrowserDialog.tpl.html',
            'folderBrowserDialogCtrl',
            {rootFolder:$scope.rootFolder.path,editRootFolderMode:false,title:"Select excluded folder",mediaType:(<Operations.DtoRootFolder>$scope.rootFolder).mediaType},
            {size: 'lg',windowClass: 'offsetDialog1'});

        dlg.result.then(function (newRootFolderPath:string) {
          if($scope.rootFolderId) {
            var fObj = new Operations.DtoFolderExcludeRule();
            fObj.operator = EFilterOperators.equals.toString();
            fObj.matchString = newRootFolderPath;
            addExcludedFolder(fObj);
          }
          else {
            var found = $scope.selectedFolders.filter(f=>f==newRootFolderPath)[0];
            if(!found) {
              $scope.selectedFolders.push(newRootFolderPath);
            }
          }
        }, function () {

        });

      }

      var addExcludedFolder=function(excludeRule:Operations.DtoFolderExcludeRule)
      {
        if($scope.rootFolderId)
        {
          rootFoldersResource.addNewExcludedFolderRule($scope.rootFolderId,excludeRule,function(excludedFolder)
          {
            $scope.reloadDataListItems(excludedFolder);
          },onError);
        }
      }
      $scope.onExportToExcel=function(){
        $scope.exportToInProgress = true;
        $scope.exportToExcel = {fileName:'excludedRootFolders.xlsx'};
      };

      $scope.add = function(){
        if(!$scope.rootFolderId )
        {
          if($scope.selectedFolders&&$scope.selectedFolders.length>0)
          {
            var exFoldersResult:Operations.DtoFolderExcludeRule[] = $scope.selectedFolders.map(f=>
            {
              var fObj = new Operations.DtoFolderExcludeRule();
              fObj.operator = EFilterOperators.equals.toString();
              fObj.matchString = f;
              return fObj;
            });
            $uibModalInstance.close(exFoldersResult);
          }
        }
        else { //incase of new folder the create root folder dialog will save all data at once
          $uibModalInstance.close();
        }
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
    })


///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.customer.dataCenter.new', [

  ])
  .controller('newDataCenterItemDialogCtrl',
    function ($scope, $uibModalInstance, data, dialogs, customerDataCenterResource:ICustomerDataCenterResource, localizedMessages) {


      var init=function() {
        $scope.dataCenter = <Operations.DtoCustomerDataCenter>(data.dataCenter);
        if( $scope.dataCenter)
        {
          $scope.editMode = true;
          $scope.title='Edit data center';
        }
        else
        {
          $scope.title='Add data center';
        }
       }

      init();


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.myForm.$valid) {

          if ($scope.editMode) {
            customerDataCenterResource.updateDataCenter($scope.dataCenter, function (updatedDataCenter:Operations.DtoCustomerDataCenter) {
                $uibModalInstance.close(updatedDataCenter);
              }
              , function (e) {
                if(e.status==409)
                {
                  dialogs.error('Error',localizedMessages.get('crud.update.entity.error.alreadyExists', {
                    fieldValue: $scope.dataCenter.name,
                    fieldName: 'name',
                    entityName: 'data center'
                  }));
                }
                else  if (e.data.type === 'OTHER'){
                  dialogs.error('Error', e.data.message);

                }
                else {
                  dialogs.error('Error',localizedMessages.get('crud.update.entity.error.generic', {
                    fieldValue: $scope.dataCenter.name,
                    fieldName: 'name',
                    entityName: 'data center'
                  }));
                }

                //  $scope.cancel();

              })
          }
          else {
            customerDataCenterResource.addDataCenter($scope.dataCenter, function (newDataCenter:Operations.DtoCustomerDataCenter) {
                  $uibModalInstance.close(newDataCenter);
              }
              , function (e) {
                if(e.status==409)
                {
                  dialogs.error('Error',localizedMessages.get('crud.add.entity.error.alreadyExists', {
                    fieldValue: $scope.dataCenter.name,
                    fieldName: 'name',
                    entityName: 'data center'
                  }));
                }
                else {
                  dialogs.error('Error',localizedMessages.get('crud.add.entity.error.generic', {
                    fieldValue: $scope.dataCenter.name,
                    fieldName: 'name',
                    entityName: 'data center'
                  }));
                }

                //  $scope.cancel();

              })
          }
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };


    })



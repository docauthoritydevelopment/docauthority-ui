///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.box.new', [

])
  .controller('newBoxConnectionItemDialogCtrl',
    function ($scope, $uibModalInstance, data, dialogs,
              boxConnectionResource:IMediaTypeConnectionResource<Operations.DtoBoxConnectionDetails>,
              $filter,
              eventCommunicator:IEventCommunicator) {
      const psaudoPassword = '**********************';

      var init=function() {

        $scope.connection = <Operations.DtoBoxConnectionDetails>(data.connection);
        if ($scope.connection) {
          $scope.editMode = true;
          (<Operations.DtoBoxConnectionDetails>$scope.connection).jwt =psaudoPassword;
        }
        else {
          var connection= new Operations.DtoBoxConnectionDetails();
          $scope.connection =connection;

        }
        $scope.setActiveTab( 'general');
      }
      $scope.setActiveTab=function(tabName:string)
      {
        $scope.activeTab=tabName;
      }
      init();
      $scope.jwtChanged = function(){
        $scope.connectivityTest=null;
        if($scope.editMode && (<Operations.DtoBoxConnectionDetails>$scope.connection).jwt != psaudoPassword)
        {
          (<Operations.DtoBoxConnectionDetails>$scope.connection).username = null;
        }
      }
      $scope.testConnection = function(){
        $scope.connectivityTest = null;
        $scope.failureMessage = null;

        if ($scope.myForm.$valid) {
          $scope.testConnectionInProgress = true;
          if($scope.editMode)
          {
            const copyOfCon =  _.cloneDeep($scope.connection);
            if( (<Operations.DtoBoxConnectionDetails>copyOfCon).jwt == psaudoPassword)
            {
              (<Operations.DtoBoxConnectionDetails>copyOfCon).jwt =null;
            }
            else{
              (<Operations.DtoBoxConnectionDetails>copyOfCon).id =null;
            }
            boxConnectionResource.testConnectionWithDatacenterWithSavedConnection(copyOfCon.id,copyOfCon, $scope.selectedDataCenter.id, function (success: boolean, msg) {
              $scope.testConnectionInProgress = false;
              $scope.connectivityTest = success;
                if (success) {
                  $scope.connection.username = msg;
                  $scope.failureMessage = '';
                }
                else {
                  $scope.failureMessage = msg;
                }
              }
              , onTestConnectionError)
          }
          else {
            boxConnectionResource.testConnectionWithDatacenter((<Operations.DtoBoxConnectionDetails>$scope.connection), $scope.selectedDataCenter.id, function (success: boolean, msg) {
                $scope.testConnectionInProgress = false;
                $scope.connectivityTest = success;
                if (success) {
                  $scope.connection.username = msg;
                  $scope.failureMessage = '';
                }
                else {
                  $scope.failureMessage = msg;
                }
              }
              , onTestConnectionError)
          }
        }
      };
      var onTestConnectionError  = () =>
      {
        $scope.testConnectionInProgress = false;
        dialogs.error('Error','Sorry, failed to test connection via datacenter '+(<Operations.DtoCustomerDataCenter>$scope.selectedDataCenter).name);
      }

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.myForm.$valid) {
          if ($scope.editMode) {
            if((<Operations.DtoBoxConnectionDetails>$scope.connection).jwt == psaudoPassword) {
              (<Operations.DtoBoxConnectionDetails>$scope.connection).jwt = null;
            }
            $uibModalInstance.close($scope.connection);
          }
          else {
            (<Operations.DtoBoxConnectionDetails>$scope.connection).name = !(<Operations.DtoBoxConnectionDetails>$scope.connection).name ||
            (<Operations.DtoBoxConnectionDetails>$scope.connection).name.trim()==''?null: (<Operations.DtoBoxConnectionDetails>$scope.connection).name;

            boxConnectionResource.addNewConnection($scope.connection, function (newConnection:Operations.DtoBoxConnectionDetails) {
                $uibModalInstance.close(newConnection);
              }
              , function (error) {
                if (error.status == 409) {
                  dialogs.error('Error', 'Same connection configuration already exists.');
                }
                else {
                  onError();
                }

                //  $scope.cancel();

              })
          }

        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    })



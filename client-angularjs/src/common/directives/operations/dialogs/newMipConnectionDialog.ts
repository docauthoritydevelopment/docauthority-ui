///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.mip.new', []).controller('newMipConnectionItemDialogCtrl',
  function ($scope,$uibModalInstance,data,dialogs,mipConnectionResource:IMediaTypeConnectionResource<Operations.DtoMipConnectionDetails>,$filter,eventCommunicator:IEventCommunicator) {
  const psaudoPassword = '11111111';
  var init=function(mediaType:Operations.EMediaType) {

    $scope.connection = <Operations.DtoMediaTypeConnectionDetails>(data.connection);

    if ($scope.connection) {
      $scope.editMode = true;
      if(mediaType == Operations.EMediaType.MIP) {
        if ((<Operations.DtoMipConnectionDetails>$scope.connection).mipconnectionParametersDto) {
          (<Operations.DtoMipConnectionDetails>$scope.connection).mipconnectionParametersDto.password = psaudoPassword;
        }
      }
    }
    else {
      var connection:Operations.DtoMediaTypeConnectionDetails;
      if(mediaType == Operations.EMediaType.MIP) {
        connection = new Operations.DtoMipConnectionDetails();
        (<Operations.DtoMipConnectionDetails>connection).mipconnectionParametersDto = new Operations.DtoMipConnectionParameters();
      }
      $scope.connection = connection;
    }

    $scope.setActiveTab( 'general');
  };

  $scope.$watch('selectedDataCenter', function(value) {
    $scope.testConnectionInProgress = false;
  });

  $scope.setActiveTab = function(tabName:string) {
    $scope.activeTab=tabName;
  };

  var getResource = function() {
    var res;
    if ($scope.connection.mediaType == Operations.EMediaType.MIP) {
      res = mipConnectionResource
    }
    return  res;
  };

  init(data.mediaType);

  var lastRequestTime = null;

  $scope.testConnection = function() {
    $scope.connectivityTest = null;
    $scope.failureMessage = null;
    if ($scope.myForm.$valid) {
      let currReqTime = (new Date()).getTime();
      lastRequestTime = currReqTime;
      $scope.testConnectionInProgress = true;

      if( $scope.editMode) {
        const copyOfCon =  _.cloneDeep($scope.connection);
        (<Operations.DtoMipConnectionDetails>copyOfCon).mipconnectionParametersDto.password =
          (<Operations.DtoMipConnectionDetails>copyOfCon).mipconnectionParametersDto.password == psaudoPassword
          ? null:(<Operations.DtoMipConnectionDetails>copyOfCon).mipconnectionParametersDto.password ;

        getResource().testConnectionWithDatacenter(copyOfCon, $scope.selectedDataCenter.id,
          function (success: boolean, failureMessage) {
            if (currReqTime == lastRequestTime) {
              $scope.testConnectionInProgress = false;
              $scope.connectivityTest = success;
              $scope.failureMessage = failureMessage;
            }
          }
          ,function(){
            if (currReqTime == lastRequestTime) {
              onTestConnectionError();
            }
          });
      }
      else {
        getResource().testConnectionWithDatacenter($scope.connection, $scope.selectedDataCenter.id,
          function (success: boolean, failureMessage) {
            if (currReqTime == lastRequestTime) {
              $scope.testConnectionInProgress = false;
              $scope.connectivityTest = success;
              $scope.failureMessage = failureMessage;
            }
          }
          ,function(){
          if (currReqTime == lastRequestTime) {
            onTestConnectionError();
          }
        });
      }
    }
  };

  var onTestConnectionError  = () => {
    $scope.testConnectionInProgress = false;
    dialogs.error('Error','Sorry, failed to test connection via datacenter '+(<Operations.DtoCustomerDataCenter>$scope.selectedDataCenter).name);
  };

  $scope.cancel = function(){
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function() {
    $scope.submitted = true;
    if ($scope.myForm.$valid) {
      if ($scope.editMode) {
       if((<Operations.DtoMipConnectionDetails>$scope.connection).mipconnectionParametersDto.password == psaudoPassword)
       {
         (<Operations.DtoMipConnectionDetails>$scope.connection).mipconnectionParametersDto.password =null;
       }
        $uibModalInstance.close($scope.connection);
      }
      else {
        (<Operations.DtoMediaTypeConnectionDetails>$scope.connection).name = !(<Operations.DtoMediaTypeConnectionDetails>$scope.connection).name ||
        (<Operations.DtoMediaTypeConnectionDetails>$scope.connection).name.trim()==''?null: (<Operations.DtoMediaTypeConnectionDetails>$scope.connection).name;

        getResource().addNewConnection($scope.connection, function (newConnection:Operations.DtoMediaTypeConnectionDetails) {
              $uibModalInstance.close(newConnection);
            }
            , function (error) {
              if (error.status == 409) {
                dialogs.error('Error', 'Same connection configuration already exists.');
              }
              else {
                onError();
              }
            })
      }
    }
  };

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };

  $scope.$on("$destroy", function() {
    eventCommunicator.unregisterAllHandlers($scope);
  });

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
});



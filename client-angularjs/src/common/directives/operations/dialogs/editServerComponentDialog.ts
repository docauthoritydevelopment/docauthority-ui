///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.servercomponents', [

  ])
  .controller('editServerComponentDialogCtrl', function ($window, $scope, $uibModalInstance, data,customerDataCenterResource:ICustomerDataCenterResource,
                                                         userSettings:IUserSettingsService,dialogs,serverComponentResource:IServerComponentResource) {


    $scope.getLastSelectedDataCenterFn=userSettings.getLastSelectMediaProcessorDataCenter;
    $scope.setLastSelectedDataCenterFn=userSettings.setLastSelectMediaProcessorDataCenter;

    var init=function()
    {
      let action = "New";

      $scope.serverComponent =(<Operations.DtoClaComponent>data.serverComponent);
      if($scope.serverComponent)
      {
        action = "Edit";
        $scope.editMode = true;
      } else {
        var component = new Operations.DtoClaComponent();
        component.state = Operations.EComponentState.PENDING_INIT;
        component.claComponentType = Operations.EClaComponentType.MEDIA_PROCESSOR;
        component.active = true;

        component.customerDataCenterDto = data.dataCenterItem ? (<Operations.DtoCustomerDataCenter>data.dataCenterItem) : $scope.serverComponent.customerDataCenterDto;
        $scope.serverComponent = component;
      }

      $scope.title = action + " " + (data.mediaProcessorOnly ? "media processor" : "server component");
      $scope.editInstanceId =  $scope.serverComponent.state === Operations.EComponentState.PENDING_INIT;
      $scope.initiated1 = true;
    };

    init();

    $scope.isMediaProcessor = function() {
      return data.mediaProcessorOnly || (<Operations.DtoClaComponent>$scope.serverComponent).claComponentType == Operations.EClaComponentType.MEDIA_PROCESSOR;
    }

    $scope.isMediaProcessorOnly = function() {
      return data.mediaProcessorOnly;
    }

    $scope.cancel = function(){
      $uibModalInstance.dismiss('Canceled');
    }; // end cancel

    $scope.save = function(){
      $scope.submitted=true;

      if(!$scope.serverComponent || ! $scope.serverComponent.customerDataCenterDto || !$scope.myForm.$valid) {
        return;
      }
      (<Operations.DtoClaComponent>$scope.serverComponent).lastResponseDate = null; //server expect number, we converted it to date
      (<Operations.DtoClaComponent>$scope.serverComponent).createdOnDB = null;
      (<Operations.DtoClaComponent>$scope.serverComponent).state = null;

      if ($scope.editMode) {
        serverComponentResource.updateServerComponent($scope.serverComponent,
          function (serverComponent) {
            $scope.saveLastSelectedDataCenter? $scope.saveLastSelectedDataCenter():null;
            $uibModalInstance.close(serverComponent);
          }, function (response) {
            if (response.status === 409) {
              dialogs.error('Error', 'InstanceID is already exists.');
            }

          });
      }
      else {
        serverComponentResource.addServerComponent($scope.serverComponent,
          function (serverComponent) {
            $scope.saveLastSelectedDataCenter();
            $uibModalInstance.close(serverComponent);
          }, function (response) {
            if (response.status === 409) {
              dialogs.error('Error', 'InstanceID is already exists.');
            }
          });
      }
    };

    $scope.hitEnter = function(evt){
      if(angular.equals(evt.keyCode,13))
        $scope.save();
    };

    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }
  })


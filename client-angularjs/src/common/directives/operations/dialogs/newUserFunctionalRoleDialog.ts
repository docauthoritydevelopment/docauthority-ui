///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.user.functionalRole.new', [])
  .controller('newUserFunctionalRoleItemDialogCtrl',
    function ($window, $scope, $uibModalInstance, data, dialogs, $filter,
              eventCommunicator: IEventCommunicator, functionalRolesResource:IFunctionalRolesResource, templateRolesResource: IUserTemplateRolesResource, propertiesUtils) {

      $scope.setActiveTab = function (tabName: string) {
        $scope.activeTab = tabName;
      }

      var init = function (openWithFieldName) {
        $scope.functionalRoleSummaryInfo = <Users.DtoFunctionalRoleSummaryInfo>(data.functionalRoleSummaryInfo);
        $scope.setActiveTab(data.openWithFieldName?data.openWithFieldName: 'general');

        $scope.userManagerSelectedItem = [];
        $scope.userFunctionalRoleLdapSavedSelectedItems = [];

        if ($scope.functionalRoleSummaryInfo) {
          if(!_.isEmpty($scope.functionalRoleSummaryInfo.ldapGroupMappingDtos)) {
            $scope.userFunctionalRoleLdapSavedSelectedItems = [createLdapGroupSelectedItem($scope.functionalRoleSummaryInfo.ldapGroupMappingDtos[0])];
          }
          if((<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto) {
            $scope.userManagerSelectedItem = [createUserManagerSelectedItem((<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto.managerUsername)];
            $scope.title = 'Edit data role';

            if((<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto.template) {
              $scope.templateRoleSelected = $scope.functionalRoleSummaryInfo.functionalRoleDto.template;
              $scope.userTemplateRolesSavedSelectedItems = [createTemplateRoleSelectedItem($scope.templateRoleSelected)];
            }
          }

          $scope.editMode = true;
        }else {
          $scope.functionalRoleSummaryInfo = new Users.DtoFunctionalRoleSummaryInfo();
          var funRole = new Users.DtoFunctionalRole();
          funRole.template = new Users.DtoTemplateRole();
          funRole.template.systemRoleDtos = [];
          $scope.functionalRoleSummaryInfo.functionalRoleDto = funRole;
          $scope.functionalRoleSummaryInfo.ldapGroupMappingDtos = [];
          $scope.title = 'Add data role';
        }
      };

      var createTemplateRoleSelectedItem = function(template){
        var newItem = new ParamOption();
        newItem.id = template.id;
        newItem.display = template.displayName;
        newItem.value = template.id;

        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;

        return paramActiveItem;
      }

      var createLdapGroupSelectedItem = function(ldapGroup) {
        var newItem = createLdapGroupParamOption(ldapGroup);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      };

      var createUserManagerSelectedItem = function(userManager) {
        var newItem = createUserManagerParamOption(userManager);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      };

      var createUserManagerParamOption = function (groupManager: Users.DtoLdapGroup) {
        var newItem = new ParamOption();
        newItem.id = groupManager;
        newItem.display = groupManager;
        newItem.value = groupManager;
        return newItem;
      };

      var createLdapGroupParamOption = function (ldapGroup: Users.DtoLdapGroup) {
        let newItem = new ParamOption();
        newItem.id = ldapGroup.id;
        newItem.display = ldapGroup.groupName;
        newItem.value = ldapGroup.id;
        return newItem;
      };

      var createSystemRoleNewSelectedItem= function(role:Users.DtoSystemRole) {
        var newItem = $scope.createSystemRoleParamOption(role);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      };

      var createTemplateRoleParamOption = function (template: Users.DtoTemplateRole) {
        var newItem = new ParamOption();
        newItem.id = template.id;
        newItem.display = template.displayName;
        newItem.subdisplay = template.description || template.displayName;
        newItem.value = template;
        return newItem;
      };

      $scope.openNewTemplateRoleDialog = function(successFunction,cancelFunction)
      {
        var dlg = dialogs.create(
          'common/directives/operations/dialogs/newUserTemplateRoleDialog.tpl.html',
          'newUserTemplateRoleDialogCtrl',
          {},
          {size: 'md',windowClass: 'offsetDialog'});

        dlg.result.then(function (tempateRole:Users.DtoTemplateRole) {
          successFunction(tempateRole);
        }, function () {
          cancelFunction?cancelFunction():null;
        });

      }

      $scope.createTemplateRoleParamOption= function(template:Users.DtoTemplateRole) {
        var newItem = new ParamOption();
        newItem.id = template.id;
        newItem.display = template.displayName;
        newItem.value = template;
        return newItem;
      };

      $scope.onTemplateRoleItemSelected = function(templateRole:ParamActiveItem<any>) {
        $scope.templateRoleSelected = templateRole.item;
      }

      $scope.isTemplateRoleValid = function() {
        return  !_.isEmpty($scope.templateRoleSelected);
      };

      $scope.onLdapGroupManagerSelected = function (groupManager) {
        if (groupManager) {
          $scope.selectedGroupManager = groupManager;
          $scope.functionalRoleSummaryInfo.functionalRoleDto.managerUsername = groupManager.item.display;
        }
      };

      $scope.loadTemplateRoleOptionItems = function (itemsOptions: any[]) {
        templateRolesResource.getTemplateRoles(Users.ETemplateType.FUNCROLE,function (templates: Users.DtoTemplateRole[]) {
            if (templates) {
              templates.forEach(template => {
                var newItem = createTemplateRoleParamOption(template);
                itemsOptions.push(newItem);
              })
            }
          })
      };

      $scope.onLdapGroupItemSelected = function (ldapGroup) {

        $scope.functionalRoleSummaryInfo.ldapGroupMappingDtos = [];

        if(ldapGroup) {
          let newLdapGroup = new Users.DtoLdapGroup();
          newLdapGroup.groupName = ldapGroup.item.display;
          $scope.functionalRoleSummaryInfo.ldapGroupMappingDtos.push(newLdapGroup);
        }
      };

      $scope.createNewLdapGroupSelectedItemFromText = function(text) {
        var ldapGroup = new Users.DtoLdapGroup();
        ldapGroup.groupName = text;

        var newItem = createLdapGroupParamOption(ldapGroup);
        var paramActiveItem = new ParamActiveItem();

        paramActiveItem.active = true;
        paramActiveItem.item = newItem;

        return paramActiveItem;
      };

      $scope.createNewGroupManagerSelectedItemFromText = function(text) {

        var newItem = createUserManagerParamOption(text);
        var paramActiveItem = new ParamActiveItem();

        paramActiveItem.active = true;
        paramActiveItem.item = newItem;

        return paramActiveItem;
      };

      $scope.isGroupManagerSelected = function () {
        return _.isObject($scope.selectedLdapGroup);
      };

      $scope.loadLdapGroupOptionItems = function(itemsOptions:any[]) {
        functionalRolesResource.getFunctionalRolesLdapGroups(
          function (groups:Users.DtoLdapGroup[]) {
            groups.forEach(group=> {
              var newItem = createLdapGroupParamOption(group);
              itemsOptions.push(newItem);
            })
          },
          function() {
          });
      }

      $scope.cancel = function () {
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function () {
        $scope.submitted = true;
        if ($scope.generalForm.$valid && $scope.isTemplateRoleValid() ) {
          (<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto.template = $scope.templateRoleSelected;
          (<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto.displayName =   (<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo).functionalRoleDto.name;

          if ($scope.editMode) {
            $uibModalInstance.close($scope.functionalRoleSummaryInfo);
          }
          else {
            functionalRolesResource.addNewFunctionalRole((<Users.DtoFunctionalRoleSummaryInfo>$scope.functionalRoleSummaryInfo), function (newFunctionalRoleSummaryInfo:Users.DtoFunctionalRoleSummaryInfo) {

                  $uibModalInstance.close(newFunctionalRoleSummaryInfo);
                }
                , function (error) {
                    if (error.status == 400 &&error.data.type =='ITEM_ALREADY_EXISTS')
                    {
                      dialogs.error('Error', 'Functional role with this name already exists. \n\nSelect another name.');
                    }
                    else {
                      dialogs.error('Error', 'Failed to add functional role.');
                    }
                    $scope.cancel();
                })
          }
        }
      };

      $scope.hitEnter = function (evt) {
        if (angular.equals(evt.keyCode, 13))
          $scope.save();
      };

      $scope.$on("$destroy", function () {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function () {
        dialogs.error('Error', 'Sorry, an error occurred.');
      }

      init(data.openWithFieldName);
    });


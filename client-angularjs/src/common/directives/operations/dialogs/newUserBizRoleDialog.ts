///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.user.bizRole.new', [

    ])
    .controller('newUserBizRoleItemDialogCtrl',
        function ($window, $scope, $uibModalInstance, data, dialogs,  $filter,
                  eventCommunicator:IEventCommunicator, userBizRolesResource:IUserBizRolesResource, templateRolesResource: IUserTemplateRolesResource, propertiesUtils) {

          var init=function(openWithFieldName) {
            $scope.bizRoleSummaryInfo = <Users.DtoBizRoleSummaryInfo>(data.bizRoleSummaryInfo);

            if ($scope.bizRoleSummaryInfo) {
              $scope.setActiveTab('templateRoles');
              var bizRoleSummaryInfo = (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo);
              $scope.title = 'Edit operational role';
              if(!_.isEmpty(bizRoleSummaryInfo.ldapGroupMappingDtos)) {
                $scope.bizRoleRoleLdapSavedSelectedItems = [createLdapGroupSelectedItem(bizRoleSummaryInfo.ldapGroupMappingDtos[0])];
              }
              if((<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).bizRoleDto.template) {
                $scope.templateRoleSelected = $scope.bizRoleSummaryInfo.bizRoleDto.template;
                $scope.userTemplateRolesSavedSelectedItems = [createTemplateRoleSelectedItem($scope.templateRoleSelected)]
              }
              $scope.editMode=true;
            }
            else {
              var bizRoleSummaryInfo = new Users.DtoBizRoleSummaryInfo();
              var  bizRole = new Users.DtoBizRole();
              bizRole.template = new Users.DtoTemplateRole();
              bizRole.template.systemRoleDtos = [];
              bizRoleSummaryInfo.bizRoleDto =bizRole;
              bizRoleSummaryInfo.ldapGroupMappingDtos = [];
              $scope.bizRoleSummaryInfo = bizRoleSummaryInfo;
              $scope.title = 'Add operational role';
            }
            $scope.setActiveTab(openWithFieldName?openWithFieldName: 'templateRoles');
          }

          $scope.setActiveTab=function(tabName:string)
          {
            $scope.activeTab=tabName;
          }


          var createTemplateRoleSelectedItem = function(template){
            var newItem = new ParamOption();
            newItem.id = template.id;
            newItem.display = template.displayName;
            newItem.value = template.id;

            var paramActiveItem = new ParamActiveItem();
            paramActiveItem.active = true;
            paramActiveItem.item = newItem;

            return paramActiveItem;
          }

          var createTemplateRoleParamOption = function (template: Users.DtoTemplateRole) {
            var newItem = new ParamOption();
            newItem.id = template.id;
            newItem.display = template.displayName;
            newItem.subdisplay = template.description || template.displayName;
            newItem.value = template;
            return newItem;
          };

          $scope.openNewTemplateRoleDialog = function(successFunction,cancelFunction)
          {
            var dlg = dialogs.create(
              'common/directives/operations/dialogs/newUserTemplateRoleDialog.tpl.html',
              'newUserTemplateRoleDialogCtrl',
              {},
              {size: 'md',windowClass: 'offsetDialog'});

            dlg.result.then(function (tempateRole:Users.DtoTemplateRole) {
              successFunction(tempateRole);
            }, function () {
              cancelFunction?cancelFunction():null;
            });

          }

          $scope.createTemplateRoleParamOption= function(template:Users.DtoTemplateRole) {
            var newItem = new ParamOption();
            newItem.id = template.id;
            newItem.display = template.displayName;
            newItem.value = template;
            return newItem;
          };

          $scope.onTemplateRoleItemSelected = function(templateRole:ParamActiveItem<any>) {
            $scope.templateRoleSelected = templateRole.item;
          }

          $scope.isTemplateRoleValid = function() {
            return !_.isEmpty($scope.templateRoleSelected);
          };

          $scope.loadTemplateRoleOptionItems = function (itemsOptions: any[]) {
            templateRolesResource.getTemplateRoles(Users.ETemplateType.BIZROLE, function (templates: Users.DtoTemplateRole[]) {
              if (templates) {
                templates.forEach(template => {
                  var newItem = createTemplateRoleParamOption(template);
                  itemsOptions.push(newItem);
                })
              }
            })
          };
          $scope.loadLdapGroupOptionItems = function(itemsOptions:any[]) {
            userBizRolesResource.getBizRolesLdapGroups(
              function (groups:Users.DtoLdapGroup[]) {
                groups.forEach(group=> {
                  var newItem = createLdapGroupParamOption(group);
                  itemsOptions.push(newItem);
                })
              },
              function() {
              });
          }

          var createLdapGroupSelectedItem = function(ldapGroup) {
            var newItem = createLdapGroupParamOption(ldapGroup);
            var paramActiveItem = new ParamActiveItem();
            paramActiveItem.active = true;
            paramActiveItem.item = newItem;
            return paramActiveItem;
          };

          var createLdapGroupParamOption = function (ldapGroup: Users.DtoLdapGroup) {
            let newItem = new ParamOption();
            newItem.id = ldapGroup.id;
            newItem.display = ldapGroup.groupName;
            newItem.value = ldapGroup.id;
            return newItem;
          }

          $scope.onLdapGroupItemSelected = function (ldapGroup) {

            (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).ldapGroupMappingDtos = [];

            if(ldapGroup) {
              let newLdapGroup = new Users.DtoLdapGroup();
              newLdapGroup.groupName = ldapGroup.item.display;
              (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).ldapGroupMappingDtos.push(newLdapGroup);
            }
          };

          $scope.createNewLdapGroupSelectedItemFromText = function(text) {
            var ldapGroup = new Users.DtoLdapGroup();
            ldapGroup.groupName = text;

            var newItem = createLdapGroupParamOption(ldapGroup);
            var paramActiveItem = new ParamActiveItem();

            paramActiveItem.active = true;
            paramActiveItem.item = newItem;

            return paramActiveItem;
          };
          init(data.openWithFieldName);

          $scope.cancel = function(){
            $uibModalInstance.dismiss('Canceled');
          }; // end cancel

          $scope.save = function() {
            $scope.submitted = true;
            if ($scope.generalForm.$valid &&$scope.isTemplateRoleValid()) {
              (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).bizRoleDto.template = $scope.templateRoleSelected;
              (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).bizRoleDto.displayName =(<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).bizRoleDto.displayName;
              var bizRoleSummaryInfo = new Users.DtoBizRoleSummaryInfo();
              //Create new object to be sent to server cause grid object has additional fields with tooltip html and etc...
              bizRoleSummaryInfo.bizRoleDto =  (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).bizRoleDto;
              bizRoleSummaryInfo.ldapGroupMappingDtos =  (<Users.DtoBizRoleSummaryInfo>$scope.bizRoleSummaryInfo).ldapGroupMappingDtos;
              if ($scope.editMode) {
                $uibModalInstance.close(bizRoleSummaryInfo);
              }
              else {
                userBizRolesResource.addNewBizRole(bizRoleSummaryInfo, function (newBizRole:Users.DtoBizRoleSummaryInfo) {
                      $uibModalInstance.close(newBizRole);
                    },function (error) {
                      dialogs.error('Error', 'Sorry, an error occured - '+error.data.message);
                });
              }
            }
          };

          $scope.hitEnter = function(evt){
            if(angular.equals(evt.keyCode,13))
              $scope.save();
          };

          $scope.$on("$destroy", function() {
            eventCommunicator.unregisterAllHandlers($scope);
          });
          var onError = function()
          {
            dialogs.error('Error','Sorry, an error occurred.');
          }

        })



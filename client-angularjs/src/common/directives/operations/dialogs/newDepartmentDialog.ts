///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.department.new', [])
    .controller('newDepartmentDialogCtrl',function ($scope, $uibModalInstance, $filter, data, dialogs, departmentResource:IDepartmentResource, eventCommunicator:IEventCommunicator, localizedMessages) {
      var init=function() {
        $scope.editMode = false;
        $scope.department = <Operations.DtoDepartment>(data.department);
        if ($scope.department) {
          $scope.title = 'Edit ' + data.componentName;
          $scope.editMode = true;
        }
        else {
          $scope.department = new Operations.DtoDepartment();
          $scope.title = 'Add ' + data.componentName;
          $scope.department.parentId = null;
        }

        $scope.departmentList = [{id: null, name: "None"}];
        departmentResource.getDepartments(function (departments:Operations.DtoDepartment[]) {
          departments.forEach(d => {
            if (d.parentId == null) {
              $scope.departmentList.push({id: d.id, name: d.name});
            }
          });
        }, onError);
      };

      init();

      $scope.cancel = function() {
        $uibModalInstance.dismiss('Canceled');
      };

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.departmentForm.$valid) {
          if ($scope.editMode) {
            $uibModalInstance.close($scope.department);
          }
          else {
            departmentResource.addNewDepartment($scope.department, function (newDepartment:Operations.DtoDepartment) {
                  $uibModalInstance.close(newDepartment);
                }
                , onError);
          }
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function(err) {
        dialogs.error('Error',`Sorry, an error occurred - ${err.data.message}`);
      };
    });



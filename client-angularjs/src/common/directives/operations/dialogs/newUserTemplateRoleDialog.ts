'use strict';

angular.module('directives.dialogs.user.templateRole.new', [
  'resources.templateRoles'
  ])
  .controller('newUserTemplateRoleDialogCtrl',
    function ($window, $scope, $uibModalInstance, data, dialogs, $filter,
              eventCommunicator: IEventCommunicator, templateRolesResource:IUserTemplateRolesResource,propertiesUtils) {

      $scope.setActiveTab = function (tabName: string) {
        $scope.activeTab = tabName;
      }

      var init = function (openWithFieldName) {
        $scope.templateRole = <Users.DtoTemplateRole>(data.templateRole);
        $scope.setActiveTab(data.openWithFieldName?data.openWithFieldName: 'general');

        if ($scope.templateRole) {
          initSystemRoles((<Users.DtoTemplateRole>$scope.templateRole).systemRoleDtos);
          $scope.editMode = true;
          $scope.title = 'Edit role template';
        }else {
          $scope.templateRole = new Users.DtoTemplateRole();
          $scope.title = 'Add role template';
        }
        setTemplateTypesList();
      };

      var setTemplateTypesList = function() {
        $scope.templateTypes=[
          {
            text:Users.ETemplateType.BIZROLE,
            value:Users.ETemplateType.BIZROLE
          },
          {
            text:Users.ETemplateType.FUNCROLE,
            value:Users.ETemplateType.FUNCROLE
          },
          {
            text:'both',
            value:Users.ETemplateType.NONE
          }
        ];
        $scope.templateRole.templateType = $scope.templateRole.templateType || Users.ETemplateType.NONE;
      };

      var initSystemRoles = function (roles: Users.DtoSystemRole[]) {
        var userSystemRolesOptions = [];
        if (roles) {
          roles.sort((t1, t2)=> propertiesUtils.sortAlphbeticFun('name'));
          roles.forEach(r => {
            userSystemRolesOptions.push(createSystemRoleNewSelectedItem(r));
          })
        }
        $scope.userSystemRolesSavedSelectedItems = userSystemRolesOptions;
      };

      var createSystemRoleNewSelectedItem = function (role: Users.DtoSystemRole) {
        var newItem = $scope.createSystemRoleParamOption(role);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      };

      $scope.loadSystemRoleOptionItems = function (itemsOptions: any[]) {
        templateRolesResource.getSystemRoles(function (roles: Users.DtoSystemRole[]) {
            if (roles) {
              roles.sort( propertiesUtils.sortAlphbeticFun('name'));
              roles.forEach(role => {
                var newItem = $scope.createSystemRoleParamOption(role);
                itemsOptions.push(newItem);
              })
            }
          }
          , onError)
      };

      $scope.onSystemRoleItemSelected = function (systemRoles: ParamActiveItem<Users.DtoSystemRole>[]) {
        $scope.systemRolesSelected = systemRoles.filter(roleItem => (<ParamActiveItem<Users.DtoSystemRole>>roleItem).active).map(systemRole => systemRole.item.value);
        (<Users.DtoTemplateRole>$scope.templateRole).systemRoleDtos = $scope.systemRolesSelected;
      };

      $scope.isSystemRolesValid = function () {
        return $scope.systemRolesSelected && $scope.systemRolesSelected.length > 0
      };

      $scope.createSystemRoleParamOption = function (role: Users.DtoSystemRole) {
        var newItem = new ParamOption();
        newItem.id = role.id;
        newItem.display = role.displayName;
        newItem.subdisplay = role.description ? role.description : '';
        newItem.value = role;
        return newItem;
      };

      $scope.loadSystemRoleOptionItems = function (itemsOptions: any[]) {
        templateRolesResource.getSystemRoles(function (roles: Users.DtoSystemRole[]) {
            if (roles) {
              roles.sort( propertiesUtils.sortAlphbeticFun('name')); //actually should be sorted after translate text
              roles.forEach(role => {
                var newItem = $scope.createSystemRoleParamOption(role);
                itemsOptions.push(newItem);
              })
            }
          }
          , onError)
      };

      $scope.isSystemRolesValid = function () {
        return $scope.systemRolesSelected && $scope.systemRolesSelected.length > 0
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('Canceled');
      };

      $scope.save = function () {
        $scope.submitted = true;
        if ($scope.generalForm.$valid && $scope.isSystemRolesValid()) {
          (<Users.DtoTemplateRole>$scope.templateRole).systemRoleDtos = $scope.systemRolesSelected;
          (<Users.DtoTemplateRole>$scope.templateRole).displayName = (<Users.DtoTemplateRole>$scope.templateRole).name;

          if ($scope.editMode) {
            $uibModalInstance.close($scope.templateRole);
          }
          else {
            templateRolesResource.addNewTemplateRole((<Users.DtoTemplateRole>$scope.templateRole), function (newTemplateRole: Users.DtoTemplateRole) {
                $uibModalInstance.close(newTemplateRole);
              }
              , function (error) {
                if (error.status == 400 &&error.data.type =='ITEM_ALREADY_EXISTS')
                {
                  dialogs.error('Error', 'Role template with this name already exists. \n\nSelect another name.');
                }
                else {
                  dialogs.error('Error', 'Failed to add role template.');
                }
                $scope.cancel();
              })
          }
        }
      };

      $scope.hitEnter = function (evt) {
        if (angular.equals(evt.keyCode, 13))
          $scope.save();
      };

      $scope.$on("$destroy", function () {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function () {
        dialogs.error('Error', 'Sorry, an error occurred.');
      }

      init(data.openWithFieldName);
    });

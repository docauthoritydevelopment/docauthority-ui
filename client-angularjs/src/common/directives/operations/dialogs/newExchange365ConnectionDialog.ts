///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.exchange.new', [

    ])
  .controller('newExchange365ConnectionItemDialogCtrl',
    function ($scope, $uibModalInstance, data, dialogs,
              exchange365ConnectionResource:IMediaTypeConnectionResource<Operations.DtoExchange365ConnectionDetails>,
              $filter,
              eventCommunicator:IEventCommunicator) {
      const psaudoPassword = '**********************';

      var init=function() {

        $scope.connection = <Operations.DtoExchange365ConnectionDetails>(data.connection);
        if ($scope.connection) {
          $scope.editMode = true;
          if ((<Operations.DtoExchange365ConnectionDetails>$scope.connection).exchangeConnectionParametersDto) {
            (<Operations.DtoExchange365ConnectionDetails>$scope.connection).exchangeConnectionParametersDto.password = psaudoPassword;
          }
        }
        else {
          var connection= new Operations.DtoExchange365ConnectionDetails();
          connection.exchangeConnectionParametersDto = new Operations.DtoExchangeConnectionParameters();
          $scope.connection = connection;

        }
        $scope.setActiveTab( 'general');
      }
      $scope.setActiveTab=function(tabName:string)
      {
        $scope.activeTab=tabName;
      }
      init();

/*
      $scope.testConnection = function(){
        $scope.connectivityTest = null;
        $scope.failureMessage = null;

        if ($scope.myForm.$valid) {
          $scope.testConnectionInProgress = true;
          if($scope.editMode)
          {
            const copyOfCon =  _.cloneDeep($scope.connection);
           (<Operations.DtoExchange365ConnectionDetails>copyOfCon).id =null;
            // exchange365ConnectionResource.testConnectionWithDatacenterWithSavedConnection(copyOfCon.id,copyOfCon, $scope.selectedDataCenter.id, function (success: boolean, msg) {
            //     $scope.testConnectionInProgress = false;
            //     $scope.connectivityTest = success;
            //     if (success) {
            //       $scope.connection.username = msg;
            //       $scope.failureMessage = '';
            //     }
            //     else {
            //       $scope.failureMessage = msg;
            //     }
            //   }
            //   , onTestConnectionError)
          }
          else {
            // exchange365ConnectionResource.testConnectionWithDatacenter((<Operations.DtoExchange365ConnectionDetails>$scope.connection), $scope.selectedDataCenter.id, function (success: boolean, msg) {
            //     $scope.testConnectionInProgress = false;
            //     $scope.connectivityTest = success;
            //     if (success) {
            //       $scope.connection.username = msg;
            //       $scope.failureMessage = '';
            //     }
            //     else {
            //       $scope.failureMessage = msg;
            //     }
            //   }
            //   , onTestConnectionError)
          }
        }
      };
      var onTestConnectionError  = () =>
      {
        $scope.testConnectionInProgress = false;
        dialogs.error('Error','Sorry, failed to test connection via datacenter '+(<Operations.DtoCustomerDataCenter>$scope.selectedDataCenter).name);
      }
*/
      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.myForm.$valid) {
          if ($scope.editMode) {
            if((<Operations.DtoExchange365ConnectionDetails>$scope.connection).exchangeConnectionParametersDto.password == psaudoPassword)
            {
              (<Operations.DtoExchange365ConnectionDetails>$scope.connection).exchangeConnectionParametersDto.password =null;
            }
            $uibModalInstance.close($scope.connection);
          }
          else {
            (<Operations.DtoExchange365ConnectionDetails>$scope.connection).name = !(<Operations.DtoExchange365ConnectionDetails>$scope.connection).name ||
            (<Operations.DtoExchange365ConnectionDetails>$scope.connection).name.trim()==''?null: (<Operations.DtoExchange365ConnectionDetails>$scope.connection).name;

            exchange365ConnectionResource.addNewConnection($scope.connection, function (newConnection:Operations.DtoExchange365ConnectionDetails) {
                $uibModalInstance.close(newConnection);
              }
              , function (error) {
                if (error.status == 409) {
                  dialogs.error('Error', 'Same connection configuration already exists.');
                }
                else {
                  onError();
                }
              })
          }

        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    })



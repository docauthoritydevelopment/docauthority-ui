///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.rootFolders.new', []).controller('newRootFolderItemDialogCtrl',
        function ($window, $scope, $uibModalInstance, data, dialogs, $q,sharePointConnectionResource:IMediaTypeConnectionResource<Operations.DtoSharePointConnectionDetails>,
                  currentUserDetailsProvider:ICurrentUserDetailsProvider, oneDriveConnectionResource:IMediaTypeConnectionResource<Operations.DtoSharePointConnectionDetails>,
                  boxConnectionResource:IMediaTypeConnectionResource<Operations.DtoBoxConnectionDetails>,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                  exchange365ConnectionResource:IMediaTypeConnectionResource<Operations.DtoExchange365ConnectionDetails>,
                  propertiesUtils, rootFoldersResource:IRootFoldersResource, userSettings:IUserSettingsService, configuration:IConfig,localStorage:IUserStorageService,
                  scheduleGroupResource:IScheduleGroupResource, departmentResource:IDepartmentResource, $filter) {
  $scope.EMediaType=Operations.EMediaType;

  $scope.storeLocationsOptions=[
    Operations.DtoRootFolder.storeLocation_UNDEFINED,
    Operations.DtoRootFolder.storeLocation_CLOUD_SAAS,
    Operations.DtoRootFolder.storeLocation_CLOUD_PASS,
    Operations.DtoRootFolder.storeLocation_CLOUD_FILESHARE,
    Operations.DtoRootFolder.storeLocation_LOCAL_DATACENTER,
    Operations.DtoRootFolder.storeLocation_REMOTE_DATACENTER,
    Operations.DtoRootFolder.storeLocation_OTHER
  ];
  $scope.storePurposeOptions=[
    Operations.DtoRootFolder.storePurpose_UNDEFINED,
    Operations.DtoRootFolder.storePurpose_PUBLIC,
    Operations.DtoRootFolder.storePurpose_DEPARTMENT,
    Operations.DtoRootFolder.storePurpose_PERSONAL,
    Operations.DtoRootFolder.storePurpose_IT
  ];
  $scope.storeSecurityOptions=[
    Operations.DtoRootFolder.storeSecurity_CLEAR,
    Operations.DtoRootFolder.storeSecurity_ENCRYPTED
  ];
  $scope.storeSecurityOptions=[
    Operations.DtoRootFolder.storeSecurity_CLEAR,
    Operations.DtoRootFolder.storeSecurity_ENCRYPTED
  ];
  $scope.mediaTypeOptions=[
    Operations.EMediaType.FILE_SHARE,
     Operations.EMediaType.SHARE_POINT,
     Operations.EMediaType.ONE_DRIVE,
     Operations.EMediaType.BOX
  ];
  $scope.supportedMapFileTypes = JSON.parse(configuration.rootfolder_scan_fileTypes);
  $scope.supportedIngestFileTypes = JSON.parse(configuration.rootfolder_scan_fileTypes);

  $scope.connectionIconClass=configuration.icon_mediaTypeConnection;
  $scope.getLastSelectedDataCenterFn=userSettings.getLastSelectRootFolderDataCenter;
  $scope.setLastSelectedDataCenterFn=userSettings.setLastSelectRootFolderDataCenter;

  $scope.installationMode = configuration['installation-mode'] ? configuration['installation-mode'] : 'standard';

  var createScheduleGroupNewSelectedItem= function(newSchedule:Operations.DtoScheduleGroup) {
    var newItem = $scope.createScheduleParamOption(newSchedule);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var createDepartmentNewSelectedItem= function(newDepartment:Operations.DtoDepartment) {
    var newItem = $scope.createDepartmentParamOption(newDepartment);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var createSharePointNewSelectedItem= function(connection:Operations.DtoSharePointConnectionDetails) {
    var newItem = $scope.createMicrosoftConnectionParamOption(connection);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var createBoxNewSelectedItem= function(connection:Operations.DtoBoxConnectionDetails) {
    var newItem = $scope.createBoxConnectionParamOption(connection);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var createExchange365NewSelectedItem= function(connection:Operations.DtoExchange365ConnectionDetails) {
    var newItem = $scope.createExchange365ConnectionParamOption(connection);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  $scope.createScheduleParamOption= function(schedule:Operations.DtoScheduleGroup) {
    var newItem = new ParamOption<Operations.DtoScheduleGroup>();
    newItem.id = schedule.id;
    newItem.display = schedule.name;
    var schedulingDescriptionPart =schedule.schedulingDescription?schedule.schedulingDescription:' ';
    newItem.subdisplay = schedulingDescriptionPart+' ' +scheduleConfigGetSummaryText(schedule.scheduleConfigDto);
    newItem.value = schedule;
    return newItem;
  };

  $scope.createDepartmentParamOption= function(department:Operations.DtoDepartment) {
    var newItem = new ParamOption<Operations.DtoDepartment>();

    if (department) {
      newItem.id = department.id;
      newItem.display = department.name;
      if( department.parentId && $scope.depParentMapping[department.parentId]) {
        newItem.subdisplay =  $scope.depParentMapping[department.parentId];
      }
      else{
        newItem.subdisplay = ' ';
        newItem.displayClass = 'bolder'
      }
      newItem.value = department;
      newItem.icon = configuration.icon_department + ' tag tag-' + splitViewBuilderHelper.getDepartmentStyleId(<any>department);
      if ($scope.depParentMapping) {
        newItem.groupByLable = department.parentId ? $scope.depParentMapping[department.parentId] : department.name;
      }
      else {
        newItem.groupByLable = "";
      }
    }
    else {
      newItem.id = -1;
      newItem.display = "---------- No Department ----------";
      if (currentUserDetailsProvider.isInstallationModeLegal()) {
        newItem.display = "---------- No Matter ----------";
      }
      newItem.subdisplay = ' ';
      newItem.value = null;
      newItem.groupByLable = "";
    }

    return newItem;
  };

  $scope.createMicrosoftConnectionParamOption= function(serverConnection:Operations.DtoSharePointConnectionDetails) {
    var newItem = new ParamOption<Operations.DtoSharePointConnectionDetails>();
    newItem.id = serverConnection.id;
    newItem.display = serverConnection.name?serverConnection.name: serverConnection.sharePointConnectionParametersDto.url;
    newItem.subdisplay ='Username: '+serverConnection.sharePointConnectionParametersDto.username;
    newItem.value = serverConnection;
    return newItem;
  };

  $scope.createBoxConnectionParamOption= function(serverConnection:Operations.DtoBoxConnectionDetails) {
    var newItem = new ParamOption<Operations.DtoBoxConnectionDetails>();
    newItem.id = serverConnection.id;
    newItem.display = serverConnection.name?serverConnection.name: serverConnection.url;
    newItem.subdisplay ='Username: '+serverConnection.username;
    newItem.value = serverConnection;
    return newItem;
  };

  $scope.createExchange365ConnectionParamOption= function(serverConnection:Operations.DtoExchange365ConnectionDetails) {
    var newItem = new ParamOption<Operations.DtoExchange365ConnectionDetails>();
    newItem.id = serverConnection.id;
    newItem.display = serverConnection.name?serverConnection.name: serverConnection.url;
    newItem.subdisplay ='Username: '+serverConnection.username;
    newItem.value = serverConnection;
    return newItem;
  };

  var scheduleConfigGetSummaryText = function(scheduleConfigDto: Operations.DtoScheduleConfig) {
    var scheduleTypePart = scheduleConfigDto?$filter('translate')(scheduleConfigDto.scheduleType):'';
    if (scheduleConfigDto && scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.CUSTOM) {
      return scheduleTypePart;
    }
    var scheduleTimePart = scheduleConfigDto?', every '+scheduleConfigDto.every+' '+(scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.DAILY?
        'day':scheduleConfigDto.scheduleType==Operations.EScheduleConfigType.HOURLY?'hour':'week')+' at '+(scheduleConfigDto.hour?
        $filter('numberFixedLen')(scheduleConfigDto.hour,2):'xx')+':'+ $filter('numberFixedLen')(scheduleConfigDto.minutes,2):'';
    var scheduleDaysOfWeekPart = scheduleConfigDto&&scheduleConfigDto.daysOfTheWeek?' '+scheduleConfigDto.daysOfTheWeek.map(d=>$filter('translate')(d)).join():'';
    return scheduleTypePart+scheduleTimePart+scheduleDaysOfWeekPart;
  };

  var originalScheduleGroupId ;

  var initShareFolderConnection = function(connectionId:number) {
    sharePointConnectionResource.getConnection(connectionId,function(connection:Operations.DtoSharePointConnectionDetails){
      $scope.mediaConnectionSavedSelectedItems  =[createSharePointNewSelectedItem(connection)];
    },onError);

  };

  var initOneDriveConnection = function(connectionId:number) {
    oneDriveConnectionResource.getConnection(connectionId,function(connection:Operations.DtoOneDriveConnectionDetails){
      $scope.mediaConnectionSavedSelectedItems  =[createSharePointNewSelectedItem(connection)];
    },onError);


  };

  var initBoxConnection = function(connectionId:number) {
    boxConnectionResource.getConnection(connectionId,function(connection:Operations.DtoBoxConnectionDetails ){
      $scope.mediaConnectionSavedSelectedItems  =[createBoxNewSelectedItem(connection)];
    },onError);
  };

  var initExchange365Connection = function(connectionId:number) {
    exchange365ConnectionResource.getConnection(connectionId,function(connection:Operations.DtoExchange365ConnectionDetails ){
      $scope.mediaConnectionSavedSelectedItems  =[createExchange365NewSelectedItem(connection)];
    },onError);
  };

  var initScheduleGroup = function(scheduleGroup:Operations.DtoScheduleGroup) {
    originalScheduleGroupId = scheduleGroup?scheduleGroup.id:null;
    $scope.scheduleGroupSavedSelectedItems  =scheduleGroup?[createScheduleGroupNewSelectedItem(scheduleGroup)]:null;
  };

  var initScheduleGroupByScheduleGroupId = function(scheduleGroupId) {
    if(scheduleGroupId) {
      scheduleGroupResource.getScheduleGroup(scheduleGroupId, function (scheduleGroup:Operations.DtoScheduleGroup) {
        if (scheduleGroup && scheduleGroup.id) {
          $scope.scheduleGroupSavedSelectedItems = [createScheduleGroupNewSelectedItem(scheduleGroup)];
        }
        else {
          scheduleGroupResource.getScheduleGroups( function (schedules:Operations.DtoScheduleGroup[]) {
              if (schedules&&schedules.length>0) {
                $scope.scheduleGroupSavedSelectedItems = [createScheduleGroupNewSelectedItem(schedules[0])];
              }
            }
            , function () {});
        }
      }, function () {});
    }
    else {
      scheduleGroupResource.getScheduleGroups( function (schedules:Operations.DtoScheduleGroup[]) {
            if (schedules&&schedules.length>0) {
              $scope.scheduleGroupSavedSelectedItems = [createScheduleGroupNewSelectedItem(schedules[0])];
            }
          }
          , function () {});
    }
  };

  var initFileTypes = function() {
    $scope.supportedMapFileTypes.forEach(f => (<any>f).selected = false);
    (<Operations.DtoRootFolder>$scope.rootFolderItem).fileTypes.forEach(t => $scope.supportedMapFileTypes.filter(f => f.name == t)[0].selected = true);
    $scope.supportedIngestFileTypes.forEach(f => (<any>f).selected = false);
    (<Operations.DtoRootFolder>$scope.rootFolderItem).ingestFileTypes.forEach(t => $scope.supportedIngestFileTypes.filter(f => f.name == t)[0].selected = true);

  }
  var initMediaConnectionDetails = function() {
    if ((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaType == Operations.EMediaType.SHARE_POINT) {
      initShareFolderConnection((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId);
    }
    else if ((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaType == Operations.EMediaType.ONE_DRIVE) {
      initOneDriveConnection((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId);
    }
    else if ((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaType == Operations.EMediaType.BOX) {
      initBoxConnection((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId);
    }
    else if ((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaType == Operations.EMediaType.EXCHANGE365) {
      initExchange365Connection((<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId);
    }
  };

  var rootFolderId;
  $scope.departmentList = [];

  var initScanLimits= function() {
    $scope.configuredScannedFilesCountCap = (<Operations.DtoRootFolder>$scope.rootFolderItem).scannedFilesCountCap;
    $scope.configuredScannedFilesCountCap =
      (!$scope.configuredScannedFilesCountCap || $scope.configuredScannedFilesCountCap <= 0) ?
        "" :
        $scope.configuredScannedFilesCountCap.toString();
    $scope.configuredScannedMeaningfulFilesCountCap = (<Operations.DtoRootFolder>$scope.rootFolderItem).scannedMeaningfulFilesCountCap;
    if ($scope.configuredScannedMeaningfulFilesCountCap == -1) {
      $scope.configuredScannedMeaningfulFilesCountCap = null;
    }


    (!$scope.configuredScannedMeaningfulFilesCountCap || $scope.configuredScannedMeaningfulFilesCountCap<=0)?
      "" :
      $scope.configuredScannedMeaningfulFilesCountCap.toString();
  }
  var initStartDate = function(rootfolder:Operations.DtoRootFolder) {
    if (rootfolder.fromDateScanFilter) {
      $scope.limitScanDateStartTime = new Date(rootfolder.fromDateScanFilter);
      $scope.pickerDate = $scope.limitScanDateStartTime;
      $scope.limitScanDateStartTimeStr = $filter('date')($scope.limitScanDateStartTime, configuration.dateFormat);
    }
  };

  var init=function(openWithFieldName) {
    $scope.isMngDepartmentConfigUser = data.isMngDepartmentConfigUser;
    $scope.pickerDate = null;
    $scope.limitScanDateValid = true;
    $scope.pickerOptions = {
      maxDate: new Date(),
      showWeeks: false
    };

    $scope.rootFolderItem = (<Operations.DtoRootFolderSummaryInfo>data.rootFolderSummaryInfo)?(<Operations.DtoRootFolderSummaryInfo>data.rootFolderSummaryInfo).rootFolderDto:null;

    if( $scope.rootFolderItem) {
      $scope.editMode=true;

      var crawlRunDetailsDto = (<Operations.DtoRootFolderSummaryInfo>data.rootFolderSummaryInfo).crawlRunDetailsDto;
      var rootfolderNotScannedYet = !crawlRunDetailsDto || !crawlRunDetailsDto.runOutcomeState;
      var rootfolderScannedWithNoFilesFound = crawlRunDetailsDto && crawlRunDetailsDto.totalProcessedFiles == 0 && $scope.rootFolderItem.numberOfFoldersFound == 0 &&
         (crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FINISHED_SUCCESSFULLY ||
          crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FINISHED_WITH_WARNINGS ||
          crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.STOPPED ||
          crawlRunDetailsDto.runOutcomeState == Operations.ERunStatus.FAILED);

      $scope.isScanned = !(rootfolderNotScannedYet || rootfolderScannedWithNoFilesFound);
      $scope.title='Edit root folder';
      $scope.configuredExcludedFoldersCount = (<Operations.DtoRootFolder>$scope.rootFolderItem).folderExcludeRulesCount;

      rootFolderId =  (<Operations.DtoRootFolder >$scope.rootFolderItem).id;
      initScheduleGroup((data.rootFolderSummaryInfo).scheduleGroupDtos&&(data.rootFolderSummaryInfo).scheduleGroupDtos[0]? (<Operations.DtoRootFolderSummaryInfo>data.rootFolderSummaryInfo).scheduleGroupDtos[0]:null);
      initMediaConnectionDetails();
      initFileTypes();

      if($scope.configuredExcludedFoldersCount>0) {
        initExcludedFoldersList();
      }
      $scope.updateExcludedFolderSelectedItem=data.updateExcludedFolderSelectedItem;
      $scope.onExcludedFolderDeleted=data.deleteExcludedFolder;
      initScanLimits();
      initStartDate(<Operations.DtoRootFolder >$scope.rootFolderItem);


    }
    else {
      $scope.title='Add root folder to scan ';

      var rootFolderItem: Operations.DtoRootFolder = null;
      var rootFolderItemStr = localStorage.getUserData('rootFolders', 'addNew', () => {}, () => {});
      if (rootFolderItemStr!= null  && rootFolderItemStr!= '') { //init rootfolder after 'save and create anothr'
        rootFolderItem = JSON.parse(rootFolderItemStr);
        rootFolderItem.realPath = '';
        rootFolderItem.nickName = '';
        rootFolderItem.mailboxGroup = '';
        $scope.rootFolderItem = rootFolderItem;
        (<Operations.DtoRootFolder >$scope.rootFolderItem).id = null;
        initFileTypes();
        initMediaConnectionDetails();

        initScanLimits();
        initStartDate(<Operations.DtoRootFolder >$scope.rootFolderItem);
        localStorage.deleteUserData('rootFolders', 'addNew',function(){}, function(){});
      }
      else {
        rootFolderItem = new Operations.DtoRootFolder();
        rootFolderItem.rescanActive=true;
        rootFolderItem.extractBizLists=true;
        rootFolderItem.storeLocation=Operations.DtoRootFolder.storeLocation_LOCAL_DATACENTER;
        rootFolderItem.storePurpose=Operations.DtoRootFolder.storePurpose_UNDEFINED;
        rootFolderItem.mediaType=Operations.EMediaType.FILE_SHARE;
        rootFolderItem.storeSecurity=Operations.DtoRootFolder.storeSecurity_CLEAR;
        $scope.rootFolderItem = rootFolderItem;
        $scope.configuredExcludedFoldersCount=0;
        $scope.configuredScannedFilesCountCap="";
        $scope.configuredScannedMeaningfulFilesCountCap="";
      }

      userSettings.getLastSelectRootFolderScheduleGroup(function(scheduleId) { initScheduleGroupByScheduleGroupId(scheduleId); });
    }

    getDepartments(() => { initDepartmentById((<Operations.DtoRootFolder>$scope.rootFolderItem).departmentId) });

    $scope.setActiveTab(openWithFieldName?openWithFieldName: 'general');

    if($scope.isExchange365SupportEnabled()) {
      $scope.mediaTypeOptions.push(Operations.EMediaType.EXCHANGE365);
    }
  };

  $scope.isExchange365SupportEnabled = function() {
    return configuration['exchange-365.support-enabled'] === 'true';
  };

  $scope.setActiveTab=function(tabName:string) {
     $scope.activeTab=tabName;
  };

  var initExcludedFoldersList = function() {
    rootFoldersResource.getExcludedFolderRules($scope.rootFolderItem.id,1200,
      function (excludedFolders:Operations.DtoFolderExcludeRule[],totalElements:number) {
      onCompleted(excludedFolders,totalElements);
      }, onError);

    function onCompleted(excludedFolders:Operations.DtoFolderExcludeRule[], totalElements:number) {
      if( excludedFolders&&excludedFolders[0]) {
        var parsedExcludedFolders = excludedFolders.map(excluded => {
          var paramActiveItem = createExcludedFolderNewSelectedItem(excluded);
          return paramActiveItem;
        });
        $scope.excludedFoldersSavedSelectedItems =parsedExcludedFolders;
      }
    }
  };

  var getDepartments = function(callback?) {
    departmentResource.getDepartments(function (departments:Operations.DtoDepartment[]) {
        if (departments) {
          $scope.departmentList = departments;
          if (callback) {
            callback();
          }
        }
      }
      , onError);
  };

  var initDepartmentById = function(departmentId) {
    var department = $scope.departmentList.filter(dep => dep.id == departmentId)[0];
    $scope.departmentSavedSelectedItems = [];
    if ($scope.editMode || department) {
      $scope.departmentSavedSelectedItems = [createDepartmentNewSelectedItem(department)];
    }
  };

  init(data.openWithFieldName);

  $scope.createNewExcludedFolderSelectedItemFromText = function(text) {
    var newRule = new Operations.DtoFolderExcludeRule();
    newRule.matchString = text;
    newRule.disabled = false;
    newRule.rootFolderId = rootFolderId;
    newRule.operator = EFilterOperators.equals.toString();
    var newItem = createExcludedFolderNewSelectedItem(newRule);
    return newItem;
  };

  var createExcludedFolderParamOption= function(newRule:Operations.DtoFolderExcludeRule) {
    var newItem = new ParamOption();
    newItem.id = newRule.matchString;
    newItem.value = newRule;
    return newItem;
  };

  var createExcludedFolderNewSelectedItem= function(newRule:Operations.DtoFolderExcludeRule) {
    var newItem = createExcludedFolderParamOption(newRule);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = !newRule.disabled;
    paramActiveItem.item = newItem;
    return paramActiveItem;

  };

  $scope.mediaTypeSelectedChanged = function(mediaType) {
    $scope.rootFolderItem.realPath = $scope.isExchange365Selected()||$scope.isOneDriveSelected() ? '-1' : null;
    $scope.rootFolderItem.mailboxGroup = $scope.isExchange365Selected()||$scope.isOneDriveSelected() ? null : '-1';
    $scope.mediaConnectionSavedSelectedItems = null;
    (<Operations.DtoRootFolder >$scope.rootFolderItem).mediaConnectionDetailsId = null;
  };

  $scope.loadScheduleGroupOptionItems = function(itemsOptions:any[]) {
    scheduleGroupResource.getScheduleGroups( function (schedules:Operations.DtoScheduleGroup[]) {
        if (schedules) {
          schedules.forEach(schedule=> {
            var newItem = $scope.createScheduleParamOption(schedule);
            itemsOptions.push(newItem);
          });
        }
      }
      , onError);
  };

  $scope.depParentMapping = [];

  var setDepartmentOptionItems = function(itemsOptions:any[]) {
    $scope.depParentMapping = [];
    var newItem = $scope.createDepartmentParamOption(null);
    itemsOptions.push(newItem);
    $scope.departmentList.forEach(department => {
      if (department.parentId == null) {
        $scope.depParentMapping[department.id] = department.name;
      }
      var newItem = $scope.createDepartmentParamOption(department);
      itemsOptions.push(newItem);
    });
  };

  $scope.loadDepartmentOptionItems = function(itemsOptions:any[]) {
    $scope.departmentList = [];
    getDepartments(() => { setDepartmentOptionItems(itemsOptions) });
  };

  $scope.loadOneDriveConnectionOptionItems = function(itemsOptions:any[]) {
        oneDriveConnectionResource.getConnections( function (connections:Operations.DtoOneDriveConnectionDetails[]) {
            if (connections) {
              connections.forEach(connection=> {
                var newItem = $scope.createMicrosoftConnectionParamOption(connection);
                itemsOptions.push(newItem);
              })
            }
          }
          , onError)
  };

  $scope.loadSharePointConnectionOptionItems = function(itemsOptions:any[]) {
    sharePointConnectionResource.getConnections( function (connections:Operations.DtoSharePointConnectionDetails[]) {
          if (connections) {
            connections.forEach(connection => {
              var newItem = $scope.createMicrosoftConnectionParamOption(connection);
              itemsOptions.push(newItem);
            });
          }
        }, onError);
  };

  $scope.loadBoxConnectionOptionItems = function(itemsOptions:any[]) {
    boxConnectionResource.getConnections( function (connections:Operations.DtoBoxConnectionDetails[]) {
        if (connections) {
          connections.forEach(connection=> {
            var newItem = $scope.createBoxConnectionParamOption(connection);
            itemsOptions.push(newItem);
          });
        }
      }, onError);
  };

  $scope.loadExchange365ConnectionOptionItems = function(itemsOptions:any[]) {
    exchange365ConnectionResource.getConnections( function (connections:Operations.DtoExchange365ConnectionDetails[]) {
        if (connections) {
          connections.forEach(connection=> {
            var newItem = $scope.createExchange365ConnectionParamOption(connection);
            itemsOptions.push(newItem);
          });
        }
      }, onError);
  };

  $scope.openNewScheduleGroupDialog = function(successFunction,cancelFunction) {
    var dlg = dialogs.create(
        'common/directives/operations/dialogs/newRootFolderScheduleDialog.tpl.html',
        'newRootFolderScheduleItemDialogCtrl',
        {},
        {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (schedule:Operations.DtoScheduleGroup) {
      successFunction(schedule);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.openNewDepartmentDialog = function(successFunction, cancelFunction) {
    var dlg = dialogs.create(
      'common/directives/operations/dialogs/newDepartmentDialog.tpl.html',
      'newDepartmentDialogCtrl',
      {componentName: currentUserDetailsProvider.isInstallationModeLegal() ? 'matter' : 'department'},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (department:Operations.DtoDepartment) {
      successFunction(department);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.openNewSharePointConnectionDialog = function(successFunction,cancelFunction) {
    var dlg = dialogs.create(
      'common/directives/operations/dialogs/newMicrosoftConnectionDialog.tpl.html',
      'newMicrosoftConnectionItemDialogCtrl',
      {mediaType:Operations.EMediaType.SHARE_POINT},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (connection:Operations.DtoSharePointConnectionDetails) {
      successFunction(connection);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.openNewOneDriveConnectionDialog = function(successFunction,cancelFunction) {
    var dlg = dialogs.create(
      'common/directives/operations/dialogs/newMicrosoftConnectionDialog.tpl.html',
      'newMicrosoftConnectionItemDialogCtrl',
      {mediaType:Operations.EMediaType.ONE_DRIVE},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (connection:Operations.DtoOneDriveConnectionDetails) {
      successFunction(connection);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.openNewBoxConnectionDialog = function(successFunction,cancelFunction) {
    var dlg = dialogs.create(
      'common/directives/operations/dialogs/newBoxConnectionDialog.tpl.html',
      'newBoxConnectionItemDialogCtrl',
      {mediaType:Operations.EMediaType.BOX},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (connection:Operations.DtoBoxConnectionDetails) {
      successFunction(connection);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.openNewExchange365ConnectionDialog = function(successFunction,cancelFunction) {
    var dlg = dialogs.create(
      'common/directives/operations/dialogs/newExchange365ConnectionDialog.tpl.html',
      'newExchange365ConnectionItemDialogCtrl',
      {mediaType:Operations.EMediaType.EXCHANGE365},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (connection:Operations.DtoExchange365ConnectionDetails) {
      successFunction(connection);
    }, function () {
      cancelFunction?cancelFunction():null;
    });
  };

  $scope.onExcludedItemsSelected =function(excludedItems) {
    var excludedFoldersDto = convertToDtoExcludedFolder(excludedItems);
    $scope.configuredExcludedFolders=excludedFoldersDto;
  };

  $scope.onScheduleGroupItemSelected=function(scheduleGroup:ParamActiveItem<Operations.DtoScheduleGroup>) {
    $scope.scheduleGroupSelected  =scheduleGroup.item.value;
    $scope.scheduleGroupSelectedSubdisplay  =scheduleGroup.item.subdisplay;
  };

  $scope.onMediaConnectionSelected=function(connection:ParamActiveItem<Operations.DtoMediaTypeConnectionDetails>) {
    $scope.connectionSelected = connection.item.value;
    (<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId  =connection.item.value.id;
    $scope.connectionSelectedSubdisplay  =connection.item.subdisplay;
   // (<Operations.DtoRootFolder>$scope.rootFolderItem).path = null;
  };

  $scope.onDepartmentSelected=function(department:ParamActiveItem<Operations.DtoDepartment>) {
    $scope.departmentSelected = department.item.value;
    $scope.departmentSelectedSubdisplay = department.item.subdisplay;
  };

  $scope.fullPathChanged = function() {
    extractNickName();
  };

  var extractNickName = function() {
    let theRealPath: string = $scope.rootFolderItem.realPath;

    if (theRealPath.indexOf('\\') > -1) {
      (<Operations.DtoRootFolder>$scope.rootFolderItem).nickName = theRealPath.substring(theRealPath.lastIndexOf('\\') + 1);
    }
    else if (theRealPath.indexOf('\/') > -1) {
      (<Operations.DtoRootFolder>$scope.rootFolderItem).nickName = theRealPath.substring(theRealPath.lastIndexOf('\/') + 1);
    }
  };

  $scope.isExchange365Selected=function(){
    return $scope.rootFolderItem.mediaType === Operations.EMediaType.EXCHANGE365;
  };
  $scope.isOneDriveSelected=function(){
       return $scope.rootFolderItem.mediaType === Operations.EMediaType.ONE_DRIVE;
  };

  $scope.isMediaConnectionNeeded=function(){
     return $scope.rootFolderItem.mediaType != Operations.EMediaType.FILE_SHARE;
  };

  $scope.openBrowseServerFoldersDialog = function() {
    var dlg = dialogs.create(
        'common/directives/dialogs/folderBrowserDialog.tpl.html',
        'folderBrowserDialogCtrl',
        {mediaConnectionId:   (<Operations.DtoRootFolder>$scope.rootFolderItem).mediaConnectionDetailsId,
          dataCenterId:$scope.rootFolderItem.customerDataCenterDto?$scope.rootFolderItem.customerDataCenterDto.id:null, editRootFolderMode:true,title:"Select folder",
          selectedFolder:$scope.rootFolderItem.realPath,mediaType:$scope.rootFolderItem.mediaType,selectionWithNameResponse:true},
        {size: 'lg',windowClass: 'offsetDialog1'});

    dlg.result.then(function (data) {
      (<Operations.DtoRootFolder>$scope.rootFolderItem).realPath = data.path;
      if (data.name) {
        (<Operations.DtoRootFolder>$scope.rootFolderItem).nickName = data.name;
      }
      else {
        extractNickName();
      }
    }, function () {});
  };

  $scope.fileTypeChanged = function(isMapChange:boolean,name:string, isSelected:boolean){
    if(isMapChange) {
      if(isSelected == false) {
        $scope.supportedIngestFileTypes.filter(i => i.name == name)[0].selected = false;
      }
    }
    else{  //ingest
      if(isSelected) {
        $scope.supportedMapFileTypes.filter(i => i.name == name)[0].selected = true;
      }
    }
  }
  $scope.cancel = function() {
    $uibModalInstance.dismiss('Canceled');
  };

  $scope.save = function(createAnother:boolean) {
    $scope.submitted=true;
    if($scope.generalForm.$valid && $scope.advancedForm.$valid && $scope.scheduleGroupSelected && $scope.limitScanDateValid && $scope.rootFolderItem.customerDataCenterDto && $scope.rootFolderItem.customerDataCenterDto.id) {
      if($scope.isExchange365Selected() || $scope.isOneDriveSelected()) {
        (<Operations.DtoRootFolder>$scope.rootFolderItem).realPath = null;
        (<Operations.DtoRootFolder>$scope.rootFolderItem).mailboxGroup =  (<Operations.DtoRootFolder>$scope.rootFolderItem).mailboxGroup;
      }
      else {
        (<Operations.DtoRootFolder>$scope.rootFolderItem).realPath = (<Operations.DtoRootFolder>$scope.rootFolderItem).realPath;
        (<Operations.DtoRootFolder>$scope.rootFolderItem).mailboxGroup = null;
      }

      (<Operations.DtoRootFolder>$scope.rootFolderItem).fileTypes =   $scope.supportedMapFileTypes.filter(f=> f.selected).map(d=>d.name);
      (<Operations.DtoRootFolder>$scope.rootFolderItem).ingestFileTypes =   $scope.supportedIngestFileTypes.filter(f=> f.selected).map(d=>d.name);
      (<Operations.DtoRootFolder>$scope.rootFolderItem).scannedFilesCountCap =
          ($scope.configuredScannedFilesCountCap && parseInt($scope.configuredScannedFilesCountCap)>0)?parseInt($scope.configuredScannedFilesCountCap):-1;
      (<Operations.DtoRootFolder>$scope.rootFolderItem).scannedMeaningfulFilesCountCap =
          ($scope.configuredScannedMeaningfulFilesCountCap && parseInt($scope.configuredScannedMeaningfulFilesCountCap)>0)?parseInt($scope.configuredScannedMeaningfulFilesCountCap):-1;

      if($scope.isExchange365SupportEnabled()) {
        (<Operations.DtoRootFolder>$scope.rootFolderItem).fromDateScanFilter = $scope.limitScanDateStartTime ? $scope.limitScanDateStartTime.getTime() : null;
      }

      var result:any  = {createAnother : createAnother, rootFolderItem:$scope.rootFolderItem,excludedFolders:$scope.configuredExcludedFolders };
      if( (<Operations.DtoScheduleGroup>$scope.scheduleGroupSelected).id!=originalScheduleGroupId) { //save schedule only if changed
        result['scheduleGroup']= $scope.scheduleGroupSelected;
        userSettings.setLastSelectRootFolderScheduleGroup((<Operations.DtoScheduleGroup>$scope.scheduleGroupSelected).id);
      }

      (<Operations.DtoRootFolder>$scope.rootFolderItem).departmentId = null;
      (<Operations.DtoRootFolder>$scope.rootFolderItem).departmentName = null;

      if ($scope.departmentSelected) {
        (<Operations.DtoRootFolder>$scope.rootFolderItem).departmentId = (<Operations.DtoDepartment>$scope.departmentSelected).id;
        (<Operations.DtoRootFolder>$scope.rootFolderItem).departmentName = (<Operations.DtoDepartment>$scope.departmentSelected).name;
      }

      //$scope.saveLastSelectedDataCenter();
      if (createAnother) {
        localStorage.setUserData('rootFolders', 'addNew', JSON.stringify($scope.rootFolderItem), () => {}, () => {});
      }

      $uibModalInstance.close(result);
    }
  };

  $scope.clearLimitScanDate = function() {
    $scope.limitScanDateStartTime = null;
    $scope.limitScanDateStartTimeStr = '';
    $scope.limitScanDateValid = true;
  };

  $scope.openLimitScanDateDialog = function() {
    $scope.showLimitScanDatePicker = true;
  };

  $scope.closeLimitScanDatePicker = function() {
    $scope.showLimitScanDatePicker = false;
  };

  $scope.limitScanDateChanged = function(pickerDate) {
    let today = new Date(Date.now());
    $scope.showLimitScanDatePicker = false;
    $scope.limitScanDateStartTime = pickerDate;
    $scope.limitScanDateStartTimeStr =  $filter('date')(pickerDate.getTime(), configuration.dateFormat);
    $scope.limitScanDateValid =  pickerDate <= today;
  };

  var convertToDtoExcludedFolder=function(configuredExcludedFolders:ParamActiveItem<Operations.DtoFolderExcludeRule>[]) {
    var result;
    if(configuredExcludedFolders)
    {
      result =  configuredExcludedFolders.map(x=> {
        (<Operations.DtoFolderExcludeRule>x.item.value).disabled = !x.active;
        return  (<Operations.DtoFolderExcludeRule>x.item.value);
      });
    }
    return result;
  };

  $scope.hitEnter = function(evt) {
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
});





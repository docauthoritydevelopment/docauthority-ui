///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('directives.operations',[
  'directives.dialogs.rootFolders.new',
  'ui.select',
  'directives.dialogs.rootFolders.schedule.new',
  'directives.dialogs.rootFolders.excludedFolders',
  'directives.dialogs.box.new',
  'directives.dialogs.microsoft.new',
  'directives.dialogs.mip.new',
  'directives.dialogs.exchange.new',
  'directives.dialogs.customer.dataCenter.new',
  'directives.dialogs.servercomponents',
  'directives.operations.dataCenter',
  'directives.dialogs.department.new',
])

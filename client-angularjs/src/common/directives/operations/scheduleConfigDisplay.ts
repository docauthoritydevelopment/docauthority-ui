///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.rootFolders.schedule.display', [

    ])
    .controller('scheduleConfigDisplayCtrl', function ($scope,$element,Logger:ILogger,$filter)
    {
      var log:ILog = Logger.getInstance('scheduleDisplayCtrl');

      $scope.scheduleTypeOptions=[
        Operations.EScheduleConfigType.WEEKLY,
        Operations.EScheduleConfigType.DAILY,
        Operations.EScheduleConfigType.HOURLY,
        Operations.EScheduleConfigType.CUSTOM
      ];

      $scope.dayNameOptions = [
        {  value:Operations.EDayOfTheWeek.SUN,   selected: false },
        {   value:Operations.EDayOfTheWeek.MON, selected: false },
        {  value:Operations.EDayOfTheWeek.TUE,   selected: false },
        {  value:Operations.EDayOfTheWeek.WED,selected: false },
        {  value:Operations.EDayOfTheWeek.THU,selected: false },
        {  value:Operations.EDayOfTheWeek.FRI,selected: false },
        {  value:Operations.EDayOfTheWeek.SAT,selected: false },
      ];

      $scope.every = {};

      // For hours dropdown (0 - 23)
      var timeHoursRange = [];
      for (var i = 0; i < 24; i++) {
        timeHoursRange.push(
         (i < 10) ? ('0' + i) : i + ''
        );
      }

      // For minutes dropdown (0 - 59)
      var timeMinutesRange=[];
      for (i = 0; i < 60; i++) {
        timeMinutesRange.push(
         (i < 10) ? ('0' + i) : i + ''
       );
      }
      $scope.startingTimeHoursRange = timeHoursRange;
      $scope.startingTimeHMinutesRange = timeMinutesRange;

      $scope.init=function()
      {

        if ($scope.scheduleConfig) {
          $scope.atHH =(<Operations.DtoScheduleConfig>$scope.scheduleConfig).hour!=null?$scope.startingTimeHoursRange.filter(h=>h==(<Operations.DtoScheduleConfig>$scope.scheduleConfig).hour)[0]:null;
          $scope.atMM =  (<Operations.DtoScheduleConfig>$scope.scheduleConfig).minutes!=null? $scope.startingTimeHMinutesRange.filter(m=>m==(<Operations.DtoScheduleConfig>$scope.scheduleConfig).minutes)[0]:null;
          $scope.every.value = (<Operations.DtoScheduleConfig>$scope.scheduleConfig).every;
          $scope.selectedScheduleType =  (<Operations.DtoScheduleConfig>$scope.scheduleConfig).scheduleType;
          $scope.customCronConfig = (<Operations.DtoScheduleConfig>$scope.scheduleConfig).customCronConfig;
          if ((<Operations.DtoScheduleConfig>$scope.scheduleConfig).scheduleType == Operations.EScheduleConfigType.WEEKLY &&  (<Operations.DtoScheduleConfig>$scope.scheduleConfig).daysOfTheWeek) {
            $scope.dayNameOptions.forEach(f=>(<any>f).selected=false);
            (<Operations.DtoScheduleConfig>$scope.scheduleConfig).daysOfTheWeek.forEach(d=> {
              $scope.dayNameOptions.filter(f=>f.value == d)[0].selected = true;
            });
          }

        }
        else {
          $scope.every.value =1;
          $scope.selectedScheduleType = Operations.EScheduleConfigType.DAILY;
        }
      }

      $scope.$watch(function () { return $scope.scheduleConfig; }, function (value) {
        $scope.init();
      });

      $scope.isWeekly = function(){
        return $scope.selectedScheduleType==Operations.EScheduleConfigType.WEEKLY;
      };
      $scope.isDaily = function(){
        return $scope.selectedScheduleType==Operations.EScheduleConfigType.DAILY;
      };
      $scope.isHourly = function(){
        return $scope.selectedScheduleType==Operations.EScheduleConfigType.HOURLY;
      };

      $scope.isValidEvery = function() {
        return  $scope.isWeekly() ||
            $scope.isCustom() ||
            $scope.every.value > 0;
      };
      $scope.isValidAtMM = function() {
        return  $scope.isCustom() ||
            ($scope.atMM != null && $scope.atMM > -1 && $scope.atMM <60);
      };
      $scope.isValidAtHH = function() {
        return  $scope.isHourly() || $scope.isCustom() ||
            (!$scope.isHourly() && $scope.atHH != null && $scope.atHH >-1 && $scope.atHH < 24);
      };
      $scope.isValidDayOfTheWeek = function() {
        return  (!$scope.isWeekly()) ||
            ($scope.isWeekly() &&  $scope.dayNameOptions.filter(f=> f.selected)[0]);
      };
      $scope.isCustom = function () {
        return $scope.selectedScheduleType==Operations.EScheduleConfigType.CUSTOM;
      };
      var noCronSpecialChars = function(cronExp) {
        var lower = cronExp.toLowerCase();
        return (lower.indexOf("l") < 0 &&
            lower.indexOf("w") < 0 &&
            lower.indexOf("#") < 0);
      };
      $scope.isValidCustom = function () {
        // Get cron expression validation check
        // this.cronExpressionText = this.scheduleGroupsService.customCronAsText(this.scheduleGroupItem.scheduleConfigDto.customCronConfig);
        const spacesRegex = /\s+/g; // remove spaces sequences

        return !$scope.isCustom() ||
            ($scope.customCronConfig != null && $scope.customCronConfig.length > 9 && noCronSpecialChars($scope.customCronConfig) &&
                ($scope.customCronConfig.trim()).split(spacesRegex, 10).length == 6);
      };

      $scope.isValid = function() {
        console.log("isValidEvery()="+$scope.isValidEvery()+ " isValidAtMM()="+$scope.isValidAtMM()+ " isValidAtHH()="+$scope.isValidAtHH()+ " isValidDayOfTheWeek()="+$scope.isValidDayOfTheWeek());
        console.log("atHH="+$scope.atHH+ " atMM="+$scope.atMM);
        return $scope.isCustom() ? $scope.isValidCustom() :
          ($scope.isValidEvery() && $scope.isValidAtMM() && $scope.isValidAtHH() && $scope.isValidDayOfTheWeek());
      }
      $scope.getUserScheduleData = function() {
        var schedule:Operations.DtoScheduleConfig;
        if ($scope.isWeekly()) {
          schedule = new Operations.DtoScheduleConfig();
          schedule.scheduleType=Operations.EScheduleConfigType.WEEKLY;
          schedule.daysOfTheWeek =   $scope.dayNameOptions.filter(f=> f.selected).map(d=>d.value);
        }
        else if ($scope.isDaily()) {
          schedule = new Operations.DtoScheduleConfig();
          schedule.scheduleType=Operations.EScheduleConfigType.DAILY;
        }
        else if ($scope.isHourly()) {
          schedule = new Operations.DtoScheduleConfig();
          schedule.scheduleType=Operations.EScheduleConfigType.HOURLY;
        }
        else if ($scope.isCustom()) {
          schedule = new Operations.DtoScheduleConfig();
          schedule.scheduleType = Operations.EScheduleConfigType.CUSTOM;
          schedule.customCronConfig = $scope.customCronConfig;
        }
        schedule.hour = $scope.isHourly()?null:$scope.atHH;
        schedule.minutes = $scope.atMM;
        schedule.every = $scope.every.value;

        if ($scope.isValid()) {
         return schedule;
        }
        return null;
      }

    })
   .directive('scheduleConfigDisplay',
        function () {
          return {
            templateUrl: '/common/directives/operations/scheduleConfigDisplay.tpl.html',
            replace:true,
            scope:{
              'scheduleConfig':'=',
              'getUserScheduleData':'=',
              'readOnlyMode':'=',
              'isValid':'=',
              'selectedScheduleType':'=',
            },
            controller: 'scheduleConfigDisplayCtrl',
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }
    );

///<reference path='../all.d.ts'/>
declare var $select;

'use strict';
angular.module('directives.params', ['ui.select'])
  .controller('inputParamCtrl', function ($window,$scope,$element,Logger:ILogger,$state,$stateParams,$timeout,propertiesUtils, configuration:IConfig,$filter,aclsResource:IAclsResource,  dialogs,saveFilesBuilder:ISaveFileBuilder) {
    var log:ILog = Logger.getInstance('inputParamCtrl');
    $scope.selectedItems=[];
    $scope.allSelectedItems=[];
    $scope.checkedSelectedItems=[];
    $scope.MAX_SELECTED_ITEMS = configuration.max_data_records_in_dialog_to_scroll;
    $scope.DISPLAYED_ITMES_WHEN_MORE_THEN_MAX = configuration.displayed_items_when_more_then_max;

    $scope.CREATE_NEW_OBJECT="CREATE_NEW_OBJECT";
    $scope.INPUT_EMPTY_OPTION="INPUT_EMPTY_OPTION";

    $scope.init = function() {
      loadItems();
    };

    var loadItems = function() {
      $scope.itemsOptions = [];
      if(!$scope.noCreateNewObjectButton) {
        var newOption = new ParamOption();
        newOption.id = $scope.CREATE_NEW_OBJECT;
        newOption.value = $scope.CREATE_NEW_OBJECT;
        newOption.display = $filter('translate')($scope.CREATE_NEW_OBJECT);
        $scope.itemsOptions.push(newOption);
      }
      if($scope.createEmptyItem && $scope.singleSelection) {
        var emptyOption = new ParamOption();
        emptyOption.id = $scope.INPUT_EMPTY_OPTION;
        emptyOption.value = $scope.INPUT_EMPTY_OPTION;
        emptyOption.display = $filter('translate')($scope.INPUT_EMPTY_OPTION);
        $scope.itemsOptions.push(emptyOption);
      }
      if(propertiesUtils.parseBoolean($scope.options)) {
        if(_.isFunction($scope.loadOptionItems)) {
          $scope.loadOptionItems( $scope.itemsOptions);
        }
      }
    };

    $scope.refreshList = function(items) {
      $scope.itemsOptions = items;
    };

    $scope.internalControl = $scope.control || {};

    $scope.internalControl.refreshList = function(items) {
      $scope.refreshList(items);
    };

    $scope.openBrowseServerFoldersDialog = function() {
      var dlg = dialogs.create(
          'common/directives/dialogs/folderBrowserDialog.tpl.html',
          'folderBrowserDialogCtrl',
          {rootFolder:$scope.rootFolder,mediaType:$scope.mediaType,mediaConnectionId:$scope.mediaConnectionId,dataCenterId:$scope.customerDataCenterId,editRootFolderMode:false,title:"Select excluded folder"},
          {size: 'lg',windowClass: 'offsetDialog1'});

      dlg.result.then(function (newRootFolderPath:string) {
        $scope.addNewByUserText(newRootFolderPath);
      }, function () {

      });
    };

    $scope.openAllValueItemsInDialog = function() {
      var dlg = dialogs.create(
          'common/directives/dialogs/pageableCheckableItemsDialog.tpl.html',
          'pageableCheckableItemsDialogCtrl',
          {
            loadData:loadData ,
            reloadData:$scope.reloadData,
            title: "Manage action script parameters",
            objectId:$scope.objectId,
            classType:$scope.classType,loadOptionItems:$scope.loadOptionItems,options:$scope.options,createNewSelectedItemFromText:$scope.createNewSelectedItemFromText,actionName:$scope.actionName
          },
          {size: 'md', windowClass: 'offsetDialog'});

      dlg.result.then(function (itemsToUpdate:any[]) {
        //   $scope.initialObjectValues=itemsToUpdate;
      }, function () {

      });

      function loadData(gridOptions,$element) {
        $scope.loadAllData(gridOptions,$element, $scope.savedSelectedItems);
      }

      $scope.loadAllData = function(gridOptions,$element,savedSelectedItems) {
        var data= savedSelectedItems;
        setDataToGrid( data );
        function setDataToGrid(values)
        {
          var grid = $element;
          if (grid[0]&&  grid.data("kendoGrid")) {

            gridOptions.setData(grid, values);

          }
          else {
            gridOptions.gridSettings.dataSource.data = values;
            gridOptions.gridSettings.dataSource.transport = null;
            gridOptions.gridSettings.autoBind = true;
          }
        }
      };
    };

    $scope.removeSelectedItem = function(item) {
      var index =$scope.allSelectedItems.indexOf(item);
      if(index>-1)
      {
        if($scope.onItemDeleted)
        {
          $scope.onItemDeleted($scope.allSelectedItems[index],function()
          {},function()
          {

          });
        }
        $scope.allSelectedItems.splice(index,1);

        updateSelectedItems();
      }
    };

    $scope.refreshUserText = function($select) {
      var text = $select.search;
      var FLAG = -1;
      if (text && $scope.createNewSelectedItemFromText) {
        var foundItem =  $scope.allSelectedItems ? $scope.allSelectedItems.filter(s=>s.item.id == FLAG)[0] : null;
        if (foundItem) {
          var foundIndex = $scope.allSelectedItems.indexOf(foundItem);
          $scope.allSelectedItems.splice(foundIndex, 1);
        }
        var foundInRecentItems = $select.items ? $select.items.filter(i=>i.id == FLAG)[0] : null;
        if (foundInRecentItems) { //if already exists remove it
          var foundIndex = $select.items.indexOf(foundInRecentItems);
          $select.items.splice(foundIndex, 1);
        }

        var newItem:ParamActiveItem<any> = $scope.createNewSelectedItemFromText(text);
        newItem.item.id = FLAG;
        $select.items.unshift(newItem.item);
        $scope.optionSelected(newItem.item, null, $select);
      }
    };

    $scope.addNewByUserText = function(text:string) {
      if(text && text.trim()!='') {
        var newItem:ParamActiveItem<any>  =  $scope.createNewSelectedItemFromText(text);
       var selectedItem:ParamActiveItem<any>=  addSelectedItem(newItem);
        if($scope.onNewItemAdded)
        {
           $scope.onNewItemAdded(selectedItem,function(){

           },function(){
             $scope.reloadData();
           });
        }
      }
    };

    var createSelectParamActiveItem = function(selectedItem, $select) {
      var newOption = new ParamActiveItem();
      newOption.active = true;
      newOption.item = selectedItem;
      $select.selected = newOption.item;
      return addSelectedItem(newOption);
    };

    $scope.optionSelected = function(selectedItem:ParamOption<any>, $model, $select) {
      if($scope.CREATE_NEW_OBJECT == selectedItem.id) {
         $scope.openNewObjectDialog(function(si) {
           var newOption = $scope.createNewOptionFromItem(si);
           $scope.optionSelected(newOption , $model,$select );
           loadItems();
           $scope.reloadData();
         },function() {
           return createSelectParamActiveItem($scope.selectedItem, $select);
         });
      }
      else if ($scope.INPUT_EMPTY_OPTION == selectedItem.id) {
           $scope.allSelectedItems=[];
           $scope.onItemsSelected(null);
      }
      else {
        return createSelectParamActiveItem(selectedItem, $select);
      }
      return null;
    };

    var addSelectedItem = function(selectedItem:ParamActiveItem<any>,isActive?:boolean) {
       var foundItem =selectedItem.item.id? $scope.allSelectedItems.filter(s=>s.item.id==selectedItem.item.id)[0]:$scope.allSelectedItems.filter(s=>s.item==selectedItem)[0];
        if(foundItem)
        {
          foundItem.active = true;
        }
        else {
          selectedItem.active=isActive!=null?isActive:true;
          if($scope.singleSelection)
          {
            $scope.allSelectedItems=[selectedItem];
          }
          else {
            $scope.allSelectedItems.push(selectedItem);
          }
        }
        if(!$scope.leaveTextAfterSelection) {
          $scope.selectedItem = null;
          $scope.textEntered = Date.now(); //just for calling a change
        }
      else {
          $scope.selectedItem = selectedItem.item;
       }

        updateSelectedItems();
        return selectedItem;
    };

    $scope.reloadData=function(selectedItem) {
      if(selectedItem) {

        addSelectedItem(selectedItem);
      }
      if( $scope.reloadSavedValueParameters) {
        $scope.reloadSavedValueParameters($scope.allSelectedItems);
      }
    };

    $scope.checkedChange = function(selectedItem) {
      if(!$scope.showCheckboxes) //label element triggers onClick event checkbox is not shown
      {
        return;
      }
      if($scope.onItemActiveStateChanged)
      {
        $scope.onItemActiveStateChanged(selectedItem,function()
        { },
        function(){});
      }
      updateSelectedItems();
    };

    var updateSelectedItems = function() {
      $scope.checkedSelectedItems=$scope.allSelectedItems.filter(i=>i.active).map(s=>s.item);
      $scope.onItemsSelected($scope.singleSelection?$scope.allSelectedItems[0]: $scope.allSelectedItems);
    };

    $scope.changeCheckAll = function(val:boolean) {
      $scope.allSelectedItems.forEach(s=>
      {
        (<any>s).active=val;
        if($scope.onItemActiveStateChanged)
        {
          $scope.onItemActiveStateChanged(s);
        }
      });
      updateSelectedItems();
    };

    $scope.removeAll = function() {
      $scope.allSelectedItems.forEach(t=> {
        if($scope.onItemDeleted)
        {
          $scope.onItemDeleted(t,function()
          {},function()
          {

          });
        }
      });
      $scope.allSelectedItems=[];
      updateSelectedItems();
    };

    $scope.removeAllUnchecked = function() {
      $scope.allSelectedItems.filter(i=>!i.active).forEach(t=>
      {
        if($scope.onItemDeleted)
        {
          $scope.onItemDeleted(t,function()
          {},function()
          {

          });
        }
      });
      $scope.allSelectedItems=$scope.allSelectedItems.filter(i=>i.active);
      updateSelectedItems();
    };

    $scope.$watch(function () { return $scope.savedSelectedItems; }, function (value) {
      if( $scope.savedSelectedItems&&$scope.savedSelectedItems[0]) {
        if($scope.savedSelectedItems.length>$scope.MAX_SELECTED_ITEMS)
        {
          $scope.savedItemExceededMax = true;
          $scope.totalItems = $scope.savedSelectedItems.length;
          for(var i=0; i<$scope.DISPLAYED_ITMES_WHEN_MORE_THEN_MAX; i++){
            addSelectedItem($scope.savedSelectedItems[i],$scope.savedSelectedItems[i].active);

          }
          if($scope.initiateViewAllDialogOpenState)
          {
            $scope.openAllValueItemsInDialog();
            $scope.initiateViewAllDialogOpenState=false;
          }
        }
        else {
          $scope.savedSelectedItems.forEach(s=> {   //Ensure each is not double selected
            addSelectedItem(s,s.active);
          });
        }
      }
    });

    $scope.getSelectedText = function(text) {
      if(text == $filter('translate')($scope.INPUT_EMPTY_OPTION)) {
        return '';
      }
      else if ($scope.placeholderAlwaysVisible) {
       if ($scope.placeholderText) {
         return $scope.placeholderText;
       }
       else {
         return 'Select ' + ($scope.createNewSelectedItemFromText ? 'or type':'') +  $scope.classType;
       }
      }
      return text;
    };

    $scope.exportToExcel = function() {
      var fields  = createGenericObjectValuesFields($scope,$scope.onCheckboxChanged);
      saveFilesBuilder.saveAsExcelOneSheet(fields, $scope.savedSelectedItems?$scope.savedSelectedItems:$scope.allSelectedItems,$scope.actionName+' '+$scope.classType+' values',$scope.actionName+' '+$scope.classType);
    };

    $scope.openUploadFromFileDialog = function() {
      var dlg = dialogs.create(
          'common/directives/dialogs/uploadFileDialog.tpl.html',
          'uploadFileDialogCtrl',
          {title:'Upload '+$scope.actionName+' '+$scope.classType+' values from csv file'},
          {size:'md'});

      dlg.result.then(function(data:any[] ){
        if(data && data.length>0) {
          var header = data[0].split(',');
          var isFullObject = _.startsWith(header[0],'Active') ;
          if(isFullObject) {
            data.splice(0, 1); //remove header row
          }

          var importedItems=[];
          data.forEach(d=>
          {
            var properties = d.split(',');
            if(properties[0]) {
              if (isFullObject) {
                var newItem:ParamActiveItem<any> = $scope.createNewSelectedItemFromText(properties[1]);
                newItem.active = properties[0];
                importedItems.push(newItem);
              }
              if (properties.length == 1) {
                var newItem:ParamActiveItem<any> = $scope.createNewSelectedItemFromText(properties[0]);
                importedItems.push(newItem);
              }
            }
          });
          $scope.savedSelectedItems = importedItems;
        }
      },function(){

      });
    };

    var onError = function() {
      dialogs.error('Error','Sorry, an error occurred.');
    };

    var createGenericObjectValuesFields = function(scope,onCheckboxChanged) {
      var paramActiveItem =  ParamActiveItem.Empty();
      var paramOption =  ParamOption.Empty();
      var prefixFieldName = propertiesUtils.propName(paramActiveItem, paramActiveItem.item) + '.';

      var display:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(paramOption, paramOption.display),
        title: 'Name',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,


      };
      var subdisplay:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+propertiesUtils.propName(paramOption, paramOption.subdisplay),
        title: 'Description',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,


      };
      var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: prefixFieldName+ propertiesUtils.propName(paramOption, paramOption.id),
        title: 'ID',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        //template:'<a ng-click="infoClicked(dataItem)">#: item #' +
        //'<span ng-show="dataItem.active" class="fa fa-checked"></span><span ng-show="dataItem.active" class="fa fa-square-o"></span>'+
        //' </a>',

      };
      var active:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName:  propertiesUtils.propName(paramActiveItem, paramActiveItem.active),
        title: 'Active',
        type: EFieldTypes.type_boolean,
        displayed: true,
        editable: true,
        nullable: true,
        template: '<input ng-checked="dataItem.active"  ng-show1="dataItem.active" type="checkbox" ng-click="infoClicked(dataItem)" />',
        //  template:'<a ng-click="infoClicked(dataItem)">'+
        //  '<span ng-show="dataItem.active" class="fa fa-checked"></span><span ng-show="dataItem.active" class="fa fa-square-o"></span>'+
        //  ' </a>',
        width:'25px'
      };

      scope.infoClicked = function(dataItem){
        if(dataItem)
        {
          dataItem.active=!dataItem.active;
          onCheckboxChanged(scope.restrictedEntityId,dataItem);

        }
      };

      var fields:ui.IFieldDisplayProperties[] =[];

      fields.push(active);
      fields.push(id);
      fields.push(display);
      fields.push(subdisplay);

      return fields;
    };
  })
  .directive('inputParam',function () {
    return {
      templateUrl: 'common/directives/inputParam.tpl.html',
      replace:true,
      scope:{
        'onItemsSelected':'=',
        'actionName':'@',
        'savedSelectedItems':'=',
        'removeAll':'=',
        'loadOptionItems':'=',
        'refreshList':'=',
        'reloadSavedValueParameters':'=',
        'createNewSelectedItemFromText':'=',
        'createNewOptionFromItem':'=',
        'importListFromEntityFilterDataFn':'=',
        'initiateViewAllDialogOpenState':'=',
        'importAutoEntity1':'=',
        'importAutoEntity2':'=',
        'mediaConnectionId':'=',
        'mediaType':'=',
        'customerDataCenterId':'=',
        'rootFolder':'=',
        'objectId':'=',
        'openNewObjectDialog':'=',
        'onNewItemAdded':'=',
        'onItemDeleted':'=',
        'onItemActiveStateChanged':'=',
        'options':'@',
        'classType':'@',
        'singleSelection':'=',
        'leaveTextAfterSelection':'=',
        'noCreateNewObjectButton':'=',
        'createEmptyItem':'=',
        'name':'@',
        'required':'=',
        'showCheckboxes':'=',
        'init':'=',
        'placeholderAlwaysVisible':'=',
        'placeholderText':'=',
        'control':'='
      },
      controller: 'inputParamCtrl',
      link: function (scope:any, element, attrs) {
        scope.init();
      }
    };
  });

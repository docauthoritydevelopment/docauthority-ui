///<reference path='../all.d.ts'/>

'use strict';
angular.module('directives.loader',[])
  .controller('loaderCtrl',function ($scope) {
    $scope.init = function(show) {
      $scope.show = show;
    };
  })
  .directive('loader',function () {
    return {
      templateUrl: 'common/directives/loader.tpl.html',
      replace:true,
      scope:{
        'show':'='
      },
      controller: 'loaderCtrl',
      link: function (scope:any, element, attrs) {
        scope.init(scope.show);
      }
    };
  });

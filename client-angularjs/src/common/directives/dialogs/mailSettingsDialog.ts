///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.mailSettings', [
  ])
  .controller('mailSettingsDialogCtrl',
    function ($window, $scope, $uibModalInstance, routerChangeService, data,dialogs,notificationSettingsResource:INotificationSettingsResource,healthNotificationResource:IHealthNotificationResource) {


      var psaudoPassword = '11111';

      var init=function() {
        $scope.loadLicenseStatus();
        $scope.setActiveTab('general');
      }

      $scope.setActiveTab=function(tabName:string)
      {
        $scope.activeTab=tabName;
      }
      $scope.loadLicenseStatus = function()
      {

        notificationSettingsResource.getMailNotificationSettings(
          function (settings:AppSettings.DtoMailNotificationConfiguration) {
            $scope.mailSettings = settings;
            $scope.connection = settings.smtpConfigurationDto;

            if ($scope.connection) {
              $scope.editMode = true;
              (<AppSettings.DtoSmtpConfiguration>$scope.connection).password = psaudoPassword;
            }

            $scope.initiated = true;
          },function() {
            $scope.initiated = true;
            onError()
          });
      }

      init();

      $scope.testConnection = function(){
        $scope.connectivityTest = null;
        $scope.failureMessage = null;
        if ($scope.smtpForm.$valid) {
          $scope.testConnectionInProgress = true;
          if( $scope.editMode)
          {
            const copyOfMailSettings =  <AppSettings.DtoMailNotificationConfiguration>(_.cloneDeep($scope.mailSettings));
            copyOfMailSettings.smtpConfigurationDto.password = copyOfMailSettings.smtpConfigurationDto.password == psaudoPassword ? null:copyOfMailSettings.smtpConfigurationDto.password;
            notificationSettingsResource.testMailConnectionWithSavedConnection(copyOfMailSettings,
              function (success: boolean, failureMessage) {
                $scope.testConnectionInProgress = false;
                $scope.connectivityTest = success;
                $scope.failureMessage = failureMessage;
              }
              , onTestConnectionError)
          }
          else {
            notificationSettingsResource.testMailConnection($scope.mailSettings, function (success:boolean,failureMessage) {
                $scope.connectivityTest = success;
                $scope.failureMessage = failureMessage;
              }
              , onTestConnectionError)
          }
        }
      };
      var onTestConnectionError  = () =>
      {
        $scope.testConnectionInProgress = false;
        dialogs.error('Error','Sorry, failed to test connection ');
      }



      // $scope.testConnection = function(){
      //   $scope.connectivityTest = null;
      //   $scope.failureMessage = null;
      //   if ($scope.smtpForm.$valid ) {
      //     $scope.mailSettings.smtpConfigurationDto =  $scope.connection;
      //     notificationSettingsResource.testConnection($scope.mailSettings, function (success:boolean,failureMessage) {
      //         $scope.connectivityTest = success;
      //         $scope.failureMessage = failureMessage;
      //       }
      //       , onError)
      //   }
      // };

      $scope.encryptionChanged = function(type)
      {
        if(type=='ssl' &&  ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useSsl )
        {
          ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useTls = false;
        }
        else if(type=='tls' &&   ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useTls )
        {
          ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useSsl = false;
        }
      }

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
        //routerChangeService.notifyDialogclose();
      }; // end cancel

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.generalForm.$valid && $scope.smtpForm.$valid) {

          $scope.mailSettings.smtpConfigurationDto = $scope.connection;
          notificationSettingsResource.updateMailNotificationSettings($scope.mailSettings, function (settings:AppSettings.DtoMailNotificationConfiguration) {
                $uibModalInstance.close();
                //routerChangeService.notifyDialogclose();
              }
              , function (error) {
                      onError();
               })
          }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };


      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    })



///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.folderBrowser', [
      'folders.server'
    ])
    .controller('folderBrowserDialogCtrl', function ($scope,  $uibModalInstance,data, userSettings:IUserSettingsService) {

      $scope.title=data.title;
      $scope.rootFolder = data.rootFolder;
      $scope.mediaType = data.mediaType;
      $scope.mediaConnectionId = data.mediaConnectionId;
      $scope.dataCenterId = data.dataCenterId;
      var selectionWithNameResponse = data.selectionWithNameResponse;
      $scope.lazyFirstLoad=false;//data.editRootFolderMode;


      //-- Methods --//

      $scope.init=function()
      {
        if( $scope.treeInitialized)
        {
          $scope.search(data.selectedFolder);
        }
      }

      $scope.$watch(function () { return $scope.treeInitialized; }, function (value) {
        if( $scope.treeInitialized) { //first init call - function findByPath is not yet hooked
          $scope.search(data.selectedFolder);

        }
      });

      $scope.cancel = function(){
        $uibModalInstance.dismiss($scope.selected);
      }; // end cancel

      $scope.search = function(findByText){
        $scope.selectedFolder = findByText;
        $scope.findByPath(findByText);
   //     $scope.rootFolder = initialDirectory;


      };



      $scope.onSelectFolder=function(path,name){
        $scope.selectedFolder = path;
        $scope.selectedFolderName = name;
      }


      $scope.save = function(){
        if(selectionWithNameResponse)
        {
          $uibModalInstance.close({path:$scope.selectedFolder,name: $scope.selectedFolderName&&$scope.selectedFolderName.trim()!=''?$scope.selectedFolderName.trim():null });
        }
        else {
          $uibModalInstance.close($scope.selectedFolder);
        }
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })

///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.checkableItems', [
      'directives.pageable.items.grid',
      'directives.pageable.items.menu'
    ])
    .controller('pageableCheckableItemsDialogCtrl', function ($window, $scope, dialogs, $uibModalInstance, data, policiesResource:IPoliciesResource) {

      $scope.title = data.title;
      $scope.getDataUrlBuilder = data.getDataUrlBuilder;
      $scope.reloadData = data.reloadData;
      $scope.loadData=!$scope.getDataUrlBuilder?data.loadData:null;
      $scope.objectId=data.objectId;
      $scope.classType=data.classType;
      $scope.actionName=data.actionName;
      $scope.createNewSelectedItemFromText=data.createNewSelectedItemFromText;
      $scope.loadOptionItems=data.loadOptionItems;
      $scope.options=data.options;
      //-- Methods --//

      $scope.onSelectedItems = function (data) {
        $scope.selectedItems = data;

      };
      $scope.onSelectedItem = function (id,name,data) {
        $scope.selectedItem = data;

      };
      $scope.onNewItemAdded = function (newItem) {
        if( $scope.objectId) {
          $scope.addNewObjectValueItem($scope.objectId, newItem);
        }
        else
        {
          $scope.addDirtyItemToGrid(newItem);
        }

      };

      $scope.addNewObjectValueItem = function (objectId,newItem:any) {
        //newDocType.name =  newDocType.name?( newDocType.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( newItem &&  newItem.id.trim() != '') {

          policiesResource.addNewObjectValue( objectId,newItem, function (item) {
            $scope.refreshGridData(newItem);
          }, function () {
            dialogs.error('Error','Failed to add new object value.');
          });
        }
      }

      $scope.editObjectValueItemActiveState = function (objectId,valueItem:any) {
        if(objectId) {
          if (valueItem && valueItem.item.id.trim() != '') {

            policiesResource.editObjectValueState(objectId, valueItem, function (item) {
              $scope.reloadData();
            }, function () {
              dialogs.error('Error', 'Failed to add new object value.');
            });
          }
        }
      }

      $scope.deleteObjectValueItem = function (objectId,valueItem:any) {
        if ( valueItem &&  valueItem.trim() != '') {

          policiesResource.deleteObjectValue( objectId,valueItem, function (item) {
            $scope.refreshGridData();
          }, function () {
            dialogs.error('Error','Failed to add new object value.');
          });
        }
      }

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        var itemsToUpdate=$scope.getDirtyItems();
        $uibModalInstance.close(itemsToUpdate);
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })

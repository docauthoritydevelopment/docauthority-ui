///<reference path='../../all.d.ts'/>
'use strict';
declare var _:any;
angular.module('directives.dialogs.uploadFile.serverUpload', [

  ])
  .controller('uploadFileDialogByServerCtrl', function ($scope, $compile, $uibModalInstance, data, dialogs, saveFilesBuilder:ISaveFileBuilder,configuration:IConfig) {

    $scope.fileUploaded=false;
    $scope.fileValidated=false;

    var uploadResourceFn:(fileData,updateDuplicates:boolean,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction)=> void;
    var validateUploadResourceFn:(fileData,maxErrorRowsReport:number,successFunction: (importResult:Operations.DTOImportSummaryResult) => void, errorFunction)=> void;
    var init=function(data)
    {
      $scope.createStubsOptionEnabled = data.createStubsOptionEnabled;
      if(data &&  data.uploadResourceFn) {
        uploadResourceFn = data.uploadResourceFn;
        validateUploadResourceFn = data.validateUploadResourceFn;
        $scope.itemsName = data.itemsName? data.itemsName:'items';
      }
      else
      {
       throw "uploadResourceFn is missing";
      }
      $scope.initiated=true;
    }

    init(data);
    $scope.reValidateUploadFileToServer = function() {

      var fd = new FormData();
      fd.append("file", $scope.theFile);
      if ($scope.createStubsOptionEnabled) {
        (<any>validateUploadResourceFn)(fd, 200, $scope.createStubs, function (importResult:Operations.DTOImportSummaryResult) {
          onValidationCompleted(importResult);
        }, function () {
          dialogs.error('Error', 'Failed to upload file');
        });
      }
      else {
        validateUploadResourceFn(fd, 200, function (importResult:Operations.DTOImportSummaryResult) {
          onValidationCompleted(importResult);
        }, function () {
          dialogs.error('Error', 'Failed to upload file');
        });

      }

    }
    $scope.validateUploadFileToServer = function() {
      $scope.fileValidated = false;
      $scope.importFailed = null;
      $scope.importResultDescription = null;
      $scope.reValidateUploadFileToServer();
    }

    var onValidationCompleted = function(importResult:Operations.DTOImportSummaryResult)
    {
      $scope.fileValidated = true;
      $scope.importFailed = importResult.importFailed;
      $scope.importResultDescription = importResult.resultDescription;
      if(! importResult.importFailed) {
        $scope.validationImportedRowCount = importResult.importedRowCount;
        $scope.validateErrors = importResult.errorImportRowResultDtos.map(e=> {
          return {'line': e.rowDto.lineNumber, 'msg': e.message}
        });
        $scope.validateErrorsCount = importResult.errorRowCount;
        $scope.validateWarningsCount = importResult.warningRowCount;
        $scope.validateDuplicateRowCount = importResult.duplicateRowCount;
        $scope.validateWarnings = importResult.warningImportRowResultDtos.map(e=> {
          return {'line': e.rowDto.lineNumber, 'msg': e.message}

        });
        $scope.validateDuplications = importResult.duplicateImportRowResultDtos.map(e=> {
          return {'line': e.rowDto.lineNumber, 'msg': e.message};
        })
      }
    }
    $scope.uploadFileToServer = function(){
      $scope.importErrorMsg = null;
      var fd = new FormData();
      //for( var key in $scope.theFile) {
      //  fd.append(key, $scope.theFile[key]);
      //}
      fd.append( "file", $scope.theFile);
      $scope.fileUploadedInProgress=true;
      if($scope.createStubsOptionEnabled) {
        (<any>uploadResourceFn)(fd, $scope.updateDuplicates, $scope.createStubs, function (importResult:Operations.DTOImportSummaryResult) {
          onImportCompleted(importResult);

        }, function () {
          $scope.fileUploadedInProgress=false;
          dialogs.error('Error', 'Failed to upload file');
        });
      }
      else {
        uploadResourceFn(fd, $scope.updateDuplicates, function (importResult:Operations.DTOImportSummaryResult) {
          onImportCompleted(importResult);

        }, function () {
          $scope.fileUploadedInProgress=false;
          dialogs.error('Error', 'Failed to upload file');
        });
      }
    }
    var lastImportResult:Operations.DTOImportSummaryResult=null;
    var onImportCompleted=function(importResult:Operations.DTOImportSummaryResult)
    {

      $scope.errors = importResult.errorImportRowResultDtos.map(e=> {
        return {'line': e.rowDto.lineNumber, 'msg': e.message}
      });
      $scope.warnings = importResult.warningImportRowResultDtos.map(e=> {
        return {'line': e.rowDto.lineNumber, 'msg': e.message}

      });
      $scope.duplications = importResult.duplicateImportRowResultDtos.map(e=> {
        return {'line': e.rowDto.lineNumber, 'msg': e.message};
      })
      $scope.fileUploadedInProgress=false;
      $scope.importedRowCount = importResult.importedRowCount ;
      $scope.duplicateRowCount = importResult.duplicateRowCount;
      $scope.errorsRowCount = importResult.errorRowCount;
      $scope.warningRowCount = importResult.warningRowCount;
      $scope.importId = importResult.importId;
      lastImportResult = importResult;
      $scope.fileUploaded=true;
      if($scope.importId) {
   //     loadGridHtml();
      }
      else {
        $scope.importErrorMsg = importResult.resultDescription;
      }
    }

    $scope.$watch(function () { return $scope.theFile; }, function (value) {
      $scope.importErrorMsg = null;
      $scope.fileValidated = false;
      if($scope.theFile)
      {
        $scope.fileData=null;
        $scope.fileError="";
        if(_.endsWith($scope.theFile.name.toLowerCase(),'.csv')) {
          $scope.uploadFileBrowser();
        }
        else
        {
          $scope.fileError="Only .csv file format is supported";
        }
      }
    });
    $scope.uploadFileBrowser = function(){
      var file =$scope.theFile;
      var fileReader = new FileReader();
      fileReader.onloadend = function(e){
        $scope.$apply(function () {
          let data;
          // ADDED CODE
          if (!e) {
             data = (<any>fileReader).content;
          }
          else {
             data =(<any> e.target).result;
          }
       //   $scope.fileRecords = (<any> e.target).result.split("\n");
          $scope.fileData =data;
          $scope.validateUploadFileToServer();
        });

      }
      //extend FileReader for IE 11
      if (!FileReader.prototype.readAsBinaryString) {
        FileReader.prototype.readAsBinaryString = function (fileData) {
          var binary = "";
          var pt = this;
          var reader = new FileReader();
          reader.onload = function (e) {
            var bytes = new Uint8Array(reader.result);
            var length = bytes.byteLength;
            for (var i = 0; i < length; i++) {
              binary += String.fromCharCode(bytes[i]);
            }
            //pt.result  - readonly so assign binary
            pt.content = binary;
            $(pt).trigger('onloadend');
          }
          reader.readAsArrayBuffer(fileData);
        }
      }
      fileReader.readAsBinaryString(file);
      //var start = parseInt(opt_startByte) || 0;
      //var stop = parseInt(opt_stopByte) || file.size - 1;
      //fileReader.onloadend = function(evt) {
      //  if (evt.target.readyState == FileReader.DONE) { // DONE == 2
      //    document.getElementById('byte_content').textContent = evt.target.result;
      //    document.getElementById('byte_range').textContent =
      //        ['Read bytes: ', start + 1, ' - ', stop + 1,
      //          ' of ', file.size, ' byte file'].join('');
      //  }
      //};

      //var blob = file.slice(start, stop + 1);
      //fileReader.readAsBinaryString(blob);

    }

    function addSheet(sheets:any[],rows,rowHeaders,sheetTitle) {
      if (rows.length > 0) {
        let dataRows = rows.map(e=> {
          var clone = e.rowDto.fieldsValues.slice(0);
          clone.unshift(e.message);
          return clone;
        });
        let fields = $.map(rowHeaders, function (f, i) {
          return {title: f, type: EFieldTypes.type_string, fieldName: i+1}; //index 0 will come after
        });
        fields.unshift({title: sheetTitle, type: EFieldTypes.type_string, fieldName: 0});
        var rowsForSheet = saveFilesBuilder.preparSheetRows(fields, dataRows);
        sheets.push(saveFilesBuilder.createSheet(fields, rowsForSheet,sheetTitle+' upload'));
      }
    }
    function addDataToSheet(rows:Operations.DTOImportRowResult[],rowHeaders,sheetTitle) {
      if (rows.length > 0) {
        let dataRows = rows.map(e=> {
          var clone = e.rowDto.fieldsValues.slice(0); // cheap way to duplicate an array.
          clone.unshift(e.message);  //Add e.message to the beginning of the values array
          clone.unshift((<any>e.rowDto.lineNumber));
          clone.unshift(sheetTitle);
          return clone;
        });
        let fields = $.map(rowHeaders, function (f, i) {
          return {title: f, type: EFieldTypes.type_string, fieldName: i+3}; //index 0 will come after
        });

        fields.unshift({title: "Msg Description", type: EFieldTypes.type_string, fieldName: 2});
        fields.unshift({title: 'Line number', type: EFieldTypes.type_number, fieldName: 1});
        fields.unshift({title: 'Msg Type', type: EFieldTypes.type_string, fieldName: 0});
      //  var rowsForSheet = saveFilesBuilder.preparSheetRows(fields, dataRows);
        return {fields:fields,dataRows:dataRows};
      }
      else {
        return {fields:null,dataRows:[]};
      }
    }
    function addToTxt(txtLines:any[],rows,lineTitle) {
      if (rows.length > 0) {
        rows.forEach(e=> {
            txtLines.push(lineTitle+'    line '+e.rowDto.lineNumber+' :'+e.message+'\n');
        });

      }
    }

    $scope.downloadValidationResultExcel = function()
    {
      var fd = new FormData();
      fd.append( "file", $scope.theFile);
      if ($scope.createStubsOptionEnabled) {
        (<any>validateUploadResourceFn)(fd, configuration.max_data_records_per_request,$scope.createStubs, function (importResult:Operations.DTOImportSummaryResult) {
          onDownloadValidationResultExcelCompleted(importResult);

        }, function () {
          dialogs.error('Error', 'Failed to export');
        });
      }
      else {
        (<any>validateUploadResourceFn)(fd, configuration.max_data_records_per_request, function (importResult:Operations.DTOImportSummaryResult) {
          onDownloadValidationResultExcelCompleted(importResult);

        }, function () {
          dialogs.error('Error', 'Failed to export');
        });
      }

    }
    var onDownloadValidationResultExcelCompleted = function(importResult:Operations.DTOImportSummaryResult)
    {

      var sheets = [];
      var errors = addDataToSheet( importResult.errorImportRowResultDtos, importResult.rowHeaders, "Error");
      var warnings = addDataToSheet( importResult.warningImportRowResultDtos, importResult.rowHeaders, "Warning");
      var dups = addDataToSheet( importResult.duplicateImportRowResultDtos, importResult.rowHeaders, "Duplicate");
      var fields = errors.fields?errors.fields:warnings.fields?warnings.fields:dups.fields;
      var rowsForSheet = saveFilesBuilder.preparSheetRows(fields, errors.dataRows.concat(warnings.dataRows).concat(dups.dataRows));
      sheets.push(saveFilesBuilder.createSheet(fields, rowsForSheet,'upload validation results'));
      saveFilesBuilder.saveAsExcelMultipleSheets(sheets, 'Validation log for ' + $scope.theFile.name);
    }
    $scope.downloadValidationResult = function()
    {
      var fd = new FormData();
      fd.append( "file", $scope.theFile);
      if ($scope.createStubsOptionEnabled) {
        (<any>validateUploadResourceFn)(fd, configuration.max_data_records_per_request,$scope.createStubs, function (importResult:Operations.DTOImportSummaryResult) {
          onDownloadValidationResultCompleted(importResult);
        }, function () {
          dialogs.error('Error', 'Failed to export');
        });
      }
      else {
        validateUploadResourceFn(fd, configuration.max_data_records_per_request, function (importResult:Operations.DTOImportSummaryResult) {
          onDownloadValidationResultCompleted(importResult);

        }, function () {
          dialogs.error('Error', 'Failed to export');
        });
      }

    }
    var onDownloadValidationResultCompleted = function(importResult:Operations.DTOImportSummaryResult)
    {
      var txtLines = [];
      addToTxt(txtLines, importResult.errorImportRowResultDtos, "Error.......");
      addToTxt(txtLines, importResult.warningImportRowResultDtos, "Warning.....");
      addToTxt(txtLines, importResult.duplicateImportRowResultDtos, "Duplicate...");

      saveFilesBuilder.saveAsTxt(txtLines, 'Validation remarks for ' + $scope.theFile.name);

    }

    $scope.downloadImportResult = function()
    {
      var txtLines = [];
      addToTxt(txtLines, lastImportResult.errorImportRowResultDtos,      "Error.......");
      addToTxt(txtLines, lastImportResult.warningImportRowResultDtos,    "Warning.....");
      addToTxt(txtLines, lastImportResult.duplicateImportRowResultDtos,  "Duplicate...");


      saveFilesBuilder.saveAsTxt(txtLines,'Import remarks for '+$scope.theFile.name);

    }
    $scope.downloadImportResultExcel = function()
    {
      var sheets=[];
      var errors = addDataToSheet(lastImportResult.errorImportRowResultDtos,lastImportResult.rowHeaders,"Error");
      var warnings = addDataToSheet(lastImportResult.warningImportRowResultDtos,lastImportResult.rowHeaders,"Warning");
      var dups = addDataToSheet(lastImportResult.duplicateImportRowResultDtos,lastImportResult.rowHeaders,"Duplicate");

      var fields = errors.fields?errors.fields:warnings.fields?warnings.fields:dups.fields;
      var rowsForSheet = saveFilesBuilder.preparSheetRows(fields, errors.dataRows.concat(warnings.dataRows).concat(dups.dataRows));
      sheets.push(saveFilesBuilder.createSheet(fields, rowsForSheet,'upload results'));
      saveFilesBuilder.saveAsExcelMultipleSheets(sheets,'Import log for '+$scope.theFile.name);

    }

    $scope.save = function(){
      $uibModalInstance.close($scope.fileUploaded);
    };

    $scope.cancel = function(){

      $uibModalInstance.close($scope.fileUploaded);
    }; // end cancel

    $scope.hitEnter = function(evt){
      if(angular.equals(evt.keyCode,13))
        $scope.save();
    };
  })


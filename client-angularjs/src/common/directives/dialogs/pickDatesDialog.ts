///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.datesPicker', [

    ])
    .controller('pickDatesDialogCtrl', function ($scope,  $filter,$uibModalInstance, data,configuration:IConfig) {

      $scope.format = configuration.inputDateFormat;
      $scope.altInputFormats = ['d!/M!/yy','d!-M!-yy'];
      var init=function(data)
      {
        if(data && data.startDate)
        {
          $scope.startDate =  data.startDate;
          $scope.endDate =  data.endDate;
          $scope.title='Pick dates';


        }
        else
        {
          $scope.title='Pick dates';
        }
        $scope.initiated=true;
      }

      init(data);


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel



      $scope.dateOptions  = {
        formatYear: 'yy',
      //  maxDate:new Date(),
     //   minDate: new Date(),
        startingDay: 1
      };

      // Disable weekend selection
      function disabled(data) {
        //var date = data.date,
        //    mode = data.mode;
        //return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
      }

      $scope.dateChanged=function()
      {
        $scope.startDate.setUTCHours(0,0,0,0);
        $scope.endDate?$scope.myForm.$setValidity('endDatehigher', $scope.endDate.getTime() >= $scope.startDate.getTime()):null;
        let today = new Date(Date.now());
        today.setUTCHours(0,0,0,0);
        $scope.myForm.$setValidity('startDatehigher', $scope.startDate <= today);
      }

      $scope.setDate = function(year, month, day) {
        $scope.startDate = new Date(year, month, day);
      };

      $scope.open1 = function() {
        $scope.startopened = true;
      };

      $scope.open2 = function() {
        $scope.endopened = true;
      };


      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var startTime = new Entities.DTODateRangePoint();
          if( $scope.startDate) {
            var startTimeAsUtc = new Date(Date.UTC($scope.startDate.getFullYear(), $scope.startDate.getMonth(), $scope.startDate.getDate(), 0, 0, 0));
            startTime.absoluteTime = startTimeAsUtc.getTime();
          }
          var endTime = new Entities.DTODateRangePoint();
          if( $scope.endDate) {
            var endTimeAsUtc = new Date(Date.UTC($scope.endDate.getFullYear(), $scope.endDate.getMonth(), $scope.endDate.getDate(), 0, 0, 0));
            endTime.absoluteTime = endTimeAsUtc.getTime();
          }

          $uibModalInstance.close({startTime: $scope.startDate?startTime:null,endTime:$scope.endDate?endTime:null});
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


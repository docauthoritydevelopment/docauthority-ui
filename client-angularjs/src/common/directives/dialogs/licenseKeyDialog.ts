
///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.license', [
      'directives.grid'
    ])
    .controller('licenseKeyDialogCtrl', function ($window,$scope, $compile, $uibModalInstance, data, dialogs, licenseResource:ILicenseResource,configuration:IConfig) {

      $scope.fileLoaded=false;

      $scope.licenseAlertMessage=data.licenseAlertMessage;

      $scope.$watch(function () { return $scope.theFile; }, function (value) {
         if($scope.theFile)
        {
          $scope.fileData=null;
          $scope.fileError="";
          $scope.uploadFileToServer();
        }
      });



      $scope.loadLicenseStatus = function()
      {

        licenseResource.getLicenseStatus(
            function (lic:License.DtoDocAuthorityLicense) {
              setData(lic);
            },
           onError);
      }
      $scope.dateFormat = configuration.dateFormat;
      var setData = function(lic:License.DtoDocAuthorityLicense)
      {
        $scope.lic=lic;
        $scope.applyDate = new Date(lic.applyDate);
        $scope.creationDate = new Date(lic.creationDate);
        $scope.registeredTo = lic.registeredTo;
        $scope.issuer = lic.issuer;

        $scope.serverId = $scope.serverId || lic.restrictions['serverId'];
        $scope.crawlerExpiry =new Date(parseInt( lic.restrictions['crawlerExpiry']));
        $scope.loginExpiry =new Date(parseInt( lic.restrictions['loginExpiry']));
        $scope.maxFiles =lic.restrictions['maxFiles'];
        $scope.maxRootFolders =lic.restrictions['maxRootFolders'];

        // var gridData = [];
        // gridData.push({key:'applyDate',value:'fdff'});
        // gridData.push({key:'creationDate',value:'fewedff'});
        // gridData.push({key:'crawlerExpiry',value:'fdweweff'});
        // $scope.gridData =gridData;
      }

      $scope.loadLicenseStatus();


      var onBeforeLoad = function()
      {

        $scope.showDetails=false;
        $scope.uploadFileSuccess=false;
        $scope.uploadStringSuccess=false;
        $scope.uploadError=false;
        $scope.errorUploadMsg = null;

      }

      $scope.uploadLicKeyChanged = function()
      {
        onBeforeLoad();
        $scope.theFile=null;
      }
      $scope.uploadFileToServer = function(apply,onSuccessCallBack){
        onBeforeLoad();
        $scope.licText=null;
         var fd = new FormData();
        //for( var key in $scope.theFile) {
        //  fd.append(key, $scope.theFile[key]);
        //}
        fd.append( "file", $scope.theFile);

        licenseResource.importLicenseFromFile(  fd,apply,
            function (importResult:License.DtoLicenseImportResult) {
              if(importResult.success) {
                $scope.showDetails=true;
                $scope.uploadFileSuccess=true;
                setData(importResult.docAuthorityLicenseDto);
                onSuccessCallBack?onSuccessCallBack():null;
              }
              else {
                $scope.showDetails=false;
                $scope.uploadError=true;
                $scope.errorUploadMsg = importResult.reason;
              }
            },
            function() {
              $scope.theFile = null;
              onError()
            });


      }

      $scope.uploadLicKey = function(apply,onSuccessCallBack){
        onBeforeLoad();
        licenseResource.importLicenseString(  $scope.licText,apply,
            function (importResult:License.DtoLicenseImportResult) {
              if(importResult.success) {
                $scope.uploadStringSuccess=true;
                $scope.showDetails=true;
                setData(importResult.docAuthorityLicenseDto);
                onSuccessCallBack?onSuccessCallBack():null;
              }
              else {
                $scope.uploadError=true;
               $scope.errorUploadMsg = importResult.reason;
              }
            },
            function() {
              $scope.theFile = null;
              onError()
            });


      }


      $scope.cancel = function(){

        $uibModalInstance.dismiss('Canceled');
      };
      $scope.apply = function(){
        if($scope.uploadStringSuccess)
        {
          $scope.uploadLicKey(true,function(){
            $uibModalInstance.close(  );
          })
        }
        if($scope.uploadFileSuccess)
        {
          $scope.uploadFileToServer(true,function(){
            $uibModalInstance.close(  );
          })

        }
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      var key:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'key',
        title: 'key',
        type: EFieldTypes.type_string,
        displayed: false,
        isPrimaryKey:true,
        editable: false,
        nullable: true,


      };
      var value:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: 'value',
        title: 'Value',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,


      };
      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(key);
      fields.push(value);

      $scope.fields=fields;
    })


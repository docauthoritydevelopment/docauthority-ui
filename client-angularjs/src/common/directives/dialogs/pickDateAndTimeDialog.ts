///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.dateTimePicker', [

    ])
    .controller('pickDateAndTimeDialogCtrl', function ($scope,  $filter,$uibModalInstance, data,configuration:IConfig) {

      $scope.format = configuration.dateFormat;
      $scope.altInputFormats = ['d!/M!/yy','d!-M!-yy'];
      var init=function(data)
      {
        $scope.theDate = data.theDate?new Date(data.theDate):null;
        $scope.title='Pick date and time';
        $scope.initiated=true;
      }

      init(data);




      $scope.dateChanged=function()
      {
        let today = new Date(Date.now());
        $scope.myForm.$setValidity('startDatehigher', $scope.theDate<= today);
      }



      $scope.dateOptions  = {
        formatYear: 'yy',
        startingDay: 1
      };

      // $scope.clear = function() {
      //   $uibModalInstance.close({theDate: null});
      // }


      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          $uibModalInstance.close({theDate: $scope.theDate?$scope.theDate.getTime():null});
        };
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

    })


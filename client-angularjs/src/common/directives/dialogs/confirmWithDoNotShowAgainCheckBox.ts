///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.confirms', [

  ])
  .controller('confirmWithDoNotShowAgainCheckBox',
    function ($scope, $uibModalInstance, data) {

      $scope.title = data.title;
      $scope.msg = data.msg;
      $scope.dontShowMsgAgain = false;

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.ok = function() {
        $uibModalInstance.close($scope.dontShowMsgAgain);
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.ok();
      };
    });



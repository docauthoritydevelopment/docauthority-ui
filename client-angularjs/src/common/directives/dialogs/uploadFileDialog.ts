///<reference path='../../all.d.ts'/>
'use strict';
declare var _:any;
angular.module('directives.dialogs.uploadFile', [

    ])
    .controller('uploadFileDialogCtrl', function ($scope, $uibModalInstance, data) {

      $scope.fileLoaded=false;
      $scope.bodyText = data.bodyText;
      $scope.title = data.title;
      $scope.placeholderText = data.placeholderText;
      $scope.inputText=data.initialValue;
      $scope.initiated=true;
      //-- Methods --//

      $scope.resetFile = function () {
        $scope.theFile = null;
        $scope.fileData=null;
        $scope.fileError="";
      };

      $scope.$watch(function () { return $scope.theFile; }, function (value) {
        $scope.importErrorMsg = null;
        if($scope.theFile)
        {
          $scope.fileData=null;
          $scope.fileError="";
          if(_.endsWith($scope.theFile.name.toLowerCase(),'.csv')||_.endsWith($scope.theFile.name.toLowerCase(),'.txt')) {
            $scope.uploadFileBrowser();
          }
          else
          {
            $scope.fileError="Only .csv file format is supported";
          }
        }
      });
      $scope.uploadFileBrowser = function(){
        var file =$scope.theFile;
        var fileReader = new FileReader();
        fileReader.onloadend = function(e){
          $scope.$apply(function () {
            let data;
            // ADDED CODE
            if (!e) {
              data = (<any>fileReader).content;
            }
            else {
              data =(<any> e.target).result;
            }
            $scope.fileRecords = data.split("\n");
            $scope.fileData = data;

          });

          }
        //extend FileReader for IE 11
        if (!FileReader.prototype.readAsBinaryString) {
          FileReader.prototype.readAsBinaryString = function (fileData) {
            var binary = "";
            var pt = this;
            var reader = new FileReader();
            reader.onload = function (e) {
              var bytes = new Uint8Array(reader.result);
              var length = bytes.byteLength;
              for (var i = 0; i < length; i++) {
                binary += String.fromCharCode(bytes[i]);
              }
              //pt.result  - readonly so assign binary
              pt.content = binary;
              $(pt).trigger('onloadend');
            }
            reader.readAsArrayBuffer(fileData);
          }
        }
        fileReader.readAsBinaryString(file);
        //var start = parseInt(opt_startByte) || 0;
        //var stop = parseInt(opt_stopByte) || file.size - 1;
        //fileReader.onloadend = function(evt) {
        //  if (evt.target.readyState == FileReader.DONE) { // DONE == 2
        //    document.getElementById('byte_content').textContent = evt.target.result;
        //    document.getElementById('byte_range').textContent =
        //        ['Read bytes: ', start + 1, ' - ', stop + 1,
        //          ' of ', file.size, ' byte file'].join('');
        //  }
        //};

        //var blob = file.slice(start, stop + 1);
        //fileReader.readAsBinaryString(blob);

      }
      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        if($scope.fileRecords && $scope.fileRecords.length>0) {
          $uibModalInstance.close($scope.fileRecords);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })



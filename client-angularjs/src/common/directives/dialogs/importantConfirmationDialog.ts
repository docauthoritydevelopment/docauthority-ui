///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.importantConfirmation',[]).controller('importantConfirmationDialogCtrl', function ($scope,$uibModalInstance,data,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
  $scope.data = data;
  $scope.yesInput = '';

  //-- Methods --//
  $scope.cancel = function() {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.ok = function() {
    $uibModalInstance.close('Ok');
  };

});


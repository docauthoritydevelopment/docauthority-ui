///<reference path='../../all.d.ts'/>
'use strict';
angular.module('directives.dialogs', [
      'directives.dialogs.uploadFile',
    'directives.dialogs.uploadFile.serverUpload'
    ])
    .controller('openSingleInputDialogCtrl', function ($scope, $uibModalInstance, data) {

      $scope.bodyText = data.bodyText;
      $scope.title = data.title;
      $scope.placeholderText = data.placeholderText;
      $scope.inputText=data.initialValue;
      $scope.initiated=true;
      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        if($scope.inputText && $scope.inputText.trim()!='') {
          $uibModalInstance.close($scope.inputText);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


    .controller('openCheckboxListDialogCtrl', function ($scope, $uibModalInstance, data) {

      $scope.bodyText = data.bodyText;
      $scope.title = data.title;
      $scope.placeholderText = data.placeholderText;
      $scope.inputText=data.initialValue;
      $scope.checkboxList=data.checkboxList;
      $scope.radioList=data.radioList;
      $scope.selectedValues=[];
      if($scope.checkboxList)
      {
        for(var i=0; i< $scope.checkboxList.length; i++) {
          $scope.selectedValues[i]=true;
        }
      }

      $scope.initiated=true;
      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        var checkedItems=[];
        for(var i=0; i< $scope.checkboxList.length; i++)
        {
          if($scope.selectedValues[i])
          {
            checkedItems.push($scope.checkboxList[i]);
          }
        }
        if($scope.inputText && $scope.inputText.trim()!='') {
          $uibModalInstance.close({inputText: $scope.inputText, checkedItems:checkedItems,selectedRadio:$scope.selectedRadio});
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })




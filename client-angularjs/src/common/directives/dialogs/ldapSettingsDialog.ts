///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.ldapSettings', [
])
  .controller('ldapSettingsDialogCtrl',
    function ($window, $scope, $uibModalInstance, data,dialogs,ldapConnectionResource:ILdapConnectionResource, localizedMessages) {

      var psaudoPassword = '11111';

      $scope.LdapServerDialectOptions=[
        AppSettings.ELdapServerDialect.ACTIVE_DIRECTORY,
        AppSettings.ELdapServerDialect.ACTIVE_DIRECTORY_LEGACY,
        AppSettings.ELdapServerDialect.ApacheDS
      ];

      var init=function() {
        $scope.ldapConnection = <Operations.DtoLdapConnectionDetails>(data.ldapConnection);
        if ($scope.ldapConnection) {
          $scope.title = 'Edit LDAP connection';
          $scope.editMode=true;
          $scope.ldapConnection.managerPass = psaudoPassword;
        } else {
          let ldapConnection = new Operations.DtoLdapConnectionDetails();
          ldapConnection.enabled = true;
          $scope.ldapConnection = ldapConnection;
          $scope.title = 'Add LDAP connection';
        }
      };

      $scope.testConnection = function(){
        $scope.connectivityTest = null;
        $scope.failureMessage = null;
        if ($scope.ldapForm.$valid) {
          $scope.testConnectionInProgress = true;
          if( $scope.editMode)
          {
            const copyLdapConnection =  <Operations.DtoLdapConnectionDetails>(_.cloneDeep($scope.ldapConnection));
            copyLdapConnection.managerPass = copyLdapConnection.managerPass == psaudoPassword ? null: copyLdapConnection.managerPass;
            ldapConnectionResource.testConnection(copyLdapConnection,
              function (success: boolean, failureMessage) {
                $scope.testConnectionInProgress = false;
                $scope.connectivityTest = success;
                $scope.failureMessage = failureMessage;
              }
              , onTestConnectionError)
          }
          else {
            ldapConnectionResource.testConnection($scope.ldapConnection, function (success:boolean,failureMessage) {
                $scope.connectivityTest = success;
                $scope.failureMessage = failureMessage;
                $scope.testConnectionInProgress = false;
              }
              , onTestConnectionError)
          }
        }
      };
      var onTestConnectionError  = () =>
      {
        $scope.testConnectionInProgress = false;
        dialogs.error('Error','Sorry, failed to test ldap connection ');
      }


      $scope.onPasswordChange = function() {
        $scope.connectivityTest = null;
      };

      $scope.encryptionChanged = function(type)
      {
        if(type=='ssl' &&  ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useSsl )
        {
          ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useTls = false;
        }
        else if(type=='tls' &&   ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useTls )
        {
          ( <AppSettings.DtoSmtpConfiguration> $scope.connection).useSsl = false;
        }
      };

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      };

      $scope.save = function() {
        $scope.submitted = true;
        if ($scope.ldapForm.$valid) {

          if ($scope.editMode) {
            if($scope.ldapConnection.managerPass === psaudoPassword) {
               $scope.ldapConnection.managerPass = null;
            }
            $uibModalInstance.close($scope.ldapConnection);
          }
          else {
            ldapConnectionResource.addNewConnection($scope.ldapConnection, function (newLappConnection:Operations.DtoLdapConnectionDetails) {
                $uibModalInstance.close(newLappConnection);
              }
              , function (e) {

                  if(e.status==400 && e.data.type == ERequestErrorCode.ITEM_ALREADY_EXISTS) {
                    dialogs.error('Error', localizedMessages.get('crud.add.entity.error.alreadyExists', {
                      fieldValue: $scope.ldapConnection.name,
                      fieldName: 'name',
                      entityName: 'ldap connection'
                    }));
                  }
                  else {
                    dialogs.error('Error',localizedMessages.get('crud.add.entity.error.generic', {
                      fieldValue: $scope.ldapConnection.name,
                      fieldName: 'name',
                      entityName: 'ldap connection'
                    }));
                  }

              })
          }
        }
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      };

      init();
    });



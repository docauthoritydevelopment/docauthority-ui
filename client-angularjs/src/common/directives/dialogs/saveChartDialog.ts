///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.charts',[]).controller('saveChartDialogCtrl', function ($scope,$uibModalInstance,data,currentUserDetailsProvider:ICurrentUserDetailsProvider) {
  $scope.data = data;
  $scope.isMngReportsUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngReports]);

  //-- Methods --//
  $scope.cancel = function() {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function() {
    $scope.saveNewChart(null);
    $uibModalInstance.close($scope.data);
  };

  $scope.hitEnter = function(evt) {
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };
});


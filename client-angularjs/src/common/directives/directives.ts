///<reference path='../../common/all.d.ts'/>
///<reference path='../../../typings/icheck/icheck.d.ts'/>
'use strict';

/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 * Main directives.js file
 * Define directives for used plugin
 *
 *
 * Functions (directives)
 *  - pageTitle
 *  - sideNavigation
 *  - iboxTools
 *  - minimalizaSidebar
 *  - vectorMaps
 *  - sparkline
 *  - icheck
 *  - ionRangeSlider
 *  - dropZone
 *  - responsiveVideo

 *
 */

/**
 * pageTitle - Directive for set Page title - mata title
 */

function daKendoGrid($timeout): ng.IDirective {
  return  <ng.IDirective>{
    restrict: 'A',
    //scope: {
    //  options: "=",
    //},
    scope:false,
    replace: true,
    template:'<div class="fill-height" kendo-grid ng-transclude k-on-data-bound="resizeGrid()" k-options="mainGridOptions"></div>',
    transclude : true,
    controller: function ($scope ,$timeout, $window) {
      function resizeGrid() {
        $timeout(function(){
          var gridElement = angular.element(".k-grid"), dataArea = angular.element('.k-grid-content'), gridHeight = gridElement.innerHeight(), otherElements = gridElement.children().not(".k-grid-content"), otherElementsHeight = 0;
          otherElements.each(function () {
            otherElementsHeight += $(this).outerHeight();
          });
          dataArea.height(gridHeight - otherElementsHeight - 100);
        });
      }

      $scope.$watch(function () { return $window.innerHeight; }, function (value) {
        resizeGrid();
      });

    }
  };
}

function bindHtmlCompile($compile): ng.IDirective{
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      scope.$watch(function () {
        return scope.$eval(attrs['bindHtmlCompile']);
      }, function (value) {
        element.html(value);
        $compile(element.contents())(scope);
      });
    }
  };
}
function selectOptionsDisabled($parse) {

    var disableOptions = function (scope, attr, element, data, fnDisableIfTrue) {
      // refresh the disabled options in the select element.
      var containsEmptyOption = element.find("option[value='']").length != 0;
      angular.forEach(element.find("option"), function (value, index) {
        var elem = angular.element(value);
        if (elem.val() != "") {
          var locals = {};
          //ST-Software modification (index-1 because of the initial empty option)
          var i = containsEmptyOption ? index - 1 : index;
          locals[attr] = data[i];
          elem.attr("disabled", fnDisableIfTrue(scope, locals));
        }
      });
    };
    return {
      priority: 0,
      require: 'ngModel',
      link: function (scope, iElement, iAttrs, ctrl) {
        // parse expression and build array of disabled options
        var expElements = iAttrs.selectOptionsDisabled.match(/^\s*(.+)\s+for\s+(.+)\s+in\s+(.+)?\s*/);
        var attrToWatch = expElements[3];
        var fnDisableIfTrue = $parse(expElements[1]);
        scope.$watch(attrToWatch, function (newValue, oldValue) {
          if (newValue)
            disableOptions(scope, expElements[2], iElement, newValue, fnDisableIfTrue);
        }, true);
        // handle model updates properly
        scope.$watch(iAttrs.ngModel, function (newValue, oldValue) {
          var disOptions = $parse(attrToWatch)(scope);
          if (newValue)
            disableOptions(scope, expElements[2], iElement, disOptions, fnDisableIfTrue);
        });
      }
    };

}
function toggleKendoSplitter(): ng.IDirective
{
  return{

    link:function(scope,element,attr:any)
    {

      element.bind('click', refreshSplitMode);

     function refreshSplitMode(){
        var splitterElement = $('.k-splitter');
       element.parent().children().removeClass('active');
        if(splitterElement) {
          var splitter = splitterElement.data("kendoSplitter");
          var leftPane = splitterElement.children('.panel')[0];
          var rightPane = splitterElement.children('.panel')[1];
          element.addClass('active');
          if(splitter) {
            if (attr.mode == 'left') {
              splitter.expand(leftPane);
              splitter.collapse(rightPane);
            }
            else if (attr.mode == 'right') {
              splitter.expand(rightPane);
              splitter.collapse(leftPane);
            }
            else {
              splitter.expand(rightPane);
              splitter.expand(leftPane);
            }
          }

        }
      };
    }
  }
}
function templateViewMode(userSettings:IUserSettingsService,$state:any,$stateParams): ng.IDirective
{
  return{
    link:function(scope,element,attr:any)
    {
      scope.$on(userSettings.gridViewPreferencesUpdatedEvent, function (sender, value) {
        updateElementActiveState();

      });
      updateElementActiveState();

      element.bind('click', function(){
        saveUserData(attr.templateMode);
        $state.transitionTo($state.current, $stateParams, {
               reload: true,
               inherit: false,
               notify: true
             });
      });

     function updateElementActiveState() {
       userSettings.getGridViewPreferences(function(gvp){
         var mode = gvp.gridView?'false':'true';
         if (attr.templateMode == mode) {
           element.children('i').show();
         }
         else {
           element.children('i').hide();
         }
         mode=='true'?$('body').addClass('template-view'):$('body').removeClass('template-view');
       });
     }

      function saveUserData(mode:string, savedTemplateViewMode?:any)
      {
        if (savedTemplateViewMode) {
          savedTemplateViewMode.gridView = (mode=='false');
          userSettings.setGridViewPreferences(savedTemplateViewMode);
        }
        else {
          userSettings.getGridViewPreferences(function(savedTemplateViewMode1){
            savedTemplateViewMode1.gridView = (mode=='false');
            userSettings.setGridViewPreferences(savedTemplateViewMode1);
          });
        }
      }

    }
  }
}

function ngVisible () {
  return function (scope, element, attr) {
    scope.$watch(attr.ngVisible, function (visible) {
      element.css('visibility', visible ? 'visible' : 'hidden');
    });
  };
};

function pageTitle(pageTitle:IPageTitleService,$timeout): ng.IDirective
{
  return{
    link:function(scope,element,attr)
    {
      scope.$watch(function () { return pageTitle.get() }, function (value) {
        updateTitle();
      });
      function updateTitle() {
        $timeout(function() {
          element.html(pageTitle.get()) ;
        });

      }



    }
  }
}

function resize($window): ng.IDirective {
  return {

    link: function (scope:any, element) {
      var w = angular.element($window);
      scope.getWindowDimensions = function () {
        return {'h': w.innerHeight() - 1, 'w': w.innerWidth()};
      };
      scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
        scope.windowHeight = newValue.h;
        scope.windowWidth = newValue.w;
        scope.style = function () {
          return {
            'height': (newValue.h) + 'px',
            'width': (newValue.w) + 'px'
          };
        };

        scope.height = function (addValueInPX=0) {
          return {
            'height': (newValue.h + addValueInPX) + 'px',

          };
        };

        scope.heightInPx = function (addValueInPX=0) {
          return (newValue.h + addValueInPX) + 'px';

        };

        scope.contentHeight = function (addValueInPX=0) {
          var res = element.height()+addValueInPX;
          return res>0?res+'px':0;
        };

        scope.maxHeight = function (addValueInPX) {
          return {
            'max-height': (newValue.h + addValueInPX) + 'px',

          };
        };

      }, true);

      w.bind('resize', function () {
         scope.$apply();
      });
    }
  }
}
function ngEnter () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter);
        });

        event.preventDefault();
      }
    });
  };
};

function ngEsc (){
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 27) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEsc);
        });

        event.preventDefault();
      }
    });
  };
};
/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout): ng.IDirective {
    return {
        restrict: 'A',
        link: function(scope, element:any) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();

            });
        }
    };
}

  function focusIf($timeout, $parse) : ng.IDirective{
  return {
    link: function (scope, element, attrs:any) {
      var model = $parse(attrs.focusIf);
      scope.$watch(model, function (value) {
        if (value === true) {
          $timeout(function () {
            element[0].focus();
          }, 30);
        }
      });
    }
  };
};

function selectTextIf($timeout, $parse) {
  return {
    link: function (scope, element, attrs) {
      var model = $parse(attrs.selectTextIf);
      scope.$watch(model, function (value) {
        if (value) {
          $timeout(function () {
            element[0].select();
          }, 0);
        }
      });
    }
  };
};


/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) : ng.IDirective{
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };

                // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
function minimalizaSidebar(/*$timeout*/): ng.IDirective {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope/*, $element*/) {
            $scope.minimalize = function () {
                $('body').toggleClass('mini-navbar');
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(500);
                        }, 100);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(500);
                        }, 300);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            };
        }
    };
}

/**
 * vectorMap - Directive for Vector map plugin
 */
function vectorMap() {
    return {
        restrict: 'A',
        scope: {
            myMapData: '='
        },
        link: function (scope, element/*, attrs*/) {
            element.vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        'fill-opacity': 0.9,
                        stroke: 'none',
                        'stroke-width': 0,
                        'stroke-opacity': 0
                    }
                },
                series: {
                    regions: [
                        {
                            values: scope.myMapData,
                            scale: ['#1ab394', '#22d6b1'],
                            normalizeFunction: 'polynomial'
                        }
                    ]
                }
            });
        }
    };
}


/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '='
        },
        link: function (scope, element/*, attrs*/) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function(){
                render();
            });
            var render = function () {
              (<any> $(element)).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    };
}

function folderTypeHtml($filter): ng.IDirective {
  return  {
    template: `<da-icon href='{{folderType}}'></da-icon>`,
    scope: {
      type: '='
    },
    controller: function ($scope) {
      switch($scope.type) {
        case Entities.EFolderType.ZIP:
         $scope.folderType = 'zip';
         break;
        case Entities.EFolderType.EMAIL:
          $scope.folderType = 'email';
          break;
        case Entities.EFolderType.PST:
          $scope.folderType = 'pst';
          break;
        default:
          $scope.folderType = 'folder';
      }
    }
  }
}


function fileTypeHtml($filter): ng.IDirective {
  return  {
    link: function (scope, element,attrs:any) {
      scope.$watch(function(){return attrs.type}, function (value) {
        render(value);
      });
      var render =  function(fileType:Dashboards.EFileType)
      {
        var html;
        if (fileType == Dashboards.EFileType.scannedPDF) {
          html =
              "<span class='fa-stack fa-1x' title='" + $filter('translate')(fileType) + "'>" +
              " <span class='fileType fa fa-file-pdf-o fa-stack-2x  scanned-pdf'></span> " +
              " <span class='fa fa-circle fa-stack-1x fa-stack-offset-left-background'></span> " +
              "<span class='fa fa-circle-o fa-stack-1x fa-stack-offset-left'></span>  " +
              "</span>"
        }
        else if (fileType == Dashboards.EFileType.mail) {
          html = "<span class='purple fa fa-envelope-o'></span>";
        }
        else {
          html = "<span title='" + $filter('translate')(fileType) + "' class='fileType " + fwfileType()(fileType) + "'></span>";
        }
        $(element).append(html);
      }
    }
  }
}

function groupTypeHtml(configuration:IConfig,$filter): ng.IDirective {
  return  {
    link: function (scope, element,attrs:any) {
      scope.$watch(function(){return attrs.type || attrs.fileType}, function (value) {
        render(attrs.type,<any>attrs.fileType);
      });
      var render =  function(groupType:Entities.EGroupType,fileAnalyzeHint:Entities.EAnalyzeHint)
      {
        var html;
        if ((!groupType||groupType == Entities.EGroupType.USER_GROUP) && fileAnalyzeHint==Entities.EAnalyzeHint.MANUAL) {
          html =
            "<span class='fa-stack fa-1x1' title='File was added manually to this group' style='height: 10px;vertical-align: top;width: 12px;'>" +
            " <span class=' fa-stack-1x  "+configuration.icon_group+" tag-"+groupType+" '></span> " +
              "<span class='fa-stack-1x fa fa-hand-rock-o ' style='margin-top: -6px; margin-left: -6px;'></span>  " +
            "</span>"
        }
        else {
          html = "<span title='" + $filter('translate')(groupType) + "' class='"+configuration.icon_group+" tag-"+groupType+" ' ></span>";
        }
        $(element).append(html);
      }

    }

  }
}
/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout): ng.IDirective {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs:any, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs.value;

                $scope.$watch($attrs.ngModel, function(/*newValue*/){
                    $(element).iCheck('update');
                });

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs.ngModel) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue((<any>event.target).checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs.ngModel) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
            });
        }
    };
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
function ionRangeSlider(): ng.IDirective {
    return {
        restrict: 'A',
        scope: {
            rangeOptions: '='
        },
        link: function (scope:any, elem/*, attrs*/) {
            elem.ionRangeSlider(scope.rangeOptions);
        }
    };
}
function draggable(){
  return {
    restrict: 'EA',
    link: function(scope, element,attr) {
      if (attr.draggableElementClass)
      {
        var settings=attr.handleElementClass?{handle:'.'+attr.handleElementClass}:null;
        element.closest(<any>$('.'+attr.draggableElementClass)).draggable(settings);
      }
      else {
        element.draggable();
      }
    }
  }
};
function bindFile() : ng.IDirective{
  return {
    require: "ngModel",
    restrict: 'A',
    link: function ($scope, el, attrs, ngModel) {
      el.bind('change', function (event) {
        var fileData = (<any>event.target).files[0];
        fileData['fullPath'] = el.val();
        ngModel.$setViewValue(fileData);
        $scope.$apply();
      });

      $scope.$watch(function () {
        return ngModel.$viewValue;
      }, function (value) {
        if (!value) {
          el.val("");
        }
      });
    }
  };
}
/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */
function dropZone(){
  return function(scope, element, attrs) {
    element.dropzone({
      url: "/upload",
      maxFilesize: 100,
      paramName: "uploadfile",
      maxThumbnailFilesize: 5,
      init: function() {
        scope.files.push({file: 'added'}); // here works
        this.on('success', function(file, json) {
        });

        this.on('addedfile', function(file) {
          scope.$apply(function(){
            alert(file);
            scope.files.push({file: 'added'});
          });
        });

        this.on('drop', function(file) {
          alert('file');
        });

      }

    });
  }
};

function scrollYOffsetElement($anchorScroll) {
  return function (scope, element) {
    $anchorScroll.yOffset = element;
  };
}

function scrollToBottom($timeout) : ng.IDirective {
  return {
    scope:
    {
      scrollData:'='
    },
    restrict: 'A',
    link: function(scope:any, element, attr) {
      scope.$watchCollection(function () { return scope.scrollData; }, function (value) {
        $timeout(function() {
          element[0].scrollTop = element[0].scrollHeight;
        });
      });

    }
  }
};

function backToTop ($anchorScroll, $location) {
  return function link(scope, element) {
    element.on('click', function(event) {
      $location.hash('');
      scope.$apply($anchorScroll);
    });
  };
};

function limitBig(){
  return function(num , precision) {
    if (typeof precision === 'undefined') precision = 7;
    if ((num+'').length <= (precision+2)) {
      return num;
    }
    return parseFloat(num).toPrecision(precision);
  }
}


function bytes() {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if(bytes==0)return '0';
    if (typeof precision === 'undefined') precision = 1;
    var units = [' bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
      number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  /*' ' + */ units[number];
  }
};

function commaSeparateNumber(){
  return function(val) {
    if(_.isNumber(val) && !isNaN(val)) {
      while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return val;
    }
    return "";
  }
}


function fwfileType() //font awesome file type
{
  var commonTrans = function(text:string) :string {
    var commonPref = 'fa fa-file-';
    var commonSuff = '-o';
    if(text=='')
    {
      return commonPref+'o';
    }
    return commonPref + text + commonSuff;
  };
  return function(fileType:Dashboards.EFileType) {
    if(!fileType)
    {
      return;
    }
    fileType = fileType;
    if(fileType==Dashboards.EFileType.text)
    {
      return commonTrans('text');
    }
    if(fileType==Dashboards.EFileType.other)
    {
      return commonTrans('');
    }
    if(fileType==Dashboards.EFileType.pdf)
    {
      return commonTrans('pdf');
    }
    if(fileType==Dashboards.EFileType.scannedPDF)
    {
      return commonTrans('pdf') + ' scanned-pdf';
    }
    if(fileType==Dashboards.EFileType.msExcel)
    {
      return commonTrans('excel');
    }
    if(fileType==Dashboards.EFileType.msWord)
    {
      return commonTrans('word');
    }

    return commonTrans('code');   // unknown
  }

}
function reverse(){
  return function(items) {
    return items&&items.slice? items.slice().reverse():items;
  };
};
function asDate() {
  return function (input) {
    return new Date(input);
  }
};

function fileFolder()
{
  return function(path)
  {
    if(path) {
      var tokens = path.split('\\');
      var fileName = tokens.pop();
      var folder = tokens.pop();
    //  var pfolder = tokens.pop();
      return folder;
    }
  }
}

function checkNull()
{
  return function(data)
  {
    if(data !== null) {
      return data;
    }
    return '';
  }
}



var hoursOffset = (new Date(0)).getHours();
function formatSpan($filter)
{
  return function(dateTimeSpanInMillisec)
  {
    if (dateTimeSpanInMillisec == null) {
      return '';
    }
    dateTimeSpanInMillisec<0?dateTimeSpanInMillisec=0:null;
    dateTimeSpanInMillisec>0&&dateTimeSpanInMillisec<1000 ? dateTimeSpanInMillisec=1000:null;
    let neededPeriodInMiliSec = dateTimeSpanInMillisec;
    let days = Math.floor(neededPeriodInMiliSec / (24*60*60*1000));
    let daysms=neededPeriodInMiliSec % (24*60*60*1000);
    let hours = Math.floor((daysms)/(60*60*1000));
    let hoursms=neededPeriodInMiliSec % (60*60*1000);
    let minutes = Math.floor((hoursms)/(60*1000));
    let minutesms=neededPeriodInMiliSec % (60*1000);
    let sec = Math.floor((minutesms)/(1000));
    let ans:string = null;
    let pad2 = function(number) {
      return (number < 10 ? '0' : '') + number
    }
    if (dateTimeSpanInMillisec < 24*3600*1000) {
      ans =pad2(hours) +":"+pad2(minutes)+":"+pad2(sec)
    }
    else {
      ans =days+' Days '+ pad2(hours) +":"+pad2(minutes)+":"+pad2(sec)
    }
    return ans;
  }
}
function passwordMatchCheck():ng.IDirective {
  return <ng.IDirective>{
    require: 'ngModel',
    link: function (scope, elem, attrs:any, ctrl) {
      var firstPassword = '#' + attrs.passwordMatchCheck;
      elem.add(firstPassword).on('keyup', function () {
        scope.$apply(function () {
          var v = elem.val()===$(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });
    }
  }
};
function checkPasswordStrength (): ng.IDirective  {
  return {
    replace: false,
    require: 'ngModel',
    link: function (scope, iElement, iAttrs,ngModel) {
      iElement.addClass('check-password-strength');
      var strengthMeter = {
        colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
        getColor: function (s) {

          var idx = 0;
          if (s <= 10) { idx = 0; }
          else if (s <= 20) { idx = 1; }
          else if (s <= 30) { idx = 2; }
          else if (s <= 40) { idx = 3; }
          else { idx = 4; }

          return { idx: idx + 1, col: this.colors[idx] };

        },
        getStrength:function(p) {
        var stringReverse = function(str) {
              for (var i = str.length - 1, out = ''; i >= 0; out += str[i--]) {}
              return out;
            },
            matches = <any>{
              pos:<any> {},
              neg:<any> {}
            },
            counts =<any> {
              pos:<any> {},
              neg: <any>{
                seqLetter: 0,
                seqNumber: 0,
                seqSymbol: 0
              }
            },
            tmp,
            strength = 0,
            letters = 'abcdefghijklmnopqrstuvwxyz',
            numbers = '01234567890',
            symbols = '\\!@#$%&/()=?¿',
            back,
            forth,
            i;

        if (p) {
          // Benefits
          matches.pos.lower = p.match(/[a-z]/g);
          matches.pos.upper = p.match(/[A-Z]/g);
          matches.pos.numbers = p.match(/\d/g);
          matches.pos.symbols = p.match(/[$-/:-?{-~!^_`\[\]]/g);
          matches.pos.middleNumber = p.slice(1, -1).match(/\d/g);
          matches.pos.middleSymbol = p.slice(1, -1).match(/[$-/:-?{-~!^_`\[\]]/g);

          counts.pos.lower = matches.pos.lower ? matches.pos.lower.length : 0;
          counts.pos.upper = matches.pos.upper ? matches.pos.upper.length : 0;
          counts.pos.numbers = matches.pos.numbers ? matches.pos.numbers.length : 0;
          counts.pos.symbols = matches.pos.symbols ? matches.pos.symbols.length : 0;

          tmp = Object.keys(counts.pos).reduce(function(previous, key) {
            return previous + Math.min(1, counts.pos[key]);
          }, 0);

          counts.pos.numChars = p.length;
          tmp += (counts.pos.numChars >= 8) ? 1 : 0;

          counts.pos.requirements = (tmp >= 3) ? tmp : 0;
          counts.pos.middleNumber = matches.pos.middleNumber ? matches.pos.middleNumber.length : 0;
          counts.pos.middleSymbol = matches.pos.middleSymbol ? matches.pos.middleSymbol.length : 0;

          // Deductions
          matches.neg.consecLower = p.match(/(?=([a-z]{2}))/g);
          matches.neg.consecUpper = p.match(/(?=([A-Z]{2}))/g);
          matches.neg.consecNumbers = p.match(/(?=(\d{2}))/g);
          matches.neg.onlyNumbers = p.match(/^[0-9]*$/g);
          matches.neg.onlyLetters = p.match(/^([a-z]|[A-Z])*$/g);

          counts.neg.consecLower = matches.neg.consecLower ? matches.neg.consecLower.length : 0;
          counts.neg.consecUpper = matches.neg.consecUpper ? matches.neg.consecUpper.length : 0;
          counts.neg.consecNumbers = matches.neg.consecNumbers ? matches.neg.consecNumbers.length : 0;


          // sequential letters (back and forth)
          for (i = 0; i < letters.length - 2; i++) {
            var p2 = p.toLowerCase();
            forth = letters.substring(i, parseInt(i + 3));
            back = stringReverse(forth);
            if (p2.indexOf(forth) !== -1 || p2.indexOf(back) !== -1) {
              counts.neg.seqLetter++;
            }
          }

          // sequential numbers (back and forth)
          for (i = 0; i < numbers.length - 2; i++) {
            forth = numbers.substring(i, parseInt(i + 3));
            back = stringReverse(forth);
            if (p.indexOf(forth) !== -1 || p.toLowerCase().indexOf(back) !== -1) {
              counts.neg.seqNumber++;
            }
          }

          // sequential symbols (back and forth)
          for (i = 0; i < symbols.length - 2; i++) {
            forth = symbols.substring(i, parseInt(i + 3));
            back = stringReverse(forth);
            if (p.indexOf(forth) !== -1 || p.toLowerCase().indexOf(back) !== -1) {
              counts.neg.seqSymbol++;
            }
          }


          var repeats = {};
          var _p = p.toLowerCase();
          var arr = _p.split('');
          counts.neg.repeated = 0;
          for (i = 0; i < arr.length; i++) {
            let cnt = 0 ;
            for (let grow = 0 ; grow < _p.length ; grow++) {
              if (_p[grow] == _p[i]) {
                cnt ++;
              }
            }
            if (cnt > 1 && !repeats[_p[i]]) {
              repeats[_p[i]] = cnt;
              counts.neg.repeated += cnt;
            }
          }

          // Calculations
          strength += counts.pos.numChars * 4;
          if (counts.pos.upper) {
            strength += (counts.pos.numChars - counts.pos.upper) * 2;
          }
          if (counts.pos.lower) {
            strength += (counts.pos.numChars - counts.pos.lower) * 2;
          }
          if (counts.pos.upper || counts.pos.lower) {
            strength += counts.pos.numbers * 4;
          }
          strength += counts.pos.symbols * 6;
          strength += (counts.pos.middleSymbol + counts.pos.middleNumber) * 2;
          strength += counts.pos.requirements * 2;

          strength -= counts.neg.consecLower * 2;
          strength -= counts.neg.consecUpper * 2;
          strength -= counts.neg.consecNumbers * 2;
          strength -= counts.neg.seqNumber * 3;
          strength -= counts.neg.seqLetter * 3;
          strength -= counts.neg.seqSymbol * 3;

          if (matches.neg.onlyNumbers) {
            strength -= counts.pos.numChars;
          }
          if (matches.neg.onlyLetters) {
            strength -= counts.pos.numChars;
          }
          if (counts.neg.repeated) {
            strength -= (counts.neg.repeated / counts.pos.numChars) * 10;
          }
        }
        console.log(Math.max(0, Math.min(100, Math.round(strength))));
        return Math.max(0, Math.min(100, Math.round(strength)));
      }
      };


      scope.$watch(function () {return ngModel.$modelValue}, function (newValue) {
        if (!newValue || newValue === '') {
          iElement.css({ "display": "none"  });
        } else {
          var c = strengthMeter.getColor(strengthMeter.getStrength(newValue));
          iElement.css({ "display": "inline" });
          iElement.children('li')
              .css({ "background": "#DDD" })
              .slice(0, c.idx)
              .css({ "background": c.col });
        }

      });

    },
    template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>'
  };
};
function numberFixedLen ()  {
  return function(a,b){
    return(1e4+""+a).slice(-b);
  };
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('directives',[
      'directives.grid',
      'directives.params',
      'directives.icons',
      'directives.loader'

  ])

    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('vectorMap', vectorMap)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('ionRangeSlider', ionRangeSlider)
    .directive('dropZone', dropZone)
    .directive('bindFile', bindFile)
    .directive('daKendoGrid', <any>daKendoGrid)
    .directive('scrollYOffsetElement', <any>scrollYOffsetElement)
    .directive('backToTop', <any>backToTop)
    .directive('resize', resize)
    .directive('toggleKendoSplitter', toggleKendoSplitter)
    .directive('templateViewMode', templateViewMode)
    .directive('pageTitle', pageTitle)
    .directive('ngEnter', ngEnter)
    .directive('ngEsc', ngEsc)
    .directive('focusIf', focusIf)
    .directive('selectTextIf', selectTextIf)
    .directive('draggable', draggable)
    .directive('selectOptionsDisabled', selectOptionsDisabled)
    .directive('bindHtmlCompile', bindHtmlCompile)
    .directive('ngVisible', ngVisible)
    .directive('scrollToBottom', scrollToBottom)
    .directive('checkPasswordStrength', checkPasswordStrength)
    .directive('passwordMatchCheck', passwordMatchCheck)
    .directive('fileTypeHtml', fileTypeHtml)
    .directive('folderTypeHtml', folderTypeHtml)
    .directive('groupTypeHtml', groupTypeHtml)
    .filter('bytes',bytes)
    .filter('limitBig', limitBig)
    .filter('commaSeparateNumber',commaSeparateNumber)
    .filter('fileFolder',fileFolder)
    .filter('checkNull',checkNull)
    .filter('asDate',asDate)
    .filter('fwfileType',fwfileType)
    .filter('numberFixedLen',numberFixedLen)
    .filter('reverse',reverse)
    .filter('formatSpan',formatSpan);



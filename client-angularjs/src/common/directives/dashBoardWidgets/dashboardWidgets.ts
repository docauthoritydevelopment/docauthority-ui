///<reference path='../../../common/all.d.ts'/>
'use strict';
angular.module('directives.dashboardWidgets', [
  'directives.dashboardWidgets.listReport',
  'directives.dashboardWidgets.itemReport',
  'directives.dashboardWidgets.histogram',
  'directives.dashboardWidgets.highchartsHistogram',
  'directives.dashboardWidgets.multiListReport',
  'directives.dashboardWidgets.histogram.multi',
  'directives.dashboardWidgets.histogram.multi.highchart'

]) .directive('widgetBox',
  function ($compile) {
    return {
      // restrict: 'E',
      templateUrl: 'common/directives/dashboardWidgets/widget.tpl.html',
      replace:true,
      scope:{
        'wTitle':'=',
        'wLabel':'=',
        'wBody':'=',
      },
      link: function (scope:any, element, attrs) {
        scope.$watch(function () { return scope.wBody; }, function (value) {
          if (value) {
            angular.element('.box-body').empty();
            var newElem = $compile(scope.wBody)(scope);
            angular.element('.box-body').append(newElem);
          }
        });
      }
    };
  }
);

///<reference path='../../../common/all.d.ts'/>
'use strict';
angular.module('directives.dashboardWidgets.itemReport', [])
  .controller('itemReportCtrl', function ($scope,$element,configuration,$filter,Logger:ILogger,$translate,$state,$stateParams,$timeout,propertiesUtils) {
    var log:ILog = Logger.getInstance('itemReportCtrl');
    var filterType=null;
    var filterArgs=null;
    var titleValueFieldType=null;
    var titleValueFieldArgs=null;
    $scope.init=function(left) {
      if ($scope.fieldType == EFieldTypes.type_dateTime.toString()) {
        filterType = "date";
        filterArgs = configuration.dateTimeFormat;
        ;
      }
      else if ($scope.fieldType == EFieldTypes.type_date.toString()) {
        filterType = "date";
        filterArgs = configuration.dateFormat;
        ;
      }
      else if ($scope.fieldType == EFieldTypes.type_string.toString()) {
      }
      else {
        filterType = 'number';
      }

      if ($scope.titleValueFieldType == EFieldTypes.type_dateTime.toString()) {
        titleValueFieldType = "date";
        titleValueFieldArgs = configuration.dateTimeFormat;
        ;
      }
      else if ($scope.titleValueFieldType == EFieldTypes.type_date.toString()) {
        titleValueFieldType = "date";
        titleValueFieldArgs = configuration.dateFormat;
        ;
      }
      else if ($scope.titleValueFieldType == EFieldTypes.type_string.toString()) {
      }
      else {
        titleValueFieldType = 'number';
      }

    };

    $scope.$watch(function () { return $scope.value; }, function (value) {
      var val = $scope.value;
      if($scope.fieldType == EFieldTypes.type_dateTime.toString()) {
        val = propertiesUtils.isValidDate($scope.value) ?$filter('asDate')($scope.value) : "--";
      }
      if($scope.fieldType ==EFieldTypes.type_string.toString()) {
        $scope.textValue = val;
      }
      else {
        if(filterArgs) {
          $scope.textValue = $filter(filterType)(val, filterArgs);
        }
        else
        {
          $scope.textValue = $filter(filterType)(val);
        }
      }
    });

    $scope.$watch(function () { return $scope.titleValue; }, function (value) {
      if($scope.titleValue) {
        var val = $scope.titleValue;
        if ($scope.titleValueFieldType == EFieldTypes.type_dateTime.toString()) {
          val = $filter('asDate')($scope.titleValue);
        }
        if ($scope.titleValueFieldType == EFieldTypes.type_string.toString()) {
          $scope.titileTextValue = val;
        }
        else {
          if (titleValueFieldArgs) {
            $scope.titileTextValue = $filter(titleValueFieldType)(val, titleValueFieldArgs);
          }
          else {
            $scope.titileTextValue = $filter(titleValueFieldType)(val);
          }
        }
      }
    });

  })
  .directive('itemReport',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/dashBoardWidgets/itemReport.tpl.html',
        controller: 'itemReportCtrl',

        replace:true,
        scope:{
          'subTitle':'=',
          'value':'=',
          'caption':'=',
          'boxClass':'=',
          'titleValue':'=',
          'valueHtml':'=',
          'label':'=',
          'labelClass':'=',
          'fieldType':'=',
          'titleValueFieldType':'=',
          'subTitleHtml':'=',
          'myHeight': '=',
        },
        link: function (scope:any, element, attrs) {

        }
      };
    }
  );

///<reference path='../../../common/all.d.ts'/>
'use strict';

//declare var d3:any;


angular.module('directives.dashboardWidgets.treeViewGraph',
    [
      'ui.builder.charts',
      'ui.builder.saveAsFile'
    ])
    .controller('treeViewGraphCtrl', function ($scope,$element,Logger:ILogger,$state,$stateParams,$timeout,propertiesUtils,routerChangeService,$filter,
                                               chartsBuilder:ui.IChartOptionsBuilder,saveFilesBuilder:ISaveFileBuilder,configuration) {
      var log:ILog = Logger.getInstance('treeViewGraphCtrl');


      $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
      $scope.msedge = window.navigator.userAgent.indexOf("Edge") > -1;

      $scope.calculatedMarginTop = 0;
      $scope.elementName='da-chart-tree';
      var elementOptions:ui.IChartHelper;
      $scope.init = function () {
        //  createChart();
      };


      $scope.$watch(function () { return $scope.chartType; }, function (value) {
      //  createChart();
      });
      $scope.$watch(function () { return $scope.histogramData  }, function (value) {
        if($scope.histogramData ) {
          createChart();
          $scope.chartReady = true;
        }
      });

      $scope.$watch(function () { return $scope.chartTitle  }, function (value) {
        var chart = $element.find('.'+$scope.elementName);
        if(chart[0] && $scope.chartTitle) {
          elementOptions.setTitle(chart,$scope.chartTitle);
        }

      });


      $scope.saveAsPdf=function()
      {
        var fileName= 'Datamap chat';
        saveFilesBuilder.saveSVGAsPdf($($element.find('svg')),fileName);
      };

      $scope.saveAsImage=function() {
        var fileName= 'Datamap chat';
        let noneNodes = $element.find("#node_none");
        if ($scope.msie11 || $scope.msedge) {
          saveFilesBuilder.saveSVGAsImage($($element.find('svg')),fileName);
        }
        else {
          saveFilesBuilder.saveSVGAsBigImage($($element.find('svg')),fileName);
        }


      }

      $scope.saveAsExcel = function() {
        routerChangeService.addFileToProcess('Datamap_chart_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'doc_types',{
          "take":$scope.exportToExcelData.length,
          "skip":0,
          "page":1,
          "filter" : JSON.stringify($scope.exportToExcelFilter),
          "pageSize":$scope.exportToExcelData.length
        },{
          itemsNumber : $scope.rootTotalElements
        },$scope.exportToExcelData.length  > configuration.minimum_items_to_open_export_popup);
       };

      $scope.onSeriesHover = function(e)
      {

      };

      $scope.expandAll = function(){
        if($scope.treeRoot) {
          $scope.treeRoot.expandAll();
        }
      };

      $scope.collapseAll = function() {
        if($scope.treeRoot) {
          $scope.treeRoot.collapseAll();
        }
      };

      $scope.tooltipData = {};
      var mainScope = $scope;


      var createChart = function()
      {
        if ($scope.histogramData ) {
          log.debug('Histogram receives data length: '+$scope.histogramData.length);
          $scope.treeRoot = createTreeChart($scope.histogramData);
        }
      };


      var tooltipIsTapped: boolean = false;
      var createTreeChart = function (data:any[]) {
          var m = [50, 120, 50, 120],
              w = 3000 - m[1] - m[3],
              h = 600 - m[0] - m[2],
              i = 0,
              root:any = {};

          var spendField = "sum_Federal";
          var sumFields = ["Federal", "GovXFer", "State", "Local"];
          var sourceFields = ["Category", "Level1", "Level2", "Level3", "Level4", "Level5", "Level6", "Level7", "Level7","Level8"];

          var colors = [
            "#bd0026",
            "#8b154b",
            "#fecc5c",
            "#e3ab29",
            "#fd7725",
            "#f03b20",
            "#b0475f",
            "#dd1c77",
            "#c429c7",
            "#7929c5",
            "#5A0C7A",
            "#0ca908",
            "#2d6c38",
            "#4a9aaa",
            "#3932ca",
            "#52579a"];

          var formatNumber = d3.format(",.0f");
          var formatCurrency = function (d) {
           // return "$" + formatNumber(d) + " Billion"
            return formatNumber(d)
          };
          var formatCurrency1 = function (all, direct) {
            return ((all==direct)?'':formatNumber(all)) + ((direct>0)?(' ('+ formatNumber(direct) + ')'):'');
          };


        var afterToolTipChangedLocation:boolean = false;

        var tree = d3.layout.tree();
          var circles={};
          var paths={};
          var labels={};

          tree.children(function (d:any) { return d.values; }).size([h, w]);

          var toolTip = d3.select(document.getElementById("toolTip"));
          var header = d3.select(document.getElementById("head"));
          var header2 = d3.select(document.getElementById("header2"));

          var fedSpend = d3.select(document.getElementById("fedSpend"));

          var stateSpend = d3.select(document.getElementById("stateSpend"));
          var header5 = d3.select(document.getElementById("header5"));

          var localSpend = d3.select(document.getElementById("localSpend"));

          var federalButton = d3.select(document.getElementById("federalButton"));
          var stateButton = d3.select(document.getElementById("stateButton"));
          var localButton = d3.select(document.getElementById("localButton"));
          var federalTip = d3.select(document.getElementById("federalTip"));
          var stateTip = d3.select(document.getElementById("stateTip"));
          var localTip = d3.select(document.getElementById("localTip"));
          var drillDownLink = d3.select(document.getElementById("drillDownLink"));
          var descriptionTxt = d3.select(document.getElementById("tree-graph-node-desc"));

          var tackTooltip = d3.select(document.getElementById("tackTooltip"));
          tackTooltip[0][0]['onclick'] = function(event) {
            event.stopImmediatePropagation();
            tooltipIsTapped = !tooltipIsTapped;
            if (tooltipIsTapped) {
              $('#toolTip').addClass("tapped");
            }
            else {
              $('#toolTip').removeClass("tapped");
              toolTip.style("display", "none");
            }
          };
          var element = d3.select( $scope.elementName);

          var diagonal:any = d3.svg.diagonal()
              .projection(function (d) {
                return [d.y, d.x];
              });

          var mainSvg:any  = d3.select($element.find(".tree-chart")[0]).append("svg:svg")
              .attr("width", w + m[1] + m[3])
              .attr("height", h + m[0] + m[2])


          var svg = mainSvg.append("svg:g")
              .attr("transform", "translate(" + m[3] + "," + m[0] + ")")

          var levelCeil=[{},{},{},{},{},{},{},{}];


          var nodeRadius;

        //   d3.csv("../../mock-data/FederalBudget_2013.csv", function (csv) {
        //    var data = [];

        //Remove all zero values nodes
        //csv.forEach(function (d) {
        //      var t = 0;
        //      for (var i = 0; i < sumFields.length; i++) {
        //        t += Number(d[sumFields[i]]);
        //      }
        //      if (t > 0) {
        //        data.push(d);
        //      }
        //    })

          var nest = d3.nest()
              .key(function (d:any) {  //Group by Level1, Group by Level2 and Group by Level3
                return d.Level1;
              }).sortKeys(d3.ascending)
              .key(function (d:any) {
                return d.Level2;
              }) .sortKeys(d3.ascending)
              .key(function (d:any) {
                return d.Level3;
              }).sortKeys(d3.ascending)
              .key(function (d:any) {
                return d.Level4;
              }).sortKeys(d3.ascending)
              .key(function (d:any) {
                return d.Level5;
              }).sortKeys(d3.ascending)
            .key(function (d:any) {
              return d.Level6;
            }).sortKeys(d3.ascending)
            .key(function (d:any) {
              return d.Level7;
            }).sortKeys(d3.ascending)
            .key(function (d:any) {
              return d.Level8;
            }).sortKeys(d3.ascending)
              .entries(data);

            //console.log(JSON.stringify(data[0]));
            //console.log(JSON.stringify(data[1]));

            root = {};
            root.values = nest;
            root.x0 = h / 2;
            root.y0 = 0;

            var nodes = tree.nodes(root).reverse();

            tree.children(function (d) {
              return d.children;
            });

            initialize();

            // Initialize the display to show a few nodes.
            root.values.forEach(toggleAll);

       //Open all first level items with value larger then 40% of the largest item
        if(!isItemsToExpandInitialized()) {
          for (var i = 0; i < root.values.length; i++) {
            var maxOfDepth1 = levelCeil[1 - 1]["sum_" + sumFields[0]];
            var currentVal = root.values[i]['sum_' + sumFields[0]];
            if (currentVal / maxOfDepth1 > 0.4) {
              if (root.values[i]["source_Level2"] != "none") {
                toggleNodes(root.values[i],2);
              //expand second level
              for (var j = 0; j < root.values[i]["values"].length; j++) {
                var maxOfDepth2 = levelCeil[2 - 1]["sum_" + sumFields[0]];
                var currentVal2 = root.values[i].values[j]['sum_' + sumFields[0]];
                if (currentVal2 / maxOfDepth2 > 0.4) {
                  if (root.values[i].values[j]["source_Level3"] != "none") {
                      toggleNodes(root.values[i].values[j],3);
                    }
                  }
                }
              }
            }
          }
        }
        else {
          for (var i = 0; i < root.values.length; i++) {
            if (root.values[i]["source_Level2"] != "none") {
              if (root.values[i]["source_Level2"] != "none" && getItemsToExpandState(root.values[i], 2)) {
                toggleNodes(root.values[i]);
                //expand second level
                for (var j = 0; j < root.values[i]["values"].length; j++) {
                  if (root.values[i].values[j]["source_Level3"] != "none" && getItemsToExpandState(root.values[i].values[j], 3)) {
                    toggleNodes(root.values[i].values[j]);
                    for (var k = 0; k < root.values[i].values[j]["values"].length; k++) {
                      if (root.values[i].values[j].values[k]["source_Level4"] != "none" && getItemsToExpandState(root.values[i].values[j].values[k], 4)) {
                        toggleNodes(root.values[i].values[j].values[k]);
                      }
                    }
                  }
                }
              }
            }
          }
        }


            update(root);

            toggleButtons(0);

            function initialize() {

              federalButton.on("click", function (d) {
                toggleButtons(0);
                spendField = "sum_Federal";
                update(root);
              });

              stateButton.on("click", function (d) {
                toggleButtons(1);
                spendField = "sum_State";
                update(root);
              });

              localButton.on("click", function (d) {
                toggleButtons(2);
                spendField = "sum_Local";
                update(root);
              });

              $element.on("click", function (d) {
                //console.log('click');
                if (!tooltipIsTapped) {
                  toolTip.style("display", "none");
                }
              });

              for (var i = 0; i < sumFields.length; i++) {
                for (var y = 0; y < levelCeil.length; y++) {
                  levelCeil[y]["sum_" + sumFields[i]] = 0;
                }
              }

              traverseNodes(root.children);
            }

            function toggleAll(d) {
              //if (d.values && d.values.actuals) {
              //  d.values.actuals.forEach(toggleAll);
              //  toggleNodes(d);
              //}
              //else
              if (d.values) {
                d.values.forEach(toggleAll);
                toggleNodes(d);
              }
            }


       //   });

          function setSourceFields(child, parent) {
            if (parent) {
              for (var i = 0; i < sourceFields.length; i++) {
                var sourceField = sourceFields[i];
                if (child[sourceField] != undefined) {
                  child["source_" + sourceField] = child[sourceField];
                }
                parent["source_" + sourceField] = (child["source_" + sourceField]) ? child["source_" + sourceField] : child[sourceField];
              }
            }
          }

          function traverseNodes(nodes) {
            for (var y = 0; y < nodes.length; y++) {
              var node = nodes[y];
              if (node.children) {
                traverseNodes(node.children);
                for (var z = 0; z < node.children.length; z++) {
                  var child = node.children[z];
                  setSummary(node, child);
                  copyChild(node, child);
                }
              }
              else {
                setSummary(node, null);
              }
              setSourceFields(node, node.parent);
            }
          }

          function setSummary(node, child) {
            if(child) {
              for (var i = 0; i < sumFields.length; i++) {
                if (isNaN(node["sum_" + sumFields[i]])) {
                  node["sum_" + sumFields[i]] = 0;
                }
                var index = 'Level' + node.depth + 'Count';
                var indexAgg = 'Level' + node.depth + 'CountAgg';
                if (sumFields[i] == 'Federal'  ) {
                  node["sum_" + sumFields[i]] =child[indexAgg];
                  node["sum_State"] = child[index];
                  node["sum_Local"] += Number(child[index]);
                }
                if ((node.parent)) { //save max values
                  if(levelCeil[node.depth-1]["sum_" + sumFields[i]]< Number(node["sum_" + sumFields[i]]))
                  {
                    levelCeil[node.depth - 1]["sum_" + sumFields[i]] =  Number(node["sum_" + sumFields[i]]);
                  }
                  setSourceFields(node, node.parent);
                }
              }
            } else {
              for (var i = 0; i < sumFields.length; i++) {
                node["sum_" + sumFields[i]] = 0;//Number(node[sumFields[i]]);
                if (isNaN(node["sum_" + sumFields[i]])) {
                  node["sum_" + sumFields[i]] = 0;
                }
              }
            }
          }

          function copyChild(node, child) {
             let attrArr = ['Count', "ID", "CountAgg", "FileTagDtos", "drillDownLink", "StyleId", "Description"];

             for(var i=1; i<=8; i++) {
               _.forEach(attrArr, (attr) => {
                 node[`Level${i}${attr}`] = child[`Level${i}${attr}`];
               })
             }
          }

          function onMouseOut() {
            if (!tooltipIsTapped) {
              cancelTooltipProcess = setTimeout(function () {
                toolTip.style("display", "none");
              }, 1000);
            }
          }


          function onMouseMove() {
            if(toolTip && !tooltipIsTapped && (cancelTooltipProcess == null)) {
              var mLocation = d3.mouse(d3.select("svg").node());
              let currScroll = document.getElementsByClassName("tree-chart")[0]['scrollLeft'];
              toolTip.style("left", ((mLocation[0] + 50) - currScroll) + "px")
                .style("top", (mLocation[1] - 20) + "px");
            }
          }

          var cancelTooltipProcess = null;

          function update(source) {

            var duration = d3.event && d3.event['altKey'] ? 5000 : 500;

            var nodes = tree.nodes(root).reverse();

            var depthCounter = 0;

            nodeRadius = d3.scale.sqrt()
                .domain([0, levelCeil[0][spendField]])
                .range([1, 40]);

            // Normalize for fixed-depth.
            nodes.forEach(function (d:any) {
              d.y = d.depth * 180;
              d.numChildren = (d.children) ? d.children.length : 0;
              if (d.depth == 1) {
                d.linkColor = colors[(depthCounter % (colors.length - 1))];
                depthCounter++;
              }
              if (d.numChildren == 0 && d._children) d.numChildren = d._children.length;

            });

            //Set link colors based on parent color
            nodes.forEach(function (d:any) {
              var obj = d;
              while ((obj.source && obj.source.depth > 1) || obj.depth > 1) {
                obj = (obj.source) ? obj.source.parent : obj.parent;
              }
              d.linkColor = (obj.source) ? obj.source.linkColor : obj.linkColor;

            });

            // Update the nodes…
            var node = svg.selectAll("g.node")
                .data(nodes, function (d:any) {
                  return d.id || (d.id = ++i);
                });

            // Toggle children on click.
            function click(d) {
              if (d.children) {
                d._children = d.children;
                d.children = null;
              } else {
                d.children = d._children;
                d._children = null;
              }

              if (tooltipIsTapped) {
                node_onMouseOver(d,true);
              }
              fixTheSize();
              setTimeout(()=> {
                update(d);
              });
            }

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("svg:g")
                .attr("class", "node")
                .attr("id",function (d:any) { return "node_" + d.key })
                .attr("transform", function (d) {
                  return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", click);




            nodeEnter.append("svg:circle")
                .attr("r", 1e-6)
                .on("mousemove", function (d) {
                  onMouseMove();
                })
                .on("mouseover", function (d) {
                  node_onMouseOver(d,false);
                })
                .on("mouseout", function (d:any) { node_onMouseOut(d)})
                .style("fill", function (d:any) {
                  circles[d.key] = this;
                  return d.source ? d.source.linkColor : d.linkColor;
                })
                .style("fill-opacity", ".8")
                .style("stroke", function (d:any) {
                  return d.source ? d.source.linkColor : d.linkColor;
                });

            nodeEnter.append("svg:text")
                .attr("x", function (d:any) {
                  labels[d.key] = this;
                  return d.children || d._children ? -15 : 15;
                })
                .attr("dy", ".35em")
                .attr("text-anchor",
                    function (d:any) {
                      return d.children || d._children ? "end" : "start";
                    })
                .text(function (d:any) {
                  //var ret = (d.depth == 5) ? d.Level5 : d.key;
                  var ret = d.key;
                  ret = (String(ret).length > 25) ? String(ret).substr(0, 22) + "..." : ret;
                  return ret;
                })
                .style("fill-opacity", "0")
                .style("font-size","12")
                .on("mousemove", function (d) {onMouseMove();})
                .on("mouseover", function (d) {node_onMouseOver(d, false);})
                .on("mouseout", function (d) { node_onMouseOut(d)});

            toolTip.on("mouseover", function (d) {
              if (cancelTooltipProcess != null) {
                clearTimeout(cancelTooltipProcess);
                cancelTooltipProcess = null;
              }
       //           toolTip.style("background", "orange");
              toolTip.style("display", "block");
            });
            toolTip.on("mouseenter", function (d) {
              if (cancelTooltipProcess != null) {
                clearTimeout(cancelTooltipProcess);
                cancelTooltipProcess = null;
              }
              toolTip.style("opacity", "1")
                  .style("display", "block");
            });
            toolTip.on("mouseout", function (d) {
              if (!tooltipIsTapped) {
                toolTip.style("display", "none");
              }
            });

            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                  return "translate(" + d.y + "," + d.x + ")";
                });

            nodeUpdate.select("circle")
                .attr("stroke-width", '1.5px')
                .attr("stroke", '#ccc')
                .attr("r", function (d:any) {
                  return isNaN(nodeRadius(d[spendField])) ? 2: nodeRadius(d[spendField]);
                })
                .style("fill", function (d:any) { return d.source ? d.source.linkColor : d.linkColor })
                .style("fill-opacity", function (d:any) { return ((d.depth + 1) / 5);});

            nodeUpdate.select("text")
                .style("fill-opacity", 1)
                .style("opacity", 1)
                .style("font-size", '11px');

            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                  return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            nodeExit.select("circle").attr("r", 1e-6);

            nodeExit.select("text").style("fill-opacity", 1e-6);

            var link = svg.selectAll("path.link")
                .data(tree.links(nodes), function (d:any) {
                  return d.target.id;
                });

            var rootCounter = 0;

            // Enter any new links at the parent's previous position.
            link.enter().insert("svg:path", "g")
                .attr("class", "link")
                .attr("id",function (d:any) { return "link_" + d.target.key })
                .attr("d", function (d:any) {
                  paths[d.target.key] = this;
                  if (Number(d.target[spendField]) > 0) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal(<any>{source: o, target: o});
                  }
                  else {
                    null;
                  }
                })
                .style("stroke", function (d, i) {
                  if (d.source.depth == 0) {
                    rootCounter++;
                    return (d.source.children[rootCounter - 1].linkColor);
                  }
                  else {
                    return (d.source) ? d.source.linkColor : d.linkColor;
                  }
                })
                .style("stroke-width", function (d, i) { return isNaN(nodeRadius(d.target[spendField])) ? 4: nodeRadius(d.target[spendField])*2; })
                .style("stroke-opacity", function (d) {
                  return d.target[spendField] <= 0 ? .2 : ((d.source.depth + 2) / 6.5);
                })
                .style("stroke-linecap", "round")
//                .on("mouseover", function (d) {node_onMouseOver(d.source);})
//                .on("mouseout", function (d) { node_onMouseOut(d.source)});

            link.transition()
                .duration(duration)
                .attr("fill", "transparent")
                .attr("d", diagonal)
                .style("stroke-width", function (d, i) { return isNaN(nodeRadius(d.target[spendField])) ? 4: nodeRadius(d.target[spendField])*2; })
                .style("stroke-opacity", function (d) {
                  var ret = ((d.source.depth + 1) / 5.5);
                  if (ret < 0.2 || d.target[spendField] <= 0) ret = .2;
                  return ret;
                });

            link.exit().transition()
                .duration(duration)
                .attr("d", diagonal)
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d:any) {
              d.x0 = d.x;
              d.y0 = d.y;
            });

            function node_onMouseOver(d, affectTapped) {


              if (cancelTooltipProcess !== null)
              {
                clearTimeout(cancelTooltipProcess);
                cancelTooltipProcess = null;
              }

              if (!tooltipIsTapped || affectTapped) {


                var max_color = parseInt(configuration.max_tags_colors);
                var newText = '';
                var bigText = '';
                var fTagDtos = d["Level" + d.depth + "FileTagDtos"];
                var docTypeId = d["Level" + d.depth + "ID"];
                var description = d["Level" + d.depth + "Description"];

                if (fTagDtos) {
                  for (let count = 0; count < fTagDtos.length; count++) {
                    let tag = fTagDtos[count];
                    let tagColorId = 0;
                    if (tag.type.id) {
                      tagColorId = tag.type.id % max_color;
                      newText = newText + "<span class='tag-icon tag tag-" + tagColorId + "' bubble='" + tag.name + "' ><span class='" + configuration.icon_tag + "'></span></span>";
                      bigText = bigText + "<div class='big-tag-div ellipsis'  bubble='" + tag.type.name + "' > <span class='tag-icon tag tag-" + tagColorId + "' ><span class='" + configuration.icon_tag + "'></span></span> <span class='tag label label label-tag-" + tagColorId + "'> " + tag.name + "</span> </div>"
                    }
                  }
                }
                header5.html('<div class="small-detail">' + newText + '</div><div class="big-detail">' + bigText + '</div>');

                if (typeof d.target != "undefined") {
                  d = d.target;
                }
                if (d.depth == 0) {
                  return;
                }
                toolTip.style("display", "block");
                //     toolTip.style("opacity", ".6")
                //        .style("background", "green");

                toolTip.transition("toolTip")
                  .duration(500)
                  //        .style("background", "blue")
                  .style("opacity", ".9");
                header.html('<span style="padding-right: 5px" class="tag tag-' + d["Level" + d.depth + "StyleId"] + '  fa fa-bookmark"></span>' + d["source_Level1"]);
                if (d.depth == 1) {
                  $('#head').addClass("active");
                }
                else {
                  $('#head').removeClass("active");
                }

                toolTip.select('.header-expand-node').on("click", () => {
                  // expand(d);
                  // update(d);
                });

                if (d.depth > 1) {
                  $('#header2').addClass("active");
                  header2.html(d["source_Level" + d.depth]);
                }
                else {
                  header2.html('');
                  $('#header2').removeClass("active");
                }

                let fullPathStr = '';
                for (let count = 1; count <= d.depth; count++) {
                  if (fullPathStr != '') {
                    if (tooltipIsTapped) {
                      fullPathStr = fullPathStr + ' > ';
                    }
                    else {
                      fullPathStr = fullPathStr + '<br>';
                    }
                  }
                  fullPathStr = fullPathStr + d["source_Level" + count];
                }
                header.attr('title', fullPathStr);
                header2.attr('title', fullPathStr);

                drillDownLink.attr('href', d[`Level${d.depth}drillDownLink`]());
                fedSpend.text(formatCurrency(d["sum_Federal"]));

                //stateSpend.text(formatCurrency(d["sum_State"]));
                stateSpend.text(formatCurrency1(d["sum_Federal"], d["sum_State"]));

                localSpend.text(formatCurrency(d["sum_Local"]));

                descriptionTxt.text(d[`Level${d.depth}Description`]);
                $('#tree-graph-node-desc').toggleClass('currency', propertiesUtils.isStrCurrency(d[`Level${d.depth}Description`]))
             }

           //   console.log('d3.mouse(this)   '+d3.mouse(svg));
              d3.select(labels[d.key]).transition().style("font-weight","bold").style("font-size","16");
              d3.select(circles[d.key]).transition().style("fill-opacity",0.6);
              highlightPath(d);

              function highlightPath(d) {
                if (d) {
                  d3.select(paths[d.key]).style("stroke-opacity",function (d) {
                    return d.target[spendField] <= 0 ? (.1 + .3) : ((d.source.depth + 2) / 6.5) + .3;
                  });
                  highlightPath(d.parent);
                }
              }
            }

            function node_onMouseOut(d) {
              (<any>toolTip).transition(toolTip)
                  .duration(2500)
               //   .style("opacity", "0")
           //       .style("background", "yellow")
                  .transition(toolTip)
                  .duration(25)
                  .style("opacity", "1")
             //     .style("background", "pink")

              d3.select(labels[d.key]).transition().style("font-weight","normal").style("font-size","12");
              d3.select(circles[d.key]).transition().style("fill-opacity",0.3);
              noHighlightPath(d);

              function noHighlightPath(d) {
                if (d) {
                  d3.select(paths[d.key]).style("stroke-opacity",function (d) {
                    return d.target[spendField] <= 0 ? .2 : ((d.source.depth + 2) / 6.5);
                  });
                  noHighlightPath(d.parent);
                }
              }

              onMouseOut();
            }
          }


        function fixTheSize(){
          var nodes = tree.nodes(root).reverse();
          let theHeight = 600;
          let nodesNum =  0;
          for (var y = 0; y < nodes.length; y++) {
            var node = nodes[y];
            if (!node.children) {
              nodesNum++;
            }
          }
          theHeight = Math.max(nodesNum*30,theHeight);
          theHeight = Math.min(10000,theHeight);
          mainSvg.attr("height", theHeight+100);
          tree.size([theHeight, w]);
        }


        function expand(d){
          var children = (d.children)?d.children:d._children;
          if (d._children) {
            d.children = d._children.filter(childItem => childItem.key != 'none');
            d._children = null;
          }
          if(children)
            children.forEach(expand);
        }

        function expandRoot(){
          var children = (root.children)?root.children:root._children;
          if (root._children) {
            root.children = root._children;
            root._children = null;
          }
         }

        function collapse(d) {
          if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
          }
        }

        function setItemsToExpandState(node:any, depth:number, state:boolean) {
          if (depth) {
            $scope.itemsToExpand['initialized'] = true;
            $scope.itemsToExpand[node['Level' + depth + 'ID']] = state;
          }
        }
        function getItemsToExpandState(node:any, depth:number):boolean {
          return (depth && $scope.itemsToExpand[node['Level' + depth + 'ID']]);
        }

        function isItemsToExpandInitialized():boolean {
          return $scope.itemsToExpand && $scope.itemsToExpand['initialized'];
        }

        function toggleNodes(d,depth?) {
            if(!d)
            {
              return null;
            }
            if (d.children) {
              setItemsToExpandState(d,depth,false);
              d._children = d.children;
              d.children = null;
            } else {
              setItemsToExpandState(d,depth,true);
              d.children = d._children;
              d._children = null;
            }
          }

        function toggleButtons(index) {
            d3.selectAll(".button").attr("class",function (d,i) { return (i==index) ? "button selected" : "button"; });
            d3.selectAll(".tip").attr("class",function (d,i) { return (i==index) ? "tip selected" : "tip";});
          }

        function expandAll(){
          /*
          var element = d3.select( $scope.elementName);
          $element.find("svg").attr("height",10000);
          tree.children(function (d:any) { return d.values; }).size([10000, 3000]);
          setTimeout(()=> { */
            expand(root);
            fixTheSize();
            setTimeout(()=> {
              update(root);
            });
          /* }); */
        }

        function collapseAll(){
          root.children.forEach(collapse);
          collapse(root);
          expandRoot();
          fixTheSize();
          setTimeout(()=> {
            update(root);
          });
          /*
          setTimeout(()=>{
            var element = d3.select( $scope.elementName);
            $element.find("svg").attr("height",900);
            myTree.size([900, 3000]);
            tree.children(function (d:any) { return d.values; }).size([900, 3000]);
          },200);
          */
        }

        return {
          expandAll:expandAll,
          collapseAll:collapseAll,

        }
      }
    })
    .directive('treeViewGraph',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/common/directives/dashBoardWidgets/treeViewGraph.tpl.html',
            controller: 'treeViewGraphCtrl',

            replace:true,
            scope:{
              'histogramData':'=',
              'exportToExcelData':'=',
              'exportToExcelFilter':'=',
              'exportToExcelDataFields':'=',
              'chartType':'=',
              'chartTitle':'=',
              'saveAsPdf':'=',
              'saveAsImage':'=',
              'collapseAll':'=',
              'expandAll':'=',
              'saveAsExcel':'=',
              'hideTitle':'=',
              'myHeight':'@',
              'itemsToExpand':'=',
            },
            link: function (scope:any, element, attrs) {
            }
          };
        }
    )
    .directive( 'd3CircleChart', [
      function () {
        return {
          template:' <svg viewBox="0 0 1524 768" preserveAspectRatio="xMidyMid">' +
          '<g d3 d3-data="circles" d3-renderer="circleRenderer" ></g>' +
          '</svg>',
          scope: {
            'data': '=',
            'elementName':'=',
            'marginTop':'=',
            'height':'=',
            'chartOptions':'=',
          },
          controller:function($scope) {
            var width = 700;
            var height = 650;
            var maxLabel = 150;

            $scope.circles = [];
            $scope.circleRenderer = function(el, data) {
              // Set the data for some circles
              var d = el.selectAll('circle').data($scope.circles);
              d.enter()
                  .append('circle')
                  .attr('cx', 1024 / 2)
                  .attr('cy', 768 / 2)
                  .style('fill', function() {
                    return 'rgb(' + Math.floor(Math.random() * 255) + ','
                        + Math.floor(Math.random() * 255) + ','
                        + Math.floor(Math.random() * 255) + ')';
                  })
                  .transition().duration(1000)
                  .attr('cx', function(d) {
                    return d.cx;
                  }).attr('cy', function(d) {
                return d.cy;
              }).attr('r', function(d) {
                return d.r;
              });
              d.exit()
                  .transition().duration(1000)
                  .attr('cx', 1024 / 2)
                  .attr('cy', 768 / 2)
                  .attr('r', 0)
                  .remove();
            };
            $scope.clearCircles = function() {
              $scope.circles = [];
            };
            $scope.addCircles = function() {
              for (var i = 0; i < 10; i++) {
                $scope.circles.push({
                  cx: Math.random() * 1024,
                  cy: Math.random() * 768,
                  r: Math.random() * 50
                });
              }
            };
            $scope.addCircles();
            $scope.addCircles();
            $scope.addCircles();
            $scope.addCircles();
          },

        };
      }
    ])
    .directive( 'd3BarsChart',  function () {
      return {
        scope: {
          'data': '=',
          'elementName': '=',
          'marginTop': '=',
          'height': '=',
          'chartOptions': '=',
        },
        controller: function ($scope, $element) {
          var chart = d3.select($element[0]);
          $scope.$watch(function () {return $scope.data; }, function (value) {
            createChart();
          });

          var createChart = function () {
            if ($scope.data && $scope.data.length > 0) {
              var counters=[];
              $scope.data.forEach(d=>counters.push(<Dashboards.DtoCounterReport>d.counter));
              var maxCounter =  Math.max.apply(null, counters);
              chart.append("div").attr("class", "chart")
                  .selectAll('div')
                  .data(counters).enter().append("div")
                  .transition().ease("elastic")
                  .style("width", function (d) {
                    //  return d + "%";
                    return d/maxCounter*50+ "%";
                    ;
                  })
                  .text(function (d) {
                    return d ;
                  });
            }


          };
        }
      }
    })
    .directive('d3', [
      function() {
        return {
          scope: {
            d3Data: '=',
            d3Renderer: '='
          },
          restrict: 'EAMC',
          link: function(scope, iElement, iAttrs) {
            var el = d3.select(iElement[0]);
            scope.render = function() {
              if (typeof(scope.d3Renderer) === 'function') {
                scope.d3Renderer(el, scope.d3Data);
              }
            };
            scope.$watch('d3Renderer', scope.render);
            scope.$watch('d3Data', scope.render, true);
          }
        };
      }
    ]);

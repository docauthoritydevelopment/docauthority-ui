///<reference path='../../../common/all.d.ts'/>
'use strict';
angular.module('directives.dashboardWidgets.listReport', [])
  .controller('listReportCtrl', function ($scope,$element,$filter,configuration,Logger:ILogger,$translate,$state,$stateParams,$timeout,propertiesUtils) {
    var log:ILog = Logger.getInstance('listReportCtrl');
    var counterFilterType=null;
    var counterFilterArgs=null;

    $scope.init=function()
    {
      if($scope.reportCounterFieldType ==EFieldTypes.type_dateTime.toString()) {
        counterFilterType ="date";
        counterFilterArgs=configuration.dateTimeFormat;;
      }
      else if($scope.reportCounterFieldType ==EFieldTypes.type_string.toString()) {
      }
      else
      {
        counterFilterType ='number';
      }

    };

    $scope.$watch(function () { return $scope.reportCounterData; }, function (value) {

    // console.log(' $scope.reportCounterData '+ JSON.stringify( $scope.reportCounterData));
    });

    $scope.$watch(function () { return $scope.stringListReport; }, function (value) {
    //  console.log(' $scope.stringListReport '+ JSON.stringify( $scope.stringListReport));
    });
    $scope.parseCounterText = function(value)
    {
      var val = value;
      var result='';
      if( $scope.reportCounterFieldType ==EFieldTypes.type_dateTime.toString())
      {
        if(val==0)
        {
          return '0';
        }
        result =$filter('asDate')(val);

      }

      if($scope.reportCounterFieldType ==EFieldTypes.type_string.toString())
      {
        result = val;
      }
      else if(val!=null){
        if(counterFilterArgs) {
          result = $filter(counterFilterType)(val, counterFilterArgs);
        }
        else
        {
          result = $filter(counterFilterType)(val);
        }
      }
      return result;
    }

  })
  .directive('listReport',
  function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/dashBoardWidgets/listReport.tpl.html',
      controller: 'listReportCtrl',

      replace:true,
      scope:{
        'reportCounterData':'=', //:Dashboards.DtoCounterReport[]
        'stringListReport':'=', //:Dashboards.DtoStringListReport[]
        'stringReport':'=', //:Dashboards.DtoStringReport[]
        'countReport':'=',
        'countCenterReport':'=',
        'caption':'=',
        'label':'=',
        'labelClass':'=',
        'myHeight': '=',
        'reportCounterFieldType':'=',
      },
      link: function (scope:any, element, attrs) {

      }
    };
  }
);

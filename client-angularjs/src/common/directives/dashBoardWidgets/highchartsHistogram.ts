///<reference path='../../../common/all.d.ts'/>
'use strict';

//declare var d3:any;
declare var Highcharts:any;

angular.module('directives.dashboardWidgets.highchartsHistogram',
    [
      'ui.builder.charts',
      'ui.builder.saveAsFile',
      'directives.dashboardWidgets.treeViewGraph',
      'ui.builder.highcharts'
    ])
  .controller('highchartsHistogramCtrl', function ($scope,$element,Logger:ILogger,$compile,$state,$stateParams,$timeout,propertiesUtils,
                                                   chartsBuilder:ui.IChartOptionsBuilder,saveFilesBuilder:ISaveFileBuilder, highChartsBuilder:ui.IChartOptionsBuilder) {
    var log:ILog = Logger.getInstance('highchartsHistogramCtrl');

    $scope.calculatedMarginTop = 0;
    $scope.elementName='da-chart';
    $scope.myChart = null;
    $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;

    var lastChartType =  null;
    var elementOptions:any;// ui.IChartHelper;
    $scope.init = function () {
    //  createChart();
    };


    var dTOCounter:Dashboards.DtoCounterReport = Dashboards.DtoCounterReport.Empty();

    var name:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.name),
      title: 'Label',
      type: EFieldTypes.type_string,
      displayed: true,
      isCategoryField:true,
      format:"N0"
    };

    var counter:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.counter),
      title: 'Count',
      type: EFieldTypes.type_number,
      displayed: true,
      isSeriesField:true,
      format:"N0",
      dir:"desc"
    };

    var description:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.description),
      title: 'Details',
      type: EFieldTypes.type_string,
      isToolTipField:true,
      displayed: true,

    };

    var fields:ui.IChartFieldDisplayProperties[] = [];
    fields.push(name);
    fields.push(counter);
    fields.push(description);


    $scope.$watch(function () { return $scope.showTotalCount; }, function (value) {
      createChart();
    });

   $scope.$watch(function () { return $scope.chartType; }, function (value) {
     createChart();
    });
   $scope.$watch(function () { return $scope.histogramData  }, function (value) {
      if($scope.histogramData ) {

        createChart();
        $scope.chartReady = true;
      }
    });

    $scope.$watch(function () { return $scope.chartTitle  }, function (value) {
      var chart = $element.find('.'+$scope.elementName);
      if(chart[0] && $scope.chartTitle && elementOptions) {
        elementOptions.exporting.chartOptions.title.text= $scope.chartTitle;
        $scope.myChart.update(elementOptions);
      }

    });


    $scope.saveAsPdf=function()
    {
      if($scope.myChart) {
        var fileName= $scope.chartTitle+' '+$scope.chartType+' chart';
        $scope.myChart.exportChartLocal({
          type: 'application/pdf',
          filename: fileName
        });
      }
   };

    $scope.saveAsImage=function()
    {
      if($scope.myChart) {
        var fileName= $scope.chartTitle+' '+$scope.chartType+' chart';
        $scope.myChart.exportChartLocal({
          filename: fileName
        });
      }

    };
    $scope.saveAsExcel = function() {
      if ($scope.chartType == 'pie' || $scope.chartType == 'bar') {
         saveFilesBuilder.saveAsExcelOneSheet(fields, $scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);
      }
      else {
        saveFilesBuilder.saveSeriesCategoriesChartAsExcel(fields, <ui.IStackedChartData>$scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);

      }
    };

    $scope.onSeriesHover = function(e)
    {

    };


    var createChart = function()
    {
      if ($scope.histogramData ) {
        log.debug('Histogram receives data length: '+$scope.histogramData.length);
        if($scope.chartType=='pie') {
          createHighChartPieChart();
        }
        else if($scope.chartType=='stacked100') {
          createHighChartStackedBarChart100();
        }
        else
        {
          createHighChartBarChart();
        }
        lastChartType = $scope.chartType;
      }
    };

    var createHighChartBarChart = function () {
       elementOptions   = (<any>highChartsBuilder).createBarChart($scope.histogramData,$scope.chartTitle,fields,$scope.showTotalCount);
      elementOptions.plotOptions.bar.events = {
        click: function (event) {
          if ($scope.onSeriesClick) {
            let clickFunc = $scope.onSeriesClick();
            if (clickFunc) {
              clickFunc($scope.histogramData[event.point.index]);
            }
          }
        }
      };

      elementOptions.plotOptions.bar.animation = ($scope.myChart == null || (lastChartType != $scope.chartType));
      elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
      $scope.myChart = new Highcharts.Chart(elementOptions);
    };

    var createHighChartPieChart = function () {
      var  seriesColors= ["#EB742D", "#954111", "#5A5A5A", "#243D6F", "#906A00", "#3D5F28", "#73A7D9", "#5292D0", "#235588"];
      elementOptions   = highChartsBuilder.createPie($scope.histogramData,$scope.chartTitle,fields,seriesColors);
      elementOptions.plotOptions.pie.events = {
        click: function (event) {
          if ($scope.onSeriesClick) {
            let clickFunc = $scope.onSeriesClick();
            if (clickFunc) {
              clickFunc($scope.histogramData[event.point.index]);
            }
          }
        }
      };
      elementOptions.plotOptions.pie.animation = ($scope.myChart == null || (lastChartType != $scope.chartType));
      elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
      $scope.myChart = new Highcharts.Chart(elementOptions);
    };

    var createHighChartStackedBarChart100  = function () {
      elementOptions   = highChartsBuilder.createStackedBar100Chart($scope.histogramData,$scope.chartTitle,fields);
      elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
      $scope.myChart = new Highcharts.Chart(elementOptions);
    };





  })
  .directive('highchartsHistogramChart',
  function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/dashBoardWidgets/highchartsHistogram.tpl.html',
      controller: 'highchartsHistogramCtrl',

      replace:true,
      scope:{
        'histogramData':'=',
        'onSeriesClick':'&',
        'chartType':'=',
        'categoryDateResolution':'=',
        'categoryMaxDateGroups':'=',
        'aggregateType':'=',
        'chartTitle':'=',
        'saveAsPdf':'=',
        'saveAsImage':'=',
        'saveAsExcel':'=',
        'hideTitle':'=',
        'includeOthers':'=',
        'seriesColors':'=',
        'forcedColors':'=',
        'showTotalCount':'=',

        'onChartCreated':'=',
        'disableAnimation':'=',
        'myHeight':'@',
        'isTrendChart':'@',
      },
      link: function (scope:any, element, attrs) {

      }
    };
  });


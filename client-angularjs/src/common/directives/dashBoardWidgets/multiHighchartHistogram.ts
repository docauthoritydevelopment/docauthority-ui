///<reference path='../../../common/all.d.ts'/>
'use strict';

//declare var d3:any;
declare var Highcharts:any;


angular.module('directives.dashboardWidgets.histogram.multi.highchart',
    [
      'ui.builder.charts',
      'ui.builder.charts.multi',
      'ui.builder.charts.multi.highchart',
      'ui.builder.saveAsFile',
      'directives.dashboardWidgets.treeViewGraph'
    ])
    .controller('multiHighchartHistogramCtrl', function ($scope,$element,Logger:ILogger,$compile,$state,configuration:IConfig,$stateParams,$timeout,propertiesUtils,saveFilesBuilder:ISaveFileBuilder,multiHighChartsBuilder) {
      var log:ILog = Logger.getInstance('multiHighchartHistogramCtrl');

      $scope.calculatedMarginTop = 0;
      $scope.elementName='da-chart';
      $scope.myChart = null;
      $scope.msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;

      var elementOptions:ui.IChartHelper;
      $scope.init = function () {
        $scope.isTrendChart =$scope.isTrendChart? propertiesUtils.parseBoolean($scope.isTrendChart):false;
      };


      var dTOCounter:Dashboards.DtoCounterReport = Dashboards.DtoCounterReport.Empty();
      var dTOItemCounter:Dashboards.DtoItemCounter<string> = Dashboards.DtoItemCounter.Empty();


      var count:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOItemCounter, dTOItemCounter.count),
        title: '#',
        type: EFieldTypes.type_number,
        displayed: true,
        isSeriesField:true,
        format:"N0",
        dir:"desc"
      };

      var description:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.description),
        title: 'description',
        type: EFieldTypes.type_string,
        isToolTipField:true,
        displayed: true,

      };
      var item:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOItemCounter, dTOItemCounter.item),
        title: 'Date time',
        isCategoryField:true,
        type: EFieldTypes.type_string,
        displayed: true,
        isValueField:true,

      };
      var timestamp:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: 'timestamp',
        title: 'Epoch start time',
        isCategoryDataField:true,
        type: EFieldTypes.type_number,
        displayed: true,
      };



      var fields:ui.IChartFieldDisplayProperties[] = [];
      fields.push(item);
      fields.push(timestamp);
      fields.push(count);
      fields.push(description);




      $scope.$watch(function () { return $scope.chartType; }, function (value) {
        createChart();
      });
      //$scope.$watch(function () { return $scope.categoryDateResolution; }, function (value) {
      //
      //  createChart();
      //});
      //$scope.$watch(function () { return $scope.aggregateType; }, function (value) {
      //
      //  createChart();
      //});

      $scope.$watch(function () { return $scope.histogramData  }, function (value) {
        if($scope.histogramData ) {

          createChart();
          $scope.chartReady = true;
        }
      });

      $scope.$watch(function () { return $scope.chartTitle  }, function (value) {
        var chart = $element.find('.'+$scope.elementName);
        if(chart[0] && $scope.chartTitle && elementOptions) {
          elementOptions.setTitle(chart,$scope.chartTitle);
        }

      });


      $scope.saveAsPdf=function()
      {
        if($scope.myChart) {
          var fileName= $scope.chartTitle+' '+$scope.chartType+' chart';
          $scope.myChart.exportChartLocal({
            type: 'application/pdf',
            filename: fileName
          });
        }
      };

      $scope.saveAsImage=function()
      {
        if($scope.myChart) {
          var fileName= $scope.chartTitle+' '+$scope.chartType+' chart';
          $scope.myChart.exportChartLocal({
            filename: fileName
          });
        }
      };

      $scope.saveAsExcel = function() {
          saveFilesBuilder.saveSeriesCategoriesChartAsExcel(fields, <ui.IStackedChartData>$scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);
      };

      $scope.onSeriesHover = function(e)
      {

      };


      var createChart = function()
      {
        if ($scope.histogramData ) {
          log.debug('Histogram receives data length: '+$scope.histogramData.length);

          if($scope.chartType=='stacked'||$scope.chartType==Dashboards.EChartDisplayType.TREND_BAR_STACKED) {
            createHighchartMultipleStackedBarChart();
          }

          else if($scope.chartType=='stackedHorizontal'||$scope.chartType==Dashboards.EChartDisplayType.BAR_HORIZONTAL) {
          }
          else if($scope.chartType=='stackedBars'||$scope.chartType==Dashboards.EChartDisplayType.TREND_BAR) {
            createHighchartMultipleBarChart();
          }
          else if($scope.chartType=='lines'||$scope.chartType==Dashboards.EChartDisplayType.TREND_LINE) {
            createHighChartMultipleLinesChart();
          }

        }
      };

      var createHighchartMultipleStackedBarChart = function(){
        let elementOptions   = multiHighChartsBuilder.createStackedBarChart($scope.histogramData, $scope.chartTitle);
        elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
        $scope.myChart = new Highcharts.Chart(elementOptions);
      }

      var createHighChartMultipleLinesChart = function () {
        let elementOptions   = multiHighChartsBuilder.createLineChart($scope.histogramData, $scope.chartTitle);
        elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
        $scope.myChart = new Highcharts.Chart(elementOptions);
      };

      var createHighchartMultipleBarChart = function() {
        let elementOptions   = multiHighChartsBuilder.createBarChart($scope.histogramData,$scope.chartTitle,fields);
        elementOptions.chart.renderTo = $element.find("."+$scope.elementName)[0];
        $scope.myChart = new Highcharts.Chart(elementOptions);
      }




      $scope.$on("kendoWidgetCreated", function (e) {
        if ($scope.onChartCreated) {
          $scope.onChartCreated(e);
        }
      });

      $scope.$watch(function () { return $scope.disableAnimation; }, function (value) {
        if(  $scope.mainOptions)
        {
          $scope.mainOptions.transitions =  !$scope.disableAnimation ;
          var chartData =  $scope.getChartObject()? $scope.getChartObject().refresh():null;
        }
      });

      $scope.getChartObject = function()
      {
        return $element.find("."+$scope.elementName).data("kendoChart");
      }
      var setSettings=function(elementOptions)
      {
        elementOptions.chartSettings.transitions =  !$scope.disableAnimation ;
        elementOptions.chartSettings.seriesClick= function(e) {
          var chartData =  $scope.getChartObject();
          $scope.onSeriesClick?$scope.onSeriesClick()(e,chartData):null;
        };
        elementOptions.chartSettings.legendItemClick= function(e) {
          var chartData =  $scope.getChartObject();
          $scope.onLegenedItemClick?$scope.onLegenedItemClick()(e,chartData):null;
        };
        $scope.mainOptions = elementOptions.chartSettings;
        //   var dataSource = elementOptions.chartSettings.dataSource;
        //  delete elementOptions.chartSettings.dataSource;
        //   $scope.dataSource= dataSource;
        $timeout(function()
        {

          //   $element.find("."+$scope.elementName).data("kendoChart").refresh();
        })

      }


    })
    .directive('multiHighchartHistogramChart',
        function () {
          return {
            // restrict: 'E',
            templateUrl: 'common/directives/dashBoardWidgets/histogram.tpl.html',
            controller: 'multiHighchartHistogramCtrl',

            replace:true,
            scope:{
              'histogramData':'=',
              'onSeriesClick':'&',
              'onLegenedItemClick':'&',
              'chartType':'=',
         //     'categoryDateResolution':'=',
         //     'categoryMaxDateGroups':'=',
         //     'aggregateType':'=',
              'chartTitle':'=',
              'saveAsPdf':'=',
              'saveAsImage':'=',
              'saveAsExcel':'=',
              'hideTitle':'=',
              'includeOthers':'=',
              'seriesColors':'=',
              'forcedColors':'=',
              'valueAxisType':'=',

              'onChartCreated':'=',
              'disableAnimation':'=',
              'myHeight':'@',
              'isTrendChart':'@',
              'getChartObject':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }


    )

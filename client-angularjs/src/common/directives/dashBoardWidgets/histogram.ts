///<reference path='../../../common/all.d.ts'/>
'use strict';

//declare var d3:any;

angular.module('directives.dashboardWidgets.histogram',
    [
      'ui.builder.charts',
      'ui.builder.saveAsFile',
      'directives.dashboardWidgets.treeViewGraph'
    ])
  .controller('histogramCtrl', function ($scope,$element,Logger:ILogger,$compile,$state,$stateParams,$timeout,propertiesUtils,chartsBuilder:ui.IChartOptionsBuilder,saveFilesBuilder:ISaveFileBuilder) {
    var log:ILog = Logger.getInstance('histogramCtrl');

    $scope.calculatedMarginTop = 0;
    $scope.elementName='da-chart';
    var elementOptions:ui.IChartHelper;
    $scope.init = function () {
    //  createChart();
    };


    var dTOCounter:Dashboards.DtoCounterReport = Dashboards.DtoCounterReport.Empty();

    var name:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.name),
      title: 'Label',
      type: EFieldTypes.type_string,
      displayed: true,
      isCategoryField:true,
      format:"N0"
    };

    var counter:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.counter),
      title: 'Count',
      type: EFieldTypes.type_number,
      displayed: true,
      isSeriesField:true,
      format:"N0",
      dir:"desc"
    };

    var description:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
      fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.description),
      title: 'Details',
      type: EFieldTypes.type_string,
      isToolTipField:true,
      displayed: true,

    };

    var fields:ui.IChartFieldDisplayProperties[] = [];
    fields.push(name);
    fields.push(counter);
    fields.push(description);

   $scope.$watch(function () { return $scope.chartType; }, function (value) {
     createChart();
    });
   $scope.$watch(function () { return $scope.histogramData  }, function (value) {
      if($scope.histogramData ) {

        createChart();
        $scope.chartReady = true;
      }
    });

    $scope.$watch(function () { return $scope.chartTitle  }, function (value) {
      var chart = $element.find('.'+$scope.elementName);
      if(chart[0] && $scope.chartTitle && elementOptions) {
        elementOptions.setTitle(chart,$scope.chartTitle);
      }

    });


    $scope.saveAsPdf=function()
    {
      var chart = $element.find('.'+$scope.elementName);
      if(chart[0]) {
        var fileName= $scope.chartTitle+' '+$scope.chartType+' chart.pdf';
        elementOptions.showTitle(chart,true);
        elementOptions.saveAsPdf(chart,fileName,function(){elementOptions.showTitle(chart,false);});

      }
   };

    $scope.saveAsImage=function()
    {
      var chart = $element.find('.'+$scope.elementName);
      if(chart[0]) {
        var fileName= $scope.chartTitle+' '+$scope.chartType+' chart.png';
        elementOptions.showTitle(chart,true);
        elementOptions.saveAsImage(chart,fileName,function(){elementOptions.showTitle(chart,false);});
     }

    };
    $scope.saveAsExcel = function() {
      if ($scope.chartType == 'pie' || $scope.chartType == 'bar') {
         saveFilesBuilder.saveAsExcelOneSheet(fields, $scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);
      }
      else {
        saveFilesBuilder.saveSeriesCategoriesChartAsExcel(fields, <ui.IStackedChartData>$scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);

      }
    };

    $scope.onSeriesHover = function(e)
    {

    };


    var createChart = function()
    {
      if ($scope.histogramData ) {
        log.debug('Histogram receives data length: '+$scope.histogramData.length);
        if($scope.chartType=='pie') {
          createPie();
        }
        else if($scope.chartType=='column') {
          createColumnChart();
        }
        else if($scope.chartType=='stacked') {
          createStackedBarChart();
        }
        else if($scope.chartType=='stacked100') {
          createStackedBarChart100();
        }
        else
        {
          createBarChart();
        }
      }
    };

    var createPie = function () {
      $scope.calculatedHeight= $scope.myHeight;
      var  seriesColors= ["#EB742D", "#954111", "#5A5A5A", "#243D6F", "#906A00", "#3D5F28", "#73A7D9", "#5292D0", "#235588"];
      elementOptions  = chartsBuilder.createPie($scope.histogramData,$scope.chartTitle,
          fields,seriesColors);
      elementOptions.chartSettings.seriesClick= function(e) {
        var chartData =  $element.find("."+$scope.elementName).data("kendoChart")
        $scope.onSeriesClick?$scope.onSeriesClick()(e,chartData):null;

        //$timeout(function()
        //{
        //  createPie();;
        //})



      };
      setSettings(elementOptions);

    };
    var createBarChart = function () {
      var calculatedHeight;
      var initialBarHeight = 40;
      var gap=0.2;
     if($scope.histogramData.length>=1)
      {
        calculatedHeight = initialBarHeight*$scope.histogramData.length+initialBarHeight*gap*($scope.histogramData.length+1);
        if(calculatedHeight>parseInt($scope.myHeight))
        {
          calculatedHeight =$scope.myHeight;
        }
        else
        {
          $scope.calculatedMarginTop = 20;
        }
      }
      $scope.calculatedHeight=$scope.histogramData.length==1&&calculatedHeight<75?75:calculatedHeight; //min 75 for one bar - fixes bug in save as image
      elementOptions  = chartsBuilder.createBarChart($scope.histogramData,$scope.chartTitle,
          fields,gap);
      elementOptions.chartSettings.seriesClick= function(e) {
        var chartData =  $element.find("."+$scope.elementName).data("kendoChart")
        $scope.onSeriesClick?$scope.onSeriesClick()(e,chartData):null;
       // chartData.refresh();
        //$timeout(function()
        //{
        //  createBarChart();;
        //})



      };
      setSettings(elementOptions);

    };
    var createColumnChart = function () {
      $scope.calculatedHeight= $scope.myHeight;
     elementOptions  = chartsBuilder.createColumnChart($scope.histogramData,$scope.chartTitle,
          fields,null);

      setSettings(elementOptions);
    };

    var createStackedBarChart = function () {
      /**
       *  Optional exteranl color settings

       //var seriesColors = ["#3D5F28", "#5292D0", "#235588", "#906A00", "#EB742D", "#954111", "#5A5A5A", "#243D6F", "#73A7D9"];
      var forcedColors = {
        "ERROR":"#F47521", "ANALYSED":"#1AB394", "SCANNED":"#AFD8ED", "INGESTED":"#2F4050",
        "Sensitive files": "#B52E31",
      };
       */

      if ($scope.seriesColors || $scope.forcedColors) {
        var scIndex = 0;
        for (var s in $scope.histogramData.series) {
          //log.debug("series name: " + $scope.histogramData.series[s].name);
          //$scope.histogramData.series[s].color = (forcedColors[$scope.histogramData.series[s].name])?
          //    forcedColors[$scope.histogramData.series[s].name]:
          //    seriesColors[s];
          var c = $scope.forcedColors[$scope.histogramData.series[s].name];
          if (c) {
            $scope.histogramData.series[s].color = c;
          }
          else if ($scope.seriesColors && $scope.seriesColors[scIndex]) {
            $scope.histogramData.series[s].color = $scope.seriesColors[scIndex++];
          }
        }
      }

      $scope.calculatedHeight= $scope.myHeight;
     elementOptions  = chartsBuilder.createStackedBarChart($scope.histogramData,$scope.chartTitle,
          fields,null);

      setSettings(elementOptions);

    };
    $scope.$on("kendoWidgetCreated", function (e) {
      if ($scope.onChartCreated) {
        $scope.onChartCreated(e);
      }
    });

    $scope.$watch(function () { return $scope.disableAnimation; }, function (value) {
      if(  $scope.mainOptions)
      {
        $scope.mainOptions.transitions =  !$scope.disableAnimation ;
        $element.find("."+$scope.elementName).data("kendoChart")? $element.find("."+$scope.elementName).data("kendoChart").refresh():null;
      }
    });

    var setSettings=function(elementOptions)
    {
      elementOptions.chartSettings.transitions =  !$scope.disableAnimation ;
      $scope.mainOptions = elementOptions.chartSettings;
      var dataSource = elementOptions.chartSettings.dataSource;
      delete elementOptions.chartSettings.dataSource;
      $scope.dataSource= dataSource;
      $timeout(function()
      {
        $element.find("."+$scope.elementName).data("kendoChart").refresh();
      })

    }
    var createStackedBarChart100 = function () {
      createStackedBarChart();
      // same as stacked chart with setting stack type to 100%
      $scope.mainOptions.seriesDefaults.stack = { type: '100%' };
    };

  })
  .directive('histogramChart',
  function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/dashBoardWidgets/histogram.tpl.html',
      controller: 'histogramCtrl',

      replace:true,
      scope:{
        'histogramData':'=',
        'onSeriesClick':'&',
        'chartType':'=',
        'categoryDateResolution':'=',
        'categoryMaxDateGroups':'=',
        'aggregateType':'=',
        'chartTitle':'=',
        'saveAsPdf':'=',
        'saveAsImage':'=',
        'saveAsExcel':'=',
        'hideTitle':'=',
        'includeOthers':'=',
        'seriesColors':'=',
        'forcedColors':'=',

        'onChartCreated':'=',
        'disableAnimation':'=',
        'myHeight':'@',
        'isTrendChart':'@',
      },
      link: function (scope:any, element, attrs) {

      }
    };
  }


)
 .directive('kendoHistogramChart',
    function () {
      return {
        // restrict: 'E',
        //template: " <div kendo-chart class='{{elementName}}' k-options='chartOptions' k-rebind='chartOptions'   k-data-source='dataSource' " +
        //" k-series-hover='onSeriesHover' ng-style='{height: height+\"px\",\"margin-top\":marginTop+\"px\"}'" +
        //" ></div>",
        template:' <div kendo-chart    k-options="chartOptions"    k-rebind="chartOptions"     k-data-source="dataSource"    k-series-hover1="onSeriesHover"     style="height: 250px;" ></div>',
        replace:true,
        scope:
        {
          'elementName':'=',
          'marginTop':'=',
          'height':'=',
          'chartOptions':'=',
          'dataSource':'=',
        }

      };
    }
 ) .directive( 'd3CircleChart', [
  function () {
    return {
      template:' <svg viewBox="0 0 1524 768" preserveAspectRatio="xMidyMid">' +
      '<g d3 d3-data="circles" d3-renderer="circleRenderer" ></g>' +
     '</svg>',
      scope: {
        'data': '=',
        'elementName':'=',
        'marginTop':'=',
        'height':'=',
        'chartOptions':'=',
      },
      controller:function($scope) {
        var width = 700;
        var height = 650;
        var maxLabel = 150;

        $scope.circles = [];
        $scope.circleRenderer = function(el, data) {
          // Set the data for some circles
          var d = el.selectAll('circle').data($scope.circles);
          d.enter()
              .append('circle')
              .attr('cx', 1024 / 2)
              .attr('cy', 768 / 2)
              .style('fill', function() {
                return 'rgb(' + Math.floor(Math.random() * 255) + ','
                    + Math.floor(Math.random() * 255) + ','
                    + Math.floor(Math.random() * 255) + ')';
              })
              .transition().duration(1000)
              .attr('cx', function(d) {
                return d.cx;
              }).attr('cy', function(d) {
            return d.cy;
          }).attr('r', function(d) {
            return d.r;
          });
          d.exit()
              .transition().duration(1000)
              .attr('cx', 1024 / 2)
              .attr('cy', 768 / 2)
              .attr('r', 0)
              .remove();
        };
        $scope.clearCircles = function() {
          $scope.circles = [];
        };
        $scope.addCircles = function() {
          for (var i = 0; i < 10; i++) {
            $scope.circles.push({
              cx: Math.random() * 1024,
              cy: Math.random() * 768,
              r: Math.random() * 50
            });
          }
        };
        $scope.addCircles();
        $scope.addCircles();
        $scope.addCircles();
        $scope.addCircles();
      },

    };
  }
])
    .directive( 'd3BarsChart',  function () {
      return {
        scope: {
          'data': '=',
          'elementName': '=',
          'marginTop': '=',
          'height': '=',
          'chartOptions': '=',
        },
        controller: function ($scope, $element) {
          var chart = d3.select($element[0]);
          $scope.$watch(function () {return $scope.data; }, function (value) {
            createChart();
          });

          var createChart = function () {
            if ($scope.data && $scope.data.length > 0) {
              var counters=[];
              $scope.data.forEach(d=>counters.push(<Dashboards.DtoCounterReport>d.counter));
             var maxCounter =  Math.max.apply(null, counters);
              chart.append("div").attr("class", "chart")
                  .selectAll('div')
                  .data(counters).enter().append("div")
                  .transition().ease("elastic")
                  .style("width", function (d) {
                  //  return d + "%";
                    return d/maxCounter*50+ "%";
                  ;
                  })
                  .text(function (d) {
                    return d ;
                  });
            }


          };
        }
      }
    })
    .directive('d3', [
  function() {
    return {
      scope: {
        d3Data: '=',
        d3Renderer: '='
      },
      restrict: 'EAMC',
      link: function(scope, iElement, iAttrs) {
        var el = d3.select(iElement[0]);
        scope.render = function() {
          if (typeof(scope.d3Renderer) === 'function') {
            scope.d3Renderer(el, scope.d3Data);
          }
        };
        scope.$watch('d3Renderer', scope.render);
        scope.$watch('d3Data', scope.render, true);
      }
    };
  }
]);

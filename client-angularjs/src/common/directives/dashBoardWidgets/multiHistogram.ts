///<reference path='../../../common/all.d.ts'/>
'use strict';

//declare var d3:any;

angular.module('directives.dashboardWidgets.histogram.multi',
    [
      'ui.builder.charts',
      'ui.builder.charts.multi',
      'ui.builder.saveAsFile',
      'directives.dashboardWidgets.treeViewGraph'
    ])
    .controller('multiHistogramCtrl', function ($scope,$element,Logger:ILogger,$compile,$state,configuration:IConfig,$stateParams,$timeout,propertiesUtils,
                                                multiChartsBuilder:ui.IChartOptionsBuilder,saveFilesBuilder:ISaveFileBuilder) {
      var log:ILog = Logger.getInstance('multiHistogramCtrl');

      $scope.calculatedMarginTop = 0;
      $scope.elementName='da-chart';
      var elementOptions:ui.IChartHelper;
      $scope.init = function () {
        $scope.isTrendChart =$scope.isTrendChart? propertiesUtils.parseBoolean($scope.isTrendChart):false;
      };


      var dTOCounter:Dashboards.DtoCounterReport = Dashboards.DtoCounterReport.Empty();
      var dTOItemCounter:Dashboards.DtoItemCounter<string> = Dashboards.DtoItemCounter.Empty();


      var count:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOItemCounter, dTOItemCounter.count),
        title: '#',
        type: EFieldTypes.type_number,
        displayed: true,
        isSeriesField:true,
        format:"N0",
        dir:"desc"
      };

      var description:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOCounter, dTOCounter.description),
        title: 'description',
        type: EFieldTypes.type_string,
        isToolTipField:true,
        displayed: true,

      };
      var item:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOItemCounter, dTOItemCounter.item),
        title: 'Date time',
        isCategoryField:true,
        type: EFieldTypes.type_string,
        displayed: true,
        isValueField:true,

      };
      var timestamp:ui.IChartFieldDisplayProperties = <ui.IChartFieldDisplayProperties>{
        fieldName: 'timestamp',
        title: 'Epoch start time',
        isCategoryDataField:true,
        type: EFieldTypes.type_number,
        displayed: true,
      };



      var fields:ui.IChartFieldDisplayProperties[] = [];
      fields.push(item);
      fields.push(timestamp);
      fields.push(count);
      fields.push(description);




      $scope.$watch(function () { return $scope.chartType; }, function (value) {
        createChart();
      });
      //$scope.$watch(function () { return $scope.categoryDateResolution; }, function (value) {
      //
      //  createChart();
      //});
      //$scope.$watch(function () { return $scope.aggregateType; }, function (value) {
      //
      //  createChart();
      //});

      $scope.$watch(function () { return $scope.histogramData  }, function (value) {
        if($scope.histogramData ) {

          createChart();
          $scope.chartReady = true;
        }
      });

      $scope.$watch(function () { return $scope.chartTitle  }, function (value) {
        var chart = $element.find('.'+$scope.elementName);
        if(chart[0] && $scope.chartTitle && elementOptions) {
          elementOptions.setTitle(chart,$scope.chartTitle);
        }

      });


      $scope.saveAsPdf=function()
      {
        var chart = $element.find('.'+$scope.elementName);
        if(chart[0]) {
          var fileName= $scope.chartTitle+' '+$scope.chartType+' chart.pdf';
          elementOptions.showTitle(chart,true);
          elementOptions.saveAsPdf(chart,fileName,function(){elementOptions.showTitle(chart,false);});

        }
      };

      $scope.saveAsImage=function()
      {
        var chart = $element.find('.'+$scope.elementName);
        if(chart[0]) {
          var fileName= $scope.chartTitle+' '+$scope.chartType+' chart.png';
          elementOptions.showTitle(chart,true);
          elementOptions.saveAsImage(chart,fileName,function(){elementOptions.showTitle(chart,false);});
        }

      };
      $scope.saveAsExcel = function() {
          saveFilesBuilder.saveSeriesCategoriesChartAsExcel(fields, <ui.IStackedChartData>$scope.histogramData,$scope.chartTitle+' '+$scope.chartType,$scope.chartTitle);
      };

      $scope.onSeriesHover = function(e)
      {

      };


      var createChart = function()
      {
        if ($scope.histogramData ) {
          log.debug('Histogram receives data length: '+$scope.histogramData.length);

          if($scope.chartType=='stacked'||$scope.chartType==Dashboards.EChartDisplayType.TREND_BAR_STACKED) {
            createStackedBarChart();
          }

          else if($scope.chartType=='stackedHorizontal'||$scope.chartType==Dashboards.EChartDisplayType.BAR_HORIZONTAL) {
            createStackedHorizontalBarChart();
          }
          else if($scope.chartType=='stackedBars'||$scope.chartType==Dashboards.EChartDisplayType.TREND_BAR) {
            createStackedMultipleBarChart();
          }
          else if($scope.chartType=='lines'||$scope.chartType==Dashboards.EChartDisplayType.TREND_LINE) {
            createStackedMultipleLinesChart();
          }

        }
      };

      var createStackedBarChart = function () {
        var elementOptions = createStackedBarChartSettings(true);
        elementOptions.chartSettings.seriesDefaults.type = 'column';
        elementOptions.chartSettings.seriesDefaults.labels.rotation = 0;
        elementOptions.chartSettings.seriesDefaults.labels.position = 'center';

        //elementOptions.chartSettings.legendItemClick= function(e) {
        //  setTotalLabel(e.sender, e.seriesIndex);
        //},
        // elementOptions.chartSettings.dataBound= function(e) {
        //  setTotalLabel(e.sender,null);
        //}
        setSettings(elementOptions);

      };
      var createStackedHorizontalBarChart = function () {
        var elementOptions = createStackedBarChartSettings(true)

        setSettings(elementOptions);
      };
      var createStackedMultipleBarChart = function () {
        var elementOptions = createStackedBarChartSettings(false)
        elementOptions.chartSettings.seriesDefaults.type = 'column';
        elementOptions.chartSettings.seriesDefaults.labels.rotation =90;
        elementOptions.chartSettings.seriesDefaults.labels.position = 'top';

        setSettings(elementOptions);
      };
      var createStackedMultipleLinesChart = function () {
        var elementOptions = createStackedBarChartSettings(false);
        elementOptions.chartSettings.seriesDefaults.type = 'line';
        elementOptions.chartSettings.seriesDefaults.style = 'smooth';
        elementOptions.chartSettings.seriesDefaults.labels.rotation =90;
        elementOptions.chartSettings.seriesDefaults.labels.position = 'top';

        setSettings(elementOptions);
      };
      var createStackedBarChartSettings = function (stack) {

        if ($scope.seriesColors || $scope.forcedColors) {
          var scIndex = 0;
          for (var s in $scope.histogramData.series) {
            //log.debug("series name: " + $scope.histogramData.series[s].name);
            //$scope.histogramData.series[s].color = (forcedColors[$scope.histogramData.series[s].name])?
            //    forcedColors[$scope.histogramData.series[s].name]:
            //    seriesColors[s];
            var c = $scope.forcedColors[$scope.histogramData.series[s].name];
            if (c) {
              $scope.histogramData.series[s].color = c;
            }
            else if ($scope.seriesColors && $scope.seriesColors[scIndex]) {
              $scope.histogramData.series[s].color = $scope.seriesColors[scIndex++];
            }
          }
        }

        var calculatedHeight;
        var initialBarWidth = 40;
        var gap=0.3;
        var spacing=0.01;
        if(stack)
        {

        }
        //if($scope.histogramData.length>=1)
        //{
        //  calculatedHeight = initialBarWidth*$scope.histogramData.categories.length*$scope.histogramData.series.length+initialBarHeight*spacing*($scope.histogramData.length+1);
        //  if(calculatedHeight>parseInt($scope.myHeight))
        //  {
        //    calculatedHeight =$scope.myHeight;
        //  }
        //  else
        //  {
        //    $scope.calculatedMarginTop = 20;
        //  }
        //}

        $scope.calculatedHeight= $scope.myHeight;
        elementOptions  = multiChartsBuilder.createStackedBarChart(<ui.IStackedChartData>$scope.histogramData,$scope.chartTitle,
           fields,null);

     //   elementOptions.chartSettings.categoryAxis.baseUnit = $scope.categoryDateResolution; //works only for x axis type date
      //  elementOptions.chartSettings.categoryAxis.maxDateGroups = $scope.categoryMaxDateGroups;
      //  elementOptions.chartSettings.seriesDefaults.aggregate = $scope.aggregateType; //works only for client dates presentation
      //  elementOptions.chartSettings.theme= "Silver";
        elementOptions.chartSettings.seriesColors = ["rgb(255, 141, 0)",   "#EB742D", "#954111", "#5A5A5A", "#243D6F", "#73A7D9"];
        elementOptions.chartSettings.seriesDefaults.gap = gap;
        elementOptions.chartSettings.seriesDefaults.spacing = spacing;
        elementOptions.chartSettings.seriesDefaults.stack = stack;
        elementOptions.chartSettings.seriesDefaults.labels.visible = false;
        elementOptions.chartSettings.seriesDefaults.missingValues = 'gap';
        elementOptions.chartSettings.valueAxis.title={text : propertiesUtils.capitalize($scope.valueAxisType)};
        elementOptions.chartSettings.categoryAxis.select=true;
        elementOptions.chartSettings.categoryAxis.labels.rotation ='auto';

        elementOptions.chartSettings.plotAreaClick= function(e) {
        //  elementOptions.chartSettings.seriesDefaults.labels.visible = ! elementOptions.chartSettings.seriesDefaults.labels.visible;
        };
        //elementOptions.chartSettings.categoryAxis.plotBands=
        //    [
        //      {from: 0, to:1, color: "#CCCCCC"},
        //      {from:1, to:2, color: "#D3F4A3"},
        //      {from:2, to:3, color: "#CCCCCC"},
        //    ]
        return elementOptions;
      };

      $scope.$on("kendoWidgetCreated", function (e) {
        if ($scope.onChartCreated) {
          $scope.onChartCreated(e);
        }
      });

      $scope.$watch(function () { return $scope.disableAnimation; }, function (value) {
        if(  $scope.mainOptions)
        {
          $scope.mainOptions.transitions =  !$scope.disableAnimation ;
          var chartData =  $scope.getChartObject()? $scope.getChartObject().refresh():null;
        }
      });

      $scope.getChartObject = function()
      {
        return $element.find("."+$scope.elementName).data("kendoChart");
      }
      var setSettings=function(elementOptions)
      {
        elementOptions.chartSettings.transitions =  !$scope.disableAnimation ;
        elementOptions.chartSettings.seriesClick= function(e) {
          var chartData =  $scope.getChartObject();
          $scope.onSeriesClick?$scope.onSeriesClick()(e,chartData):null;
        };
        elementOptions.chartSettings.legendItemClick= function(e) {
          var chartData =  $scope.getChartObject();
          $scope.onLegenedItemClick?$scope.onLegenedItemClick()(e,chartData):null;
        };
        $scope.mainOptions = elementOptions.chartSettings;
        //   var dataSource = elementOptions.chartSettings.dataSource;
        //  delete elementOptions.chartSettings.dataSource;
        //   $scope.dataSource= dataSource;
        $timeout(function()
        {

          //   $element.find("."+$scope.elementName).data("kendoChart").refresh();
        })

      }


    })
    .directive('multiHistogramChart',
        function () {
          return {
            // restrict: 'E',
            templateUrl: 'common/directives/dashBoardWidgets/histogram.tpl.html',
            controller: 'multiHistogramCtrl',

            replace:true,
            scope:{
              'histogramData':'=',
              'onSeriesClick':'&',
              'onLegenedItemClick':'&',
              'chartType':'=',
         //     'categoryDateResolution':'=',
         //     'categoryMaxDateGroups':'=',
         //     'aggregateType':'=',
              'chartTitle':'=',
              'saveAsPdf':'=',
              'saveAsImage':'=',
              'saveAsExcel':'=',
              'hideTitle':'=',
              'includeOthers':'=',
              'seriesColors':'=',
              'forcedColors':'=',
              'valueAxisType':'=',

              'onChartCreated':'=',
              'disableAnimation':'=',
              'myHeight':'@',
              'isTrendChart':'@',
              'getChartObject':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }


    )

///<reference path='../../../common/all.d.ts'/>
'use strict';
angular.module('directives.dashboardWidgets.multiListReport', [])
  .controller('multiListReportCtrl', function ($scope,$element,Logger:ILogger,$translate,$state,$stateParams,$timeout,propertiesUtils) {
    var log:ILog = Logger.getInstance('multiListReportCtrl');


  })
  .directive('multiListReport',
  function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/dashBoardWidgets/multiListReport.tpl.html',
      controller: 'multiListReportCtrl',

      replace:true,
      scope:{
        'reportData':'=', //:Dashboards.DtoMultiValueList
      },
      link: function (scope:any, element, attrs) {
      }
    };
  }
);

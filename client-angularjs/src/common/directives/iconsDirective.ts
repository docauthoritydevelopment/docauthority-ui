///<reference path='../all.d.ts'/>


'use strict';
angular.module('directives.icons', [

  ])

  .directive('daIcon',
    function ($compile,configuration:IConfig) {
      return {
        template:"<span  class='da-i'></span>",
        replace: true,
        scope: {},
        link: function(scope, element, attrs:any) {
          var config={edit:'fa fa-pencil',
                      import:'fa fa-upload',
                      add:'fa fa-plus',
                      extension : 'fa fa-file-o',
                      fileTypeCategory : 'fa fa-object-group',
                      fileSize : 'fa fa-shopping-basket',
                      delete:'fa fa-trash-o',
                      export:'fa fa-download',
                      zip: 'fa fa-file-archive-o',
                      email: 'fa fa-envelope-o',
                      pst: 'fa fa-envelope-o.duplicate',
                      folder:configuration.icon_folder,
                   //   folder:'__folder',
                      group:configuration.icon_group,
                      group1:'__group',
                      owner:'fa fa-user',
                      duplication : 'fa fa-files-o',
                      searchPatterns:configuration.icon_searchPattern,
                      labels:'fa fa fa-tag',
                      regulations : 'fa fa-book',
                      sharePermissions : 'fa fa-shield',
                      patternCategories:'fa fa-cubes',
                      scheduleGroup:configuration.icon_scheduleGroup,
                      ldapConnection:configuration.icon_ldap_user,
                      mediaConnection:configuration.icon_mediaTypeConnection,
                      docTypes:configuration.icon_docType,
                      tagType:configuration.icon_tagType,
                      dateFilters: configuration.icon_dateFilter,
                      department: configuration.icon_department,
                      matter: configuration.icon_matter,
                      rootfolder:configuration.icon_rootfolder,
                      rootfolder1:'__folder-check',
                      check:'fa fa-check',
                      dataCenter:'fa fa-university',
                      funcRole:configuration.icon_functionalRole,
                      bizRole:configuration.icon_bizRole,
                      templateRole:configuration.icon_templateRole,
                      user:configuration.icon_user,
                      oneDrive:'__oneDrive',
                      box:'__Logo_Box',
                      logLevels: configuration.icon_logLevels,
                      filters: 'fa fa-filter',
                      views: 'fa fa-image',
                      charts: 'fa fa-bar-chart',
                      dashboards: 'fa fa-television',
                      widgets: 'fa fa-th-large'
          };
          var iconName = config[attrs.href]?config[attrs.href]:attrs.href;
          var obj = element;
          if(iconName &&_.endsWith(iconName.toLowerCase(), '.svg')) { //svg file
            let html = "<span ng-include='\"/assets/svg/" + iconName + "\"' style='display: inline-block;'></span>";
            var newElem2 = $compile(html)(scope);
            obj.append(newElem2);
          }
          else if(iconName && _.startsWith(iconName, '__')){ //svg sprite

            let html = '<svg> \
                        <use xlink:href="/assets/svg/sprites/svg-defs.svg'+ iconName.replace('__','#')+'"></use> \
                     </svg>'
            obj.html(html);
          }
          else if(iconName && _.endsWith(iconName, '.showText')){ //png file
            let cutItemName =  iconName.substring(0,iconName.lastIndexOf('.showText'));
            let theText = cutItemName.substring(cutItemName.lastIndexOf('.')+1);
            let theIcon = cutItemName.substring(0,cutItemName.lastIndexOf('.'));
            obj.addClass(theIcon);
            let html = '<span class="icon-with-text" >'+theText+'</span>';
            obj.html(html);
          }
          else if(iconName && _.endsWith(iconName.toLowerCase(), '.duplicate')){ //png file
            let cutItemName =  iconName.substring(0,iconName.indexOf('.duplicate'));
            obj.addClass(cutItemName);
            obj.addClass('icon-with-duplications-main-icon');

            if (_.endsWith(cutItemName,"-o")) {
             let fullIconName =  cutItemName.substring(0,cutItemName.lastIndexOf('-o'));
              let html = '<span class="icon-with-duplications ' + fullIconName + '" style="color:white"><span class="icon-with-duplications-inner-icon '+ cutItemName + '" ></span></span>';
              obj.html(html);
            }
            else {
              let html = '<span class="icon-with-duplications ' + cutItemName + '" ></span>';
              obj.html(html);
            }
          }
          else if(iconName && _.endsWith(iconName.toLowerCase(), '.png')){ //png file
            let html = '<img class="da-i fill-width fill-height ' +(attrs.class?attrs.class:'')+'"   \
                       src="/assets/images/'+ iconName+'"> '
            obj.html(html);
          }
          else { //class
           // let html = "<span class='da-i " + iconName + "'></span>";
            obj.addClass(iconName);
          }

        }

      }
    }
  );

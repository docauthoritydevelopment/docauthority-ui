///<reference path='../all.d.ts'/>

'use strict';
angular.module('directives.date.RelativeAbsoluteSelector', [

  ])
  .controller('dateRelativeAbsoluteSelectorCtrl', function ($scope,$element,Logger:ILogger,configuration:IConfig) {
    var log:ILog = Logger.getInstance('dateRelativeAbsoluteSelectorCtrl');
    $scope.relativeTimePeriodOptions=[Entities.ETimePeriod.YEARS,Entities.ETimePeriod.MONTHS,Entities.ETimePeriod.WEEKS,Entities.ETimePeriod.DAYS,Entities.ETimePeriod.HOURS];
    $scope.dateFormat = configuration.inputDateFormat;
    $scope.titleTimeFormat =  configuration.dateFormat;
    $scope.init=function()
    {
      if( $scope.dateRangePoint ) {
        $scope.absoluteDate = $scope.isAbsolute() ? new Date( (<Entities.DTODateRangePoint>$scope.dateRangePoint).absoluteTime):null;
      }
      else {
        var point = new Entities.DTODateRangePoint();
        point.type = Entities.EDateRangePointType.RELATIVE;
        point.relativePeriod = Entities.ETimePeriod.MONTHS;
        point.relativePeriodAmount = 1;
        $scope.dateRangePoint = point;
      }
    }


    $scope.changeRelativePeriod = function(period: Entities.ETimePeriod,relativePeriodAmount)
    {
      (<Entities.DTODateRangePoint> $scope.dateRangePoint).relativePeriod =period;
  //    (<Entities.DTODateRangePoint> $scope.dateRangePoint).relativePeriodAmount =relativePeriodAmount;
       validateRelativeDates();
      $scope.onPointChange($scope.dateRangeItem, $scope.dateRangePoint);
    }


    $scope.relativeStepChanged = function()
    {
      validateRelativeDates();
      $scope.onPointChange($scope.dateRangeItem, $scope.dateRangePoint);
    }

    $scope.$watch(function () { return $scope.myForm.$valid; },  (value) => {
      $scope.onValidityChange? $scope.onValidityChange($scope.dateRangeItem.$$hashKey, $scope.myForm.$valid):null;
    });

    var validateRelativeDates=function()
    {
      $scope.myForm.$setValidity('relativePeriod',  (<Entities.DTODateRangePoint> $scope.dateRangePoint).relativePeriod!=null);
    }

    $scope.absoluteDateChanged=function()
    {
      if($scope.absoluteDate) {
        var startTimeAsUtc = new Date(Date.UTC($scope.absoluteDate.getFullYear(), $scope.absoluteDate.getMonth(), $scope.absoluteDate.getDate(), 0, 0, 0));
        (<Entities.DTODateRangePoint>$scope.dateRangePoint).absoluteTime = startTimeAsUtc.getTime();

      }
      else {
        (<Entities.DTODateRangePoint>$scope.dateRangePoint).absoluteTime = null;
      }
      $scope.onPointChange($scope.dateRangeItem, $scope.dateRangePoint);
    }

    $scope.toggleIsAbsolute = function() {
      if ($scope.isAbsolute()) {
        (<Entities.DTODateRangePoint>$scope.dateRangePoint).type = Entities.EDateRangePointType.RELATIVE;
      }
      else {
        (<Entities.DTODateRangePoint>$scope.dateRangePoint).type = Entities.EDateRangePointType.ABSOLUTE;
      }
    }
    $scope.isAbsolute = function()
    {
        return    (<Entities.DTODateRangePoint>$scope.dateRangePoint).type==Entities.EDateRangePointType.ABSOLUTE;
    }

    $scope.setEditTitleMode=function(value:boolean)
    {
      if(value)
      {
        $scope.newTitle=  $scope.itemTitle;
      }
      $scope.editTitleMode=value;

    }
    $scope.cancelEditTitle=function()
    {
      $scope.newTitle=null;
      $scope.setEditTitleMode(false);
    }
    $scope.editTitle=function()
    {
      $scope.itemTitle=$scope.newTitle;
      $scope.onPointTitleChange($scope.dateRangeItem,$scope.newTitle);
      $scope.setEditTitleMode(false);
    }
    $scope.openAbsolute = function() {
      $scope.absoluteopened = true;
    };


  })
  .directive('dateRelativeAbsoluteSelector',
    function () {
      return {
        templateUrl: 'common/directives/dateRelativeAbsoluteSelector.tpl.html',
        replace:true,
        transclude:true,
        scope:{
          'dateRangeItem':'=',
          'dateRangePoint':'=',
          'editTitleMode':'=',
          'itemTitle':'=',
          'onDeletePoint':'=',
          'onPointChange':'=',
          'onPointTitleChange':'=',
          'onValidityChange':'=',
          'disableTitleEditMode':'='
        },
        controller: 'dateRelativeAbsoluteSelectorCtrl',
        link: function (scope:any, element, attrs) {
          scope.init();
        }
      };
    }
  )
;

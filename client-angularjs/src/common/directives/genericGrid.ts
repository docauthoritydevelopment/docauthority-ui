///<reference path='../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('directives.grid',[

]);

angular.module('directives.grid')

    .controller('GenericGridCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,
                                        propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,userSettings) {
      var log: ILog = Logger.getInstance('GenericGridCtrl');

      var gridOptions;

      $scope.elementName='generic-grid';
      var id,name;

      $scope.init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;
        var key:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: 'key',
          title: 'key',
          type: EFieldTypes.type_string,
          displayed: false,
          isPrimaryKey:true,
          editable: false,
          nullable: true,


        };
        var value:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: 'value',
          title: 'Value',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,


        };
        var fields:ui.IFieldDisplayProperties[] =[];
        fields.push(key);
        fields.push(value);

        $scope.fields=fields;

        id = $scope.fields.filter(f=>f.fieldName=='id')[0];
        name = $scope.fields.filter(f=>f.fieldName=='item.id')[0]?$scope.fields.filter(f=>f.fieldName=='item.id')[0]:$scope.fields.filter(f=>f.fieldName=='name')[0];

          gridOptions = GridBehaviorHelper.createGrid<Entities.DTOFileInfo>($scope,log,configuration,eventCommunicator,userSettings,null,$scope.fields,
              $scope.parseData,$scope.rowTemplate,null,null,$scope.getDataUrlBuilder,true,$scope.fixHeight);

        GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,gridOptions ,null,$scope.fields,onSelectedItemChanged,$element);

        gridOptions.gridSettings.pageable=false;
        gridOptions.gridSettings.dataSource.data=$scope.data;
        $scope.mainGridOptions1 = gridOptions.gridSettings;

      };
      $scope.$watch(function () { return $scope.data; }, function (value) {
        (<ui.IGridHelper >gridOptions).setData($scope.element,$scope.data);
      });


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected($scope.selectedItem[name.fieldName],$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }
      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


    })
    .directive('genericGrid', function($timeout){
      return {
        restrict: 'EA',
        template:  '<div class="fill-height {{elementName}}"  kendo-grid ng-transclude k-on-data-binding="dataBinding(e,r)"  k-on-change="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions1" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          itemSelected: '=',
          unselectAll: '=',
          itemsSelected: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          fields1: '=',
          getDataUrlBuilder: '=',
          rowTemplate: '=',
          parseData: '=',
          data: '=',

        },
        controller: 'GenericGridCtrl',
        link: function (scope:any, element, attrs) {
          scope.init(attrs.restrictedentitydisplaytypename);
        }

      }

    });

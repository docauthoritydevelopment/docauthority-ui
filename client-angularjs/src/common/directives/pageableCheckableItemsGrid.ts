///<reference path='../../common/all.d.ts'/>
'use strict';

angular.module('directives.pageable.items.grid',[
    ])

    .controller('pageableCheckableItemsGridCtrl', function
        ($scope, $stateParams, $timeout,$location,$window, $filter,Logger:ILogger, propertiesUtils, configuration:IConfig,$element,bizListsResource,
         userSettings,eventCommunicator:IEventCommunicator) {
      var _this = this;
      var log: ILog = Logger.getInstance('pageableCheckableItemsGridCtrl');
      var fields:ui.IFieldDisplayProperties[];
      var id,name;

      var rowTemplate='';

      $scope.Init = function(restrictedEntityDisplayTypeName)
      {
        $scope.restrictedEntityDisplayTypeName =restrictedEntityDisplayTypeName;

        fields  = createGenericObjectValuesFields($scope,$scope.onCheckboxChanged);
        id = fields.filter(f=>f.fieldName=='id')[0];
        name = fields.filter(f=>f.fieldName=='item.id')[0];

        $scope.lazyFirstLoad = false;

        _this.gridOptions  = GridBehaviorHelper.createGrid<Policy.DtoPolicyObjectItemBase>($scope,log,configuration,eventCommunicator,userSettings,null,fields, $scope.getDataUrlBuilder?parseData:null,rowTemplate,null,null,$scope.getDataUrlBuilder);


        GridBehaviorHelper.connect($scope,$timeout,$window, log,configuration,_this.gridOptions ,rowTemplate,fields,onSelectedItemChanged,$element,null);
    //    $scope.mainGridOptions = _this.gridOptions.gridSettings;

        if($scope.loadData) {
          _this.gridOptions.gridSettings.dataSource.serverPaging=false;
          _this.gridOptions.gridSettings.dataSource.schema.total= function(response) {
            return response.length; // total is returned in the "total" field of the response
          }
          _this.gridOptions.reloadData=function(gridElement:any) {
            var gridData = gridElement.data("kendoGrid");
            if (gridData) {

              $scope.loadData(_this.gridOptions,$element);
            }

          }

          $scope.loadData(_this.gridOptions,$element);
        }
      };

      $scope.openEditDialog = function(dataItem:any,triggeredFieldName) {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridPoliciesUserAction, {
          action: 'openEditPolicyObjectItemDialog',
          policyItem: dataItem,
          triggeredFieldName: triggeredFieldName,
        });
      };



      var parseData = function (objects:any[]) {
        if(objects) {
          objects = objects.map(o=> {
            return {active: true, value: o.item}
          });
        }
        return objects;
      };


      var onSelectedItemChanged = function()
      {
        $scope.itemSelected(id?$scope.selectedItem[id.fieldName]:null,$scope.selectedItem[name.fieldName],$scope.selectedItem);
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var createGenericObjectValuesFields = function(scope,onCheckboxChanged){

        var paramActiveItem =  ParamActiveItem.Empty();
        var paramOption =  ParamOption.Empty();
        var prefixFieldName = propertiesUtils.propName(paramActiveItem, paramActiveItem.item) + '.';

        var display:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName+propertiesUtils.propName(paramOption, paramOption.display),
          title: 'Name',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,


        };
        var subdisplay:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName+propertiesUtils.propName(paramOption, paramOption.subdisplay),
          title: 'Description',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,


        };
        var id:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName: prefixFieldName+ propertiesUtils.propName(paramOption, paramOption.id),
          title: 'ID',
          type: EFieldTypes.type_string,
          displayed: true,
          editable: false,
          nullable: true,
          //template:'<a ng-click="infoClicked(dataItem)">#: item #' +
          //'<span ng-show="dataItem.active" class="fa fa-checked"></span><span ng-show="dataItem.active" class="fa fa-square-o"></span>'+
          //' </a>',

        };
        var active:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
          fieldName:  propertiesUtils.propName(paramActiveItem, paramActiveItem.active),
          title: 'Active',
          type: EFieldTypes.type_boolean,
          displayed: true,
          editable: true,
          nullable: true,
          template: '<input ng-checked="dataItem.active"  ng-show1="dataItem.active" type="checkbox" ng-click="infoClicked(dataItem)" />',
          //  template:'<a ng-click="infoClicked(dataItem)">'+
          //  '<span ng-show="dataItem.active" class="fa fa-checked"></span><span ng-show="dataItem.active" class="fa fa-square-o"></span>'+
          //  ' </a>',
          width:'25px'
        };

        scope.infoClicked = function(dataItem){
          if(dataItem)
          {
            dataItem.active=!dataItem.active;
            onCheckboxChanged(scope.restrictedEntityId,dataItem);

          }
        };

        var fields:ui.IFieldDisplayProperties[] =[];

        fields.push(active);
        fields.push(id);
        fields.push(display);
        fields.push(subdisplay);

        return fields;
      };

    })
    .directive('pageableCheckableItemsGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div class="fill-height {{elementName}}"    k-on-change="handleSelectionChange(data, dataItem, columns)" kendo-grid ng-transclude k-on-data-bound="onDataBound()" k-on-data-binding="dataBinding(e,r)" k-options="mainGridOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {

          lazyFirstLoad: '=',
          restrictedEntityId:'=',
          restrictedEntityItem:'=',
          itemSelected: '=',
          itemsSelected: '=',
          unselectAll: '=',
          filterData: '=',
          sortData: '=',
          exportToPdf: '=',
          exportToExcel: '=',
          templateView: '=',
          reloadDataToGrid:'=',
          refreshData:'=',
          pageChanged:'=',
          changePage:'=',
          processExportFinished:'=',
          getCurrentUrl: '=',
          getCurrentFilter: '=',
          totalElements: '=',
          includeSubEntityData: '=',
          changeSelectedItem: '=',
          findId: '=',
          elementName: '=',
          getDataUrlBuilder: '=',
          loadData: '=',
          onCheckboxChanged: '=',


        },
        controller: 'pageableCheckableItemsGridCtrl',
        link: function (scope:any, element, attrs:any) {
          scope.Init(attrs.restrictedentitydisplaytypename);
        }
      }

    });

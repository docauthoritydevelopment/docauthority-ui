///<reference path='../../all.d.ts'/>
'use strict';
angular.module('directives.highlights', [])
    .controller('highlightResultsCtrl', function ($scope, $element, Logger:ILogger, $state, $stateParams, $timeout, $location,configuration:IConfig,dialogs,
                                               splitViewFilterService:IFilterService, eventCommunicator:IEventCommunicator) {
      var log:ILog = Logger.getInstance('highlightResultsCtrl');


    })
    .directive('highlightResults',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/common/directives/reports/highlightResults.tpl.html',
            scope:{
              'fileSnippets':'=',
              'excelLocations':'=',
              'searchText':'=',
              'fileModifiedSinceScanned':'=',
              'showFileSearchSinippets':'=',
            },
            controller: 'highlightResultsCtrl',


          };
        }
    );

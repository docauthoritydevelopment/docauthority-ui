///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.functionalRoles', [
    'resources.functionalRoles',
   ])
    .controller('functionalRolesMenuCtrl', function ($scope,$window,$element,$timeout,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                          userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,functionalRolesResource:IFunctionalRolesResource,functionalRoleResource) {
      var log: ILog = Logger.getInstance('functionalRolesMenuCtrl');
      var isMasterManager;
      var MAX_RECENTLY_USED: number = parseInt(configuration.max_recently_used_functionalRoles);

      $scope.iconClass = configuration.icon_functionalRole;

      $scope.init = function (isMaster) {
        isMasterManager = isMaster;
        if (isMasterManager) {
          registerCommunicationHandlers();
          initUserSettingsPreferences();
        }
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        $timeout(function() //load only after page has loaded
        {
          loadFuncRoles();
        },45);

      };
      var loadFuncRoles = function()
      {
        if(isMasterManager) {
          initUserSettingsActiveFuncRole();
        }

      }
      var initUserSettingsActiveFuncRole = function () {

        setActiveRecentlyUsed();

        $scope.$on(userSettings.activeRecentlyUsedFunctionalRoleItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsed();

        });
      };
      var setActiveRecentlyUsed = function () {
        userSettings.getRecentlyUsedFunctionalRole(function (savedActiveRecentlyUsedFunctionalRolesIDs:number[]) {
          if (savedActiveRecentlyUsedFunctionalRolesIDs && savedActiveRecentlyUsedFunctionalRolesIDs[0]) {
            functionalRolesResource.getFunctionalRolesByIds(savedActiveRecentlyUsedFunctionalRolesIDs,
              function (functionalRoles:Users.DtoFunctionalRole[]) {
                var approvedListOfRecntlyUsedItems = [];
                savedActiveRecentlyUsedFunctionalRolesIDs.forEach(s=> {
                  var foundItem = functionalRoles.filter(d=>d.id == s)[0];
                  if (foundItem) {
                    if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                      approvedListOfRecntlyUsedItems.push(foundItem);
                    }
                  }
                });
                $scope.savedRecentlyUsedFunctionaRoles = approvedListOfRecntlyUsedItems;

              },onError
            );
          }

        });
      };
      var setUserRoles = function()
      {
        $scope.isAssignFunctionalUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignDataRole]);
        $scope.isMngRolesConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngRoles]);
      }
      $scope.toggleMinimizeToolbar = function () {
        $scope.minimizeToolbar = !$scope.minimizeToolbar;
        userSettings.setFunctionalRolesPreferences({minimizeToolbar: $scope.minimizeToolbar});

        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      }

      var initUserSettingsPreferences = function () {

        userSettings.getFunctionalRolesPreferences(function (preferences) {
          $scope.minimizeToolbar = preferences && typeof (<any>preferences).minimizeToolbar != 'undefined' ? (<any>preferences).minimizeToolbar : false;
        });


      };

      var setLastSelectedFunctionalRoleItemId = function (lastSelectedId) {
        if (lastSelectedId) {
          userSettings.setCurrentFunctionalRole(lastSelectedId);
        }
      };

      var showWarning = function(callback) {
        let warningRequired = _.some($scope.selectedFunctionalRolesItemsToTag, (item) => {
          return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
        });
        if(warningRequired) {
          userSettings.getDontShowAssociationWarning((dontShow) => {
            if (!dontShow) {
              var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
              dlg.result.then((dontShow) => {
                userSettings.setDontShowAssociationWarning(dontShow);
                callback();
              });
            }else {
              callback();
            }
          });
        }else {
          callback();
        }
      };


      $scope.toggleFunctionalRoleToSelectedItems = function (functionalRoleToToggle: Users.DtoFunctionalRole) {

        if (functionalRoleToToggle && $scope.selectedFunctionalRolesItemsToTag && $scope.selectedFunctionalRolesItemsToTag.length > 0) {
          var firstSelectedItem = $scope.selectedFunctionalRolesItemsToTag[0];
          var tags = (<any>firstSelectedItem).item ? (<any>firstSelectedItem).item.associatedFunctionalRoles : (<any>firstSelectedItem).associatedFunctionalRoles;

          showWarning(()=> {
            if (isExplicitFunctionalRoleExists(tags, functionalRoleToToggle)) {
              $scope.removeFunctionalRoleFromSelectedItems(functionalRoleToToggle);
            }
            else {
              $scope.addFunctionalRoleToSelectedItems(functionalRoleToToggle);
            }
          });
        }
      };

      $scope.addFunctionalRoleToSelectedItems = function (functionalRole: Users.DtoFunctionalRole) {
        var functionalRoleToAdd: Users.DtoFunctionalRole = functionalRole == null ? $scope.activeSelectedFunctionalRoleItem : functionalRole;
        functionalRoleToAdd = (<Users.DtoFunctionalRoleSummaryInfo>(<any>functionalRoleToAdd)).functionalRoleDto?(<Users.DtoFunctionalRoleSummaryInfo>(<any>functionalRoleToAdd)).functionalRoleDto:functionalRoleToAdd;
        if (functionalRoleToAdd && $scope.selectedFunctionalRolesItemsToTag && $scope.selectedFunctionalRolesItemsToTag.length > 0) {
          var _functionalRolesResource: IFunctionalRoleResource = functionalRoleResource($scope.displayType);
          var loopPromises = [];
          $scope.selectedFunctionalRolesItemsToTag.forEach(function (selectedItem) {
            var addFunctionalRole = _functionalRolesResource.addFunctionalRole(functionalRoleToAdd.id, selectedItem['id']);
            var deferred = $q.defer();
            loopPromises.push(deferred.promise);
            addFunctionalRole.$promise.then(function (g: any) {
                log.debug('receive addFunctionalRole success for ' + selectedItem.id);
                updateDisplayedItemAfterAdd(selectedItem, functionalRoleToAdd);
                deferred.resolve(selectedItem);
              },
              function (error) {
                deferred.resolve();
                log.error('receive addFunctionalRole failure' + error);
                onError();
              });
          });
          $q.all(loopPromises).then(function (selectedItemsToUpdate) {
            if ($scope.refreshNeeded) {
              $scope.refreshData();
            }
            else {
              $scope.reloadData(); //no need when auto sync is false
            }
          });
        }
      };

      $scope.removeFunctionalRoleFromSelectedItems = function (functionalRoleToRemove: Users.DtoFunctionalRole) {

        if (functionalRoleToRemove && $scope.selectedFunctionalRolesItemsToTag && $scope.selectedFunctionalRolesItemsToTag.length > 0) {
          functionalRoleToRemove = (<Users.DtoFunctionalRoleSummaryInfo>(<any>functionalRoleToRemove)).functionalRoleDto?(<Users.DtoFunctionalRoleSummaryInfo>(<any>functionalRoleToRemove)).functionalRoleDto:functionalRoleToRemove;
          var _functionalRolesResource: IFunctionalRoleResource = functionalRoleResource($scope.displayType);
          var itemsThatHasThisFunctionalRole = $scope.selectedFunctionalRolesItemsToTag.filter(i => {
            var functionalRolesDtos = i.item ? i.item.associatedFunctionalRoles : i.associatedFunctionalRoles;
            var theFunctionalRole = functionalRolesDtos ? functionalRolesDtos.filter(a => a.id == functionalRoleToRemove.id)[0] : null;
            return theFunctionalRole != null && !theFunctionalRole.implicit;
          });
          var loopPromises = [];
          if (!_.isEmpty(itemsThatHasThisFunctionalRole)) {
            _.each(itemsThatHasThisFunctionalRole, function (selectedItem) {
              var removeTag = _functionalRolesResource.removeFunctionalRole(functionalRoleToRemove.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              removeTag.$promise.then(function (g: any) {
                  log.debug('receive removeFunctionalRole from selected items success for' + selectedItem['id']);
                  updateDisplayedItemAfterRemove(selectedItem, functionalRoleToRemove);
                  deferred.resolve(selectedItem);
                },
                function (error) {
                  deferred.resolve();
                  log.error('receive removeFunctionalRole from selected items failure' + error);
                  onError();
                });
            })
            $q.all(loopPromises).then(function (selectedItemsToUpdate) {
              if ($scope.refreshNeeded) {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
            });
          }
        }
      };

      var onError = function () {
        dialogs.error('Error', 'Sorry, an error occurred.');
      };

      $scope.openSelectDialog = function (editMode: boolean) {
        userSettings.getCurrentFunctionalRole(function(savedActiveFuncRoleID) {
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/manageFunctionalRolesDialog.tpl.html',
            'manageFunctionalRolesDialogCtrl',
            {editMode: editMode, changeSelectedItemId: savedActiveFuncRoleID},
            {size:'lg'});

          dlg.result.then(
            function (selectedFunctionalRole) {
              if(selectedFunctionalRole) {
                setLastSelectedFunctionalRoleItemId(selectedFunctionalRole.functionalRoleDto.id);
                $scope.toggleFunctionalRoleToSelectedItems(selectedFunctionalRole);
                saveRecentlyUsedFunctionalRoles(selectedFunctionalRole.functionalRoleDto);
              }
            },
            function (selectedFunctionalRole) {
              setLastSelectedFunctionalRoleItemId(selectedFunctionalRole ? selectedFunctionalRole.functionalRoleDto.id : null);
            });
        });
      };

      var saveRecentlyUsedFunctionalRoles = function (functionalRoleToSave) {
        if (functionalRoleToSave) {
         userSettings.getRecentlyUsedFunctionalRole(function (savedActiveRecentlyUsedFunctionalRolesIDs) {
           if (savedActiveRecentlyUsedFunctionalRolesIDs) { //add current active to recenlty used
             var foundInRecentItems = savedActiveRecentlyUsedFunctionalRolesIDs.filter(d => d == functionalRoleToSave.id)[0];
             if (foundInRecentItems) { //if already exists remove it
               var foundIndex = savedActiveRecentlyUsedFunctionalRolesIDs.indexOf(foundInRecentItems);
               savedActiveRecentlyUsedFunctionalRolesIDs.splice(foundIndex, 1);
             }
             savedActiveRecentlyUsedFunctionalRolesIDs.unshift(functionalRoleToSave.id);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
             if (savedActiveRecentlyUsedFunctionalRolesIDs.length > MAX_RECENTLY_USED) {
               savedActiveRecentlyUsedFunctionalRolesIDs.splice(MAX_RECENTLY_USED, savedActiveRecentlyUsedFunctionalRolesIDs.length - MAX_RECENTLY_USED); //limit size of list
             }
           }
           else {
             savedActiveRecentlyUsedFunctionalRolesIDs = [functionalRoleToSave.id];
           }
           userSettings.setRecentlyUsedFunctionalRole(savedActiveRecentlyUsedFunctionalRolesIDs);
         });
        }
      }

      $scope.getStyleId = function (funcRole) {
        if (funcRole) {
          return splitViewBuilderHelper.getFunctionalRoleStyleId(funcRole.id);
        }
        return null;
      };

      $scope.doFilter = function (functionalRole: Users.DtoFunctionalRole) {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportReportDDLMenuUserAction, {
          action: 'doFunctionalRolesFilter',
          values: [functionalRole],
        });
      }

      $scope.$on("$destroy", function () {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var registerCommunicationHandlers = function () {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportFunctionalRolesAction,
          $scope,
          (new RegisteredEvent(function (registrarScope: any, registrationParam?: any, fireParam?: any) {
            if (fireParam.action === 'removeRole') {
              var item=fireParam.item;
              var tagToRemove=fireParam.tagToRemove;
              if($scope.displayType == fireParam.displayType) {
                removeFunctionalRoleFromItem(item, tagToRemove);
              }
            }
          })));
      };

      var removeFunctionalRoleFromItem = function (item,funcRoleToRemove:Users.DtoFunctionalRole) {
        if (item && funcRoleToRemove) {
          var resource:IFunctionalRoleResource = functionalRoleResource($scope.displayType);

          var removeTag = resource.removeFunctionalRole(funcRoleToRemove.id, item['id']);
          removeTag.$promise.then(function (g:any) {
              log.debug('receive funcRoleToRemove from  item success for' + item['id']);
              updateDisplayedItemAfterRemove(item, funcRoleToRemove);
              if( $scope.refreshNeeded)
              {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
            },
            function (error) {
              log.error('receive funcRoleToRemove from selected items failure' + error);
              onError();
            });

        }
      };

      var closeAllDdls = function () {
        $('html').trigger('click'); //to close all ddls
      };

      var updateDisplayedItemAfterAdd = function (selectedItem, functionalRoleToAdd: Users.DtoFunctionalRole) {
        var tags = (<any>selectedItem).item ? (<Entities.IFunctionalRolesAssignee>(<any>selectedItem).item).associatedFunctionalRoles : (<Entities.IFunctionalRolesAssignee>selectedItem).associatedFunctionalRoles;
        tags = JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
        if (!isExplicitFunctionalRoleExists(tags, functionalRoleToAdd)) {
          (<any>functionalRoleToAdd).isExplicit = true;
          (<any>functionalRoleToAdd).styleId =$scope.getStyleId(functionalRoleToAdd);
          tags ? tags.push(functionalRoleToAdd) : tags = [functionalRoleToAdd];
          updateSelectedItemWithFunctionalRoles(selectedItem, tags);
          log.debug(tags ? '0' : tags.length + " tags exists after adding " + functionalRoleToAdd.name);

        }
        else {
          log.info('tag ' + functionalRoleToAdd.name + ' already exsits for... in ' + JSON.stringify(tags));
        }
      };

      var updateSelectedItemWithFunctionalRoles = function (selectedItem, tags) {
        if ((<any>selectedItem).item) {
          var newItem = (<any>selectedItem).item;
          (<Entities.IFunctionalRolesAssignee>newItem).associatedFunctionalRoles = tags;

          if (selectedItem.loaded) { //for folder tree view refresh
            selectedItem.item = undefined; // force refresh of dataItem
            selectedItem.set("item", newItem);
            selectedItem.loaded(false);
       //     selectedItem.load();  //this line is loading the child folders and if there are many, i.e11 is stuck. It loads them even if not exists yet
          }
          else {
            selectedItem.item = newItem;
          }
        }
        else {
          (<Entities.IFunctionalRolesAssignee>selectedItem).associatedFunctionalRoles = tags;
        }
      }

      var isExplicitFunctionalRoleExists = function (tags: Users.DtoFunctionalRole[], selectedTag: Users.DtoFunctionalRole) {
        if (tags && tags[0]) {
          return tags.filter(t => (t.id == selectedTag.id && (<any>t).isExplicit))[0] != null;
        }
        return false;
      }

      var updateDisplayedItemAfterRemove = function (selectedItem, functionalRoleToRemove: Users.DtoFunctionalRole) {

        var tags: Users.DtoFunctionalRole[] = (<any>selectedItem).item ? (<any>selectedItem).item.associatedFunctionalRoles : (<any>selectedItem).associatedFunctionalRoles;
        if (isExplicitFunctionalRoleExists(tags, functionalRoleToRemove)) {
          tags = JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
          var itemToRemove = tags.filter(t => t.id == functionalRoleToRemove.id && (<any>t).isExplicit)[0];
          if (itemToRemove) {
            var index = tags.indexOf(itemToRemove);
            tags.splice(index, 1);
          }
          tags = tags.length == 0 ? null : tags;
          updateSelectedItemWithFunctionalRoles(selectedItem, tags);
          log.debug(tags ? tags.length + " tags exists after removing " + functionalRoleToRemove.name : '0');
        }
        else {
          log.info('tag ' + functionalRoleToRemove.name + ' already removed for  ... in ' + JSON.stringify(tags));
        }
      };

      $scope.doFilterBy = function () {

        userSettings.getCurrentFunctionalRole(function(savedActiveFunctionalRoleID){
          var dlg = dialogs.create(
              'common/directives/reports/dialogs/manageFunctionalRolesDialog.tpl.html',
              'manageFunctionalRolesDialogCtrl',
              {editMode:false,changeSelectedItemId:savedActiveFunctionalRoleID,filterMode:true},
              {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

          dlg.result.then(
            function(functionalRoleToFilter){
              let functionalRoleDto = functionalRoleToFilter.functionalRoleDto;
              if(functionalRoleDto) {
                $scope.doFilter(functionalRoleDto);
                saveRecentlyUsedFunctionalRoles(functionalRoleDto);
                setLastSelectedFunctionalRoleItemId(functionalRoleDto.id);
              }
            },
            function(functionalRoleToFilter){
              setLastSelectedFunctionalRoleItemId(functionalRoleToFilter.functionalRoleDto?functionalRoleToFilter.functionalRoleDto.id:null);
            });

        });
      }

      $scope.doFunctionalRoleFilter = function( functionalRole:Users.DtoFunctionalRole)
      {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDocTypeMenuUserAction, {
          action: 'doFunctionalRoleFilter',
          values:  [functionalRole],
        });
      }
    })
    .directive('functionalRolesMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/functionalRolesMenu.tpl.html',
        controller: 'functionalRolesMenuCtrl',
        replace:true,
        scope:{
          'selectedFunctionalRolesItemToTag':'=',
          'selectedFunctionalRolesItemsToTag':'=',
          'displayType':'=',
          'reloadData':'=',
          'refreshData':'=',
          'refreshNeeded':'=',
          'disabled':'=',
          'openManagePage':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
);



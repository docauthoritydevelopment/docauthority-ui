///<reference path='../../../common/all.d.ts'/>
'use strict';
/* global kendo */


angular.module('directives.reports',[
      'directives.splitView',
      'directives.filterTags',
      'directives.searchBox',
      'directives.actions',
      'directives.tags',
      'directives.functionalRoles'
    ])

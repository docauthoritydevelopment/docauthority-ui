///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.bizListAssociations', [
      'resources.bizListAssociations',
      'directives.dialogs.bizLists.manage',
    ])
    .controller('bizListAssociationsMenuCtrl', function ($scope,$timeout,$window,$element,Logger:ILogger,$q,bizListsResource:IBizListsResource,configuration:IConfig,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                                         routerChangeService,userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,bizListAssociationResource,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
      var log:ILog = Logger.getInstance('bizListAssociationsMenuCtrl');
      var isMasterManager;
     $scope.userFilterOptions = [EFilterContextNames.bizList_item_clients_extraction, EFilterContextNames.bizList_item_clients];


      var MAX_RECENTLY_USED:number = parseInt(configuration.max_recently_used_biz_lists_association);

      $scope.init = function (isMaster) {
        isMasterManager=isMaster;
        if($scope.isClientsBizList())
        {
          $scope.setSelectedFilterOptionMode(EFilterContextNames.bizList_item_clients,true);
          registerCommunicationHandlers();

        }
        else if($scope.isConsumerBizList())
        {
           $scope.setSelectedFilterOptionMode(EFilterContextNames.bizList_item_clients_extraction,true);
        }
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
        $timeout(function() //load only after page has loaded
        {
          initUserSettingsPreferences();
          loadLists();
        },25);
      };
        var setUserRoles = function()
        {
          $scope.isMngBizListsConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngBizLists]);
        }
      var loadLists = function()
      {
        bizListsResource.getAllBizLists($scope.bizListType, getAllBizListsCompleted,onError);
      }
      var getAllBizListsCompleted=function(listsByIdDic:any)
      {
        var lists = $.map(listsByIdDic, function(value, index) {
          return [value];
        });
          $scope.bizLists =lists;
          if(isMasterManager) {
            initUserSettingsActiveBizListAssociation();
          }
        };


      $scope.toggleMinimizeToolbar=function()
      {
        $scope.minimizeToolbar=!$scope.minimizeToolbar;
        saveCurrentPreferences();
        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      }

        var saveCurrentPreferences = function()
        {
          var pref =  { minimizeToolbar:$scope.minimizeToolbar,selectedFilterOption:$scope.selectedUserFilterOption};
          userSettings.setBizListAssociationsPreferences($scope.bizListType,pref);

        }

        var initUserSettingsPreferences = function () {

        userSettings.getBizListAssociationsPreferences($scope.bizListType,function(preferences){
           $scope.minimizeToolbar=preferences&& typeof (<any>preferences).minimizeToolbar!='undefined'?(<any>preferences).minimizeToolbar:false;
          var selectedFilterOption = preferences&& typeof (<any>preferences).selectedFilterOption!='undefined'?(<any>preferences).selectedFilterOption:null;
          if(selectedFilterOption) {
            $scope.setSelectedFilterOptionMode(selectedFilterOption,true);
          }
         });

       };

      var initUserSettingsActiveBizListAssociation = function () {

        setActiveRecentlyUsedBizListAssociations();
        $scope.$on(userSettings.activeRecentlyUsedBizListAssociationItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsedBizListAssociations();

        });
      };

      var setActiveRecentlyUsedBizListAssociations = function()
      {
        userSettings.getRecentlyUsedBizListAssociations($scope.bizListType,function(savedActiveRecentlyUsedBizListAssociations:RecentlyUsedData[])
        {
          var approvedListOfRecentlyUsedItems=[];
          if( savedActiveRecentlyUsedBizListAssociations && savedActiveRecentlyUsedBizListAssociations.length >0  ) {
            savedActiveRecentlyUsedBizListAssociations.forEach(s=> {
              var bizlistExist = $scope.bizLists.filter(l=> l.id == s.bizListId)[0];
              if(bizlistExist) {
                if (approvedListOfRecentlyUsedItems.length <= MAX_RECENTLY_USED) {
                  approvedListOfRecentlyUsedItems.push(s);
                }
              }
            });
          }
          $scope.savedRecentlyUsedBizListAssociationItems= approvedListOfRecentlyUsedItems;
        });

      }

          $scope.associationIcon = configuration.icon_bizListItemClients;
          $scope.bizListTypeIcon= function(listTypeName)
          {
            return splitViewBuilderHelper.bizListTypeIcon(listTypeName);
          }

      $scope.getBizListAssociationPath = function(recentlyUsedBizListAssociation:RecentlyUsedData)
      {
        if(recentlyUsedBizListAssociation) {
          var list = $scope.bizLists.filter(l=> l.id == recentlyUsedBizListAssociation.bizListId)[0];
          if(list) {
            return $scope.bizListType +' > '+list.name +' > '+recentlyUsedBizListAssociation.bizListItemName;
          }
        }
        return '';
      }
      var saveRecentlyUsedBizListAssociation = function(bizListAssociationToSaveId,bizListAssociationToSaveName,bizList)
      {
        if(bizListAssociationToSaveId) {
          var itemToSave = new RecentlyUsedData(bizList.id,bizListAssociationToSaveId,bizListAssociationToSaveName);
         userSettings.getRecentlyUsedBizListAssociations($scope.bizListType,
              function(savedActiveRecentlyUsedBizListAssociations:RecentlyUsedData[])
              {
                if (savedActiveRecentlyUsedBizListAssociations&&savedActiveRecentlyUsedBizListAssociations.length>0) { //add current active to recenlty used
                  var foundInRecentItems = savedActiveRecentlyUsedBizListAssociations.filter(d=>d.bizListItemId == bizListAssociationToSaveId)[0];
                  if (foundInRecentItems) { //if already exists remove it
                    var foundIndex =  savedActiveRecentlyUsedBizListAssociations.map(function(e) { return e.bizListItemId }).indexOf(bizListAssociationToSaveId);
                    savedActiveRecentlyUsedBizListAssociations.splice(foundIndex, 1);
                  }
                  savedActiveRecentlyUsedBizListAssociations.unshift(itemToSave);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
                  if (savedActiveRecentlyUsedBizListAssociations.length > MAX_RECENTLY_USED) {
                    savedActiveRecentlyUsedBizListAssociations.splice(MAX_RECENTLY_USED, savedActiveRecentlyUsedBizListAssociations.length - MAX_RECENTLY_USED); //limit size of list
                  }
                }
                else {
                  savedActiveRecentlyUsedBizListAssociations =[itemToSave];
                }
                userSettings.setRecentlyUsedBizListAssociations($scope.bizListType,savedActiveRecentlyUsedBizListAssociations);
              }
          );


        }
      }

      $scope.toggleBizListAssociationToSelectedItemsFromRecentlyUsed = function(recentlyUsedData:RecentlyUsedData)
      {
         var itemToToggle = new Entities.DTOSimpleBizListItem();
        itemToToggle.bizListId = recentlyUsedData.bizListId;
        itemToToggle.id = recentlyUsedData.bizListItemId;
        itemToToggle.name = recentlyUsedData.bizListItemName;
        $scope.toggleBizListAssociationToSelectedItems(itemToToggle);
      }


      var showWarning = function(callback) {
        let warningRequired = _.some($scope.selectedItemsToBizListAssociate, (item) => {
          return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
        });
        if(warningRequired) {
          userSettings.getDontShowAssociationWarning((dontShow) => {
            if (!dontShow) {
              var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
              dlg.result.then((dontShow) => {
                userSettings.setDontShowAssociationWarning(dontShow);
                callback();
              });
            }else {
              callback();
            }
          });
        }else {
          callback();
        }
      };


      $scope.toggleBizListAssociationToSelectedItems = function (bizListAssociationToToggle: Entities.DTOSimpleBizListItem) {

        if (bizListAssociationToToggle && $scope.selectedItemsToBizListAssociate && $scope.selectedItemsToBizListAssociate.length > 0) {
          var firstSelectedItem = $scope.selectedItemsToBizListAssociate[0];
          var associations = (<any>firstSelectedItem).item ? (<any>firstSelectedItem).item.associatedBizListItems : (<any>firstSelectedItem).associatedBizListItems;

          showWarning(()=> {
            if (isExplicitBizListAssociationExists(associations, bizListAssociationToToggle.id)) {
              $scope.removeBizListAssociationFromSelectedItems(bizListAssociationToToggle);
            }
            else {
              $scope.addBizListAssociationToSelectedItems(bizListAssociationToToggle);
            }
          });
         }
      };

      $scope.addBizListAssociationToSelectedItems = function (bizListAssociationToAdd:Entities.DTOSimpleBizListItem) {

        if (bizListAssociationToAdd && $scope.selectedItemsToBizListAssociate && $scope.selectedItemsToBizListAssociate.length > 0) {
          var theBizListAssociationResource:IBizListAssociationResource = bizListAssociationResource($scope.displayType);
          var loopPromises = [];
          $scope.selectedItemsToBizListAssociate.filter(s=>(<any>s).assignBizListAuthorized||((<any>s).item && (<any>s).item.assignBizListAuthorized)).forEach(function (selectedItem) {
            var addBizListAssociation = theBizListAssociationResource.addBizListAssociation(bizListAssociationToAdd.bizListId,bizListAssociationToAdd.id, selectedItem['id']);
            var deferred = $q.defer();
            loopPromises.push(deferred.promise);
            addBizListAssociation.$promise.then(function (updatedItem:any) {
                  log.debug('receive addBizListAssociation success for ' + selectedItem.id);
                  updateDisplayedItemAfterAdd(selectedItem, bizListAssociationToAdd);
                  deferred.resolve(selectedItem);
                },
                function (error) {
                  deferred.resolve();
                  log.error('receive addBizListAssociation failure' + error);
                  onError();
                });
          });
          $q.all(loopPromises).then(function (selectedItemsToUpdate) {
            if( $scope.refreshNeeded)
            {
              $scope.refreshData();
            }
            else {
              $scope.reloadData(); //no need when auto sync is false
            }
            routerChangeService.updateAsyncUserOperations();
          });
        }
      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.removeBizListAssociationFromSelectedItems= function (bizListAssociationToRemove:Entities.DTOSimpleBizListItem) {
       if (bizListAssociationToRemove && $scope.selectedItemsToBizListAssociate && $scope.selectedItemsToBizListAssociate.length > 0) {
         var theBizListAssociationResource:IBizListAssociationResource = bizListAssociationResource($scope.displayType);
         var itemsThatHasThisAssoc = $scope.selectedItemsToBizListAssociate.filter(i=> {
           var associatedBizListItems= i.item? i.item.associatedBizListItems: i.associatedBizListItems;
           var theAssoc =associatedBizListItems? associatedBizListItems.filter(a=>a.id == bizListAssociationToRemove.id)[0]:null ;
           return theAssoc!= null && !theAssoc.implicit;
         });
         var loopPromises = [];
         if (itemsThatHasThisAssoc && itemsThatHasThisAssoc.length > 0) {
           itemsThatHasThisAssoc.filter(s=>(<any>s).assignBizListAuthorized||((<any>s).item && (<any>s).item.assignBizListAuthorized)).forEach(function (selectedItem) {
             var removeTag = theBizListAssociationResource.removeBizListAssociation(bizListAssociationToRemove.bizListId, bizListAssociationToRemove.id, selectedItem['id']);
             var deferred = $q.defer();
             loopPromises.push(deferred.promise);
             removeTag.$promise.then(function (g:any) {
                   log.debug('receive removeBizListAssociation from selected items success for' + selectedItem['id']);
                   updateDisplayedItemAfterRemove(selectedItem, bizListAssociationToRemove.id);
                   deferred.resolve(selectedItem);
                 },
                 function (error) {
                   deferred.resolve();
                   log.error('receive removeBizListAssociation from selected items failure' + error);
                   onError();
                 });
           })
           $q.all(loopPromises).then(function (selectedItemsToUpdate) {
             if ($scope.refreshNeeded) {
               $scope.refreshData();
             }
             else {
               $scope.reloadData(); //no need when auto sync is false
             }
             routerChangeService.updateAsyncUserOperations();
           });
         }
       }
      }

          var removeBizListAssociationFromItem = function (item,bizListId,bizListAssociationTIdoRemove:string) {
            if (item && bizListAssociationTIdoRemove) {
              var resource:IBizListAssociationResource = bizListAssociationResource($scope.displayType);

              var removeTag = resource.removeBizListAssociation(bizListId,bizListAssociationTIdoRemove, item['id']);
              removeTag.$promise.then(function (g:any) {
                    log.debug('receive removeBizListAssociation from  item success for' + item['id']);
                    updateDisplayedItemAfterRemove(item, bizListAssociationTIdoRemove);
                    if( $scope.refreshNeeded)
                    {
                      $scope.refreshData();
                    }
                    else {
                      $scope.reloadData(); //no need when auto sync is false
                    }
                  },
                  function (error) {
                    log.error('receive removeBizListAssociation from selected items failure' + error);
                    onError();
                  });

            }
          };

      $scope.filterByBizListType=function(bizListType)
      {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizListAssociationMenuUserAction, {
          action: 'doBizListTypeFilter',
          type:  bizListType,
           filterContext: $scope.selectedUserFilterOption
        });
      }
      $scope.doBizListAssociationFilter = function(id,name)
      {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportBizListAssociationMenuUserAction, {
          action: 'doBizListAssociationFilter',
          id:  id,
          name:  name,
          filterContext: $scope.selectedUserFilterOption
        });
      }

      $scope.setSelectedFilterOptionMode = function (value:EFilterContextNames,doNotSave) {
        if ($scope.selectedUserFilterOption!=value) {
          $scope.selectedUserFilterOption = value;
          if(!doNotSave)
          {
            saveCurrentPreferences();
          }
         }
      };

      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      }

      var updateDisplayedItemAfterAdd = function (selectedItem, bizListAssociationToAdd:Entities.DTOSimpleBizListItem) {
        var tags = (<any>selectedItem).item ? (<any>selectedItem).item.associatedBizListItems : (<any>selectedItem).associatedBizListItems;
        tags= JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
       if (!isExplicitBizListAssociationExists(tags, bizListAssociationToAdd.id)) {
          bizListAssociationToAdd.implicit = false;
         (<any>bizListAssociationToAdd).isExplicit = true;
          tags ? tags.push(bizListAssociationToAdd) : tags = [bizListAssociationToAdd];
         updateSelectedItemWithBizAssociations(selectedItem,tags);
          log.debug(tags ? '0' : tags.length + " tags exists after adding " + bizListAssociationToAdd.name);

        }
        else {
          log.info('tag ' + bizListAssociationToAdd.name + ' already exsits for... in ' + JSON.stringify(tags));
        }
      };
       var updateSelectedItemWithBizAssociations=function(selectedItem,tags)
      {
            if ((<any>selectedItem).item) {
              var newItem  = (<any>selectedItem).item;
              newItem.associatedBizListItems = tags;

              if( selectedItem.loaded) { //for folder tree view refresh
                selectedItem.item = undefined; // force refresh of dataItem
                selectedItem.set("item", newItem);
                selectedItem.loaded(false);
         //       selectedItem.load();    //this line is loading the child folders and if there are many, i.e11 is stuck. It loads them even if not exists yet
              }
              else {
                selectedItem.item = newItem;
              }
            }
            else {
              (<any>selectedItem).associatedBizListItems = tags;
            }
          }

          var isExplicitBizListAssociationExists = function (tags:Entities.DTOSimpleBizListItem[], selectedTagId:string) {
        if(tags&& tags[0]) {
          return tags.filter(t=>(t.id == selectedTagId && (<any>t).isExplicit))[0] != null;
        }
        return false;
      }

      var updateDisplayedItemAfterRemove = function (selectedItem, bizListAssociationIdToRemove:string) {

        var tags:Entities.DTOSimpleBizListItem[] = (<any>selectedItem).item ? (<any>selectedItem).item.associatedBizListItems : (<any>selectedItem).associatedBizListItems;
        if (isExplicitBizListAssociationExists(tags, bizListAssociationIdToRemove))
        {
          tags= JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
          var itemToRemove = tags.filter(t=>t.id == bizListAssociationIdToRemove  )[0];
          if (itemToRemove) {
            var index = tags.indexOf(itemToRemove);
            tags.splice(index, 1);
          }
          tags = tags.length == 0 ? null : tags;
          updateSelectedItemWithBizAssociations(selectedItem,tags);
          log.debug(tags ?  tags.length + " tags exists after removing " + bizListAssociationIdToRemove:'0');
        }
        else {
          log.info('tag ' +bizListAssociationIdToRemove + ' already removed for  ... in ' + JSON.stringify(tags));
        }
      };
          var lastSelectedBizListAssociationFilterItemId;
      $scope.openBizListAssociationFilterBy = function(bizList:Entities.DTOBizList)
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/manageBizListDialog.tpl.html',
            'ManageBizListsDialogCtrl',
            {bizListType:$scope.bizListType,bizList:bizList,changeSelectedItem:lastSelectedBizListAssociationFilterItemId, changeGridView: $scope.changeGridView,filterMode:true},
            {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

        dlg.result.then(function(bizListAssociationToFilter){
          $scope.doBizListAssociationFilter(bizListAssociationToFilter.selectedId,bizListAssociationToFilter.selectedName);
          saveRecentlyUsedBizListAssociation(bizListAssociationToFilter.selectedId,bizListAssociationToFilter.selectedName,bizList);
          lastSelectedBizListAssociationFilterItemId = bizListAssociationToFilter.selectedId;
        },function(bizListAssociationToFilter){
          lastSelectedBizListAssociationItemId=bizListAssociationToFilter.selectedId;
        });

      }
      var lastSelectedBizListAssociationItemId;
      $scope.openSelectBizListAssociationsDialog = function(bizList:Entities.DTOBizList)
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/manageBizListDialog.tpl.html',
            'ManageBizListsDialogCtrl',
            {bizListType:$scope.bizListType,bizList:bizList,changeSelectedItem:lastSelectedBizListAssociationItemId,
              openManageAssociationPage:$scope.openManageAssociationPage, changeGridView: $scope.changeGridView},
            {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

        dlg.result.then(function(selectedBizListAssociation){
          lastSelectedBizListAssociationItemId=selectedBizListAssociation.selectedId;
          var itemToToggle = new Entities.DTOSimpleBizListItem();
          itemToToggle.bizListId = bizList.id;
          itemToToggle.id = selectedBizListAssociation.selectedId;
          itemToToggle.name = selectedBizListAssociation.selectedName;
          $scope.toggleBizListAssociationToSelectedItems(itemToToggle);
          saveRecentlyUsedBizListAssociation(selectedBizListAssociation.selectedId,selectedBizListAssociation.selectedName,bizList);
        },function(selectedBizListAssociation){ //close dialog
          lastSelectedBizListAssociationItemId=selectedBizListAssociation.selectedId;
        });
      }
        $scope.$watch(function () { return $scope.selectedItemsToBizListAssociate; }, function (value) {
          if($scope.selectedItemsToBizListAssociate &&$scope.selectedItemsToBizListAssociate.length>0)
          {
            $scope.isAssignBizListUser =  $scope.selectedItemsToBizListAssociate.filter(s=> {
                return  (s.assignBizListAuthorized || s.deleteBizListAuthorized) ||
                  (s.item && (s.item.assignBizListAuthorized ||s.item.deleteBizListAuthorized ));
              })[0]!=null;
          }
          else {
            $scope.isAssignBizListUser = false;
          }
        });
      $scope.isConsumerBizList = function() {
        return $scope.bizListType == Entities.DTOBizList.itemType_CONSUMERS;
      }
      $scope.isClientsBizList = function() {
            return $scope.bizListType == Entities.DTOBizList.itemType_CLIENTS;
      }
      $scope.getBizListIcon = function() {
        if( $scope.isClientsBizList())
            return configuration.icon_bizListItemClients;
        if( $scope.isConsumerBizList())
          return configuration.icon_bizListItemConsumerExtraction;

      }

          $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      var registerCommunicationHandlers=function()
      {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridBizListUserAction,
            $scope,
            (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

              if (fireParam.action === 'removeBizListItemAssociation') {
                var selectedDataItem=fireParam.selectedDataItem;
                var bizListItemId=fireParam.bizListItemId;
                var bizListId=fireParam.bizListId;
                  removeBizListAssociationFromItem(selectedDataItem,bizListId, bizListItemId);
              }

            })));
        //eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizListAssociationsManageMenuUserAction,
        //    $scope,
        //    (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        //
        //      if (fireParam.action === 'refreshState') {
        //        loadBizListAssociations();
        //      }
        //
        //    })));
      }

    }
      )
    .directive('bizListAssociationMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/common/directives/reports/bizListAssociationsMenu.tpl.html',
            controller: 'bizListAssociationsMenuCtrl',

            replace:true,
            scope:{
              'selectedItemsToBizListAssociate':'=',
              'displayType':'=',
              'reloadData':'=',
              'refreshData':'=',
              'refreshNeeded':'=',
              'disabledMenu':'=',
              'bizListType':'=',
              'openManageAssociationPage':'=',
              'changeGridView':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init(true);
            }
          };
        }
    );
class RecentlyUsedData
{
  constructor(public bizListId:number,public bizListItemId:string,public bizListItemName:string)
  {

  }
}



///<reference path='../../all.d.ts'/>
'use strict';
angular.module('directives.splitView.toolbars.editMode', [])
  .controller('splitViewEditModeToolbarCtrl', function ($scope,$state,$stateParams, currentUserDetailsProvider:ICurrentUserDetailsProvider,$window,$timeout,propertiesUtils,$rootScope) {

    $scope.readOnly = $stateParams.readOnly  && propertiesUtils.isBoolean( $stateParams.readOnly ) ? propertiesUtils.parseBoolean( $stateParams.readOnly ) : false;

    $scope.init=function()
    {
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });

    }
    var setUserRoles = function()
    {
      $scope.isAssignUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignTag,Users.ESystemRoleName.AssignBizList,Users.ESystemRoleName.AssignDataRole,Users.ESystemRoleName.AssignDocType]);
    }

    $scope.toggleEditMode=function()
    {
      $scope.readOnly=!$scope.readOnly;
      if(!$scope.toggleInProgress) {
        $scope.toggleInProgress = true;
        $stateParams['readOnly'] = $scope.readOnly;
        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: false, inherit: true
          });
        if ($scope.readOnly) {
          currentUserDetailsProvider.removeUserPermissions([Users.ESystemRoleName.AssignTag, Users.ESystemRoleName.AssignBizList, Users.ESystemRoleName.AssignDataRole, Users.ESystemRoleName.AssignDocType]);
          $state.reload();
        }
        else {
          window.location.reload();
        }
        $scope.toggleInProgress=false;
      }
    }

    // $rootScope.$on('$stateChangeStart', function (event, newState,toParams) {
    //   if( $scope.readOnly)
    //   {
    // //    window.location.reload();
    //   }
    //});

    $scope.init();



  })


  .directive('splitViewEditModeToolbar',
    function () {
      return {
        transclude: true,
        // restrict: 'E',
        replace: true,
        template: `<span  tooltip-direction="bottom" title="Turn off/on edit mode" ng-class="{'disabled':toggleInProgress}" > <a  ng-click="toggleEditMode()" class="btn btn-flat btn-sm" ng-class="{'active':!readOnly}">
        <i ng-if="!readOnly && isAssignUser" class="fa fa-check-square-o"></i> <i ng-show="readOnly || !isAssignUser" class="fa fa-square-o"></i>&nbsp;Edit</a> </span>`,
        scope: {
          'readOnly':'=',
          'onReadOnlyChanged':'='
        },
        link: function (scope, iElement, iAttrs:any, ctrl) {

        },

        controller: 'splitViewEditModeToolbarCtrl'
      }
    }
  )


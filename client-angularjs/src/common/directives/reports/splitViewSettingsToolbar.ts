///<reference path='../../all.d.ts'/>
'use strict';
angular.module('directives.splitView.toolbars.settings', [])
    .controller('splitViewSettingsToolbarCtrl', function ($scope, userSettings:IUserSettingsService,$window,$timeout) {


      $scope.allToolbars = [];
      $scope.toggleToolbarVisibility = function (toolbar) {
        toolbar.visible = !toolbar.visible;
        toolbar.changeShowToolbarModel(toolbar.visible);
        if(toolbar.visible) {
          saveNewToolbarsVisibility(toolbar);
        }
        else {
          removeToolbarsVisibility(toolbar)
        }
      }
      $scope.changeMultiple = function (toolbarElement, multiple:ToolbarItem[]) {
        var toolbar = (<ToolbarItem[]>$scope.allToolbars).filter(t=>t.element == toolbarElement)[0];
        toolbar.multiple = multiple;

      }

      $scope.init=function()
      {
        initUserSettings();

      }
      var initUserSettings = function () {

        $scope.$on(userSettings.activeSpiltViewDisplayedToolbarsItemKeyUpdatedEvent, function (sender, value) {
          initToolbarsVisibility();

        });

        initToolbarsVisibility();
      };
      var initToolbarsVisibility = function()
      {
        userSettings.getSpiltViewDisplayedToolbars(function (savedVisibleToolbarIDs) {
              var approvedListOfToolbarItems = [];
              if ($scope.allToolbars && $scope.allToolbars.length > 0) {
                if (savedVisibleToolbarIDs && savedVisibleToolbarIDs.length > 0) {
                  savedVisibleToolbarIDs.forEach(s=> {
                    var foundItem = (<ToolbarItem[]>$scope.allToolbars).filter(d=>d.showToolbarModelName == s)[0];
                    if (foundItem) {
                      approvedListOfToolbarItems.push(foundItem);
                    }
                  });
                  $scope.savedApprovedVisibleToolbarIDs = approvedListOfToolbarItems ? approvedListOfToolbarItems.map(t=>t.showToolbarModelName) : []; //toolbars
                }
                else { //first time - use default settings
                  $scope.savedApprovedVisibleToolbarIDs =  (<ToolbarItem[]>$scope.allToolbars).filter(t=>t.showByDefault!=null).map(a=>a.showToolbarModelName) ;
                }


                $scope.savedApprovedVisibleToolbarIDs.forEach(v=> {
                      (<ToolbarItem[]>$scope.allToolbars).filter(t=>t.showToolbarModelName == v)[0].changeShowToolbarModel(true)
                    }
                );
              }
            }
        );
      }

      var updateUserDisplayToolbars = _.debounce(()=> {
        userSettings.setSpiltViewDisplayedToolbars( $scope.savedApprovedVisibleToolbarIDs);
      },2000);


      var saveNewToolbarsVisibility = function(toolbarToAdd:ToolbarItem) {
        if(toolbarToAdd) {
          if ( $scope.savedApprovedVisibleToolbarIDs) {
            var foundInRecentItems = $scope.savedApprovedVisibleToolbarIDs.filter(d=>d == toolbarToAdd.showToolbarModelName)[0];
            if (!foundInRecentItems) { //if already exists remove it
              $scope.savedApprovedVisibleToolbarIDs.push(toolbarToAdd.showToolbarModelName);
            }
          }
          else {
            $scope.savedApprovedVisibleToolbarIDs=[toolbarToAdd.showToolbarModelName]; //first one
          }
          updateUserDisplayToolbars();
         }
      }
      var removeToolbarsVisibility = function(toolbarToRemove:ToolbarItem) {
        if(toolbarToRemove) {
          if ( $scope.savedApprovedVisibleToolbarIDs) {
            var foundInRecentItems = $scope.savedApprovedVisibleToolbarIDs.filter(d=>d == toolbarToRemove.showToolbarModelName)[0];
            if (foundInRecentItems) { //if already exists remove it
              var index = $scope.savedApprovedVisibleToolbarIDs.indexOf(foundInRecentItems);
              $scope.savedApprovedVisibleToolbarIDs.splice(index, 1);
              updateUserDisplayToolbars();
            }
          }
        }
      }
      $scope.init();



    })
    .controller('splitViewToolbarCtrl', function ($scope, $element) {

      var createToolbarItem = function(toolbarName,showToolbarModel,iconClass,multiple,showByDefault, order)
      {
        var  toolbarInst= new ToolbarItem();
        toolbarInst.order=order;
        toolbarInst.element=$element;
        toolbarInst.name=toolbarName;
        toolbarInst.iconClass=iconClass;
        toolbarInst.showToolbarModelName=showToolbarModel;
        toolbarInst.showByDefault=showByDefault;
        toolbarInst.changeShowToolbarModel=function(val)
        {
          toolbarInst.visible=val;
          $scope.$eval(showToolbarModel+'='+val);
        };
        toolbarInst.multiple = multiple;

        return toolbarInst;
      }

      $scope.init = function(toolbarName,showToolbarModel,iconClass,multiple:ToolbarItem[],showByDefault:boolean, order)
      {
        var toolbarInst = createToolbarItem(toolbarName,showToolbarModel,iconClass,multiple,showByDefault, order);

        (<any[]>$scope.allToolbars).push(toolbarInst);
        (<any[]>$scope.allToolbars).sort((tbItem1,tbItem2)=> {
          return (tbItem1.order - tbItem2.order);
        });

      }

    })

    .directive('splitViewSettingsToolbar',
       function () {
         return {
           transclude: true,
           // restrict: 'E',
           replace: true,
           template: ' <div><div  role="toolbar settings button" class="btn-group filter pull-right ddl-like" style="margin-right:-20px;margin-top:8px;" >' +
           ' <a class="btn btn-link dropdown-toggle btn-xs" title="Show / hide toolbars" style="vertical-align: bottom"><span class="fa fa-bars"></span></a>' +
           '   <ul class="dropdown-menu" style="max-width: 240px;width: max-content;" role="menu" ng-click="$event.stopPropagation();">' +
           '   <li ng-repeat="toolbar in allToolbars"  ng-class="{\'dropdown-submenu pull-left \':toolbar.multiple}"   style="float:none !important;">' +
           ' <a ng-if="toolbar.multiple" ng-click="$event.preventDefault();"><i class="{{toolbar.iconClass}}"></i>  {{toolbar.name}} {{\'(\'+toolbar.multiple.length+\')\'}} </a>' +
           '<ul class="dropdown-menu" ng-click="$event.stopPropagation();" style="overflow-y: scroll;height: 320px;">' +
           '<li ng-repeat="subToolbar in toolbar.multiple" >' +
           '<a ng-click="subToolbar.changeShowToolbarModel(subToolbar);" title="{{subToolbar.name}}"><i class="fa" ng-class="{\'fa-check-square-o\':subToolbar.visible,\'fa-square-o\' :!subToolbar.visible}"></i> ' +
           '<span>&nbsp;&nbsp;<i class="{{subToolbar.iconClass}}"></i></span>  {{subToolbar.name}} </a>' +
           '</li></ul>' +
           '     <a ng-if="!toolbar.multiple" ng-click="toggleToolbarVisibility(toolbar);"><i class="fa" ng-class="{\'fa-check-square-o\':toolbar.visible,\'fa-square-o\' :!toolbar.visible}"></i>' +
           '&nbsp;&nbsp;<span class="btn btn-flat btn-xs"><i class="{{toolbar.iconClass}}"></i></span><span class="btn btn-flat btn-xs"> {{toolbar.name}}</span> </a>' +
           '</li></ul></div><div ng-transclude style="display:inline-block;white-space: normal"></div></div>',
           scope: false,
           link: function (scope, iElement, iAttrs:any, ctrl) {

           },

           controller: 'splitViewSettingsToolbarCtrl'
         }
       }
).directive('splitViewToolbar',
    function ($window,$timeout) {
      return {
        restrict: 'EA',
        scope: false,
        replace:true,
     //   transclude: true,
        link: function (scope, iElement, iAttrs:any, ctrl) {
           var toolbarName = iAttrs.splitViewToolbar;
          var multipleModelName = iAttrs.multipleToolbars;
          var multiple =  scope.$eval(iAttrs.multipleToolbars);
          var order = iAttrs.orderToolbars;
          scope.init(toolbarName,iAttrs.showToolbar,iAttrs.iconClass,iAttrs.multipleToolbars?multiple?multiple:[]:null,iAttrs.showByDefault,order);
            if(!iAttrs.multiple) {
              scope.$watch(iAttrs.showToolbar, function (value,oldValue) {
                scope.allToolbars.filter(t=>t.element == iElement)[0].visible = value;
                value ? iElement.show() : iElement.hide();
                if(value!=oldValue) {
                  $timeout(function () {
                    let isMsie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
                    if (isMsie11) {
                      var tmpEvent = $window.document.createEvent("Event");
                      tmpEvent.initEvent("resize", false, true);
                      $window.dispatchEvent(tmpEvent);
                    }
                    else {
                      $window.dispatchEvent(new Event("resize"));
                    }
                   }, 0);
                }

              });
            }
          scope.$watch(iAttrs.multipleToolbars, function (value) {
            if(value) {
             scope.changeMultiple(iElement,value);
            }
          });
        },

        controller: 'splitViewToolbarCtrl'
      }
    }
);

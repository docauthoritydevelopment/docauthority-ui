///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.actions', [
      'directives.dialogs.actions'

    ])
    .controller('immediateActionsMenuCtrl', function ($scope,$element,Logger:ILogger,fileTagsResource:IFileTagsResource,splitViewFilterService:IFilterService,fileTagResource,propertiesUtils,
                                                      eventCommunicator:IEventCommunicator,dialogs, immediateActionsResource:IImmediateActionsResource, labelResource:ILabelResource) {
      var log:ILog = Logger.getInstance('immediateActionsMenuCtrl');

      var baseFilterValue,baseFilterField,filter;
      $scope.init = function () {
        initSupportedActions();
      };

      var initSupportedActions=function()
      {
        $scope.supportedActions = immediateActionsResource.getSupportedActions();

      }
      $scope.isPermissionAction=function(action)
      {
        return action=='ADD_PERMISSION' ||action=='REMOVE_PERMISSION';
      }



      var parseFilterParams = function()
      {
        filter=$scope.getCurrentFilter()?$scope.getCurrentFilter().filter:null;
        var url=$scope.getCurrentUrl();
        var index = url.indexOf('?');
        var queryParams={};
        if(index>-1)
        {
          queryParams= propertiesUtils.parseQuery(url.substring(index+1));
        }
        baseFilterValue =queryParams['baseFilterValue']? decodeURIComponent(queryParams['baseFilterValue']):null;
        baseFilterField =queryParams['baseFilterField']? decodeURIComponent(queryParams['baseFilterField']):null;
      }

      $scope.openNewActionDialog = function(selectedAction,autoImportParams?:EntityDisplayTypes)
      {
        parseFilterParams();
        if (selectedAction == 'APPLY_LABEL') {
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/applyLabelDialog.tpl.html',
            'applyLabelDialogCtrl',
            {

            },
            {size: 'md'});
          dlg.result.then(function (selectedLabel: number) {
            if (selectedLabel != -1 ) {
              labelResource.applyLabelById({
                filter: filter,
                baseFilterField: baseFilterField,
                baseFilterValue: baseFilterValue,
                labelId:selectedLabel
              })
            }
            else {
              labelResource.applyLabel({
                filter: filter,
                baseFilterField: baseFilterField,
                baseFilterValue: baseFilterValue
              })
            }
          }, function () {

          });
        }
        else {
          var allowForSelectedItems: boolean = $scope.selectedItemsToExecuteOn && $scope.selectedItemsToExecuteOn.length > 0;
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/triggerImmediateActionDialog.tpl.html',
            'triggerImmediateActionDialogCtrl',
            {
              actions: $scope.supportedActions,
              allowForSelectedItems: allowForSelectedItems,
              autoImportParams: autoImportParams,
              effectedItemsAll: $scope.totalEffectedEntities,
              selectedAction: selectedAction,
              effectedItemsSelected: $scope.selectedItemsToExecuteOn ? $scope.selectedItemsToExecuteOn.length : 0,
              filter: filter,
              baseFilterField: baseFilterField,
              baseFilterValue: baseFilterValue
            },
            {size: 'md'});

          dlg.result.then(function (result: any) {
            var newAction = <Actions.DtoExecuteActionRequest>result.immediateAction;
            if (!result.applyToSelectedItemsOnly) {
              startNewImmediateAction(newAction);
            }
            else {
              startActionForSelectedItems(newAction);
            }
          }, function () {

          });
        }
      }
      var startActionForSelectedItems = function (newAction:Actions.DtoExecuteActionRequest) {
        if ( newAction&& $scope.selectedItemsToExecuteOn && $scope.selectedItemsToExecuteOn.length>0) {
          log.debug('add new immediate action for selected items '+  JSON.stringify(newAction));
          var fileIds=[];
          $scope.selectedItemsToExecuteOn.forEach(i=>fileIds.push(i.id));
          immediateActionsResource.startImmediateActionForFiles(newAction,fileIds,function (status:Actions.DtoActionExecutionTaskStatus) {
            notifyChangeRunStatus();
            openActionScriptProgressDialog(status.actionTriggerId, status.scriptFolders);
            //   dialogs.notify('Create action script notification','Creating script started.<br/><br/>Script can be found at:<br/>'+status.scriptFolders);

          }, function () {
            dialogs.error('Error','Failed to start new action for selected files.');
          });
        }
      }

      var startNewImmediateAction = function (newAction:Actions.DtoExecuteActionRequest) {
        if ( newAction) {
          parseFilterParams();
          log.debug('add new immediate action '+  JSON.stringify(newAction));
          immediateActionsResource.startImmediateAction(newAction,baseFilterValue,baseFilterField, filter,function (status:Actions.DtoActionExecutionTaskStatus) {
            notifyChangeRunStatus();
            openActionScriptProgressDialog(status.actionTriggerId, status.scriptFolders);
            //     dialogs.notify('Create action script notification','Creating script started.<br/><br/>Script can be found at:<br/>'+status.scriptFolders);

          }, function () {
            dialogs.error('Error','Failed to start new action.');
          });
        }
      }

      var openActionScriptProgressDialog=function(actionTriggerId,scriptFolders)
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/triggerImmediateActionResultDialog.tpl.html',
            'triggerImmediateActionResultDialogCtrl',
            {actionTriggerId:actionTriggerId,scriptFolders:scriptFolders},
            {size:'md'});

        dlg.result.then(function(result:any){

        },function(){

        });
      }

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var notifyChangeRunStatus = function () {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderUserAction, {
          action: 'checkStatus',
        });
      };

      eventCommunicator.registerHandler(EEventServiceEventTypes.ReportActionsRunningTasksStatusUserAction,
          $scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
            /*
             * fireParam object:
             *   { initSearch: false, value: searchText, mode: mode}
             */
            if (fireParam.action === 'checkStatus') {
              $scope.disabled=!fireParam.enable;
            }
          })));


      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }


    })
    .directive('immediateActionsMenu',
        function () {
          return {
            // restrict: 'E',
            template: ' <ul class="dropdown-menu pull-right"><li ng-repeat="action in supportedActions"><a ng-click="openNewActionDialog(action)">{{action|translate}}</a></li></ul>',
            controller: 'immediateActionsMenuCtrl',
            transclude:true,
            replace:true,
            scope:{
              'displayType':'=',
              'disabled':'=',
              'getCurrentUrl':'=',
              'getCurrentFilter':'=',
              'selectedItemsToExecuteOn':'=',
              'totalEffectedEntities':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init();
            }
          };
        }
    ).directive('permissionsImmediateActionsMenuItems',
    function () {
      return {
        // restrict: 'E',
        template: ' <ul class="dropdown-menu pull-right">  <li ng-repeat="action in supportedActions|filter:isPermissionAction"><a ng-click="openNewActionDialog(action,autoImportParams)">{{action|translate}}</a></li></ul>',
        controller: 'immediateActionsMenuCtrl',
        transclude:true,
        replace:true,
        scope:{
          'displayType':'=',
          'disabled':'=',
          'getCurrentUrl':'=',
          'getCurrentFilter':'=',
          'selectedItemsToExecuteOn':'=',
          'totalEffectedEntities':'=',
          'autoImportParams':'=',


        },
        link: function (scope:any, element, attrs, ctrl, transclude) {
          transclude(function(clone) {
            element.prepend(clone);
          });
          scope.init();

        }
      };
    }
);


///<reference path='../../all.d.ts'/>
import IFilterFactory = ui.IFilterFactory;
'use strict';
/* global kendo */

angular.module('directives.splitViewFilter',[])
  .controller('splitViewFilterCtrl', function ($scope,$element,Logger:ILogger,$state,$stateParams,$timeout,filterFactory:ui.IFilterFactory,
                                         propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,
                                         pageTitle:IPageTitleService,splitViewFilterService:IFilterService,filesResource:IFilesResource) {
    var log:ILog = Logger.getInstance('splitViewFilterCtrl');
    $scope.disableRightFilter= $scope.changeDisableRightFilter;
    $scope.userSearchOnlyFilterRight= $scope.changeUserSearchOnlyFilterRight?$scope.changeUserSearchOnlyFilterRight:false;
    if( $stateParams.filter&& $stateParams.filter !='none') {
      try {
        var filterFromUrl = $stateParams.filter ? JSON.parse($stateParams.filter) : null;
        if (filterFromUrl != null) {
          splitViewFilterService.setFilterData(filterFromUrl);
        }
      }
      catch(e) {
        log.info('Failed to load filter from url');
        $stateParams.filter = null;
        $state.transitionTo($state.current,
          $stateParams,
          {
            notify: true, inherit: true
          });
      }
    }
    else {
      splitViewFilterService.setFilterData(new FilterData());
      $stateParams.filter = null;
    }

    $scope.init = function() {
      $scope.setFilterToGrids();
      $timeout(notifyUpdateFilterState,0); //notify filter loaded from url to other components after all components are ready
    };

    var toggleOperatorToFilterTag = function (filter:SingleCriteria) {
      if(filterFactory.isInvertedFilterEnabled(filter)) {
        splitViewFilterService.toggleOperatorToFilterTag(filter);
        onFilterUpdate();
      }
    };

    var onFilterUpdate = function() {
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
      filterChanged();
    };

    var removePageRestrictionFilterTag = function (filter:SingleCriteria) {
      splitViewFilterService.removePageRestrictionFilter(filter);
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
      filterChanged();
    };

    var addPageRestrictionFilterTag = function (tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators) {
      if (tags && tags.length > 0) {
        var tagFilterCriteria = tagFilter(tags, criteriaOperation);
        var currentFilters:CriteriaOperation = splitViewFilterService.getFilterData().getPageRestrictionFilter();
        var alreadyExists;
        if(currentFilters && tags.length==1)
        {
          alreadyExists = currentFilters.getAllCriteriaLeafs().filter(c=> c.fieldName == (<SingleCriteria>tagFilterCriteria).fieldName && (<SingleCriteria>tagFilterCriteria).operator == c.operator && (<SingleCriteria>tagFilterCriteria).value== c.value )[0];
        }
        if(!alreadyExists) {
          splitViewFilterService.addPageRestrictionFilter(tagFilterCriteria);
        }

        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var loadFilter = function (filterData:FilterData) {
      splitViewFilterService.setFilterData(filterData);
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
      filterChanged();
    };

    var removePageFilterTag = function (filter:SingleCriteria) {
      splitViewFilterService.removePageFilter(filter);
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
      filterChanged();
    };

    var removePageUserFilterTag = function (filter:SingleCriteria) {
      splitViewFilterService.removePageUserFilter(filter);
      //notifyUpdateFilterState();
      //$scope.setFilterToGrids();
      //filterChanged();
    };

    var clearUserSearch = function () {
      splitViewFilterService.clearUserSearchFilter();
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
      filterChanged();
    };

    var clearAll = function () {
      splitViewFilterService.setFilterData(new FilterData());
      $scope.onClearFilter();
      notifyUpdateFilterState();
      $scope.setFilterToGrids();
    };

    $scope.setFilterToGrids = function () {
      setFilterToLeftGrid();
      setFilterToRightGrid();
      if( $scope.hasMainView) {
        setFilterToMainGrid();
      }
      log.debug('Grids loaded with filters: ' + $scope.filterLeft != null || $scope.filterRight);
    };

    function filterChanged() {
      $scope.onSetFilter();
    }
    var setFilterToLeftGrid = function () {
      if(splitViewFilterService.getFilterData() && splitViewFilterService.getFilterData().hasCriteria()) {
         $element.addClass('left-filtered');
      }
      else {
        $element.removeClass('left-filtered');
      }
      var kendoFilter = splitViewFilterService.toKendoFilter();
      var leftFilterStr = angular.toJson(kendoFilter);

      if (!$scope.userSearchOnlyFilterRight && angular.toJson($scope.filterLeft) != leftFilterStr) {
        $scope.filterLeft = <ui.IDisplayElementFilterData>{
          filter:JSON.parse(leftFilterStr),
          text:filterFactory.getFilterDataDisplayedText(splitViewFilterService.getFilterData())};
      }
      else { //no filter
        $scope.filterLeft = null;
      }
      log.debug('Left Filter: ' +  angular.toJson($scope.filterLeft));
    };

    var setFilterToRightGrid = function () {
      var kendoFilter = splitViewFilterService.toKendoFilter();

      if(splitViewFilterService.getFilterData()&& splitViewFilterService.getFilterData().hasCriteria()) {
        $element.addClass('right-filtered');
      }
      else {
        $element.removeClass('right-filtered');
      }

      if(kendoFilter && !$scope.disableRightFilter) {
        var rightFilterStr =  angular.toJson(kendoFilter);
        if (angular.toJson($scope.filterRight) != rightFilterStr) {
          $scope.filterRight = <ui.IDisplayElementFilterData>{
            filter:JSON.parse(rightFilterStr),
            text:filterFactory.getFilterDataDisplayedText(splitViewFilterService.getFilterData())};
        }
      }
      else { // no filter
        $scope.filterRight = null;
      }
     // console.log('Right Filter:' +angular.toJson($scope.filterRight));
    };

    var setFilterToMainGrid = function () {
      if(splitViewFilterService.getFilterData() && splitViewFilterService.getFilterData().hasCriteria()) {
        $element.addClass('main-filtered');
      }
      else {
        $element.removeClass('main-filtered');
      }
      var kendoFilter = splitViewFilterService.toKendoFilter();
      var mainFilterStr = angular.toJson(kendoFilter);

      if (!$scope.userSearchOnlyFilterSplitView && angular.toJson($scope.filterMain) != mainFilterStr) {
        $scope.filterMain = <ui.IDisplayElementFilterData>{
          filter:JSON.parse(mainFilterStr),
          text:filterFactory.getFilterDataDisplayedText(splitViewFilterService.getFilterData())};
      }
      else { //no filter
        $scope.filterMain = null;
      }
      log.debug('Main Filter: ' +  angular.toJson($scope.filterMain));
    };

    var doUserSearch = function (value:string,selectedUserSearchMode:EUserSearchMode,noRefreshGrids?:boolean,onSuccessFunc?) {
      if (value && value.length>0) {

      }
      var filterByFileID = filterFactory.createFilterByID(EntityDisplayTypes.files)(0,'check validity');//send file id 0 inorder to test filter
      var userTextFilter = $scope.searchModes[selectedUserSearchMode.toString()](value);
      var filter = new AndCriteria([filterByFileID],[userTextFilter]);

      checkFilterValidity(filter.toKendoFilter(),function() {
        // $scope.clearSelected(false);
        // shaul: #DAMAIN-3147 - call to clearSelected set dataSource.transport.options.read.url to null and thus filter is not assigned (in function filterKendoFormat)
        if (value == '') {
          clearUserSearch();
        }
        else {
          var userFilter = $scope.searchModes[selectedUserSearchMode.toString()](value);
          splitViewFilterService.addUserSearchFilter(userFilter);
        }
        if(!noRefreshGrids) {
          doFilter();
        }
        if(onSuccessFunc)
        {
          onSuccessFunc();
        }
      },function() {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxSetState, {
          action: 'reportUserSearchTermNotValid'
        });
      });
    };

    var checkFilterValidity=function(filter,successFunction,errorFunction) {
      filesResource.getFiles(filter,function(response) {
        successFunction();
      },function(err) {
        if (err.status == 400) {
          log.debug('Filter validity check failed status 400: ' + err.data);
        }
        else {
          log.error('Filter validity check failed status '+ err.status +': ' + err.data.message);
        }
        errorFunction()
      });
    };

    var moveUserSearchToPageUserSearch = function () {
      if (splitViewFilterService.getFilterData().getUserSearchFilter() && splitViewFilterService.getFilterData().getUserSearchFilter().hasCriteria()) {
        splitViewFilterService.addPageUserSearchFilter(splitViewFilterService.getFilterData().getUserSearchFilter());
        splitViewFilterService.clearUserSearchFilter();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doTagTypeMismatchFilter= function (tags:Entities.DTOFileTagType[]) {
      if (tags && tags.length > 0) {
         var tagMismatchFilterCriteria = tagTypeMismatchFilter(tags);
        splitViewFilterService.addPageFilter(tagMismatchFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        notifyUpdateFilterState();
        filterChanged();
      }
    };

    var doTagTypeFilter = function (tags:Entities.DTOFileTagType[],criteriaOperation:ECriteriaOperators) {
      if (tags && tags.length > 0) {
        var tagFilterCriteria = tagTypeFilter(tags, criteriaOperation);
        splitViewFilterService.addPageFilter(tagFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doSearchPatternFilter = function(id,name) {
      if (id) {
        var existingExtractionFilters = clearTagFilter(false,configuration.patternFilterFieldName);
        var patternExtractionFilterCriteria = patternExtractionFilter(id,name);
        if(patternExtractionFilterCriteria) {
          patternExtractionFilterCriteria= new OrCriteria([patternExtractionFilterCriteria],existingExtractionFilters);
        }
        splitViewFilterService.addPageFilter(patternExtractionFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doSearchPatternMultiFilter = function(id,name) {
      if (id) {
        var existingExtractionFilters = clearTagFilter(false,configuration.patternMultiFilterFieldName);
        var patternExtractionFilterCriteria = patternMultiExtractionFilter(id,name);
        if(patternExtractionFilterCriteria) {
          patternExtractionFilterCriteria= new OrCriteria([patternExtractionFilterCriteria],existingExtractionFilters);
        }
        splitViewFilterService.addPageFilter(patternExtractionFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doLabelFilter = function(id,name) {
      if (id) {
        var existingExtractionFilters = clearTagFilter(false,configuration.labelFilterFieldName);
        var labelExtractionFilterCriteria = labelExtractionFilter(id,name);
        if(labelExtractionFilterCriteria) {
          labelExtractionFilterCriteria= new OrCriteria([labelExtractionFilterCriteria],existingExtractionFilters);
        }
        splitViewFilterService.addPageFilter(labelExtractionFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doBizListAssociationFilter = function (id,name) {
      if (id) {
        var existingAssociationFilters = clearTagFilter(false,configuration.bizListItemAssociationFilterFieldName);
        var bizListAssociationFilterCriteria = bizListAssociationFilter(id,name);
        if(existingAssociationFilters) {
          bizListAssociationFilterCriteria = new OrCriteria([bizListAssociationFilterCriteria],existingAssociationFilters);
        }

        splitViewFilterService.addPageFilter(bizListAssociationFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doBizListAssociationTypeFilter = function (type) {
      if (type) {
        var existingAssociationFilters = clearTagFilter(false,configuration.bizListItemAssociationTypeFilterFieldName);
        var bizListAssociationFilterCriteria = bizListAssociationTypeFilter(type);
        if(existingAssociationFilters) {
          bizListAssociationFilterCriteria = new OrCriteria([bizListAssociationFilterCriteria],existingAssociationFilters);
        }
        splitViewFilterService.addPageFilter(bizListAssociationFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doBizListExtractionFilter = function (id,name) {
      if (id) {
        var existingExtractionFilters = clearTagFilter(false,configuration.bizListItemExtractionFilterFieldName);
        var bizListExtractionFilterCriteria = bizListExtractionFilter(id,name);
        if(existingExtractionFilters) {
          bizListExtractionFilterCriteria= new OrCriteria([bizListExtractionFilterCriteria],existingExtractionFilters);
        }
        splitViewFilterService.addPageFilter(bizListExtractionFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doBizListExtractionTypeFilter = function (type) {
      if (type) {
      //  var existingExtractionFilters = clearTagFilter(false,FilterFactory.bizListItemExtractionTypeFilterFieldName);
        var bizListExtractionFilterCriteria = bizListExtractionTypeFilter(type);
     //   if(existingExtractionFilters) {
     //     bizListExtractionFilterCriteria= new OrCriteria([bizListExtractionFilterCriteria],[existingExtractionFilters]);
     //   }
        splitViewFilterService.addPageFilter(bizListExtractionFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doDocTypeUnassociated = function() {
      var existingDocTypeFilters = clearTagFilter(false, configuration.docTypeFilterFieldName);
      var existingSubDocTypeFilters = clearTagFilter(false, configuration.subDocTypesFilterFieldName);
      if (existingDocTypeFilters && existingSubDocTypeFilters) {
        existingSubDocTypeFilters = existingSubDocTypeFilters.concat(existingDocTypeFilters);
      }
      var docTypesFilterCriteria = allByFieldFilter(configuration.subDocTypesFilterFieldName, EFilterOperators.notEquals, 'Associated files');

      if (existingSubDocTypeFilters || existingDocTypeFilters) {
        docTypesFilterCriteria = new AndCriteria([docTypesFilterCriteria], existingSubDocTypeFilters ? existingSubDocTypeFilters : existingDocTypeFilters);
      }
      splitViewFilterService.addPageFilter(docTypesFilterCriteria);
      $scope.setFilterToGrids();
      $scope.onAfterSearch();
      filterChanged();
      notifyUpdateFilterState();
    };

    var doDocTypeNotFilter = function (docTypes, isSubtreeFilter) {
      if (docTypes && docTypes.length > 0) {
        var existingDocTypeFilters = clearTagFilter(false, configuration.docTypeFilterFieldName);
        var existingSubDocTypeFilters = clearTagFilter(false, configuration.subDocTypesFilterFieldName);
        if (existingDocTypeFilters && existingSubDocTypeFilters) {
          existingSubDocTypeFilters = existingSubDocTypeFilters.concat(existingDocTypeFilters);
        }
        var docTypesFilterCriteria = docTypeFilter(docTypes, ECriteriaOperators.and, isSubtreeFilter, EFilterOperators.notEquals);
        if (existingSubDocTypeFilters || existingDocTypeFilters) {
          docTypesFilterCriteria = new AndCriteria([docTypesFilterCriteria], existingSubDocTypeFilters ? existingSubDocTypeFilters : existingDocTypeFilters);
        }
        splitViewFilterService.addPageFilter(docTypesFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

  var doDocTypeFilter = function (docTypes:Entities.DTODocType[], isSubtreeFilter?:boolean) {
      if (docTypes && docTypes.length > 0) {
        var existingDocTypeFilters = clearTagFilter(false,configuration.docTypeFilterFieldName);
        var existingSubDocTypeFilters = clearTagFilter(false,configuration.subDocTypesFilterFieldName);
        if(existingDocTypeFilters && existingSubDocTypeFilters ) {
          existingSubDocTypeFilters = existingSubDocTypeFilters.concat(existingDocTypeFilters);
        }
        var docTypesFilterCriteria = docTypeFilter(docTypes, ECriteriaOperators.or,isSubtreeFilter);
        if(existingSubDocTypeFilters || existingDocTypeFilters) {
          docTypesFilterCriteria = new OrCriteria([docTypesFilterCriteria],existingSubDocTypeFilters?existingSubDocTypeFilters:existingDocTypeFilters);
        }
        splitViewFilterService.addPageFilter(docTypesFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doDepartmentFilter = function (department:Entities.DTODepartment[], isSubtreeFilter?:boolean) {
      if (department && department.length == 1) {
        var existingDepartmentFilters = clearTagFilter(false,configuration.departmentFilterFieldName);
        var existingSubDepartmentFilters = clearTagFilter(false,configuration.subDepartmentFilterFieldName);
        if(existingDepartmentFilters && existingSubDepartmentFilters ) {
          existingSubDepartmentFilters = existingSubDepartmentFilters.concat(existingDepartmentFilters);
        }
        var departmentFilterCriteria = departmentFilter(department[0], ECriteriaOperators.or, isSubtreeFilter);
        if(existingSubDepartmentFilters || existingDepartmentFilters) {
          departmentFilterCriteria = new OrCriteria([departmentFilterCriteria],existingSubDepartmentFilters?existingSubDepartmentFilters:existingDepartmentFilters);
        }
        splitViewFilterService.addPageFilter(departmentFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doDepartmentNotFilter = function (departments:Entities.DTODepartment[], isSubtreeFilter?:boolean) {
      if (departments && departments.length > 1) {
        var existingDepartmentFilters = clearTagFilter(false,configuration.departmentFilterFieldName);
        var existingSubDepartmentFilters = clearTagFilter(false, configuration.subDepartmentFilterFieldName);
        if (existingDepartmentFilters && existingSubDepartmentFilters) {
          existingSubDepartmentFilters = existingSubDepartmentFilters.concat(existingDepartmentFilters);
        }
        var departmentFilterCriteria = departmentsFilter(departments, ECriteriaOperators.and, isSubtreeFilter, EFilterOperators.notEquals);
        if (existingSubDepartmentFilters || existingDepartmentFilters) {
          departmentFilterCriteria = new AndCriteria([departmentFilterCriteria], existingSubDepartmentFilters?existingSubDepartmentFilters:existingDepartmentFilters);
        }
        splitViewFilterService.addPageFilter(departmentFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doDepartmentUnassociated = function() {
      var existingDepartmentFilters = clearTagFilter(false, configuration.departmentFilterFieldName);
      var existingSubDepartmentFilters = clearTagFilter(false, configuration.subDepartmentFilterFieldName);
      if (existingDepartmentFilters && existingSubDepartmentFilters) {
        existingSubDepartmentFilters = existingSubDepartmentFilters.concat(existingDepartmentFilters);
      }
      var departmentsFilterCriteria = allByFieldFilter(configuration.departmentFilterFieldName, EFilterOperators.notEquals, 'Associated files');

      if (existingSubDepartmentFilters || existingDepartmentFilters) {
        departmentsFilterCriteria = new AndCriteria([departmentsFilterCriteria], existingSubDepartmentFilters ? existingSubDepartmentFilters : existingDepartmentFilters);
      }
      splitViewFilterService.addPageFilter(departmentsFilterCriteria);
      doFilter();
    };

    var doMatterFilter = function (matter:Entities.DTODepartment[], isSubtreeFilter?:boolean) {
      if (matter && matter.length == 1) {
        var existingMatterFilters = clearTagFilter(false,configuration.matterFilterFieldName);
        var existingSubMatterFilters = clearTagFilter(false,configuration.subMatterFilterFieldName);
        if(existingMatterFilters && existingSubMatterFilters) {
          existingSubMatterFilters = existingSubMatterFilters.concat(existingMatterFilters);
        }
        var matterFilterCriteria = matterFilter(matter[0], ECriteriaOperators.or, isSubtreeFilter);
        if(existingSubMatterFilters || existingMatterFilters) {
          matterFilterCriteria = new OrCriteria([matterFilterCriteria],existingSubMatterFilters?existingSubMatterFilters:existingMatterFilters);
        }
        splitViewFilterService.addPageFilter(matterFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doMatterNotFilter = function (matters:Entities.DTODepartment[], isSubtreeFilter?:boolean) {
      if (matters && matters.length > 1) {
        var existingMatterFilters = clearTagFilter(false,configuration.matterFilterFieldName);
        var existingSubMatterFilters = clearTagFilter(false, configuration.subMatterFilterFieldName);
        if (existingMatterFilters && existingSubMatterFilters) {
          existingSubMatterFilters = existingSubMatterFilters.concat(existingMatterFilters);
        }
        var matterFilterCriteria = mattersFilter(matters, ECriteriaOperators.and, isSubtreeFilter, EFilterOperators.notEquals);
        if (existingSubMatterFilters || existingMatterFilters) {
          matterFilterCriteria = new AndCriteria([matterFilterCriteria], existingSubMatterFilters?existingSubMatterFilters:existingMatterFilters);
        }
        splitViewFilterService.addPageFilter(matterFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doMatterUnassociated = function() {
      var existingMatterFilters = clearTagFilter(false, configuration.matterFilterFieldName);
      var existingSubMatterFilters = clearTagFilter(false, configuration.subMatterFilterFieldName);
      if (existingMatterFilters && existingSubMatterFilters) {
        existingSubMatterFilters = existingSubMatterFilters.concat(existingMatterFilters);
      }
      var mattersFilterCriteria = allByFieldFilter(configuration.matterFilterFieldName, EFilterOperators.notEquals, 'Associated files');

      if (existingSubMatterFilters || existingMatterFilters) {
        mattersFilterCriteria = new AndCriteria([mattersFilterCriteria], existingSubMatterFilters ? existingSubMatterFilters : existingMatterFilters);
      }
      splitViewFilterService.addPageFilter(mattersFilterCriteria);
      $scope.setFilterToGrids();
      $scope.onAfterSearch();
      filterChanged();
      notifyUpdateFilterState();
    };

    var doFunctionalRoleFilter = function (funcRoles: Users.DtoFunctionalRole[]) {
      if (funcRoles) {
        var existingFuncRoleFilters = clearTagFilter(false, configuration.functionalRoleFilterFieldName);
        var functionalRoleFilterCriteria = functionalRoleFilter(funcRoles, ECriteriaOperators.or);
        if (existingFuncRoleFilters) {
          functionalRoleFilterCriteria = new OrCriteria([functionalRoleFilterCriteria], existingFuncRoleFilters);
        }
        splitViewFilterService.addPageFilter(functionalRoleFilterCriteria);
         doFilter();
      }
    };

    var doGeneralProcessingState = function (state) {
      if (state) {
        var existingFuncRoleFilters = clearTagFilter(false, configuration.fileGeneralProcessingStateFilterFieldName);
        var filterCriteria = filterFactory.createFilterByGeneralProcessingState(state);
        if (existingFuncRoleFilters) {
          filterCriteria = new OrCriteria([filterCriteria], existingFuncRoleFilters);
        }
        splitViewFilterService.addPageFilter(filterCriteria);
        doFilter();
      }
    };
    var doSpecificProcessingState = function (state) {
      if (state) {
        var existingFuncRoleFilters = clearTagFilter(false, configuration.fileSpecificProcessingStateFilterFieldName);
        var filterCriteria = filterFactory.createFilterBySpecificProcessingState(state);
        if (existingFuncRoleFilters) {
          filterCriteria = new OrCriteria([filterCriteria], existingFuncRoleFilters);
        }
        splitViewFilterService.addPageFilter(filterCriteria);
        doFilter();
      }
    };

    var doDateRangeFilter = function (dateRanges:Entities.DTODateRangeItem[],filterFieldType:string,operator?:EFilterOperators) {
      if (dateRanges && dateRanges.length > 0) {
        var existingFileCreatedDateFilters = clearTagFilter(false,filterFieldType);
        var fileCreatedDateFilterCriteria = dateRangeFilter(dateRanges, ECriteriaOperators.or,filterFieldType,operator);
        if(existingFileCreatedDateFilters) {
          fileCreatedDateFilterCriteria = new OrCriteria([fileCreatedDateFilterCriteria],existingFileCreatedDateFilters);
        }
        splitViewFilterService.addPageFilter(fileCreatedDateFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doTagFilter = function (tags:Entities.DTOFileTag[],criteriaOperation:ECriteriaOperators) {
      if (tags && tags.length > 0) {
        var tagFilterCriteria = tagFilter(tags, criteriaOperation);
        splitViewFilterService.addPageFilter(tagFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };
    var doRootFolderTextSearchFilter = function (textSearch) {
      if (textSearch) {
        clearTagFilter(false,configuration.rootFolderNameLike);
        var searchFilterCriteria = rootFolderNameLikeFilter(textSearch);
        splitViewFilterService.addPageFilter(searchFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var removeRootFolderTextSearchFilter = function() {
      clearTagFilter(true,configuration.rootFolderNameLike);
    };

    var doMailFilter = function (id, filterName) {
      if (_.isNumber(id)) {
        var mailFilterCriteria = mailFilter(id, filterName);
        splitViewFilterService.addPageFilter(mailFilterCriteria);
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var doTagExclusiveTagTypesFilter = function (tags:Entities.DTOFileTag[]) {
      if (tags && tags.length > 0) {
        var  uniqueTagTypeIDs = propertiesUtils.unique(tags,function(t:Entities.DTOFileTag){return t.type.id}).map(u=>u.type.id);
        uniqueTagTypeIDs.forEach(t=> {  //Between types add and, between same type tags use or
          var tagFilterCriteria = tagFilter(tags.filter(i=>i.type.id == t), ECriteriaOperators.or);
          splitViewFilterService.addPageFilter(tagFilterCriteria);
        });
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
    };

    var clearTagFilter = function (redrawGrids:boolean, filterType) {
      var pageFilters = splitViewFilterService.getFilterData().getPageFilter();
      if(pageFilters&& pageFilters.hasCriteria()) {
        var tagsCriterias = pageFilters.getAllCriteriaLeafs().filter(f=>f.fieldName == filterType);
        if(tagsCriterias[0]) {
          tagsCriterias.forEach( c => {
            splitViewFilterService.removePageFilter(c)
          });
          if(redrawGrids) {
            notifyUpdateFilterState();
            $scope.setFilterToGrids();
            $scope.onAfterSearch();
          }
          filterChanged();
          return tagsCriterias;
        }
      }
      return null;
   //   notifyUpdateFilterState();
    };

    // Init user Search filters
    //----------------------
    $scope.userSearchModes = EUserSearchMode.all();

    $scope.searchModes = {};
    $scope.searchModes[EUserSearchMode.content.toString()] = filterFactory.createContentSearch();
    $scope.searchModes[EUserSearchMode.properties.toString()] =filterFactory.createFilterByProperties();
    var tagFilter =filterFactory.createTagFilter();
    var mailFilter =filterFactory.createMailFilter();
    var tagTypeFilter =filterFactory.createTagTypeFilter();
    var docTypeFilter =filterFactory.createDocTypeFilter();
    var allByFieldFilter = filterFactory.createAllByFieldFilter();
    var departmentFilter =filterFactory.createDepartmentFilter();
    var departmentsFilter =filterFactory.createDepartmentsFilter();
    var matterFilter =filterFactory.createMatterFilter();
    var mattersFilter =filterFactory.createMattersFilter();
    var dateRangeFilter =filterFactory.createDateRangeFilter();
    var rootFolderNameLikeFilter =filterFactory.createRootFolderNameLikeFilter();
    var tagTypeMismatchFilter = filterFactory.createTagTypeMismatchFilter();
    var bizListAssociationFilter = filterFactory.createFilterByID( EntityDisplayTypes.bizlistItems_clients);
    var bizListExtractionFilter = filterFactory.createFilterByID(EntityDisplayTypes.bizlistItem_clients_extractions);
    var patternExtractionFilter = filterFactory.createFilterByID(EntityDisplayTypes.patterns);
    var patternMultiExtractionFilter = filterFactory.createFilterByID(EntityDisplayTypes.patterns_multi);
    var labelExtractionFilter = filterFactory.createFilterByID(EntityDisplayTypes.labels);
    var functionalRoleFilter = filterFactory.createFunctionalRoleFilter();
    var bizListAssociationTypeFilter = filterFactory.createBizListItemAssociationTypeFilter();
    var bizListExtractionTypeFilter = filterFactory.createBizListItemExtractionTypeFilter(EFilterContextNames.bizList_item_clients_extraction.toString());

    function notifyUpdateFilterState () {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxSetState, {
        action: 'reportUpdateState',
        userSearchOnlyFilterRight:$scope.userSearchOnlyFilterRight
      });

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsSetState, {
        action: 'reportUpdateState',
        disableRightFilter:$scope.disableRightFilter
      });

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportRootFolderByTexSetState, {
        action: 'reportUpdateState',
      });
    }

    $scope.onBeforeFollowUp = function (followUp:EPaneOption) {
      var filterDataToRestore =JSON.parse(angular.toJson(splitViewFilterService.getFilterData()));
      addSelectedLeftIdToFilterServiceOnFollowUp(followUp);
   //   addSelectedRightIdToFilterService(followUp);
   //   filterChanged();
      return filterDataToRestore;
    };

    $scope.onAfterFollowUp = function (filterDataToRestore) {
       splitViewFilterService.setFilterData(filterDataToRestore);
      filterChanged();
      notifyUpdateFilterState();
    };

    var addSelectedLeftIdToFilterServiceOnFollowUp = function (followUp:EPaneOption) {
      if (splitViewFilterService.getFilterData().getPageRestrictionFilter() && splitViewFilterService.getFilterData().getPageRestrictionFilter().hasCriteria()) {
        splitViewFilterService.addPageFilter(splitViewFilterService.getFilterData().getPageRestrictionFilter());
        splitViewFilterService.clearPageRestrictionFilter();
      }
      else {
        var filter = followUp.leftFilterFunction(
            $scope.isDirectFilesToggleEnabled($scope.leftPaneType)?$scope.includeSubEntityDataLeft:null ,
            $scope.isFullyContainedToggleEnabled($scope.leftPaneType,$scope.rightPaneType)?$scope.includeFullyContainedDataLeft:null
        )($scope.selectedLeftId, $scope.selectedLeftName);
        splitViewFilterService.addPageFilter(filter);
      }
      filterChanged();
    };

    $scope.filterRightDisabledChanged = function(isDisabled) {
      $scope.onFilterRightDisabledChanged(isDisabled);
    };

    $scope.userSearchOnlyFilterRightChanged = function(isDisabled) {
      $scope.onUserSearchOnlyFilterRightChanged(isDisabled);
    };
    var doFilter = function(){
      $scope.setFilterToGrids();
      $scope.onAfterSearch();
      filterChanged();
      notifyUpdateFilterState();
    }
    var filterByEntitySearchText = function (entityDisplayType:EntityDisplayTypes, textSearch:string, noRefreshGrids?) {
      if (textSearch) {
        // clearTagFilter(false,configuration.rootFolderNameLike);
        var filter =  filterFactory.createFilterBySearchText(entityDisplayType)(textSearch);

        splitViewFilterService.addPageFilter(filter);
        if(!noRefreshGrids) {
          doFilter();
        }
      }
    };

    var doGoogleLikeSearch = function(value:string, selectedUserSearchMode:EUserSearchMode) {
      let trimedStr = _.trimStart(_.trimEnd(value));
      let tokens = _.split(trimedStr, ' ');
      let textSearch = "";
      for (let count = 0 ; count < tokens.length ; count++) {
        let token = tokens[count];
        if (token.indexOf('"')>-1){
          token = token.replace('"','');
          let count2=count+1;
          while (count2 < tokens.length && tokens[count2].indexOf('"')==-1){
            token = token + ' ' + tokens[count2];
            count2++;
          }
          if (count2 < tokens.length) {
            token = token + ' ' + tokens[count2].replace('"','');
          }
          count = count2;
        }
        let foundFilterKey: boolean = false;
        for (var key in  filterFactory.searchTextAbbreviationsDic) {
          let aFilterTextFound = _.startsWith(token.toLowerCase(), key);
          if (aFilterTextFound) {
            let displayType =  filterFactory.searchTextAbbreviationsDic[key];
            let text = token.substr(key.length);
            foundFilterKey = true;
            filterByEntitySearchText(displayType, text, true);
          }
        }
        if (!foundFilterKey) {
          if (textSearch != "") {
            textSearch = textSearch + " ";
          }
          textSearch = textSearch + token;
        }
      }
      if (textSearch != "") {
         doUserSearch(textSearch, selectedUserSearchMode, true,doFilter);
      }
      else {
        doFilter();
      }
    }


    var doFilterByID = function(id,entityDisplayType,filterName,includeSubEntityDataLeft) {
      var filter;
      if(!includeSubEntityDataLeft) {
        filter =  filterFactory.createFilterByID(entityDisplayType)(id,filterName);
      }
      else if(entityDisplayType==EntityDisplayTypes.folders||entityDisplayType==EntityDisplayTypes.folders_flat) {
        filter = filterFactory.createSubFolderFilter()(id,filterName);
      }
      else if(entityDisplayType==EntityDisplayTypes.departments||entityDisplayType==EntityDisplayTypes.departments_flat) {
        filter = filterFactory.createSubDepartmentFilter()(id,filterName);
      }
      else if(entityDisplayType==EntityDisplayTypes.matters||entityDisplayType==EntityDisplayTypes.matters_flat) {
        filter = filterFactory.createSubMatterFilter()(id,filterName);
      }
      else if(entityDisplayType==EntityDisplayTypes.doc_types) {
        filter = filterFactory.createSubDocTypesFilter()(id,filterName);
      }

      splitViewFilterService.addPageFilter(filter);
      doFilter();
    };

    var doFileDupFilder = function(id, filterName) {
      var filter = filterFactory.createFileDuplicationsFilter()(id, filterName);
      splitViewFilterService.addPageFilter(filter);
      $scope.setFilterToGrids();
      filterChanged();
      notifyUpdateFilterState();
    };

    var doRestrictionFilterByID = function(id,entityDisplayType,filterName,includeSubEntityDataLeft,clearDisplayTypeBeforeFilter) {
      if(clearDisplayTypeBeforeFilter) {
        var pageRestricationsFilters = splitViewFilterService.getFilterData().getPageRestrictionFilter();
        if (pageRestricationsFilters && pageRestricationsFilters.hasCriteria()) {
          var tagsCriterias = pageRestricationsFilters.getAllCriteriaLeafs().filter(f=>f.contextName?EFilterContextNames.convertEntityDisplayType(entityDisplayType).value == f.contextName.value:false);
          if (tagsCriterias[0]) {
            tagsCriterias.forEach(c=> splitViewFilterService.removePageRestrictionFilter(c));
          }
        }
      }
      if(id) {
        var filter;
        if (!includeSubEntityDataLeft) {
          filter = filterFactory.createFilterByID(entityDisplayType)(id, filterName);
        }
        else if (entityDisplayType == EntityDisplayTypes.folders || entityDisplayType == EntityDisplayTypes.folders_flat) {
          filter = filterFactory.createSubFolderFilter()(id, filterName);
        }
        else if (entityDisplayType == EntityDisplayTypes.departments || entityDisplayType == EntityDisplayTypes.departments_flat) {
          filter = filterFactory.createSubDepartmentFilter()(id, filterName);
        }
        else if (entityDisplayType == EntityDisplayTypes.matters || entityDisplayType == EntityDisplayTypes.matters_flat) {
          filter = filterFactory.createSubMatterFilter()(id, filterName);
        }
        else if (entityDisplayType == EntityDisplayTypes.doc_types) {
          filter = filterFactory.createSubDocTypesFilter()(id, filterName);
        }
        splitViewFilterService.addPageRestrictionFilter(filter);
      }
      doFilter();
    };

    var filterByUngrouped = function() {
      var filter =  filterFactory.createFilterByUngrouped();
      splitViewFilterService.addPageFilter(filter);
      doFilter();
    };
    var doExpiredUser = function() {
      var filter =  filterFactory.createFilterByExpiredUser();
      splitViewFilterService.addPageFilter(filter);
      doFilter();
    };

    var filterByGroupDetachedFiles = function() {
      var filter =  filterFactory.createFilterByGroupDetachedFiles();
      splitViewFilterService.addPageFilter(filter);
      $scope.setFilterToGrids();
      filterChanged();
      notifyUpdateFilterState();
    };

    var filterByOpenAccessRights = function() {
      var filter =  filterFactory.createFilterByOpenAccessRights();
      splitViewFilterService.addPageFilter(filter);
      $scope.setFilterToGrids();
      filterChanged();
      notifyUpdateFilterState();
    };

    var filterByNoneMailRelatedDataFiles = function() {
      var filter =  filterFactory.createFilterByNoneMailRelatedDataFiles();
      splitViewFilterService.addPageFilter(filter);
      $scope.setFilterToGrids();
      filterChanged();
      notifyUpdateFilterState();
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportSearchBoxUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'clearUserSearch') {
        clearUserSearch();
      }
      else if (fireParam.action === 'doUserSearch') {
        var value = fireParam.value;
        var selectedUserSearchMode = fireParam.selectedUserSearchMode;
        doUserSearch(value,selectedUserSearchMode);
      }
      else if (fireParam.action === 'toggleUserSearchOnlyFilterRight') {
        $scope.userSearchOnlyFilterRight = fireParam.userSearchOnlyFilterRight;
        $scope.userSearchOnlyFilterRightChanged( $scope.userSearchOnlyFilterRight);
        $scope.setFilterToGrids();
      }
      else if (fireParam.action === 'startNewUserSearch') {
        moveUserSearchToPageUserSearch();
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportFilterTagsUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      /*
       * fireParam:{
       *      action: 'clear' / 'search' / 'modeSelected',
       *      value: 'text',
       *      mode: 'Content' / 'Properties'
       * }
       */

      if (fireParam.action === 'clearUserSearch') {
           clearUserSearch();
       }
      else if (fireParam.action === 'removePageRestrictionFilterTag') {
        removePageRestrictionFilterTag(fireParam.filter);
      }
      else if (fireParam.action === 'removePageUserFilterTag') {
        if(fireParam.filter) {
          removePageUserFilterTag(fireParam.filter);
        }
        else  if(fireParam.filters) {
          fireParam.filters.forEach(f=>   removePageUserFilterTag(f));

        }
        notifyUpdateFilterState();
        $scope.setFilterToGrids();
        filterChanged();
      }
      else if (fireParam.action === 'removePageFilterTag') {
        removePageFilterTag(fireParam.filter);
      }
      else if(fireParam.action === 'loadSavedFilter')
      {
        loadFilter(fireParam.filterData);
      }
      else if (fireParam.action === 'disableRightFilter') {
        $scope.disableRightFilter = fireParam.value;
        $scope.filterRightDisabledChanged( $scope.disableRightFilter);
        setFilterToRightGrid();

      }
      else if (fireParam.action === 'clearAll') {
        clearAll();
      }
      else if (fireParam.action === 'filterByUngrouped') {
        filterByUngrouped();
      }
      else if (fireParam.action === 'doDocTypeUnassociated') {
        doDocTypeUnassociated();
      }
      else if (fireParam.action === 'filterByExpiredUser') {
        doExpiredUser();
      }
      else if (fireParam.action === 'generalProcessingState') {
        doGeneralProcessingState('');
      }
      else if (fireParam.action === 'fileCurrentStateMap') {
        doSpecificProcessingState('Map');
      }
      else if (fireParam.action === 'fileCurrentStateIngested') {
        doSpecificProcessingState('Ingested');
      }
      else if (fireParam.action === 'fileCurrentStateError') {
        doSpecificProcessingState('Error');
      }
      else if (fireParam.action === 'fileCurrentStateAnalyzable') {
        doSpecificProcessingState('Analyzable');
      }
      else if (fireParam.action === 'fileCurrentStateAnalyzed') {
        doSpecificProcessingState('Analyzed');
      }
      else if (fireParam.action === 'doDepartmentUnassociated') {
        doDepartmentUnassociated();
      }
      else if (fireParam.action === 'filterByGroupDetachedFiles') {
        filterByGroupDetachedFiles();
      }
      else if(fireParam.action === 'filterByNoneMailRelatedDataFiles') {
        filterByNoneMailRelatedDataFiles();
      }
      else if(fireParam.action === 'filterByOpenAccessRights') {
        filterByOpenAccessRights();
      }
      else if(fireParam.action === 'filterByMediaTypeOneDrive') {
        filterByEntitySearchText(EntityDisplayTypes.media_type,<any>Operations.EMediaType.ONE_DRIVE);
      }
      else if(fireParam.action === 'filterByMediaTypeSharePoint') {
        filterByEntitySearchText(EntityDisplayTypes.media_type,<any>Operations.EMediaType.SHARE_POINT);
      }
      else if(fireParam.action === 'filterByMediaTypeFileShare') {
        filterByEntitySearchText(EntityDisplayTypes.media_type,<any>Operations.EMediaType.FILE_SHARE);
      }
      else if(fireParam.action === 'filterByMediaTypeEx') {
        filterByEntitySearchText(EntityDisplayTypes.media_type,<any>Operations.EMediaType.EXCHANGE365);
      }
      else if(fireParam.action === 'filterByMediaTypeBox') {
        filterByEntitySearchText(EntityDisplayTypes.media_type,<any>Operations.EMediaType.BOX);
      }
      else if (fireParam.action === 'toggleOperator') {
        toggleOperatorToFilterTag(fireParam.filter);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportTagsMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doTagFilter') {
        var values = fireParam.values;
        var criteriaOperation = fireParam.criteriaOperation;
        doTagFilter(values,criteriaOperation);
      }
      else if (fireParam.action === 'clearAndDoTagExclusiveTagTypesFilter') {
        var values = fireParam.values;
        clearTagFilter(!values||values.length==0,configuration.tagFilterFieldName); //if no tags., refresh grids after clear
        doTagExclusiveTagTypesFilter(values);
      }
      else if (fireParam.action === 'refreshGrids') {
        $scope.setFilterToGrids();
        $scope.onAfterSearch();
        filterChanged();
        notifyUpdateFilterState();
      }
      else if (fireParam.action === 'doTagTypeFilter') {
        var values = fireParam.values;
        var criteriaOperation = fireParam.criteriaOperation;
        doTagTypeFilter(values,criteriaOperation);
      }
      else if (fireParam.action === 'doTagTypeMismatchFilter') {
        var values = fireParam.values;
        doTagTypeMismatchFilter(values);
      }
      else if (fireParam.action === 'clearTagFilter') {
        var redraw =  (typeof fireParam.redrawGrids!='undefined')?fireParam.redrawGrids:true;
        clearTagFilter(redraw,configuration.tagFilterFieldName);
        clearTagFilter(redraw,configuration.tagTypeFilterFieldName);
      }
      else if (fireParam.action === 'doPageRestrictionFilter') {
        var values = fireParam.values;
        var criteriaOperation = fireParam.criteriaOperation;
        addPageRestrictionFilterTag(values,criteriaOperation);
      }
      else if (fireParam.action === 'doClearAndRestrictionFilterByID') {
        doRestrictionFilterByID(fireParam.id,fireParam.entityDisplayType,fireParam.filterName,fireParam.includeSubEntityDataLeft,true);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportReportDDLMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doTagFilter') {
        var values = fireParam.values;
        var criteriaOperation = fireParam.criteriaOperation;
        doTagFilter(values,criteriaOperation);
      }
      else if (fireParam.action === 'doTagTypeFilter') {
        var values = fireParam.values;
        var criteriaOperation = fireParam.criteriaOperation;
        doTagTypeFilter(values,criteriaOperation);
      }
      else if (fireParam.action === 'doTagTypeMismatchFilter') {
        var values = fireParam.values;
        doTagTypeMismatchFilter(values);
      }
      else if (fireParam.action === 'doSearchPatternFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doSearchPatternFilter(id,name);
      }
      else if (fireParam.action === 'doSearchPatternMultiFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doSearchPatternMultiFilter(id,name);
      }
      else if (fireParam.action === 'doLabelFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doLabelFilter(id,name);
      }
      else if (fireParam.action === 'doBizAssociationFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doBizListAssociationFilter(id,name);
      }
      else if (fireParam.action === 'doDepartmentFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDepartmentFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doMatterFilter') {
        var values = fireParam.values;
        doMatterFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doDocTypeFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDocTypeFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doFunctionalRoleFilter') {
        var values = fireParam.values;
        doFunctionalRoleFilter(values);
      }
      else if (fireParam.action === 'doCreationDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileCreatedDatesFilterFieldName,operator);
      }
      else if (fireParam.action === 'doModifiedDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileModifiedDatesFilterFieldName,operator);
      }
      else if (fireParam.action === 'doAccessedDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileAccessedDatesFilterFieldName,operator);
      }
      else if (fireParam.action === 'doBizExtractionFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doBizListExtractionFilter(id,name);
      }
      else if (fireParam.action === 'doExtractionRuleFilter') {
        var id = fireParam.id;
        var name = fireParam.name;
        doFilterByID(id,EntityDisplayTypes.bizlistItem_consumers_extraction_rules,name,false);
      }
      else if (fireParam.action === 'doFunctionalRolesFilter') {
        var values = fireParam.values;
        doFunctionalRoleFilter(values);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDocTypeMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doDocTypeFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDocTypeFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doDocTypeUnassociated') {
        doDocTypeUnassociated();
      }
      else if (fireParam.action === 'doDocTypeNotFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDocTypeNotFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doFunctionalRoleFilter') {
        var values = fireParam.values;
        doFunctionalRoleFilter(values);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDepartmentMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doDepartmentFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDepartmentFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doDepartmentNotFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doDepartmentNotFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doDepartmentUnassociated') {
        doDepartmentUnassociated();
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportMatterMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doMatterFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doMatterFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doMatterNotFilter') {
        var values = fireParam.values;
        var isSubtreeFilter = fireParam.isSubtreeFilter;
        doMatterNotFilter(values, isSubtreeFilter);
      }
      else if (fireParam.action === 'doMatterUnassociated') {
        doMatterUnassociated();
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDateFilterMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doCreationDateDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileCreatedDatesFilterFieldName,operator);
      }
     else if (fireParam.action === 'doModifiedDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileModifiedDatesFilterFieldName,operator);
      }
      else if (fireParam.action === 'doAccessedDateRangeFilter') {
        var values = fireParam.values;
        var operator = fireParam.operator;
        doDateRangeFilter(values,configuration.fileAccessedDatesFilterFieldName,operator);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportBizListAssociationMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doBizListAssociationFilter') {
        var name = fireParam.name;
        var id = fireParam.id;
        if(fireParam.filterContext==EFilterContextNames.bizList_item_clients_extraction) {
          doBizListExtractionFilter(id, name);
        }
        else if(fireParam.filterContext==EFilterContextNames.bizList_item_clients) {
          doBizListAssociationFilter(id, name);
        }
      }
      else if (fireParam.action === 'doBizListTypeFilter') {
        var type = fireParam.type;
        if(fireParam.filterContext.value==EFilterContextNames.bizList_item_clients_extraction.value ||fireParam.filterContext.value ==EFilterContextNames.bizList_item_consumers_extraction_rule.value  ) {
          doBizListExtractionTypeFilter(type);
        }
        else if(fireParam.filterContext.value ==EFilterContextNames.bizList_item_clients.value) {
          doBizListAssociationTypeFilter(type);
        }
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridFilterUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'doFilterByID') {
         doFilterByID(fireParam.id,fireParam.entityDisplayType,fireParam.filterName,fireParam.includeSubEntityDataLeft);
      }
      else if (fireParam.action === 'doFileDuplicationsFilter') {
         doFileDupFilder(fireParam.id,fireParam.filterName);
      }
      else if (fireParam.action === 'doMailFilter') {
        doMailFilter(fireParam.id,fireParam.filterName);
      }
      else  if (fireParam.action === 'doRootFolderTextSearch') {
        doRootFolderTextSearchFilter(fireParam.searchText);
      }
      else if(fireParam.action === 'removeRootFolderTextSearch') {
        removeRootFolderTextSearchFilter();
      }
      else  if (fireParam.action === 'doEntityTextSearch') {
        filterByEntitySearchText(fireParam.entityDisplayType,fireParam.searchText);
      }
      else  if (fireParam.action === 'doGoogleLikeSearch') {
        doGoogleLikeSearch(fireParam.searchText, fireParam.selectedUserSearchMode);
      }
    })));

    // eventCommunicator.registerHandler(EEventServiceEventTypes.ReportNotifyReady,
    //   $scope,
    //   (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
    //     /*
    //      * fireParam:{
    //      *      generatorName: ...
    //      * }
    //      */
    //
    //      notifyUpdateFilterState();
    //
    //
    //   })));
    //
  })
  .directive('splitViewFilter',function () {
    return {
      // restrict: 'E',
    //  templateUrl: 'common/directives/searchBox.tpl.html',
      scope:false,
      controller: 'splitViewFilterCtrl',

      link: function (scope:any, element, attrs) {
        scope.init();
      }
    };
  }
);

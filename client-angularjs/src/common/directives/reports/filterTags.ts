///<reference path='../../all.d.ts'/>
'use strict';

angular.module('directives.filterTags', ['resources.filters','directives.dialogs','directives.dialogs.confirms','directives.dialogs.savedFilters','directives.fileDates','doc2Services'])
  .controller('pageFilterTagsCtrl', function ($scope,$window,$element,Logger:ILogger,$state,$stateParams,$timeout,propertiesUtils,configuration:IConfig,filterFactory,
                                              apiUtils,userSettings:IUserSettingsService,currentUserDetailsProvider:ICurrentUserDetailsProvider,splitViewFilterService:IFilterService,
                                              eventCommunicator:IEventCommunicator,filterResource:IFilterResource,dialogs) {
    let log:ILog = Logger.getInstance('pageFilterTagsCtrl');
    $scope.modifiledFilesFilterContextName = EFilterContextNames.file_modified_date;

    $scope.init = function(left) {
      $scope.isSupportMngUserViewData =  currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUserViewData]);
      $scope.username = currentUserDetailsProvider.getUserData().name;

      if($scope.filterService())
      {
        splitViewFilterService = $scope.filterService()();
      }
      $scope.isLeft=left ? (left.toLowerCase() == "true") : false ;

      if($scope.activeFiltersOnly) {
        refreshState();
      } else {
        _.defer(()=> {
          loadSavedFiltersList();
        },300);
      }
    };
    var refreshState = function () {
      $scope.hasFilter = splitViewFilterService.getFilterData().hasCriteria();
      $scope.pagefilterTags = splitViewFilterService.getFilterData().getPageFilter() ? splitViewFilterService.getFilterData().getPageFilter().getAllCriteriaLeafs() : null;
      $scope.userSearchFilterTag =splitViewFilterService.getFilterData().getUserSearchFilter() ? splitViewFilterService.getFilterData().getUserSearchFilter() : null;
      $scope.sampleUserFilter =  $scope.userSearchFilterTag?splitViewFilterService.getFilterData().getUserSearchFilter().getAllCriteriaLeafs()[0]:null;
      $scope.pageUserFiltersTag =splitViewFilterService.getFilterData().getPageUserSearchFilters() ? splitViewFilterService.getFilterData().getPageUserSearchFilters().getAllCriteriaLeafs() : null;
      $scope.pageUserUniqueFiltersTag = splitViewFilterService.getFilterData().getPageUserSearchFilters() ? splitViewFilterService.getFilterData().getPageUserSearchFilters().getAllUniqueCriteriaOperationLeafs(propertiesUtils) : null;
      $scope.pageRestricationFilterTags = splitViewFilterService.getFilterData().getPageRestrictionFilter() ? splitViewFilterService.getFilterData().getPageRestrictionFilter().getAllCriteriaLeafs() : null;

      if($scope.isFiltersPaneVisible) {
        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      }
    };
    $scope.isOperatorContains=function(operator:EFilterOperators){
      return operator==EFilterOperators.contains;
    }

    $scope.removePageFilterTag = function (filter:SingleCriteria) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'removePageFilter',
        filter:filter
      });
    };

    $scope.removePageRestrictionFilterTag = function (filter:SingleCriteria) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'removePageRestrictionFilterTag',
        filter:filter
      });
    };

    $scope.removePageUserFilterTag = function (filter:SingleCriteria) {
      var parentFilter =  $scope.pageUserFiltersTag.filter(p=> p.originalCriteriaOperationId == filter.originalCriteriaOperationId);
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'removePageUserFilterTag',
        filters:parentFilter
      });
    };

    $scope.removePageFilterTag = function (filter:SingleCriteria) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'removePageFilterTag',
        filter:filter
      });
    };

    $scope.toggleOperatorFilterTag = function (filter:SingleCriteria) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'toggleOperator',
        filter:filter
      });
    };

    $scope.clearUserSearch = function () {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'clearUserSearch',
      });
    };

    $scope.removeAll = function() {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
        action: 'clearAll',
      });
    };

    $scope.getIconName = function(filter:SingleCriteria) {
      if(filter.fieldName == configuration.tagTypeFilterFieldName)
          return configuration.icon_tagType;
      if(filter.fieldName == configuration.regulationFilterFieldName)
        return configuration.icon_regulation;
      if(filter.fieldName == configuration.patternFilterFieldName)
        return configuration.icon_pattern_filter;
      if(filter.fieldName == configuration.patternMultiFilterFieldName)
        return configuration.icon_pattern_filter;
      if(filter.fieldName == configuration.subFolderFilterFieldName)
        return configuration.icon_folder;
      if(filter.fieldName == configuration.departmentFilterFieldName || filter.fieldName == configuration.subDepartmentFilterFieldName) {
        if (currentUserDetailsProvider.isInstallationModeStandard()) {
          return configuration.icon_department;
        }
        else if (currentUserDetailsProvider.isInstallationModeLegal()) {
          return configuration.icon_matter;
        }
        return configuration.icon_department;
      }
      if(filter.fieldName == configuration.fileDuplicationsFilterFieldName)
        return configuration.icon_fileContent;
      if(filter.fieldName == configuration.sharePermissionFilterFieldName)
        return configuration.icon_share_permission_allow_read;
      if(filter.fieldName == configuration.metadataFilterFieldName)
        return configuration.icon_metadata;
      if(filter.fieldName == configuration.metadataTypeFilterFieldName)
        return configuration.icon_metadata;
      if(filter.fieldName == configuration.fileExtensionFilterFieldName)
        return configuration.icon_extension;
      if(filter.fieldName == configuration.fileTypeCategoryFilterFieldName)
        return configuration.icon_fileTypeCategory;
      if(filter.fieldName == configuration.fileSizeFilterFieldName)
        return configuration.icon_file_sizes;
      if(filter.fieldName == configuration.tagFilterFieldName)
          return configuration.icon_tag;
      if(filter.fieldName == configuration.tagTypeMismatchFilterFieldName)
        return configuration.icon_tagTypeMismatch;
      if(filter.fieldName == configuration.docTypeFilterFieldName ||filter.fieldName == configuration.subDocTypesFilterFieldName)
        return configuration.icon_docType;
      if(filter.fieldName == configuration.bizListItemAssociationFilterFieldName||filter.fieldName == configuration.bizListItemAssociationTypeFilterFieldName)
        return configuration.icon_bizListItemClients;
      if(filter.fieldName == configuration.bizListItemExtractionFilterFieldName )
        return configuration.icon_bizListItemClientExtraction;
      if(filter.fieldName == configuration.bizListItemExtractionTypeFilterFieldName && filter.value == Entities.DTOBizList.itemType_CONSUMERS)
        return configuration.icon_bizListItemConsumerExtraction;
      if(filter.fieldName == configuration.bizListItemExtractionTypeFilterFieldName && filter.value == Entities.DTOBizList.itemType_CLIENTS)
        return configuration.icon_bizListItemClientExtraction;
      if(filter.fieldName == configuration.bizListItemExtractionRuleFilterFieldName)
        return configuration.icon_bizListItemConsumerExtraction;
      if(filter.fieldName == configuration.fileCreatedDatesFilterFieldName)
        return configuration.icon_fileCreatedDate;
      if(filter.fieldName == configuration.fileModifiedDatesFilterFieldName)
        return configuration.icon_fileModifiedDate;
      if(filter.fieldName == configuration.fileAccessedDatesFilterFieldName)
        return configuration.icon_fileAccessedDate;
      if(filter.fieldName == configuration.functionalRoleFilterFieldName)
        return configuration.icon_functionalRole;
      if(filter.fieldName == configuration.mailFilterFieldName)
        return configuration.icon_mail;
      if(filter.fieldName == configuration.recipientDomainFilterFieldName || filter.fieldName == configuration.senderDomainFilterFieldName)
        return configuration.icon_domain;
      if(filter.fieldName == configuration.recipientAddressFilterFieldName)
        return configuration.icon_recipient_address;
      if(filter.fieldName == configuration.senderAddressFilterFieldName)
        return configuration.icon_sender_address;
      if(filter.contextName && configuration['icon_'+filter.contextName])
        return configuration['icon_'+filter.contextName];
      return '';
    };

    $scope.getFilterDisplayedName = function(filter) {
      return filterFactory.getFilterReportDisplayedName(filter);
    };

    $scope.toggleFiltersPaneVisibility = function() {
      $scope.isFiltersPaneVisible = !$scope.isFiltersPaneVisible;
      userSettings.setFiltersPaneVisibility($scope.isFiltersPaneVisible);

      $timeout(function () {
        apiUtils.triggerWindowResizeEvent();
      },0);
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportFilterTagsSetState,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
          /*
           * fireParam object:
           *   { initSearch: false, value: searchText, mode: mode}
           */
      if (fireParam.action === 'reportUpdateState') {
        refreshState();
      }
    })));
    // eventCommunicator.fireEvent(EEventServiceEventTypes.ReportNotifyReady, {
    //   action: 'reportState',
    //   generatorName: 'filterTags'
    // });  //must be after register handler


    $scope.loadSavedFilter = function(savedFilterId:number) {
      var savedFilter =(<ui.SavedDiscoveredFilter[]>$scope.savedFilters).filter(f=>f.id == savedFilterId)[0];
       if (savedFilter != null) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
          action: 'loadSavedFilter',
          filterData: JSON.parse(savedFilter.savedFilterDto.rawFilter),
        });
      }
    };

    $scope.savedFiltersListCtrl = {};

    var loadSavedFiltersList = function() {
      filterResource.getFilters(function(filters:ui.SavedDiscoveredFilter[]) {
        $scope.savedFilters = filters;
        var itemsOptions:any[] = [];
        if ($scope.savedFilters) {
          setSavedFiltersParamList(itemsOptions, $scope.savedFilters);
          if ($scope.savedFiltersListCtrl.refreshList) {
            $scope.savedFiltersListCtrl.refreshList(itemsOptions);
          }
        }
      },function() {});
    };

    $scope.addNewSaveFilterDialog = function() {
      if(splitViewFilterService.getFilterData().hasCriteria()) {
        var thinFilterCopy = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
          if (value == null) {
            return undefined;
          }
          return value;
        });
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/saveFilterDialog.tpl.html',
            'saveFilterDialogCtrl',
            {
              filter: thinFilterCopy,
              lefts: $scope.viewLeftOptions,
              rights: $scope.viewRightOptions,
              leftEntityDisplayTypeName: $scope.leftPaneType.toString(),
              rightEntityDisplayTypeName: $scope.rightPaneType.toString(),
              isRightDisabled: $scope.viewRightDisabled,
              kendoFilter : splitViewFilterService.getFilterData().toKendoFilter(),
              rawFilter : splitViewFilterService.getThinFilterCopy(),
              displayType: Entities.EFilterDisplayType.DISCOVER
            },
            {
              size: 'md',
              keyboard: true,
              backdrop: false,
              windowClass: 'center-modal',
              animation: false,
              backdropClass: 'halfHeight'
            });

        dlg.result.then(function (newSavedFilter:ui.SavedDiscoveredFilter) {
          addNewSavedFilter(newSavedFilter)
        }, function () {

        });
      }
    };

    var addNewSavedFilter = function (newSavedFilter:ui.SavedDiscoveredFilter) {
      if (newSavedFilter.name == null || newSavedFilter.name.trim() == '') {
        return;
      }

      newSavedFilter.name = newSavedFilter.name ? (newSavedFilter.name.replace(new RegExp("[;,:\"']", "gm"), " ")) : null;
      if (newSavedFilter.name && newSavedFilter.name.trim() != '') {
        log.debug('add new tag to: ' + newSavedFilter.name);

        filterResource.addNew(newSavedFilter, function () {
          loadSavedFiltersList();
          notifySavedFiltersUpdate();
        }, function (error) {
          if (error.status == 400 &&error.data.type ==ERequestErrorCode.ITEM_ALREADY_EXISTS) {
            dialogs.error('Error', 'Filter with this name already exists. \n\nSelect another name.');
          }
          else {
            onError();
          }
        });
      }
    };

    var closeAllDdls = function() {
      $('html').trigger('click'); //to close all ddls
    };

    $scope.editSavedFilterDialog = function(savedFilter:ui.SavedDiscoveredFilter) {
      closeAllDdls();
      var dlg = dialogs.create(
          'common/directives/reports/dialogs/saveFilterDialog.tpl.html',
          'saveFilterDialogCtrl',
          {savedFilter:savedFilter,
            lefts:$scope.viewLeftOptions,
            rights:$scope.viewRightOptions,
            rightEntityDisplayTypeName:$scope.viewRightOptions.toString(),
            leftEntityDisplayTypeName:$scope.leftPaneType,
            right:$scope.rightPaneType.toString(),
            isRightDisabled:$scope.viewRightDisabled},
          {size:'md',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

      dlg.result.then(function(savedFilter){
        editSavedFilter(savedFilter)
      },function(){

      });
    };

    $scope.isInvertedFilter = function (filter:SingleCriteria) {
      return filter && (filter.operator === EFilterOperators.notEquals || filter.operator === EFilterOperators.doesNotContain );
    };

    $scope.isInvertedFilterEnabled = function (filter:SingleCriteria) {
      return filterFactory.isInvertedFilterEnabled(filter);
    };

    var editSavedFilter = function (savedFilter:ui.SavedDiscoveredFilter) {
      if(savedFilter.name==null || savedFilter.name.trim() =='') {
        return;
      }

      savedFilter.name = savedFilter.name ? (savedFilter.name.replace(new RegExp("[;,:\"']", "gm"), " ")) : null;
      if (savedFilter.name && savedFilter.name.trim() != '') {
        log.debug('update saved filter: ' + JSON.stringify(savedFilter));
       filterResource.updateFilter(savedFilter, function () {
         loadSavedFiltersList();
         notifySavedFiltersUpdate();
        }, function () {
          onError();
        });
      }
    };

    $scope.deleteSavedFilter = function (savedFilterId:number) {
      closeAllDdls();
      var dlg = dialogs.confirm('Confirmation', 'Are you sure you want do delete this saved filter?',null );
      dlg.result.then(function(btn){ //user confirmed deletion
        log.debug('delete saved filter: ' + savedFilterId);
        filterResource.deleteFilter(savedFilterId, function () {

          loadSavedFiltersList();
          notifySavedFiltersUpdate();
        },onError);
      },function(btn){
        // $scope.confirmed = 'You confirmed "No."';
      });
    };

    $scope.onSavedFilterSelected = function(selectedFilter) {
      $scope.loadSavedFilter(selectedFilter.item.id);
    };

    $scope.createSavedFilterParamOption = function(filter:ui.SavedDiscoveredFilter) {
      var newItem = new ParamOption<ui.SavedDiscoveredFilter>();

      newItem.id = filter.id;
      newItem.display = filter.name;
      newItem.value = filter;
      newItem.icon = filter.globalFilter ? 'fa fa-globe' : 'fa fa-lock';

      return newItem;
    };

    var setSavedFiltersParamList = function (itemsOptions:any[], filters) {
      filters.forEach(filter => {
        var newItem = $scope.createSavedFilterParamOption(filter);
        itemsOptions.push(newItem);
      });
    };

    $scope.getSavedFilters = function(itemsOptions:any[]) {
      if ($scope.savedFilters) {
        setSavedFiltersParamList(itemsOptions, $scope.savedFilters);
      }
      else {
        filterResource.getFilters(function(filters:ui.SavedDiscoveredFilter[]) {
          setSavedFiltersParamList(itemsOptions, filters);
        },function() {});
      }
    };

    $scope.onPredefinedFilterSelected = function(selectedFilter) {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction,{action: selectedFilter.item.value});
    };

    $scope.createPredefinedFilterParamOption = function(display:string, value:string,subdisplay?) {
      var newItem = new ParamOption();

      newItem.id = value;
      newItem.display = display;
      newItem.subdisplay = subdisplay;
      newItem.value = value;

      return newItem;
    };

    $scope.getPredefinedFilters = function(itemsOptions:any[]) {
      itemsOptions.push($scope.createPredefinedFilterParamOption("Un-grouped files", "filterByUngrouped",'Analysed documents that were not groupd'));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Group: files detached", "filterByGroupDetachedFiles"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Without mail related", "filterByNoneMailRelatedDataFiles"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Acl: Open access rights", "filterByOpenAccessRights"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Acl: Expired user", "filterByExpiredUser"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Media type: OneDrive", "filterByMediaTypeOneDrive"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Media type: SharePoint", "filterByMediaTypeSharePoint"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Media type: FileShare", "filterByMediaTypeFileShare"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Media type: Exchange365", "filterByMediaTypeEx"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Media type: Box", "filterByMediaTypeBox"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("DocType: unassociated", "doDocTypeUnassociated"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("Department: unassociated", "doDepartmentUnassociated"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("File current state: mapped", "fileCurrentStateMap"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("File current state: ingested", "fileCurrentStateIngested"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("File current state: ingest error", "fileCurrentStateError"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("File current state: pending analyze", "fileCurrentStateAnalyzable"));
      itemsOptions.push($scope.createPredefinedFilterParamOption("File current state: analyzed", "fileCurrentStateAnalyzed"));
    };

    var notifySavedFiltersUpdate = function() {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderSetState, {
        action: 'reportState',
        generatorName: 'filterTags'
      });
    };

    var onError = function() {
      dialogs.error('Error','Sorry, an error occurred.');
    };
  })
  .directive('pageFilterTags',function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/reports/filterTags.tpl.html',
      controller: 'pageFilterTagsCtrl',
     // require: '^splitView',
      replace:true,
      scope:{
        'getFilterName':'=',
        'leftPaneType':'=',
        'rightPaneType':'=',
        'viewRightDisabled':'=',
        'viewRightOptions':'=',
        'viewLeftOptions':'=',
        'readOnly':'=',
        'activeFiltersOnly':'=',
        'filterBarOption': '=',
        'isFiltersPaneVisible':'=',
        'filterService':'&',
      },
      link: function (scope:any, element, attrs) {
        scope.init(attrs.left);
      }
    };
  })
  .directive('activeFiltersPane',function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/reports/filtersTags.pane.tpl.html',
      controller: 'pageFilterTagsCtrl',
      // require: '^splitView',
      replace:true,
      scope:{
        'getFilterName':'=',
        'leftPaneType':'=',
        'rightPaneType':'=',
        'viewRightDisabled':'=',
        'viewRightOptions':'=',
        'viewLeftOptions':'=',
        'readOnly':'=',
        'activeFiltersOnly':'=',
        'filterService':'&',
        'isFiltersPaneVisible':'='
      },
      link: function (scope:any, element, attrs) {
        scope.init(attrs.left);
      }
    };
  })
  .directive('disablePageFilterTags',function () {
    return {
      // restrict: 'E',
      template: ' <span title="Toggle filter" ng-show="hasFilter" style="padding:0 8px;"><i ng-show="!disableRightFilter" class="fa fa-filter notice" ></i><i ng-show="disableRightFilter" class="fa fa-filter" style1="color:blue;"></i>' +
      ' <a style="line-height: 10px;vertical-align: middle;" ng-click="disableFilter(disableRightFilter=!disableRightFilter)" >' +
      '<i ng-show="!disableRightFilter" class="fa fa-check-square-o notice" ></i><i ng-show="disableRightFilter" class="fa fa-square-o" ></i></a></span>',
      controller: function($scope,eventCommunicator,splitViewFilterService) {
        var refreshState = function () {
          $scope.hasFilter = splitViewFilterService.getFilterData().hasCriteria();
        };

        $scope.disableFilter = function (disable:boolean) {
            eventCommunicator.fireEvent(EEventServiceEventTypes.ReportFilterTagsUserAction, {
              action: 'disableRightFilter',
              value:disable
            });
        };

        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportFilterTagsSetState, $scope, (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
              /*
               * fireParam object:
               *   { initSearch: false, value: searchText, mode: mode}
               */
              if (fireParam.action === 'reportUpdateState') {
                $scope.disableRightFilter = fireParam.disableRightFilter;
                refreshState();
              }
            })));
        refreshState();
      },
      // require: '^splitView',
      replace:true,
      scope:{},
      link: function (scope:any, element, attrs) {}
    };
  });


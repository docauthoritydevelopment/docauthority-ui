'use strict';

angular.module('directives.department', [/*'directives.dialogs.department',*/'resources.operations'])
  .controller('departmentMenuCtrl', function ($scope,$window,$element,$timeout,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                            currentUserDetailsProvider:ICurrentUserDetailsProvider,userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,
                                            departmentResource:IDepartmentResource,routerChangeService) {
      var log: ILog = Logger.getInstance('departmentMenuCtrl');
      var isMasterManager;
      var MAX_RECENTLY_USED: number = parseInt(configuration.max_recently_used_departments);
      $scope.iconClass = configuration.icon_department;

      $scope.init = function (isMaster) {
        isMasterManager = isMaster;
        if (isMasterManager) {
          registerCommunicationHandlers();
          initUserSettingsPreferences();
        }
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });

        $timeout(function () { //load only after page has loaded
          loadDepartments();
        }, 25);
      };

      $scope.$watch(function () {
        return $scope.selectedDepartmentId;
      }, function (value) {
        $scope.selectedDepartment = _.filter($scope.department, (department) => {
          return department.fileDepartmentDto.id === $scope.selectedDepartmentId;
        })[0];
      }, true);

      var setUserRoles = function () {
        $scope.isMngDepartmentConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDepartmentConfig]);
      };

      var loadDepartments = function () {
        log.debug('Departments ---- Bar Loaded');
        departmentResource.getDepartments($scope.onGetDepartmentsCompleted, onError);
      };

      var initUserSettingsActiveDepartment = function () {
        setActiveRecentlyUsedDepartments();

        $scope.$on(userSettings.activeRecentlyUsedDepartmentItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsedDepartments();
        });
      };

      $scope.toggleMinimizeToolbar = function () {
        $scope.minimizeToolbar = !$scope.minimizeToolbar;
        userSettings.setDepartmentsPreferences({minimizeToolbar: $scope.minimizeToolbar});

        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      };

      var initUserSettingsPreferences = function () {
        userSettings.getDepartmentsPreferences(function (preferences) {
          $scope.minimizeToolbar = preferences && typeof (<any>preferences).minimizeToolbar != 'undefined' ? (<any>preferences).minimizeToolbar : false;
        });
      };

      var setLastSelectedDepartmentItemId = function (lastSelectedId) {
        if (lastSelectedId) {
          userSettings.setCurrentDepartment(lastSelectedId);
        }
      };

      var setActiveRecentlyUsedDepartments = function () {
        userSettings.getRecentlyUsedDepartment(function (savedActiveRentlyUsedDepartmentIDs) {
            var approvedListOfRecntlyUsedItems = [];
            if ($scope.departmentList && $scope.departmentList.length > 0 && savedActiveRentlyUsedDepartmentIDs && savedActiveRentlyUsedDepartmentIDs.length > 0) {
              savedActiveRentlyUsedDepartmentIDs.forEach(s => {
                var foundItem = $scope.departmentList.filter(d => d.id == s)[0];
                if (foundItem) {
                  if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                    approvedListOfRecntlyUsedItems.push(foundItem.id);
                  }
                }
              });
            }

            $scope.savedRecentlyUsedDepartmentItems = _.filter($scope.departmentList, (department) => {
              return _.includes(approvedListOfRecntlyUsedItems, department.id);
            });

            if (approvedListOfRecntlyUsedItems.length > 0 && approvedListOfRecntlyUsedItems.length !== savedActiveRentlyUsedDepartmentIDs.length) {
              userSettings.setRecentlyUsedDepartment(approvedListOfRecntlyUsedItems);
            }
          }
        );
      };

      var saveRecentlyUsedDepartment = function (departmentToSave) {
        if (departmentToSave) {
          userSettings.getRecentlyUsedDepartment(function (savedActiveRecentlyUsedDepartmentIDs) {
            if (savedActiveRecentlyUsedDepartmentIDs) { //add current active to recently used
              var foundInRecentItems = savedActiveRecentlyUsedDepartmentIDs.filter(d => d == departmentToSave.id)[0];
              if (foundInRecentItems) { //if already exists remove it
                var foundIndex = savedActiveRecentlyUsedDepartmentIDs.indexOf(foundInRecentItems);
                savedActiveRecentlyUsedDepartmentIDs.splice(foundIndex, 1);
              }
              savedActiveRecentlyUsedDepartmentIDs.unshift(departmentToSave.id);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
              if (savedActiveRecentlyUsedDepartmentIDs.length > MAX_RECENTLY_USED) {
                savedActiveRecentlyUsedDepartmentIDs.splice(MAX_RECENTLY_USED, savedActiveRecentlyUsedDepartmentIDs.length - MAX_RECENTLY_USED); //limit size of list
              }
            }
            else {
              savedActiveRecentlyUsedDepartmentIDs = [departmentToSave.id];
            }
            userSettings.setRecentlyUsedDepartment(savedActiveRecentlyUsedDepartmentIDs);
          });
        }
      };

      $scope.getDepartmentStyleId = function(department) {
        if (department) {
          return splitViewBuilderHelper.getDepartmentStyleId(department);
        }
        return null;
      };

      $scope.getDepartmentDepth = function(department) {
        if (department) {
          return splitViewBuilderHelper.getDepartmentDepth(department);
        }
        return null;
      };

      $scope.onGetDepartmentsCompleted = function (departments: Entities.DTODepartment[]) {
        $scope.departmentList = departments;
        if (isMasterManager) {
          initUserSettingsActiveDepartment();
        }
      };

      $scope.$watch(function () {
        return $scope.selectedDepartmentItemToTag;
      }, function (value) {
        $scope.isAssignEnabled = !$scope.disabled && areSelectedItemsAuthorized();
      });

      $scope.$watch(function () {
        return $scope.selectedDepartmentItem;
      }, function (value) {
        if ($scope.selectedDepartmentItem) {

        }
      });

      $scope.toggleDepartmentToSelectedItems = function (department:Entities.DTODepartment) {
        var selectedItemDepartment = ($scope.selectedDepartmentItemToTag && $scope.selectedDepartmentItemToTag.length > 0) ? $scope.selectedDepartmentItemToTag[0].item.departmentDto : null;
        if (department && selectedItemDepartment && selectedItemDepartment.id == department.id) {
          $scope.removeDepartmentFromSelectedItems(department);
        }
        else {
          $scope.addDepartmentToSelectedItems(department);
        }
      };

      var showWarning = function(callback) {
        let warningRequired = _.some($scope.selectedDepartmentItemToTag, (item) => {
          return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
        });
        if(warningRequired) {
          userSettings.getDontShowAssociationWarning((dontShow) => {
            if (!dontShow) {
              var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
              dlg.result.then((dontShow) => {
                userSettings.setDontShowAssociationWarning(dontShow);
                callback();
              });
            }else {
              callback();
            }
          });
        }else {
          callback();
        }
      };

      $scope.addDepartmentToSelectedItems = function (departmentToAdd:Entities.DTODepartment) {
        if (departmentToAdd && $scope.selectedDepartmentItemToTag) {
          showWarning (() => {
            addDepartmentToItems($scope.selectedDepartmentItemToTag, $scope.selectedDepartmentItemToTag.map(s => getEntity(s)), departmentToAdd);
          });
        }
      };

      $scope.removeDepartmentFromSelectedItems= function (departmentToRemove:Entities.DTODepartment) {
        if (departmentToRemove && $scope.selectedDepartmentItemToTag) {
          showWarning (() => {
            removeDepartmentFromItems($scope.selectedDepartmentItemToTag, $scope.selectedDepartmentItemToTag.map(s => getEntity(s)), departmentToRemove);
          });
        }
      };

      var areSelectedItemsAuthorized = function():boolean {
        if($scope.selectedDepartmentItemToTag &&$scope.selectedDepartmentItemToTag.length>0) {
          return getAuthorizedItems($scope.selectedDepartmentItemToTag.map(i => getEntity(i)))[0]?true:false;
        }
        return false;
      };

      var addDepartmentToItems = function(originalItems, items:Entities.IDepartmentAuthorization[], departmentToAdd:Entities.DTODepartment) {
        if (departmentToAdd && items && items.length > 0) {
        var loopPromises = [];
        var approvedItems = getAuthorizedItems(items);
        if (approvedItems && approvedItems.length > 0) {
          approvedItems.forEach(function (selectedItem:Entities.IDepartmentAuthorization) {
            var performAddPromise = isSingleDepartmentReplacement(departmentToAdd, selectedItem);
            performAddPromise.then(function (replaced: boolean) {
              var addDepartment = departmentResource.addDepartment(departmentToAdd.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              addDepartment.$promise.then(function (updatedItem: any) {
                 updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  onError();
                  log.error('receive addDepartment failure' + error);
              });
              $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                if ($scope.refreshNeeded) {
                  $scope.refreshData();
                }
                else {
                  $scope.reloadData(); //no need when auto sync is false
                }
                routerChangeService.updateAsyncUserOperations();
              });
            });
          });
        }
      }
    };

    var isSingleDepartmentReplacement = function(departmentToAdd:Entities.DTODepartment,selectedItem:Entities.IDepartmentAuthorization):any {
      var deferred = $q.defer();
        if( selectedItem.departmentDto ) {
          if(!selectedItem.departmentDto.implicit){
            var dlg:any =  dialogs.confirm('Replace Department', 'You are about to replace existing department \''+selectedItem.departmentDto.name+'\' with \''+departmentToAdd.name+ '\'.<br><br></br>Continue?', null);
            dlg.result.then(function(){
              deferred.resolve(true);
             },function(){
              deferred.reject();
            });
          }
          else{
            deferred.resolve(false);
          }
        }
        else{
          deferred.resolve(false);
        }
        return deferred.promise;
    };

      var removeDepartmentFromItems= function (originalItems, items:Entities.IDepartmentAuthorization[], departmentToRemove:Entities.DTODepartment) {
        if (departmentToRemove && items && items.length > 0) {
          var approvedItems =getApprovedItemsForRemoval(items, departmentToRemove);
          var loopPromises = [];
          if (approvedItems && approvedItems.length > 0) {
            approvedItems.forEach(function (selectedItem) {
              var removeTag = departmentResource.removeDepartment(departmentToRemove.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              removeTag.$promise.then(function (updatedItem:any) {
                  log.debug('receive removeDepartment from selected items success for' + selectedItem['id']);
                  updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  onError();
                  log.error('receive removeDepartment from selected items failure' + error);
                });
            });
            $q.all(loopPromises).then(function (selectedItemsToUpdate) {
              if ($scope.refreshNeeded) {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
              routerChangeService.updateAsyncUserOperations();
            });
          }
        }
      };

      var getApprovedItemsForRemoval = function(items: Entities.IDepartmentAuthorization[], departmentToRemove: Entities.DTODepartment) {
        var approvedItems = items.filter(i => {
          var departmentDto =i.departmentDto ;
          return !departmentDto.implicit;
        });
        return approvedItems.filter(s=>s.deleteDepartmentAuthorized)
      };

      var getAuthorizedItems = function(items: Entities.IDepartmentAuthorization[]) {
        var approvedItems = items.filter(s => {
          return s.assignDepartmentAuthorized;
        });
        return approvedItems;
      };

      var getEntity = function(item): Entities.IDepartmentAuthorization {
        return item.item ? item.item : item; //kendo grid
      };

      $scope.doDepartmentFilter = function( department:Entities.DTODepartment,isSubtreeFilter:boolean) {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDepartmentMenuUserAction, {
          action: 'doDepartmentFilter',
          values:  [department],
          isSubtreeFilter:isSubtreeFilter
        });
      };

      var registerCommunicationHandlers=function() {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDepartmentMenuUserAction,$scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
            if (fireParam.action === 'removeDepartment') {
              var item=<Entities.IDepartmentAuthorization>fireParam.item;
              var departmentToRemove=fireParam.departmentToRemove;

              if($scope.displayType == fireParam.displayType) {
                removeDepartmentFromItems([item],[getEntity(item)], departmentToRemove);
              }
            }
            else if (fireParam.action === 'setDepartment') {
              var entity=<Entities.IDepartmentAuthorization>getEntity(fireParam.item);
              $scope.selectDepartmentDialog(false, $scope.addDepartmentToSelectedItems);
            }
        })));
      };

      var closeAllDdls=function() {
        $('html').trigger('click'); //to close all ddls
      };

      var updateSelectedItem=function(originalItem, updatedItem:Entities.IDepartmentAssignee) {
        var originalEntity =   getEntity(originalItem);
        originalEntity.departmentDto =  updatedItem.departmentDto;
        splitViewBuilderHelper.parseDepartment( originalEntity,configuration);
        if ((<any>originalItem).item) {
          if( originalItem.loaded) { //for folder tree view refresh
            originalItem.item = undefined; // force refresh of dataItem
            originalItem.set("item", originalEntity);
            originalItem.loaded(false);
          }
        }
      };

      $scope.doUnAssociationFilterBy = function() {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDepartmentMenuUserAction, {
          action: 'doDepartmentUnassociated',
          isSubtreeFilter: true
        });
      };


      $scope.doDepartmentFilterBy = function() {
        $scope.selectDepartmentDialog(true, function(selectedDepartment) {
          $scope.doDepartmentFilter(selectedDepartment, true);
        });
      };

      $scope.selectDepartmentDialog = function(filterMode:string, callback?:any) {
        var firstSelectedItem, activeDepartmentID;
        if ($scope.selectedDepartmentItemToTag && $scope.selectedDepartmentItemToTag[0])
        {
          firstSelectedItem = getEntity($scope.selectedDepartmentItemToTag[0]);
          activeDepartmentID = firstSelectedItem.departmentDto ? firstSelectedItem.departmentDto.id : null;
        }
        if(activeDepartmentID) {
          openSelectedDepartmentDialog(activeDepartmentID, filterMode, callback);
        }
        else {
          userSettings.getCurrentDepartment(function(savedActiveDepartmentID) {
            openSelectedDepartmentDialog(savedActiveDepartmentID, filterMode, callback);
          });
        }

        function openSelectedDepartmentDialog(activeDepartmentID:number, filterMode:string, callback?:any) {
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/selectDepartmentDialog.tpl.html',
            'selectDepartmentDialogCtrl',
            {filterMode: filterMode, changeSelectedItemId: activeDepartmentID},
            {
              size: 'md',
              keyboard: true,
              backdrop: false,
              windowClass: 'center-modal',
              animation: false,
              backdropClass: 'halfHeight'
            });

          dlg.result.then(function (selectedDepartment) {
            setLastSelectedDepartmentItemId(selectedDepartment ? selectedDepartment.id : null);
            if (callback) {
              callback(selectedDepartment);
            }
            saveRecentlyUsedDepartment(selectedDepartment);
          });
        }
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function() {
        dialogs.error('Error','Sorry, an error occurred.');
      };
    })
    .directive('departmentMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/departmentMenu.tpl.html',
        controller: 'departmentMenuCtrl',
        replace:true,
        scope:{
          'selectedDepartmentItemToTag':'=',
          'displayType':'=',
          'reloadData':'=',
          'refreshData':'=',
          'refreshNeeded':'=',
          'disabled':'=',
          'openManagePage':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
);



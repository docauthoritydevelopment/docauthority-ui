///<reference path='../../all.d.ts'/>
import ITaggable = Entities.ITaggable;
'use strict';


angular.module('directives.tags', [
  'directives.dialogs.tags',
  'directives.dialogs.tagTypes',
  'directives.dialogs.assignTag',
  'ui.bootstrap.accordion'
])
  .controller('tagsMenuCtrl', function ($scope,$element,$window,$timeout,Logger:ILogger,fileTagsResource:IFileTagsResource,filterFactory:ui.IFilterFactory,
                                        splitViewFilterService:IFilterService,fileTagResource,currentUserDetailsProvider:ICurrentUserDetailsProvider,propertiesUtils,userSettings:IUserSettingsService,
                                        $q,routerChangeService,eventCommunicator:IEventCommunicator,dialogs,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
    var log:ILog = Logger.getInstance('tagsMenuCtrl');
    $scope.userSearchActive = null;
    $scope.tagList = {};
    $scope.MAX_RECENTLY_USED =(configuration.max_tags_buttons_in_toolbar);
    $scope.MAX_TAGS_IN_LIST = (configuration.max_tags_in_list);
    $scope.MAX_RECENTLY_USED_ITEMS_PER_TOOLBAR = parseInt(configuration.max_recently_used_tags);
    $scope.savedRecentlyUsedTagItemsPerType=[];
    var _this=this;
    _this.savedActiveRecentlyUsedTagsPerTypeIDs={};

    $scope.init = function (left) {
      $scope.icon_tagType = configuration.icon_tagType;
      $scope.icon_tag = configuration.icon_tag;
      $scope.isLeft = left;
      $scope.toggleCriteriaOperation();
      setUserRoles();
      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
        setUserRoles();
      });
      $timeout(function() //load only after page has loaded
      {
        fileTagsResource.getAllTags($scope.onGetAllTagsCompleted, onError);
      });
    };

    var setUserRoles = function() {
      $scope.isMngTagConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngTagConfig]);
      $scope.isTagFileAssociationUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignTagFilesBatch]);
    };

    $scope.isTagFilesDisabled = function() {
      return !$scope.isTagFileAssociationUser || ($scope.displayType.equals( EntityDisplayTypes.all_files) && !splitViewFilterService.getFilterData().hasCriteria());
    };

    var areSelectedItemsAuthorized = function() {
      if($scope.selectedItemsToTag &&$scope.selectedItemsToTag.length>0) {
        return getAuthorizedItems($scope.selectedItemsToTag.map(i => getEntity(i)))[0]?true:false;
        /*return $scope.selectedItemsToTag.filter(s=> {
          return  (s.assignTagsAuthorized || s.deleteTagsAuthorized) ||
            (s.item && (s.item.assignTagsAuthorized ||s.item.deleteTagsAuthorized ));
        })[0]!=null;*/
      }
      else {
        return false;
      }
    };

    $scope.$watch(function () {
      return $scope.selectedItemsToTag;
      }, function (value) {
      $scope.isAssignEnabled = areSelectedItemsAuthorized();
    });
    var initUserSettings = function () {

       $scope.$on(userSettings.activeRecentlyUsedTagTypeItemUpdatedEvent, function (sender, value) {
         initRecentlyUsedData();
        initUserSettingsPreferences();

      });

      initRecentlyUsedData();
      initUserSettingsPreferences();
    };

    $scope.toggleMinimizeToolbar=function(index:number)
    {
      $scope.minimizeToolbar[index]=!$scope.minimizeToolbar[index];
      userSettings.setTagsPreferences({minimizeToolbar:$scope.minimizeToolbar});
      $timeout(function () {
        $(window).trigger('resize'); // Force IE 11 to fix grids heights
     //   $window.dispatchEvent(new Event("resize"));
      }, 0);
    }
    var initUserSettingsPreferences = function () {

       userSettings.getTagsPreferences(function(preferences){
         if(preferences && typeof (<any>preferences).minimizeToolbar!='undefined')
         {
           $scope.minimizeToolbar =(<any>preferences).minimizeToolbar;

         }
         else if(  $scope.savedRecentlyUsedTagTypeItems &&   $scope.savedRecentlyUsedTagTypeItems.length>0)
         {
           $scope.minimizeToolbar=[];
           $scope.savedRecentlyUsedTagTypeItems.forEach(r=>$scope.minimizeToolbar.push(false));
         }
         // _.defer(() => {
         //   $(window).trigger('resize'); // Force IE 11 to fix grids heights
         // });
      });
    };

    $scope.onGetAllTagsCompleted = function (fileTagLists:Entities.DTOFileTagTypeAndTags[]) {
      log.debug('File Tags ---- Bar Loaded');
      $scope.tagTypes = fileTagLists.map(l=>l.fileTagTypeDto);

      fileTagLists.forEach(l=> { //prepare $scope.tagList
        initTagList((<Entities.DTOFileTagType>l.fileTagTypeDto).id, l.fileTags)
      })
      initUserSettings(); //initRecentlyUsedData and minimizeToolbar

    };




    var validateTagTypesStillExistsAndLimitList = function (savedActiveRecentlyUsedTagTypeIDs:number[], approvedListOfRecentlyUsedItems) {
      var changed=false;
      if( $scope.tagTypes&& $scope.tagTypes.length>0) {
        for (var i = 0; i < savedActiveRecentlyUsedTagTypeIDs.length; i++) {
          var tagTypeId = savedActiveRecentlyUsedTagTypeIDs[i];
          var foundItem = $scope.tagTypes.filter(d=>d.id == tagTypeId)[0];
          if (foundItem) {
            if (approvedListOfRecentlyUsedItems.length < $scope.MAX_RECENTLY_USED) {
              approvedListOfRecentlyUsedItems.push(foundItem);
            }
          }
          else {
            savedActiveRecentlyUsedTagTypeIDs.splice(i, 1); //unexisting save id -> remove it
            changed = true;
          }
        }
        ;
        return changed;
      }
      else {
        throw ('tagType list is missing');
      }
    };
    var validateTagItemsStillExistsAndLimitList = function (savedTagItemIDs:string[], tagTypeId, approvedListOfRecentlyUsedItems) {
      var changed=false;
      if( $scope.tagList) {
        for (var i = 0; i < savedTagItemIDs.length; i++) {
          var savedTagItemID = savedTagItemIDs[i];
          var tagItems = $scope.tagList[tagTypeId];
          if (tagItems && tagItems[0]) {
            var foundItem = tagItems.filter(t=>t.id == savedTagItemID)[0];
            if (foundItem) {
              if (approvedListOfRecentlyUsedItems.length < $scope.MAX_RECENTLY_USED_ITEMS_PER_TOOLBAR) {
                approvedListOfRecentlyUsedItems.push(foundItem);
              }
            }
            else {  //a saved item is no longer exists
              var foundListItemIndex = savedTagItemIDs.indexOf(savedTagItemID);
              savedTagItemIDs.splice(foundListItemIndex, 1);
              changed = true;
            }
          }
          else {
            //list type not exists any more
            //     changed = true;
            //    var foundListIndex = _this.savedActiveRecentlyUsedTagsPerTypeIDs[tagTypeId];
            //   typeRecenlyTags.splice(foundListIndex, 1);
          }
        }
        return changed;
      }
      else {
        throw ('tagList list is missing');
      }
    };
    var prepareTagItemsList = function (tageItemToSave:Entities.DTOFileTag,savedActiveRecntlyUsedTagIDsForType) {
      var foundInRecentItems = savedActiveRecntlyUsedTagIDsForType.filter(d=>d == tageItemToSave.id)[0];
      if (foundInRecentItems) { //if already exists remove it
        var foundIndex = savedActiveRecntlyUsedTagIDsForType.indexOf(foundInRecentItems);
        savedActiveRecntlyUsedTagIDsForType.splice(foundIndex, 1);
      }
      savedActiveRecntlyUsedTagIDsForType.unshift(tageItemToSave.id);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
      if (savedActiveRecntlyUsedTagIDsForType.length > $scope.MAX_RECENTLY_USED_ITEMS_PER_TOOLBARSED) {
        savedActiveRecntlyUsedTagIDsForType.splice($scope.MAX_RECENTLY_USED_ITEMS_PER_TOOLBARSED, savedActiveRecntlyUsedTagIDsForType.length - $scope.MAX_RECENTLY_USED_ITEMS_PER_TOOLBARSED); //limit size of list
      }

    };



    var initRecentlyUsedData = function()
    {
      userSettings.getRecentlyUsedTagTypes(function(tagsPerTypeIDs)
      {
        var approvedListOfRecentlyUsedItems=[];
        if( tagsPerTypeIDs && tagsPerTypeIDs.length >0  ) {
          _this.savedActiveRecentlyUsedTagTypeIDs= _.uniq(tagsPerTypeIDs);
          var changed = validateTagTypesStillExistsAndLimitList(_this.savedActiveRecentlyUsedTagTypeIDs, approvedListOfRecentlyUsedItems);
          if(changed) {
            log.debug('Found tag type ids that does not exists any more in recently used tags toolbar items, save list');
            userSettings.setRecentlyUsedTagTypes(_this.savedActiveRecentlyUsedTagTypeIDs);
          }
        }
        else  //first initialization, no recently used tag yet
        {
          var numOfButtonsForInitialSetup:number = 5;   // Ariel asked for first 10 - may get too crowded for POC defaults

          var tagTypesById:Entities.DTOFileTagType[] = [];
          var selectedTagTypesById:Entities.DTOFileTagType[] = [];
          $scope.tagTypes.forEach(t => tagTypesById.push(t));
          tagTypesById.sort(
              (x:Entities.DTOFileTagType,y:Entities.DTOFileTagType) =>
                  ((x.id > y.id) ? 1 : ((x.id < y.id)? -1 : 0)));
          _this.savedActiveRecentlyUsedTagTypeIDs=[];
          for (var inx=0;(inx<tagTypesById.length && inx<numOfButtonsForInitialSetup);inx++) {
            _this.savedActiveRecentlyUsedTagTypeIDs.push(tagTypesById[inx].id);
            selectedTagTypesById.push(tagTypesById[inx]);
          }
          selectedTagTypesById.sort(propertiesUtils.sortAlphbeticFun('name'));
          selectedTagTypesById.forEach(t => approvedListOfRecentlyUsedItems.push(t));

          // _this.savedActiveRecentlyUsedTagTypeIDs=[$scope.tagTypes[0].id];
          // approvedListOfRecentlyUsedItems.push($scope.tagTypes[0]);

        }
        $scope.savedRecentlyUsedTagTypeItems = approvedListOfRecentlyUsedItems;
        initRecentlyUsedTagItemsForAllRecentlyUsedTypes();
        refreshToolbarOptionsVisibility();
        $timeout(function () {
          let isMsie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
          if (isMsie11) {
            var tmpEvent = $window.document.createEvent("Event");
            tmpEvent.initEvent("resize", false, true);
            $window.dispatchEvent(tmpEvent);
          }
          else {
            $window.dispatchEvent(new Event("resize"));
          }
        }, 0); //make grids get the right height when line number changes
      });
    };

    var refreshToolbarOptionsVisibility = function()
    {
      for(var i=0; i<$scope.tagToolbars.length; i++)
      {
        var t = $scope.tagToolbars[i];
        var prevVisible =  t.visible?true:false;
        t.visible=$scope.savedRecentlyUsedTagTypeItems&&(<Entities.DTOFileTagType[]>$scope.savedRecentlyUsedTagTypeItems).filter(s=>s.id== t.element.id)[0]!=null;
        if( t.visible!=prevVisible) {
          $timeout(function () {
            $(window).trigger('resize'); // Force IE 11 to fix grids heights
        //    $window.dispatchEvent(new Event("resize"));
          }, 0);
        }
      }
    };

    var initRecentlyUsedTagItemsForAllRecentlyUsedTypes = function() {
      if ($scope.savedRecentlyUsedTagTypeItems && $scope.tagList) {
        userSettings.getRecentlyUsedTagItemsPerType(  function(allSavedTagItemIDs) {
          _this.savedActiveRecentlyUsedTagsPerTypeIDs=allSavedTagItemIDs?allSavedTagItemIDs:{};
          var changed = false;
          $scope.savedRecentlyUsedTagTypeItems.forEach(t=> {
                var recentlyTagIDs:any[] = _this.savedActiveRecentlyUsedTagsPerTypeIDs[t.id];
                changed =  initRecentlyUsedTagItems(t.id,recentlyTagIDs);
              }
          );
          if(changed) {
            userSettings.setRecentlyUsedTagItemsPerType(_this.savedActiveRecentlyUsedTagsPerTypeIDs);
          }
        });

      }

    }

    var initRecentlyUsedTagItems = function(tagTypeId:number,recentlyTagIDs:string[]) {
      var changed = false;
      if ($scope.isTagTypeInRecentlyUsed(tagTypeId) && recentlyTagIDs && recentlyTagIDs[0]) {

        var approvedListOfRecentlyUsedItems = [];
        changed = validateTagItemsStillExistsAndLimitList(recentlyTagIDs, tagTypeId, approvedListOfRecentlyUsedItems);

        $scope.savedRecentlyUsedTagItemsPerType[tagTypeId] = approvedListOfRecentlyUsedItems;
      }
      return changed;
    }

    var initTagList = function(tagTypeId,tags:Entities.DTOFileTag[])
    {
      $scope.tagList[tagTypeId] = tags;
      var theTagType:Entities.DTOFileTagType =  $scope.tagTypes.filter(t=>t.id==tagTypeId)[0];
      if(tags&&tags[0]&& typeof tags[0].type.tagItemsCount== 'undefined') {
        theTagType.tagItemsCount = tags.length;
      }
      else {
        theTagType.tagItemsCount = 0;
      }
      initToolBarOptions();

    }

    var initToolBarOptions = function()
    {
      var tagTypeToolbars = (<Entities.DTOFileTagType[]>$scope.tagTypes).map(t=>{
        var toolbar = new ToolbarItem();
        toolbar.element = t;
        toolbar.name = (<Entities.DTOFileTagType>t).name + ' ('+(<Entities.DTOFileTagType>t).tagItemsCount+')';
        toolbar.changeShowToolbarModel = function(val:ToolbarItem)
        {
          val.visible = !val.visible;
          $scope.toggleRecentlyUsedButton(val.element);
        }
        toolbar.iconClass = configuration.icon_tagType+' tag-'+$scope.getTagTypeStyleId((<Entities.DTOFileTagType>t));
        return toolbar;
      });
      $scope.tagToolbars=tagTypeToolbars;
    }

    var saveNewRecentlyUsedTagItem = function(tagItemToAdd:Entities.DTOFileTag) {
      if (tagItemToAdd && $scope.isTagTypeInRecentlyUsed(tagItemToAdd.type.id)) {

        var savedTagItemIDs:any[] = _this.savedActiveRecentlyUsedTagsPerTypeIDs ? _this.savedActiveRecentlyUsedTagsPerTypeIDs[tagItemToAdd.type.id] : null;
        if (savedTagItemIDs && savedTagItemIDs.length > 0) { //add current active to recenlty used
          prepareTagItemsList(tagItemToAdd, savedTagItemIDs);

        }
        else { //first item to save for type
          savedTagItemIDs = [tagItemToAdd.id];
        }

        _this.savedActiveRecentlyUsedTagsPerTypeIDs[tagItemToAdd.type.id] = savedTagItemIDs;
        initRecentlyUsedTagItems(tagItemToAdd.type.id, savedTagItemIDs); //immidiateUI refresh
        userSettings.setRecentlyUsedTagItemsPerType(_this.savedActiveRecentlyUsedTagsPerTypeIDs);
      }
    }

    var updateServerWithRecentUsaedTagTypes = _.debounce(()=> {
      userSettings.setRecentlyUsedTagTypes(_this.savedActiveRecentlyUsedTagTypeIDs);
    },2000);

    var saveNewRecentlyUsedTagType = function(tagTypeItemToAdd:Entities.DTOFileTagType) {
      if (tagTypeItemToAdd && !$scope.isTagTypeInRecentlyUsed(tagTypeItemToAdd.id)) {
       _this.savedActiveRecentlyUsedTagTypeIDs.push(tagTypeItemToAdd.id);
        $scope.savedRecentlyUsedTagTypeItems.push(tagTypeItemToAdd); //Adds toolbar immidialty to display, if max reached it will be removed after settings update
        updateServerWithRecentUsaedTagTypes();
      }
    }

    var removeRecentlyUsedTagType = function(tagTypToRemove:Entities.DTOFileTagType) {
      if (tagTypToRemove && $scope.isTagTypeInRecentlyUsed(tagTypToRemove.id)) {
        if (_this.savedActiveRecentlyUsedTagTypeIDs.length > 1) { //do not allow to delete last one
          var index1 = _this.savedActiveRecentlyUsedTagTypeIDs.indexOf(tagTypToRemove.id);
          _this.savedActiveRecentlyUsedTagTypeIDs.splice(index1, 1);
          var index = $scope.savedRecentlyUsedTagTypeItems.indexOf(tagTypToRemove);
          $scope.savedRecentlyUsedTagTypeItems.splice(index, 1);
          updateServerWithRecentUsaedTagTypes();
        }
      }
    }

    $scope.toggleRecentlyUsedButton = function(tagType:Entities.DTOFileTagType)
    {
      if( $scope.isTagTypeInRecentlyUsed(tagType.id))
      {
        removeRecentlyUsedTagType(tagType);
      }
      else {
        saveNewRecentlyUsedTagType(tagType);
      }
    }
    $scope.isTagTypeInRecentlyUsed = function(tagTypeId)
    {
      return $scope.savedRecentlyUsedTagTypeItems &&
          $scope.savedRecentlyUsedTagTypeItems.filter(s=>s.id==tagTypeId)[0]
    }

    $scope.openNewTagDialog = function(tagType:Entities.DTOFileTagType)
    {
      var dlg = dialogs.create(
          'common/directives/reports/dialogs/newTagDialog.tpl.html',
          'newTagDialogCtrl',
          {tagType:tagType,tagTypes:$scope.tagTypes},
          {size:'md'});

      dlg.result.then(function(data:any){
        var newTag:Entities.DTOFileTag = data.tag;
        addNewTag(newTag);
        if(data.createAnother) {
          $timeout(function () {
            $scope.openNewTagDialog(tagType);
          })
        }
      },function(){

      });
    }

    $scope.openEditTagDialog = function(tagItem:Entities.DTOFileTag, tagType:Entities.DTOFileTagType)
    {
      var dlg = dialogs.create(
          'common/directives/reports/dialogs/newTagDialog.tpl.html',
          'newTagDialogCtrl',
          {tagType:tagType,tagTypes:$scope.tagTypes,tagItem:tagItem},
          {size:'md'});

      dlg.result.then(function(newTag:Entities.DTOFileTag){
        editTag(newTag);
      },function(){

      });
    }

    $scope.openDeleteTagDialog = function(tagItem:Entities.DTOFileTag) {
      if(tagItem) {
        if(tagItem.type.system) {
          var dlg = dialogs.notify('Delete tag type', "Cannot delete tag of type "+tagItem.type.name+" that is a system type", null);
          return;
        }

        var tagItemCopy =  angular.copy(tagItem);
        tagItemCopy.id = (<any>tagItem).originalTagId?(<any>tagItem).originalTagId: tagItem.id;

        var dlg = dialogs.confirm('Confirmation', "You are about to delete tag type named: '" + tagItem.name + "'. Continue?", null);
        dlg.result.then(function (btn) { //user confirmed deletion
            log.debug('about to delete saved tag: ' + tagItem.name);
            fileTagsResource.deleteTag(tagItemCopy.id,tagItemCopy.type.id, function () {
              refreshTagList(tagItem.type.id);
              $scope.refreshData([tagItem.type.id]); //deep refresh from server is needed in case tag assignees are displayed and tag was deleted
            }, function (err) {
              dialogs.error('Error',err.data.message );
            });
          }
          , function (btn) {
            // $scope.confirmed = 'You confirmed "No."';
          });
      }
    };


    $scope.openEditTagTypeDialog = function( tagType:Entities.DTOFileTagType)
    {
      var dlg = dialogs.create(
          'common/directives/reports/dialogs/newTagTypeDialog.tpl.html',
          'newTagTypeDialogCtrl',
          {tagType:tagType},
          {size:'md'});

      dlg.result.then(function(newTagType:Entities.DTOFileTagType){
        editTagType(newTagType);
      },function(){

      });
    }
    $scope.openNewTagTypeDialog = function()
    {
      var dlg = dialogs.create(
          'common/directives/reports/dialogs/newTagTypeDialog.tpl.html',
          'newTagTypeDialogCtrl',
          {},
          {size:'md'});

      dlg.result.then(function(newTagType:Entities.DTOFileTagType){
        addNewTagType(newTagType);
      },function(){

      });
    }


    $scope.openDeleteTagTypeDialog = function(tagType:Entities.DTOFileTagType)
    {
      if(tagType) {
        if(tagType.system)
        {
          var dlg = dialogs.notify('Delete tag type', "Cannot delete tag type "+tagType.name+" that is a system type", null);
          return;
        }
        if(tagType.tagItemsCount>0)
        {
          var dlg = dialogs.notify('Delete tag type', "Cannot delete tag type "+tagType.name+" that have child tags.", null);
          return;
        }
        fileTagsResource.getAssignedFilesForTagType(tagType,function (tagDetails:Entities.DTOAggregationCountItem<Entities.DTOFileTagType>[],totalAggregatedCount){
          var allFiles = totalAggregatedCount;
          var dlg = dialogs.confirm('Confirmation', "You are about to delete tag type named: '"+tagType.name + "' which has "+allFiles+" associated files. Continue?", null);
          dlg.result.then(function (btn) { //user confirmed deletion

                log.debug('about to delete saved tag: ' + tagType.name+' with '+allFiles+' assosiated files');
                fileTagsResource.deleteTagType(tagType.id, function () {
                  initRecentlyUsedData();
                  $scope.refreshData(); //deep refresh from server is needed in case tag assignees are displayed and tag name was change
                }, function () {

                  dialogs.error('Error','Failed to delete tag type: '+tagType.name);
                });

              }
              , function (btn) {
                // $scope.confirmed = 'You confirmed "No."';
              });


        },onError);
      }
    };

    var refreshTagList = function (tagTypeId) {
      if (tagTypeId) {
        fileTagsResource.getTagListByTagType(tagTypeId, function (tags:Entities.DTOFileTag[]) {
          initTagList(tagTypeId,tags);
          var recentlyTagIDs:any[] = _this.savedActiveRecentlyUsedTagsPerTypeIDs[tagTypeId];
          initRecentlyUsedTagItems(tagTypeId, recentlyTagIDs);
        }, onError);
      }
    }
    var addNewTag = function (newTag:Entities.DTOFileTag) {

      newTag.name =  newTag.name?( newTag.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
      if ( newTag.name &&  newTag.name.trim() != '') {
        newTag.description =  newTag.description?( newTag.description.replace(new RegExp("[']", "gm"), "\"")):null;
        log.debug('add new tag to: ' +  newTag.type.id + ' ' +  newTag.name);
        fileTagsResource.addNewTag( newTag.type.id, newTag, function (added) {
          var refreshItemId = added.id;
          if($scope.parseTagItem)
          {
            refreshItemId = $scope.parseTagItem(added);
          }
          refreshTagList( newTag.type.id);
          if( $scope.refreshNeeded)
          {
            $scope.refreshData([refreshItemId]);
          }
          else {
            $scope.reloadData(refreshItemId); //no need when auto sync is false
          }
        }, function (error) {
          if(error && error.userMsg)
          {
            dialogs.notify('Note',error.userMsg);
          }
          else{
            dialogs.error('Error','Failed to create new tag: '+newTag.name);
          }

        });
      }
    }
    var editTag = function (tagToUpdate:Entities.DTOFileTag) {

      tagToUpdate.name =  tagToUpdate.name?( tagToUpdate.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
      if ( tagToUpdate.name &&  tagToUpdate.name.trim() != '') {
        tagToUpdate.description  =  tagToUpdate.description?( tagToUpdate.description.replace(new RegExp("[']", "gm"), "\"")):null;
        log.debug('update tag to: ' +  tagToUpdate.type.id + ' ' +  tagToUpdate.name);
        fileTagsResource.updateTag( tagToUpdate, function () {
          var refreshItemId = tagToUpdate.id;
          if($scope.parseTagItem)
          {
            refreshItemId = $scope.parseTagItem(tagToUpdate);
          }
          refreshTagList( tagToUpdate.type.id);
          $scope.refreshData([refreshItemId]); //deep refresh from server is needed in case tag assignees are displayed and tag name was change

        }, function () {
          dialogs.error('Error','Failed to edit tag: '+tagToUpdate.name);
        });
      }
    }

    var addNewTagType = function (newTagType:Entities.DTOFileTagType) {

      newTagType.name =  newTagType.name?( newTagType.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
      if ( newTagType.name &&  newTagType.name.trim() != '') {
        newTagType.description =  newTagType.description?( newTagType.description.replace(new RegExp("[']", "gm"), "\"")):null;
        log.debug('add new tag to: ' +  newTagType.id + ' ' +  newTagType.name);
        fileTagsResource.addNewTagType( newTagType,  function (added:Entities.DTOFileTagType) {
          $scope.tagTypes.unshift(added);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
          initTagList(added.id,[]);
          if( $scope.refreshNeeded)
          {
            $scope.refreshData([added.id], null,true);
          }
          else {
            $scope.reloadData(added.id); //no need when auto sync is false
          }
        }, function (error) {
          if(error && error.userMsg)
          {
            dialogs.notify('Note',error.userMsg);
          }
          else{
            dialogs.error('Error','Failed to add tag type: '+newTagType.name);
          }

        });
      }
    }

    var editTagType = function (newTagType:Entities.DTOFileTagType) {

      newTagType.name =  newTagType.name?( newTagType.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
      if ( newTagType.name &&  newTagType.name.trim() != '') {
        newTagType.description =  newTagType.description?( newTagType.description.replace(new RegExp("[']", "gm"), "\"")):null;
        log.debug('update tag type to: ' +  newTagType.id + ' ' +  newTagType.name);
        fileTagsResource.updateTagType( newTagType, function (updated:Entities.DTOFileTagType) {
          var index = $scope.tagTypes.indexOf(t=>t.id==updated.id);
          $scope.tagTypes[index]=updated;
          $scope.refreshData([newTagType.id]); //deep refresh from server is needed in case tag assignees are displayed and tag name was change
        }, function () {
          dialogs.error('Error','Failed to edit tag type: '+newTagType.name);
        });
      }
    }


    var showWarning = function(callback) {
      let warningRequired = _.some($scope.selectedItemsToTag, (item) => {
        return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
      });
      if(warningRequired) {
        userSettings.getDontShowAssociationWarning((dontShow) => {
          if (!dontShow) {
            var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
            dlg.result.then((dontShow) => {
              userSettings.setDontShowAssociationWarning(dontShow);
              callback();
            });
          }else {
            callback();
          }
        });
      }else {
        callback();
      }
    };

    $scope.addTagToSelectedItems = function (tagItemToAdd:Entities.DTOFileTag) {
      if (tagItemToAdd && $scope.selectedItemsToTag && $scope.selectedItemsToTag.length > 0) {
          addTagToItems($scope.selectedItemsToTag, $scope.selectedItemsToTag.map(s => getEntity(s)), tagItemToAdd, $scope.displayType);
      }
    };

    var addTagToItems = function (originalItems, itemsToTag:Entities.ITagsAuthorization[],tagToAdd:Entities.DTOFileTag,displayType:EntityDisplayTypes) {
      if (tagToAdd && itemsToTag && itemsToTag.length > 0) {
        showWarning (() => {
          var tagResource: IfileTagResource = fileTagResource(displayType);
          var loopPromises = [];
          var approvedItems = getAuthorizedItems(itemsToTag);
          if (approvedItems && approvedItems.length > 0) {
            approvedItems.forEach(function (selectedItem: ITaggable) {
              var performAddPromise = isSingleTagTypeReplacement(tagToAdd, selectedItem);
              performAddPromise.then(function (replaced: boolean) {
                var addTag = tagResource.addTag(tagToAdd.id, selectedItem.id);
                var deferred = $q.defer();
                loopPromises.push(deferred.promise);
                addTag.$promise.then(function (updatedItem: any) {
                    updateSelectedItem(originalItems.filter(o => o.id == updatedItem.id)[0], updatedItem);
                    deferred.resolve(updatedItem);

                  },
                  function (error) {
                    deferred.resolve();
                    if (error.status == 400 && error.data && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                      dialogs.error('Associate tag', 'Cannot associate tag to group ' + selectedItem['groupName'] + ' right now.</BR>There is already running operation for this group.</BR>Try again later.');
                    }
                    else {
                      onError();
                    }
                    log.error('receive addTag failure' + error);

                  });
                //TODO: this is not really waiting for all to be queued since they are queued one by one
                $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                  if ($scope.refreshNeeded) {
                    $scope.refreshData();
                  }
                  else {
                    $scope.reloadData(); //no need when auto sync is false
                  }
                  saveNewRecentlyUsedTagItem(tagToAdd);
                  routerChangeService.updateAsyncUserOperations();
                });
              });

            });

          }
        });
      }
    }
    var isSingleTagTypeReplacement = function(tagToAdd:Entities.DTOFileTag,selectedItem:Entities.ITaggable):any {
      var deferred = $q.defer();
      if( tagToAdd.type.singleValueTag && selectedItem.fileTagDtos ) {
        var tagToReplace = selectedItem.fileTagDtos.filter(d => !d.implicit && d.type.id==tagToAdd.type.id)[0];
        if(tagToReplace){
          var dlg:any =  dialogs.confirm('Replace Tag', 'You are about to replace existing tag \''+tagToReplace.name+'\' with \''+tagToAdd.name+ '\'.<br><br></br>Continue?', null);
          dlg.result.then(function(){
            deferred.resolve(true);
          },function(){
            deferred.reject();
          });
        }
        else{
          deferred.resolve(false);
        }
      }
      else{
        deferred.resolve(false);
      }
      return deferred.promise;
    };

    var addRemoveTagSuccess = function (tagItem) {
      if ($scope.refreshNeeded) {
        $scope.refreshData();
      }
      else {
        $scope.reloadData(); //no need when auto sync is false
      }
      saveNewRecentlyUsedTagItem(tagItem);
      routerChangeService.updateAsyncUserOperations();
    };

    $scope.openAddRemoveTagDialog = function(tagItem:Entities.DTOFileTag) {
      let baseFilterField = configuration[EFilterContextNames.convertEntityDisplayTypeToBaseFilterField($scope.displayType)];
      let baseFilterValue = $scope.selectedItemsToTag[0].item ? ($scope.selectedItemsToTag[0].item.id ? $scope.selectedItemsToTag[0].item.id : $scope.selectedItemsToTag[0].id) : $scope.selectedItemsToTag[0].id;

        let dlg = dialogs.create(
        'common/directives/reports/dialogs/addRemoveTagDialog.tpl.html',
        'addRemoveTagDialogCtrl',
        { tagItem: tagItem,
                displayType: $scope.displayType,
                addedFilter: $scope.addedFilter.filter,
                baseFilterField: baseFilterField,
                baseFilterValue: baseFilterValue,
                selectionTxt: $scope.addedFilter.selectionTxt},
        {size:'md'});

      dlg.result.then(function(res:any) {
        var tagResource:IfileTagResource = fileTagResource();
        var params = {filter: null, baseFilterField: null, baseFilterValue: null};

        let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
        if ($scope.addedFilter.filter) {
          filterData.setPageFilters($scope.addedFilter.filter);
        }

        params.filter = filterData.toKendoFilter();

        if (res.applyToSelectedItemsOnly) {
          params.baseFilterField = baseFilterField;
          params.baseFilterValue = baseFilterValue;
        }

        if (res.action == "add") {
          if (!res.overrideExplicit || !res.overrideImplicit) {
            var tagTypeExplicitFilter = filterFactory.createExplicitTagTypeAssociationFilter();
            var tagTypeFilter = filterFactory.createTagTypeFilter();

            let newNotExplicitCriteria = tagTypeExplicitFilter([tagItem.type], ECriteriaOperators.and, EFilterOperators.notEquals);
            let newExplicitCriteria = tagTypeExplicitFilter([tagItem.type], ECriteriaOperators.and, EFilterOperators.equals);
            let newNotTagTypeCriteria = tagTypeFilter([tagItem.type], ECriteriaOperators.and, EFilterOperators.notEquals);

            if (!res.overrideExplicit) { //} && res.overrideImplicit) {
              filterData.setPageFilters(newNotExplicitCriteria);
            }
            /*else if (!res.overrideImplicit) {
              if (res.overrideExplicit) {
                //filterData.setPageFilters(new OrCriteria([newExplicitCriteria], [newNotTagTypeCriteria]));

                //filterData.setPageFilters(newExplicitCriteria);
                //params.filter = filterData.toKendoFilter();
                //tagResource.addTag(tagItem.id, params,function () {});
                //filterData = _.cloneDeep(splitViewFilterService.getFilterData());
                //filterData.setPageFilters(newNotTagTypeCriteria);

                filterData = new OrCriteria([new AndCriteria([filterData],[newExplicitCriteria])], [new AndCriteria([filterData],[newNotTagTypeCriteria])]);
              }
              else {
                filterData.setPageFilters(newNotTagTypeCriteria);
              }
            }*/

            params.filter = filterData.toKendoFilter();
          }
          tagResource.addTag(tagItem.id, params,function () {
            addRemoveTagSuccess(tagItem);
          });
        }
        else if (res.action == "remove") {
          tagResource.removeTag(tagItem.id, params,function () {
            addRemoveTagSuccess(tagItem);
          });
        }
      },function(){

      });
    };

    $scope.isAssignBySelection = function () {
      return $scope.displayType == EntityDisplayTypes.files ||
             $scope.displayType == EntityDisplayTypes.groups ||
             $scope.displayType == EntityDisplayTypes.folders ||
             $scope.displayType == EntityDisplayTypes.folders_flat;
    };

    $scope.toggleTagToSelectedItems = function (tagItem:Entities.DTOFileTag) {
      if ($scope.isAssignBySelection() && $scope.isAssignEnabled) {
        var tagToToggle:Entities.DTOFileTag = tagItem == null?$scope.activeSelectedTagItem:tagItem;
        if (tagToToggle && $scope.selectedItemsToTag && $scope.selectedItemsToTag.length > 0) {
          var firstSelectedItem = $scope.selectedItemsToTag[0];
          var tags = (<any>firstSelectedItem).item ? (<any>firstSelectedItem).item.fileTagDtos : (<any>firstSelectedItem).fileTagDtos;
          if (isExplicitTagExists(tags, tagToToggle)) {
            $scope.removeTagFromSelectedItems(tagItem);
          }
          else {
            $scope.addTagToSelectedItems(tagItem);
          }
        }
      }
      else {
        $scope.openAddRemoveTagDialog(tagItem);
      }
    };

    var isExplicitTagExists = function (tags:Entities.DTOFileTag[], selectedTag:Entities.DTOFileTag) {
      if(tags&& tags[0]) {
        return tags.filter(t=>(t.id == selectedTag.id && (<any>t).isExplicit))[0] != null;
      }
      return false;
    };

    $scope.removeTagFromSelectedItems= function (tagItemToRemove:Entities.DTOFileTag) {

      if (tagItemToRemove && $scope.selectedItemsToTag && $scope.selectedItemsToTag.length > 0) {
          removeTagFromItems($scope.selectedItemsToTag, $scope.selectedItemsToTag.map(s => getEntity(s)), tagItemToRemove, $scope.displayType);
      }
    };

   var removeTagFromItems = function (originalItems,items:Entities.ITagsAuthorization[], tagItemToRemove:Entities.DTOFileTag,displayType:EntityDisplayTypes) {
      if (tagItemToRemove && items && items.length > 0) {
        showWarning ( () => {
          var approvedItems = getApprovedItemsForRemoval(items, tagItemToRemove);
          var loopPromises = [];
          if (approvedItems && approvedItems.length > 0) {
            var tagResource: IfileTagResource = fileTagResource(displayType);
            approvedItems.forEach(function (selectedItem: Entities.ITaggable) {
              var removeTag = tagResource.removeTag(tagItemToRemove.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              removeTag.$promise.then(function (updatedItem: any) {
                  log.debug('receive removeTag from selected items success for' + selectedItem['id']);
                  updateSelectedItem(originalItems.filter(o => o.id == updatedItem.id)[0], updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  log.error('receive removeTag from selected items failure' + error);
                  if (error.status == 400 && error.data && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                    dialogs.error('Unassociate tag ', 'Cannot unassociate tag from group \'' + selectedItem['groupName'] + '\' right now.<BR>There is already running operation for this group.<BR>Try again later.');
                  }
                  else {
                    onError();
                  }
                });
            });
            $q.all(loopPromises).then(function (selectedItemsToUpdate) {
              if ($scope.refreshNeeded) {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
              saveNewRecentlyUsedTagItem(tagItemToRemove);
              routerChangeService.updateAsyncUserOperations();
            });
          }
        });
      }
    }
    $scope.getTagTypeStyleId = function(tag:Entities.DTOFileTagType)
    {
      return splitViewBuilderHelper.getTagTypeStyleId(tag);

    };


    var updateSelectedItem = function (originalItem ,updatedItem:Entities.ITaggable) {
      //selectedItem = JSON.parse(JSON.stringify(selectedItem));
      var originalEntity =   getEntity(originalItem);
      originalEntity.fileTagDtos =  updatedItem.fileTagDtos;
      splitViewBuilderHelper.parseTags(<any> originalEntity);

      if ((<any>originalItem).item) {
        if( originalItem.loaded) { //for folder tree view refresh
          originalItem.item = undefined; // force refresh of dataItem
          originalItem.set("item", originalEntity);
          originalItem.loaded(false);
      //    originalItem.load();
        }
        else {
      //    selectedItem.item = newItem;
        }
      }
      else {
     //   (<any>selectedItem).fileTagDtos = tags;
      }

    };



    $scope.toggleCriteriaOperation = function()
    {
      $scope.criteriaOperation =$scope.criteriaOperation== ECriteriaOperators.and?ECriteriaOperators.or:ECriteriaOperators.and;
    }
    var getApprovedItemsForRemoval = function(items: Entities.ITagsAuthorization[], tagToRemove: Entities.DTOFileTag) {
      var approvedItems = items.filter(i => {
        var fileTagDtos =i.fileTagDtos ;
        var theTag = fileTagDtos ? fileTagDtos.filter(a => a.id == tagToRemove.id && !a.implicit)[0] : null;
        return theTag != null ;
      });
      return approvedItems.filter(s=>s.deleteTagsAuthorized);
    };
    var getAuthorizedItems = function(items: Entities.ITagsAuthorization[]) {
      var approvedItems = items.filter(s => {
        return s.assignTagsAuthorized;
      });
      return approvedItems;
    };
    var getEntity = function(item): Entities.ITagsAuthorization
    {
      return item.item ? item.item : item; //kendo grid
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportTagsMenuSetState,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        /*
         * fireParam object:
         *   { initSearch: false, value: searchText, mode: mode}
         */
        if (fireParam.action === 'reportState') {
           $scope.criteriaOperation= fireParam.criteriaOperation;
        }
      })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridTagTypesManageUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        if (fireParam.action === 'openEditTagTypeItemDialog') {
           $scope.openEditTagTypeDialog( fireParam.tagType);
        }
        else if (fireParam.action === 'openEditTagItemDialog') {
           $scope.openEditTagDialog(fireParam.tagItem, fireParam.tagItem.type);
        }
        else if (fireParam.action === 'openAddTagTypeItemDialog') {
           $scope.openNewTagTypeDialog( fireParam.tagType);
        }
        else if (fireParam.action === 'openAddTagItemDialog') {
           $scope.openNewTagDialog(fireParam.tagItem, fireParam.tagItem.type);
        }
        else if (fireParam.action === 'openDeleteTagTypeDialog') {
          $scope.openDeleteTagTypeDialog( fireParam.tagType);
        }
        else if (fireParam.action === 'openDeleteTagItemDialog') {
          $scope.openDeleteTagDialog(fireParam.tagItem, fireParam.tagItem.type);
        }
      })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridTagsUserAction,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

        if (fireParam.action === 'removeTag') {
          var item=fireParam.item;
          var tagToRemove=fireParam.tagToRemove;
          var displayType = fireParam.displayType;
     //     if($scope.displayType == fireParam.displayType) { //displayType is needed to decide the resource type
            removeTagFromItems([item],[getEntity(item)], tagToRemove,displayType);
     //     }
        }
        else if (fireParam.action === 'addTag') {
          var itemsToTag=fireParam.itemsToTag;
          var tagToAdd=fireParam.tagToAdd;
          var displayType = fireParam.displayType;
          addTagToItems(itemsToTag, itemsToTag.map(s=>getEntity(s)),tagToAdd, displayType);

         }


      })));


    var closeAllDdls=function()
    {
      $('html').trigger('click'); //to close all ddls
    }

    $scope.doFilter = function (tagFilter:Entities.DTOFileTag[]) {
      closeAllDdls();
      if ( tagFilter.length>0) {
        saveNewRecentlyUsedTagItem(tagFilter[0]);
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportTagsMenuUserAction, {
          action: 'doTagFilter',
          values: tagFilter,
          criteriaOperation: $scope.criteriaOperation
        });

      }
    };

    $scope.doTagTypeMismatchFilter = function (tagType:Entities.DTOFileTagType) {
      closeAllDdls();
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportTagsMenuUserAction, {
        action: 'doTagTypeMismatchFilter',
        values:  [tagType],
       });
    };

    $scope.doTagTypeFilter = function (tagType:Entities.DTOFileTagType) {
      closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportTagsMenuUserAction, {
          action: 'doTagTypeFilter',
          values:  [tagType],
          criteriaOperation: $scope.criteriaOperation
        });
    };
    var onError = function()
    {
      dialogs.error('Error','Sorry, an error occurred.');
    }

  })
  .directive('tagsMenu',
  function () {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/reports/tagsMenu.tpl.html',
      controller: 'tagsMenuCtrl',
      transclude: true,
      replace:true,
      scope:{
        'selectedItemsToTag':'=',
        'displayType':'=',
        'reloadData':'=',
        'refreshData':'=',
        'refreshNeeded':'=',
        'disabled':'=',
        'tagToolbars':'=',
        'openManagePage':'=',
        'addedFilter':'='
      },
      link: function (scope:any, element, attrs) {
        scope.init(attrs.left);
      }
    };
  }
).directive('tagsManageMenu',
    function () {
      return {
        // restrict: 'E',
        template:
        '<div>' +
        '<button class="btn btn-flat btn-xs" ng-click="openNewTagTypeDialog()">' +
        '<i class="fa fa-plus"></i>' +
        '&nbsp;Add type</button>' +

        '<button class="btn btn-link btn-sm"  ' +
        'ng-class="{disabled:((!selectedTagItems)||selectedTagItems.length >1||selectedTagItem.type)}" ' +
        'ng-click="openEditTagTypeDialog(selectedTagItem.type?selectedTagItem.type:selectedTagItem)">' +
        '<i class="fa fa-pencil"></i>' +
        '&nbsp;Edit type</button>' +

        '<button class="btn btn-link btn-sm" ' +
        'ng-class="{disabled:((!selectedTagItems)||selectedTagItem.type||selectedTagItems.length>1)}" ' +
        'ng-click="openDeleteTagTypeDialog(selectedTagItem)">' +
        '<i class="fa fa-trash-o"></i>&nbsp;Delete type</button>' +
        '<button class="btn btn-flat btn-xs" ng-click="openNewTagDialog(selectedTagItem.type?selectedTagItem.type:selectedTagItem)"  style="margin-left: 12px;"' +
        'ng-class="{disabled:((!selectedTagItems)||selectedTagItems.length==0||selectedTagItems.length>1)}" >' +
        '<i class="fa fa-plus"></i>' +
        '&nbsp;Add tag</button>' +
        '<button class="btn btn-link btn-sm"  ' +
        'ng-class="{disabled:(!selectedTagItem || !selectedTagItem.type )}" ' +
        'ng-click="openEditTagDialog(selectedTagItem,selectedTagItem.type)">' +
        '<i class="fa fa-pencil"></i>' +
        '&nbsp;Edit tag</button>' +
        '<button class="btn btn-link btn-sm" ' +
        'ng-class="{disabled:(!selectedTagItem || !selectedTagItem.type )}" ' +
        'ng-click="openDeleteTagDialog(selectedTagItem)">' +
        '<i class="fa fa-trash-o"></i>&nbsp;Delete tag</button>' +
         '</div>',
        controller: 'tagsMenuCtrl',

        replace:true,
        scope:{
          'selectedTagItem':'=',
          'selectedTagItems':'=',
          'displayType':'=',
          'reloadData':'=',
          'refreshData':'=',
          'refreshNeeded':'=',
          'disabled':'=',
          parseTagItem: '=',

        },
        link: function (scope:any, element, attrs) {
          scope.init(false);
        }
      };
    }
);



﻿///<reference path='../../all.d.ts'/>
'use strict';
/* global kendo */


angular.module('directives.splitView',[
  'directives.splitViewFilter',
  'dialogs.main',
  'directives.dialogs.charts',
  'directives.findBy',
  'directives.dialogs.docTypes.manage',
  'directives.dialogs.department.select',
  'directives.dialogs.matter.select',
  'directives.dialogs.functionalRoles.manage',
  'directives.bizListAssociations',
  'directives.markedItems',
  'directives.splitView.toolbars.settings',
  'directives.splitView.filterByText',
  'directives.splitView.navigateByMenu',
  'directives.department',
  'directives.matter'
])
  .controller('splitViewCtrl', function ($scope,$element,$filter,Logger:ILogger,$state,$stateParams,$timeout,saveFilesBuilder:ISaveFileBuilder,$window,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                         propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,chartResource:IChartsResource,apiUtils,$http,$httpParamSerializer,
                                         pageTitle:IPageTitleService,splitViewFilterService:IFilterService,dialogs, userSettings:IUserSettingsService,routerChangeService, filterResource:IFilterResource, filterFactory:ui.IFilterFactory) {
    var log: ILog = Logger.getInstance('splitViewCtrl');

    $scope.departmentIconClass = configuration.icon_department;
    $scope.matterIconClass = configuration.icon_matter;
    $scope.docTypeIconClass = configuration.icon_docType;
    $scope.funcRoleIconClass = configuration.icon_functionalRole;
    $scope.tagTypeIconClass = configuration.icon_tagType;
    $scope.markedItemIconClass = configuration.icon_markedItem;
    $scope.extractionIconClass = configuration.icon_bizListItemConsumerExtraction;
    $scope.associationIconClass = configuration.icon_bizListItemClients;
    $scope.datePartitionsIconClass = configuration.icon_fileCreatedDate;
    $scope.createChartLeftInProgress = false;
    $scope.createChartRightInProgress = false;

    $scope.initialLoadParamsLeft = {
      //sortData
      filterData: $stateParams.filter && $stateParams.filter != 'none'
    };
    $scope.initialLoadParamsRight = _.clone($scope.initialLoadParamsLeft);
    $scope.initialLoadParamsRight.includeSubEntityData = $scope.changeIncludeSubEntityData;
    $scope.initialLoadParamsRight.collapsedData = $scope.collapsedData;
    $scope.initialLoadParamsRight.fullyContainedFilter = $scope.changeFullyContainedFilterState;

    $scope.includeSubEntityDataLeft = $scope.changeIncludeSubEntityData;
    $scope.collapsedDataRight = $scope.changeCollapsedDataRight?$scope.changeCollapsedDataRight:false;
    $scope.includeFullyContainedDataLeft = $scope.changeFullyContainedFilterState;
  //  $scope.collapsedDataRight=false;
    $scope.shouldSearchLeft = true;
    $scope.shouldSearchRight = true;

//    $scope.showToolbarsActive = false;
    $scope.hideMainToolbar = false;
    $scope.isFiltersPaneVisible = true;

    $scope.selectedLeftId = null;
    $scope.selectedLeftName = null;
    $scope.selectedRightId = null;
    $scope.selectedRightName = null;

    $scope.selectItemsLeft = null;
    $scope.selectItemsRight = null;
    $scope.showOnlyUnassigned = false;
    $scope.selectedViewId = $stateParams.view ? parseInt($stateParams.view): null;
    $scope.selectedView = null;


    $scope.tagMenuFilter = {filter: null, selectionTxt: {isLeftPane: true, leftTypeTxt: "", leftName: "", rightTypeTxt: "", rightName: ""}};

    $scope.exportLeftToExcelInProgress = false;
    $scope.exportRightToExcelInProgress = false;

    $scope.toggleShowOnlyUnassigned = function() {
      $scope.showOnlyUnassigned = !$scope.showOnlyUnassigned;
    };

    $scope.init = function() {
      updateTitle();
      setView();

      //   $scope.setFilterToGrids();
      $scope.onActivePaneClicked($scope.changeActivePan=='right'?false:true);
      $scope.resizeSplitter();
      $timeout($scope.resizeSplitter,0);

      setUserRoles();
      setUserSettings();

      $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent,  (sender, value: Users.UserAuthenticationData) => {
        setUserRoles();
        setUserSettings();
      });
    };

    var setView = function() {
      $scope.viewTitle = 'Discover view';
      $scope.newViewTitle = '';
      $scope.editTitleMode = false;
      $scope.disableEditSavedView = true;
      if ($scope.selectedViewId) {
        filterResource.getView($scope.selectedViewId,
          function (view) {
            $scope.selectedView = view;
            $scope.viewTitle = view.name;
          }, () => {});
        $scope.disableEditSavedView = localStorage.getItem("onSelectedView") ? JSON.parse(localStorage.getItem("onSelectedView")) : true;
      }
    };

    $scope.newViewTitle = '';
    $scope.editTitleMode = false;

    $scope.editViewTitle = function() {
      $scope.selectedView.name = $scope.newViewTitle;
      $scope.newViewTitle = '';
      $scope.editTitleMode = false;
      editSavedView($scope.selectedView);
    };

    var setUserSettings = function() {
      userSettings.getFiltersPaneVisibility((isVisible) => {
        if (angular.isDefined(isVisible) && isVisible != null) {
          $scope.isFiltersPaneVisible = isVisible;
        }
        else {
          $scope.isFiltersPaneVisible = true;
        }
      });

      userSettings.getMainToolbarHidden((isHidden) => {
        if (angular.isDefined(isHidden)) {
          $scope.hideMainToolbar = isHidden;
        }
        else {
          $scope.hideMainToolbar = false;
        }
      });
    };

    $scope.setMainToolbar = function() {
      $scope.hideMainToolbar = !$scope.hideMainToolbar;
      userSettings.setMainToolbarHidden($scope.hideMainToolbar);
      apiUtils.triggerWindowResizeEvent();
    };

    $scope.setFilterPane = function() {
      $scope.isFiltersPaneVisible = !$scope.isFiltersPaneVisible;
      userSettings.setFiltersPaneVisibility($scope.isFiltersPaneVisible);
      apiUtils.triggerWindowResizeEvent();
    };

    $scope.createDashboardWidget = function() {
      $scope.createChartLeftInProgress = true;
      let savedFilter = null;
      let kendoFilter = splitViewFilterService.toKendoFilter();
      if (kendoFilter) {
        savedFilter = new Entities.SavedFilterDto();
        savedFilter.name = "Current filter";
        savedFilter.globalFilter = false;
        savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
        savedFilter.filterDescriptor = kendoFilter;
        savedFilter.filterDescriptor.name = "Current filter";
      }
      routerChangeService.openAddEditChartWidgetDialog({data : {
          discoverName : $scope.leftPaneType.value,
          discoverRightName : $scope.rightPaneType.value,
          discoverEntityTypeId : $scope.leftEntityTypeId,
          filter : savedFilter,
          datePartitionId : $scope.leftEntityTypeId,
          globalFilter: false
        }});
      setTimeout(()=>{
        $scope.$apply(function () {
          $scope.createChartLeftInProgress = false;
        });
      },5000);
    }

    $scope.createRightDashboardWidget = function() {
      $scope.createChartRightInProgress = true;
      let kendoFilter = splitViewFilterService.toKendoFilter();
      let savedFilter = null;
      if ($scope.leftPaneType != EntityDisplayTypes.all_files) {
        let leftKendoFilter = filterFactory.createFilterByID($scope.leftPaneType)($scope.selectedLeftId, $scope.selectedLeftName).toKendoFilter();
        if (leftKendoFilter.field == "file.subDepartment") {
          leftKendoFilter.field = "departmentId";
        }
        if ($scope.includeSubEntityDataLeft) {
          if($scope.leftPaneType.equals(EntityDisplayTypes.folders)||$scope.leftPaneType.equals(EntityDisplayTypes.folders_flat)) {
            leftKendoFilter.field = 'subfolderId';
          }
          else if($scope.leftPaneType.equals(EntityDisplayTypes.departments)||$scope.leftPaneType.equals(EntityDisplayTypes.departments_flat)) {
            leftKendoFilter.field = 'subDepartmentId';
          }
          else if($scope.leftPaneType.equals(EntityDisplayTypes.matters)||$scope.leftPaneType.equals(EntityDisplayTypes.matters_flat)) {
            leftKendoFilter.field = 'subDepartmentId';
          }
          else if($scope.leftPaneType.equals(EntityDisplayTypes.doc_types))
          {
            leftKendoFilter.field = 'subDocTypeId';
          }
        }
        if (kendoFilter) {
          kendoFilter = {
            logic: "and",
            filters: [kendoFilter, leftKendoFilter]
          }
        }
        else {
          kendoFilter = leftKendoFilter;
        }
        savedFilter = new Entities.SavedFilterDto();
        savedFilter.name = "Current filter";
        savedFilter.globalFilter = false;
        savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
        savedFilter.filterDescriptor = kendoFilter;
        savedFilter.filterDescriptor.name = "Current filter";
      }
      routerChangeService.openAddEditChartWidgetDialog({data : {
          discoverName : $scope.rightPaneType.value,
          discoverEntityTypeId : $scope.rightEntityTypeId,
          filter : savedFilter,
          globalFilter: false
        }});
      setTimeout(()=>{
        $scope.$apply(function () {
          $scope.createChartRightInProgress = false;
        });
      },5000);
    }

    var setUserRoles=function() {
      $scope.isDemoUser =  currentUserDetailsProvider.isDemoUser();
      $scope.isViewTagsUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewTagTypes, Users.ESystemRoleName.ViewTagAssociation,Users.ESystemRoleName.MngTagConfig,Users.ESystemRoleName.AssignTag,Users.ESystemRoleName.AssignTagFilesBatch],null,true);
      $scope.isViewDocTypesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDocTypeTree,Users.ESystemRoleName.ViewDocTypeAssociation,Users.ESystemRoleName.AssignDocType],null,true);
      $scope.isViewDepartmentUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentTree, Users.ESystemRoleName.ViewDepartmentAssociation,Users.ESystemRoleName.AssignDepartment],null,true) && currentUserDetailsProvider.isInstallationModeStandard();
      $scope.isViewMatterUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDepartmentTree, Users.ESystemRoleName.ViewDepartmentAssociation,Users.ESystemRoleName.AssignDepartment],null,true) && currentUserDetailsProvider.isInstallationModeLegal();
      $scope.isViewBizListUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngBizLists,Users.ESystemRoleName.ViewBizListAssociation,Users.ESystemRoleName.AssignBizList],null,true);
      $scope.isViewFunctionalRolesUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.ViewDataRoleAssociation,Users.ESystemRoleName.AssignDataRole,Users.ESystemRoleName.MngRoles]);
      $scope.isRunActionsUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.RunActions]);
      $scope.isManageUserViewDataUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngUserViewData]);
      $scope.isRolesInitiated  =  true;  //To keep the order of toolbars
    };

    var w = angular.element($window);
    w.bind('resize', function () {
      $scope.$applyAsync(   $scope.resizeSplitter);
    });

    $scope.resizeSplitter=function() {
      var mainBody = $(".main-body-grid");
      var mainBodyBottom = mainBody.offset().top + mainBody.height();
      var splitter = $($element.find(".splitter-element"));
      var splitterHeight = mainBodyBottom-splitter.offset().top ;
      splitter.height(splitterHeight);
      $timeout(function() {
        var splitterData = ($element.find(".splitter-element")).data("kendoSplitter");
        splitterData ? splitterData.resize() : null;
      });
    };

    var updateTitle = function() {
      var selectedPart = $scope.selectedLeftName? ' > ' + $scope.selectedLeftName:'';
      var filterPart = $scope.searchTerm? ' > filter: ' + $scope.searchTerm:'';
      var title = 'Discover: '+$scope.right+'s' + ' by ' + $scope.left+filterPart+selectedPart;
      pageTitle.set(title);

      setTagMenuFilterTxt($scope.activePan == 'left');
    };

    $scope.clearSelected = function(clearRightOnly?:boolean) {
      if(!clearRightOnly) {
        $scope.selectedLeftId = null;
        $scope.selectedLeftName = null;
        $scope.selectItemsLeft = null;
        $scope.onSelectedItemLeft('');
      }
      $scope.selectedRightId = null;
      $scope.selectedRightName = null;
      $scope.selectItemsRight = null;
      $scope.onActivePaneClicked(true); //select left
      $scope.onSelectedItemRight(''); //clear from url
      updateTitle();
    };

    $scope.isActivePanTagsRefreshNeeded = function() {
      return  $scope.activePanType==EntityDisplayTypes.folders_flat;
    };

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDiscoverViewChanged, $scope, (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        if (fireParam.action === 'onSetFilter') {
          localStorage.setItem("onSelectedView", JSON.stringify(false));
          $scope.disableEditSavedView = false;
        }
        else if (fireParam.action === 'onClearFilter') {
          localStorage.setItem("onSelectedView", JSON.stringify(false));
          $scope.disableEditSavedView = false;
        }
        else if (fireParam.action === 'onViewChanged') {
          localStorage.setItem("onSelectedView", JSON.stringify(false));
          $scope.disableEditSavedView = false;
        }
      })));

    var setTagMenuFilter = function() {
      if($scope.includeSubEntityDataLeft) {
        if($scope.leftPaneType==EntityDisplayTypes.folders || $scope.leftPaneType==EntityDisplayTypes.folders_flat) {
          $scope.tagMenuFilter.filter = filterFactory.createSubFolderFilter()($scope.selectedLeftId,$scope.selectedLeftName);
        }
        else if($scope.leftPaneType==EntityDisplayTypes.departments || $scope.leftPaneType==EntityDisplayTypes.departments_flat) {
          $scope.tagMenuFilter.filter = filterFactory.createSubDepartmentFilter()($scope.selectedLeftId,$scope.selectedLeftName);
        }
        else if($scope.leftPaneType==EntityDisplayTypes.matters || $scope.leftPaneType==EntityDisplayTypes.matters_flat) {
          $scope.tagMenuFilter.filter = filterFactory.createSubMatterFilter()($scope.selectedLeftId,$scope.selectedLeftName);
        }
        else if($scope.leftPaneType==EntityDisplayTypes.doc_types) {
          $scope.tagMenuFilter.filter = filterFactory.createSubDocTypesFilter()($scope.selectedLeftId,$scope.selectedLeftName);
        }
        else {
          if ($scope.leftPaneType != EntityDisplayTypes.all_files) {
            $scope.tagMenuFilter.filter = filterFactory.createFilterByID($scope.leftPaneType)($scope.selectedLeftId,$scope.selectedLeftName);
          }
        }
      }
      else {
        if ($scope.leftPaneType != EntityDisplayTypes.all_files) {
          $scope.tagMenuFilter.filter = filterFactory.createFilterByID($scope.leftPaneType)($scope.selectedLeftId,$scope.selectedLeftName);
        }
      }
    };

    var setTagMenuFilterTxt = function(isLeftPane) {
      $scope.tagMenuFilter.selectionTxt.isLeftPane = isLeftPane;
      $scope.tagMenuFilter.selectionTxt.leftTypeTxt = $scope.left.toLowerCase() + ($scope.isDirectFilesToggleEnabled($scope.leftPaneType) && $scope.includeSubEntityDataLeft ? ' subtree ':' ');
      $scope.tagMenuFilter.selectionTxt.leftName = $scope.selectedLeftName;
      $scope.tagMenuFilter.selectionTxt.rightTypeTxt = "";
      $scope.tagMenuFilter.selectionTxt.rightName = "";

      if (!isLeftPane) {
        $scope.tagMenuFilter.selectionTxt.rightTypeTxt =  $scope.right.toLowerCase();
        $scope.tagMenuFilter.selectionTxt.rightName = $scope.selectedRightName;
      }
    };

    $scope.onActivePaneClicked = function(isLeft) {
      $scope.tagMenuFilter = {filter: null, selectionTxt: {isLeftPane: true, leftTypeTxt: "", leftName: "", rightTypeTxt: "", rightName: ""}};

      if (isLeft) {
        $scope.activePan = 'left';
        $scope.activePanType=$scope.leftPaneType;
        $scope.selectItemsActivePan = $scope.selectItemsLeft;

        setTagMenuFilterTxt(true);
      }
      else {
        $scope.activePan = 'right';
        $scope.activePanType=$scope.rightPaneType;
        $scope.selectItemsActivePan = $scope.selectItemsRight;

        setTagMenuFilter();
        setTagMenuFilterTxt(false);
      }

      $scope.onActivePaneChanged(  $scope.activePan);
    };

    $scope.reloadDataActivePan = function(params) {
      if ($scope.activePan == 'left') {
        $scope.reloadDataLeftPan.apply(this, [params]);
      }
      else {
        $scope.reloadDataRightPan.apply(this, [params]);
      }
    };

    $scope.refreshDataActivePan = function(params) {
      if ($scope.activePan == 'left') {
        $scope.refreshDataLeftPan.apply(this, [params]);
      }
      else {
        $scope.refreshDataRightPan.apply(this, [params]);
      }
    };

    $scope.onSelectLeft = function (id, name,idForRelocate) {
      log.debug('onExpandL  left was changed to ' + name);

      if($scope.selectedLeftId != id) {
        $scope.clearSelected();
        $scope.selectedLeftId = id;
        $scope.selectedLeftName = name;
        updateTitle();
      }
      $scope.onSelectedItemLeft(idForRelocate?idForRelocate:id);
    };

    $scope.onSelectItemsLeft = function (data) {
      $scope.selectItemsLeft = data;
      //if(data && data.length>0) {
      //  $scope.onActivePaneClicked(true);
      //}
      if($scope.activePan=='left') {
        $scope.selectItemsActivePan = $scope.selectItemsLeft;
      }
    };

    $scope.onSelectItemsRight = function (data) {
      $scope.selectItemsRight = data;
      if($scope.changeActivePan ==='right' && data && data.length>0) {
        $scope.activePan = 'right';
        $scope.onActivePaneClicked(false);
        $scope.selectItemsActivePan = $scope.selectItemsRight;
      }
      else if($scope.activePan=='right') {
         $scope.selectItemsActivePan = $scope.selectItemsRight;
      }
    };

    $scope.onSelectRight = function (id, name,idForRelocate) {
      // log.debug("onSelectRight1: id=" + id + ", selectedRightId=" + $scope.selectedRightId);
      if($scope.selectedRightId != id) {
        $scope.selectedRightId = id;
        $scope.selectedRightName = name;
        updateTitle();
      }
      $scope.onSelectedItemRight(idForRelocate?idForRelocate:id);
      // log.debug("onSelectRight2: id=" + id + ", selectedRightId=" + $scope.selectedRightId);
    };

    $scope.onSelectEntityTypeIdLeft = function (id) {
      $scope.leftEntityTypeId = id;
      $scope.activePanEntityTypeId = id;
    };

    $scope.onSelectEntityTypeIdRight = function (id) {
      $scope.rightEntityTypeId = id;
      $scope.activePanEntityTypeId = id;
    };

    $scope.viewRightOptionHandlerClick = function(view:EPaneOption) {
      $scope.clearSelected(true); //clear only right selected
      $scope.viewOptionHandler(view);
    };

    $scope.viewLeftChanged=function(view) {
       $scope.viewOptionHandler(view);
    };

    $scope.leftSortBy = function(fieldName:string,descDirection:boolean) {
      $scope.sortLeft = {fieldName:fieldName,descDirection:descDirection};
    };

    $scope.pageLeftChanged = function(pageNumber:number) {
      $scope.onPageLeftChanged(pageNumber);
    };

    $scope.pageRightChanged = function(pageNumber:number) {
      $scope.onPageRightChanged(pageNumber);
    };

    $scope.rightTxt = function() {
      if($scope.right && $scope.right[$scope.right.length-1] === 's') {
        return $scope.right + 'es';
      }
      return $scope.right + 's';
    };

    $scope.doFollowUp = function (followUp:EPaneOption) {
      if (!$scope.selectedRightId) {
        console.error('A ' + $scope.right + ' must be selected. Clicking on row to select one.');
        return;
      }
      var filterDataToRestore = $scope.onBeforeFollowUp(followUp);
      //setIsSwitchViewsDisabled(followUp.view);
      $scope.viewOptionHandler(followUp);
      $scope.onAfterFollowUp(filterDataToRestore);

    };

    $scope.onAfterSearch = function() {
      updateTitle();
    };

    $scope.filterLeft=null;
    $scope.leftSearched=false;
    $scope.filterRight=null;
    $scope.rightSearched=false;
    $scope.lazyFirstLoadRight= true;

    $scope.createSaveChartDialog = function(isLeft:boolean) {
      var chartInfo=isLeft?   createChartLeft():   createChartRight();

      var dlg = dialogs.create(
          'common/directives/dialogs/saveChartDialog.tpl.html',
          'saveChartDialogCtrl',
          chartInfo,
          {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal modal-content-height',animation:false,backdropClass:''});

      dlg.result.then(function(chartToSave){

      },function(){
        if(angular.equals($scope.name,''))
          $scope.name = 'You did not enter in your name!';
      });
    };

    var createSavedChartDefaultObj = function() {
      var newChart = new ui.SavedChart();
      newChart.chartType = ui.SavedChart.chartType_FILES_BY_ENTITY;
      newChart.chartDisplayType = ui.SavedChart.chartDisplayType_BAR_HORIZONTAL;
      newChart.sortType = ui.SavedChart.sortType_SIZE;
      newChart.maximumNumberOfEntries = 20;
      newChart.showOthers = false;
      return newChart;
    };

    var setSavedChartFilter = function(newChart:ui.SavedChart) {
      if( splitViewFilterService.getFilterData().hasCriteria()) {
        var titleFilterPart = ' (' + ($filter('translate')('filtered')).toLowerCase() + ')';
        newChart.name += titleFilterPart;
        let savedFilter = new Entities.SavedFilterDto();
        savedFilter.name = newChart.name + (new Date()).getTime(); //saved filter name is unique
        savedFilter.globalFilter = false;
        savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
        savedFilter.filterDescriptor = splitViewFilterService.toKendoFilter();
        newChart.savedFilterDto = savedFilter;
      }
    };


    $scope.showSunburstWidget = function(){
        $scope.createChartLeftInProgress = true;
        let savedFilter = null;
        let kendoFilter = splitViewFilterService.toKendoFilter();
        if (kendoFilter) {
          savedFilter = new Entities.SavedFilterDto();
          savedFilter.name = "Current filter";
          savedFilter.globalFilter = false;
          savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
          savedFilter.filterDescriptor = kendoFilter;
          savedFilter.filterDescriptor.name = "Current filter";
        }
        routerChangeService.openAddEditChartWidgetDialog({data : {
            initialChartType : "SUNBURST_CHART",
            discoverName : $scope.rightPaneType.value,
            filter : savedFilter,
            globalFilter: false
          }});
        setTimeout(()=>{
          $scope.$apply(function () {
            $scope.createChartLeftInProgress = false;
          });
        },5000);
    }

    $scope.showSunburstWidgetRight = function(){
        $scope.createChartRightInProgress = true;
        let kendoFilter = splitViewFilterService.toKendoFilter();
        let leftKendoFilter = filterFactory.createFilterByID($scope.leftPaneType)($scope.selectedLeftId, $scope.selectedLeftName).toKendoFilter();
        leftKendoFilter.field = 'subfolderId';
        if (kendoFilter) {
          kendoFilter = {
            logic: "and",
            filters: [kendoFilter, leftKendoFilter]
          }
        }
        else {
          kendoFilter = leftKendoFilter;
        }
        let savedFilter = new Entities.SavedFilterDto();
        savedFilter.name = "Current filter";
        savedFilter.globalFilter = false;
        savedFilter.rawFilter = splitViewFilterService.getThinFilterCopy();
        savedFilter.filterDescriptor = kendoFilter;
        savedFilter.filterDescriptor.name = "Current filter";
        let sendDataObject:any  = {
          initialChartType : "SUNBURST_CHART",
          discoverName : $scope.rightPaneType.value,
          filter : savedFilter,
          globalFilter: false
        }
        if ($scope.selectItemsLeft) {
          let selectedLeftItem = $scope.selectItemsLeft[$scope.selectItemsLeft.length-1].item;
          sendDataObject.minDepth =  selectedLeftItem.depthFromRoot;
        }
        routerChangeService.openAddEditChartWidgetDialog({data : sendDataObject});
        setTimeout(()=>{
          $scope.$apply(function () {
            $scope.createChartRightInProgress = false;
          });
        },5000);
    }

    var createChartRight = function() {
      var newChart = createSavedChartDefaultObj();
      var subtreePart = $scope.includeSubEntityDataLeft && $scope.isDirectFilesToggleEnabled($scope.leftPaneType) ? 'subtree' + ' ' : '';
      var fullyContainedPart = $scope.includeFullyContainedDataLeft && $scope.isFullyContainedToggleEnabled($scope.leftPaneType, $scope.rightPaneType) &&
      $scope.includeFullyContainedDataLeft != EFullyContainedFilterState.all ? $filter('translate')('state_' + $scope.includeFullyContainedDataLeft.toString()).toLowerCase() + ' ' : '';
      var selecteLeftTitlePart = $scope.selectedLeftName ? 'for ' + $scope.left.toLowerCase() + ' ' + subtreePart + $scope.selectedLeftName : '';
      newChart.name = $filter('translate')(EntityDisplayTypes.files.toString()) + 's by ' + $scope.right.toLowerCase() + ' ' + fullyContainedPart + selecteLeftTitlePart;
   //   var rightFilter:ui.IDisplayElementFilterData = $scope.getRightFilter();

      setSavedChartFilter(newChart);
      if($scope.rightPaneType == EntityDisplayTypes.doc_types) { //change url to flat since we need only values grater then 0 sorted desc
        newChart.populationJsonUrl = $scope.getRightUrl().replace(configuration.doc_types_url,configuration.doc_types_flat_url);
      }
      else if($scope.rightPaneType == EntityDisplayTypes.departments) {
        newChart.populationJsonUrl = $scope.getRightUrl().replace(configuration.departments_url,configuration.departments_flat_url);
      }
      else if($scope.rightPaneType == EntityDisplayTypes.matters) {
        newChart.populationJsonUrl = $scope.getRightUrl().replace(configuration.matters_url,configuration.matters_flat_url);
      }
      else {
        newChart.populationJsonUrl = $scope.getRightUrl();
      }
      newChart.mainEntityType = $scope.rightPaneType.toString();
      newChart.globalFilter = true;

      return newChart;
    };

    var createChartLeft = function() {
      var newChart = createSavedChartDefaultObj();
      newChart.name = $filter('translate')(EntityDisplayTypes.files.toString()) + 's by ' + $scope.left.toLowerCase();

      setSavedChartFilter(newChart);
      if($scope.leftPaneType == EntityDisplayTypes.doc_types){  //change url to flat since we need only values grater then 0 sorted desc
        newChart.populationJsonUrl = $scope.getLeftUrl().replace(configuration.doc_types_url,configuration.doc_types_flat_url);
      }
      else if($scope.leftPaneType == EntityDisplayTypes.departments){
        newChart.populationJsonUrl = $scope.getLeftUrl().replace(configuration.departments_url,configuration.departments_flat_url);
      }
      else if($scope.leftPaneType == EntityDisplayTypes.matters){
        newChart.populationJsonUrl = $scope.getLeftUrl().replace(configuration.matters_url,configuration.matters_flat_url);
      }
      else {
        newChart.populationJsonUrl = $scope.getLeftUrl();
      }
      newChart.mainEntityType = $scope.leftPaneType.toString();
      newChart.globalFilter = true;
      return newChart;
    };

    $scope.userFilterChanged = function(userFilterText) {
      $timeout(function() {
        if(userFilterText&& userFilterText.trim()!='') {
          $scope.filterLeft ={ filter: {field: "item.name", operator: "contains", value: userFilterText},isClientFilter:true };
        }
        else {
          $scope.clearUserFilter();
        }
      },200);
    };

    $scope.clearUserFilter = function() {
      $scope.userFilterText=null;
      $scope.filterLeft={filter:{},isClientFilter:true};
    };

    var onError = function() {
      dialogs.error('Error','Sorry, an error occurred.');
    };

    $scope.onExportLeftToExcel = function() {
      return exportToExcel(true);
    };

    $scope.onExportRightToExcel = function() {
      return exportToExcel(false);
    };

    $scope.onExportRightAsFilesToExcel=function(){
      var populationJsonUrl = GridBehaviorHelper.getDataUrlBuilder2(EntityDisplayTypes.files, $scope.leftPaneType,$scope.selectedLeftId,configuration,
        $scope.isDirectFilesToggleEnabled($scope.leftPaneType)? $scope.includeSubEntityDataLeft:null,
        $scope.isFullyContainedToggleEnabled($scope.leftPaneType,$scope.rightPaneType)?$scope.includeFullyContainedDataLeft:null,
        $scope.isCollapsedDataRightEnabled($scope.rightPaneType)?$scope.collapsedDataRight:null);

      let queryParams = getQueryParameters(populationJsonUrl);
      let checkParams = _.clone(queryParams);
      checkParams.pageSize = 1;
      checkParams.take = 1;
      checkParams.page = configuration.minimum_items_to_open_export_popup;
      if (populationJsonUrl.indexOf("?")>-1) {
        populationJsonUrl = populationJsonUrl.substring(0,populationJsonUrl.indexOf("?"));
      }
      let queryString = $httpParamSerializer(checkParams);
      $scope.exportRightToExcelInProgress = true;
      $http.get(populationJsonUrl +'?'+ queryString).then(function successCallback(response) {
        let gotALotOfRecords = (response.data && response.data.content && response.data.content.length > 0 )
        routerChangeService.addFileToProcess('As_files_'+ $filter('date')(Date.now(), configuration.exportNameDateTimeFormat),'as_files',queryParams,{},gotALotOfRecords);
        setTimeout(()=> {
          $scope.$apply(function () {
            $scope.exportRightToExcelInProgress = false;
          });
        },2000);
      }, function errorCallback(response) {
        $scope.exportRightToExcelInProgress = false;
      });
    };

    var createExcelParts = function(populationJsonUrl,rightFilter,fields,partNumber, fileName:string) {
      const params = {
        skip :(configuration.max_data_records_per_request*(partNumber-1)),
        pageSize:configuration.max_data_records_per_request,
        page: partNumber
      };

      const url =  apiUtils.buildUrl(populationJsonUrl,params);
      chartResource.getChartData(url, rightFilter ? rightFilter.filter : null,configuration.max_data_records_per_request,false,false, null,
        function (chartData:Dashboards.DtoCounterReport[], data) {
          saveFilesBuilder.saveAsExcelOneSheet(fields,data.content,fileName+' '+partNumber,'all files '+partNumber);
          $scope.exportRightToExcelInProgress = false;
          $scope.exportRightToExcelCompleted(true);
        }, function () {
          $scope.exportRightToExcelInProgress = false;
        });
    };

    $scope.onExportLeftToPdf = function() { return exportToPdf(true); };
    $scope.onExportRightToPdf = function() { return exportToPdf(false); };

    var getQueryParameters = function(str) {
      if (str.indexOf('?')>-1) {
        str = str.substring(str.indexOf('?')+1);
      }
      else {
        return {};
      }
      let ans = (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = decodeURIComponent(n[1]),this}.bind({}))[0];
      return ans;
    };

    var exportToExcel = function(isLeft:boolean) {
      log.debug('export '+' to excel');
      let exportType ;
      let elementsNum ;
      let queryParams;
      let qURL = '';
      if (isLeft) {
        exportType = $scope.leftPaneType.value;
        elementsNum = $scope.totalElementsLeft ;
        qURL = $scope.getLeftUrl();
        queryParams = getQueryParameters($scope.getLeftUrl());
      }
      else {
        exportType = $scope.rightPaneType.value;
        elementsNum = $scope.totalElementsRight;
        qURL = $scope.getRightUrl();
        queryParams = getQueryParameters($scope.getRightUrl());
      }
      let filterStr = JSON.stringify(splitViewFilterService.toKendoFilter());
      if (filterStr != null && filterStr != 'null') {
        queryParams.filter = filterStr;
      }
      queryParams.take = elementsNum;
      queryParams.skip = 0;
      queryParams.page = 1;
      queryParams.pageSize = elementsNum;
      if (elementsNum == null ){
        let checkParams = _.clone(queryParams);
        let needeedPage = configuration.minimum_items_to_open_export_popup;
        checkParams.pageSize = 1;
        checkParams.page = needeedPage;
        checkParams.take = 1;
        if (qURL.indexOf("?")>-1) {
          qURL = qURL .substring(0,qURL.indexOf("?"));
        }
        let queryString = $httpParamSerializer(checkParams);
        $http.get(qURL +'?'+ queryString).then(function successCallback(response) {
          let gotALotOfRecords = (response.data && response.data.content && response.data.content.length > 0 && response.data.pageNumber == needeedPage)
          routerChangeService.addFileToProcess(exportType + '_' + $filter('date')(Date.now(), configuration.exportNameDateTimeFormat), exportType, queryParams, {
            itemsNumber: elementsNum
          }, gotALotOfRecords);
        }, function errorCallback(response) {
        });
      }
      else {
        if (isLeft) {
          $scope.exportLeftToExcelInProgress = true;
        }
        else {
          $scope.exportRightToExcelInProgress = true;
        }
        routerChangeService.addFileToProcess(exportType + '_' + $filter('date')(Date.now(), configuration.exportNameDateTimeFormat), exportType, queryParams, {
          itemsNumber: elementsNum
        }, elementsNum > configuration.minimum_items_to_open_export_popup);
        setTimeout(()=>{
          $scope.$apply(function () {
            $scope.exportLeftToExcelInProgress = false;
            $scope.exportRightToExcelInProgress = false;
          });
        },2000);
      }
    };

    /*
    $scope.exportLeftToExcelCompleted = function(notFullRequest:boolean) {
      if(  $scope.exportLeftToExcelInProgress) {
        if (notFullRequest) {
          dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');
        }
        $scope.exportLeftToExcelInProgress = false;
      }
    };


    $scope.exportRightToExcelCompleted = function(notFullRequest:boolean) {
      if(  $scope.exportRightToExcelInProgress) {
        if (notFullRequest) {
          dialogs.notify('Note', 'Excel file do not contain all data.<br/><br/>Excel file created but records count limit reached ('+configuration.max_data_records_per_request+').');

        }
        $scope.exportRightToExcelInProgress = false;
      }
    };
    */

    $scope.showFollowUp = function() {
      $scope.isFollowUpDisabled($scope.leftPaneType, $scope.rightPaneType) || (!$scope.selectItemsRight || $scope.selectItemsRight.length < 1)
    }

    $scope.reloadDataLeftPan = function() {
      $scope.reloadDataLeft();
      $scope.refreshDataRight();
    };

    $scope.reloadDataRightPan = function() {
      $scope.reloadDataRight();
    };

    $scope.refreshDataLeftPan = function() {
      $scope.refreshDataLeft();
      $scope.refreshDataRight();
    };

    $scope.refreshDataRightPan = function() {
      $scope.refreshDataRight();
    };

    $scope.toggleIncludeSubEntityDataLeft = function() {
      $scope.includeSubEntityDataLeft=!$scope.includeSubEntityDataLeft;
      $scope.onIncludeSubEntityDataChanged($scope.includeSubEntityDataLeft);
    };

    $scope.toggleCollapsedDataRight = function() {
      $scope.collapsedDataRight=!$scope.collapsedDataRight;
      $scope.onCollapsedDataRightChanged($scope.collapsedDataRight);
    };

    $scope.fullyContainesStates = [
      EFullyContainedFilterState.fullyContained,
      EFullyContainedFilterState.notContained,
      EFullyContainedFilterState.all
    ];

    $scope.setIncludeFullyContainedDataLeft = function(val:EFullyContainedFilterState) {
      $scope.includeFullyContainedDataLeft = val;
      $scope.onFullyContainedDataChanged($scope.includeFullyContainedDataLeft);
    };

    $scope.fullyContainedDataLeftHasFilter = function() {
          return $scope.includeFullyContainedDataLeft!=EFullyContainedFilterState.all;
    };

    $scope.isFullyContainedFilter = function() {
      return $scope.includeFullyContainedDataLeft==EFullyContainedFilterState.fullyContained;
    };

    $scope.isShowPatternButton = function(thePane) {
      return (thePane == EntityDisplayTypes.patterns || thePane == EntityDisplayTypes.patterns_multi);
    };

    $scope.isShowMultiplePatterns = function(thePane) {
      return (thePane == EntityDisplayTypes.patterns_multi);
    };

    $scope.toggleLeftPatternButton = function(thePane) {
      let neededViewName =  EntityDisplayTypes.patterns_multi.value;
      if (thePane == EntityDisplayTypes.patterns_multi.value) {
        neededViewName = EntityDisplayTypes.patterns.value;
      }
      let neededView = null;
      $scope.viewLeftOptions.forEach((viewLeftOption:EPaneOption)=> {
        if (neededViewName == viewLeftOption.view) {
          neededView = viewLeftOption;
        }
      });
      $scope.viewOptionHandler(neededView);
    };

    $scope.toggleRightPatternButton = function(thePane) {
      let neededViewName =  EntityDisplayTypes.patterns_multi.value;
      if (thePane == EntityDisplayTypes.patterns_multi.value) {
        neededViewName = EntityDisplayTypes.patterns.value;
      }
      let neededView = null;
      $scope.viewRightOptions.forEach((viewLeftOption:EPaneOption)=> {
        if (neededViewName == viewLeftOption.view) {
          neededView = viewLeftOption;
        }
      });
      $scope.clearSelected(true); //clear only right selected
      $scope.viewOptionHandler(neededView);
    };

    $scope.isNotFullyContainedFilter = function() {
      return $scope.includeFullyContainedDataLeft==EFullyContainedFilterState.notContained;
    };

    var exportToPdf = function(isLeft:boolean) {
      log.debug('export '+' to pdf');
      if(isLeft) {
        $scope.exportLeftTopdf = {fileName: $scope.left + '.pdf'};
      }
      else {
        $scope.exportLeftTopdf = {fileName: $scope.right + '.pdf'};
      }
    };

    $scope.onSaveView = function(isNew) {
      localStorage.setItem("onSelectedView", JSON.stringify(true));
      if (isNew) {
        openAddNewSavedViewDialog();
      }
      else {
        openEditSavedViewDialog();
      }
    };

    var openEditSavedViewDialog = function() {
      var view =_.clone($scope.selectedView);
      if(splitViewFilterService.getFilterData().hasCriteria()) {
        if (!view.savedFilterDto) {
          (<ui.SavedDiscoveredFilter>view).savedFilterDto = new Entities.SavedFilterDto();
        }
        (<ui.SavedDiscoveredFilter>view).savedFilterDto.filterDescriptor = splitViewFilterService.getFilterData().toKendoFilter();
        (<ui.SavedDiscoveredFilter>view).savedFilterDto.rawFilter = splitViewFilterService.getThinFilterCopy();
      }
      else {
        (<ui.SavedDiscoveredFilter>view).savedFilterDto = null;
      }
      if (view.savedFilterDto) {
        (<ui.SavedDiscoveredFilter>view).savedFilterDto.name = (<ui.SavedDiscoveredFilter>view).name;
      }
      (<ui.SavedDiscoveredFilter>view).leftEntityDisplayTypeName = $scope.leftPaneType.toString();
      (<ui.SavedDiscoveredFilter>view).rightEntityDisplayTypeName =  $scope.rightPaneType.toString();
      (<ui.SavedDiscoveredFilter>view).globalFilter = $scope.globalFilter;
      editSavedView(view);
    };

    var editSavedView = function(savedView:ui.SavedDiscoveredFilter) {
      filterResource.updateFilter(savedView, filter => {
        $scope.viewTitle = filter.name;
        $scope.selectedView.name = filter.name;
        $scope.disableEditSavedView = true;
        notifySavedFiltersUpdate();
      }, onError);
    };

    var allowUserEditView = function() {
      var username = currentUserDetailsProvider.getUserData() ? currentUserDetailsProvider.getUserData().username : '';
      return $scope.selectedView && (!$scope.selectedView.globalFilter || $scope.isManageUserViewDataUser || $scope.selectedView.username == username);
    };

    $scope.allowEditViewTitle = function() {
      return allowUserEditView() && !$scope.editTitleMode;
    };

    $scope.allowEditSavedView = function() {
      return allowUserEditView() && !$scope.disableEditSavedView;
    };

    var openAddNewSavedViewDialog = function() {
      var filterData = null;
      var kendoFilter = null;
      var thinViewCopy = null;
      if(splitViewFilterService.getFilterData().hasCriteria()) {
        filterData = JSON.stringify(splitViewFilterService.getFilterData(), function (key, value) {
          if (value == null) {
            return undefined;
          }
          return value;
        });

        kendoFilter = splitViewFilterService.getFilterData().toKendoFilter();
        thinViewCopy = splitViewFilterService.getThinFilterCopy();
      }
      var dlg = dialogs.create(
        'common/directives/reports/dialogs/saveFilterDialog.tpl.html',
        'saveFilterDialogCtrl',
        {
          filter: filterData,
          lefts: $scope.viewLeftOptions,
          rights: $scope.viewRightOptions,
          leftEntityDisplayTypeName: $scope.leftPaneType.toString(),
          rightEntityDisplayTypeName: $scope.rightPaneType.toString(),
          isRightDisabled: $scope.viewRightDisabled,
          kendoFilter : kendoFilter,
          rawFilter : thinViewCopy,
          displayType: Entities.EFilterDisplayType.DISCOVER_VIEW
        },
        {size:'md',keyboard:true,backdrop:false,windowClass:'center-modal',animation:false,backdropClass:'halfHeight'});

      dlg.result.then(function (newSavedView:ui.SavedDiscoveredFilter) {
        addNewSavedView(newSavedView)
      }, function () {

      });
    };

    var addNewSavedView = function (newSavedView:ui.SavedDiscoveredFilter) {
      if (newSavedView.name == null || newSavedView.name.trim() == '') {
        return;
      }

      newSavedView.name = newSavedView.name ? (newSavedView.name.replace(new RegExp("[;,:\"']", "gm"), " ")) : null;
      if (newSavedView.name && newSavedView.name.trim() != '') {
        log.debug('add new tag to: ' + newSavedView.name);

        filterResource.addNew(newSavedView, filter => {
          $scope.selectedViewId = filter.id;
          $scope.viewTitle = filter.name;
          $stateParams['view'] = filter.id;
          $state.transitionTo($state.current,
            $stateParams,
            {
              notify: true, inherit: true
            });
          notifySavedFiltersUpdate();
        }, function (error) {
          if (error.status == 400 &&error.data.type ==ERequestErrorCode.ITEM_ALREADY_EXISTS) {
            dialogs.error('Error', 'Filter with this name already exists. \n\nSelect another name.');
          }
          else {
            onError();
          }
        });
      }
    };

    var notifySavedFiltersUpdate = function() {
      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportHeaderSetState, {
        action: 'reportState',
        generatorName: 'splitView'
      });
    };

    $scope.$on(userSettings.gridViewPreferencesUpdatedEvent, function (sender, value) {
      $scope.isTemplateView = value;
    });

    $scope.bizListAssociationMenuType = Entities.DTOBizList.itemType_CLIENTS;
    $scope.bizListExtractionRulesMenuType = Entities.DTOBizList.itemType_CONSUMERS;

    $scope.dataGovernancePolicyObjectType=JSON.parse('{"id":16,"name":"Data governance","actionClass":"users","description":"","parentId":null,"parents":[16],"hasChildren":false}');

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridDataChangeUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'refreshSplitView') {
        $scope.refreshDataRight();
        $scope.refreshDataLeft();
      }
      else if (fireParam.action === 'reloadData') {
        $scope.reloadDataActivePan();
      }
      else if (fireParam.action === 'openNewTab') {
        $scope.openNewTab(fireParam.left,fireParam.right,fireParam.findIdLeft,fireParam.selectedItemRight, fireParam.withFilter);
      }
    })));

    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportMarkedItemMenuUserAction,$scope,(new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
      if (fireParam.action === 'refreshSplitView') {
        $scope.refreshDataRight();
        $scope.refreshDataLeft();
      }
    })));

    $scope.isRightViewItemDisabled = function(opt){
      return $scope.viewRightDisabled($scope.leftPaneType.toString(),opt);
    }

    $scope.checkTabKey  = function(keyEvent) {
      if (keyEvent.which === 9) {
        let isLeft = $scope.activePan == 'left';
        $scope.onActivePaneClicked(!isLeft);
        setTimeout(function () {
          if (isLeft) {
            $(".right-view-panel").find(".k-grid").find("table").focus();
          }
          else {
            $(".left-view-panel").find(".k-grid").find("table").focus();
          }
        },100);
        keyEvent.stopImmediatePropagation();
        keyEvent.preventDefault();
      }
    }

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });
  })
  .directive('splitView',
  function ($compile, $timeout) {
    return {
      // restrict: 'E',
      templateUrl: '/common/directives/reports/splitView.tpl.html',
      scope: {
        left: '=',
        right: '=',
        leftSorts: '=',
        rightSorts: '=',
        leftBodies: '=',
        rightBodies: '=',
        viewOptionHandler: '=',
        viewLeftOptions: '=',
        switchPanes: '=',
        leftPaneType: '=',
        rightPaneType: '=',
        viewRightOptions: '=',
        viewRightDisabled: '=',
        followUpOptions: '=',
        isFollowUpOptionDisabled: '=',
        isFollowUpDisabled: '=',
        isTagDisabled: '=',
        isChartDisabled: '=',
        reloadDataLeftPan:'=',
        reloadDataRightPan:'=',
        isDirectFilesToggleEnabled:'=',
        isFullyContainedToggleEnabled:'=',
        isCollapsedDataRightEnabled:'=',
        isFindByEnabled:'=',
        isAttestationEnabled:'=',
        isFilterDatesEnabled:'=',
        isShowSunburstEnabled:'=',
        isDocTypeDisabled:'=',
        isDepartmentDisabled:'=',
        getFilterDisplayedName:'=',
        onSelectedItemRight:'=',
        onSelectedItemLeft:'=',
        onClearFilter:'=',
        onSetFilter:'=',
        onPageLeftChanged:'=',
        onPageRightChanged:'=',
        onActivePaneChanged:'=',
        onFilterRightDisabledChanged:'=',
        onUserSearchOnlyFilterRightChanged:'=',
        onIncludeSubEntityDataChanged:'=',
        onFullyContainedDataChanged:'=',
        onCollapsedDataRightChanged:'=',
        changeLeftPage:'=',
        changeRightPage:'=',
        changeSelectedItemLeft:'=',
        changeSelectedItemRight:'=',
        changeActivePan:'=',
        changeDisableRightFilter:'=',
        changeUserSearchOnlyFilterRight:'=',
        changeFullyContainedFilterState:'=',
        changeIncludeSubEntityData:'=',
        changeCollapsedDataRight:'=',
        findIdLeft:'=',
        findIdRight:'=',
        bizListSupportedTypes:'=',
        isBizListAssociationDisabled:'=',
        isFunctionalRoleDisabled:'=',
        openManageAssociationPage:'=',
        openManageDocTypesPage:'=',
        openManageViewsPage:'=',
        openManageDepartmentPage:'=',
        openManageMatterPage:'=',
        openManageFunctionalRolesPage:'=',
        openManageTagTypesPage:'=',
        isImmediateActionsDisabled:'=',
        isCreatePolicyObjectDisabled:'=',
        isExportToExcelDisabled:'=',
        openNewTab:'=',
        changeGridView:'=',
        isSwitchViewsDisabled:'=',
        hideToolbarHeader:'=',
        resizeSplitter:'=',
        leftPanWidth:'=',
        selectItemsActivePan:'=',
        reloadDataActivePan:'=',
        refreshDataActivePan:'=',
        activePanType:'=',
        viewPermitted:'=',
      },
      controller: 'splitViewCtrl',
      link: function (scope:any, element, attrs) {
        scope.$watch(function () { return scope.leftBodies; }, function (value) {
          if(value) {
            // we want to use the scope OUTSIDE of this directive
            // (which itself is an isolate scope).
            angular.element('.leftBody').empty();
            var newElem = $compile(scope.leftBodies)(scope);
            angular.element('.leftBody').append(newElem);
            scope.resizeSplitter();
            $timeout(function() {
              angular.element('.leftBody').bind("click", function (event) {
                scope.$apply(function () {
                  scope.onActivePaneClicked(true);
                });
              });
            });
          }
        });

        scope.$watch(function () { return scope.rightBodies; }, function (value) {
          if(value) {
            // we want to use the scope OUTSIDE of this directive
            // (which itself is an isolate scope).
            angular.element('.rightBody').empty();
            var newElem2 = $compile(scope.rightBodies)(scope);
            angular.element('.rightBody').append(newElem2);
            scope.resizeSplitter();
            $timeout(function() {
              angular.element('.rightBody').bind("click", function (event) {
                scope.$apply(function () {
                  scope.onActivePaneClicked(false);
                });
              });
            });
          }
        });
      }
    };
  }
);

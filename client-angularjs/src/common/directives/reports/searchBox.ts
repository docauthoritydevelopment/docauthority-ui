///<reference path='../../all.d.ts'/>
'use strict';
angular.module('directives.searchBox', [
  'ui.builder.filters'
])
    .controller('userSearchBoxCtrl', function ($scope, $element, Logger:ILogger, $state, $stateParams, $timeout, $location,configuration:IConfig,dialogs,
                                               currentUserDetailsProvider:ICurrentUserDetailsProvider,filterFactory: IFilterFactory,$filter, splitViewFilterService:IFilterService, eventCommunicator:IEventCommunicator) {
      var log:ILog = Logger.getInstance('searchBoxCtrl');

      $scope.userSearchModes = EUserSearchMode.all();
      $scope.userSearchOptions = [EUserSearchOption.all, EUserSearchOption.selection];
      $scope.searched = false;
      $scope.showGoogleLikeSearch = currentUserDetailsProvider.isAdvancedSearchEnabled();
      $scope.selectedUserSearchOption=null;
      $scope.selectedUserSearchMode =EUserSearchMode.content;
      $scope.searchMinLength=3;
      let ele;
      $scope.init=function(element)
      {
         refreshState();
        setFilterTerms();
        ele=angular.element(element.find('input'));
      }

      var refreshState = function () {
        $scope.hasFilter = splitViewFilterService.getFilterData().getUserSearchFilter() ? splitViewFilterService.getFilterData().getUserSearchFilter().hasCriteria() : false;
        $scope.userSearchFilterTag =$scope.hasFilter ? splitViewFilterService.getFilterData().getUserSearchFilter() : null;
        $scope.sampleUserFilter =  $scope.userSearchFilterTag? splitViewFilterService.getFilterData().getUserSearchFilter().getAllCriteriaLeafs()[0]:null;
        var userContentFilter = $scope.hasFilter ?isUserContentFilter( $scope.sampleUserFilter.fieldName):false;
        $scope.selectedUserSearchMode = $scope.userSearchFilterTag?userContentFilter?EUserSearchMode.content:EUserSearchMode.properties:$scope.selectedUserSearchMode;
        $scope.searchTerm= $scope.sampleUserFilter ?$scope.sampleUserFilter.value:null;
        $scope.searched = $scope.searchTerm?true:false;
        $scope.badSearchText = false;
      };

      var isUserContentFilter=function(fieldName)
      {
        if(fieldName!=null) {
          return (fieldName == configuration.contentFilterFieldName);
        }
      }

      $scope.addSearchTerm = function(term){

        $scope.searchTerm= $scope.searchTerm?$scope.searchTerm+' '+term:term;
       ele.focus();
      }
      var setFilterTerms = function(){
        let filterTerms = [];
        let abbrList = filterFactory.searchTextAbbreviationsDic
        for(let key in abbrList) {
          if(isExists(key,filterTerms))
          {

          }
          else {
            filterTerms.push({
              title: $filter('translate')(abbrList[key].toString()),
              type: abbrList[key],
              term: key,
              tooltip: $filter('translate')('DESC_' + abbrList[key].toString())
            });
          }
        }
        function isExists(key,filterTerms){
          return filterTerms.filter(t=>t.type == abbrList[key])[0]
        }
          $scope.filterTerms = filterTerms;
      }

      $scope.doUserSearch = function (value) {
        $scope.badSearchText = false;
        $scope.userTypingSearchTerm=false;
        if (value && value.length>= $scope.searchMinLength) {
          if(value.indexOf(':')>-1){
            doGoogleLikeSearch(value);
          }
          else {
            doContentSearch(value);
          }

          //   $scope.searched = true;

        }

      };

      var filterByEntitySearchText = function(searchText:string,entityDisplayType:EntityDisplayTypes)
      {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doEntityTextSearch',
          searchText: searchText,
          entityDisplayType:entityDisplayType,
        });
      };
      var doGoogleLikeSearch = function(searchText:string)
      {

        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doGoogleLikeSearch',
          searchText: searchText,
          selectedUserSearchMode:  $scope.selectedUserSearchMode ,
        });
      };

      var doContentSearch = function(searchText){
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxUserAction, {
          action: 'doUserSearch',
          selectedUserSearchMode:  $scope.selectedUserSearchMode ,
          value: searchText,
        });
      }
      $scope.clearUserSearch = function () {
        $scope.badSearchText = false;
        $scope.userTypingSearchTerm=false;
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxUserAction, {
          action: 'clearUserSearch',
        });
        // $scope.searched = false;
      };

      $scope.setSelectedSearchOptionMode = function (value:EUserSearchOption) {
      if ($scope.selectedUserSearchOption!=value) {
        $scope.badSearchText = false;
        $scope.selectedUserSearchOption = value;
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxUserAction, {
          action: 'toggleUserSearchOnlyFilterRight',
          userSearchOnlyFilterRight:  $scope.selectedUserSearchOption == EUserSearchOption.selection,
        });
      }

    };

     $scope.setSelectedUserSearchMode= function (mode:EUserSearchMode) {

      if(mode!=$scope.selectedUserSearchMode)
      {
        $scope.badSearchText = false;
        $scope.userTypingSearchTerm=false;
        $scope.selectedUserSearchMode = mode;
      }
    };

     $scope.onEnter = function(){
       $scope.doUserSearch($scope.searchTerm);
       $scope.userSubmittedSearch=true;
     }

     $scope.shouldFocus = function(){
       return $scope.badSearchText || $scope.focusOnInit;
     }
    $scope.startNewUserSearch= function () {
      $scope.badSearchText = false;
      $scope.userTypingSearchTerm=false;
          eventCommunicator.fireEvent(EEventServiceEventTypes.ReportSearchBoxUserAction, {
            action: 'startNewUserSearch',
          });
         refreshState();
      };

    $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
    });



    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportSearchBoxSetState,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        /*
         * fireParam object:
         *   { initSearch: false, value: searchText, mode: mode}
         */
        if (fireParam.action === 'reportUpdateState') {
          $scope.selectedUserSearchOption =  fireParam.userSearchOnlyFilterRight?EUserSearchOption.selection:EUserSearchOption.all;
              refreshState()
        }
       else if (fireParam.action === 'reportUserSearchTermNotValid') {
          var invalidSearchTerm = $scope.searchTerm;
          $scope.clearUserSearch();
          $scope.badSearchText = true;
          $scope.searchTerm =invalidSearchTerm;
          $scope.searched=false;
        }
        else if (fireParam.action === 'userSearchInputError' ||fireParam.action === 'reportUserSearchTermNotValid') {
          var invalidSearchTerm = $scope.searchTerm;
         $scope.clearUserSearch();
          $scope.badSearchText = true;
          $scope.searchTerm =invalidSearchTerm;
          $scope.searched=false;

        }
      })));

    // //must be after register handler cause an init event is raised back
    //   eventCommunicator.fireEvent(EEventServiceEventTypes.ReportNotifyReady, {
    //     generatorName: 'searchBox'
    //   });

   //   $scope.init();
    })
    .directive('userSearchBox',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/searchBox.tpl.html',
        scope:{
          searchTerm:'=',
          doUserSearch:'=',
          focusOnInit:'=',
          onEnter:'=',
        },
        controller: 'userSearchBoxCtrl',
        link: function(scope, element, attrs) {
          // Title element
          (<any>scope).init(element);
            // ...
            }

      };
    }
);

///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.bizLists.manage', [

    ])
    .controller('ManageBizListsDialogCtrl', function ($scope, $uibModalInstance, data, userSettings:IUserSettingsService) {

      $scope.changeGridView = data.changeGridView;
      $scope.openManageAssociationPage = data.openManageAssociationPage;
      $scope.changeShowOnlyActiveItems=true;
      $scope.bizListType = data.bizListType;
      $scope.filterMode = data.filterMode;
      $scope.bizList = data.bizList;
      $scope.title=($scope.editMode?"Manage ":"Choose ")+ $scope.bizListType+' > '+$scope.bizList.name + ($scope.filterMode?' to filter':'');
      $scope.allowSelect=!$scope.editMode;
      $scope.changeBizListSelectedItem=data.changeSelectedItem;
      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss({selectedId:$scope.selectedId,selectedName:$scope.selectedName});
      }; // end cancel

      $scope.save = function(){

        $uibModalInstance.close({selectedId:$scope.selectedId,selectedName:$scope.selectedName});
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


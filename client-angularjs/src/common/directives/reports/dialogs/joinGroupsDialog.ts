///<reference path='../../../all.d.ts'/>
'use strict';


angular.module('directives.dialogs.group.grouping', [

    ])
    .controller('joinGroupsDialogCtrl', function ($scope, $uibModalInstance ,dialogs,data,userSettings:IUserSettingsService,groupsResource:IGroupsResource) {


      $scope.title = data.title;

      $scope.filesList=data.filesList;
      $scope.entityType = EntityDisplayTypes.groups;
      $scope.initialGroups = data.initialGroups;

      $scope.initiated=true;
      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel


      $scope.onGroupItemsSelected=function(groups:ParamActiveItem<Entities.DTOGroup>[])
      {
        $scope.groupsSelected  =groups.filter(i=>i.active).map(g=> g.item.value) ;
        var max =      Math.max.apply(null,  (<Entities.DTOGroup[]>$scope.groupsSelected).map(g=>g.numOfFiles));
        var selectedUserGroupsOnly =  $scope.groupsSelected.filter(g=>g.groupType == Entities.EGroupType.USER_GROUP);
        var candidateJoin:Entities.DTOGroup[] = selectedUserGroupsOnly.length==0? $scope.groupsSelected: (<Entities.DTOGroup[]>selectedUserGroupsOnly);

        var maxCandidateJoin = candidateJoin.filter(g=>(<any>g).numOfFiles == max)[0];
        $scope.largestGroup = maxCandidateJoin?maxCandidateJoin: $scope.largestGroup;
        $scope.newGroupName = (<Entities.DTOGroup>$scope.largestGroup).groupName;
        if(selectedUserGroupsOnly.length > 0) //There are joined groups selected for joining
        {
          var maxFilesOfUserGroup =      Math.max.apply(null,  selectedUserGroupsOnly.map(g=>g.numOfFiles));
          $scope.largestUserGroup = selectedUserGroupsOnly.filter(g=>g.numOfFiles == maxFilesOfUserGroup)[0];
        }
        else
        {
          $scope.largestUserGroup = null;
        }
         var total = 0;
        (<Entities.DTOGroup[]>$scope.groupsSelected).forEach(g => {
          total+= g.numOfFiles;
        });
        $scope.selectedGroupsTotalNumberOfFiles = total;
      }

      $scope.createGroupParamOption= function(group:Entities.DTOGroup) {
        var newItem = new ParamOption();
        newItem.id = group.id;
        newItem.display = group.groupName;
        newItem.subdisplay = group.id? (<any>group).numOfFiles+' files in group':'New group';
        newItem.value = group;
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;
      }


      $scope.initialGroupValues = (<Entities.DTOGroup[]>$scope.initialGroups).map(g=> $scope.createGroupParamOption(g));

      $scope.loadGroupOptionItems = function(itemsOptions:any[]) {
        userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[]) { //shaul

          var markedGroupsList:DTOMarkedItem[] = savedActiveRecentlyUsedMarkedItems?savedActiveRecentlyUsedMarkedItems.filter(f=>$scope.entityType.equals(f.entity)):[];

          if (markedGroupsList.length > 0) {
            groupsResource.getGroups(markedGroupsList.map(r=>r.id), function (groups:Entities.DTOGroup[]) {
              var groupsDic = {};
              groups.forEach(g=>groupsDic[g.id] = g);

              markedGroupsList.forEach(markedGroup=> {
                let group = <Entities.DTOGroup>groupsDic[markedGroup.id];
                if(group.numOfFiles > 0 && _.isNull(group.parentGroupId) && group.groupType != Entities.EGroupType.SUB_GROUP && !group.groupProcessing) {
                  var newItem = $scope.createGroupParamOption(group);
                  itemsOptions.push(newItem.item);
                }
              })
          //    $scope.groupsList = groupsList;

            }, onError);

          }

        });

      }

      $scope.minGroupsAreSelected = function()
      {
        return $scope.groupsSelected&&$scope.groupsSelected.length>=2;
      }

      $scope.save = function(){

        if($scope.minGroupsAreSelected()&&$scope.groupForm.$valid ) {
          $uibModalInstance.close({newGroupName:$scope.newGroupName, selectedGroups:$scope.groupsSelected,largestUserGroup: $scope.largestUserGroup});
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
    })


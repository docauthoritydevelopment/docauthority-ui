///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.dateRange.partition.new', [
  'directives.date.RelativeAbsoluteSelector'
    ])
    .controller('createDatePartitionDialogCtrl', function ($scope, $uibModalInstance,dialogs, data,configuration:IConfig,$filter,dateRangeResource:IDateRangeResource) {

      let invalidDates = [];
      $scope.relativeTimePeriodOptions=[Entities.ETimePeriod.YEARS,Entities.ETimePeriod.MONTHS,Entities.ETimePeriod.WEEKS,Entities.ETimePeriod.DAYS,Entities.ETimePeriod.HOURS];
      $scope.dateFormat = configuration.dateFormat;
      $scope.titleTimeFormat =  configuration.dateFormat;
      $scope.isRangeItemsFormValid = true;

      var init=function(partition:Entities.DTODateRangePartition,openWithFieldName)
      {
        if(partition )
        {

          $scope.title='Edit date partition';
          $scope.datePartitionDto=partition;
          $scope.editMode=true;
          initDateRangeItems();

        }
        else
        {

          $scope.title='Add date partition';
          var datePartitionDto=new Entities.DTODateRangePartition();
          var ever = new Entities.DTODateRangeItem();
          ever.start =  new Entities.DTODateRangePoint();
          ever.start.type = Entities.EDateRangePointType.EVER;
          var today = new Entities.DTODateRangeItem();
          today.start =  new Entities.DTODateRangePoint();
          today.start.relativePeriodAmount = 0;
          today.start.relativePeriod = Entities.ETimePeriod.DAYS;
          today.start.type =  Entities.EDateRangePointType.RELATIVE;
          datePartitionDto.continuousRanges = true;
          datePartitionDto.dateRangeItemDtos=[ever,today];
          $scope.datePartitionDto = datePartitionDto;
          setSummaryText(datePartitionDto.dateRangeItemDtos[0]);
          $scope.initiated=true;
        }

        $scope.setActiveTab(openWithFieldName?openWithFieldName: 'rangeItems');

      }
      $scope.setActiveTab=function(tabName:string)
      {
        $scope.activeTab=tabName;
      }

      $scope.shouldBe = function($index,$last) {
        if ($scope.isStartFromEver() && $index == 0) {
          return false;
        }

        if ($scope.isUpToToday() && $last) {
          return false;
        }
        return true;
      }




      var initDateRangeItems = function()
      {
        dateRangeResource.getPartition($scope.datePartitionDto.id,function(datePartition:Entities.DTODateRangePartition)
        {
          if(datePartition.dateRangeItemDtos.length > 0) {
            if(datePartition.dateRangeItemDtos[0].start.type ===  Entities.EDateRangePointType.UNAVAILABLE) {
              datePartition.dateRangeItemDtos.shift();
            }
          }
          (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos = datePartition.dateRangeItemDtos;
          $scope.activeDateRangeItem = null; //$scope.datePartitionDto.dateRangeItemDtos[ $scope.datePartitionDto.dateRangeItemDtos.length-1];
          $scope.initiated=true;
        },onError);
      }

      var setSummaryText = function(dateRangeDto:Entities.DTODateRangeItem)
      {
       var index = (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeDto);
       if(index == (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1)
       {
         (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index].name = null;
         return; //Last range item do not have name
       }
       if ((<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length>index+1) //At least two items exists in order to write title
       {
         var title;
         var valid = true;
         var item =  (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index];
         var nextItem =  (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index+1];
         if(item.start.type == Entities.EDateRangePointType.EVER)
         {
           title = 'Ever';
         }
         else if(item.start.type == Entities.EDateRangePointType.RELATIVE)
         {
           valid =valid && item.start.relativePeriodAmount!=null && item.start.relativePeriod!=null;
           title = item.start.relativePeriodAmount + ' ' +item.start.relativePeriod;
         }
         else {
           valid =valid && item.start.absoluteTime!=null ;
           title = $filter('date')(item.start.absoluteTime, configuration.dateFormat)  ;
         }
         title+=' till ';
         if(nextItem.start.type == Entities.EDateRangePointType.RELATIVE &&nextItem.start.relativePeriodAmount == 0)
         {
           title += 'today';
         }
         else if(nextItem.start.type == Entities.EDateRangePointType.RELATIVE)
         {
           valid =valid && nextItem.start.relativePeriodAmount!=null && nextItem.start.relativePeriod!=null;
           title += nextItem.start.relativePeriodAmount + ' ' +nextItem.start.relativePeriod;
         }
         else {
           valid =valid && nextItem.start.absoluteTime!=null ;
            title += $filter('date')(nextItem.start.absoluteTime, configuration.dateFormat)  ;
         }
         (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index].name = valid?title:null;
       }
      }

      init(data.partition,data.openWithFieldName);

      $scope.isStartFromEver = function()
      {
        return    $scope.initiated&&(< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[0]!=null &&
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[0].start.type == Entities.EDateRangePointType.EVER;
      }
      $scope.toggleStartFromEver = function()
      {
        if( $scope.isStartFromEver()) {
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice(0,1);
          $scope.pointChange((< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[0]);
        }
        else {
          var ever = new Entities.DTODateRangeItem();
          ever.start =  new Entities.DTODateRangePoint();
          ever.start.type = Entities.EDateRangePointType.EVER;
          ever.start.absoluteTime = (new Date(" 1/1/1971")).getTime();
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice(0,0,ever);
          $scope.pointChange(ever);
        }
      }
      $scope.pointTitleChange = function(dateRangeDto:Entities.DTODateRangeItem,newTitle:string)
      {
        if(!newTitle||newTitle=='') {
          setSummaryText(dateRangeDto);
        }
        else {
          dateRangeDto.name = newTitle;
        }
      };
      $scope.onRangeItemValidityChange = function(itemId, isValid) {
        if(!isValid) {
            invalidDates.push(itemId);
        } else {
          _.remove(invalidDates, function(id) {
            return id == itemId;
          });
        }
        $scope.isRangeItemsFormValid = _.isEmpty(invalidDates);
      };
      $scope.pointChange = function(dateRangeDto:Entities.DTODateRangeItem)
      {
        var index = (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeDto);
        if(index-1>=0) {
          var prevItem = (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index-1];
          setSummaryText(prevItem);
        }
        setSummaryText(dateRangeDto);
      }
      $scope.isUpToToday = function()
      {
        return $scope.initiated && (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[0]!=null &&
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[ (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1].start.type == Entities.EDateRangePointType.RELATIVE&&
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[ (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1].start.relativePeriodAmount == 0;
      };

      var addUnavailableRange = function() {
        var newRange = new Entities.DTODateRangeItem();

        newRange.name = "Unavailable";
        newRange.start = new Entities.DTODateRangePoint();
        newRange.start.type = Entities.EDateRangePointType.UNAVAILABLE;

        $scope.datePartitionDto.dateRangeItemDtos.push(newRange);
      };

      var removeEndDates = function() {
        let items = (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos;

        _.forEach(items, (item) => {
          item.end = null;
        });
      };

      $scope.toggleUpToToday = function()
      {
        if( $scope.isUpToToday()) {
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice( (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1,1);
          $scope.pointChange( (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[(< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1]);
        }
        else {
          var today = new Entities.DTODateRangeItem();
          today.start =  new Entities.DTODateRangePoint();
          today.start.type = Entities.EDateRangePointType.RELATIVE;
          today.start.relativePeriod = Entities.ETimePeriod.DAYS;
          today.start.relativePeriodAmount = 0;
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.push(today);
          $scope.pointChange(today);
        }

      }
      $scope.createNewPoint = function()
      {
        var newPoint = new Entities.DTODateRangeItem();

        newPoint.start = new Entities.DTODateRangePoint();
        newPoint.start.type = Entities.EDateRangePointType.RELATIVE;
        if($scope.activeDateRangeItem)
        {
          var index =   (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.indexOf($scope.activeDateRangeItem);
          newPoint.start.type = (<Entities.DTODateRangeItem>$scope.activeDateRangeItem).start.type == Entities.EDateRangePointType.ABSOLUTE?Entities.EDateRangePointType.ABSOLUTE:Entities.EDateRangePointType.RELATIVE; //not ever
          (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice(index+1,0,newPoint);

        }
        else {
          if($scope.isUpToToday()) {
            var lastIndex =  (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length-1;
            (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice(lastIndex,0,newPoint);
          }
          else {
            (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.push(newPoint);
          }
        }
        $scope.activeDateRangeItem = newPoint;
      }

      $scope.deletePoint = function(dateRangeItem)
      {
        var index =   (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeItem);
        (< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.splice(index,1);
        if(index-1>=0) {
          var prevItem = (<Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index-1];
          setSummaryText(prevItem);
        }
        if(index<(< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos.length) {
          setSummaryText((< Entities.DTODateRangePartition>$scope.datePartitionDto).dateRangeItemDtos[index]);
        }
        $scope.activeDateRangeItem = null;
        $scope.onRangeItemValidityChange(dateRangeItem.$$hashKey, true);
      }
      $scope.openNewRangeItemDialog = function()
      {
        var lastRangeItem:Entities.DTODateRangePoint =  $scope.datePartitionDto.dateRangeItemDtos.length>0? $scope.datePartitionDto.dateRangeItemDtos[ $scope.datePartitionDto.dateRangeItemDtos.length-1].end:null;
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/dateRangeFilterDialog.tpl.html',
            'dateRangeFilterDialogCtrl',
            {onePointMode:!lastRangeItem, startDateReadOnlyMode:true,startDateItem:lastRangeItem},
            {size: 'md',windowClass: 'offsetDialog'});

        dlg.result.then(function ( rangeItem:Entities.DTODateRangeItem) {
          $scope.datePartitionDto.dateRangeItemDtos.push(rangeItem);
        }, function(){});

      }


      $scope.setActiveDateRangeItem = function(rangeItem) {
        $scope.activeDateRangeItem = rangeItem;
      }
      $scope.openEditRangeItemDialog = function(rangeItem)
      {
        var lastRangeItem;
        var foundItemIndex =  $scope.datePartitionDto.dateRangeItemDtos.indexOf(rangeItem);
        if($scope.datePartitionDto.dateRangeItemDtos.length>0)
        {

          lastRangeItem = foundItemIndex>0? $scope.datePartitionDto.dateRangeItemDtos[foundItemIndex-1]:null;

        }
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/dateRangeFilterDialog.tpl.html',
            'dateRangeFilterDialogCtrl',
            {dateRangeDto:rangeItem,onePointMode:!lastRangeItem, startDateReadOnlyMode:true,startDateItem:lastRangeItem},
            {size: 'md',windowClass: 'offsetDialog'});

        dlg.result.then(function ( rangeItem:Entities.DTODateRangeItem) {
          $scope.datePartitionDto.dateRangeItemDtos[foundItemIndex]=rangeItem;
        }, function(){});

      }

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $scope.submitted=true;
        if($scope.generalForm.$valid && $scope.isRangeItemsFormValid) {
          addUnavailableRange();
          removeEndDates();
          $uibModalInstance.close($scope.datePartitionDto);
        }
      };

      $scope.delete = function(){
        $uibModalInstance.close({delete:true});
      };

      $scope.setEditEverTitleMode=function(value:boolean)
      {
        if(value)
        {
          $scope.newEverTitle=   $scope.datePartitionDto.dateRangeItemDtos[0].name;
        }
        $scope.editEverTitleMode=value;

      }
      $scope.cancelEditEverTitle=function()
      {
        $scope.newEverTitle=null;
        $scope.setEditEverTitleMode(false);
      }
      $scope.editEverTitle=function()
      {
        if(!$scope.newEverTitle||$scope.newEverTitle=='') {
          setSummaryText($scope.datePartitionDto.dateRangeItemDtos[0]);
        }
        else {
          $scope.datePartitionDto.dateRangeItemDtos[0].name = $scope.newEverTitle;
        }
        $scope.setEditEverTitleMode(false);
      }
      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    })


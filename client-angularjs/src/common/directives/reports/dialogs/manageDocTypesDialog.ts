///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.docTypes.manage', [

    ])
    .controller('manageDocTypesDialogCtrl', function ($scope, $uibModalInstance, data, userSettings:IUserSettingsService) {

      $scope.editMode = data.editMode;
      $scope.filterMode = data.filterMode;
      $scope.title=$scope.editMode?"Manage DocTypes":$scope.filterMode?"Choose DocType to filter": "Choose DocType to assign";
      $scope.allowSelect=!$scope.editMode;
      $scope.changeDocTypeSelectedItemId=data.changeSelectedItemId;

      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss($scope.selected);
      }; // end cancel

      $scope.save = function(){
        $scope.selected.fileTagDtos = _.extend({}, $scope.selected.fileTagTypeSettings);
        $uibModalInstance.close($scope.selected);
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


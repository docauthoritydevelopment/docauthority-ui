///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.functionalRoles.manage', [

    ])
    .controller('manageFunctionalRolesDialogCtrl', function ($scope, $uibModalInstance, data, userSettings:IUserSettingsService) {

      $scope.editMode = data.editMode;
      $scope.filterMode = data.filterMode;
      $scope.title=$scope.editMode?"Manage data roles":$scope.filterMode?"Choose data roles to filter":"Choose data roles";
      $scope.allowSelect=!$scope.editMode;
      $scope.changeSelectedItemId=data.changeSelectedItemId;
      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss($scope.selected);
      }; // end cancel

      $scope.save = function(){

        console.log("directives.dialogs.functionalRoles.manage:save..")
        $uibModalInstance.close($scope.selected);
      };

      $scope.aaa=function(id,name,item)
      {
        $scope.selected=item;
      }
      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


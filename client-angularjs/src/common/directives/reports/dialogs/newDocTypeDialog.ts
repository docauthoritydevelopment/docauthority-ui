///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.docTypes', [

    ])
    .controller('newDocTypeDialogCtrl', function ($scope, $uibModalInstance, data) {


      //-- Methods --//


      var init=function(docTypeItem:Entities.DTODocType)
      {
        setSelectedParent();
        if(docTypeItem && docTypeItem.name)
        {
          $scope.editMode=true;
          $scope.inputText =  docTypeItem.name;
          $scope.title='Edit DocType';
          $scope.id=docTypeItem.id;
          $scope.description =docTypeItem.description;
          $scope.singleValue =docTypeItem.singleValue;
        }
        else
        {
          $scope.editMode=false;
          if ($scope.parentName && $scope.parentName!='') {
            $scope.title='Add DocType under \'' + $scope.parentName + '\'';
          }
          else {
            $scope.title='Add top level DocType';
          }
        }
        $scope.initiated=true;
      }
      var setSelectedParent = function()
      {
        if(data.parentDocType) {
          $scope.parentName = data.parentDocType.name
          $scope.parentId = data.parentDocType.id;
        }
      }
      init(data.docTypeItem);

      $scope.removeParent = function()
      {
        $scope.parentId=null;
      }
      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(createAnother:boolean){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var docType = new Entities.DTODocType();
          docType.description = $scope.description;
          docType.name=$scope.inputText;
          docType.parentId =$scope.parentId;
          docType.singleValue =$scope.singleValue;
          docType.id =$scope.id;
          if($scope.editMode)
          {
            $uibModalInstance.close({tag:docType});
          }
          else {
            $uibModalInstance.close({tag:docType,createAnother:createAnother});
          }
        }
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


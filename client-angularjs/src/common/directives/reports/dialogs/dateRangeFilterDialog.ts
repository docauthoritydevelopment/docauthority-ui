///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.dateRange.filter', [

    ])
    .controller('dateRangeFilterDialogCtrl', function ($scope, $uibModalInstance, data,configuration:IConfig,$filter) {

      $scope.relativeTimePeriodOptions=[Entities.ETimePeriod.HOURS,Entities.ETimePeriod.DAYS,Entities.ETimePeriod.WEEKS,Entities.ETimePeriod.MONTHS,Entities.ETimePeriod.YEARS];
      $scope.dateFormat = configuration.inputDateFormat;
      $scope.relativeFrom=false;
      $scope.relativeTo=null;
      var relativePeriodInMsc = {hours:60*60*1000,days:60*60*1000*24,weeks:60*60*1000*24*7,months: 60*60*1000*24*30,quarters:60*60*1000*24*364/4,years:60*60*1000*24*365};

      var init=function(dateRangeDto:Entities.DTODateRangeItem,onePointMode:boolean,startDateReadOnlyMode:boolean,startDateItem:Entities.DTODateRangePoint,enableInvertRange)
      {
        $scope.enableInvertRange = enableInvertRange;
        $scope.onePointMode = onePointMode;
        $scope.startDateReadOnlyMode = startDateReadOnlyMode;
        if(dateRangeDto )
        {
          $scope.title='Edit date range filter';
          dateRangeDto.start = dateRangeDto.start ? dateRangeDto.start : new Entities.DTODateRangePoint(); //in base of 'Ever'
          dateRangeDto.end = dateRangeDto.end ? dateRangeDto.end : new Entities.DTODateRangePoint(); //in base of 'Today'

          dateRangeDto.start.relativePeriod = dateRangeDto.start.relativePeriod ? dateRangeDto.start.relativePeriod : Entities.ETimePeriod.MONTHS;
          dateRangeDto.end.relativePeriod = dateRangeDto.end.relativePeriod ? dateRangeDto.end.relativePeriod :Entities.ETimePeriod.MONTHS;
          $scope.dateRangeDto=dateRangeDto;
          $scope.startDate =$scope.dateRangeDto.start&&$scope.dateRangeDto.start.absoluteTime? new Date( $scope.dateRangeDto.start.absoluteTime):null;
          $scope.endDate = $scope.dateRangeDto.end&&$scope.dateRangeDto.end.absoluteTime? new Date($scope.dateRangeDto.end.absoluteTime):null;
          initTypeStates();

        }
        else
        {
          $scope.title='Create date range filter';
          var dateRangeDto=new Entities.DTODateRangeItem();
          dateRangeDto.end = new Entities.DTODateRangePoint();
          dateRangeDto.end.relativePeriod = Entities.ETimePeriod.MONTHS;
          dateRangeDto.end.type =Entities.EDateRangePointType.RELATIVE;

          if(startDateItem)
          {
            dateRangeDto.start = startDateItem;
            dateRangeDto.end.type = dateRangeDto.start.type;

          }
          else if( !$scope.onePointMode ){

            dateRangeDto.start = new Entities.DTODateRangePoint();
            dateRangeDto.start.relativePeriod =Entities.ETimePeriod.MONTHS;
            dateRangeDto.start.type =Entities.EDateRangePointType.RELATIVE;
          }


          $scope.dateRangeDto = dateRangeDto;
          if($scope.startDateReadOnlyMode) {
            $scope.startDate =$scope.dateRangeDto.start&&$scope.dateRangeDto.start.absoluteTime? new Date( $scope.dateRangeDto.start.absoluteTime):null;
          }
          if( $scope.onePointMode ) {
            $scope.ever = $scope.dateRangeDto.start==null;
          }

          initTypeStates();
          $scope.invertRange=false;
        }
        $scope.initiated=true;
      }

      var initTypeStates = function()
      {
        $scope.isStartAbsoluteRadioChecked = $scope.dateRangeDto.start? $scope.dateRangeDto.start.type==Entities.EDateRangePointType.ABSOLUTE:false;
        $scope.isEndAbsoluteRadioChecked =  $scope.dateRangeDto.end? $scope.dateRangeDto.end.type==Entities.EDateRangePointType.ABSOLUTE:false;
      }

      init(data.dateRangeDto,data.onePointMode,data.startDateReadOnlyMode,data.startDateItem,data.enableInvertRange);


      $scope.changeStartRelativePeriod = function(period: Entities.ETimePeriod)
      {
        (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.relativePeriod =period;
        if(!period) //Start of time was selected
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.relativePeriodAmount = null;
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type = null;
        }
        else {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type= Entities.EDateRangePointType.RELATIVE
        }
        validateRelativeDates();
      }
      $scope.changeEndRelativePeriod = function(period: Entities.ETimePeriod)
      {
        (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.relativePeriod =period;
        if(!period) //Today was selected
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.relativePeriodAmount = null;
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type = null;
        }
        else {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type = Entities.EDateRangePointType.RELATIVE
        }
        validateRelativeDates();
      }
      $scope.$watch(function () { return $scope.dateRangeDto.end; }, function (value) {
        if($scope.dateRangeDto.end==null) {
          $scope.dateRangeDto.name = $scope.summaryText();
        }
      });

      $scope.relativeStepChanged = function()
      {
        validateRelativeDates();
      }

      var validateRelativeDates=function()
      {
        if(!$scope.onePointMode && !$scope.myForm.inputStartRelativeStep.$error.required&&!$scope.myForm.inputEndRelativeStep.$error.required && $scope.isStartTimeRelative() && $scope.isEndTimeRelative()) {
         var relativeStartPeriodInMsc =  relativePeriodInMsc[(<string>(<any>$scope.dateRangeDto).start.relativePeriod).toLowerCase()]*(<Entities.DTODateRangeItem>$scope.dateRangeDto).start.relativePeriodAmount;
         var relativeEndPeriodInMsc =  relativePeriodInMsc[(<string>(<any>$scope.dateRangeDto).end.relativePeriod).toLowerCase()]*(<Entities.DTODateRangeItem>$scope.dateRangeDto).end.relativePeriodAmount;
         $scope.myForm.$setValidity('endDatehigher', relativeStartPeriodInMsc > relativeEndPeriodInMsc);
        }
        if($scope.myForm.$valid) {
          $scope.dateRangeDto.name = $scope.summaryText();
        }
      }
      $scope.isStartTimeRelative=function()
      {
        return !$scope.onePointMode &&$scope.dateRangeDto.start &&  $scope.dateRangeDto.start.type == Entities.EDateRangePointType.RELATIVE;
      }
      $scope.isEndTimeRelative=function()
      {
        return $scope.dateRangeDto.end && $scope.dateRangeDto.end.type == Entities.EDateRangePointType.RELATIVE;
      }
      $scope.isEndTimeAbsolute=function()
      {
        return $scope.dateRangeDto.end && $scope.dateRangeDto.end.type == Entities.EDateRangePointType.ABSOLUTE;
      }
      $scope.isStartTimeAbsolute=function()
      {
        return !$scope.onePointMode && $scope.dateRangeDto.start&& $scope.dateRangeDto.start.type == Entities.EDateRangePointType.ABSOLUTE ;
      }


      $scope.onStartAbsoluteRadioCheckedChange = function()
      {
        if($scope.isStartAbsoluteRadioChecked )
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type=Entities.EDateRangePointType.ABSOLUTE;
        }
        else
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type=Entities.EDateRangePointType.RELATIVE;
        }

      }

      $scope.onEndAbsoluteRadioCheckedChange = function()
      {
        if($scope.isEndAbsoluteRadioChecked )
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type=Entities.EDateRangePointType.ABSOLUTE;
        }
        else
        {
          (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type=Entities.EDateRangePointType.RELATIVE;
        }
      }
      $scope.setFromEver = function(isEver:boolean)
      {
        if(isEver &&$scope.dateRangeDto.start)
        {
          $scope.dateRangeDto.end = $scope.dateRangeDto.start;
          $scope.dateRangeDto.start=null;
        }
        else if(!isEver &&  $scope.dateRangeDto.end){
          $scope.dateRangeDto.start =  $scope.dateRangeDto.end;

        }
        if($scope.myForm.$valid) {
          $scope.dateRangeDto.name = $scope.summaryText();
        }
      }
      $scope.dateChanged=function()
      {
        if($scope.endDate && $scope.isEndTimeAbsolute()) {
          $scope.isStartTimeAbsolute()?$scope.myForm.$setValidity('endDatehigher', $scope.endDate.getTime() > $scope.startDate.getTime()): null;
          $scope.myForm.$setValidity('endLessThenToday', $scope.endDate <= new Date(Date.now()));
        }
        if($scope.startDate && $scope.isStartTimeAbsolute()) {
          $scope.myForm.$setValidity('startLessThenToday', $scope.startDate <= new Date(Date.now()));
        }
        if($scope.myForm.$valid) {
          $scope.dateRangeDto.name = $scope.summaryText();
        }
      }
      $scope.startSummaryText = function()
      {
        var fromPart ='';
        if((!$scope.onePointMode || ($scope.onePointMode&&$scope.ever))&& !$scope.myForm.inputStartRelativeStep.$error.required&&!$scope.myForm.startDate.$error.required) {
         fromPart = (<Entities.DTODateRangeItem>$scope.dateRangeDto).start == null ||(<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type == null ? 'Ever' :
             (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.type == Entities.EDateRangePointType.ABSOLUTE ?
                 $filter('date')($scope.startDate, configuration.dateFormat) : (<Entities.DTODateRangeItem>$scope.dateRangeDto).start.relativePeriodAmount + ' ' + $filter('translate')((<Entities.DTODateRangeItem>$scope.dateRangeDto).start.relativePeriod) ;
       }
        return fromPart;
      }
      $scope.endSummaryText = function()
      {
        var toPart='';
        if( ($scope.onePointMode&&$scope.ever) || !$scope.myForm.inputEndRelativeStep.$error.required&&!$scope.myForm.endDate.$error.required) {
          toPart = ((<Entities.DTODateRangeItem>$scope.dateRangeDto).end ==null || (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type ==null) ? 'Today' :
              (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.type == Entities.EDateRangePointType.ABSOLUTE ?
                  $filter('date')($scope.endDate, configuration.dateFormat) : (<Entities.DTODateRangeItem>$scope.dateRangeDto).end.relativePeriodAmount + ' ' + $filter('translate')((<Entities.DTODateRangeItem>$scope.dateRangeDto).end.relativePeriod)+ ' ago';
        }
        return toPart;
      }
      $scope.summaryText = function()
      {
        var fullText =  $scope.startSummaryText()+' - '+ $scope.endSummaryText();
        if($scope.enableInvertRange&&!$scope.onePointMode&&$scope.invertRange)
        {
          fullText = 'Do not include file from '+ fullText;
        }
         return  fullText;
      }
      $scope.open1 = function() {
        $scope.startopened = true;
      };

      $scope.open2 = function() {
        $scope.endopened = true;
      };


      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid && $scope.nameForm.$valid)
        {
          var dateRangeDto = <Entities.DTODateRangeItem>angular.copy($scope.dateRangeDto);
          if($scope.isStartTimeAbsolute()&& !$scope.startDateReadOnlyMode)
          {
            var startTimeAsUtc = new Date(Date.UTC($scope.startDate.getFullYear(), $scope.startDate.getMonth(), $scope.startDate.getDate(), 0, 0, 0));
            dateRangeDto.start.absoluteTime = startTimeAsUtc.getTime();

            dateRangeDto.start.relativePeriod=null;
            dateRangeDto.start.relativePeriodAmount=null;
          }
          else if($scope.isStartTimeRelative()&& !$scope.startDateReadOnlyMode)
          {
            dateRangeDto.start.absoluteTime=null;
          }
          if($scope.isEndTimeAbsolute())
          {
            var endTimeAsUtc = new Date(Date.UTC($scope.endDate.getFullYear(), $scope.endDate.getMonth(), $scope.endDate.getDate(), 0, 0, 0));
            dateRangeDto.end.absoluteTime = endTimeAsUtc.getTime();

            dateRangeDto.end.relativePeriod=null;
            dateRangeDto.end.relativePeriodAmount=null;
          }
          else if($scope.isEndTimeRelative()&& !$scope.startDateReadOnlyMode)
          {
            dateRangeDto.end.absoluteTime=null;
          }

          if(dateRangeDto.start&&dateRangeDto.start.type==null)
          {
            dateRangeDto.start=null; //Ever
          }
          if(dateRangeDto.end&&dateRangeDto.end.type==null)
          {
            dateRangeDto.end=null; //Today
          }

          if( $scope.onePointMode && !$scope.ever  ) {
           dateRangeDto.start== dateRangeDto.end;
           dateRangeDto.start== null;
          }

          $uibModalInstance.close(dateRangeDto);
        }
      };



      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };


      $scope.datePickerOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
  //      maxDate: new Date(2020, 5, 22),
  //      minDate: new Date(),
        startingDay: 1
      };


    })


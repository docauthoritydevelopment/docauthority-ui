///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.assignTag', [])
  .controller('addRemoveTagDialogCtrl', function ($scope, $uibModalInstance, $http, $httpParamSerializer, splitViewBuilderHelper:ui.ISplitViewBuilderHelper, splitViewFilterService:IFilterService, configuration:IConfig, filterFactory:ui.IFilterFactory, data) {

    $scope.title = 'Tag files operation';
    $scope.applyToSelectedItemsOnly = true;
    $scope.overrideExplicit = true;
    $scope.overrideImplicit = true;

    var getFileCount = function(filterData:FilterData, selectedOnly:boolean, onSuccess:any) {
      let queryParams:any = {
        "take":1000,
        "skip":0,
        "page":1,
        "pageSize":1000
      };

      let url = configuration['files_url'];

      let filterStr = JSON.stringify(filterData.toKendoFilter());
      if (filterStr != null && filterStr != 'null') {
        queryParams.filter = filterStr;
      }

      if (selectedOnly) {
        queryParams.baseFilterField = $scope.baseFilterField;
        queryParams.baseFilterValue = $scope.baseFilterValue;
      }

      if (url.indexOf("?")>-1) {
        url = url.substring(0,url.indexOf("?"));
      }
      let queryString = $httpParamSerializer(queryParams);
      $http.get(url +'?'+ queryString).then(function successCallback(response) {
        let fileCount = 0;

        fileCount = response.data.totalElements;

        onSuccess(fileCount);
      }, function errorCallback(err) {});
    };

    var setActionOptionSelected = function() {
      if (angular.isDefined($scope.addFiles) && angular.isDefined($scope.selectedAddFiles) &&
          angular.isDefined($scope.explicitTagFiles) && angular.isDefined($scope.selectedExplicitTagFiles)) {
        if ($scope.applyToSelectedItemsOnly) {
          if ($scope.selectedAddFiles == 0) {
            $scope.actionOptionSelected = 'remove';
            if ($scope.selectedExplicitTagFiles == 0) {
              if ($scope.noFilter) {
                $scope.actionOptionSelected = 'add';
              }
              else if ($scope.addFiles == 0) {
                if ($scope.explicitTagFiles == 0) {
                  $scope.actionOptionSelected = 'add';
                }
              }
              else {
                $scope.actionOptionSelected = 'add';
              }
            }
          }
        }
        else if ($scope.addFiles == 0) {
          $scope.actionOptionSelected = 'remove';
          if ($scope.explicitTagFiles == 0) {
            $scope.actionOptionSelected = 'add';
          }
        }

        $scope.loading = false;
      }
    };

    var setAddFiles = function(selected, force) {
      if (!selected) {
        if ((force || !angular.isDefined($scope.addFiles)) &&
          angular.isDefined($scope.totalFiles) &&
          angular.isDefined($scope.explicitTagFiles) &&
          angular.isDefined($scope.explicitTagTypeFiles)/* &&
          angular.isDefined($scope.tagTypeFiles)*/) {
          $scope.addFiles = $scope.totalFiles - $scope.explicitTagFiles;
          if (!$scope.overrideExplicit) {
            $scope.addFiles -= ($scope.explicitTagTypeFiles - $scope.explicitTagFiles);
          }
          //if (!$scope.overrideImplicit) {
          //  $scope.addFiles -= ($scope.tagTypeFiles - $scope.explicitTagTypeFiles);
          //}
        }
      }
      else {
        if ((force || !angular.isDefined($scope.selectedAddFiles)) &&
          angular.isDefined($scope.selectedFiles) &&
          angular.isDefined($scope.selectedExplicitTagFiles) &&
          angular.isDefined($scope.selectedExplicitTagTypeFiles)/* &&
          angular.isDefined($scope.selectedTagTypeFiles)*/) {
          $scope.selectedAddFiles = $scope.selectedFiles - $scope.selectedExplicitTagFiles;
          if (!$scope.overrideExplicit) {
            $scope.selectedAddFiles -= ($scope.selectedExplicitTagTypeFiles - $scope.selectedExplicitTagFiles);
          }
          //if (!$scope.overrideImplicit) {
          //  $scope.selectedAddFiles -= ($scope.selectedTagTypeFiles - $scope.selectedExplicitTagTypeFiles);
          //}
        }
      }

      setActionOptionSelected();
    };

    var setExplicitFiles = function(selected) {
      if (!selected) {
        if (!angular.isDefined($scope.explicitFiles) &&
          angular.isDefined($scope.explicitTagTypeFiles) &&
          angular.isDefined($scope.explicitTagFiles)) {
          $scope.explicitFiles = $scope.explicitTagTypeFiles - $scope.explicitTagFiles;
        }
      }
      else {
        if (!angular.isDefined($scope.selectedExplicitFiles) &&
          angular.isDefined($scope.selectedExplicitTagTypeFiles) &&
          angular.isDefined($scope.selectedExplicitTagFiles)) {
          $scope.selectedExplicitFiles = $scope.selectedExplicitTagTypeFiles - $scope.selectedExplicitTagFiles;
        }
      }
    };

    /*var setImplicitFiles = function(selected) {
      if (!selected) {
        if (!angular.isDefined($scope.implicitFiles) &&
          angular.isDefined($scope.tagTypeFiles) &&
          angular.isDefined($scope.explicitTagTypeFiles)) {
          $scope.implicitFiles = $scope.tagTypeFiles - $scope.explicitTagTypeFiles;
        }
      }
      else {
        if (!angular.isDefined($scope.selectedImplicitFiles) &&
          angular.isDefined($scope.selectedTagTypeFiles) &&
          angular.isDefined($scope.selectedExplicitTagTypeFiles)) {
          $scope.selectedImplicitFiles = $scope.selectedTagTypeFiles - $scope.selectedExplicitTagTypeFiles;
        }
      }
    };*/

    $scope.setOverrideExplicit =function() {
      $scope.overrideExplicit = !$scope.overrideExplicit;
      setAddFiles(false,true);
      setAddFiles(true,true);
    };

    $scope.setOverrideImplicit =function() {
      $scope.overrideImplicit = !$scope.overrideImplicit;
      setAddFiles(false,true);
      setAddFiles(true,true);
    };

    $scope.setApplyToSelectedItemsOnly = function(apply) {
      $scope.applyToSelectedItemsOnly = apply;
    };

    var setTotalFiles = function() {
      let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
      if ($scope.addedFilter) {
        filterData.setPageFilters($scope.addedFilter);
      }
      getFileCount(filterData,false, function (res) {
        $scope.totalFiles = res;
        setAddFiles(false,true);
      });
      getFileCount(filterData,true,  function (res) {
        $scope.selectedFiles = res;
        setAddFiles(true,true);
      });
    };

    var setTagFiles = function() {
      let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
      var tagFilter = filterFactory.createTagFilter();
      let newCriteria = tagFilter([$scope.tagItem], ECriteriaOperators.and);
      filterData.setPageFilters(newCriteria);
      if ($scope.addedFilter) {
        filterData.setPageFilters($scope.addedFilter);
      }
      getFileCount(filterData, false, function (res) {
        $scope.tagFiles = res;
      });
      getFileCount(filterData, true, function (res) {
        $scope.selectedTagFiles = res;
      });
    };

    /*var setTagTypeFiles = function() {
      let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
      var tagTypeFilter = filterFactory.createTagTypeFilter();
      let newCriteria = tagTypeFilter([$scope.tagItem.type], ECriteriaOperators.and);
      filterData.setPageFilters(newCriteria);
      getFileCount(filterData.toKendoFilter(),false,function (res) {
        $scope.tagTypeFiles = res;
        setAddFiles(false,true);
        //setImplicitFiles(false);
      });
      getFileCount(filterData.toKendoFilter(),true,function (res) {
        $scope.selectedTagTypeFiles = res;
        setAddFiles(true,true);
        //setImplicitFiles(true);
      });
    };*/

    var setExplicitTagFiles = function () {
      let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
      var tagExplicitFilter = filterFactory.createExplicitTagAssociationFilter();
      let newCriteria = tagExplicitFilter([$scope.tagItem], ECriteriaOperators.and);
      filterData.setPageFilters(newCriteria);
      if ($scope.addedFilter) {
        filterData.setPageFilters($scope.addedFilter);
      }
      getFileCount(filterData,false,function (res) {
        $scope.explicitTagFiles = res;
        setAddFiles(false,true);
        setExplicitFiles(false);
      });
      getFileCount(filterData,true,function (res) {
        $scope.selectedExplicitTagFiles = res;
        setAddFiles(true,true);
        setExplicitFiles(true);
      });
    };

    var setExplicitTagTypeFiles = function () {
      let filterData = _.cloneDeep(splitViewFilterService.getFilterData());
      var tagTypeExplicitFilter = filterFactory.createExplicitTagTypeAssociationFilter();
      let newCriteria = tagTypeExplicitFilter([$scope.tagItem.type], ECriteriaOperators.and);
      filterData.setPageFilters(newCriteria);
      if ($scope.addedFilter) {
        filterData.setPageFilters($scope.addedFilter);
      }
      getFileCount(filterData,false,function (res) {
        $scope.explicitTagTypeFiles = res;
        setAddFiles(false,false);
        setExplicitFiles(false);
        //setImplicitFiles(false);
      });
      getFileCount(filterData,true,function (res) {
        $scope.selectedExplicitTagTypeFiles = res;
        setAddFiles(true,false);
        setExplicitFiles(true);
        //setImplicitFiles(true);
      });
    };

    var init = function() {
      $scope.actionOptionSelected = 'add';

      if ($scope.displayType.equals( EntityDisplayTypes.all_files)) {
        $scope.applyToSelectedItemsOnly = false;
      }

      $scope.noFilter = !splitViewFilterService.getFilterData().hasCriteria();

      $scope.loading = true;

      setTotalFiles();
      setTagFiles();
      //setTagTypeFiles();
      setExplicitTagFiles();
      setExplicitTagTypeFiles();
    };

    $scope.tagItem = data.tagItem;
    $scope.tagTypeStyleId = splitViewBuilderHelper.getTagTypeStyleId($scope.tagItem.type);
    $scope.displayType = data.displayType;
    $scope.addedFilter = data.addedFilter;
    $scope.baseFilterField = data.baseFilterField;
    $scope.baseFilterValue = data.baseFilterValue;
    $scope.selectionTxt = data.selectionTxt;
    init();

    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.assign = function() {
      $uibModalInstance.close({action: $scope.actionOptionSelected, applyToSelectedItemsOnly: $scope.applyToSelectedItemsOnly, overrideExplicit: $scope.overrideExplicit, overrideImplicit: $scope.overrideImplicit});
    };

    $scope.hitEnter = function(evt) {
      if(angular.equals(evt.keyCode,13))
        $scope.assign();
    };
});

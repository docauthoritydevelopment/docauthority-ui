///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.tags', [

    ])
    .controller('newTagDialogCtrl', function ($scope, $uibModalInstance, data, splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {


      //-- Methods --//
    $scope.setSelectedTagType = function(tagType)
    {
      $scope.selectedTagType = tagType;
    }


    var init=function(tagItem)
    {
      if(tagItem && tagItem.name)
      {
        $scope.editMode = true;
        $scope.inputText =  tagItem.name;
        $scope.title='Edit tag';
        $scope.id=tagItem.originalTagId?tagItem.originalTagId: tagItem.id;
        $scope.description =tagItem.description;
      }
      else
      {
        $scope.title='Add tag';
      }
      $scope.initiated1=true;
    }
      $scope.setSelectedTagType(data.tagType);
      $scope.tagTypes = data.tagTypes;
      init(data.tagItem);

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.getTagTypeStyleId = function(tag:Entities.DTOFileTagType)
      {
        return splitViewBuilderHelper.getTagTypeStyleId(tag);

      };

      $scope.save = function(createAnother:boolean){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var newTag = new Entities.DTOFileTag();
          newTag.description = $scope.description;
          newTag.name=$scope.inputText;
          newTag.type =$scope.selectedTagType;
          newTag.id =$scope.id;
          if($scope.editMode)
          {
            $uibModalInstance.close(newTag);
          }
          else {
            $uibModalInstance.close({tag:newTag,createAnother:createAnother});
          }

        }
      };


      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


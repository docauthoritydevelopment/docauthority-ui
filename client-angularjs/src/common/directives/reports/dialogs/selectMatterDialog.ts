///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.matter.select', []).controller('selectMatterDialogCtrl',
  function ($scope, $uibModalInstance, departmentResource:IDepartmentResource, data, dialogs, configuration:IConfig,
            splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
  $scope.filterMode = data.filterMode;
  $scope.title=$scope.filterMode?"Choose matter to filter": "Choose matter to assign";
  $scope.changeMatterSelectedItemId=data.changeSelectedItemId;

  $scope.matterList = [];

  //-- Methods --//

  var createMatterParamOption = function(matter:Operations.DtoDepartment) {
    var newItem = new ParamOption<Operations.DtoDepartment>();

    if (matter) {
      newItem.id = matter.id;
      newItem.display = matter.name;
      newItem.value = matter;
      newItem.subdisplay = "";
      newItem.groupByLable = "";
      newItem.icon = configuration.icon_matter + ' tag tag-' + splitViewBuilderHelper.getDepartmentStyleId(<any>matter);
      newItem.displayClass = !matter.parentId ? 'bolder' : null;
      if ($scope.depParentMapping) {
        newItem.subdisplay = matter.parentId ? $scope.depParentMapping[matter.parentId] : "";
        newItem.groupByLable = matter.parentId ? $scope.depParentMapping[matter.parentId] : matter.name;

      }
    }

    return newItem;
  };

  var createMatterNewSelectedItem= function(newMatter:Operations.DtoDepartment) {
    var newItem = createMatterParamOption(newMatter);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var initMatterById = function(matterId) {
    if(matterId) {
      var matter = $scope.matterList.find(dep => dep.id == matterId);
      $scope.matterSavedSelectedItems = matter ? [createMatterNewSelectedItem(matter)] : [];
    }
  };

  var getMatters = function(callback?) {
    departmentResource.getDepartments(function (matters:Operations.DtoDepartment[]) {
        if (matters) {
          $scope.matterList = matters;
          if (callback) {
            callback();
          }
        }
      }
      , onError);
  };

  var init = function() {
    getMatters(() => { initMatterById($scope.changeMatterSelectedItemId) });
  };

  init();

  $scope.cancel = function(){
    $uibModalInstance.dismiss();
  };

  $scope.save = function(){
    if ($scope.matterSelected) {
      $uibModalInstance.close($scope.matterSelected);
    }
  };

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };

  $scope.onMatterSelected=function(matter:ParamActiveItem<Operations.DtoDepartment>) {
    $scope.matterSelected = matter.item.value;
  };

  var setMatterOptionItems = function(itemsOptions:any[]) {
    $scope.depParentMapping = [];
    $scope.matterList.forEach(matter => {
      if (matter.parentId == null) {
        $scope.depParentMapping[matter.id] = matter.name;
      }
      var newItem = createMatterParamOption(matter);
      itemsOptions.push(newItem);
    });
  };

  $scope.loadMatterOptionItems = function(itemsOptions:any[]) {
    if (!$scope.matterList || $scope.matterList.length == 0) {
      getMatters(() => { setMatterOptionItems(itemsOptions) });
    }
    else {
      setMatterOptionItems(itemsOptions);
    }
  };

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
});


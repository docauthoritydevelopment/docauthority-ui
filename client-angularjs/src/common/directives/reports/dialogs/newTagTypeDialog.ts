///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.tagTypes', [

    ])
    .controller('newTagTypeDialogCtrl', function ($scope, $uibModalInstance, data) {


      var init=function(tagType:Entities.DTOFileTagType)
      {
        if(tagType && tagType.name)
        {
          $scope.editMode = true;
          $scope.inputText =  tagType.name;
          $scope.title='Edit tag type';
          $scope.id=tagType.id;
          $scope.description =tagType.description;
          $scope.sensitive =tagType.sensitive;
          $scope.singleValueTag =tagType.singleValueTag;
        }
        else
        {
          $scope.title='Add tag type';
        }
        $scope.initiated=true;
      }

      init(data.tagType);

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $scope.submitted=true;
        if($scope.myForm.$valid)
        {
          var newTag = new Entities.DTOFileTagType();
          newTag.description = $scope.description;
          newTag.name=$scope.inputText;
          newTag.sensitive =$scope.sensitive;
          newTag.singleValueTag =$scope.singleValueTag;
          newTag.id =$scope.id;
          $uibModalInstance.close(newTag);
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })


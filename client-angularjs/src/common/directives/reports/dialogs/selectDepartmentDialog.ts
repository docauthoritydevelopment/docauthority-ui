///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.department.select', []).controller('selectDepartmentDialogCtrl',
  function ($scope, $uibModalInstance, departmentResource:IDepartmentResource, data, dialogs, configuration:IConfig,
            splitViewBuilderHelper:ui.ISplitViewBuilderHelper) {
  $scope.filterMode = data.filterMode;
  $scope.title=$scope.filterMode?"Choose department to filter": "Choose department to assign";
  $scope.changeDepartmentSelectedItemId=data.changeSelectedItemId;

  $scope.departmentList = [];

  //-- Methods --//

  var createDepartmentParamOption = function(department:Operations.DtoDepartment) {
    var newItem = new ParamOption<Operations.DtoDepartment>();

    if (department) {
      newItem.id = department.id;
      newItem.display = department.name;
      newItem.value = department;
      newItem.subdisplay = "";
      newItem.groupByLable = "";
      newItem.icon = configuration.icon_department + ' tag tag-' + splitViewBuilderHelper.getDepartmentStyleId(<any>department);
      newItem.displayClass = !department.parentId ? 'bolder' : null;
      if ($scope.depParentMapping) {
        newItem.subdisplay = department.parentId ? $scope.depParentMapping[department.parentId] : "";
        newItem.groupByLable = department.parentId ? $scope.depParentMapping[department.parentId] : department.name;
      }
    }

    return newItem;
  };

  var createDepartmentNewSelectedItem= function(newDepartment:Operations.DtoDepartment) {
    var newItem = createDepartmentParamOption(newDepartment);
    var paramActiveItem = new ParamActiveItem();
    paramActiveItem.active = true;
    paramActiveItem.item = newItem;
    return paramActiveItem;
  };

  var initDepartmentById = function(departmentId) {
    if(departmentId) {
      var department = $scope.departmentList.find(dep => dep.id == departmentId);
      $scope.departmentSavedSelectedItems = department ? [createDepartmentNewSelectedItem(department)] : [];
    }
  };

  var getDepartments = function(callback?) {
    departmentResource.getDepartments(function (departments:Operations.DtoDepartment[]) {
        if (departments) {
          $scope.departmentList = departments;
          if (callback) {
            callback();
          }
        }
      }
      , onError);
  };

  var init = function() {
    getDepartments(() => { initDepartmentById($scope.changeDepartmentSelectedItemId) });
  };

  init();

  $scope.cancel = function(){
    $uibModalInstance.dismiss();
  };

  $scope.save = function(){
    if ($scope.departmentSelected) {
      $uibModalInstance.close($scope.departmentSelected);
    }
  };

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };

  $scope.onDepartmentSelected=function(department:ParamActiveItem<Operations.DtoDepartment>) {
    $scope.departmentSelected = department.item.value;
  };

  var setDepartmentOptionItems = function(itemsOptions:any[]) {
    $scope.depParentMapping = [];
    $scope.departmentList.forEach(department => {
      if (department.parentId == null) {
        $scope.depParentMapping[department.id] = department.name;
      }
      var newItem = createDepartmentParamOption(department);
      itemsOptions.push(newItem);
    });
  };

  $scope.loadDepartmentOptionItems = function(itemsOptions:any[]) {
    if (!$scope.departmentList || $scope.departmentList.length == 0) {
      getDepartments(() => { setDepartmentOptionItems(itemsOptions) });
    }
    else {
      setDepartmentOptionItems(itemsOptions);
    }
  };

  var onError = function() {
    dialogs.error('Error','Sorry, an error occurred.');
  };
});


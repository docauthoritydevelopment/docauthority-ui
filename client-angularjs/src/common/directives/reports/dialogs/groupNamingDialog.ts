///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.debug', [

    ])
    .controller('groupNamingDialogCtrl', function ($scope, $uibModalInstance, data:GroupNaming.DTOGroupNamingDebugData) {
      $scope.formData=data.factors;
      $scope.name = data.groupDto.groupName;
      $scope.newName = data.newName;
      $scope.data=data;
      $scope.useFilename=data.useFilename;
      //$scope.topToken = JSON.stringify(data.topToken);

     $scope.exportToExcel= function () {
        if($scope.data) {
          var createTokensRows=function(data:GroupNaming.DTOToken[]) {
            var rows = [];

            rows.push({ //columns header
              cells: [
                {value: 'token'},
                {value: 'tokenScore'},
                {value: 'nLen'},
                {value: 'nTf'},
                {value: 'nTfidf'},
                {value: 'nWords'},
                {value: 'tf'},
                {value: 'tfidf'},
                {value: 'titRate'},
                {value: 'titOrd'},
                {value: 'titLen'},
              ]
            });
            if (data && data.length > 0) {
              for (var i = 0; i < data.length; i++) {

                var dataItem = (data)[i];

                rows.push({
                  cells: [
                    {value: dataItem.token},
                    {value: dataItem.tokenScore},
                    {value: dataItem.nLen},
                    {value: dataItem.nTf},
                    {value: dataItem.nTfidf},
                    {value: dataItem.nWords},
                    {value: dataItem.tf},
                    {value: dataItem.tfidf},
                    {value: dataItem.titleRate},
                    {value: dataItem.titleOrder},
                    {value: dataItem.titleLen},
                  ]
                });

              }

            }
            return rows;
          }

          var createFactorssRows=function(data:GroupNaming.DTOFactors) {
            var rows = [];

            rows.push({ //columns header
              cells: [
                {value: 'factor'},
                {value: 'value'},
              ]
            });
            if (data) {
              for (var f in data) {
                if (data.hasOwnProperty(f)) {}
                var dataItem = (data)[f];
                rows.push({
                  cells: [
                    {value: f},
                    {value: dataItem},
                  ]
                });
              }
            }
            return rows;
          }

          var createSheet=function(rows,title)
          {

            var sheet = {
              columns: [
                {autoWidth: true},
                {autoWidth: true},

              ],
              title: title,
              rows: rows
            }
            return sheet;

          };
          var workbook = new kendo.ooxml.Workbook({sheets:[
          //createSheet( createTokensRows([data.topToken]),'topToken'),
          createSheet( createTokensRows(data.combinedListTokens),'combined'),
          createSheet( createTokensRows(data.filenameBasedTokens),'filename'),
          createSheet( createTokensRows(data.titleBasedTokens),'title'),
          createSheet( createTokensRows(data.parentDirBasedTokens),'parentDir'),
          createSheet( createTokensRows(data.grandparentDirBasedTokens),'grandParentDir'),
          createSheet( createFactorssRows(data.factors),'factors'),

        ]});
          kendo.saveAs({dataURI: workbook.toDataURL(), fileName: 'group naming data_'+ $scope.id+'.xlsx'});
         }

      };

      //-- Methods --//

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.save = function(){
        $uibModalInstance.close({restart:true,weightData: $scope.formData,groupId:$scope.data.groupDto.id,saveNewName:$scope.saveNewName});
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.save();
      };
    })
    .controller('GroupNamingDebugDataCtrl', function ($scope, $state,$window, $location,Logger,$timeout,$stateParams,$element,
                                             propertiesUtils, configuration:IConfig,eventCommunicator:IEventCommunicator,localStorage:IUserStorageService) {
      var log: ILog = Logger.getInstance('GroupNamingDebugDataCtrl');

      $scope.elementName = 'group-naming--grid';
      $scope.mainGridOptions = null;
      var gridOptions:ui.IGridHelper;

      $scope.init = function()
      {
        var gridBuilder:ui.IGridOptionsBuilder<GroupNaming.DTOToken> = new KendoGridOptionsBuilder<GroupNaming.DTOToken>();
        gridOptions = gridBuilder.createGrid(null, getDataUrlParams, fields,configuration.max_data_records_per_request, null, null,false, null, null, null);

        gridOptions.gridSettings.pageable=false;
        parseData();
        gridOptions.gridSettings.dataSource.data=$scope.data;
      //  $scope.mainGridOptions = gridOptions.gridSettings;
      };

      var dTOToken:GroupNaming.DTOToken =  GroupNaming.DTOToken.Empty();

      var token:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.token),
        title: 'token',
        type: EFieldTypes.type_string,
        displayed: true,
        editable: false,
        nullable: true,
        width:'35%'
      };
      var tokenScore:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.tokenScore),
        title: 'tokenScore',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var nLen:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.nLen),
        title: 'nLen',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var nTf:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.nTf),
        title: 'nTf',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var nTfidf:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.nTfidf),
        title: 'nTfidf',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var nWords:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.nWords),
        title: 'nWords',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var tf:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.tf),
        title: 'tf',
        type: EFieldTypes.type_number,
        displayed: true,
        editable: false,
        nullable: true
      };
      var tfidf:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.tfidf),
        title: 'tfidf',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var titleRate:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.titleRate),
        title: 'titRate',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var titleOrder:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.titleOrder),
        title: 'titOrd',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };
      var titleLen:ui.IFieldDisplayProperties = <ui.IFieldDisplayProperties>{
        fieldName: propertiesUtils.propName(dTOToken, dTOToken.titleLen),
        title: 'titLen',
        type: EFieldTypes.type_number,
        format: '{0:n2}',
        displayed: true,
        editable: false,
        nullable: true
      };


      var fields:ui.IFieldDisplayProperties[] =[];
      fields.push(token);
      fields.push(tokenScore);
      fields.push(tfidf);
      fields.push(tf);
      fields.push(nTfidf);
      fields.push(nTf);
      fields.push(nLen);
      fields.push(nWords);
      fields.push(titleRate);
      fields.push(titleOrder);
      fields.push(titleLen);

      var parseData = function () {
        //if($scope.data)
        //{
        //  $scope.data.forEach(d=> (<any>d).isScanned = d.numberOfFoldersFound>0?true:false);
        //}
      }
      $scope.$watch(function () { return $scope.data; }, function (value) {
        setData();
      });
      function setData()
      {
        parseData();
        $timeout(function() {
          var grid = $element;;
          if (grid[0]&&$scope.data) {
             gridOptions.setData(grid, $scope.data);
          }
        });
      }

      var getDataUrlParams = function() {
        return null;
      };

      $scope.init();
    })
    .directive('groupNamingTokenDataGrid', function(){
      return {
        restrict: 'EA',
        template:  '<div  class="fill-height {{elementName}}" kendo-grid ng-transclude k-on-data-binding1="dataBinding(e,r)"  k-on-change1="handleSelectionChange(data, dataItem, columns)" k-on-data-bound="onDataBound()" k-options="mainGridOptions" ></div>',
        replace: true,
        transclude:true,
        scope://false,
        {
          data:'=',
        },
        controller: 'GroupNamingDebugDataCtrl',
        link: function (scope:any, element, attrs) {

        }
      }

    });


///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.actions.result', [
    ])
    .controller('triggerImmediateActionResultDialogCtrl',
        function ($scope, $uibModalInstance, data, configuration:IConfig,  immediateActionsResource:IImmediateActionsResource,dialogs,$timeout,
                  eventCommunicator:IEventCommunicator) {


          $scope.scriptFolders = data.scriptFolders;
          var actionTriggerId = data.actionTriggerId;
          $scope.downLoadUrl = configuration.immediate_action_scripts_files_url.replace(':actionTriggerId', actionTriggerId);
          $scope.actionInProgress = true;


          var checkStatus = function () {
            immediateActionsResource.immediateActionsProgress(actionTriggerId, immediateActionsProgressCompleted, onError);
          };


          var immediateActionsProgressCompleted = function (status: Actions.DtoActionExecutionTaskStatus) {
            $scope.initiated = true;
            $scope.status = status;
            $scope.actionTriggerId = status.actionTriggerId;
            $scope.currentImmediateActionProgressPercentage = Math.round(status.progressMark / status.progressMaxMark * 100);
            $scope.actionInProgress = status.status == Actions.DtoActionExecutionTaskStatus.status_RUNNING;
            if( $scope.actionInProgress) {
              $timeout(checkStatus, 17000);
            }
            if( status.status == Actions.DtoActionExecutionTaskStatus.status_FAILED){
              onError();
            }
          };

          checkStatus();

          $scope.$on("$destroy", function () {
        //    eventCommunicator.unregisterAllHandlers($scope);
          });
          $scope.cancel = function () {
            $uibModalInstance.dismiss('Canceled');
          }; // end cancel
          var onError = function () {
            dialogs.error('Error', 'Sorry, an error occurred.');
          }
          // eventCommunicator.registerHandler(EEventServiceEventTypes.ReportActionsRunningTasksStatusUserAction,
          //     $scope,
          //     (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
          //       /*
          //        * fireParam object:
          //        *   { initSearch: false, value: searchText, mode: mode}
          //        */
          //       if (fireParam.action === 'checkStatus') {
          //
          //         $scope.downLoadUrl=configuration.immediate_action_scripts_files_url.replace(':actionTriggerId',actionTriggerId);
          //         $scope.actionInProgress=!fireParam.enable;
          //       }
          //     })));


          $scope.hitEnter = function (evt) {
            if (angular.equals(evt.keyCode, 13))
              $scope.save();
          };
        })

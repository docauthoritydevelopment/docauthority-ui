///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.labels', [
      "resources.label"
    ])
    .controller('applyLabelDialogCtrl', function ($scope, $uibModalInstance, labelResource:ILabelResource,dialogs) {

      $scope.selectedLabelId = null;
      $scope.selectedOption = 0 ;

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      };


      $scope.onLabelSelected= (labelObj:any) => {
        $scope.selectedLabelId = labelObj.item.id;
      };



      $scope.save = function() {
        if ($scope.selectedOption == 1) {
          $uibModalInstance.close($scope.selectedLabelId);
        }
        else {
          $uibModalInstance.close(-1);
        }
      };


      function convertToParamObtion(labelObj:any):any {
        var newItem = new ParamOption();
        newItem.id = labelObj.id;
        newItem.display = labelObj.name;
        newItem.subdisplay = ' ';
        newItem.value = labelObj.id;
        return newItem;
      }

      $scope.loadLabelOptionItems = (labelOptions:any[])=>  {
        labelResource.getLabels(function (labelList:Entities.DTOLabel[]) {
            labelList.forEach((labelObj:any)=> {
              labelOptions.push(convertToParamObtion(labelObj));
            });
          }
          , onError);
      };



      var onError = function() {
        dialogs.error('Error','Sorry, an error occurred.');
      };


    })


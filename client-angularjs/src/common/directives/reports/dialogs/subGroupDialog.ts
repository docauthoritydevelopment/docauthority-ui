///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.subGroup', [

])
  .controller('subGroupDialogCtrl', function ($scope, $uibModalInstance, data,groupResource:IGroupResource) {

    var rightFileOption;

    var init=function(group:Entities.DTOGroup)
    {
      $scope.originalGroup = group;
      $scope.originalGroupName = group.groupName;
      if( group.groupType == Entities.EGroupType.SUB_GROUP)
      {
        $scope.editMode = true;
        $scope.title='Edit sub group cluster';
        initGroupName(group.rawAnalysisGroupId);
        initGroupCluster(group.rawAnalysisGroupId);
      }
      else if( group.groupType == Entities.EGroupType.ANALYSIS_GROUP)
      {
        $scope.title='Create sub groups cluster';
        $scope.initiated=true;

      }

    }


    var initGroupCluster = function(parentGroupId)
    {
      if(parentGroupId) {
        groupResource.listRulesForSubGroupCluster(parentGroupId, function (data: DTOPagedResult<Entities.DtoRefinementRuleDto>) {
          $scope.inputText = data.content.map(t=>t.predicate.values[0]);
          $scope.initiated=true;
        }, function () {
        })
      }
    }

    var initGroupName = function(parentGroupId)
    {
      if(parentGroupId) {
        groupResource.getGroupById(parentGroupId, function (data:Entities.DTOGroupDetails) {
          $scope.groupName = data.groupDto.groupName;
        }, function () {
        })
      }
    }
    init(data.group);


    $scope.checkIncorrectToken = function() {
      let theText = $scope.inputText;
      if (theText.split(',').filter((token) => {
        return (!token || token.length < 3);
      }).length > 0) {
        $scope.incorrectTokens = true;
      }
      else {
        $scope.incorrectTokens = false;
      }
    }


    $scope.cancel = function(){
      $uibModalInstance.dismiss('Canceled');
    }; // end cancel

    $scope.removeAll = function(){
      $scope.submitted=true;
      $uibModalInstance.close(null);
    };
  $scope.save = function(){
      $scope.submitted=true;
      if($scope.myForm.$valid && !$scope.incorrectTokens)
      {
        $uibModalInstance.close($scope.inputText);
      }
    };

    $scope.hitEnter = function(evt){
      if(angular.equals(evt.keyCode,13))
        $scope.save();
    };
  })


///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.savedFilters', []).controller('saveFilterDialogCtrl', function ($scope, $uibModalInstance, data) {
  $scope.leftOptionSelectedChanged = function() {
    (<EPaneOption[]>(data.rights)).forEach(rightOption=>
      (<any>rightOption).disabled  = $scope.isRightDisabled($scope.leftOptionSelected.view,rightOption,'dialog')
    );

    if ($scope.rightOptionSelected.disabled) {
      $scope.rightOptionSelected =  (<EPaneOption[]>(data.rights)).filter(p=>p.view == EntityDisplayTypes.files.toString())[0];
    }
  };

  var init=function(savedFilter:ui.SavedDiscoveredFilter) {
    $scope.lefts=data.lefts;
    $scope.rights=data.rights;
    $scope.globalFilter = true;
    if(savedFilter && savedFilter.name) {
      $scope.savedFilter = savedFilter;
      $scope.title='Edit filter';
      $scope.leftOptionSelected = (<EPaneOption[]>(data.lefts)).filter(p=>p.view == savedFilter.leftEntityDisplayTypeName)[0];
      $scope.rightOptionSelected = (<EPaneOption[]>(data.rights)).filter(p=>p.view == savedFilter.rightEntityDisplayTypeName)[0];
      $scope.globalFilter = $scope.savedFilter.globalFilter;
    }
    else {
      $scope.title='Add filter';
      savedFilter = new ui.SavedDiscoveredFilter();
      savedFilter.savedFilterDto = null;
      if (data.kendoFilter) {
        savedFilter.savedFilterDto = new Entities.SavedFilterDto();
        savedFilter.savedFilterDto.filterDescriptor = data.kendoFilter;
        savedFilter.savedFilterDto.rawFilter = data.rawFilter;
      }
      savedFilter.displayType = data.displayType;
      $scope.savedFilter = savedFilter;
      $scope.leftOptionSelected = (<EPaneOption[]>(data.lefts)).filter(p=>p.view == data.leftEntityDisplayTypeName)[0];
      $scope.rightOptionSelected = (<EPaneOption[]>(data.rights)).filter(p=>p.view == data.rightEntityDisplayTypeName)[0];
    }
    $scope.isView = $scope.savedFilter.displayType == Entities.EFilterDisplayType.DISCOVER_VIEW;
    if ($scope.isView) {
      $scope.title = $scope.title.replace("filter", "view");
    }
    $scope.isRightDisabled=data.isRightDisabled;
    $scope.leftOptionSelectedChanged();
    $scope.initiated=true;
  };

  $scope.hasFilter =function() {
    return $scope.filter!=null;
  };

  $scope.savedFilter = data.savedFilter;
  init(data.savedFilter);

  $scope.cancel = function() {
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function() {
    $scope.submitted=true;
    if($scope.myForm.$valid) {
      if ($scope.savedFilter.savedFilterDto) {
        (<ui.SavedDiscoveredFilter>$scope.savedFilter).savedFilterDto.name = (<ui.SavedDiscoveredFilter>$scope.savedFilter).name;
      }
      (<ui.SavedDiscoveredFilter>$scope.savedFilter).leftEntityDisplayTypeName = $scope.leftOptionSelected.view;
      (<ui.SavedDiscoveredFilter>$scope.savedFilter).rightEntityDisplayTypeName =  $scope.rightOptionSelected.view;
      (<ui.SavedDiscoveredFilter>$scope.savedFilter).globalFilter = $scope.globalFilter;
      $uibModalInstance.close( $scope.savedFilter);
    }
  };

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };
});


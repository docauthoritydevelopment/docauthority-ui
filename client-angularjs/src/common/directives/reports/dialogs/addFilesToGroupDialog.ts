///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.files.grouping', [

    ])
.controller('addFilesToGroupDialogCtrl', function ($scope, $uibModalInstance ,dialogs,data,userSettings:IUserSettingsService,groupsResource:IGroupsResource) {


  $scope.title = data.title;

  $scope.filesList=data.filesList;
  $scope.entityType = EntityDisplayTypes.groups
 //$scope.groupsList=data.groupsList;
 // $scope.groupsList?$scope.groupsList.push({id:'newGroup',name:' Create New Group  '}):null;
  $scope.filesLeftOutNumber = data.filesLeftOutNumber;
  $scope.selectedFiles=[];
  $scope.showFilesList=true;
  $scope.newGroupMode = !$scope.groupsList||$scope.groupsList.length==0;
  if($scope.filesList)
  {
    for(var i=0; i< $scope.filesList.length; i++) {
      $scope.selectedFiles[i]=true;
    }
  }

  $scope.setActiveTab=function(tabName:string)
  {
    $scope.activeTab=tabName;
  }
  $scope.setActiveTab( 'selectGroup');
  $scope.initiated=true;
  //-- Methods --//

  $scope.cancel = function(){
    $uibModalInstance.dismiss('Canceled');
  }; // end cancel
  $scope.filesAreSelected = function()
  {
   return $scope.selectedFiles.filter(s=>s==true)[0]!=null;
  }

  $scope.selectedFilesCount = function()
  {
    var selected = $scope.selectedFiles.filter(s=>s==true);
     return selected.length;
  }

  $scope.openNewGroupDialog = function(successFunction,cancelFunction)
  {
    var dlg = dialogs.create(
        'common/directives/dialogs/genericDialogs.tpl.html',
        'openSingleInputDialogCtrl',
        {bodyText:'Type new group name: ',title:'New group for files'},
        {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (groupName) {
      successFunction(new DTOMarkedItem(null,EntityDisplayTypes.groups, groupName));
    }, function () {
      cancelFunction?cancelFunction():null;
    });

  }

  $scope.onGroupItemSelected=function(group:ParamActiveItem<Entities.DTOGroup>)
  {
    $scope.groupSelected  =group.item.value;
    $scope.groupSelectedSubdisplay  =group.item.subdisplay;
  }

  $scope.createGroupParamOption= function(group:DTOMarkedItem) {
    var newItem = new ParamOption();
    newItem.id = group.id;
    newItem.display = group.name;
    newItem.subdisplay = group.id? (<any>group).numOfFiles+' files in group':'New group';
    newItem.value = group;
    return newItem;
  }

  $scope.loadGroupOptionItems = function(itemsOptions:any[]) {
    userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[]) {

        var groupsList:DTOMarkedItem[] = savedActiveRecentlyUsedMarkedItems?savedActiveRecentlyUsedMarkedItems.filter(f=>$scope.entityType.equals(f.entity)):[];

        if (groupsList.length > 0) {
          groupsResource.getGroups(groupsList.map(r=>r.id), function (groups:Entities.DTOGroup[]) {
            var groupsDic = {};
            groups.forEach(g=>groupsDic[g.id] = g);
            groupsList.forEach(r=>
            {r.name = (<Entities.DTOGroup>groupsDic[r.id]).groupName;
              (<any>r).numOfFiles = (<Entities.DTOGroup>groupsDic[r.id]).numOfFiles;
            });

            groupsList.forEach(group=> {
                var newItem = $scope.createGroupParamOption(group);
                itemsOptions.push(newItem);
              })
            $scope.groupsList = groupsList;

          }, onError);

        }

    });

  }

  $scope.save = function(){
    var checkedItems=[];
    for(var i=0; i< $scope.filesList.length; i++)
    {
      if($scope.selectedFiles[i])
      {
        checkedItems.push($scope.filesList[i]);
      }
    }
    if($scope.filesAreSelected()&&$scope.groupSelected ) {
      $uibModalInstance.close({newGroupName:$scope.groupSelected.name, checkedFileItems:checkedItems,selectedGroupId:$scope.groupSelected.id});
    }
  };

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13))
      $scope.save();
  };

  var onError = function()
  {
    dialogs.error('Error','Sorry, an error occurred.');
  }
})


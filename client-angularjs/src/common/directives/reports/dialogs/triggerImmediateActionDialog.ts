///<reference path='../../../all.d.ts'/>
'use strict';

angular.module('directives.dialogs.actions', [
      'resources.acls',
      'directives.dialogs.actions.result',
      'directives.dialogs.folderBrowser'

    ])
    .controller('triggerImmediateActionDialogCtrl', function ($scope, $uibModalInstance, data, aclsResource:IAclsResource, dialogs, $timeout) {


      //-- Methods --//

      $scope.filter=data.filter;
      $scope.baseFilterField=data.baseFilterField;
      $scope.baseFilterValue=data.baseFilterValue;
      $scope.initiateViewAllDialogOpenState=true;
      $scope.operations = [Operations.EUpdateFileOperationType.SET,Operations.EUpdateFileOperationType.CLEAR];
      $scope.propertyName = 'TAG';
      $scope.operation=Operations.EUpdateFileOperationType.SET;
      var queueAutoImport;
      var init=function()
      {

        $scope.actions=data.actions;
        $scope.actionOptionSelected = data.selectedAction;
        $scope.title='Create action script';
        $scope.subTitleForAll='This operation will effect '+data.effectedItems+' files.';
        $scope.subTitleForSelected='This operation will effect '+data.effectedItems+' files.';
        $scope.effectedItemsAll = data.effectedItemsAll;
        $scope.effectedItemsSelected =data.effectedItemsSelected;
        $scope.allowForSelectedItems =data.allowForSelectedItems;
        $scope.applyToSelectedItemsOnly =   $scope.allowForSelectedItems;
        if( data.autoImportParams)
        {
          $scope.autoImportParamsFromFilterData(data.autoImportParams);
        }

        $scope.active=$scope.actionOptionSelected?'actionParams':'actionType';
        $scope.initiated=true;
      }
      var importAclListFromFilterData = function(isWrite:boolean)
      {
        if(!isWrite)
        {
          aclsResource.getAclReads(1200,$scope.baseFilterValue,$scope.baseFilterField,$scope.filterData,
              function (acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[], totalElements:number) {
                onCompleted(acls,totalElements);

              }
              , onError)
        }
        else {
          aclsResource.getAclWrites(1200,$scope.baseFilterValue,$scope.baseFilterField,$scope.filterData,
              function (acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[], totalElements:number) {
                onCompleted(acls,totalElements);

              }
              , onError)
        }

        function onCompleted(acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[], totalElements:number) {
          if( acls&&acls[0]) {
            var parsedAcls =  acls.map(acl=>
            {
              var paramActiveItem = createNewSelectedItem(acl);
              return paramActiveItem;
            })
            $scope.aclSavedSelectedItems = parsedAcls;

          }

        };
      }

      $scope.autoImportEntity1 = EntityDisplayTypes.acl_reads;
      $scope.autoImportEntity2 = EntityDisplayTypes.acl_writes;
      $scope.autoImportParamsFromFilterData = function(paramType:EntityDisplayTypes)
      {
        if(EntityDisplayTypes.acl_reads.equals(paramType)||EntityDisplayTypes.acl_writes.equals(paramType)) {
          importAclListFromFilterData( EntityDisplayTypes.acl_writes.equals(paramType)?true:false);
        }
      }



      init();

      $scope.onAclItemsSelected =function(aclItems)
      {
        var checkedSelectedItems=aclItems.filter(i=>i.active).map(s=>(<any>s).item);
        $scope.selectedAclItems=checkedSelectedItems;
      }
      $scope.isPermissionActionSelected=function()
      {
        return $scope.actionOptionSelected=='ADD_PERMISSION' || $scope.actionOptionSelected=='REMOVE_PERMISSION';
      }
      $scope.isServerTargetFolderActionSelected=function()
      {
        return $scope.actionOptionSelected=='COPY_FILE' || $scope.actionOptionSelected=='MOVE_FILE'|| $scope.actionOptionSelected=='DECRYPT_FILE'|| $scope.actionOptionSelected=='ENCRYPT_FILE';
      }
      $scope.isFilePropertyUpdateActionSelected=function()
      {
        return $scope.actionOptionSelected=='UPDATE_FILE_PROPERTY';
      }


      $scope.$watch(function () { return   $scope.actionOptionSelected; }, function (value) {
        $scope.selectedAclItems=null;
        $scope.inputParamsText='';
        $scope.targetParam='';
      });

      $scope.openBrowseServerFoldersDialog = function() {
        var dlg = dialogs.create(
            'common/directives/dialogs/folderBrowserDialog.tpl.html',
            'folderBrowserDialogCtrl',
            {rootFolder: null, selectedFolder: $scope.targetParam, title: "Select target folder"},
            {size: 'lg'});

        dlg.result.then(function (newFolderPath:string) {
          $scope.targetParam=newFolderPath;
        }, function () {

        });
      }



      $scope.loadAclsOptionItems = function(itemsOptions:any[])
      {
        aclsResource.getAclReads( $scope.MAX_ITEMS, null,null,null,
              function (acls:Entities.DTOAggregationCountItem<Entities.DTOFileUser>[], totalElements:number) {
                acls.forEach(acl=> {
                  var newItem = createParamOption(acl);
                  itemsOptions.push(newItem);
                })
              }
              , onError)
      }
      var createParamOption= function(acl:Entities.DTOAggregationCountItem< Entities.DTOFileUser>) {
        var newItem = new ParamOption();
        newItem.id = acl.item.user;
        newItem.value = acl.item;
        return newItem;
      }
      var createNewSelectedItem= function(acl:Entities.DTOAggregationCountItem< Entities.DTOFileUser>)
      {
        var newItem = createParamOption(acl);
        var paramActiveItem = new ParamActiveItem();
        paramActiveItem.active = true;
        paramActiveItem.item = newItem;
        return paramActiveItem;

      }
      $scope.createNewAclSelectedItemFromText = function(text) {
        var newUser = new Entities.DTOAggregationCountItem< Entities.DTOFileUser>();
        newUser.item = new Entities.DTOFileUser();
        newUser.item.user = text;
        var paramActiveItem = createNewSelectedItem(newUser);
        return paramActiveItem;
      }

      $scope.cancel = function(){
        $uibModalInstance.dismiss('Canceled');
      }; // end cancel

      $scope.actionParamValid = function()
      {
        if($scope.isPermissionActionSelected())
        {
          if(!$scope.selectedAclItems||$scope.selectedAclItems.length==0)
          {
            return false;
          }
        }
        if(  $scope.isServerTargetFolderActionSelected())
        {
          if(!$scope.targetParam||$scope.targetParam.trim()=='')
          {
            return false;
          }
        }
        if(  $scope.isFilePropertyUpdateActionSelected())
        {
          if(!$scope.targetParam||$scope.targetParam.trim()=='')
          {
            return false;
          }
          if(!$scope.propertyName||$scope.propertyName.trim()=='')
          {
            return false;
          }
          if(!$scope.propertyValue||$scope.propertyValue.trim()=='')
          {
            return false;
          }
        }
       return true;
      }

      $scope.operationValid = function()
      {

        if(  $scope.isFilePropertyUpdateActionSelected())
        {
          if(!$scope.operation) {
            return false;
          }
        }
        return true;
      }

      $scope.start = function(){
        $scope.submitted=true;
        if($scope.actionOptionSelected &&  $scope.actionParamValid () && $scope.operationValid()) {
          var immediateAction = new Actions.DtoExecuteActionRequest();
          immediateAction.actionDefinitionId = $scope.actionOptionSelected;
          if($scope.isPermissionActionSelected())
          {
            var usersList=$scope.selectedAclItems.map(a=><ParamOption<Actions.DtoExecuteActionRequest>>a.id);
            immediateAction.parameters={'users':JSON.stringify(usersList)};
          }
          else if(  $scope.isServerTargetFolderActionSelected())
          {
            immediateAction.parameters={'target':$scope.targetParam};
          }
          else if(  $scope.isFilePropertyUpdateActionSelected())
          {
            immediateAction.actionDefinitionId  =  $scope.operation==Operations.EUpdateFileOperationType.SET?'ADD_PROPERTY_VALUE':'REM_PROPERTY_VALUE'
            immediateAction.parameters={'tempFolder':$scope.targetParam,
             'property':$scope.propertyName,'value':$scope.propertyValue};
          }
          else {
            immediateAction.parameters={'param1':$scope.inputParamsText};
          }

          $uibModalInstance.close({applyToSelectedItemsOnly:$scope.applyToSelectedItemsOnly,immediateAction:immediateAction});
        }
      };

      $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13))
          $scope.start();
      };

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }
    })

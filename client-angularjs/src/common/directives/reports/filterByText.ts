///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.splitView.filterByText', [

  ])
  .controller('filterByTextCtrl', function ($scope,$element,$window,Logger:ILogger,$q,configuration:IConfig,$timeout,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                             userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,dateRangeResource:IDateRangeResource,splitViewFilterService:IFilterService) {
    var log:ILog = Logger.getInstance('filterByTextCtrl');


    $scope.init = function () {
      fillSearchText();
    };

    $scope.doSearchText = function(){
       filterBySearchText($scope.searchText);
    };
    $scope.doEntitySearchText = function(){
      filterByEntitySearchText($scope.searchText);
    };

    $scope.searchTextChange = function() {
      if(_.isEmpty($scope.searchText)){
        $scope.clearSearchText();
      }
    }

    $scope.clearSearchText = function() {
      $scope.searchText = '';

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'removeRootFolderTextSearch',
      });
    };

    $scope.$on("$destroy", function() {
      eventCommunicator.unregisterAllHandlers($scope);
    });

    var fillSearchText = function() {
      var pageFilters = splitViewFilterService.getFilterData().getPageFilter();
      if(pageFilters && pageFilters.hasCriteria()) {
        var tagsCriterias = pageFilters.getAllCriteriaLeafs().filter(f=>f.fieldName == configuration.rootFolderNameLike);
        if(tagsCriterias[0]) {
          $scope.searchText = tagsCriterias[0].value;
        }
        else {
          $scope.searchText = null;
        }
      }
      else {
        $scope.searchText = null;
      }
    }

    var filterBySearchText = function(searchText:string)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doRootFolderTextSearch',
        searchText: searchText,
      });
    };

    var filterByEntitySearchText = function(searchText:string)
    {

      eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
        action: 'doEntityTextSearch',
        searchText: searchText,
        entityDisplayType:$scope.displayType,
      });
    };
    eventCommunicator.registerHandler(EEventServiceEventTypes.ReportRootFolderByTexSetState,
      $scope,
      (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
        /*
         * fireParam object:
         *   { initSearch: false, value: searchText, mode: mode}
         */
        if (fireParam.action === 'reportUpdateState') {
          fillSearchText()
        }
      })));


  })
  .directive('filterByText',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/filterByText.tpl.html',
        controller: 'filterByTextCtrl',

        replace:true,
        scope:{
          'displayType':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
  )

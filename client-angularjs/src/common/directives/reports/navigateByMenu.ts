///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.splitView.navigateByMenu', [

  ])
  .controller('navigateByMenuCtrl', function ($scope,$element,Logger:ILogger,$filter) {
    var log:ILog = Logger.getInstance('navigateByMenuCtrl');
    $scope.goIndex = -1;
    var dropDownItem:any;


    $scope.init = function () {
      $scope.menuObject = $element.find(".dropdown-menu");
      dropDownItem  = $element;
      dropDownItem.on('show.bs.dropdown', function () {
        setTimeout(()=>{
          $(".navigate-by-input").focus();
        });
      })
    };

    $scope.checkArrowKeys = (keyEvent) => {
      let displayList:any[]  = $filter('orderBy')($filter('filter')($scope.viewOptions,{title : $scope.searchNavBy}),"title");
      if (keyEvent.which === 40) {  //arrow down
        let lastLocation = $scope.goIndex;
        do  {
          $scope.goIndex = $scope.goIndex + 1;
        }
        while ($scope.goIndex < $scope.viewOptions.length - 1 && $scope.viewDisabled && $scope.viewDisabled(displayList[$scope.goIndex]))
        if ($scope.goIndex >= $scope.viewOptions.length) {
          $scope.goIndex = lastLocation;
        }
      }
      else if (keyEvent.which === 38) {  //arrow up
        let lastLocation = $scope.goIndex;
        do  {
          $scope.goIndex = $scope.goIndex - 1;
        }
        while ($scope.goIndex >= 0  && $scope.viewDisabled && $scope.viewDisabled(displayList[$scope.goIndex]))
        if ($scope.goIndex < 0 ) {
          $scope.goIndex = lastLocation;
        }
      }
      else if (keyEvent.which === 13) {  //enter
        if ($scope.goIndex >=0 && $scope.goIndex<displayList.length) {
          dropDownItem.dropdown("toggle");
          $scope.onChangeFunc(displayList[$scope.goIndex]);
        }
      }
      else {
        $scope.goIndex = 0 ;
        return;
      }
      keyEvent.stopImmediatePropagation();
      keyEvent.preventDefault();
      setTimeout(function () {
        $scope.menuObject.find('.go-index-highlight')[0].scrollIntoView(false);
      });


    };



  })
  .directive('navigateByMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/navigateByMenu.tpl.html',
        controller: 'navigateByMenuCtrl',
        replace:true,
        scope:{
          'btnDisabled' : '=',
          'viewOptions' : '=',
          'viewPermitted' : '=',
          'onChangeFunc' : '=',
          'title': '@',
          'className': '@',
          'viewDisabled': '='
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
  )

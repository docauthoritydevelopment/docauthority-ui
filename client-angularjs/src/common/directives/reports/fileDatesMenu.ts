///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.fileDates', [
      'directives.dialogs.dateRange.filter',
      'resources.dateRange',
      'directives.dialogs.dateRange.partition.new'
    ])
    .controller('fileDatesMenuCtrl', function ($scope,$element,$window,Logger:ILogger,$q,configuration:IConfig,$timeout,$filter,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                              userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,dateRangeResource:IDateRangeResource,$state) {
      var log:ILog = Logger.getInstance('fileDatesMenuCtrl');

      var MAX_RECENTLY_USED:number = parseInt(configuration.max_recently_used_docTypes);

      $scope.userFilterOptions = [EFilterContextNames.file_created_date, EFilterContextNames.file_modified_date, EFilterContextNames.file_accessed_date];


      var deafultPartition=new Entities.DTODateRangePartition();
      deafultPartition.id=1;
      deafultPartition.name='default';
      $scope.deafultPartition = deafultPartition;

      $scope.requestSelectChange = function(selectedPartition:Entities.DTODateRangePartition) {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDateMenuChangeAction, {
          action: 'selectDatePartition',
          item: selectedPartition,
        });
      };

      $scope.selectPartition = function(selectedPartition:Entities.DTODateRangePartition)
      {
        if(selectedPartition && (!$scope.selectedPartition || $scope.selectedPartition.id!=selectedPartition.id)) {
          $scope.selectedPartition=selectedPartition;
          $scope.onSelectPartition?$scope.onSelectPartition( selectedPartition.id,selectedPartition.name):null;
          dateRangeResource.getPartition(selectedPartition.id,function(datePartition:Entities.DTODateRangePartition)
          {
            $scope.partitionRangeItems = datePartition.dateRangeItemDtos;
          },onError);
          saveRecentlyUsedDateRange(selectedPartition.id);
        }
      }

      $scope.openDateFiltersPage = function() {
        openInNewTab(ERouteStateName.settings,{activeTab: ESettingsTabType.dateFilters});
      }

      function openInNewTab(stateName,urlParams)
      {
        var url = $state.href(stateName, urlParams);
        url = url.replace('/v1','');
        $window.open(url,'_blank');
      }

      $scope.init = function (isMasterMenu:boolean) {
        $scope.isMasterMenu = isMasterMenu;
        $scope.setSelectedFilterOptionMode(EFilterContextNames.file_created_date,true);
        registerEvents();
        initUserSettingsPreferences();
        loadDatePartitions(initUserSettingsActiveDateRange);
        setUserRoles();


        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });
      };
      var setUserRoles = function()
      {
        $scope.isMngTagConfigUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngTagConfig]);
        $scope.isAssignTagUser =   currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.AssignTag]);
      }
      $scope.iconClass= function(filterType:EFilterContextNames)
      {
        if(!filterType)
        {
          return EFilterContextNames.file_created_date;
        }
        return configuration['icon_'+filterType.value];
      }
      $scope.setSelectedFilterOptionMode = function (value:EFilterContextNames,doNotSave) {
        if ($scope.selectedUserFilterOption!=value) {
          $scope.selectedUserFilterOption = value;
          if(!doNotSave)
          {
            saveCurrentPreferences();
          }
        }
      };

      var loadDatePartitions = function(callback?)
      {
        dateRangeResource.getDateRanges(function(partitions:Entities.DTODateRangePartition[])
        {
          $scope.partitions=partitions;
          callback?callback():null;
        },     onError);
      }


      var initUserSettingsActiveDateRange = function () {
        setActiveRecentlyUsedDateRange();

        $scope.$on(userSettings.activeRecentlyUsedDateRangeItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsedDateRange();
        });
      };
      $scope.toggleMinimizeToolbar=function()
      {
        $scope.minimizeToolbar=!$scope.minimizeToolbar;
        saveCurrentPreferences();
        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
         }, 0);
      }
      var initUserSettingsPreferences = function () {

        userSettings.getDateRangesPreferences(function(preferences){
          $scope.minimizeToolbar=preferences&& typeof (<any>preferences).minimizeToolbar!='undefined'?(<any>preferences).minimizeToolbar:true;
          var selectedFilterOption = preferences&& typeof (<any>preferences).selectedFilterOption!='undefined'?(<any>preferences).selectedFilterOption:null;
          if(selectedFilterOption) {
            $scope.setSelectedFilterOptionMode(selectedFilterOption,true);
          }
        });

      };

      var saveCurrentPreferences = function()
      {
        var pref =  { minimizeToolbar:$scope.minimizeToolbar,selectedFilterOption:$scope.selectedUserFilterOption};
        userSettings.setDateRangesPreferences(pref);

      }


      var setActiveRecentlyUsedDateRanges = function() {
        userSettings.getRecentlyUsedDateRanges(function (savedActiveRecntlyUsedDateRangeObjects) {
              var approvedListOfRecntlyUsedItems = [];
              if (savedActiveRecntlyUsedDateRangeObjects && savedActiveRecntlyUsedDateRangeObjects.length > 0) {
                savedActiveRecntlyUsedDateRangeObjects.forEach(s=> {
                  var foundItem = s;
                  if (foundItem) {
                    if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                      approvedListOfRecntlyUsedItems.push(foundItem);
                    }
                  }
                });
              }
              $scope.savedRecentlyUsedDateRangeItems = approvedListOfRecntlyUsedItems;
            }
        );

      }
      var setActiveRecentlyUsedDateRange = function(skipSelect?:boolean) {
        userSettings.getRecentlyUsedDateRange(function (savedActiveRecentlyUsedDateRangeId) {
          var partitionToSelect =deafultPartition;
                 if (savedActiveRecentlyUsedDateRangeId) {

                  var foundItem = (<Entities.DTODateRangePartition[]>$scope.partitions).filter(s=>s.id == savedActiveRecentlyUsedDateRangeId)[0];
                  if (foundItem) {
                    partitionToSelect = foundItem;
                  }

              }
              $scope.selectPartition( partitionToSelect);
            } );
      }

      var saveRecentlyUsedDateRanges = function(dateRangeToSave:Entities.DTODateRangeItem,fieldType:EFilterContextNames)
      {
        if(dateRangeToSave) {
          (<any>dateRangeToSave).fieldType=fieldType;
          userSettings.getRecentlyUsedDateRanges(function(savedActiveRecntlyUsedDateRangeObjects:Entities.DTODateRangeItem[])
          {
            if (savedActiveRecntlyUsedDateRangeObjects) { //add current active to recenlty used
              var foundInRecentItems = savedActiveRecntlyUsedDateRangeObjects.filter(d=> angular.toJson(d) ==  angular.toJson(dateRangeToSave))[0];
              if (foundInRecentItems) { //if already exists remove it
                var foundIndex = savedActiveRecntlyUsedDateRangeObjects.indexOf(foundInRecentItems);
                savedActiveRecntlyUsedDateRangeObjects.splice(foundIndex, 1);
              }
              savedActiveRecntlyUsedDateRangeObjects.unshift(dateRangeToSave);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
              if (savedActiveRecntlyUsedDateRangeObjects.length > MAX_RECENTLY_USED) {
                savedActiveRecntlyUsedDateRangeObjects.splice(MAX_RECENTLY_USED, savedActiveRecntlyUsedDateRangeObjects.length - MAX_RECENTLY_USED); //limit size of list
              }


            }
            else {
              savedActiveRecntlyUsedDateRangeObjects=[dateRangeToSave];
            }
            userSettings.setRecentlyUsedDateRanges(savedActiveRecntlyUsedDateRangeObjects);
          });


        }
      }

      var saveRecentlyUsedDateRange = function(dateRangeToIdSave)
      {
        if(dateRangeToIdSave) {
               userSettings.setRecentlyUsedDateRange(dateRangeToIdSave);
         }
      }



      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }




      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });


      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      }



      $scope.doDateRangeCustomFilter = function(filterMode?:EFilterContextNames)
      {
        if(filterMode ==  EFilterContextNames.file_modified_date || $scope.selectedUserFilterOption == EFilterContextNames.file_modified_date)
        {
          $scope.doModifiedDateRangeCustomFilter();
        }
        else {
          $scope.doCreatedDateRangeCustomFilter();
        }
      }

      $scope.doModifiedDateRangeCustomFilter = function()
      {
          var dlg = dialogs.create(
              'common/directives/reports/dialogs/dateRangeFilterDialog.tpl.html',
              'dateRangeFilterDialogCtrl',
              {editMode:false},
              {size:'md'});

          dlg.result.then(function(dateRangeToFilter:Entities.DTODateRangeItem){
       //     var  invertRange= dateRangeToFilter.invertRange;
       //    delete dateRangeToFilter.invertRange;
            $scope.doModifiedDateFilter(dateRangeToFilter,false);
          },function(){

          });
      }
      var startSummaryText = function(dateRangeDto:Entities.DTODateRangeItem)
      {
        var  fromPart = dateRangeDto.start == null ? 'Ever' :
            dateRangeDto.start.type == Entities.EDateRangePointType.ABSOLUTE ?
                $filter('date')(dateRangeDto.start.absoluteTime, configuration.dateFormat) : dateRangeDto.start.relativePeriodAmount + ' ' + $filter('translate')(dateRangeDto.start.relativePeriod) + ' ago';

        return fromPart;
      }
      var endSummaryText = function(dateRangeDto:Entities.DTODateRangeItem)
      {
        var  toPart = dateRangeDto.end == null ? 'Today' :
            dateRangeDto.end.type == Entities.EDateRangePointType.ABSOLUTE ?
                $filter('date')(dateRangeDto.end.absoluteTime, configuration.dateFormat) : dateRangeDto.end.relativePeriodAmount + ' ' + $filter('translate')(dateRangeDto.end.relativePeriod);

        return toPart;
      }
      var summaryText = function(dateRangeDto:Entities.DTODateRangeItem)
      {
        return  (startSummaryText(dateRangeDto)+' until ')+endSummaryText(dateRangeDto);
      }
      $scope.doCreatedDateRangeCustomFilter = function()
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/dateRangeFilterDialog.tpl.html',
            'dateRangeFilterDialogCtrl',
            {editMode:false,enableInvertRange:true},
            {size:'md'});

        dlg.result.then(function(dateRangeToFilter:Entities.DTODateRangeItem){
          if(!dateRangeToFilter.name||dateRangeToFilter.name=='')
          {
            dateRangeToFilter.name = summaryText(dateRangeToFilter);
          }
          $scope.doCreatedDateFilter(dateRangeToFilter,false);
         },function(){

        });
      }


      $scope.doDateFilter = function(dateRangeItem:Entities.DTODateRangeItem,invertRange?)
      {
          if($scope.selectedUserFilterOption == EFilterContextNames.file_modified_date)
          {
            $scope.doModifiedDateFilter(dateRangeItem,invertRange);
          }
          else {
            $scope.doCreatedDateFilter(dateRangeItem,invertRange);
          }
      }
      $scope.doModifiedDateFilter= function( dateRangeItem:Entities.DTODateRangeItem,invertRange?)
      {
        var copyOfDateRangeItem={};
        angular.copy(dateRangeItem,copyOfDateRangeItem)
        delete (<any>copyOfDateRangeItem).fieldType;
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDateFilterMenuUserAction, {
          action: 'doModifiedDateRangeFilter',
          operator:invertRange,
          values:  [copyOfDateRangeItem],
        });
        onAfterFilter(dateRangeItem,EFilterContextNames.file_modified_date);
      }
      $scope.doCreatedDateFilter  = function( dateRangeItem:Entities.DTODateRangeItem,invertRange?)
      {
        var copyOfDateRangeItem={};
        angular.copy(dateRangeItem,copyOfDateRangeItem)
        delete (<any>copyOfDateRangeItem).fieldType;
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDateFilterMenuUserAction, {
          action: 'doCreationDateDateRangeFilter',
          operator:invertRange,
          values:  [copyOfDateRangeItem],
        });
        onAfterFilter(dateRangeItem,EFilterContextNames.file_created_date);
      };
      $scope.doAccessedDateFilter= function( dateRangeItem:Entities.DTODateRangeItem,invertRange?)
      {
        var copyOfDateRangeItem={};
        angular.copy(dateRangeItem,copyOfDateRangeItem)
        delete (<any>copyOfDateRangeItem).fieldType;
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDateFilterMenuUserAction, {
          action: 'doAccessedDateRangeFilter',
          operator:invertRange,
          values:  [copyOfDateRangeItem],
        });
        onAfterFilter(dateRangeItem,EFilterContextNames.file_accessed_date);
      };
      $scope.doRecentlyUsedDateFilter=function(recentlyUsedDateRange)
      {
        if(EFilterContextNames.file_created_date.equals(recentlyUsedDateRange.fieldType))
        {
          $scope.doCreatedDateFilter(recentlyUsedDateRange);
        }
        else if(EFilterContextNames.file_modified_date.equals(recentlyUsedDateRange.fieldType)){
          $scope.doModifiedDateFilter(recentlyUsedDateRange);
        }
      }
      var onAfterFilter=function(dateRangeItemToFilter:Entities.DTODateRangeItem,fieldType:EFilterContextNames)
      {
        saveRecentlyUsedDateRanges(dateRangeItemToFilter,fieldType);

      }



      $scope.openNewDatePartitionDialog = function()
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/createDatePartitionDialog.tpl.html',
            'createDatePartitionDialogCtrl',
            {},
            {size:'md'});

        dlg.result.then(function(partition:Entities.DTODateRangePartition){
          addNewPartition(partition);
        },function(){

        });
      }

      var addNewPartition = function (partition:Entities.DTODateRangePartition) {
        partition.name =  partition.name?( partition.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( partition.name &&  partition.name.trim() != '') {
          log.debug('add new partition: name: ' +  partition.name);
          partition.id=null;
          dateRangeResource.addNewDateRange( partition, function (addedpartition) {
            loadDatePartitions();
            $scope.reloadData([addedpartition.id]);
            notifyChange();
            }, function () {
            dialogs.error('Error','Failed to add partition.');
          });
        }
      }


      $scope.openEditDatePartitionDialog = function(partition:Entities.DTODateRangePartition)
      {
        var dlg = dialogs.create(
            'common/directives/reports/dialogs/createDatePartitionDialog.tpl.html',
            'createDatePartitionDialogCtrl',
            {partition:partition},
            {size:'md'});

        dlg.result.then(function(updatedPartition:Entities.DTODateRangePartition){
          if((<any>updatedPartition).delete)
          {
            $scope.openDeleteDatePartitionDialog(partition);
          }
          else {
            editPartition(updatedPartition);
          }
        },function(){

        });
      }

      var editPartition = function (partition:Entities.DTODateRangePartition) {
        partition.name =  partition.name?( partition.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( partition.name &&  partition.name.trim() != '') {
          log.debug('edit partition: name: ' +  partition.name);
          dateRangeResource.updateDateRange( partition, function (updatedDatePartition:Entities.DTODateRangePartition) {
            loadDatePartitions();
            $scope.refreshData([updatedDatePartition.id]);
          }, function () {
            dialogs.error('Error','Failed to edit partition.');
          });
        }
      }



      $scope.openDeleteDatePartitionDialog = function(partition:Entities.DTODateRangePartition) {
        var dlg = dialogs.confirm('Confirmation', "You are about to delete date partition named: '" + partition.name + "'. Continue?", null);
        dlg.result.then(function (btn) { //user confirmed deletion
              dateRangeResource.deleteDateRange(partition.id, function () {
                loadDatePartitions();
                if( $scope.selectedPartition &&  $scope.selectedPartition.id ==partition.id )
                {
                  $scope.selectPartition(deafultPartition);
                }
                else {
                  $scope.reloadData();
                }
              },onError);
            }

            , function (btn) {
              // $scope.confirmed = 'You confirmed "No."';
            });

      }

      var notifyChange = function(){
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridDateMenuChangeAction, {
          action: 'reportState',
        });
      }


      var registerEvents = function(){
        if($scope.isMasterMenu) {
          eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridDateMenuChangeAction, $scope, (new RegisteredEvent((registrarScope, registrationParam, fireParam) => {
            if (fireParam.action === 'selectDatePartition') {
              $scope.selectPartition(fireParam.item);
            }
          })));
        }
        else{
          eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridDateMenuChangeAction, $scope, (new RegisteredEvent((registrarScope, registrationParam, fireParam) => {
            if (fireParam.action === 'reportState') {
              loadDatePartitions();
            }
          })));
        }
      }
    })

    .directive('fileDatesMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/common/directives/reports/fileDatesMenu.tpl.html',
            controller: 'fileDatesMenuCtrl',

            replace:true,
            scope:{
              'selectedDateRangesItemToTag':'=',
              'selectedDateRangesItemsToTag':'=',
              'displayType':'=',
              'reloadData':'=',
              'refreshData':'=',
              'refreshNeeded':'=',
              'disabledMenu':'=',
              'onSelectPartition':'=',
            },
            link: function (scope:any, element, attrs) {
              scope.init(true);
            }
          };
        }
    )

    .directive('selectDatePartition',
        function () {
      return {
        template:'<span class="btn-group dropdown" >' +
        '<a class="btn btn-flat btn-xs dropdown-toggle ellipsis " style="max-width: 110px;padding-right: 20px;position: relative;margin-top: 0"  data-toggle="dropdown" title="Change displayed partition">' +
        '<span  >{{selectedPartition.name}}&nbsp;</span>' +
        '<span class="caret" style="position: absolute;right: 5px;  top:9px;;"></span></a>' +
        '<ul class="dropdown-menu" role="menu" aria-labelledby="split-button" style="max-height: 200px; overflow: auto">' +
        '<li  style="position: relative;" ng-repeat="partition in partitions">' +
        ` <a class="btn btn-link btn-sm"  ng-click="requestSelectChange(partition);">` +
        ' <span class="{{iconClass}}"></span>&nbsp;{{partition.name}}&nbsp;&nbsp; </a></li>' +
        '</ul></span>',
        controller: 'fileDatesMenuCtrl',

        replace:true,
        scope:{
          'onSelectPartition':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init();
        }
      };
    }
    )

    .directive('filterByDates',
      function () {
        return {
          template:
            '<li role="filter by user date range"><span ng-click="doDateRangeCustomFilter(filterMode);" style="cursor: pointer;margin-left: 12px;"><i class=" fa fa-calendar"></i>&nbsp;Custom date filter...</span>',
          controller: 'fileDatesMenuCtrl',

          replace:true,
          scope:{
            'filterMode':'='
          },
          link: function (scope:any, element, attrs) {
            scope.init();
          }
        };
    }
    );



///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.docTypes', [
      'directives.dialogs.docTypes',
    'resources.docTypes',
   ])
    .controller('docTypesMenuCtrl', function ($scope,$window,$element,$timeout,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,currentUserDetailsProvider:ICurrentUserDetailsProvider,
                                          userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,routerChangeService,dialogs,docTypesResource:IDocTypesResource,docTypeResource) {
      var log: ILog = Logger.getInstance('docTypesMenuCtrl');
      var isMasterManager;
      var MAX_RECENTLY_USED: number = parseInt(configuration.max_recently_used_docTypes);
      $scope.iconClass = configuration.icon_docType;

      $scope.init = function (isMaster) {
        isMasterManager = isMaster;
        if (isMasterManager) {
          registerCommunicationHandlers();
          initUserSettingsPreferences();
        }
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });

        $timeout(function () //load only after page has loaded
        {
          loadDocTypes();
        }, 25);
      };

      $scope.$watch(function () {
        return $scope.selectedTagTypeId;
      }, function (value) {
        $scope.selectedTagType = _.filter($scope.tagTypes, (tagType) => {
          return tagType.fileTagTypeDto.id === $scope.selectedTagTypeId;
        })[0];
      }, true);

      var setUserRoles = function () {
        $scope.isMngDocTypeConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDocTypeConfig]);
      }
      var loadDocTypes = function () {
        log.debug('DocTypes ---- Bar Loaded');
        docTypesResource.getDocTypes($scope.onGetDocTypesCompleted, onError);
      }

      var initUserSettingsActiveDocType = function () {
        //     setActiveSelectedDocTypeItem();
        setActiveRecentlyUsedDocTypes();

        //    $scope.$on(userSettings.activeSelectedDocTypeItemUpdatedEvent, function (sender, value) {
        //       setActiveSelectedDocTypeItem();

        //    });
        $scope.$on(userSettings.activeRecentlyUsedDocTypeItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsedDocTypes();

        });
      };
      $scope.toggleMinimizeToolbar = function () {
        $scope.minimizeToolbar = !$scope.minimizeToolbar;
        userSettings.setDocTypesPreferences({minimizeToolbar: $scope.minimizeToolbar});

        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      }
      var initUserSettingsPreferences = function () {

        userSettings.getDocTypesPreferences(function (preferences) {
          $scope.minimizeToolbar = preferences && typeof (<any>preferences).minimizeToolbar != 'undefined' ? (<any>preferences).minimizeToolbar : false;
        });

      };
      var setLastSelectedDocTypeItemId = function (lastSelectedId) {
        if (lastSelectedId) {
          userSettings.setCurrentDocType(lastSelectedId);
        }
      }

      $scope.getDocTypePath = function (docType: Entities.DTODocType) {
        if (docType && docType.parents) {
          var parentsNames = [];
          docType.parents.forEach(p => {
            var found = $scope.docTypeList.filter(d => (<Entities.DTODocType>d).id == p)[0];
            parentsNames.push(found.name);
          });
          var parentsPath = parentsNames.join('/');
          return parentsPath;
        }
        return '';
      }

      var setActiveRecentlyUsedDocTypes = function () {
        userSettings.getRecentlyUsedDocType(function (savedActiveRentlyUsedDocTypeIDs) {
            var approvedListOfRecntlyUsedItems = [];
            if ($scope.docTypeList && $scope.docTypeList.length > 0 && savedActiveRentlyUsedDocTypeIDs && savedActiveRentlyUsedDocTypeIDs.length > 0) {
              savedActiveRentlyUsedDocTypeIDs.forEach(s => {
                var foundItem = $scope.docTypeList.filter(d => d.id == s)[0];
                if (foundItem) {
                  if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                    approvedListOfRecntlyUsedItems.push(foundItem.id);
                  }
                }
              });
            }
            $scope.savedRecentlyUsedDocTypeItems = _.filter($scope.docTypeList, (docType) => {
              return _.includes(approvedListOfRecntlyUsedItems, docType.id);
            });

            if (approvedListOfRecntlyUsedItems.length > 0 && approvedListOfRecntlyUsedItems.length !== savedActiveRentlyUsedDocTypeIDs.length) {
              userSettings.setRecentlyUsedDocType(approvedListOfRecntlyUsedItems);
            }
          }
        );
      }

      var saveRecentlyUsedDocType = function (docTypeToSave) {
        if (docTypeToSave) {
          userSettings.getRecentlyUsedDocType(function (savedActiveRecntlyUsedDocTypeIDs) {
            if (savedActiveRecntlyUsedDocTypeIDs) { //add current active to recenlty used
              var foundInRecentItems = savedActiveRecntlyUsedDocTypeIDs.filter(d => d == docTypeToSave.id)[0];
              if (foundInRecentItems) { //if already exists remove it
                var foundIndex = savedActiveRecntlyUsedDocTypeIDs.indexOf(foundInRecentItems);
                savedActiveRecntlyUsedDocTypeIDs.splice(foundIndex, 1);
              }
              savedActiveRecntlyUsedDocTypeIDs.unshift(docTypeToSave.id);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
              if (savedActiveRecntlyUsedDocTypeIDs.length > MAX_RECENTLY_USED) {
                savedActiveRecntlyUsedDocTypeIDs.splice(MAX_RECENTLY_USED, savedActiveRecntlyUsedDocTypeIDs.length - MAX_RECENTLY_USED); //limit size of list
              }


            }
            else {
              savedActiveRecntlyUsedDocTypeIDs = [docTypeToSave.id];
            }
            userSettings.setRecentlyUsedDocType(savedActiveRecntlyUsedDocTypeIDs);
          });


        }
      }

      $scope.getDocTypeStyleId = function (docType) {
        if (docType) {
          return splitViewBuilderHelper.getDocTypeStyleId(docType);
        }
        return null;

      }
      $scope.getDocTypeDepth = function (docType) {
        if (docType) {
          return splitViewBuilderHelper.getDocTypeDepth(docType);
        }
        return null;
      }

      $scope.onGetDocTypesCompleted = function (docTypes: Entities.DTODocType[]) {
        $scope.docTypeList = docTypes;
        if (isMasterManager) {
          initUserSettingsActiveDocType();
        }
      };
      $scope.$watch(function () {
        return $scope.selectedDocTypesItemsToTag;
      }, function (value) {
        $scope.isAssignEnabled = !$scope.disabled && areSelectedItemsAuthorized();
      });
      $scope.$watch(function () {
        return $scope.selectedDocTypesItems;
      }, function (value) {
        //rebing grid
        if ($scope.selectedDocTypesItems && $scope.selectedDocTypesItems.length > 1) {
          $scope.moveMode = false;
        }
        ;
      });
      $scope.$watch(function () {
        return $scope.selectedDocTypesItem;
      }, function (value) {
        // rebind grid
        if ($scope.moveMode) {
          moveModeDestinationSelected();
        }
      });

      $scope.openNewDocTypeDialog = function () {
        $scope.moveMode = false;
        var parentDocType: Entities.DTODocType = $scope.selectedDocTypesItem;
        var dlg = dialogs.create(
          'common/directives/reports/dialogs/newDocTypeDialog.tpl.html',
          'newDocTypeDialogCtrl',
          {parentDocType: parentDocType},
          {size: 'md'});

        dlg.result.then(function (newDocType: Entities.DTODocType) {
          addNew(newDocType);
        }, function () {

        });
      }

      $scope.updateMoveMode = function() {
        $scope.moveMode=!$scope.moveMode;
      }

      $scope.openEditDocTypeDialog = function () {
        $scope.moveMode = false;
        var docTypeItem: Entities.DTODocType = $scope.selectedDocTypesItem;
        var dlg = dialogs.create(
          'common/directives/reports/dialogs/newDocTypeDialog.tpl.html',
          'newDocTypeDialogCtrl',
          {docTypeItem: docTypeItem},
          {size: 'md'});

        dlg.result.then(function (docTypeItem: Entities.DTODocType) {
          editTag(docTypeItem);
        }, function () {

        });
      }

      $scope.openMoveDocTypeDialog = function () {
        var docTypeItem: Entities.DTODocType = $scope.selectedDocTypesItem;
        //var dlg:any =  dialogs.confirm('Move DocType', 'Please choose new destination for DocType '+docTypeItem.name, null);
        //dlg.result.then(function(){
        $scope.docTypeToMove = docTypeItem;
        $scope.moveMode = true;
        // },function(){
        //
        //});
      }

      $scope.moveDocTypeAsFirstLevel = function () {
        var dlg: any = dialogs.confirm('Move operation', 'You have selected to move DocType ' + $scope.docTypeToMove.name + ' as top level DocType. Continue?', null);
        dlg.result.then(function () {
          moveTag($scope.docTypeToMove);
          $scope.moveMode = false;
        }, function () {

        });
      }

      var moveModeDestinationSelected = function () {
        var docTypeItem: Entities.DTODocType = $scope.selectedDocTypesItem;

        var dlg: any = dialogs.confirm('Move operation', 'You have selected to move <br\>DocType ' + $scope.docTypeToMove.name + ' to be under <br\>DocType ' + docTypeItem.name + '. <br\><br\>Continue?', null);
        dlg.result.then(function () {
          moveTag($scope.docTypeToMove, docTypeItem);
          $scope.moveMode = false;
        }, function () {

        });
      }

      $scope.openDeleteDocTypeDialog = function () {
        $scope.moveMode = false;
        var docTypeItems: Entities.DTODocType[] = $scope.selectedDocTypesItems;
        if (docTypeItems && docTypeItems.length > 0) {
          docTypesResource.getAssignedDocTypes(null, function (assignedDocTypes: Entities.DTOAggregationCountItem<Entities.DTODocType>[], totalAggregatedCount?: number) {
            var loopPromises = [];
            docTypeItems.forEach(docTypeItem => {
              var hasChildren = (<any>docTypeItem).hasChildren;
              var assignedItemsForDocType = assignedDocTypes.filter(a => a.item.id == docTypeItem.id)[0];
              var allFiles = assignedItemsForDocType.count2;

              //var dlg = dialogs.confirm('Confirmation', "You are about to delete " + docTypeItem.name + (hasChildren ? ' and its children' : '') + " which have " + allFiles + " associated files . Continue?", null);
              var dlg = dialogs.confirm('Confirmation', "You are about to delete DocType named: \'" + docTypeItem.name + (hasChildren ? '\' and its children' : '\'')+". Continue?", null);
              dlg.result.then(function (btn) { //user confirmed deletion
                if (allFiles > 0) {
                  dialogs.info('Note', 'DocType is associated to file/s and cannot be deleted.');
                }
                else {
                  var deferred = $q.defer();
                  loopPromises.push(deferred.promise);
                  log.debug('about to delete saved doc type: ' + docTypeItem.name + ' with ' + allFiles + ' assosiated files');
                  docTypesResource.deleteDocType(docTypeItem.id, function () {
                    deferred.resolve(docTypeItem.parentId);

                  }, function (err) {
                    deferred.resolve();
                    //dialogs.error('Error', 'Failed to delete DocType. '+docTypeItem.name);
                    dialogs.error('Error', err.data.message);
                  });
                  $q.all(loopPromises).then(function (selectedParentIdsToUpdate) {
                    $scope.reloadData([selectedParentIdsToUpdate[0]]);
                    reportStateChanged();
                  });
                }
              }, function (btn) {
                // $scope.confirmed = 'You confirmed "No."';
              });
            }
          );}, $scope.onGetRequestError);
        }
      };

      $scope.toggleTagTypeColumn = function (event, tag) {
        if (!_.isEmpty($scope.tagsChanges)) {
          let dlg: any = dialogs.confirm('Save changes', `The changes done on Tag type '${$scope.selectedTagType.fileTagTypeDto.name}' have not been saved. Save Changes?`, null);
          dlg.result.then(() => { //user confirmed save
              $scope.saveTagsChanges(() => {
                doTagToggling(tag);
              })
          },
          () => {
            $scope.resetTagsChanges();
            doTagToggling(tag);
          });
        } else {
          _.defer(()=> {
            doTagToggling(tag);
          });
        }
      };

      var doTagToggling = function(tag) {
        $scope.selectedTagTypeId = tag.fileTagTypeDto.id;
        userSettings.setDocTypesTagColumns(tag.fileTagTypeDto.id);
      };

      var addNew = function (data) {

       var newDocType = data.tag;

        if ( newDocType.name &&  newDocType.name.trim() != '') {
          log.debug('add new DocType to parentId: ' +  newDocType.parentId + ' name: ' +  newDocType.name);
          docTypesResource.addNewDocType( newDocType, function (addedDocType) {
            let selectedItem = !data.createAnother ? [addedDocType.id] : null;
            $scope.reloadData(selectedItem);
            reportStateChanged();
          }, function () {
            dialogs.error('Error','Failed to add DocType.');
          });
        }
        if (data.createAnother) {
          $timeout(function () {
            $scope.openNewDocTypeDialog(newDocType);
          });
        }
      }

      var reportStateChanged = function()
      {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDocTypesManageMenuUserAction, {
          action: 'refreshState',
        });
      }
      var editTag = function (data) {

        var docTypeToUpdate = data.tag;

        docTypeToUpdate.name =  docTypeToUpdate.name?( docTypeToUpdate.name.replace(new RegExp("[;,:\"']", "gm"), " ")):null;
        if ( docTypeToUpdate.name &&  docTypeToUpdate.name.trim() != '') {
          log.debug('add new DocType to parentId: ' +  docTypeToUpdate.parentId + ' name: ' +  docTypeToUpdate.name);
          docTypesResource.updateDocType( docTypeToUpdate, function () {
            $scope.reloadData();
            reportStateChanged();

          }, function () {
            dialogs.error('Error','Failed to edit doc type.');
          });
        }
      }
      var moveTag = function (sourceDocType:Entities.DTODocType,destinationDocType?:Entities.DTODocType) {
          log.debug('moving DocType ' +  sourceDocType.name + ' to be under: ' + (destinationDocType? destinationDocType.name:'none'));
          docTypesResource.moveDocType( sourceDocType,destinationDocType, function () {
            $scope.reloadData();
            reportStateChanged();
          }, function () {
            dialogs.error('Error','Failed to move doc type.');
          });
      }

     $scope.toggleDocTypeToSelectedItems = function (docType:Entities.DTODocType) {
        var docTypeToToggle:Entities.DTODocType = docType == null?$scope.activeSelectedDocTypeItem:docType;
        if (docTypeToToggle && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
          var firstSelectedItem = getEntity($scope.selectedDocTypesItemsToTag[0]);
          if (isExplicitDocTypeExists(firstSelectedItem.docTypeDtos, docTypeToToggle)) {
            $scope.removeDocTypeFromSelectedItems(docType);
          }
          else {
            $scope.addDocTypeToSelectedItems(docType);
          }
        }
      }

      var showWarning = function(callback) {
        let warningRequired = _.some($scope.selectedDocTypesItemsToTag, (item) => {
          return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
        });
        if(warningRequired) {
          userSettings.getDontShowAssociationWarning((dontShow) => {
            if (!dontShow) {
              var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
              dlg.result.then((dontShow) => {
                userSettings.setDontShowAssociationWarning(dontShow);
                callback();
              });
            }else {
              callback();
            }
          });
        }else {
          callback();
        }
      };

      $scope.addDocTypeToSelectedItems = function (docTypeToAdd:Entities.DTODocType) {
        if (docTypeToAdd && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
          showWarning (() => {
            addDocTypeToItems($scope.selectedDocTypesItemsToTag, $scope.selectedDocTypesItemsToTag.map(s => getEntity(s)), docTypeToAdd);
          });
        }
      }

      // const resolveDocTypeResponse = function() {
      //   if ($scope.refreshNeeded) {
      //     $scope.refreshData();
      //   }
      //   else {
      //     $scope.reloadData(); //no need when auto sync is false
      //   }
      // }

      $scope.removeDocTypeFromSelectedItems= function (docTypeToRemove:Entities.DTODocType) {

        if (docTypeToRemove && $scope.selectedDocTypesItemsToTag && $scope.selectedDocTypesItemsToTag.length > 0) {
          showWarning (() => {
            removeDocTypeFromItems($scope.selectedDocTypesItemsToTag, $scope.selectedDocTypesItemsToTag.map(s => getEntity(s)), docTypeToRemove);
          });
        }
      }

      var areSelectedItemsAuthorized = function():boolean
      {
        if($scope.selectedDocTypesItemsToTag &&$scope.selectedDocTypesItemsToTag.length>0) {
          return getAuthorizedItems($scope.selectedDocTypesItemsToTag.map(i => getEntity(i)))[0]?true:false;
        }
        return false;
      }
      var addDocTypeToItems = function(originalItems, items:Entities.IDocTypesAuthorization[], docTypeToAdd:Entities.DTODocType) {
      if (docTypeToAdd && items && items.length > 0) {
        var theDocTypeResource: IDocTypeResource = docTypeResource($scope.displayType);
        var loopPromises = [];
        var approvedItems = getAuthorizedItems(items);
        if (approvedItems && approvedItems.length > 0) {
          approvedItems.forEach(function (selectedItem:Entities.IDocTypesAuthorization) {
            var performAddPromise = isSingleDocTypeReplacement(docTypeToAdd, selectedItem);
            performAddPromise.then(function (replaced: boolean) {
              var addDocType = theDocTypeResource.addDocType(docTypeToAdd.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              addDocType.$promise.then(function (updatedItem: any) {
                 updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  if (error.status == 400 &&  error.data && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                    dialogs.error('Associate docType ', 'Cannot associate docType to group '+selectedItem['groupName']+' right now.</BR>There is already running operation for this group.</BR>Try again later.');
                  }
                  else{
                    onError();
                  }
                  log.error('receive addDocType failure' + error);
              });
              $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                if ($scope.refreshNeeded) {
                  $scope.refreshData();
                }
                else {
                  $scope.reloadData(); //no need when auto sync is false
                }
                routerChangeService.updateAsyncUserOperations();
              });
            });

          });

        }
      }
    }

    var isSingleDocTypeReplacement = function(docTypeToAdd:Entities.DTODocType,selectedItem:Entities.IDocTypesAuthorization):any {
      var deferred = $q.defer();
        if( docTypeToAdd.singleValue && selectedItem.docTypeDtos ) {
          var docTypeToReplace = selectedItem.docTypeDtos.filter(d => !d.implicit)[0];
          if(docTypeToReplace){
            var dlg:any =  dialogs.confirm('Replace DocType', 'You are about to replace existing DocType \''+docTypeToReplace.name+'\' with \''+docTypeToAdd.name+ '\'.<br><br></br>Continue?', null);
            dlg.result.then(function(){
              deferred.resolve(true);
             },function(){
              deferred.reject();
            });
          }
          else{
            deferred.resolve(false);
          }
        }
        else{
          deferred.resolve(false);
        }
        return deferred.promise;
    }

      var removeDocTypeFromItems= function (originalItems, items:Entities.IDocTypesAuthorization[], docTypeToRemove:Entities.DTODocType) {
        if (docTypeToRemove && items && items.length > 0) {
          var approvedItems =getApprovedItemsForRemoval(items, docTypeToRemove);
          var loopPromises = [];
          if (approvedItems && approvedItems.length > 0) {
            var theDocTypeResource:IDocTypeResource = docTypeResource($scope.displayType);
            approvedItems.forEach(function (selectedItem) {
              var removeTag = theDocTypeResource.removeDocType(docTypeToRemove.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              removeTag.$promise.then(function (updatedItem:any) {
                  log.debug('receive removeDocType from selected items success for' + selectedItem['id']);
                  updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  if (error.status == 400 &&  error.data && error.data.type == ERequestErrorCode.GROUP_LOCKED) {
                    dialogs.error('Unassociate docType ', 'Cannot unassociate docType from group \''+selectedItem['groupName']+'\' right now.<br>There is already running operation for this group.<br>Try again later.');
                  }
                  else{
                    onError();
                  }
                  log.error('receive removeDocType from selected items failure' + error);
                });
            })
            $q.all(loopPromises).then(function (selectedItemsToUpdate) {
              if ($scope.refreshNeeded) {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
              routerChangeService.updateAsyncUserOperations();
            });
          }
        }
      }

      var getApprovedItemsForRemoval = function(items: Entities.IDocTypesAuthorization[], docTypeToRemove: Entities.DTODocType) {
        var approvedItems = items.filter(i => {
          var docTypeDtos =i.docTypeDtos ;
          var theDocType = docTypeDtos ? docTypeDtos.filter(a => a.id == docTypeToRemove.id && !a.implicit)[0] : null;
          return theDocType != null ;
        });
        return approvedItems.filter(s=>s.deleteDocTypesAuthorized)
      }
      var getAuthorizedItems = function(items: Entities.IDocTypesAuthorization[]) {
        var approvedItems = items.filter(s => {
          return s.assignDocTypesAuthorized;
        });
        return approvedItems;
      }
      var getEntity = function(item): Entities.IDocTypesAuthorization
      {
        return item.item ? item.item : item; //kendo grid
      }

      $scope.doDocTypeFilter = function( docType:Entities.DTODocType,isSubtreeFilter:boolean)
      {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDocTypeMenuUserAction, {
          action: 'doDocTypeFilter',
          values:  [docType],
          isSubtreeFilter:isSubtreeFilter
        });
      }

      var registerCommunicationHandlers=function()
      {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridDocTypesUserAction,
            $scope,
            (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

              if (fireParam.action === 'removeDocType') {
                var item=<Entities.IDocTypesAuthorization>fireParam.item;
                var docTypeToRemove=fireParam.docTypeToRemove;

                if($scope.displayType == fireParam.displayType) {
                  removeDocTypeFromItems([item],[getEntity(item)], docTypeToRemove);
                }
              }
              else if (fireParam.action === 'setDocType') {
                var entity=<Entities.IDocTypesAuthorization>getEntity(fireParam.item);
                $scope.openSelectDocTypesDialog(false,entity.docTypeDtos && entity.docTypeDtos[0]?entity.docTypeDtos[0].id:null,true);
              }

            })));
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportDocTypesManageMenuUserAction,
            $scope,
            (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

              if (fireParam.action === 'refreshState') {
                  loadDocTypes();
              }

            })));
      }

      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      }


      var updateSelectedItem=function(originalItem, updatedItem:Entities.IDocTypesAssignee)
      {
        var originalEntity =   getEntity(originalItem);
        originalEntity.docTypeDtos =  updatedItem.docTypeDtos;
        (<Entities.ITaggable><any>originalEntity).fileTagDtos =   (<Entities.ITaggable><any>updatedItem).fileTagDtos;
        splitViewBuilderHelper.parseTags(<Entities.ITaggable><any>originalEntity);
        splitViewBuilderHelper.parseDocTypes( originalEntity,configuration);
        if ((<any>originalItem).item) {
          if( originalItem.loaded) { //for folder tree view refresh
            originalItem.item = undefined; // force refresh of dataItem
            originalItem.set("item", originalEntity);
            originalItem.loaded(false);
      //      originalItem.load();   //this line is loading the child folders and if there are many, i.e11 is stuck. It loads them even if not exists yet
          }
          else {
            //    originalItem.item.docTypeDtos =originalItem;
          }
        }
        else {
          // (<any>originalItem).docTypeDtos= updatedItem.docTypeDtos;
          // splitViewBuilderHelper.parseTags( originalItem);
          // splitViewBuilderHelper.parseDocTypes( originalItem,configuration);
        }
      }

      var isExplicitDocTypeExists = function (tags:Entities.DTODocType[], selectedTag:Entities.DTODocType) {
        if(tags&& tags[0]) {
          return tags.filter(t=>(t.id == selectedTag.id && (<any>t).isExplicit))[0] != null;
        }
        return false;
      };

      $scope.doUnAssociationFilterBy = function() {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportDocTypeMenuUserAction, {
          action: 'doDocTypeUnassociated',
          isSubtreeFilter: true
        });
      };


      $scope.doDocTypeFilterBy = function()
      {
        userSettings.getCurrentDocType(function(savedActiveDocTypeID){
          var dlg = dialogs.create(
              'common/directives/reports/dialogs/manageDocTypesDialog.tpl.html',
              'manageDocTypesDialogCtrl',
              {editMode:false,changeSelectedItemId:savedActiveDocTypeID,filterMode:true},
              {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});

          dlg.result.then(function(docTypeToFilter){
            $scope.doDocTypeFilter(docTypeToFilter,true);
            saveRecentlyUsedDocType(docTypeToFilter);
            setLastSelectedDocTypeItemId(docTypeToFilter?docTypeToFilter.id:null);
          },function(docTypeToFilter){
            setLastSelectedDocTypeItemId(docTypeToFilter?docTypeToFilter.id:null);
          });

        });

      }



      $scope.openSelectDocTypesDialog = function(editMode:boolean,disableToggle:boolean)
      {
        var firstSelectedItem = getEntity($scope.selectedDocTypesItemsToTag[0]);
        const activeDocTypeID = firstSelectedItem.docTypeDtos && firstSelectedItem.docTypeDtos[0]?firstSelectedItem.docTypeDtos[0].id:null;
        if(activeDocTypeID)
        {
          openAssociateDocTypeDialog(editMode, activeDocTypeID,disableToggle);
        }
        else{
          userSettings.getCurrentDocType(function(savedActiveDocTypeID) {
            openAssociateDocTypeDialog(editMode, savedActiveDocTypeID,disableToggle);
          });
        }

        function openAssociateDocTypeDialog(editMode: boolean,activeDocTypeID:number,disableToggle:boolean) {
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/manageDocTypesDialog.tpl.html',
            'manageDocTypesDialogCtrl',
            {editMode: editMode, changeSelectedItemId: activeDocTypeID},
            {
              size: 'lg',
              keyboard: true,
              backdrop: false,
              windowClass: 'center-modal',
              animation: false,
              backdropClass: 'halfHeight'
            });

          dlg.result.then(function (selectedDocType) {
            setLastSelectedDocTypeItemId(selectedDocType ? selectedDocType.id : null);
            if(disableToggle)
            {
              $scope.addDocTypeToSelectedItems(selectedDocType);
            }
            else {
              $scope.toggleDocTypeToSelectedItems(selectedDocType);
            }
            saveRecentlyUsedDocType(selectedDocType);
          }, function (selectedDocType) {
         //   setLastSelectedDocTypeItemId(selectedDocType ? selectedDocType.id : null);

          });
        }
      }

      $scope.toggleFlags = function(){
        $scope.showFlags = !$scope.showFlags;
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

    })
    .directive('docTypesManageMenu',
        function () {
          return {
            // restrict: 'E',
            template:
            '<span class="left-actions">' +
              '<span ng-if="isMngDoctypeConfigUser">'+
                '<button class="btn btn-flat btn-xs" ng-click="openNewDocTypeDialog()">' +
                  '<i class="fa fa-plus"></i>' +
                '&nbsp;Add</button>' +
                '<a class="btn btn-link btn-sm"  ' +
                'ng-class="{disabled:((!selectedDocTypesItems)||selectedDocTypesItems.length==0)}" ' +
                'ng-click="openEditDocTypeDialog()"><da-icon href="edit"></da-icon>&nbsp;Edit</a>' +
                '<a class="btn btn-link btn-sm" ' +
                  'ng-class="{disabled:((!selectedDocTypesItems)||selectedDocTypesItems.length==0)}" ' +
                  'ng-click="openDeleteDocTypeDialog()">' +
                  '<i class="fa fa-trash-o"></i>&nbsp;Delete</a>' +
                '<a class="btn btn-link btn-sm " ' +
                  'ng-class="{disabled:((!selectedDocTypesItems)||selectedDocTypesItems.length==0||selectedDocTypesItems.length>1),active:moveMode}" ' +
                  'ng-show="!moveMode"  ng-click="openMoveDocTypeDialog()">Move</a>' +
                '<span ng-show="moveMode" style="margin-top: 5px;margin-right: 30px;" class="label label-info-bar">' +
                  '<span>Move: Please choose new parent or click</span>' +
                  '<a class1="btn btn-xs btn-flat" title="Set as top level" style="margin: 0 5px;" ' +
                    'ng-click="moveDocTypeAsFirstLevel()">here</a>' +
                  '<span>to set as top level DocType </span>' +
                  '<a class1="btn btn-xs btn-flat" title="Cancel move"  ng-click="updateMoveMode()" ' +
                    'style="margin-left: 10px;"><i class="fa fa-times"></i></a>' +
                '</span>' +
              '</span>' +
               `<span ng-if="tagTypes && !moveMode" class="doc-type-tags-main-btn phases btn-group dropdown" >
                  <a class="btn btn-link btn-sm" style="margin-right:10px" ng-click="toggleFlags()">
                     <i ng-if="showFlags" style="padding-right: 2px;" class="fa fa-check-square-o"></i>
                     <i ng-if="!showFlags" style="padding-right: 2px;" class="fa fa fa-square-o"></i>
                     <span>Show</span> 
                     <span class="tag"> <span class="fa fa-flag"></span></span>
                  </a>             
                  <button data-toggle="dropdown"  class="btn btn-flat btn-xs dropdown-toggle" style="margin-left: 7px;float: none;margin-top: 2px;border-left:0;" bubble="Select tag types columns">
                      <div style="display:flex; flex-direction: row; align-items: center;">
                         <div style="max-width:150px;padding-right: 5px" >
                         <span ng-if="!selectedTagType" style="padding-right:10px">
                            Select tag type                  
                         </span>  
                         <span ng-if="selectedTagType" style="padding-right:10px">
                             <i style="width: 10px; padding-right: 5px; display: inline-block;">
                               <span class="tag tag-{{selectedTagType.styleId}}">
                                 <span class="fa fa-flag"></span>
                               </span>
                             </i> 
                             <span>{{selectedTagType.fileTagTypeDto.name}}</span>
                         </span>    
                         <i class="caret"></i>
                     </div>
                  </button>
                  <ul class="doc-type-tags-main-menu dropdown-menu rtl-dir" style="margin-left: 30px;">
                     <li ng-repeat="tagType in tagTypes"  class="btn btn-link btn-xs" >            
                        <a ng-click="toggleTagTypeColumn($event, tagType)" class="btn btn-link btn-xs">
                           <span style="min-width:20px"><span ng-if="tagType.fileTagTypeDto.id === selectedTagTypeId" class="fa fa-check"></span></span>
                           <i class="tag-wrapper">
                             <span class="tag tag-{{tagType.styleId}}">
                               <span class="fa fa-flag"></span>
                             </span>
                           </i> 
                           <span style="padding-left:5px" class="ng-binding">{{tagType.fileTagTypeDto.name}}</span>
                        </a>         
                     </li>
                  </ul>                  
                  <a ng-show="tagsChanges && tagsChanges.length > 0" class="btn btn-primary btn-xs pull-right ng-scope" ng-click="saveTagsChanges()" style="margin-left: 10px;border-radius: 3px">Save</a>
                </span> `+
              '</span>',
            controller: 'docTypesMenuCtrl',

            replace:true,
            scope:{
              'selectedDocTypesItem':'=',
              'selectedDocTypesItems':'=',
              'saveTagsChanges': '=',
              'resetTagsChanges': '=',
              'displayType':'=',
              'reloadData':'=',
              'disabled':'=',
              'tagTypes':'=',
              'selectedTagTypeId': '=',
              'tagsChanges': '=',
              'isMngDoctypeConfigUser': '=',
              'showFlags': '=',
              'moveMode': '=',
            },
            link: function (scope:any, element, attrs) {
              scope.init(false);
            }
          };
        }
    )
    .directive('docTypesMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/docTypesMenu.tpl.html',
        controller: 'docTypesMenuCtrl',

        replace:true,
        scope:{
          'selectedDocTypesItemToTag':'=',
          'selectedDocTypesItemsToTag':'=',
          'displayType':'=',
          'reloadData':'=',
          'refreshData':'=',
          'refreshNeeded':'=',
          'disabled':'=',
          'openManagePage':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
);



///<reference path='../../all.d.ts'/>
'use strict';


angular.module('directives.markedItems', [
    ])
    .controller('markedItemsMenuCtrl', function ($scope,$element,$timeout,$window,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,groupsResource:IGroupsResource,
                                              userSettings:IUserSettingsService,groupResource:IGroupResource,eventCommunicator:IEventCommunicator,dialogs,fileResource:IFileResource) {
      var log:ILog = Logger.getInstance('markedItemsMenuCtrl');

      var MAX_RECENTLY_USED:number = parseInt(configuration.max_recently_used_markedItems);
      $scope.iconClass=configuration.icon_markedItem;

      $scope.init = function () {

          registerCommunicationHandlers();
          initUserSettingsPreferences();


        $timeout(function() //load only after page has loaded
        {
          initUserSettingsActiveMarkedItem();
        },45);
      };

      $scope.getIconName = function(entity:EntityDisplayTypes) {
        if (EntityDisplayTypes.groups.equals(entity))
          return configuration.icon_group;

      }
      var initUserSettingsActiveMarkedItem = function () {

        setActiveRecentlyUsedMarkedItems();

        $scope.$on(userSettings.activeRecentlyUsedMarkedItemUpdatedEvent, function (sender, values:DTOMarkedItem[]) {
          setActiveRecentlyUsedMarkedItems();
          if($scope.markItemIsDisplayed()) {
            $scope.refreshScreenData();
          }

        });
      };

      $scope.refreshScreenData = function() //in order to refresh file count after dettache/attach to group
      {
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMarkedItemMenuUserAction, {
          action: 'refreshSplitView',

        });
      };

      var setMinimizeToolbar=function(minimized:boolean)
      {
        $scope.minimizeToolbar=minimized;
        userSettings.setMarkedItemsPreferences({minimizeToolbar:$scope.minimizeToolbar});
      }
      $scope.toggleMinimizeToolbar=function()
      {
        setMinimizeToolbar(!$scope.minimizeToolbar);
        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      }


      var initUserSettingsPreferences = function () {

        userSettings.getMarkedItemsPreferences(function(preferences){
          $scope.minimizeToolbar=preferences&& typeof (<any>preferences).minimizeToolbar!='undefined'?(<any>preferences).minimizeToolbar:false;
        });

      };


      var setActiveRecentlyUsedMarkedItems = function() {
        userSettings.getRecentlyUsedMarkedItems(function (savedActiveRentlyUsedMarkedItems:DTOMarkedItem[]) {
              var approvedListOfRecntlyUsedItems = [];
               var loopPromises = [];
              if ( savedActiveRentlyUsedMarkedItems && savedActiveRentlyUsedMarkedItems.length > 0) {
               var markedItems = savedActiveRentlyUsedMarkedItems.filter(s=> EntityDisplayTypes.groups.equals(s.entity));
                if(markedItems[0])
                {
                  var markedItemIds =  markedItems.map(r=>r.id);

                    var deferred = $q.defer();
                    loopPromises.push(deferred.promise);
                    groupsResource.getGroups(markedItemIds, function (groups:Entities.DTOGroup[]) {
                      if (groups&&groups.length>0) {
                        markedItems.forEach(m=> {
                          if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                            var theGroup = groups.filter(g=>g.id == m.id)[0];
                            if(!_.isEmpty(theGroup) && theGroup.numOfFiles > 0 && _.isNull(theGroup.parentGroupId)) {
                              (<any>m).name = getEntityName(theGroup);
                              approvedListOfRecntlyUsedItems.push(m);
                            }
                          }
                        });
                        if(approvedListOfRecntlyUsedItems.length > 0 && approvedListOfRecntlyUsedItems.length !== savedActiveRentlyUsedMarkedItems.length) {
                          userSettings.setRecentlyUsedMarkedItems(approvedListOfRecntlyUsedItems); //todo: support entities that are not group
                        }
                      }
                      deferred.resolve();
                    },function(e){
                      deferred.resolve();
                      log.error('Failed to load marked groups ' + e);
                    });


                }

              }
              $q.all(loopPromises).then(function () {
                    $scope.savedRecentlyUsedMarkedItemItems = approvedListOfRecntlyUsedItems;
              });

            }
        );

      }


      var updateRecentlyUsedMarkedItemName = function(markedItemToUpdate:DTOMarkedItem)
      {
        if(markedItemToUpdate) {
          userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[])
          {
            if (savedActiveRecentlyUsedMarkedItems) { //add current active to recenlty used
              var foundInRecentItems = savedActiveRecentlyUsedMarkedItems.filter(d=>d.id == markedItemToUpdate.id && d.entity == markedItemToUpdate.entity)[0];
              if (foundInRecentItems) { //if already exists remove it
                foundInRecentItems.name = markedItemToUpdate.name;
              }
            }
          });
        }
      }

      var saveRecentlyUsedMarkedItems = function(markedItemsToSave:DTOMarkedItem[],removeItems?:DTOMarkedItem[])
      {
        if(markedItemsToSave) {
          userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[])
          {
            if (savedActiveRecentlyUsedMarkedItems) { //add current active to recenlty used
              removeItems = removeItems?removeItems:[];
              markedItemsToSave.forEach((markedItemToSave)=> {
                removeItems.push(markedItemToSave);
              })
              var foundInRecentItems = savedActiveRecentlyUsedMarkedItems.filter(d=> removeItems.filter(m=>d.id ==m.id && d.entity.value == m.entity.value)[0]!=null );
              if (foundInRecentItems[0]) { //if already exists remove it
                foundInRecentItems.forEach(m=> {
                  var foundIndex = savedActiveRecentlyUsedMarkedItems.indexOf(m);
                  savedActiveRecentlyUsedMarkedItems.splice(foundIndex, 1);
                });
              }

              markedItemsToSave.forEach((markedItemToSave)=> {
                savedActiveRecentlyUsedMarkedItems.unshift(markedItemToSave);  //Use unshift, which modifies the existing array by adding the arguments to the beginning:
              })
              if (savedActiveRecentlyUsedMarkedItems.length > MAX_RECENTLY_USED) {
                savedActiveRecentlyUsedMarkedItems.splice(MAX_RECENTLY_USED, savedActiveRecentlyUsedMarkedItems.length - MAX_RECENTLY_USED); //limit size of list
              }
            }
            else {
              savedActiveRecentlyUsedMarkedItems=markedItemsToSave;
            }
            userSettings.setRecentlyUsedMarkedItems(savedActiveRecentlyUsedMarkedItems);
            setMinimizeToolbar(false);
          });
        }
      }

      $scope.removeRecentlyUsedMarkedItem = function(markedItemToRemove:DTOMarkedItem)
      {
        if(markedItemToRemove) {
          userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[]) {
            if (savedActiveRecentlyUsedMarkedItems && savedActiveRecentlyUsedMarkedItems.length > 0) {
                var foundInRecentItems = savedActiveRecentlyUsedMarkedItems.filter(d=>d.id == markedItemToRemove.id && d.entity.value == markedItemToRemove.entity.value)[0];
                if (foundInRecentItems) { //if already exists remove it
                  var foundIndex = savedActiveRecentlyUsedMarkedItems.indexOf(foundInRecentItems);
                  savedActiveRecentlyUsedMarkedItems.splice(foundIndex, 1);
                  userSettings.setRecentlyUsedMarkedItems(savedActiveRecentlyUsedMarkedItems);
                }
            }
          });
        }
      }

      $scope.removeRecentlyUsedMarkedItems = function(markedItemsToRemove:DTOMarkedItem[])
      {
        if(markedItemsToRemove && markedItemsToRemove.length>0) {
          userSettings.getRecentlyUsedMarkedItems(function(savedActiveRecentlyUsedMarkedItems:DTOMarkedItem[]) {
            if (savedActiveRecentlyUsedMarkedItems && savedActiveRecentlyUsedMarkedItems.length > 0) {
                   var foundInRecentItems = savedActiveRecentlyUsedMarkedItems.filter(d=> markedItemsToRemove.filter(m=>d.id ==m.id && d.entity.value == m.entity.value)[0]!=null );
                if (foundInRecentItems[0]) { //if already exists remove it
                  foundInRecentItems.forEach(m=> {
                    var foundIndex = savedActiveRecentlyUsedMarkedItems.indexOf(m);
                    savedActiveRecentlyUsedMarkedItems.splice(foundIndex, 1);
                  });
                  userSettings.setRecentlyUsedMarkedItems(savedActiveRecentlyUsedMarkedItems);

                }
            }
          });
        }
      }

      var onError = function()
      {
        dialogs.error('Error','Sorry, an error occurred.');
      }

      $scope.doMarkedItemFilter = function( markedItem:DTOMarkedItem)
      {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportGridFilterUserAction, {
          action: 'doFilterByID',
          id: markedItem.id,
          entityDisplayType:markedItem.entity,
          filterName:markedItem.name
        });
      }


      $scope.locateRecentlyUsedMarkedItem = function( markedItem:DTOMarkedItem)
      {
        if(EntityDisplayTypes.files.equals(markedItem.entity))
        {
          $scope.openNewTabFn(EntityDisplayTypes.folders,markedItem.entity,null,markedItem.id,false); //TODO
        }
        else {
          $scope.openNewTabFn(markedItem.entity,EntityDisplayTypes.files,markedItem.id,false);
        }
      }

      $scope.enableMarkItem = function() {
          return EntityDisplayTypes.groups.equals($scope.displayType);
      }
      $scope.markItemIsDisplayed = function() {
          return EntityDisplayTypes.groups.equals($scope.displayType)||EntityDisplayTypes.groups.equals($scope.notActiveDisplayType);
      }
      $scope.addSelectedItemToMarkedItems = function()
      {
        let newMarkedItemList: any[] = [];
        if($scope.enableMarkItem()) {
          (<any[]>$scope.selectedItemsToTag).forEach(s=> {
            var newMarkedItem = new DTOMarkedItem(s.id, $scope.displayType);
            (<any>newMarkedItem).name = getEntityName(s);
            newMarkedItemList.push(newMarkedItem);
          });
          saveRecentlyUsedMarkedItems(newMarkedItemList);
        }
      }
      var getEntityName = function(entity:any){
        return entity.name?entity.name: (<Entities.DTOGroup>entity.groupName)?(<Entities.DTOGroup>entity.groupName):(<Entities.DTOFileInfo>entity).baseName?(<Entities.DTOFileInfo>entity).baseName: (<Entities.DTOOwner>entity).user?(<Entities.DTOOwner>entity).user:null;
      }

      var registerCommunicationHandlers=function()
      {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportGridMarkItemUserAction,
            $scope,
            (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {

              if (fireParam.action === 'markItem') {

                var entityDisplayType:EntityDisplayTypes=fireParam.entityDisplayType;
                var removeIds=fireParam.removeIds;
                var removeMarkedItems;
                if(removeIds && removeIds[0]) {
                  removeMarkedItems = removeIds.map(id=>  new DTOMarkedItem(id, entityDisplayType));
                }
                var newMarkedItem =  new DTOMarkedItem(fireParam.id,entityDisplayType);
                saveRecentlyUsedMarkedItems([newMarkedItem],removeMarkedItems);

              }
             else if (fireParam.action === 'removeMarkItem') {
                var id=fireParam.id;
                var entityDisplayType:EntityDisplayTypes=fireParam.entityDisplayType;
                var removeMarkedItem =  new DTOMarkedItem(id,entityDisplayType);
                 $scope.removeRecentlyUsedMarkedItem(removeMarkedItem);

              }
              else if (fireParam.action === 'removeMarkItems') {
                var ids=fireParam.ids;
                if(ids && ids[0]) {
                  var removeMarkedItems = ids.map(id=> {
                    var entityDisplayType:EntityDisplayTypes = fireParam.entityDisplayType;
                    var removeMarkedItem = new DTOMarkedItem(id, entityDisplayType);
                    return removeMarkedItem;
                  });
                }
                $scope.removeRecentlyUsedMarkedItems(removeMarkedItems);

              }
              else if (fireParam.action === 'itemNameChanged') {
                var id=fireParam.id;
                var entityDisplayType:EntityDisplayTypes=fireParam.entityDisplayType;
                var updatedMarkedItem =  new DTOMarkedItem(id,entityDisplayType);
                updatedMarkedItem .name = fireParam.name;
                updateRecentlyUsedMarkedItemName(updatedMarkedItem);

              }

            })));

      }

      var closeAllDdls=function()
      {
        $('html').trigger('click'); //to close all ddls
      }

      //var updateDisplayedItemAfterAdd = function (selectedItem, markedItemToAdd:DTOMarkedItem) {
      //  var tags = (<any>selectedItem).item ? (<any>selectedItem).item.markedItemDtos : (<any>selectedItem).markedItemDtos;
      //  tags= JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
      //  if (!isExplicitMarkedItemExists(tags, markedItemToAdd)) {
      //    (<any>markedItemToAdd).isExplicit = true;
      //    (<any>markedItemToAdd).styleId =$scope.getMarkedItemStyleId(markedItemToAdd);
      //    (<any>markedItemToAdd).depth =$scope.getMarkedItemDepth(markedItemToAdd);
      //    tags ? tags.push(markedItemToAdd) : tags = [markedItemToAdd];
      //    updateSelectedItemWithMarkedItems(selectedItem,tags);
      //    log.debug(tags ? '0' : tags.length + " tags exists after adding " + markedItemToAdd.name);
      //
      //  }
      //  else {
      //    log.info('tag ' + markedItemToAdd.name + ' already exsits for... in ' + JSON.stringify(tags));
      //  }
      //};
      //var updateSelectedItemWithMarkedItems=function(selectedItem,tags)
      //{
      //  if ((<any>selectedItem).item) {
      //    var newItem  = (<any>selectedItem).item;
      //    newItem.markedItemDtos = tags;
      //
      //    if( selectedItem.loaded) { //for folder tree view refresh
      //      selectedItem.item = undefined; // force refresh of dataItem
      //      selectedItem.set("item", newItem);
      //      selectedItem.loaded(false);
      //      selectedItem.load();
      //    }
      //    else {
      //      selectedItem.item = newItem;
      //    }
      //  }
      //  else {
      //    (<any>selectedItem).markedItemDtos = tags;
      //  }
      //}
      //
      //var isExplicitMarkedItemExists = function (tags:DTOMarkedItem[], selectedTag:DTOMarkedItem) {
      //  if(tags&& tags[0]) {
      //    return tags.filter(t=>(t.id == selectedTag.id && (<any>t).isExplicit))[0] != null;
      //  }
      //  return false;
      //}

      //var updateDisplayedItemAfterRemove = function (selectedItem, markedItemToRemove:DTOMarkedItem) {
      //
      //  var tags:DTOMarkedItem[] = (<any>selectedItem).item ? (<any>selectedItem).item.markedItemDtos : (<any>selectedItem).markedItemDtos;
      //  if (isExplicitMarkedItemExists(tags, markedItemToRemove)) {
      //    tags= JSON.parse(JSON.stringify(tags)); //use copy of the tags not to cause grid refresh
      //    var itemToRemove = tags.filter(t=>t.id == markedItemToRemove.id && (<any>t).isExplicit )[0];
      //    if (itemToRemove) {
      //      var index = tags.indexOf(itemToRemove);
      //      tags.splice(index, 1);
      //    }
      //    tags = tags.length == 0 ? null : tags;
      //    updateSelectedItemWithMarkedItems(selectedItem,tags);
      //    log.debug(tags ?  tags.length + " tags exists after removing " + markedItemToRemove.name:'0');
      //  }
      //  else {
      //    log.info('tag ' + markedItemToRemove.name + ' already removed for  ... in ' + JSON.stringify(tags));
      //  }
      //};

      //$scope.doMarkedItemFilterBy = function()
      //{
      //  userSettings.getCurrentMarkedItem(function(savedActiveMarkedItemID){
      //    var dlg = dialogs.create(
      //        'common/directives/reports/dialogs/manageMarkedItemsDialog.tpl.html',
      //        'manageMarkedItemsDialogCtrl',
      //        {editMode:false,changeSelectedItemId:savedActiveMarkedItemID},
      //        {size:'lg',keyboard: true,backdrop: false,windowClass: 'center-modal',animation:false,backdropClass:'halfHeight'});
      //
      //    dlg.result.then(function(markedItemToFilter){
      //      $scope.doMarkedItemFilter(markedItemToFilter);
      //      saveRecentlyUsedMarkedItem(markedItemToFilter);
      //
      //    },function(markedItemToFilter){
      //
      //    });
      //
      //  });
      //
      //}


      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });
    })

    .directive('markedItemsMenu',
        function () {
          return {
            // restrict: 'E',
            templateUrl: '/common/directives/reports/markedItemsMenu.tpl.html',
            controller: 'markedItemsMenuCtrl',

            replace:true,
            scope:{
              'selectedItemsToTag':'=',
              'displayType':'=',
              'notActiveDisplayType':'=',
              'reloadData':'=',
              'refreshData':'=',
              'refreshNeeded':'=',
              'disabled':'=',
              'openNewTabFn':'='
            },
            link: function (scope:any, element, attrs) {
              scope.init(true);
            }
          };
        }
    );



'use strict';

angular.module('directives.matter', ['resources.operations'])
  .controller('matterMenuCtrl', function ($scope,$window,$element,$timeout,Logger:ILogger,$q,configuration:IConfig,splitViewBuilderHelper:ui.ISplitViewBuilderHelper,
                                            currentUserDetailsProvider:ICurrentUserDetailsProvider,userSettings:IUserSettingsService,eventCommunicator:IEventCommunicator,dialogs,
                                            departmentResource:IDepartmentResource) {
      var log: ILog = Logger.getInstance('matterMenuCtrl');
      var isMasterManager;
      var MAX_RECENTLY_USED: number = parseInt(configuration.max_recently_used_departments);
      $scope.iconClass = configuration.icon_matter;

      $scope.init = function (isMaster) {
        isMasterManager = isMaster;
        if (isMasterManager) {
          registerCommunicationHandlers();
          initUserSettingsPreferences();
        }
        setUserRoles();
        $scope.$on(currentUserDetailsProvider.userDataUpdatedEvent, function (sender, value: Users.UserAuthenticationData) {
          setUserRoles();
        });

        $timeout(function () { //load only after page has loaded
          loadMatters();
        }, 25);
      };

      $scope.$watch(function () {
        return $scope.selectedMatterId;
      }, function (value) {
        $scope.selectedMatter = _.filter($scope.matter, (matter) => {
          return matter.fileDepartmentDto.id === $scope.selectedMatterId;
        })[0];
      }, true);

      var setUserRoles = function () {
        $scope.isMngMatterConfigUser = currentUserDetailsProvider.isAuthorized([Users.ESystemRoleName.MngDepartmentConfig]);
      };

      var loadMatters = function () {
        log.debug('Matters ---- Bar Loaded');
        departmentResource.getDepartments($scope.onGetMattersCompleted, onError);
      };

      var initUserSettingsActiveMatter = function () {
        setActiveRecentlyUsedMatters();

        $scope.$on(userSettings.activeRecentlyUsedDepartmentItemUpdatedEvent, function (sender, value) {
          setActiveRecentlyUsedMatters();
        });
      };

      $scope.toggleMinimizeToolbar = function () {
        $scope.minimizeToolbar = !$scope.minimizeToolbar;
        userSettings.setDepartmentsPreferences({minimizeToolbar: $scope.minimizeToolbar});

        $timeout(function () {
          $window.dispatchEvent(new Event("resize"));
        }, 0);
      };

      var initUserSettingsPreferences = function () {
        userSettings.getDepartmentsPreferences(function (preferences) {
          $scope.minimizeToolbar = preferences && typeof (<any>preferences).minimizeToolbar != 'undefined' ? (<any>preferences).minimizeToolbar : false;
        });
      };

      var setLastSelectedMatterItemId = function (lastSelectedId) {
        if (lastSelectedId) {
          userSettings.setCurrentDepartment(lastSelectedId);
        }
      };

      var setActiveRecentlyUsedMatters = function () {
        userSettings.getRecentlyUsedDepartment(function (savedActiveRentlyUsedMatterIDs) {
            var approvedListOfRecntlyUsedItems = [];
            if ($scope.matterList && $scope.matterList.length > 0 && savedActiveRentlyUsedMatterIDs && savedActiveRentlyUsedMatterIDs.length > 0) {
              savedActiveRentlyUsedMatterIDs.forEach(s => {
                var foundItem = $scope.matterList.filter(d => d.id == s)[0];
                if (foundItem) {
                  if (approvedListOfRecntlyUsedItems.length <= MAX_RECENTLY_USED) {
                    approvedListOfRecntlyUsedItems.push(foundItem.id);
                  }
                }
              });
            }

            $scope.savedRecentlyUsedMatterItems = _.filter($scope.matterList, (matter) => {
              return _.includes(approvedListOfRecntlyUsedItems, matter.id);
            });

            if (approvedListOfRecntlyUsedItems.length > 0 && approvedListOfRecntlyUsedItems.length !== savedActiveRentlyUsedMatterIDs.length) {
              userSettings.setRecentlyUsedDepartment(approvedListOfRecntlyUsedItems);
            }
          }
        );
      };

      var saveRecentlyUsedMatter = function (matterToSave) {
        if (matterToSave) {
          userSettings.getRecentlyUsedDepartment(function (savedActiveRecentlyUsedMatterIDs) {
            if (savedActiveRecentlyUsedMatterIDs) { //add current active to recently used
              var foundInRecentItems = savedActiveRecentlyUsedMatterIDs.filter(d => d == matterToSave.id)[0];
              if (foundInRecentItems) { //if already exists remove it
                var foundIndex = savedActiveRecentlyUsedMatterIDs.indexOf(foundInRecentItems);
                savedActiveRecentlyUsedMatterIDs.splice(foundIndex, 1);
              }
              savedActiveRecentlyUsedMatterIDs.unshift(matterToSave.id);//Use unshift, which modifies the existing array by adding the arguments to the beginning:
              if (savedActiveRecentlyUsedMatterIDs.length > MAX_RECENTLY_USED) {
                savedActiveRecentlyUsedMatterIDs.splice(MAX_RECENTLY_USED, savedActiveRecentlyUsedMatterIDs.length - MAX_RECENTLY_USED); //limit size of list
              }
            }
            else {
              savedActiveRecentlyUsedMatterIDs = [matterToSave.id];
            }
            userSettings.setRecentlyUsedDepartment(savedActiveRecentlyUsedMatterIDs);
          });
        }
      };

      $scope.getMatterStyleId = function(matter) {
        if (matter) {
          return splitViewBuilderHelper.getDepartmentStyleId(matter);
        }
        return null;
      };

      $scope.getMatterDepth = function(matter) {
        if (matter) {
          return splitViewBuilderHelper.getDepartmentDepth(matter);
        }
        return null;
      };

      $scope.onGetMattersCompleted = function (matters: Entities.DTODepartment[]) {
        $scope.matterList = matters;
        if (isMasterManager) {
          initUserSettingsActiveMatter();
        }
      };

      $scope.$watch(function () {
        return $scope.selectedMatterItemToTag;
      }, function (value) {
        $scope.isAssignEnabled = !$scope.disabled && areSelectedItemsAuthorized();
      });

      $scope.$watch(function () {
        return $scope.selectedMatterItem;
      }, function (value) {
        if ($scope.selectedMatterItem) {

        }
      });

      $scope.toggleMatterToSelectedItems = function (matter:Entities.DTODepartment) {
        var selectedItemMatter = ($scope.selectedMatterItemToTag && $scope.selectedMatterItemToTag.length > 0) ? $scope.selectedMatterItemToTag[0].item.departmentDto : null;
        if (matter && selectedItemMatter && selectedItemMatter.id == matter.id) {
          $scope.removeMatterFromSelectedItems(matter);
        }
        else {
          $scope.addMatterToSelectedItems(matter);
        }
      };

      var showWarning = function(callback) {
        let warningRequired = _.some($scope.selectedMatterItemToTag, (item) => {
          return _.has(item, 'numOfAllFiles') && item.numOfAllFiles > configuration.association_warning_threshold
        });
        if(warningRequired) {
          userSettings.getDontShowAssociationWarning((dontShow) => {
            if (!dontShow) {
              var dlg = dialogs.create('common/directives/dialogs/confirmWithDoNotShowAgainCheckBox.tpl.html', 'confirmWithDoNotShowAgainCheckBox', {title: 'Confirm operation', msg: 'This action may take few minutes to fully complete. Refresh page in order to see changes.'}, {size: 'sm'});
              dlg.result.then((dontShow) => {
                userSettings.setDontShowAssociationWarning(dontShow);
                callback();
              });
            }else {
              callback();
            }
          });
        }else {
          callback();
        }
      };

      $scope.addMatterToSelectedItems = function (matterToAdd:Entities.DTODepartment) {
        if (matterToAdd && $scope.selectedMatterItemToTag) {
          showWarning (() => {
            addMatterToItems($scope.selectedMatterItemToTag, $scope.selectedMatterItemToTag.map(s => getEntity(s)), matterToAdd);
          });
        }
      };

      $scope.removeMatterFromSelectedItems= function (matterToRemove:Entities.DTODepartment) {
        if (matterToRemove && $scope.selectedMatterItemToTag) {
          showWarning (() => {
            removeMatterFromItems($scope.selectedMatterItemToTag, $scope.selectedMatterItemToTag.map(s => getEntity(s)), matterToRemove);
          });
        }
      };

      var areSelectedItemsAuthorized = function():boolean {
        if($scope.selectedMatterItemToTag &&$scope.selectedMatterItemToTag.length>0) {
          return getAuthorizedItems($scope.selectedMatterItemToTag.map(i => getEntity(i)))[0]?true:false;
        }
        return false;
      };

      var addMatterToItems = function(originalItems, items:Entities.IDepartmentAuthorization[], matterToAdd:Entities.DTODepartment) {
        if (matterToAdd && items && items.length > 0) {
        var loopPromises = [];
        var approvedItems = getAuthorizedItems(items);
        if (approvedItems && approvedItems.length > 0) {
          approvedItems.forEach(function (selectedItem:Entities.IDepartmentAuthorization) {
            var performAddPromise = isSingleMatterReplacement(matterToAdd, selectedItem);
            performAddPromise.then(function (replaced: boolean) {
              var addMatter = departmentResource.addDepartment(matterToAdd.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              addMatter.$promise.then(function (updatedItem: any) {
                 updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  onError();
                  log.error('receive addMatter failure' + error);
              });
              $q.all(loopPromises).then(function (selectedItemsToUpdate) {
                if ($scope.refreshNeeded) {
                  $scope.refreshData();
                }
                else {
                  $scope.reloadData(); //no need when auto sync is false
                }
              });
            });
          });
        }
      }
    };

    var isSingleMatterReplacement = function(matterToAdd:Entities.DTODepartment,selectedItem:Entities.IDepartmentAuthorization):any {
      var deferred = $q.defer();
        if( selectedItem.departmentDto ) {
          if(!selectedItem.departmentDto.implicit){
            var dlg:any =  dialogs.confirm('Replace Matter', 'You are about to replace existing matter \''+selectedItem.departmentDto.name+'\' with \''+matterToAdd.name+ '\'.<br><br></br>Continue?', null);
            dlg.result.then(function(){
              deferred.resolve(true);
             },function(){
              deferred.reject();
            });
          }
          else{
            deferred.resolve(false);
          }
        }
        else{
          deferred.resolve(false);
        }
        return deferred.promise;
    };

      var removeMatterFromItems= function (originalItems, items:Entities.IDepartmentAuthorization[], matterToRemove:Entities.DTODepartment) {
        if (matterToRemove && items && items.length > 0) {
          var approvedItems =getApprovedItemsForRemoval(items, matterToRemove);
          var loopPromises = [];
          if (approvedItems && approvedItems.length > 0) {
            approvedItems.forEach(function (selectedItem) {
              var removeTag = departmentResource.removeDepartment(matterToRemove.id, selectedItem['id']);
              var deferred = $q.defer();
              loopPromises.push(deferred.promise);
              removeTag.$promise.then(function (updatedItem:any) {
                  log.debug('receive removeMatter from selected items success for' + selectedItem['id']);
                  updateSelectedItem(originalItems.filter(o=>o.id==updatedItem.id)[0],updatedItem);
                  deferred.resolve(updatedItem);
                },
                function (error) {
                  deferred.resolve();
                  onError();
                  log.error('receive removeMatter from selected items failure' + error);
                });
            });
            $q.all(loopPromises).then(function (selectedItemsToUpdate) {
              if ($scope.refreshNeeded) {
                $scope.refreshData();
              }
              else {
                $scope.reloadData(); //no need when auto sync is false
              }
            });
          }
        }
      };

      var getApprovedItemsForRemoval = function(items: Entities.IDepartmentAuthorization[], matterToRemove: Entities.DTODepartment) {
        var approvedItems = items.filter(i => {
          var matterDto =i.departmentDto ;
          return !matterDto.implicit;
        });
        return approvedItems.filter(s=>s.deleteDepartmentAuthorized)
      };

      var getAuthorizedItems = function(items: Entities.IDepartmentAuthorization[]) {
        var approvedItems = items.filter(s => {
          return s.assignDepartmentAuthorized;
        });
        return approvedItems;
      };

      var getEntity = function(item): Entities.IDepartmentAuthorization {
        return item.item ? item.item : item; //kendo grid
      };

      $scope.doMatterFilter = function( matter:Entities.DTODepartment,isSubtreeFilter:boolean) {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMatterMenuUserAction, {
          action: 'doMatterFilter',
          values:  [matter],
          isSubtreeFilter:isSubtreeFilter
        });
      };

      var registerCommunicationHandlers=function() {
        eventCommunicator.registerHandler(EEventServiceEventTypes.ReportMatterMenuUserAction,$scope,
          (new RegisteredEvent(function (registrarScope:any, registrationParam?:any, fireParam?:any) {
            if (fireParam.action === 'removeMatter') {
              var item=<Entities.IDepartmentAuthorization>fireParam.item;
              var matterToRemove=fireParam.matterToRemove;

              if($scope.displayType == fireParam.displayType) {
                removeMatterFromItems([item],[getEntity(item)], matterToRemove);
              }
            }
            else if (fireParam.action === 'setMatter') {
              var entity=<Entities.IDepartmentAuthorization>getEntity(fireParam.item);
              $scope.selectMatterDialog(false, $scope.addMatterToSelectedItems);
            }
        })));
      };

      var closeAllDdls=function() {
        $('html').trigger('click'); //to close all ddls
      };

      var updateSelectedItem=function(originalItem, updatedItem:Entities.IDepartmentAssignee) {
        var originalEntity =   getEntity(originalItem);
        originalEntity.departmentDto =  updatedItem.departmentDto;
        splitViewBuilderHelper.parseDepartment( originalEntity,configuration);
        if ((<any>originalItem).item) {
          if( originalItem.loaded) { //for folder tree view refresh
            originalItem.item = undefined; // force refresh of dataItem
            originalItem.set("item", originalEntity);
            originalItem.loaded(false);
          }
        }
      };

      $scope.doUnAssociationFilterBy = function() {
        closeAllDdls();
        eventCommunicator.fireEvent(EEventServiceEventTypes.ReportMatterMenuUserAction, {
          action: 'doMatterUnassociated',
          isSubtreeFilter: true
        });
      };


      $scope.doMatterFilterBy = function() {
        $scope.selectMatterDialog(true, function(selectedMatter) {
          $scope.doMatterFilter(selectedMatter, true);
        });
      };

      $scope.selectMatterDialog = function(filterMode:string, callback?:any) {
        var firstSelectedItem, activeMatterID;
        if ($scope.selectedMatterItemToTag && $scope.selectedMatterItemToTag[0]) {
           firstSelectedItem = getEntity($scope.selectedMatterItemToTag[0]);
            activeMatterID = firstSelectedItem.departmentDto ? firstSelectedItem.departmentDto.id : null;
        }
        if(activeMatterID) {
          openSelectedMatterDialog(activeMatterID, filterMode, callback);
        }
        else {
          userSettings.getCurrentDepartment(function(savedActiveMatterID) {
            openSelectedMatterDialog(savedActiveMatterID, filterMode, callback);
          });
        }

        function openSelectedMatterDialog(activeMatterID:number, filterMode:string, callback?:any) {
          var dlg = dialogs.create(
            'common/directives/reports/dialogs/selectMatterDialog.tpl.html',
            'selectMatterDialogCtrl',
            {filterMode: filterMode, changeSelectedItemId: activeMatterID},
            {
              size: 'md',
              keyboard: true,
              backdrop: false,
              windowClass: 'center-modal',
              animation: false,
              backdropClass: 'halfHeight'
            });

          dlg.result.then(function (selectedMatter) {
            setLastSelectedMatterItemId(selectedMatter ? selectedMatter.id : null);
            if (callback) {
              callback(selectedMatter);
            }
            saveRecentlyUsedMatter(selectedMatter);
          });
        }
      };

      $scope.$on("$destroy", function() {
        eventCommunicator.unregisterAllHandlers($scope);
      });

      var onError = function() {
        dialogs.error('Error','Sorry, an error occurred.');
      };
    })
    .directive('matterMenu',
    function () {
      return {
        // restrict: 'E',
        templateUrl: '/common/directives/reports/matterMenu.tpl.html',
        controller: 'matterMenuCtrl',
        replace:true,
        scope:{
          'selectedMatterItemToTag':'=',
          'displayType':'=',
          'reloadData':'=',
          'refreshData':'=',
          'refreshNeeded':'=',
          'disabled':'=',
          'openManagePage':'=',
        },
        link: function (scope:any, element, attrs) {
          scope.init(true);
        }
      };
    }
);



// Generated on 2015-02-08 using generator-angular-fullstack 2.0.13
'use strict';

//Typescript files are compiled to src/bin keeping the directories hierarchy.
module.exports = function (grunt) {
  var localConfig;
  try {
    localConfig = require('../../server/config/local.env');
  } catch(e) {
    localConfig = {};
  }


  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server',
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    //cdnify: 'grunt-google-cdn',
    protractor: 'grunt-protractor-runner',
    //buildcontrol: 'grunt-build-control',
    injector: 'grunt-asset-injector',
    typescript: 'grunt-typescript',
    less:'grunt-contrib-less',
    replace:'grunt-replace',
    karma:'grunt-karma',
    svgstore:'grunt-svgstore',
    cachebreaker:'grunt-cache-breaker'
  });

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({
    distDir: 'dist',
    clientDir: require('./bower.json').appPath || '.',

    bowerDeploy: Object.keys(grunt.file.readJSON('bower.json').dependencies).join(),
    nodeDeploy: Object.keys(grunt.file.readJSON('package.json').dependencies).join(),
    serverDir: '../server',
    htmlFiles: '{index,login,404}.html',
    test:{
      testDir:'<%= clientDir %>/tests',
      unitConfig:'<%= test.testDir %>/config/unit.js',
      unitSpec:'<%= test.testDir %>/unit/**/*.spec.js',

    },
    src: {
      srcDir:'<%= clientDir %>/src',
      node_modules:'<%= clientDir %>/node_modules',
      appDir:'<%= src.srcDir %>/app',
      commonDir:'<%= src.srcDir %>/common',
      lessDir:'<%= src.srcDir %>/less',
      cssDir:'<%= src.lessDir %>/css',
      binDir:'<%= src.srcDir %>/bin',

      js: '<%= src.binDir %>/*/**/*.js',
      templatesJs: '<%= src.binDir %>/temp*.js',
      appFile: '<%= src.binDir %>/**/app.js',
      specs: 'test/**/*.spec.js',
      mock: 'test/**/*.mock.js',
      indexHtml: 'index.html',
      loginHtml: 'login.html',
      html: '{index,login}.html',
      tpl: {
        app: '<%= src.appDir %>/**/*.tpl.html',
        common: '<%= src.commonDir %>/**/*.tpl.html'
      },
      htmlBase:'**/*_base.html',
      lessFile: '<%= src.lessDir %>/style.less', // recess:build doesn't accept ** in its file patterns
      less: '<%= src.lessDir %>/**/*.less',
      cssFile:'<%= src.cssDir %>/style.css',
      loginCss:'login.css',
      css:'<%= src.cssDir %>/**/*.css',
      appCss: '<%= src.appDir %>/**/*.css',
      vendorCss: '<%= clientDir %>/vendor/**/*.css',
      assetsCss: '<%= src.srcDir %>/assets/css/**/*.css',
      assetsSvgDir: '<%= src.srcDir %>/assets/svg',
      assetsSvg: '<%= src.assetsSvgDir %>/**/*.svg',
      assetsSvgSpritesFile: '<%= src.assetsSvgDir %>/sprites/svg-defs.svg',
      typescriptFile:'<%= src.appDir %>/app.ts',
      typescript:'<%= src.srcDir %>/**/*.ts',

      //typescript:'<%= src.srcDir %>/{app,common}/**/*.ts',
      config:['<%= src.srcDir %>/config/**/*.js','<%= src.srcDir %>/config/**/*.json'],
      configFile:'<%= src.srcDir %>/config/config.js',
      appDirBin:'<%= src.binDir %>/src/app',

      imagesDir:'<%= src.srcDir %>/assets/images',
      images:'<%= src.imagesDir %>/**/*.{png,jpg,jpeg,gif,webp,svg}'
    },
    dist: {
      publicDir: '<%= distDir %>/public',
      js: '<%= dist.publicDir %>/{,*/}*.js',
      cssDir: '<%= dist.publicDir %>/css',
      css: '<%= dist.publicDir %>/{,*/}*.css',
      cssFile: '<%= dist.cssDir %>/style.css',
      html: '<%= distDir %>/*.html',
      imagesDir: '<%= dist.publicDir %>/assets/images',
      images: '<%= dist.imagesDir %>/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
      fontsDir: '<%= dist.publicDir %>/assets/fonts',
      fonts: '<%= dist.fontsDir %>/*',
      jsTpl: '<%= publicDir %>/templates'
    },
    server: {
      port: localConfig.port || 9000,
      protocol: (localConfig.ssl ? 'https://' : 'http://'),
      livereloadConf:  ((localConfig.livereloadPort && localConfig.livereloadPort<0) ? false : (localConfig.livereloadPort || 4141)),
      appFile: '<%= serverDir %>/app.js',
      js:'<%= serverDir %>/**/*.js'
    },

    express: {
      options: {
        port: process.env.PORT || '<%= server.port %>'
      },
      dev: {
        options: {
          script: '<%= serverDir %>/app.js',
          debug: false,

        }
      },
      prod: {
        options: {
          script: 'dist/server/app.js'
        }
      }
    },
    open: {
      server: {
        url: '<%= server.protocol %>localhost:<%= server.port %>'
      }
    },
    less: {
      dev: {
        options: {
          compress: false,
          yuicompress: false,
          optimization: 2,
          sourceMap: false,
          sourceMapFilename: "<%= src.cssFile %>.map",
          sourceMapBasepath: "<%= src.cssDir %>"
        },
        files: {
          "<%= src.cssFile %>": "<%= src.lessFile %>"
        }
      },
      prod: {
        options: {
          compress: false,
          sourceMap: false,
          // paths: ["assets/css"],
          plugins: [
            //new require('less-plugin-autoprefix')({browsers: ["last 2 versions"]}),
            //new require('less-plugin-clean-css')({
            //    advanced: true,
            //   'compatibility': 'ie8'
            //})
          ]
        },
        files: {
          "<%= dist.cssFile %>": "<%= src.lessFile %>"
        }
      }
    },
    typescript: {
      dev: {
        options: {
          sourceMap: true,
          keepDirectoryHierarchy: true,
          target: 'es5', //or es3
        },
        src: [
          '<%= src.typescriptFile %>',
          '<%= src.typescript %>'
        ],
        dest: '<%= src.binDir %>'
      }
    },
    watch: {
      typescript: {
        files: ['<%= src.typescript %>'],
        tasks: ['typescript'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      less: {
        files: ['<%= src.less %>'],
        tasks: ['less:dev'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      htmlBase: {
        files: ['<%= src.htmlBase %>'],
        tasks: ['copy:htmlBase','injector:scripts','injector:css','wiredep'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      tpl: {
        files: [  '<%= src.tpl.app %>','<%= src.tpl.common %>'],
        tasks: ['ngtemplates'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      svg: {
        files: [ '<%= src.assetsSvg %>','!<%= src.assetsSvgSpritesFile %>'],
        tasks: ['svgstore'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      //karma: {
      //  files: ['app/js/**/*.js', 'test/browser/**/*.js'],
      //  tasks: ['karma:unit:run'] //NOTE the :run flag
      //},
      uncss: {
        files: ['<%= src.css %>'],
        tasks: ['copy:htmlBase','uncss','injector:scripts','injector:css','wiredep'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      replace: {
        files: ['<%= src.config %>'],
        tasks: ['replace'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      mochaTest: {
        files: ['server/**/*.spec.js'],
        tasks: ['env:test', 'mochaTest'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      //jsTest: {
      //  files: [
      //    '<%= src.test %>',
      //    '<%= src.mock %>'
      //  ],
      //  tasks: ['newer:jshint:all', 'karma']
      //},
      gruntfile: {
        files: ['Gruntfile.js'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      },
      livereload: {
        files: [
          '<%= src.html %>',
          '<%= src.tpl.app %>',
          '<%= src.tpl.common %>',
          '<%= src.css %>',
          '<%= src.js %>',
          '<%= src.templatesJs %>',
          '<%= src.assetsSvg %>'
          //     '!<%= src.test %>}',
          //     '!<%= src.mock %>/**/*.mock.js',
          //    '<%= src.images %>'
        ],
        options: {
          livereload: '<%= server.livereloadConf %>'
        }
      },
      express: {
        files: [
          'server/**/*.{js,json}'
        ],
        tasks: ['express:dev', 'wait'],
        options: {
          nospawn: true,
          livereload: '<%= server.livereloadConf %>'
        }
      }
    },

    // We have declared the development.json file to be used as the pattern to replace content within the config.js file
    replace: {
      development: {
        options: {
          patterns: [{
            json: grunt.file.readJSON('../client-angularjs/src/config/environments/development.json')
          }]
        },
        files: [{
          expand: true,
          flatten: true,
          src: ['<%= src.configFile %>'],
          dest:  '<%= src.appDirBin %>'
        }]
      },
      production: {
        options: {
          patterns: [{
            json: grunt.file.readJSON('../client-angularjs/src/config/environments/production.json')
          }]
        },
        files: [{
          expand: true,
          flatten: true,
          src: ['<%= src.configFile %>'],
          dest:  '<%= src.appDirBin %>'
          //dest:  '<%= dist.publicDir %>'
        }]
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '<%= clientDir %>/.jshintrc',
        reporter: require('jshint-stylish')
      },
      server: {
        options: {
          jshintrc: 'server/.jshintrc'
        },
        src: [
          '<%= server.js %>',
          '!server/**/*.spec.js'
        ]
      },
      serverTest: {
        options: {
          jshintrc: 'server/.jshintrc-spec'
        },
        src: ['server/**/*.spec.js']
      },
      all: [
        '<%= src.js %>'
      ],
      test: {
        src: [
          '<%= src.spec %>',
          '<%= src.mock %>'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= distDir %>/*',
            '!<%= distDir %>/.git*',
            '!<%= distDir %>/.openshift',
            '!<%= distDir %>/Procfile',
            'tmp_prod2_nm/node_modules/*',         // Clean proprietary production package module
            'server/tmp_prod_nm/node_modules/*'    // Clean proprietary production package module
          ]
        }]
      },
      server: ['.tmp','<%= src.binDir %>']

    },

    // parse CSS and add vendor prefixes to CSS ex: from :fullscreen to :-webkit-full-screen,-moz-full-screen...
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/',
          src: '{,*/}*.css',
          dest: '.tmp/'
        }]
      },
      dev: {
        files: [{
          expand: true,
          cwd: '<%= server.cssDir %>/',
          src: '{,*/}*.css',
          dest: '<%= server.cssDir %>/'
        }]
      }
    },

    // Debugging with node inspector
    'node-inspector': {
      custom: {
        options: {
          'web-host': 'localhost'
        }
      }
    },

    // Use nodemon to run server in debug mode with an initial breakpoint
    nodemon: {
      debug: {
        script: '<%= server.appFile %>',
        options: {
          nodeArgs: ['--debug-brk'],
          env: {
            PORT: '<%= server.port %>'
          },
          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            // opens browser on initial server start
            nodemon.on('config:update', function () {
              setTimeout(function () {
                require('open')('<%= server.protocol %>localhost:<%= server.port %>/debug?port=5858');
              }, 500);
            });
          }
        }
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      target: {
        src: '<%= src.indexHtml %>',
        //  ignorePath: '<%= src.clientDir %>/',
        exclude: ['/bootstrap-sass-official/', '/json3/', '/es5-shim/','jasny-bootstrap'],
        // fileTypes: {
        //   html: {
        //     replace: {
        //       js: function(filePath) {
        //         return '<script src="' + filePath.replace('../', '') + '"></script>'
        //       },
        //       css: function(filePath) {
        //         return '<link rel="stylesheet" type="text/css" href="' +  filePath.replace('../', '') +'" />'
        //       }
        //     }
        //   }
        // }
      }
    },

    // Renames files for browser caching purposes
    //rev: {
    //  dist: {
    //    files: {
    //      src: [
    //        '<%= dist.js %>',
    //        '<%= dist.css %>',
    //        '<%= dist.images %>',
    //        '<%= dist.fonts %>'
    //      ]
    //    }
    //  }
    //},

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: ['<%= src.html %>'],
      options: {
        root: ['<%= clientDir %>'],
        dest: '<%= dist.publicDir  %>'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration. concatenates files, minifyand uglify js,minify css, revision files
    usemin: {
      html: ['<%= dist.html %>'],
      css: ['<%= dist.publicDir %>/**/*.css'],
      js: ['<%= dist.publicDir %>/**/*.js'],
      options: {
        assetsDirs: [
          '<%= dist.publicDir %>',
          '<%= dist.publicDir %>/assets/images',
          '<%= dist.publicDir %>/assets/images/patterns'
        ],
        // This is so we update image references in our ng-templates
        patterns: {
          js: [
            [/(assets\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
          ]
        }
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= src.imagesDir %>',
            src: '{,*/}*.{png,jpg,jpeg,gif}',
            dest: '<%= dist.imagesDir %>'
          }
        ]
      }
    },

    //create sprite svg file of symbols from every svg images in folder
    svgstore: {
      dist: {
        files: {
          '<%= src.assetsSvgSpritesFile %>': ['<%= src.assetsSvg %>','!<%= src.assetsSvgSpritesFile %>']
        },
      },
      options: {
        cleanup: true
      }
    },


    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat',
          src: '*/**.js',
          dest: '.tmp/concat'
        }]
      }
    },

    // Package all the html partials into a single javascript payload
    ngtemplates: {
      appTemplates:        {
        cwd:     '<%= src.srcDir %>',
        src:      [ 'app/**/*.tpl.html',
          'common/**/*.tpl.html']
        ,
        dest:     '<%= src.binDir %>/template1.js',
        options:  {
          standalone:true,
          append:false,
          prefix: '/',
          htmlmin: {
            collapseBooleanAttributes:      false,
            collapseWhitespace:             true,
            removeAttributeQuotes:          false,
            removeComments:                 true,
            removeEmptyAttributes:          false,
            removeRedundantAttributes:      false,
            removeScriptTypeAttributes:     false,
            removeStyleLinkTypeAttributes:  false
          },
        }
      },
      appSvg:        {
        cwd:     '<%= src.srcDir %>',
        src:      [  'assets/svg/**/*.svg']
        ,
        dest:     '<%= src.binDir %>/templates1Svg.js',
        options:  {
          standalone:true,
          append:false,
          prefix: '/',
          htmlmin: {
            collapseBooleanAttributes:      false,
            collapseWhitespace:             true,
            removeAttributeQuotes:          false,
            removeComments:                 true,
            removeEmptyAttributes:          false,
            removeRedundantAttributes:      false,
            removeScriptTypeAttributes:     false,
            removeStyleLinkTypeAttributes:  false
          },
        }
      }
    },

    // Replace Google CDN references
    //cdnify: {
    //  dist: {
    //    html: ['<%= distDir %>/public/*.html']
    //  }
    //},

    uncss: {
      login: {
        files: {
          '<%= src.loginCss %>': ['<%= src.loginHtml %>']
        }
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              '*.{ico,png,txt}',
              '.htaccess',
              'bower_components/**/*',
              'fonts/*'
            ]
          },
          {
            // create assets/css and html
            expand: true,
            cwd: '<%= src.srcDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              '<%= htmlFiles %>',
              'favicon.ico',
              'assets/fonts/*',
              'assets/images/**/*'
            ]
          },
          //{
          //  // create assets/css
          //  expand: true,
          //  cwd: '<%= src.srcDir %>',
          //  dest: '<%= dist.publicDir %>',
          //  src: [
          //    'assets/**/*.css',
          //    'assets/vavicon.ico',
          //    'assets/fonts/*',
          //    'assets/images',
          //    'app/login.css',
          //    'fonts/**/*'
          //  ]
          //},
          {
            // kendo scripts
            expand: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              'vendor/kendo/**/*.{min.css,min.js}'
            ]
          }, {
            // kendo Bootstrap resources
            expand: true,
            cwd: '<%= clientDir %>/vendor/kendo',
            dest: '<%= dist.publicDir %>',
            src: [
              'Bootstrap/**/*'
            ]
          },
          //  {
          //  expand: true,
          //  cwd: '.tmp/images',
          //  dest: '<%= distDir %>/public/assets/images',
          //  src: ['generated/*']
          //},
          {
            expand: true,
            dest: '<%= distDir %>',
            src: [
              'package.json',
              'server/**/*',
              'ssl/*.{key,crt}',
              '!ssl/*.enc.crt'
            ]
          }]
      },
      packDev: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              '*.{ico,png,txt}',
              '.htaccess',
              // 'bower_components/**/*',
              'fonts/*'
            ]
          },
          {
            // templates files
            expand: true,
            dot: true,
            cwd: '<%= clientDir %>/src',
            dest: '<%= dist.publicDir %>',
            src: '**/*.tpl.html'
          },
          {
            // Mock data
            expand: true,
            dot: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: 'mock-data/**/*'
          },
          {
            // create assets/images and html
            expand: true,
            cwd: '<%= clientDir %>',
            dest: '<%= distDir %>',
            src: [
              '<%= htmlFiles %>',
              'favicon.ico'
            ]
          },
          {
            // create assets/images and html
            expand: true,
            cwd: '<%= src.srcDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              'assets/images/*'
              // 'assets/**/*'
            ]
          },
          {
            // copy assets.fonts
            expand: true,
            cwd: '<%= src.srcDir %>/assets',
            dest: '<%= dist.publicDir %>',
            src: 'fonts/*'
          },
          {
            // Copy bower components assets
            expand: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              'bower_components/{<%= bowerDeploy %>}/fonts/**/*',
              'bower_components/{<%= bowerDeploy %>}/images/**/*'
            ],
            rename: function(dest, src) {
              // Remove the 'bower_components/*' part of the destination path to have all resources under /images, /fonts.
              var inx = src.indexOf('/images');
              if (inx>=0) {
                return dest + src.substring(inx);
              }
              inx = src.indexOf('/fonts');
              if (inx>=0) {
                return dest + src.substring(inx);
              }
              return dest+'/'+src;
            }
          },
          {
            // kendo scripts
            expand: true,
            cwd: '<%= clientDir %>',
            dest: '<%= dist.publicDir %>',
            src: [
              'vendor/kendo/*.{min.css,min.js}',
              'vendor/kendo/Bootstrap/*',
              'vendor/kendo/fonts/**'
            ]
          },
          {
            // kendo Bootstrap resources
            expand: true,
            cwd: '<%= clientDir %>/vendor/kendo',
            dest: '<%= dist.publicDir %>',
            src: [
              'Bootstrap/**/*'
            ]
          },
          {
            expand: true,
            dest: '<%= distDir %>',
            src: [
              'package.json',
              'server/**/*'
            ]
          },
          {
            expand: false,
            dest: '<%= distDir %>/ssl/',
            src: [
              '../ssl/*.{key,crt}'
            ]
          }
        ]
      },
      unUglified: {
        files: [
          {     // copy original conact files until uglify is working ...
            expand: true,
            dot: true,
            cwd: '.tmp/concat',
            dest: '<%= dist.publicDir %>',
            src: 'app/*.{js,css}'
          }
        ]
      },
      htmlBase: {
        expand: true,
        //  cwd: '<%= src.srcDir %>',
        dest: '<%= src.clientDir %>',
        src:  ['*_base.html'],
        rename: function(dest, matchedSrcPath, options) {
          // return the destination path and filename:
          return (dest + matchedSrcPath).replace('_base', '');
        }
      }
    },

    // Used for packaging the code for field deployment (production as well as dev/demo)
    // usage example: grunt compress:node
    compress: {
      // used for creating deployment packages
      dev: {
        options: {
          archive: '../build/da_ui_dev.zip'
        },
        files: [
          {
            expand: true,
            cwd: '',
            src: [
              'bower_components/**/*',
              'favicon.ico',
              'fonts/**/*',
              'index.html',
              'login.css',
              'login.html',
              'package-lock.json',
              'package.json',
              'robots.txt',
              'src/**/*',
              'vendor/**/*',
              // '!<%= src.node_modules %>',
              // '!<%= src.node_modules %>',
              // '!<%= distDir %>',
              // '!.temp',
              // '!typings',
            ]
          }
        ]
      },
      // used for creating deployment packages
      server: {
        options: {
          archive: '../build/da_ui_server.zip'
        },
        files: [
          {
            expand: true,
            cwd: '../server',
            src: [
              '{components,config,views}/**/*',
              '*.{js,json}'
            ]
          }
        ]
      },
      node: {
        options: {
          archive: '../build/da_ui_node.zip'
        },
        files: [
          {
            expand: true,
            cwd: '../server/tmp_prod_nm',   // works in sync with prepare_install.bat
            // src: ['node_modules/{<%= nodeDeploy %>}/**/*']   // package 'non dev' modules only
            src: ['node_modules/**/*']
          }
        ]
      },
      dist: {
        options: {
          archive: '../build/da_ui_dist.zip'
        },
        files: [
          {
            expand: true,
            cwd: '',
            src: ['<%= distDir %>/**/*']    // archive path to be created under 'dist' to support 'dual' mode
            // cwd: '<%= distDir %>',
            // src: ['**/*']
          }
        ]
      },

    },
    // Run some tasks in parallel to speed up the build process
    concurrent: {
      client: {
        tasks: [
          'typescript',     //build .ts files into binDir/.js and .js.map
          'less:dev',        //build .less files into less/css/.css
          'svgstore'          //create svg sprite of symbols file
        ],
        options: {
          logConcurrentOutput: false
        }
      },

      debug: {
        tasks: [
          'nodemon',
          'node-inspector'
        ],
        options: {
          logConcurrentOutput: true
        }
      },
      dist: [
        'typescript',
        'less:dev',
        'imagemin',
      ]
    },

    // Test settings
    karma: {
      options: {
        files: [
          //     '<%= dist.js %>', // js source files
          //    '<%= test.unitSpec %>' // unit test files
        ]
      },
      unit: {
        configFile: '<%= test.unitConfig %>',
        //  configFile: '/client/tests/config/unit.js',
        //   files: ['<%= test.unitSpec %>'],
        port: 9999,
        singleRun: true, //The singleRun: false option will tell grunt to keep the karma server up after a test run.
        background: false, //The background option will tell grunt to run karma in a child process so it doesn't block subsequent grunt tasks.
        browsers: ['Chrome'],
        logLevel: 'INFO',
        autoWatch: false
      },
      unit_watch: {
        configFile: 'karma.conf.js',
        singleRun: false,
        autoWatch: true
      },
      e2e: {
        configFile: 'karma-e2e.conf.js'
      },
      e2e_watch: {
        configFile: 'test/karma-e2e.conf.js',
        singleRun: false,
        autoWatch: true
      }
    },

    mochaTest: {
      options: {
        reporter: 'spec'
      },
      src: ['server/**/*.spec.js']
    },

    protractor: {
      options: {
        configFile: 'protractor.conf.js'
      },
      chrome: {
        options: {
          args: {
            browser: 'chrome'
          }
        }
      }
    },

    env: {
      test: {
        NODE_ENV: 'test'
      },
      prod: {
        NODE_ENV: 'production'
      },
      all: localConfig
    },

    injector: {
      options: {
        min: true
      },
      // Inject application script files into index.html (doesn't include bower)
      scripts: {
        options: {
          transform: function(filePath) {
            //filePath = filePath.replace('/client/src/bin/client', '');
            filePath = filePath.replace('/./', '');
            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:js -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          '<%= src.indexHtml %>': [
            [
              '<%= src.js %>',
              '<%= src.templatesJs %>',
              '!<%= src.appFile %>',
              //'!<%= src.spec %>',
              //'!<%= src.mock %>',
              '<%= src.appFile %>'
            ]  //app.js must be last one
          ]
        }
      },

      // Inject component css into index.html
      css: {
        options: {
          transform: function(filePath) {
            //filePath = filePath.replace('/client/src/bin/client', '');
            //filePath = filePath.replace('/src', '');
            filePath = filePath.replace('/./', '');
            return '<link rel="stylesheet" href="' + filePath + '">';
          },
          starttag: '<!-- injector:css -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          '<%= src.loginHtml %>': [
            './<%= src.loginCss %>'
          ],
          '<%= src.indexHtml %>': [
            // '<%= src.vendorCss %>',    // minified kendo are embedded in html_base
            '<%= src.appCss %>',
            '<%= src.assetsCss %>',
            '<%= src.css %>',
            '<%= src.assetsSvgSpritesFile %>',
          ]
        }
      }
    },
    uglify: {
      generated: {
        options: {
          // beautify: true
        }
      }
    },
    concat: {
    },
    filerev: {
      options: {
        algorithm: 'md5',
        length: 8
      },
      images: {
        src: '<%= dist.images %>'
      }
    },
    cachebreaker: {
      prod: {
        options: {
          match: [
            'vendor.js',
            'app/app.js',
            'app/app.css',
            'app/vendor.css',
            'vendor/kendo/kendo.bootstrap.min.css',
            'vendor/kendo/kendo.common-bootstrap.min.css'
          ]
        },
        files: {
          src: ['<%= distDir %>/index.html']
        }
      }
    }
  });

  // Used for delaying livereload until after server has restarted
  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');
    if (grunt.config.get('server').livereloadConf) {
      grunt.log.ok('Livereload grunt task is waiting on port ' + JSON.stringify(grunt.config.get('server').livereloadConf));
    }
    else {
      grunt.log.ok('Livereload grunt server is off');
    }

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
    this.async();
  });

  grunt.registerTask('injectorPrep', 'copy base templates', ['copy:htmlBase'/*, 'uncss'*/]);

  grunt.registerTask('build', [
    'clean:dist',         //empty .temp folder and dist folder
    'typescript',          //build .ts files into binDir/.js and .js.map
    'less:dev',           //build .less files into less/css/.css
    //'imagemin',
    //  'svgmin',               //optimizing SVG vector graphics files.
    //'concurrent:dist',
    'replace:production',

    'ngtemplates',         //build .tpl.html files into binDir/template1.js
    'injectorPrep',       //create index.html and login.html from _base.html. create login.css
    'injector',           //Inject binDir/.js and vendorCss and appCss files into index.html (doesn't include bower) login.css into html.login
    'wiredep',           // Automatically inject Bower components .js and .css into index.html

    'useminPrepare',  //creating configuration task according to .html for concat cssmin uglify anf=d filerev
    //'autoprefixer:dist',
    'copy:dist',     //copy to publicDir :   bower_components,assets/images,assets/fonts,
    //to dist:  'package.json',all server files. Does not copy js and css files

    'concat:generated', //task generated by useminPrepare. Concatin multiple files to one
    'ngAnnotate',    //prepare .js files at .tmp/concat to uglify

    'cssmin:generated',  //task generated by useminPrepare. min css
    'uglify:generated',  //task generated by useminPrepare. uglify js
    'filerev',           //task generated by useminPrepare.
    'usemin'             //create app.js from app files on .html + vendor.js from bowerfiles +  copy files to dist/html dist/css dist/js
  ]);

  // grunt.registerTask('serve', function (target) {
  //   if (target === 'dist') {
  //     return grunt.task.run([
  //       'build',
  //       'env:all',
  //       'env:prod',
  //       'express:prod',
  //       'wait',
  //       'open',
  //       'express-keepalive'
  //     ]);
  //   }
  //
  //   if (target === 'debug') {
  //     return grunt.task.run([
  //       'clean:server',
  //       'env:all',
  //       'typescript:dev',
  //       'less:dev',
  //       'concurrent:server',
  //       'injectorPrep',
  //       'injector',
  //       'wiredep',
  //       'autoprefixer:dev',
  //       'concurrent:debug',
  //       'replace:development'
  //     ]);
  //   }
  //
  //   grunt.task.run([
  //     'clean:server',   //empty .temp folder
  //     'env:all',        //set server localConfig
  //       'concurrent:client',
  //     'replace:development',  //create bin/app/config.js file from app/config/config.js
  //    // 'ngtemplates:dev',
  //     'injectorPrep',      //create index.html and login.html from _base.html. create login.css
  //     'injector',          //Inject binDir/.js and vendorCss and appCss files into index.html (doesn't include bower) login.css into html.login
  //     'wiredep',           // Inject Bower components .js and .css into index.html
  //    // 'autoprefixer:dev',     // parse .css files from less/css and add vendor prefixes to CSS ex: from :fullscreen to :-webkit-full-screen,-moz-full-scree
  //
  //     'express:dev',         //run server app.js on port
  // //    'karma:unit',
  //     'wait',
  //     'open',
  //
  //     'watch'
  //   ]);
  // });

  grunt.registerTask('buildDev', function (target) {
    grunt.task.run([
      'clean:server',   //empty .temp folder
      'env:all',        //set server localConfig
      'concurrent:client',   //compile less .less files into less/css/.css, build .ts files into binDir/.js and .js.map,svgstore  concurrently
      //'svgstore' ,    //svgstore:create sprite svg file of symbols from every svg images in folder - moved to concurrent
      'replace:development',  //create bin/app/config.js file from app/config/config.js
      'ngtemplates',         //build .tpl.html files into binDir/template1.js
      'injectorPrep',      //create index.html and login.html from _base.html. create login.css
      'injector',          //Inject binDir/.js and vendorCss and appCss and svg symbols files into index.html (doesn't include bower) login.css into html.login
      'wiredep'           // Inject Bower components .js and .css into index.html
    ]);
  });

  grunt.registerTask('packDev', function (target) { //must be run after buildDev inorder to run un prod mode from dist folder
    grunt.task.run([
      'clean:dist',
      'useminPrepare',      // creating configuration task according to .html for concat cssmin uglify anf=d filerev
      'copy:packDev',       // copy to publicDir
      'concat:generated',   // task generated by useminPrepare. Concatin multiple files to one
      'cssmin:generated',   // task generated by useminPrepare. min css
      'ngAnnotate',         // prepare .js files at .tmp/concat to uglify
      'uglify:generated',   //task generated by useminPrepare. uglify js
      //'copy:unUglified',     // use original conact files until uglify is working ...
      // 'filerev',            //task generated by useminPrepare.
      'usemin',              // create app.js from app files on .html + vendor.js from bowerfiles +  copy files to dist/html dist/css dist/js
      'cachebreaker:prod'      //File urls will be rewritten to all.4252425.js, for example
    ]);
  });

  grunt.registerTask('serve', function (target) {
    grunt.task.run([
      'buildDev',   // build

      //    'express:dev',         //run server app.js on port
      //    'karma:unit',
      //    'wait',
      //    'open',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('test', function(target) {
    if (target === 'server') {
      return grunt.task.run([
        'env:all',
        'env:test',
        'mochaTest'
      ]);
    }

    else if (target === 'client') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'typescript:dev',
        'less:dev',
        'concurrent:test',
        'injectorPrep',
        'injector',
        'autoprefixer:dev',
        'replace:development',
        'karma'
      ]);
    }

    else if (target === 'e2e') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'typescript:dev',
        'less:dev',
        'env:test',
        'concurrent:test',
        'injectorPrep',
        'injector',
        'wiredep',
        'autoprefixer:dev',
        'replace:development',
        'express:dev',
        'protractor'
      ]);
    }

    else grunt.task.run([
        'test:server',
        'test:client'
      ]);
  });


  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);
};

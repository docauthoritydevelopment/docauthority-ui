export interface Environment {
  production: boolean;
  defaultTimeout: number;
  /**
   * When set to false, no timeout limit will apply regardless of the defaultTimeout value
   */
  useTimeout: boolean;
  dateTimeFormat: string;
  dateFormat: string;
  maximumItemsOnServerCallForExcel: number;
  maximumItemsShowOnExcel: number;
  maximumParallelServerCalls: number;

  useWebSocket?: boolean;
}

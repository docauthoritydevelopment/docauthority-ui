import {Environment} from './environment.interface';

export const environment = {
  production: true,
  defaultTimeout: 6000,
  useTimeout: false,
  useWebSocket: false,
  dateTimeFormat: 'dd-MM-yyyy HH:mm',
  dateFormat: 'dd-MM-yyyy',
  maximumItemsOnServerCallForExcel: 5000,
  maximumItemsShowOnExcel: 300000,
  maximumParallelServerCalls: 3
};

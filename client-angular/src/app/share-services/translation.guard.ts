import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UrlAuthorizationService} from '../core/services/url-authorization.service';
import {LicenseService} from "../core/services/license.service";
import {TranslateService} from "@ngx-translate/core";


@Injectable()
export class TranslationGuard implements CanActivate {

  constructor(private translateService:TranslateService) {

  }

  canActivate(nextRoute: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Promise<boolean> {
    return new Promise<boolean>((resolve: any) => {
      this.translateService.setDefaultLang('en');
      this.translateService.use('en').subscribe((res) => {
          resolve(true);
        },
        (err) => {
          console.error("Fail to use translation language");
          resolve(false);
        });
    });
  }

}

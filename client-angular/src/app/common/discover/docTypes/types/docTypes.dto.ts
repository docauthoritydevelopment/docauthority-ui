import {DTOFileTag, DTOFileTagTypeAndTags} from "@app/common/discover/tagTypes/types/tagTypes.dto";


export class DocTypeDto {
  id:number;
  name:string;
  fileTagDtos:DTOFileTag[];
  uniqueName:string;
  parentId:number;
  description:string;
  parents:number[];
  implicit:boolean;
  tagItems:any[];
  singleValue:boolean;
  fileTagTypeSettings:DTOFileTagTypeAndTags[];
  fullName:string;
}

export class DocTypeDetailsDto {
  docTypeDto:DocTypeDto;
  totalAssociations:number;
  fileAssociations:number;
  groupAssociations:number;
  explicitFolderAssociations:number;
}







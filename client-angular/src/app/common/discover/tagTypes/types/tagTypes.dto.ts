export class DTOFileTagType {
  id:number;
  name:string;
  system:boolean;
  hidden:boolean;
  tagItemsCount:number;
  description:string;
  sensitive:boolean;
  singleValueTag:boolean;
}

export class DTOFileTagTypeAndTags {
  fileTagTypeDto:DTOFileTagType ;
  fileTags:DTOFileTag[];
}

export class DTOFileTag{
  id:number;
  name:string;
  type:DTOFileTagType;
  description:string;
  implicit:boolean;
}

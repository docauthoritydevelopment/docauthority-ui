import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {DiscoverService} from './services/discover.service';
import {DocTypeDisplayComponent} from './components/discover-display-components/doc-type-display/doc-type-display.component';
import {CommonModule} from '@angular/common';
import {MarkedItemDisplayComponent} from './components/discover-display-components/marked-item-display/marked-item-display.component';
import {FunctionalRoleDisplayComponent} from './components/discover-display-components/functional-role-display/functional-role-display.component';
import {BizListDisplayComponent} from './components/discover-display-components/biz-list-display/biz-list-display.component';
import {TagDisplayComponent} from './components/discover-display-components/tag-display/tag-display.component';
import {GroupItemGridComponent} from './components/item-grids/group-item-grid/group-item-grid.component';
import {DocTypeItemGridComponent} from './components/item-grids/doc-type-item-grid/doc-type-item-grid.component';
import {NumOfFilesDisplayComponent} from './components/discover-display-components/num-of-files-display/num-of-files-display.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DiscoverExcelService} from './services/discoverExcel.service';
import {DiscoverTypesService} from './services/discover.types';
import {DiscoverFilterComponent} from './components/discover-filter/discover-filter.component';
import {MetadataDiscoverFilterComponent} from './components/discover-filter-components/metdata-discover-filter/metadata-discover-filter.component';
import {InputDiscoverFilterComponent} from './components/discover-filter-components/input-discover-filter/input-discover-filter.component';
import {TagDiscoverFilterComponent} from './components/discover-filter-components/tag-discover-filter/tag-discover-filter.component';
import {ChooseDocTypeComponent} from './popups/chooseDocType/choose-doc-type.component';
import {DocTypeChartComponent} from './popups/doc-type-chart/doc-type-chart.component';
import {TreeGridModule} from '../../shared-ui/components/tree-grid/tree-grid.module';
import {LoaderModule} from '../../shared-ui/components/loader/loader.module';
import {FilesItemGridComponent} from './components/item-grids/files-item-grid/files-item-grid.component';
import {DiscoverDetailPageComponent} from './components/discover-detail-page/discover-detail-page.component';
import {DoctypesService} from '@app/common/discover/services/doctypes.service';
import {CheckboxDiscoverFilterComponent} from "@app/common/discover/components/discover-filter-components/checkbox-discover-filter/checkbox-discover-filter.component";
import {FolderItemGridComponent} from "@app/common/discover/components/item-grids/folder-item-grid/folder-item-grid.component";
import {OwnerItemGridComponent} from "@app/common/discover/components/item-grids/owner-item-grid/owner-item-grid.component";
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {NewFilterService} from "@app/common/discover/services/new.filter.service";
import {FolderStatusItemGridComponent} from "@app/common/discover/components/item-grids/folder-status-item-grid/folder-status-item-grid.component";
import {DiscoverItemListComponent} from "@app/common/discover/components/discover-item-list/discover-item-list.component";
import {TranslationModule} from "@app/common/translation/translation.module";
import {TranslateModule} from "@ngx-translate/core";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {DepartmentItemGridComponent} from "@app/common/discover/components/item-grids/department-item-grid/department-item-grid.component";


const SHARED_UI_COMPONENTS = [DiscoverItemListComponent, DocTypeDisplayComponent,
  MarkedItemDisplayComponent, FunctionalRoleDisplayComponent, BizListDisplayComponent,
  TagDisplayComponent, GroupItemGridComponent, DocTypeItemGridComponent, NumOfFilesDisplayComponent,FolderItemGridComponent,OwnerItemGridComponent,
  DiscoverFilterComponent, MetadataDiscoverFilterComponent, CheckboxDiscoverFilterComponent, InputDiscoverFilterComponent, TagDiscoverFilterComponent,
  ChooseDocTypeComponent, DocTypeChartComponent, FilesItemGridComponent, DiscoverDetailPageComponent,FolderStatusItemGridComponent, DepartmentItemGridComponent];

@NgModule({

  imports: [
    CommonModule,
    SharedUiModule,
    TreeGridModule,
    NgbModule,
    LoaderModule,
    FormsModule,
    DaDraggableModule,
    FormsModule,
    TranslationModule,
    TranslateModule
  ],
  declarations: SHARED_UI_COMPONENTS,
  exports: SHARED_UI_COMPONENTS,
  entryComponents: [GroupItemGridComponent,
    DocTypeItemGridComponent, ChooseDocTypeComponent, DocTypeChartComponent, FilesItemGridComponent, OwnerItemGridComponent, FolderItemGridComponent,FolderStatusItemGridComponent, DepartmentItemGridComponent],
  providers: [
    DiscoverService,
    DiscoverTypesService,
    DoctypesService,
    NewFilterService,
    DiscoverExcelService]
})
export class CommonDiscoverModule {
}

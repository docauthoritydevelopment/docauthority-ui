import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from "../../../services/discover.service";
import {BaseDiscoverDTO, FilterTypeObject} from "../../../types/discover-interfaces";
import {FilterData} from "@services/filter-data";
import {EFilterOperators} from "@services/filter-types/filter-operators";
import {SingleCriteria} from "@services/filter-types/single-criteria";
import {MetadataService} from "@services/metadata.service";
import {NewFilterService} from "@app/common/discover/services/new.filter.service";
import {BaseDiscoverWrapper} from "@app/common/discover/types/base-discover-wrapper";
import {MetadataType} from "@app/common/discover/types/common-enums";



@Component({
  selector: 'metadata-filter',
  templateUrl: './metadata-discover-filter.component.html',
  styleUrls: ['./metadata-discover-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MetadataDiscoverFilterComponent  {
  _filterTypeObject: FilterTypeObject;
  mList : BaseDiscoverWrapper<BaseDiscoverDTO>[] = [];

  @Input()
  filterField: string;

  @Input()
  filterData: FilterData;

  @Output() filterChanged = new EventEmitter();


  @Input()
  set filterTypeObject(newFilterObj : FilterTypeObject) {
    this._filterTypeObject = newFilterObj;
    this.mList  = this.metadataService.getMetadataList(<MetadataType>newFilterObj.field);

    for (let count: number = 0 ; count < this.mList.length ; count++ ) {
      if (this.filterData.getSingleCriteriaFromPageFilter(this.filterField,this.mList[count].item.id)) {
        this.mList[count].selected = true;
      }
      else {
        this.mList[count].selected = false;
      }
    }

  }

  get filterTypeObject(): FilterTypeObject {
    return this._filterTypeObject;
  }

  constructor(private discoverService : DiscoverService, private metadataService : MetadataService,private newFilterService: NewFilterService) {
  }

  selectMItem(mItem:BaseDiscoverWrapper<BaseDiscoverDTO>) {
    mItem.selected = mItem.selected ? false : true;
    let sCriteria:SingleCriteria  = new SingleCriteria(null,this.filterField, mItem.item.id, EFilterOperators.equals);
    if (mItem.selected) {
      this.filterData.setPageFilters(sCriteria);
    }
    else {
      this.filterData.removeCriteriaFromPageFilter(sCriteria);
    }
    this.filterChanged.emit();
  }

}


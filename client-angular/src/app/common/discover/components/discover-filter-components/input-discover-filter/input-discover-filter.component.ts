import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from "../../../services/discover.service";
import {FilterData} from "@services/filter-data";
import {FilterTypeObject} from "@app/common/discover/types/discover-interfaces";
import {MetadataService} from "@services/metadata.service";



@Component({
  selector: 'input-filter',
  templateUrl: './input-discover-filter.component.html',
  styleUrls: ['./input-discover-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InputDiscoverFilterComponent  {
  _filterTypeObject: FilterTypeObject;

  @Input()
  filterField: string;

  @Input()
  filterData: FilterData;

  @Output() filterChanged = new EventEmitter();


  @Input()
  set filterTypeObject(newFilterObj : FilterTypeObject) {
    this._filterTypeObject = newFilterObj;
  }

  get filterTypeObject(): FilterTypeObject {
    return this._filterTypeObject;
  }

  constructor(private discoverService : DiscoverService, private metadataService : MetadataService) {
  }

}


import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {FilterTypeObject} from "@app/common/discover/types/discover-interfaces";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {FilterData} from "@app//core/services/filter-data";
import {EFilterOperators} from "@app//core/services/filter-types/filter-operators";
import {SingleCriteria} from "@app/core/services/filter-types/single-criteria";
import {BaseDiscoverWrapper} from '@app/common/discover/types/base-discover-wrapper';
import {DiscoverService} from "@app/common/discover/services/discover.service";
import {BaseDiscoverDTO} from "@app/common/discover/types/discover-interfaces";
import {MetadataService} from "@services/metadata.service";



@Component({
  selector: 'tag-filter',
  templateUrl: './tag-discover-filter.component.html',
  styleUrls: ['./tag-discover-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TagDiscoverFilterComponent  {
  _filterTypeObject: FilterTypeObject;
  tagsList:any[] = [];

  @Input()
  filterField: string;

  @Input()
  filterData: FilterData;

  @Output() filterChanged = new EventEmitter();



  @Input()
  set filterTypeObject(newFilterObj : FilterTypeObject) {
    this._filterTypeObject = newFilterObj;
    let mList: any[]  = this.metadataService.getMetadataList(<MetadataType>newFilterObj.field);
    let tagTypeMap = {};
    this.tagsList = [];
    for (let count: number = 0 ; count < mList.length ; count ++) {
      let tagTypeObject: any  = [];
      if (tagTypeMap[mList[count].item.type.id+'']) {
        tagTypeObject = tagTypeMap[mList[count].item.type.id+''];
      }
      else {
        tagTypeMap[mList[count].item.type.id+''] = tagTypeObject;
        this.tagsList.push(tagTypeObject);
      }
      if (this.filterData.getSingleCriteriaFromPageFilter(this.filterField,mList[count].item.id)) {
        mList[count].selected = true;
      }
      else {
        mList[count].selected = false;
      }
      tagTypeObject.push(mList[count]);
    }
  }

  get filterTypeObject(): FilterTypeObject {
    return this._filterTypeObject;
  }

  constructor(private discoverService : DiscoverService, private metadataService : MetadataService) {
  }

  selectMItem(mItem:BaseDiscoverWrapper<BaseDiscoverDTO>) {
    mItem.selected = mItem.selected ? false : true;
    let sCriteria:SingleCriteria  = new SingleCriteria(null,this.filterField, mItem.item.id, EFilterOperators.equals);
    if (mItem.selected) {
      this.filterData.setPageFilters(sCriteria);
      this.filterChanged.emit();
    }
    else {
      this.filterData.getPageFilter().getAllCriteriaLeafs();
      this.filterData.removeCriteriaFromPageFilter(sCriteria);
      this.filterChanged.emit();
    }
  }

}


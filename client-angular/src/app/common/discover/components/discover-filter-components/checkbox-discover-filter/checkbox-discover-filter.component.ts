import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from "../../../services/discover.service";
import {FilterData} from "@services/filter-data";
import {FilterTypeCheckBoxObject, FilterTypeObject} from "@app/common/discover/types/discover-interfaces";
import {MetadataService} from "@services/metadata.service";
import {NewFilterService} from "@app/common/discover/services/new.filter.service";



@Component({
  selector: 'checkbox-filter',
  templateUrl: './checkbox-discover-filter.component.html',
  styleUrls: ['./checkbox-discover-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CheckboxDiscoverFilterComponent  {
  _filterTypeObject: FilterTypeObject;

  @Input()
  filterField: string;

  @Input()
  filterData: FilterData;

  @Output() filterChanged = new EventEmitter();


  @Input()
  set filterTypeObject(newFilterObj : FilterTypeObject) {
    this._filterTypeObject = newFilterObj;
  }

  get filterTypeObject(): FilterTypeObject {
    return this._filterTypeObject;
  }

  constructor(private discoverService : DiscoverService, private metadataService : MetadataService,private newFilterService: NewFilterService) {
  }

  selectMItem(mItem:FilterTypeCheckBoxObject) {
    mItem.selected = mItem.selected ? false : true;
    if (mItem.children) {
      mItem.children.map((childItem:FilterTypeCheckBoxObject) => {childItem.selected = mItem.selected})
    }
  }

}


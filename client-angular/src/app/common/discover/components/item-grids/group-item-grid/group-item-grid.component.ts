import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DTOGroup} from "@app/common/discover/types/discover-interfaces";

declare let $: any;

@Component({
  selector: 'group-item-grid',
  templateUrl: './group-item-grid.component.html'
})
export class GroupItemGridComponent extends TreeGridInnerItemBaseComponent<DTOGroup> implements OnInit {


  @ViewChild('el') el: ElementRef;

  constructor() {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
  }
}

import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {I18nService} from "@app/common/translation/services/i18n-service";

declare let $: any;

@Component({
  selector: 'folder-status-item-grid',
  templateUrl: './folder-status-item-grid.component.html',
  styleUrls: ['./folder-status-item-grid.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FolderStatusItemGridComponent extends TreeGridInnerItemBaseComponent<any> implements OnInit {


  @ViewChild('el') el: ElementRef;
  folderSummary:string = '';
  filesSummary:string = '';
  filesPercentage: string = ''

  constructor(private i18nService: I18nService) {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.folderSummary =  (this.dataItem.item.crawlRunDetailsDto ? this.dataItem.item.crawlRunDetailsDto.totalProcessedFiles : 0 )+ ' files in folder, ' + this.dataItem.item.rootFolderDto.numberOfFoldersFound + ' subfolders';
  }
}

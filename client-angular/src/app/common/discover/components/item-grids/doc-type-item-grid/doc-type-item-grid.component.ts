import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DiscoverService} from '../../../services/discover.service';
import {BaseDiscoverDTO, BaseDiscoverWrapper, DTODocType} from '../../../types/discover-interfaces';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';


declare let $: any;

@Component({
  selector: 'doc-type-item-grid',
  templateUrl: './doc-type-item-grid.component.html'
})
export class DocTypeItemGridComponent extends TreeGridInnerItemBaseComponent<DTODocType> implements OnInit {
  docTypeColorId = null;


  @ViewChild('el') el: ElementRef;

  constructor(private discoverService: DiscoverService) {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.docTypeColorId = this.discoverService.getDocTypeStyleId(this.dataItem.item);
  }
}

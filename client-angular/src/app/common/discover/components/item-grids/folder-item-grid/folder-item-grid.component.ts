import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DTOFolderInfo} from "@app/common/discover/types/discover-interfaces";

declare let $: any;

@Component({
  selector: 'folder-item-grid',
  templateUrl: './folder-item-grid.component.html'
})
export class FolderItemGridComponent extends TreeGridInnerItemBaseComponent<DTOFolderInfo> implements OnInit {


  @ViewChild('el') el: ElementRef;
  folderSummary:string = '';
  filesSummary:string = '';
  filesPercentage: string = ''

  constructor() {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.folderSummary =  this.dataItem.item.numOfDirectFiles + ' files in folder, ' + this.dataItem.item.numOfSubFolders + ' subfolders';
    this.filesSummary = (this.dataItem.additional.numOfAllFiles ? this.dataItem.additional.numOfAllFiles : 0) + '/' + this.dataItem.item.numOfAllFiles+' files';
    this.filesPercentage =  (100 * this.dataItem.additional.numOfAllFiles /  this.dataItem.item.numOfAllFiles).toFixed(1) +'%'
  }
}

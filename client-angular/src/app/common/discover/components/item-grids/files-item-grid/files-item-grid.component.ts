import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DiscoverService} from '../../../services/discover.service';
import {UtilService} from '@services/util.service';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DTOFileInfo} from "@app/common/discover/types/discover-interfaces";
import {environment} from "../../../../../../environments/environment";


declare let $: any;

@Component({
  selector: 'files-item-grid',
  templateUrl: './files-item-grid.component.html'
})
export class FilesItemGridComponent extends TreeGridInnerItemBaseComponent<DTOFileInfo> implements OnInit {

  fileTypeClass: string;
  bytesStr: string;
  myEnvironment = environment;

  @ViewChild('el') el: ElementRef;

  constructor(private discoverService: DiscoverService, private utilService: UtilService) {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.fileTypeClass = this.utilService.getClassByFileType(this.dataItem.item.type);
    this.bytesStr = this.utilService.getBytesDisplayString(this.dataItem.item.fsFileSize);
  }
}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DiscoverService} from "@app/common/discover/services/discover.service";
import {DTODepartment} from "@app/common/discover/types/discover-interfaces";

declare let $: any;

@Component({
  selector: 'department-item-grid',
  templateUrl: './department-item-grid.component.html'
})
export class DepartmentItemGridComponent extends TreeGridInnerItemBaseComponent<DTODepartment> implements OnInit {
  departmentColorId = null;

  @ViewChild('el') el: ElementRef;

  constructor(private discoverService: DiscoverService) {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.departmentColorId = this.discoverService.getDepartmentStyleId(this.dataItem.item);
  }
}

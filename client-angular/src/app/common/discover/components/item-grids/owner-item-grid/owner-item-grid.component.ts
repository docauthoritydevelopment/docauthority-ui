import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DTOFolderInfo} from "@app/common/discover/types/discover-interfaces";

declare let $: any;

@Component({
  selector: 'owner-item-grid',
  templateUrl: './owner-item-grid.component.html'
})
export class OwnerItemGridComponent extends TreeGridInnerItemBaseComponent<DTOFolderInfo> implements OnInit {


  @ViewChild('el') el: ElementRef;
  ownerSummary:string = '';

  constructor() {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.ownerSummary =  this.dataItem.additional.count+ ' files';
  }
}

import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DiscoverTypeObject} from "../../types/discover-interfaces";
import {DiscoverTypesService} from "../../services/discover.types";
import {MetadataService} from "../../../../core/services/metadata.service";
import {FilterData} from "../../../../core/services/filter-data";
import {DiscoverFilterType, DiscoverType} from "@app/common/discover/types/common-enums";


@Component({
  selector: 'discover-filter',
  templateUrl: './discover-filter.component.html',
  styleUrls: ['./discover-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiscoverFilterComponent implements OnInit {
  @Input()
  discoverType:DiscoverType;

  @Input()
  filterData : FilterData;


  @Output() filterChanged = new EventEmitter<boolean>();


  FilterType = DiscoverFilterType;



  discoverTypeObject: DiscoverTypeObject = null;

  loading:boolean= false;
  constructor(private metadataService:MetadataService, private discoverTypesService: DiscoverTypesService) {
  }



  loadData(){
    this.discoverTypeObject = this.discoverTypesService.getDiscoverTypeObject(this.discoverType);
  }


  ngOnInit() {
    this.loadData();
  }

  onFilterChanged() {
    this.filterChanged.emit();
  }

}


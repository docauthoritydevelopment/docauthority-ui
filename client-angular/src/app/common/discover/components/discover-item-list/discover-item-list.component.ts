import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {TreeGridData} from "@tree-grid/types/tree-grid.type";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DiscoverService} from "@app/common/discover/services/discover.service";
import {DiscoverExcelService} from "@app/common/discover/services/discoverExcel.service";
import {DiscoverTypesService} from "@app/common/discover/services/discover.types";
import {BaseDiscoverDTO, DiscoverTypeObject} from "@app/common/discover/types/discover-interfaces";
import {DocTypeChartComponent} from "@app/common/discover/popups/doc-type-chart/doc-type-chart.component";
import {DiscoverType} from "@app/common/discover/types/common-enums";


@Component({
  selector: 'discover-item-list',
  templateUrl: './discover-item-list.component.html',
  styleUrls: ['./discover-item-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiscoverItemListComponent implements OnInit {
  _parameters: any;
  _discoverType: any;

  @Input()
  titlePrefix: string = null;

  @Input()
  inViewMode: boolean = false;

  @Input()
  hideHeader: boolean = false;


  @Input()
  set discoverType(newType : DiscoverType) {
    this.discoverTypeObject = this.discoverTypesService.getDiscoverTypeObject(newType);
    this._discoverType = newType;
  }

  get discoverType(): DiscoverType {
    return this._discoverType;
  }


  @Input()
  set parameters(newParams:any) {
    this._parameters = newParams;
    if (newParams != null) {
      this.loadData();
    }
  }

  get parameters(): any {
    return this._parameters;
  }

  loading = false;
  currentData: TreeGridData<BaseDiscoverDTO>;
  totalItems = 0;
  isExporting: boolean = false;
  discoverTypeObject: DiscoverTypeObject = null;



  @Output() listPageChanged = new EventEmitter<boolean>();
  @Output() selectedItemChanged = new EventEmitter<boolean>();
  @Output() titlePrefixClicked = new EventEmitter<boolean>();

  constructor(private modalService : NgbModal, private discoverService : DiscoverService, private discoverExcelService : DiscoverExcelService, private discoverTypesService: DiscoverTypesService) {
  }



  ngOnInit() {

  }

  onPageChanged = (pageNum) => {
    this.listPageChanged.emit(pageNum);
  };

  onItemSelected = (itemId, item) => {
    this.selectedItemChanged.emit(item);
  };

  private showLoader = (show: boolean) => {
    this.loading = show;
  };


  exportData() {
    this.isExporting = true;
    this.discoverExcelService.exportDiscoverList(this.discoverTypeObject.loadUrlSuffix, this.parameters,this.discoverType).subscribe(() =>{
      this.isExporting = false;
    })
  }


  loadData() {
    this.showLoader(true);
    this.discoverService.loadDiscoverList(this.discoverTypeObject.loadUrlSuffix, this.parameters).subscribe((data:TreeGridData<BaseDiscoverDTO>) => {
       this.showLoader(false);
       this.currentData = data;
       this.totalItems = data.totalElements;
     });
  }

  showChart() {
    this.modalService.open(DocTypeChartComponent, {centered: true}).result.then((result) => {
    }, (reason) => {

    });
  }

  clickOnTitlePrefix() {
    this.titlePrefixClicked.emit();
  }

  treeGridEvents = {
    onPageChanged: this.onPageChanged,
    onItemSelected: this.onItemSelected
  };

}


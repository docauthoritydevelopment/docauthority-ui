import {Component, Input, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from "../../../services/discover.service";
import {DTOFileTag} from "../../../types/discover-interfaces";
import {IconsConfig} from "@app/common/configuration/common-config";


@Component({
  selector: 'tag-display',
  templateUrl: './tag-display.component.html',
  styleUrls: ['./tag-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TagDisplayComponent  {
  _tagsDTOs: DTOFileTag[] = null;
  tagsColorArray = null;
  IconsConfig = IconsConfig;

  @Input()
  editable = false;

  @Input()
  set tagsDTOs(newTagsDTOs: DTOFileTag[]) {
    this._tagsDTOs = newTagsDTOs;
    if (this._tagsDTOs != null && this._tagsDTOs.length > 0 ) {
      this.tagsColorArray = [];
      for (let count = 0 ; count < this._tagsDTOs.length ; count++) {
        this.tagsColorArray.push(this.discoverService.getTagStyleId(this._tagsDTOs[count]));
      }
    }
  }

  get tagsDTOs() : DTOFileTag[] {
    return this._tagsDTOs;
  }


  constructor(private discoverService : DiscoverService) {
  }

}


import {Component, Input, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from '../../../services/discover.service';
import {DtoFunctionalRole} from '@app/common/users/model/functional-role.dto';
import {IconsConfig} from "@app/common/configuration/common-config";


@Component({
  selector: 'functional-role-display',
  templateUrl: './functional-role-display.component.html',
  styleUrls: ['./functional-role-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FunctionalRoleDisplayComponent {
  functionalRoleColorArray = null;
  IconsConfig = IconsConfig;
  dropDownItems = [{
    title: "Add to Filter",
    iconClass : "fa fa-filter"
  }, {
    title: "Remove" ,
    iconClass : "fa fa-trash-o"
  }];


  constructor(private discoverService: DiscoverService) {
  }

  _functionalRoleDTOs: DtoFunctionalRole[] = null;

  @Input()
  editable:boolean = false;

  get functionalRoleDTOs(): DtoFunctionalRole[] {
    return this._functionalRoleDTOs;
  }

  @Input()
  set functionalRoleDTOs(newFunctionalRoleDTOs: DtoFunctionalRole[]) {
    this._functionalRoleDTOs = newFunctionalRoleDTOs;
    if (this._functionalRoleDTOs && this._functionalRoleDTOs.length > 0) {
      this.functionalRoleColorArray = this._functionalRoleDTOs.map(item => this.discoverService.getFunctionalRoleStyleId(item))
    }
  }

}


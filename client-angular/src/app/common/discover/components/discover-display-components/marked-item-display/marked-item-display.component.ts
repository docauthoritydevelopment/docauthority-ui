import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseDiscoverDTO} from "../../../types/discover-interfaces";
import {IconsConfig} from "@app/common/configuration/common-config";


@Component({
  selector: 'marked-item-display',
  templateUrl: './marked-item-display.component.html',
  styleUrls: ['./marked-item-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MarkedItemDisplayComponent  {
  @Input() dataItem: BaseDiscoverDTO;
  IconsConfig = IconsConfig;


  constructor() {
  }

}


import {Component, Input, ViewEncapsulation} from '@angular/core';


@Component({
  selector: 'num-of-files-display',
  templateUrl: './num-of-files-display.component.html',
  styleUrls: ['./num-of-files-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class NumOfFilesDisplayComponent  {
  @Input()
  set dataItem(newDataItem : any) {
    this._dataItem = newDataItem;
    this.fileNumberCount = newDataItem.additional.count +' / ' + newDataItem.additional.count2;
    if (newDataItem.additional.count  != 0 && newDataItem.additional.count == newDataItem.additional.count2) {
      this.fileNumberMsg = "All at this level"
    }
  }

  _dataItem: any = null;
  fileNumberCount: string;
  fileNumberMsg: string;

  constructor() {
  }

}


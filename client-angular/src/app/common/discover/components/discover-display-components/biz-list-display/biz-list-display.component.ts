import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DiscoverService} from "../../../services/discover.service";
import {
  DTOSimpleBizListItem
} from "../../../types/discover-interfaces";
import {IconsConfig} from "@app/common/configuration/common-config";



@Component({
  selector: 'biz-list-display',
  templateUrl: './biz-list-display.component.html',
  styleUrls: ['./biz-list-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BizListDisplayComponent  {
  _bizListDTOs: DTOSimpleBizListItem[] = null;
  bizListColorArray = null;
  IconsConfig = IconsConfig;

  @Input()
  editable : boolean = false;

  @Input()
  set bizListDTOs(newBizListDTOs: DTOSimpleBizListItem[]) {
    this._bizListDTOs = newBizListDTOs;
    if (this._bizListDTOs != null && this._bizListDTOs.length > 0 ) {
      this.bizListColorArray = [];
      for (let count = 0 ; count < this._bizListDTOs.length ; count++) {
        this.bizListColorArray.push(this.discoverService.getBizListStyleId(this._bizListDTOs[count]));
      }
    }
  }

  get bizListDTOs() : DTOSimpleBizListItem[] {
    return this._bizListDTOs;
  }


  constructor(private discoverService : DiscoverService) {
  }

}


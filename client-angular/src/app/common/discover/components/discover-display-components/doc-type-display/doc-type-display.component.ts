import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {DialogService} from '@app/shared-ui/components/modal-dialogs/dialog.service';
import {IconsConfig} from "@app/common/configuration/common-config";
import {DiscoverService} from "@app/common/discover/services/discover.service";
import {MetadataService} from "@services/metadata.service";
import {BaseDiscoverWrapper} from "@app/common/discover/types/base-discover-wrapper";
import {BaseDiscoverDTO, DTODocType} from "@app/common/discover/types/discover-interfaces";
import {ChooseDocTypeComponent} from "@app/common/discover/popups/chooseDocType/choose-doc-type.component";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";


@Component({
  selector: 'doc-type-display',
  templateUrl: './doc-type-display.component.html',
  styleUrls: ['./doc-type-display.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DocTypeDisplayComponent implements OnInit{
  docTypeColorId = null;
  docTypeFullPath = '';
  @Input()
  editable = false;
  IconsConfig = IconsConfig;
  dropDownItems:DropDownConfigDto[] = [];


  constructor(private discoverService: DiscoverService, private dialogsService: DialogService, private metadataService: MetadataService) {
  }

  updateDropDownList() {
    let recentlyList = this.metadataService.getRecentlyUsedList(MetadataType.DOC_TYPE);
    this.dropDownItems = [{
      title: "Add to Filter",
      iconClass : "fa fa-filter"
    }, {
      title: "Remove" ,
      iconClass : "fa fa-trash-o"
    },{}];

    for (let count = 0; count < recentlyList.length; count++) {
      let mItem: BaseDiscoverWrapper<BaseDiscoverDTO> = this.metadataService.getMetadataItem(MetadataType.DOC_TYPE, recentlyList[count]);
      this.dropDownItems.push({
        title: mItem.item.name,
        iconClass : IconsConfig.docTypes + ' tag-' + this.discoverService.getDocTypeStyleId(<DTODocType>mItem.item)
      })
    }

    this.dropDownItems.push({
      title: 'Choose Doc Type'
    });
  }


  ngOnInit() {
    this.updateDropDownList();
  }


  _docTypeDTOs: DTODocType[] = null;

  get docTypeDTOs(): DTODocType[] {
    return this._docTypeDTOs;
  }

  @Input()
  set docTypeDTOs(newDocTypeDTOs: DTODocType[]) {
    this._docTypeDTOs = newDocTypeDTOs;
    if (this._docTypeDTOs != null && this._docTypeDTOs.length > 0) {
      this.docTypeColorId = this.discoverService.getDocTypeStyleId(this._docTypeDTOs[0]);
      this.docTypeFullPath = this.getDocTypeFullPath();
    }
    this.updateDropDownList();
  }

  getDocTypeFullPath(): string {
    let ans = '';
    if (this.docTypeDTOs !== null && this.docTypeDTOs.length > 0) {
      let goDocType: any = this.docTypeDTOs.length > 0 ? this.docTypeDTOs[0] : null;
      while (goDocType !== null && goDocType!== undefined ) {
        if (ans !== '') {
          ans = ' > ' + ans
        }
        ans = goDocType.name + ans;
        goDocType = goDocType.parentId ? this.metadataService.getMetadataItem(MetadataType.DOC_TYPE, goDocType.parentId) : null;
        if (goDocType) {
          goDocType = goDocType.item;
        }

      }
    }
    return ans;
  }

  openChooseDocTypePopup() {
    const modalInstance = this.dialogsService.openModalPopup(ChooseDocTypeComponent,
      {selectedId: this.docTypeDTOs && this.docTypeDTOs.length > 0 ? this.docTypeDTOs[0].id : null})

    modalInstance.result.then((result) => {
    }, (reason) => {
    });
  }

}


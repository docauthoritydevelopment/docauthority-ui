import {Component, ElementRef, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {DiscoverQueryParams} from "@app/features/discover/discover.route.manager";
import {DiscoverTypeObject, DTOGroup} from "@app/common/discover/types/discover-interfaces";
import {DiscoverTypesService} from "@app/common/discover/services/discover.types";
import {DiscoverType} from "@app/common/discover/types/common-enums";

@Component({
  selector: 'discover-detail-page',
  templateUrl: './discover-detail-page.component.html',
  styleUrls: ['./discover-detail-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiscoverDetailPageComponent implements OnInit {

  @ViewChild('nameInput')
  nameInput: ElementRef;

  _discoverType: any;
  _displayObject:DTOGroup;
  discoverTypeObject: DiscoverTypeObject;
  loading:boolean = false;
  innerType = null;
  filesPage: number;
  innerParams: any ;
  inEditNameMode:boolean = false;
  inViewParams: any ={};
  DiscoverType = DiscoverType;


  @Input()
  set discoverType(newType : DiscoverType) {
    this.discoverTypeObject = this.discoverTypesService.getDiscoverTypeObject(newType);
    this._discoverType = newType;
  }

  get discoverType(): DiscoverType {
    return this._discoverType;
  }

  @Input()
  set displayObject(newObject:DTOGroup) {
    this._displayObject = newObject;
    if (this.displayObject != null) {
      this.filesPage = 1;
      this.innerParams = {
        [DiscoverQueryParams.PAGE_NUM]: this.filesPage,
        [DiscoverQueryParams.BASE_FILTER_VALUE]: this.displayObject.id,
        [DiscoverQueryParams.BASE_FILTER_FIELD]: this.discoverTypeObject.baseFilterField
      }

      this.inViewParams = {
        [DiscoverQueryParams.PAGE_NUM]: 1,
        [DiscoverQueryParams.PAGE_SIZE] : 3,
        [DiscoverQueryParams.BASE_FILTER_VALUE]: this.displayObject.id,
        [DiscoverQueryParams.BASE_FILTER_FIELD]: this.discoverTypeObject.baseFilterField
      }
      this.loadData();
    }
  }

  get displayObject(): DTOGroup {
    return this._displayObject;
  }

  backToDetailsPage() {
    this.innerType = null;
  }

  changeNameEditMode(inEditMode:boolean) {
    this.inEditNameMode = inEditMode;
    if (this.inEditNameMode) {
      setTimeout(()=> {
          this.nameInput.nativeElement.focus();
      });
    }
  }

  moreFiles() {
    this.innerType = DiscoverType.FILES;
  }
  moreFolders() {
    this.innerType = DiscoverType.FOLDERS;
  }
  moreOwners() {
    this.innerType = DiscoverType.OWNERS;
  }


  constructor(private discoverTypesService: DiscoverTypesService) {
  }


  loadData() {

  }


  ngOnInit() {
  }


}


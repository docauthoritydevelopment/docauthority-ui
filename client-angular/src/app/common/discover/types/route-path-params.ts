export enum RoutePathParams {
  findId = 'findId',
  filter = 'filter',
  selectedItemId = 'selectedItemId',
  containerId = 'containerId',
  page = 'page',
  sortBy = 'sortBy',
  sortOrder = 'sortOrder',
  auditType = 'auditType',
  errorType = 'errorType',
  fileType = 'fileType',
  action = 'action',
  runStatus = 'runStatus',
  runPhase = 'runPhase',
  mediaType = 'mediaType',
  dateFrom = 'dateFrom',
  dateTo = 'dateTo',
  view = 'view'
}

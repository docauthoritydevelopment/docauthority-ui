import {DiscoverFilterType, DiscoverType, EGroupType} from './common-enums';
import {DtoFunctionalRole} from "@users/model/functional-role.dto";
import {ESystemRoleName} from "@users/model/system-role-name";


export interface BaseDiscoverWrapper<T> {
  item: T;
  count?: number;
  count2?: number;
  selected?: boolean;
}

export interface BaseDiscoverDTO {
  id: string;
  marked?: boolean
  parents?: number[]
  name?: string;
  selected?: boolean;
}

export interface DTOFileTagType extends BaseDiscoverDTO {
  system: boolean;
  hidden: boolean;
  tagItemsCount: number;
  description: string;
  sensitive: boolean;
  singleValueTag: boolean;
}


export interface DTOFileTag extends BaseDiscoverDTO {
  type: DTOFileTagType;
  description: string;
  implicit: boolean;
}


export interface DTOFileTagAssociation {
  associationTimeStamp: number;
  fileTagDto: DTOFileTag;
  fileTagSource: string;
}

export interface DTODocType extends BaseDiscoverDTO {
  uniqueName: string;
  parentId: number;
  parentUniqueName: string;  // Not recieved from server
  description: string;
  parents: number[];
  implicit: boolean;
  singleValue: boolean;
}

export interface DTODepartment extends BaseDiscoverDTO {
  uniqueName: string;
  parentId: number;
  parentUniqueName: string;  // Not recieved from server
  description: string;
  implicit: boolean;
  singleValue: boolean;
}

export interface DTOSimpleBizListItem extends BaseDiscoverDTO {
  id: string;
  businessId: string;
  bizListId: number;
  entityAliases: any[];
  state: string;
  implicit: boolean;
  type: string;
  aggregated: boolean;
}


export interface DtoSystemRole {
  name: ESystemRoleName;
  id: number;
  description: string;
  displayName: string;
  authorities: string[];
}

export interface DTOGroup extends BaseDiscoverDTO {
  groupName: string;
  numOfFiles: number;
  firstMemberPath: string;
  firstMemberName: string;
  firstMemberId: number;
  numOfHitEntities: number;
  numOfHitFiles: number;
  fileTagDtos: DTOFileTag[];
  fileTagAssociations: DTOFileTagAssociation[];
  docTypeDtos: DTODocType[];
  associatedFunctionalRoles: DtoFunctionalRole[];
  associatedBizListItems: DTOSimpleBizListItem[];
  groupType: EGroupType;
  parentGroupName: string;
  parentGroupId: string;
  rawAnalysisGroupId: string;
  hasSubgroups: boolean;
  groupProcessing: boolean;
  deleted: boolean;
  permittedOperations: DtoSystemRole[];
  parentGroup: DTOGroup;
}

export interface DtoContentMetadata {
  contentId: number;
  fileCount: number;
}

export interface DTOFileInfo extends BaseDiscoverDTO {
  fileName: string;
  type: string;
  baseName: string;
  fsFileSize: number;
  fsLastModified: number;
  fsLastAccess: number;
  creationDate: number;
  fileTagDtos: DTOFileTag[];
  associatedFunctionalRoles: DtoFunctionalRole[];
  fileTagAssociations: DTOFileTagAssociation[];
  docTypeDtos: DTODocType[];
  groupName: string;
  groupId: string;
  owner: string;
  contentMetadataDto: DtoContentMetadata;
  associatedBizListItems: DTOSimpleBizListItem[];
  bizListExtractionSummary: DTOExtractionSummary[];
  contentId: number;
  analyzeHint: string;
  permittedOperations: DtoSystemRole[];
}

export interface DTOExtractionSummary {

  id: string;
  bizListItemType: string;
  bizListId: string;
  sampleExtractions: DTOSimpleBizListItem[];
  count: number;
}

export interface FilterTypeCheckBoxObject {
  id?: any,
  name : string,
  selected? : boolean,
  children?  : FilterTypeCheckBoxObject[]
}
export interface FilterTypeObject {
  name: string,
  field: string,
  type: DiscoverFilterType,
  iconClass?: string,
  filterField?: string,
  list?: FilterTypeCheckBoxObject[]
}

export interface DiscoverTypeObject {
  loadUrlSuffix: string,
  treeGridItemComponent?: any,
  treeGridHeaderComponent?: any,
  baseFilterField?: string,
  title?: string;
  type: string;
  filtersList?: FilterTypeObject[]
}

export interface DiscoverExportTypeObject {
  type: DiscoverType,
  exportFields: {
    field?: string,
    title: string,
    parser?: any
  }[]
}

export interface DTOFolderInfo extends BaseDiscoverDTO {
  mediaItemId: string;
  path: string;
  depthFromRoot: number;
  parentFolderId: number;
  numOfSubFolders: number;
  numOfDirectFiles: number;
  numOfAllFiles: number;
  folderType: string;
  fileTagDtos: DTOFileTag[];
  fileTagAssociations: DTOFileTagAssociation[];
  docTypeDtos: DTODocType[];
  associatedFunctionalRoles: DtoFunctionalRole[];
  associatedBizListItems: DTOSimpleBizListItem[];
  rootFolderId: number;
  rootFolderLength: number;
  rootFolderNickName: string;
  permittedOperations: DtoSystemRole[];
  user: any;
}

export interface DTODepartment extends BaseDiscoverDTO {
  parentId: number;
  description: string;
  implicit: boolean;
}

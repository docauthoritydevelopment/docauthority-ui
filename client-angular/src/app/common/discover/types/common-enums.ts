export enum COMMON_ENUMS {
  foldersURL = '/api/filetree/folders/filecount',
  fileGroupsURL = '/api/group/filecount',
}

export enum  EGroupType {
  ANALYSIS_GROUP= <any>"ANALYSIS_GROUP",
  USER_GROUP= <any>"USER_GROUP",
  SUB_GROUP= <any>"SUB_GROUP",
}

export enum  DiscoverType {
  GROUPS = 'groups',
  DOC_TYPES = 'docTypes',
  FILES = 'files',
  GROUP_DETAILS = 'groupDetails',
  FOLDERS = 'folders',
  OWNERS = 'owners',
  ROOT_FOLDERS_STATUS = 'rootFoldersStatus',
  DEPARTMENTS = 'departments'
}

export enum  MetadataType {
  CREATED_DATE = 'createdDate',
  DATA_ROLE = 'dataRole',
  TAG = 'tag',
  DOC_TYPE = 'docType',
  SCHEDULE_GROUP = 'scheduleGroup',
  REGULATION = 'regulation',
  PATTERN_CATEGORIES = 'regulation',
  DATA_CANTER = 'dataCenter',
  DEPARTMENT = 'department',
  MATTER = 'matter'
}

export enum DiscoverFilterType {
  METADATA = 'metadtata',
  INPUT = 'input',
  CHECKBOX = 'checkBox',
  TAG = 'tag'
}

import {BaseDiscoverDTO} from '@app/common/discover/types/discover-interfaces';

export interface BaseDiscoverWrapper<T> {
  item: T;
  count?: number;
  count2?: number;
  selected?: boolean;
}

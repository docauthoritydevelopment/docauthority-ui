export class ScheduledOperationMetadataDto {

  id:number;


  operationType:ScheduledOperationType;

  step:number;

  description:string;

  opData:any;

  createTime:number;

  startTime:number;

  isUserOperation: boolean;

  state: ScheduledOperationState;
}

export  enum ScheduledOperationType {
  REFINEMENT_RULE_APPLY,
  CREATE_JOINED_GROUP,
  DELETE_JOINED_GROUP,
  UPDATE_JOINED_GROUP,
  ADD_TO_JOINED_GROUP,
  DEL_FROM_JOINED_GROUP,
  CREATE_DEPARTMENT_ASSOCIATIONS_FOR_ROOT_FOLDER,
  APPLY_DOC_TYPES_TAGS_TO_SOLR,
  DOC_TYPE_MOVE,
  DELETE_ROOT_FOLDER,
  RE_TRANSLATE_FILE_METADATA,
  ASSOCIATION_FOR_FILES_BY_FILTER
}


export enum ScheduledOperationState {
  NEW = 'NEW',
  ENQUEUED = 'ENQUEUED',
  IN_PROGRESS = 'IN_PROGRESS',
  FAILED = 'FAILED'
}

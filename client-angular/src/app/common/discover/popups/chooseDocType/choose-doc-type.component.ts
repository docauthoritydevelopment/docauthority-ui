import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MetadataType} from "../../types/common-enums";
import {TreeGridAdapterMapper, TreeGridData} from "../../../../shared-ui/components/tree-grid/types/tree-grid.type";
import {DocTypeItemGridComponent} from "../../components/item-grids/doc-type-item-grid/doc-type-item-grid.component";
import {BaseDiscoverDTO} from "../../types/discover-interfaces";
import {MetadataService} from "../../../../core/services/metadata.service";
import {TreeGridAdapterService} from "../../../../shared-ui/components/tree-grid/services/tree-grid-adapter.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";



@Component({
  selector: 'choose-doc-type',
  templateUrl: './choose-doc-type.component.html',
  styleUrls: ['./choose-doc-type.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ChooseDocTypeComponent implements  OnInit{
  docTypeTreeGridItemComponent = DocTypeItemGridComponent;
  currentData: TreeGridData<BaseDiscoverDTO>;
  selectedItem: any = null;
  selectedId: any = null;

  constructor(private metadataService: MetadataService, private treeGridAdapterService: TreeGridAdapterService, private activeModal: NgbActiveModal) {
  }

  closeMe() {
    this.activeModal.dismiss();
  }

  choose() {
    this.activeModal.close(this.selectedItem);
  }

  private onItemSelected = (itemId, item) => {
    this.selectedItem = item;
  };


  ngOnInit() {
    let treeMapper = new TreeGridAdapterMapper();
    treeMapper.contentProperty = null ;
    // this.currentData = this.treeGridAdapterService.adaptDiscoverList(
    //   this.metadataService.getMetadataList(MetadataType.DOC_TYPE),
    //   this.selectedId,
    //   treeMapper
    // );
  }

  treeGridEvents = {
    onItemSelected: this.onItemSelected
  };

}


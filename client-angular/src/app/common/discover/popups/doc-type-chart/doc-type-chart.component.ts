import {Component,  OnInit,  ViewEncapsulation} from '@angular/core';
import {MetadataService} from "../../../../core/services/metadata.service";
import {TreeGridAdapterService} from "../../../../shared-ui/components/tree-grid/services/tree-grid-adapter.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {MetadataType} from "../../types/common-enums";


@Component({
  selector: 'doc-type-chart',
  templateUrl: './doc-type-chart.component.html',
  styleUrls: ['./doc-type-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DocTypeChartComponent implements  OnInit{
  chartData: any[];
  chartOptions: any ;

  constructor(private metadataService: MetadataService, private treeGridAdapterService: TreeGridAdapterService, private activeModal: NgbActiveModal) {
  }

  closeMe() {
    this.activeModal.dismiss();
  }



  ngOnInit() {
    let chartData: any[]  = this.metadataService.getMetadataList(MetadataType.DOC_TYPE);
    let data = [{
      'id': 'root',
      'parent': '',
      'name': 'All Doc Types'
    }];
    for (let count = 0 ; count < chartData.length ; count++) {
      let newObj: any  = {
        'id': chartData[count].item.id+'',
        'parent': chartData[count].item.parentId? chartData[count].item.parentId+'' : 'root',
        'name': chartData[count].item.name,
        'value' : chartData[count].count2 == 0 ? null : chartData[count].count2
      };
      data.push(newObj);
    }
    this.chartOptions = {
      chart: {
        height: '100%'
      },

      title: {
        text: ''
      },
      series: [{
        type: "sunburst",
        data: data,
        allowDrillToNode: true,
        cursor: 'pointer',
        dataLabels: {
          format: '{point.name}',
          filter: {
            property: 'innerArcLength',
            operator: '>',
            value: 16
          }
        },
        levels: [{
          level: 1,
          levelIsConstant: false,
          dataLabels: {
            filter: {
              property: 'outerArcLength',
              operator: '>',
              value: 64
            }
          }
        }, {
          level: 2,
          colorByPoint: true
        },
          {
            level: 3,
            colorVariation: {
              key: 'brightness',
              to: -0.5
            }
          }, {
            level: 4,
            colorVariation: {
              key: 'brightness',
              to: 0.5
            }
          }]
      }],
      tooltip: {
        headerFormat: "",
        pointFormat: 'For Doc Type <b>{point.name}</b> there are <b>{point.value}</b> files'
      },
      credits: {
        enabled: false
      }
    }
  }


}


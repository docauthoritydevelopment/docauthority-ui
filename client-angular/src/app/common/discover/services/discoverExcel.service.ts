import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlParamsService} from '../../../core/services/urlParams.service';

import {CommonConfig} from '../../configuration/common-config';
import {ExcelService} from '../../../core/services/excel.service';
import {DiscoverType} from '../types/common-enums';
import {
  BaseDiscoverDTO,
  DiscoverExportTypeObject, DTODocType, DTOFileTag, DTOGroup,
  DTOSimpleBizListItem
} from '../types/discover-interfaces';
import {map} from 'rxjs/internal/operators';
import {DTOPagingResults} from '../../../core/types/query-paging-results.dto';
import {DtoFunctionalRole} from '../../users/model/functional-role.dto';
import {BaseDiscoverWrapper} from '@app/common/discover/types/base-discover-wrapper';

@Injectable()
export class DiscoverExcelService {

  discoverTypeExcelAttrList: DiscoverExportTypeObject[] = [
    {
      type: DiscoverType.GROUPS,
      exportFields: [{
        field: 'item.groupName',
        title: 'Group Name'
      },
        {
          field: 'item.numOfFiles',
          title: '# files'
        },
        {
          field: 'item.associatedBizListItems',
          title: 'ClientAssociations',
          parser: this.parseBizList
        },
        {
          field: 'item.docTypeDtos',
          title: 'DocTypes',
          parser: this.parseBizList
        },
        {
          field: 'item.associatedFunctionalRoles',
          title: 'Functionalroles',
          parser: this.parseFunctionalRole
        },
        {
          field: 'item.fileTagDtos',
          title: 'Tags',
          parser: this.parseTag
        },
        {
          field: 'item',
          title: 'SampleMember',
          parser: this.parseGroupSampleMember
        }]
    },
    {
      type: DiscoverType.DOC_TYPES,
      exportFields: [{
        field: 'item',
        title: 'Name',
        parser: this.parseTreeName
      },
        {
          field: 'count2',
          title: 'Number of all filtered files'
        },
        {
          field: 'item.description',
          title: 'Description'
        }]
    }
  ]

  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private excelService: ExcelService) {
  }

  getDiscoverTypeExcelObject(type: DiscoverType): DiscoverExportTypeObject {
    return this.discoverTypeExcelAttrList.find(attrList => attrList.type === type)

    // for (let count = 0; count < this.discoverTypeExcelAttrList.length; count++) {
    //   if (this.discoverTypeExcelAttrList[count].type === type) {
    //     return this.discoverTypeExcelAttrList[count];
    //   }
    // }
    // return null;
  }


  exportDiscoverList(URL,params,discoverType:DiscoverType) {
    let discoverExportTypeObject : DiscoverExportTypeObject = this.getDiscoverTypeExcelObject(discoverType);
    params  = this.urlParamsService.fixedPagedParams(params);
    params.take = CommonConfig.MAX_DATA_RECORDS_PER_REQUEST;
    params.page = 1;
    params.skip = 0;
    params.pageSize = CommonConfig.MAX_DATA_RECORDS_PER_REQUEST;
    return this.http.get(URL, {params})
      .pipe(
        map((data: DTOPagingResults<BaseDiscoverWrapper<BaseDiscoverDTO>>) => {
          const ans = [];
          for (let count = 0; count < data.content.length; count++) {
            const ansItem: any = {};
            for (let fGrow = 0; fGrow < discoverExportTypeObject.exportFields.length; fGrow++) {
              let fieldItem: any = null;
              const fieldName: string = discoverExportTypeObject.exportFields[fGrow].field;
              if (fieldName) {
                if (fieldName.indexOf('.') > -1) {
                  fieldItem = data.content[count]
                    [fieldName.substring(0, fieldName.indexOf('.'))][fieldName.substring(fieldName.indexOf('.') + 1)];
                } else {
                  fieldItem = data.content[count][fieldName];
                }
              } else {
                fieldItem = data.content[count]
              }

              if (discoverExportTypeObject.exportFields[fGrow].parser) {
                ansItem[discoverExportTypeObject.exportFields[fGrow].title] =
                  discoverExportTypeObject.exportFields[fGrow].parser(fieldItem);
              } else {
                ansItem[discoverExportTypeObject.exportFields[fGrow].title] = fieldItem;
              }
            }
            ans.push(ansItem);
          }
          this.excelService.exportAsExcelFile(ans, discoverExportTypeObject.type);
        }));
  }


  parseTreeName(item: BaseDiscoverDTO): string {
    const prefix = item.parents.map(() => ' ').join('');
    // for (let count = 0; count < item.parents.length; count++) {
    //   preffix = preffix + '  ';
    // }
    return prefix + item.name;
  }


  parseBizList(item: DTOSimpleBizListItem[]): string {
    let ans = '';
    if (item != null) {
      for (let count = 0; count < item.length; count++) {
        if (ans !== '') {
          ans = ans + ', '
        }
        ans = ans + item[count].name + ':' + item[count].state;
      }
    }
    return ans;
  }

  parseDocType(item: DTODocType[]): string {
    let ans = '';
    if (item != null) {
      for (let count = 0; count < item.length; count++) {
        if (ans !== '') {
          ans = ans + ', '
        }
        ans = ans + item[count].name;
      }
    }
    return ans;
  }

  parseFunctionalRole(item: DtoFunctionalRole[]): string {
    let ans = '';
    if (item != null) {
      for (let count = 0; count < item.length; count++) {
        if (ans !== '') {
          ans = ans + ', '
        }
        ans = ans + item[count].name;
      }
    }
    return ans;
  }

  parseTag(item: DTOFileTag[]): string {
    let ans = '';
    if (item != null) {
      for (let count = 0; count < item.length; count++) {
        if (ans !== '') {
          ans = ans + ', '
        }
        ans = ans + item[count].name;
      }
    }
    return ans;
  }

  parseGroupSampleMember(item: DTOGroup): string {
    if (item == null) {
      return '';
    }
    return item.firstMemberPath + item.firstMemberName;
  }

}

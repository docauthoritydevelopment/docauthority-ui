import {Injectable} from '@angular/core';
import {ServerPushPublisherService} from '../../../core/push-client/server-push-publisher.service';
import {environment} from '../../../../environments/environment';
import {first, skipWhile} from 'rxjs/operators';

@Injectable()
export class DoctypesService {
  constructor(private serverPushClient: ServerPushPublisherService) {
  }

  addDocTypesToFiles(
    docTypeId: string,
    displayType: string,
    fileIds: string[]
  ): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (!environment.useWebSocket) {
        reject();
      }
      this.serverPushClient.message('addDocTypeToItems', {
        displayType,
        docTypeId,
        fileIds
      });
      this.serverPushClient
        .getSubject('addDocTypeToItems')
        .pipe(
          skipWhile(data => !data),
          first()
        )
        .subscribe(data => {
          if (data.error) {
            reject(data.error);
          } else {
            resolve(data);
          }
        });
    });
  }
}

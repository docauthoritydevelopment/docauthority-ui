import { Injectable } from '@angular/core';
import {DiscoverTypeObject} from "@app/common/discover/types/discover-interfaces";
import {FilesItemGridComponent} from "@app/common/discover/components/item-grids/files-item-grid/files-item-grid.component";
import {FolderItemGridComponent} from "@app/common/discover/components/item-grids/folder-item-grid/folder-item-grid.component";
import {OwnerItemGridComponent} from "@app/common/discover/components/item-grids/owner-item-grid/owner-item-grid.component";
import {GroupItemGridComponent} from "@app/common/discover/components/item-grids/group-item-grid/group-item-grid.component";
import {DocTypeItemGridComponent} from "@app/common/discover/components/item-grids/doc-type-item-grid/doc-type-item-grid.component";
import {DiscoverFilterType, DiscoverType, MetadataType} from "@app/common/discover/types/common-enums";
import {FolderStatusItemGridComponent} from "@app/common/discover/components/item-grids/folder-status-item-grid/folder-status-item-grid.component";
import {DepartmentItemGridComponent} from "@app/common/discover/components/item-grids/department-item-grid/department-item-grid.component";

@Injectable()
export class DiscoverTypesService {

  constructor() {
  }


  discoverTypesList: DiscoverTypeObject[] = [
    {
      type: DiscoverType.FILES,
      loadUrlSuffix: '/api/file/dn',
      treeGridItemComponent: FilesItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: null ,
      title: 'Files',
      filtersList: []
    },
    {
      type: DiscoverType.GROUP_DETAILS,
      loadUrlSuffix: '/api/group/filecount',
      baseFilterField: 'groupId' ,
      title: 'Details'
    },
    {
      type: DiscoverType.ROOT_FOLDERS_STATUS,
      loadUrlSuffix: '/api/filetree/rootfolders/summary',
      treeGridItemComponent: FolderStatusItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: 'folderId' ,
      title: ''
    },
    {
      type: DiscoverType.FOLDERS,
      loadUrlSuffix: '/api/filetree/folders/node/filecount',
      treeGridItemComponent: FolderItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: 'folderId' ,
      title: 'Folders'
    },
    {
      type: DiscoverType.OWNERS,
      loadUrlSuffix: '/api/owner/filecount',
      treeGridItemComponent: OwnerItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: 'ownerId' ,
      title: 'Owners'
    },
    {
      type: DiscoverType.GROUPS,
      loadUrlSuffix: '/api/group/filecount',
      treeGridItemComponent: GroupItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: 'groupId',
      title: 'File groups',
      filtersList: [
        {
          name: "DocType Assignment",
          field: "docTypeAssignment",
          list: [{name : "Show only unassigned"}],
          type: DiscoverFilterType.CHECKBOX
        },
        {
          name: "Files Amount Range",
          field: "docTypeAssignment",
          list: [{name : "2-10"},{name : "10-50"},{name : "50-100"},{name : "100-200"},{name : "200-500"}],
          type: DiscoverFilterType.CHECKBOX
        },
        {
          name: "Patterns",
          field: "docTypeAssignment",
          list: [
            {
              name : "GDPR",
              children : [{
                name : 'SSN Pattern'
              },
                {
                  name : 'UK driving license'
                }]
            },
            {
              name : "PCI",
              children : [{
                name : 'Credit card valid'
              },
                {
                  name : 'Social security'
                }]
            }
          ],
          type: DiscoverFilterType.CHECKBOX
        },
        {
          name: "Group Name",
          field: "groupName",
          type: DiscoverFilterType.INPUT
        },
        {
          name: "Created Date",
          field: MetadataType.CREATED_DATE,
          type: DiscoverFilterType.METADATA,
          filterField : 'file.creationDate'
        },
        {
          name: "Data Role",
          field: MetadataType.DATA_ROLE,
          type: DiscoverFilterType.METADATA,
          iconClass: 'fa fa-address-book',
          filterField: 'file.functionalRoleAssociation'
        },
        {
          name: "Tag",
          field: MetadataType.TAG,
          type: DiscoverFilterType.TAG,
          filterField: 'file.tags'
        }
      ]
    },
    {
      type: DiscoverType.DOC_TYPES,
      loadUrlSuffix: '/api/doctype/filecount',
      treeGridItemComponent: DocTypeItemGridComponent,
      treeGridHeaderComponent: null,
      title: 'Doc Types'
    },
    {
      type: DiscoverType.DEPARTMENTS,
      loadUrlSuffix: '/api/department/filecount',
      treeGridItemComponent: DepartmentItemGridComponent,
      treeGridHeaderComponent: null,
      baseFilterField: 'departmentId' ,
      title: 'Departments'
    }];

  getDiscoverTypeObject(type: DiscoverType ): DiscoverTypeObject {
    for (let count = 0 ; count < this.discoverTypesList.length ; count++) {
      if (this.discoverTypesList[count].type == type) {
        return this.discoverTypesList[count];
      }
    }
    return null;
  }

}

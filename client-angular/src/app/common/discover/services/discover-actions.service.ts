import { Injectable } from '@angular/core';
import {ApplySystemUpdates, PollApplyToSolr, StartPollApplyToSolr} from '../../../core/store/core.actions';
import {DialogService} from '../../../shared-ui/components/modal-dialogs/dialog.service';
import {Store} from '@ngrx/store';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class DiscoverActionsService {

  constructor(private dialogService: DialogService, private store: Store<any>,  private http: HttpClient) { }

  applyAssignmentUpdates() {
    this.dialogService.openConfirm('Confirmation', 'Are you sure you want to apply assignment updates?').then(result => {
      if (result) {
        const applySystemUpdate = new ApplySystemUpdates();
        const pollApplySystemUpdate = new PollApplyToSolr();
        pollApplySystemUpdate.metadata.successSideActions = [() => new StartPollApplyToSolr()]
        applySystemUpdate.metadata.successAction = () => pollApplySystemUpdate;
        applySystemUpdate.metadata.errorAction = () => pollApplySystemUpdate;
        this.store.dispatch(applySystemUpdate);
      }
    });
  }

  sendMailNotificationNow() {
    return this.http.get('/api/communication/scan/email-report');
  }

  getMailNotificationSettings()  {
    return this.http.get('/api/communication/email/notification/settings');
  }

   sendHealthReportNow() {
    alert('Not implemented');
    // notificationSettingsResource.getMailNotificationSettings(function (settings: AppSettings.DtoMailNotificationConfiguration) {
    //     if (!settings.enabled) {
    //       dialogs.notify('Note', 'Mail was not sent.\n\nMail notifications is disabled.\nTo enable mails, go to Mail notifications > Settings');
    //     }
    //     else if (!settings.addresses) {
    //       dialogs.notify('Note', 'Mail was not sent.\n\nMissing destination address configuration.\nGo to Mail notifications > Settings');
    //     }
    //     else {
    //       healthNotificationResource.sendMailNotificationNow(function () {
    //           dialogs.notify('Note', 'Scan Progress Report was sent as configured');
    //         }
    //         , onError)
    //     }
    //   }
    //   , onError);

  }

}

import { Injectable } from '@angular/core';
import {FilterData} from "../../../core/services/filter-data";
import {EFilterOperators} from "../../../core/services/filter-types/filter-operators";
import {SingleCriteria} from "../../../core/services/filter-types/single-criteria";
import {AndCriteria} from "../../../core/services/filter-types/and-criteria";
import {OrCriteria} from "../../../core/services/filter-types/or-criteria";
import {CriteriaOperation} from "../../../core/services/filter-types/criteria-operation";


@Injectable()
export class NewFilterService {

  constructor() {
  }


  parseUrlToPageFilter(filterStr : string): FilterData {
    if (filterStr == null ) {
      return new FilterData();
    }
    let ans = new FilterData();
    let parsedObject = JSON.parse(filterStr);
    let sCriteria:SingleCriteria  = this.restoreFilterRecursive(parsedObject);
    ans.setPageFilters(sCriteria);
    return ans;
  }




  restoreFilterRecursive(filterField) {
    if (!filterField) {
      return null;
    }
    if (filterField.objType == 'SingleCriteria') {
      (<SingleCriteria>filterField).contextName = null;
      (<SingleCriteria>filterField).operator = filterField.operator?EFilterOperators.parse(filterField.operator.value):null;
      return new SingleCriteria((<SingleCriteria>filterField).contextName, (<SingleCriteria>filterField).fieldName, (<SingleCriteria>filterField).value, (<SingleCriteria>filterField).operator, (<SingleCriteria>filterField).displayedText,(<SingleCriteria>filterField).displayedFilterType,(<SingleCriteria>filterField).originalCriteriaOperationId);
    }
    // single item compound criteria - return the single leaf only
    if (filterField.criterias.length == 1) {
      return this.restoreFilterRecursive(filterField.criterias[0]);
    }
    // Multiple items handling ...
    var criterias = [];
    var result;

    filterField.criterias.forEach(c=> {
      var parts = this.restoreFilterRecursive(c);
      criterias.push(parts);
    });
    if (filterField.objType == 'AndCriteria') {
      result = new AndCriteria(criterias);
    }
    else if (filterField.objType == 'OrCriteria') {
      result = new OrCriteria(criterias);
    }
    return result;

  }

}

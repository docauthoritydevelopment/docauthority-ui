import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlParamsService} from '../../../core/services/urlParams.service';
import {DTOPagingResults} from '../../../core/types/query-paging-results.dto';
import {
  BaseDiscoverDTO,
  DTODepartment,
  DTODocType,
  DTOFileTag,
  DTOSimpleBizListItem
} from '../types/discover-interfaces';
import {CommonConfig} from '../../configuration/common-config';
import {ExcelService} from '../../../core/services/excel.service';
import {DtoFunctionalRole} from '../../users/model/functional-role.dto';
import {TreeGridAdapterService} from '../../../shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper, TreeGridData} from '../../../shared-ui/components/tree-grid/types/tree-grid.type';
import {Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';


@Injectable()
export class DiscoverService {

  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private excelService: ExcelService,
    private treeGridAdapterService: TreeGridAdapterService) {
  }

  getBizListStyleId(bizListItem: DTOSimpleBizListItem) {
    const maxColor: number = CommonConfig.MAX_DOC_TYPE_COLORS;
    return bizListItem.bizListId % maxColor;
  }

  getFunctionalRoleStyleId(functionalRole: DtoFunctionalRole) {
    const maxColor: number = CommonConfig.MAX_DOC_TYPE_COLORS;
    let funcRoleId = functionalRole.id;
    // if its type is string, change to a number
    if (typeof(funcRoleId) === 'string') {
      funcRoleId = parseInt(funcRoleId, 10);
    }
    return funcRoleId % maxColor;
  }

  getDocTypeStyleId(docType: DTODocType) {
    const maxColor: number = CommonConfig.MAX_DOC_TYPE_COLORS;
    if (docType.parents && docType.parents.length > 0) {
      const result = docType.parents[0] % maxColor + 1;
      return result;
    }
    return (parseInt(docType.id, 10) % maxColor + 1);
  }

  getDepartmentStyleId(department: DTODepartment) {
    const maxColor: number = CommonConfig.MAX_DEPARTMENT_COLORS;
    if (department.parentId) {
      return department.parentId % maxColor + 1;
    }
    return (parseInt(department.id, 10) % maxColor + 1);
  }

  getTagStyleId(fileTag: DTOFileTag) {
    const maxColor: number = CommonConfig.MAX_DOC_TYPE_COLORS;
    if (fileTag.type.id) {
      return parseInt(fileTag.type.id, 10) % maxColor;
    }
    return null;
  }

  loadDiscoverList(URL,params) {
    let selectedItemId = params.selectedItemId;
    params  = this.urlParamsService.fixedPagedParams(params);
    return this.http.get(URL, {params}).pipe(map((data: DTOPagingResults<BaseDiscoverDTO>) => {
         let theMapper =  new TreeGridAdapterMapper();
         theMapper.idProperty = 'rootFolderDto.id';
         return this.treeGridAdapterService.adapt(
           data,
           selectedItemId,
           theMapper
         );
       }));
  }

  updateParamsObject(targetObject: any, sourceObject: any): boolean {
    let changed: boolean = false;
    for (let variableName in sourceObject) {
      if (targetObject[variableName] != sourceObject[variableName]) {
        if (sourceObject[variableName] == null) {
          delete targetObject[variableName];
        }
        else {
          targetObject[variableName] = sourceObject[variableName];
        }
        changed = true;
      }
    }
    return changed;
  }

}

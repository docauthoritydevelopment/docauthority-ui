export enum FilterTypes {
  checkbox = 'CheckBox',
  selectBtn = 'SelectBtn',
  favoriteBox = 'FavoriteBox'
}

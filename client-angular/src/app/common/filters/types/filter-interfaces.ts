import {FilterTypes} from "@app/common/filters/types/filter-enums";

export interface FilterItem {
  type : FilterTypes,
  filterOptions : FilterOption[],
  filterTitle : string,
  filterField : string,
  shortTitle? : string,
  isDisabled? : boolean,
  filterIcon? : string,
  filterTooltip? : string,
  isCollapsed? : boolean
}


export interface FilterOption {
  value: any,
  selected?: boolean,
  displayText: string,
  tooltipTest?: string,
  children?: FilterOption[],
  parent?,
  disabled?,
}

import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import * as _ from 'lodash'
import {FilterOption} from "@app/common/filters/types/filter-interfaces";

@Component({
  selector: 'checkBoxFilter',
  templateUrl: './checkBoxFilter.template.html',
  styleUrls: ['./checkBoxFilter.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CheckBoxFilterComponent {

  @Input() filterTitle = null;
  @Input() filterField = null;
  @Input() isDisabled :boolean = false;
  @Input() collapsible:boolean = false;
  @Input() isCollapsed:boolean = false;
  isFiltered:boolean = false;

  @Input()
  set searchText(newSearch:string) {
    this._searchText = newSearch;
    this.updateAfterFilterList();
  }

  get searchText() {
    return this._searchText;
  }

  @Output() filterChanged = new EventEmitter();
  @Output() collapseChanged = new EventEmitter();


  afterFilterList = [];

  constructor() {
  }

  _filterOptions: any[];
  _searchText: string = null;





  get filterOptions(): any[] {
    return this._filterOptions || [];
  }

  @Input()
  set filterOptions(newOptions: any[]) {
    this._filterOptions = newOptions;
    this.updateAfterFilterList();
    this.isFiltered = this.checkIfContainFilter(this.afterFilterList);
  }


  checkIfContainFilter(fOptions:any[]) {
    let ans = false;
    fOptions.forEach((filterItem)=> {
      ans = ans || (filterItem.selected);
      if (!ans && filterItem.children) {
        ans = ans || this.checkIfContainFilter(filterItem.children);
      }
    });
    return ans;
  }

  updateAfterFilterList() {
    if (this.filterOptions) {
      if (this.searchText == null) {
        this.afterFilterList = this.filterOptions;
      }
      else {
        this.afterFilterList = this.filterOptions.filter((filterItem) => {
          return (filterItem.displayText.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1)
        });
      }
    }
  }


  clickOnFilterOption(event, option:FilterOption) {
    event.stopPropagation();
    option.selected = !option.selected;
    if (option.children)
    {
      option.children.map((item:FilterOption)=> {
        item.selected = option.selected;
      })
    }
    if (option.parent) {
      let foundSelected = false;
      let foundNotSelected = false;
      option.parent.children.map((fItem:FilterOption)=> {
        if (fItem.selected) {
          foundSelected = true;
        }
        else {
          foundNotSelected = true;
        }
      });
      if (foundSelected && !foundNotSelected) {
        option.parent.selected = true;
      }
      else if (!foundSelected && foundNotSelected) {
        option.parent.selected = false;
      }
    }
    this.notifyFilterChanged();
    this.isFiltered = this.checkIfContainFilter(this.afterFilterList);
  };

  notifyFilterChanged =
    _.debounce(()=> {
      this.filterChanged.emit(this.filterOptions);
    }, 0);

  changeCollapse() {
    if (this.collapsible ) {
      this.collapseChanged.emit(!this.isCollapsed);
    }
  }


  clear(fOptions:any[]) {
    fOptions.forEach((fItem:any)=> {
      fItem.selected = false;
      if (fItem.children) {
        this.clear(fItem.children);
      }
    });
  }


  clearAll() {
    this.clear(this.filterOptions);
    this.filterChanged.emit(this.filterOptions);
  }


}

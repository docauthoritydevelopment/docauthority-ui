import {NgModule} from '@angular/core';
import {CheckBoxFilterComponent} from "@app/common/filters/checkBoxFilter/checkBoxFilter.component";
import {CommonModule} from '@angular/common';
import {TranslationModule} from "@app/common/translation/translation.module";
import {DaFilterService} from "@app/common/filters/services/da-filter.service";
import {SelectBtnFilterComponent} from "@app/common/filters/selectBtnFilter/selectBtnFilter.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FilterBoxComponent} from "@app/common/filters/filterBox/filterBox.component";
import {FormsModule} from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [CommonModule,TranslationModule,NgbModule,FormsModule,NgSelectModule, FormsModule,SharedUiModule,TranslateModule],
  declarations: [CheckBoxFilterComponent,SelectBtnFilterComponent,FilterBoxComponent],
  providers : [DaFilterService],
  exports: [CheckBoxFilterComponent,SelectBtnFilterComponent,FilterBoxComponent]
})

export class FilterModule {
}

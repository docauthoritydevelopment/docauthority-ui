import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'filterBox',
  templateUrl: './filterBox.template.html',
  styleUrls: ['./filterBox.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FilterBoxComponent {

  @Input() allMsg = 'All';
  @Input() noneMsg = 'Filter:';
  @Input() noneTooltipMsg = 'Choose for filter';
  @Input() withSearch:boolean = false;
  @Output() filterChanged = new EventEmitter();
  selectedState: string;
  selectedStateTooltip: string;
  searchText: string;

  constructor(private i18nService : I18nService) {
  }

  _filterOptions: any[];

  get filterOptions(): any[] {
    return this._filterOptions || [];
  }

  @Input()
  set filterOptions(newOptions: any[]) {
    this._filterOptions = newOptions;
    this.refreshSelectedState();
  }


  onFilterChanged() {
    this.refreshSelectedState();
    this.filterChanged.emit(this.filterOptions);
  }


  refreshSelectedState = ()=>  {
    const selectedState = this.filterOptions.filter( (s) => {
      return s.selected;
    }).map( (d) => {
      if (d.value.indexOf(':')>-1) {
        return this.i18nService.translateId(d.value.substring(d.value.indexOf(':')+1));
      }
      else {
        return this.i18nService.translateId(d.displayText);
      }
    });
    this.selectedState = (selectedState.length == 0 ? this.noneMsg : selectedState.length == this.filterOptions.length ? this.allMsg : selectedState.length > 2 ? selectedState[0] + ',' + selectedState[1] + '...' : selectedState.join(', '));
    this.selectedStateTooltip = (selectedState.length == 0 ? this.noneTooltipMsg : 'Filtered by: ' + selectedState.join(', '));
  }


}

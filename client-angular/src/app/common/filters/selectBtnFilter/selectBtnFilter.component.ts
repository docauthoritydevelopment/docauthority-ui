import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {DaDropDownService} from "@shared-ui/components/da-drop-down/services/da-drop-down.service";
import {AppInjector} from "@app/app.component";

@Component({
  selector: 'selectBtnFilter',
  templateUrl: './selectBtnFilter.template.html',
  styleUrls: ['./selectBtnFilter.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SelectBtnFilterComponent {

  @Input() filterTitle = null;
  @Input() shortTitle = null;
  @Input() filterField = null;
  @Output() filterChanged = new EventEmitter();
  @Output() collapseChanged = new EventEmitter();
  @Input() collapsible:boolean = false;
  @Input() isCollapsed:boolean = false;
  @Input() withClearAll: boolean = false;

  selectIsOpen:boolean = false;
  needToNotify:boolean = false;
  daDropDownService: DaDropDownService = null;
  constructor() {
    this.daDropDownService = AppInjector.get(DaDropDownService);
  }

  _filterOptions: any[];
  _selectedItems:any[] = [];

  get filterOptions(): any[] {
    return this._filterOptions || [];
  }

  @Input()
  set filterOptions(newOptions: any[]) {
    this._filterOptions = newOptions;
    this.updateSelectedItems();
  }

  updateSelectedItems() {
    if (this._filterOptions ) {
      this._selectedItems = [];
      this._filterOptions.forEach((filterObj) => {
        if (filterObj.selected) {
          this._selectedItems.push(filterObj);
        }
      });
    }
  }

  onCloseSelect() {
    this.daDropDownService.onDaDropDownClose();
    this.selectIsOpen = false;
    if (this.needToNotify) {
      this.filterChanged.emit(this.filterOptions);
      this.needToNotify = false;
    }
  }

  clearAll() {
    this.filterOptions =[];
    this.filterChanged.emit(this.filterOptions);
    this.needToNotify = false;
  }

  onOpenSelect() {
    this.daDropDownService.onDaDropDownOpen();
    this.needToNotify = false;
    this.selectIsOpen = true;
  }

  set selectedItems(newItems: any[]) {
    this._selectedItems = newItems ? newItems : [];
    this._filterOptions.map(item => item.selected=false);
    this._selectedItems.map((selectItem)=> {
      selectItem.selected = true;
    });
    if (this.selectIsOpen) {
      this.needToNotify = true;
    }
    else {
      this.filterChanged.emit(this.filterOptions);
    }
  }

  get selectedItems():any[] {
    return this._selectedItems;
  }
  onFilterChanged() {
    this.updateSelectedItems();
    this.filterChanged.emit(this.filterOptions);
  }

  changeCollapse() {
    if (this.collapsible ) {
      this.collapseChanged.emit(!this.isCollapsed);
    }
  }

}

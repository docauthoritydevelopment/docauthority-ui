import {ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ServerExportsTypes} from "@app/core/types/serverExport.types";
import {CommonConfig} from "@app/common/configuration/common-config";
import {FileTypes} from "@app/core/types/file.types";

@Component({
  selector: 'advanced-export-dialog',
  templateUrl: './advancedExportDialog.component.html',
  styleUrls: ['./advancedExportDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedExportDialogComponent implements OnInit {

  data: {
    type: ServerExportsTypes ,
    exportParams : any ,
    suggestItemNumber : number,
    userParams : any
  } = null;
  selectOption = 'all';
  rangeErrorMsg=null;
  recordsNumber: number = null;
  dataSizeReachTrashold: boolean = false;
  CommonConfig = CommonConfig;
  activeTab = 'general';
  showMe:boolean = false;
  // editName = false;




  SELECT_OPTIONS={
    ALL : 'all',
    RANGE : 'range'
  }

  constructor(private activeModal: NgbActiveModal,  private ref: ChangeDetectorRef,) {
    ref.detach();
  }

  setActiveTab(newTab) {
    this.activeTab = newTab;
  }

  ngOnInit() {
    this.showMe = true;
    this.ref.reattach();
    this.data.userParams.downloadWhenReady = "true";
    if (this.data.userParams.fileType == FileTypes.msExcel && this.data.userParams.itemsNumber > CommonConfig.MAX_EXCEL_RECORDS_IN_FILE) {
      this.dataSizeReachTrashold = true;
      this.selectOption = this.SELECT_OPTIONS.RANGE;
    }
    else if (this.data.suggestItemNumber){
      this.selectOption = this.SELECT_OPTIONS.RANGE;
      this.recordsNumber = Number(this.data.suggestItemNumber);
    }
  }

  runExport() {
    this.checkRecordsNumber();
    if (this.selectOption != this.SELECT_OPTIONS.RANGE || !this.rangeErrorMsg ) {
      if (this.selectOption == this.SELECT_OPTIONS.ALL) {
        if (this.data.userParams.itemsNumber) {
          this.data.exportParams.pageSize = this.data.userParams.itemsNumber;
          this.data.exportParams.take = this.data.userParams.itemsNumber;
        }
        this.activeModal.close({
          exportParams: this.data.exportParams,
          userParams : this.data.userParams
        });
      }
      else {
        this.checkRecordsNumber();
        if (!this.rangeErrorMsg) {
          this.data.exportParams.page = 1;
          this.data.exportParams.pageSize = this.recordsNumber;
          this.data.exportParams.take = this.recordsNumber;
          this.data.exportParams.skip = 0;
          this.activeModal.close({
            exportParams: this.data.exportParams,
            userParams : this.data.userParams
          });
        }
      }
    }
  }

  close() {
    this.activeModal.close(null);
  }

  checkRecordsNumber = ()=>  {
    if (!this.recordsNumber || isNaN(this.recordsNumber)) {
      this.rangeErrorMsg="Value should be valid number"
    }
    else if (this.data.userParams.itemsNumber < this.recordsNumber)
    {
      this.rangeErrorMsg = "You can not exceed the total number of records   ("+ this.data.userParams.itemsNumber+")";
    }
    else if (this.recordsNumber < 1 || (Math.floor(this.recordsNumber) != this.recordsNumber))
    {
      this.rangeErrorMsg = "Value should be positive integer";
    }
    else {
      this.rangeErrorMsg = null;
    }
  }

}

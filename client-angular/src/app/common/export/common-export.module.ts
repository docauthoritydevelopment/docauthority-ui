import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {FormsModule} from "@angular/forms";
import {CommonChartsModule} from "@app/common/charts/common-charts.module";
import {AdvancedExportDialogComponent} from "@app/common/export/popups/advancedExportDialog/advancedExportDialog.component";
const SHARED_UI_COMPONENTS = [AdvancedExportDialogComponent];


@NgModule({

  imports: [
    CommonModule,
    SharedUiModule,
    NgbModule,
    DaDraggableModule,
    FormsModule,
    CommonChartsModule
  ],
  declarations: SHARED_UI_COMPONENTS,
  exports: SHARED_UI_COMPONENTS,
  entryComponents: []
})
export class CommonExportModule {
}

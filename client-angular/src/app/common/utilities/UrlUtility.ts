import * as urlParse from 'url-parse';
import {UrlObject} from 'url';


export class UrlUtility {
  static parse(url: string, parseQuery = true): UrlObject {
    return urlParse(url, parseQuery);
  }

  static parseUrlQueryString(str): any {
    return (str).replace('?', '&').split('&').map(function (keyValuePair) {
      return keyValuePair = keyValuePair.split('='), this[keyValuePair[0]] = keyValuePair[1], this
    }.bind({}))[0];
  }
}

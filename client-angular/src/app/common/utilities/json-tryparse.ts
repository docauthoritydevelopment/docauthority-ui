/**
 * This class is a function library for JSON
 */
export class JSONUtilities {

  /**
   * Tries to parse a JSON string, resolves if succeeded, rejects otherwise
   * @param {string} jsonString
   * @returns {Promise<any>}
   */
  static tryParse(jsonString: string): Promise<any> {
    return new Promise((resolve, reject) => {
        try {
          const result = JSON.parse(jsonString);

          resolve(result);
        } catch (error) {
          reject({message: `Cannot parse string`, originalString: jsonString, error: error});
        }

      }
    );
  }

  static tryParseCollection(jsonStrings: string[]): Promise<any> {
    return new Promise(resolve => {
      const result = [];
      jsonStrings.forEach(jsonString => {
          try {
            const jsonResult = JSON.parse(jsonString);
            result.push(jsonResult)
          } catch(err) {
            // Do nothing
          }
        }
      );
      resolve(result);
    });
  }


}




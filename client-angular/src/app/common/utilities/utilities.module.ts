import {NgModule} from '@angular/core';
import {NestedProperties} from './nested-properties.service';
import {UrlUtility} from './UrlUtility';


@NgModule({
  providers: [NestedProperties, UrlUtility]
})
export class UtilitiesModule {

}

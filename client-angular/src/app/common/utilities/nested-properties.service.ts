import { Injectable } from '@angular/core';
import * as _ from 'lodash';
declare let $: any;

@Injectable()
export class NestedProperties {
  /**
   *
   * @param object
   * @param property
   * @returns {any}
   */
  public getVal(object, property) {
    if (_.isObject(object)) {
      const split = property.split('.');
      return split.reduce(function (obj, prop) {
        return obj && obj[prop];
      }, object);
    }
    return null;
  }
}

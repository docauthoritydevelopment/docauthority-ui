/**
 * Flatten matrix to an array
 * @param {TParent[]} source array
 * @param {(parentItem: TParent) => TChild[]} selector
 * @returns {TChild[]}
 */
export function flatMap<TParent, TChild>(source: TParent[], selector: (parentItem: TParent) => TChild[]): TChild[] {
  const result: TChild[] = [];
  if (!source) {
    return result;
  }
  source.forEach(item => result.push(...selector(item)));

  return result;
}

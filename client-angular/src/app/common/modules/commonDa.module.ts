import {NgModule} from '@angular/core';
import {NewFilterService} from '@app/common/discover/services/new.filter.service';


@NgModule({

  imports: [],
  declarations: [],
  exports: [],
  providers: [
    NewFilterService
  ]
})
export class CommonDaModule {
  constructor() {
  }
}

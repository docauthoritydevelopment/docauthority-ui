import {NgModule, ModuleWithProviders} from '@angular/core';
import {MainLoaderService} from "@services/main-loader.service";


@NgModule({

  imports: [],
  declarations: [],
  exports: [],
  providers: [
    MainLoaderService
  ]
})
export class MainLoaderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MainLoaderModule,
      providers: [MainLoaderService]
    }
  }
}

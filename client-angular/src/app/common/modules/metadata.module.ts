import {NgModule, ModuleWithProviders} from '@angular/core';
import {MetadataService} from "@services/metadata.service";


@NgModule({

  imports: [],
  declarations: [],
  exports: [],
  providers: [
    MetadataService
  ]
})
export class MetadataModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MetadataModule,
      providers: [MetadataService]
    }
  }
}

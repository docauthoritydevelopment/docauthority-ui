export enum ActiveScanMode {
  ROOT_FOLDERS = 'rootFolders',
  SCHEDULE_GROUPS = 'scheduleGroups',
  SCAN_HISTORY = 'scanHistory'
}

export enum DirectoryExistStatus {
  YES = "YES",
  NO = "NO",
  UNKNOWN = "UNKNOWN"
}


export enum ERunStatus{
  STOPPED = "STOPPED",
  FAILED = "FAILED",
  RUNNING = "RUNNING",
  NA = "NA",
  OK = "OK",
  FINISHED_SUCCESSFULLY = "FINISHED_SUCCESSFULLY",
  FINISHED_WITH_WARNINGS = "FINISHED_WITH_WARNINGS",
  STOPPING = "STOPPING" ,
  PAUSED = "PAUSED",
  PENDING = "PENDING",
  SUSPENDED = "SUSPENDED",
  NEW = "NEW",
}
export enum EJobState
{
  NEW= <any>"NEW",
  IN_PROGRESS= <any>"IN_PROGRESS",
  PAUSED= <any>"PAUSED",
  DONE= <any>"DONE",
  READY= <any>"READY",
  WAITING= <any>"WAITING",
  PENDING= <any>"PENDING",
  RUNNING = <any>"RUNNING"
}

export enum EJobCompletionStatus {
  UNKNOWN = <any>"UNKNOWN",
  OK = <any>"OK",
  CANCELLED = <any>"CANCELLED",
  ERROR = <any>"ERROR",
  TIMED_OUT = <any>"TIMED_OUT",
  DONE_WITH_ERRORS = <any>"DONE_WITH_ERRORS",
  SKIPPED = <any>"SKIPPED",
  STOPPED = <any>"STOPPED"
}

export enum EJobType {
  UNKNOWN = <any>"UNKNOWN",
  SCAN = <any>"SCAN",
  SCAN_FINALIZE = <any>"SCAN_FINALIZE",
  INGEST = <any>"INGEST",
  INGEST_FINALIZE = <any>"INGEST_FINALIZE",
  OPTIMIZE_INDEXES = <any>"OPTIMIZE_INDEXES",
  ANALYZE = <any>"ANALYZE",
  ANALYZE_FINALIZE = <any>"ANALYZE_FINALIZE",
  EXTRACT = <any>"EXTRACT",
  EXTRACT_FINALIZE = <any>"EXTRACT_FINALIZE",
  LABELING = <any>"LABELING",
}


export enum ECrawlerPhaseBehavior {
  ONCE = <any>"ONCE",
  EVERY_ROOT_FOLDER = <any>"EVERY_ROOT_FOLDER",
}

export enum EScheduleConfigType {
  WEEKLY = <any>"WEEKLY",
  DAILY = <any>"DAILY",
  HOURLY = <any>"HOURLY",
  CUSTOM = <any>"CUSTOM",
}


export enum EDayOfTheWeek {
  SUN = <any>"SUN",
  MON = <any>"MON",
  TUE = <any>"TUE",
  WED = <any>"WED",
  THU = <any>"THU",
  FRI = <any>"FRI",
  SAT = <any>"SAT",
}

export enum  EMediaType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  FILE_SHARE = <any>"0",
  SHARE_POINT = <any>"2",
  ONE_DRIVE = <any>"4",
  BOX = <any>"1",
  EXCHANGE_365 = <any>"5"
}

export enum  EFileType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  msWord= <any>"WORD",
  pdf= <any>"PDF",
  text= <any>"TEXT",
  msExcel= <any>"EXCEL",
  csv= <any>"CSV",
  other= <any>"OTHER",
  scannedPDF= <any>"SCANNED_PDF",
  package= <any>"PACKAGE",
}

export enum  STORE_LOCATION {
  UNDEFINED = 'UNDEFINED',
  LOCAL_DATACENTER = 'LOCAL_DATACENTER',
  REMOTE_DATACENTER = 'REMOTE_DATACENTER',
  CLOUD_FILESHARE = 'CLOUD_FILESHARE',
  CLOUD_SAAS = 'CLOUD_SAAS',
  CLOUD_PASS = 'CLOUD_PASS',
  OTHER = 'OTHER'
}

export enum  STORE_PURPOSE {
  PUBLIC = 'PUBLIC',
  DEPARTMENT = 'DEPARTMENT',
  PERSONAL = 'PERSONAL',
  UNDEFINED = 'UNDEFINED',
  IT = 'IT'
}

export enum  STORE_SECURITY {
  CLEAR = 'CLEAR',
  ENCRYPTED = 'ENCRYPTED'
}

export enum SCAN_OPERATION {
  FULL = 'scan,ingest,analyze,extract',
  ONLY_EXTRACT = 'extract',
  COMPLETE_NO_EXTRAT = 'ingest,analyze',
  ONLY_SCAN = 'scan',
  ONLY_INGEST = 'ingest',
  ONLY_ANALYZE = 'analyze',
  ONLY_SCAN_WITH_EXTRACT = 'ingest,analyze,extract'
}



export enum ERunType {
  ROOT_FOLDER= <any>"ROOT_FOLDER",
  BIZ_LIST= <any>"BIZ_LIST",
  SCHEDULE_GROUP= <any>"SCHEDULE_GROUP",
}

export enum EFilterOperators {
  equals = 'eq',
  notEquals = 'neq',
  contains = 'contains',
  doesNotContain = 'doesnotcontain',
  greaterThen = 'ge',
  lessThen = 'lt'
}

export enum SCAN_CONFIG {
  max_data_records_in_dialog_to_scroll = 50,
  displayed_items_when_more_then_max = 38
}

export enum SCAN_SERVER_PARAMS {
  STATE = 'states'
}

export enum EPauseReason {
  USER_INITIATED = 'USER_INITIATED',
  SOLR_DOWN = 'SOLR_DOWN',
  NO_AVAILABLE_MEDIA_PROCESSOR = 'NO_AVAILABLE_MEDIA_PROCESSOR',
  SYSTEM_INITIATED = 'SYSTEM_INITIATED',
  LICENSE_EXPIRED = 'LICENSE_EXPIRED',
  SYSTEM_ERROR = 'SYSTEM_ERROR'
}

export enum SCAN_STRATEGY {
  FAIR = 'FAIR',
  AUTO_EXCLUSIVE = 'AUTO_EXCLUSIVE',
  INGEST_ONLY = 'INGEST_ONLY',
  ANALYZE_ONLY = 'ANALYZE_ONLY'
}

export enum EBizListItemType {
  CONSUMERS = 'CONSUMERS',
  CLIENTS = 'CLIENTS',
  EMPLOYEES = 'EMPLOYEES',
  PROJECTS_SERVICES = 'PROJECTS_SERVICES',
  SIMPLE = 'SIMPLE'
}

export enum ClaComponentType {
  MEDIA_PROCESSOR = 'MEDIA_PROCESSOR',
  BROKER = 'BROKER',
  ZOOKEEPER = 'ZOOKEEPER',
  SOLR_CLAFILE = 'SOLR_CLAFILE',
  SOLR_SIMILARITY = 'SOLR_SIMILARITY',
  APPSERVER = 'APPSERVER',
  DB= 'DB',
  BROKER_DC = 'BROKER_DC',
  SOLR_NODE = 'SOLR_NODE',
  BROKER_FC = 'BROKER_FC',
  SOLR_SIM = 'SOLR_SIM',
  SOLR_ROOT_NODE = 'SOLR_ROOT_NODE',
  DATA_CENTER = 'DATA_CENTER',
}

export enum ComponentState {
  OK = 'OK',
  STOPPED = 'STOPPED',
  SHUTTING_DOWN = 'SHUTTING_DOWN',
  STARTING = 'STARTING',
  FAULTY = 'FAULTY',
  REMOVED = 'REMOVED',
  PENDING_INIT = 'PENDING_INIT',
  UNKNOWN = 'UNKNOWN',

  NA = 'NA'   // client side enum
}

import {
  ECrawlerPhaseBehavior, EDayOfTheWeek, EFileType,
  EJobCompletionStatus,
  EJobState,
  EJobType, EMediaType, ERunType, ERunStatus,
  EScheduleConfigType, EPauseReason, DirectoryExistStatus, EBizListItemType, ClaComponentType, ComponentState
} from "./scanEnum.types";
import {DTOPagingResults} from "../../../core/types/query-paging-results.dto";
import {TreeGridData} from "@tree-grid/types/tree-grid.type";

export interface PhaseDetailsDto {
  name: string;
  jobType: EJobType;
  phaseCounter: number;
  phaseMaxMark: number;
  phaseStart: number;
  phaseLastStopTime: number;
  pauseDuration: number;
  phaseError: string;
  failedTasksCount: number;
  jobState: EJobState;
  stateDescription: string;
  jobId: number;
  tasksPerSecRunningRate: number;
  tasksPerSecRate: number;
  runningRateDurationInMsc: number;
  completionStatus: EJobCompletionStatus;
  taskRetriesCount: number;
  bizListId: number;
  runningPhaseElapsedTime:number; //Remove this, this is UI only

}

export interface ScheduleGroupDto {
  id: number;
  name: string;
  analyzePhaseBehavior: ECrawlerPhaseBehavior;
  rootFolderIds: number[];
  scheduleConfigDto: ScheduleConfigDto;
  active: boolean;
  cronTriggerString: string;
  missedScheduleTimeStamp: number;
  schedulingDescription: string;
  lastRunId: number;
}


export interface  ScheduleGroupSummaryInfoDto {
  scheduleGroupDto: ScheduleGroupDto;
  runStatus: ERunStatus ;
  missedScheduleTimeStamp : number;
  nextPlannedWakeup : number;
  lastCrawlRunDetails: CrawlRunDetailsDto;
}

export interface CrawlRunDetailsDto {
  id: number;
  scanStartTime : number;
  scanEndTime : number;
  runOutcomeState : ERunStatus;
  numberOfFoldersFound : number;
  totalProcessedFiles : number;
  processedPdfFiles : number;
  processedWordFiles : number;
  processedExcelFiles : number;
  processedOtherFiles : number;
  stateString : string;
  runType : ERunType;
  newFiles: number;
  updatedContentFiles: number;
  updatedMetadataFiles: number;
  deletedFiles : number;
  scanErrors : number;
  scanProcessingErrors : number;
  manual : boolean;
  accessible : boolean;
  rootFolderDto: RootFolderDto;
  scanFiles: boolean;
  ingestFiles : boolean;
  ingestedRootFolders: number[];
  analyzedRootFolders : number[];
  analyzeFiles: boolean;
  scheduleGroupId : number;
  scheduleGroupName: string;
  hasChildRuns : boolean;
  parentRunId: number;
  pauseReason: EPauseReason;
  gracefulStop: boolean;

  jobs?: PhaseDetailsDto[]; // Specific client side attribute
}



export interface   ScheduleConfigDto {
  scheduleType:EScheduleConfigType;
  hour:number;
  minutes:number;
  every:number;
  daysOfTheWeek:EDayOfTheWeek[];
  customCronConfig:string;
}

export interface RootFolderRunSummaryInfo{
  rootFolderSummaryInfo:RootFolderSummaryInfo;
  jobs:PhaseDetailsDto[];
  currentJob: PhaseDetailsDto;
}

export class  RootFolderTreeGridData<T> extends  TreeGridData<T>{
  stateSummaryInfo : RootFoldersAggregatedSummaryInfo
}

export interface  RootFolderDto {
  id: number;
  lastRunId: number;
  path: string;
  realPath: string;
  docStoreId: number;
  numberOfFoldersFound: number;
  folderExcludeRulesCount: number;
  scannedFilesCountCap: number;
  scannedMeaningfulFilesCountCap: number;
  storeLocation: string;
  storePurpose: string;
  storeSecurity: string;
  extractBizLists: boolean;
  mediaType: EMediaType;
  rescanActive: boolean;
  description: string;
  fileTypes: EFileType[];
  mediaConnectionDetailsId: number;
  mediaConnectionName: string;
  nickName: string;
  customerDataCenterDto: CustomerDataCenterDto;
  reingestTimeStampMs: number;
  reingest: boolean;
  firstScan:boolean;
  isAccessible:DirectoryExistStatus;
  mailboxGroup: any;
  shareFolderPermissionError:boolean;
}

export class CustomerDataCenterDto {
  id: number;
  location: string;
  name: string;
  description: string;
  createdOnDB: number;
  defaultDataCenter: boolean;
}

export interface  ParamOption<T>
{
  id:any;
  value:T;
  display:any;
  subdisplay:any;
  title?:any;
}
export interface ParamActiveItem<T>
{
  active:boolean;
  item:ParamOption<T>;
}


export interface  MicrosoftConnectionDetailsDtoBase  extends MediaTypeConnectionDetails {
  sharePointConnectionParametersDto:SharePointConnectionParametersDto;
  type:string;
}

export interface  SharePointConnectionDetailsDto extends MicrosoftConnectionDetailsDtoBase  {

}
export interface OneDriveConnectionDetailsDto extends MicrosoftConnectionDetailsDtoBase  {

}

export interface SharePointConnectionParametersDto {
  username:string;
  url:string;
  password:string;
  domain:string;
}

export interface MediaTypeConnectionDetails {
  id:number;
  name:string;
  username:string;
  url:string;
  mediaType:EMediaType;
  connectionParametersJson: string;
}

export interface BoxConnectionDetails  extends MediaTypeConnectionDetails {
  jwt: string;
  type:string;
}


export interface DtoFolderExcludeRule {
  id?: number;
  matchString: string;
  rootFolderId: string;
  operator: string;
  disabled: boolean;
}

export interface filterChangedOutput{
  fields: string[],
  filterOptions: any[]
}


export interface breadcrumbItem{
  title : string,
  clickFunc? : any;
}


export interface RunSummaryInfoView {
  id: any;
  elapsedTime:number;
  startTime:number;
  endTime:number;
  isRunning:boolean;
  nickName:string;
  jobName:string;
  jobType:string;
  runStatus:ERunStatus;
  isManualRun:boolean;
  rootFolderId:number;
  scheduleGroupName:string;
  scheduleGroupId:number;
  currentPhaseProgressCounter:number;
  currentPhaseProgressPercentage:number;
  currentPhaseProgressFinishMark:number;
  estimatedPhaseFinish:number;
  tasksPerSecRate:number;
  tasksPerSecRunningRate:number;
  tasksPerSecRateString:string;
  tasksPerSecRunningRateString:string;
}


export interface RootFoldersAggregatedSummaryInfo {
  failedRootFoldersCount?: number
  newRootFoldersCount?: number
  pausedRootFoldersCount?: number
  runningRootFoldersCount?: number
  scannedRootFoldersCount?: number
  stoppedRootFoldersCount?: number
  suspendedRootFoldersCount?: number
  totalRootFoldersCount?: number
}

export interface  ActiveScanQueryResult {
  rootFoldersData : DTOPagingResults<RootFolderRunSummaryInfo>;
  stateSummaryInfo : RootFoldersAggregatedSummaryInfo;
}


export interface RootFolderSummaryInfo {
  crawlRunDetailsDto: CrawlRunDetailsDto;
  rootFolderDto: RootFolderDto;
  runStatus: ERunStatus;
  scheduleGroupDtos: ScheduleGroupDto[];
  accessible: boolean;
  customerDataCenterReady: boolean;
  accessibleUnknown: boolean;

  jobs: PhaseDetailsDto[]; //specific UI parameter
  runningPhase?: PhaseDetailsDto; //specific UI parameter
}


export interface BizListScheduleRunStatus {
  lastRunTime?: number;
  nextRunTime?: number;
  everyInMillis?: number;
  schedulingFlagOn?: boolean;
}

export interface BizListSummaryInfo {
  bizListDto: BizListDto;
  crawlRunDetailsDto : CrawlRunDetailsDto;
  itemsCount: number;
  activeItemsCount: number;

  runJob:PhaseDetailsDto; //specific UI parameter
}

export interface BizListDto {
  id: number;
  name: string;
  description: string;
  bizListItemType: EBizListItemType;
  template: string;
  lastSuccessfulRunDate: number;
  active: boolean;
  extractionSessionId: number;
  creationTimeStamp: number;
  lastUpdateTimeStamp: number;
}

export class  ClaComponentSummaryDto {
  claComponentDto : ClaComponentDto;
  extraMetrics : string | any;
  statusTimestamp : number;
  driveData: string;

  mainTitleBubble: string;
  mainTitle:string;
  subTitle: string;
  isAppServer:boolean;
  optimizeIndexesRunning: boolean;
  isSolrRootNode: boolean;
  diskSizeWarning: boolean;
  isDataCenter: boolean;
  isSolrNode: boolean;
  isMediaProcessor: boolean;
  isBrokerInfo: boolean;
  isBroker: boolean;
  isBrokerRC: boolean;

  lastResponseElapsedTimeSpan:number;  //client side parameters
  lastStateChangeElapsedTimeSpan:number;
  elementId: number;

}

export class  ClaComponentDto {
  claComponentType : ClaComponentType;
  componentType : string;
  instanceId : string;
  location : string;
  externalNetAddress : string;
  localNetAddress : string;
  createdOnDB : number;    // Time when was added to the system
  active : boolean;
  state : ComponentState;
  stateChangeDate : number;
  lastResponseDate : number;
  lastStatusId : number;
  customerDataCenterDto : CustomerDataCenterDto;

  id:number;  //client parameter
}

export class ClaComponentStatusDto {
  id: number;                // Optional
  claComponentId : number;
  updatedOnDb : number;
  timestamp : number;
  componentType : ClaComponentType;
  componentState : ComponentState;
  extraMetrics : string | any;
  driveData : string | any;


  analyzedTaskRequests:number;    // next attributes are client side attributes
  analyzedTaskRequestsFinished:number;
  analyzedTaskRequestFailure:number;
  ingestRunningThroughput:number;
  averageIngestThroughput:number;
  analyzedRunningThroughput:number;
  averageAnalyzedThroughput:number;
  processCpuLoad:number;
  systemCpuLoad:number;
  totalPhysicalMemorySize:number;
  freePhysicalMemorySize:number;
  usedPhysicalMemorySize: number;
  memoryPercentage: number;
}

import {DTOPagingResults} from "../../../core/types/query-paging-results.dto";
import {DateRangePointType, TimePeriod} from "@app/common/settings/types/dateFilters.enums";

export class DateRangePartitionDto {
  id : number;
  name : string;
  description : string;
  continuousRanges : boolean;
  dateRangeItemDtos : DateRangeItemDto[];
  system : boolean;
}


export class DateRangeItemDto {
  id : number;
  name : string;
  start : DateRangePointDto; // From
  end : DateRangePointDto; // To
  solrQuery : string;
  totalCount : number;


  isInValid:boolean;  //UI parameter
}


export class DateRangePointDto {
  absoluteTime: number;
  relativePeriod: TimePeriod;
  relativePeriodAmount: number
  type: DateRangePointType;
}

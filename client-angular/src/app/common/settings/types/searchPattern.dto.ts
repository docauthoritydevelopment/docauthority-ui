import {PatternType} from "@app/common/settings/types/searchPattern.enums";

export class TextSearchPatternDto {
  id : number;
  name : string;
  pattern : string;
  validatorClass : string;
  description : string;
  active : boolean;
  patternType : PatternType;
  categoryId: number;
  categoryName: string;
  subCategoryId: number;
  subCategoryName: string;
  regulationIds: number[];
  regulationStr:string;
  subCategoryActive:  boolean;
}

export class RegulationDto {
  id : number;
  name : string;
  active : boolean;
}


export class PatternCategoryDto {
 id : number;
 name : string;
 active : boolean;
 parentId : number;
 patterns : TextSearchPatternDto[];
}

export enum PatternType {
  REGEX = 'REGEX',
  PREDEFINED = 'PREDEFINED'
}

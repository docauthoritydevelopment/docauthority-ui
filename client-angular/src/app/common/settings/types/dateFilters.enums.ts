export enum TimePeriod {
  MINUTES = 'MINUTES',
  HOURS = 'HOURS',
  DAYS = 'DAYS',
  WEEKS = 'WEEKS',
  MONTHS = 'MONTHS',
  YEARS= 'YEARS'
}


export enum DateRangePointType {
  ABSOLUTE = 'ABSOLUTE',
  RELATIVE = 'RELATIVE',   // Point contains a value indicating difference from now/today - e.g. 3 years ago
  EVER = 'EVER',       // Special value indicating beginning of time
  UNAVAILABLE  = 'UNAVAILABLE' //for files without date
}

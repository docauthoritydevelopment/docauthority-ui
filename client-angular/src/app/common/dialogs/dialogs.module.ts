import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DateRangeDialog} from "@app/common/dialogs/dateRangeDialog/date-range-dialog";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DateDialog} from "@app/common/dialogs/dateDialog/date-dialog";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";
import {UploadFormComponent} from "@app/common/dialogs/uploadDialog/uploadForm/uploadForm.component";
import {UploadValidFormComponent} from "@app/common/dialogs/uploadDialog/uploadValidForm/uploadValidForm.component";
import {UploadSuccessFormComponent} from "@app/common/dialogs/uploadDialog/uploadSuccessForm/uploadSuccessForm.component";
import {UploadService} from "@app/common/dialogs/uploadDialog/upload.service";


@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    SharedUiModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DaDraggableModule
  ],
  declarations: [
    DateRangeDialog,
    DateDialog,
    UploadDialogComponent,
    UploadFormComponent,
    UploadValidFormComponent,
    UploadSuccessFormComponent
  ],
  providers: [
    UploadService
  ],
  entryComponents: [
    DateRangeDialog,
    DateDialog,
    UploadDialogComponent,
    UploadFormComponent,
    UploadValidFormComponent,
    UploadSuccessFormComponent
  ]
})
export class DialogsModule { }

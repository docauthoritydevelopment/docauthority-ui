import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {CommonConfig} from "@app/common/configuration/common-config";

@Component({
  selector: 'upload-file-form',
  templateUrl: './uploadForm.component.html',
  styleUrls: ['./uploadForm.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UploadFormComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  @Input() validateUploadFn:any;
  @Input() fileType:string;

  @Output() validated = new EventEmitter<any>();

  file:any = new Blob([''], { type: 'application/xml' });
  validationInfo:any = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private dialogService: DialogService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {

  }

  //---------------------------------------
  // privates
  //---------------------------------------

  //---------------------------------------
  // comparators
  //---------------------------------------

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.dialogService.showAlert("Error", 'Sorry, an error occurred.', AlertType.Error, err);
  }

  //---------------------------------------
  // actions/validators
  //---------------------------------------

  fileChange(e) {
    this.file = e.srcElement.files[0];
    let formData = new FormData();
    formData.append("file", this.file);
    this.validateUploadFn(formData, CommonConfig.MAX_ERROR_ROWS_REPORT).subscribe((res) => {
      this.validationInfo = res;
      this.validated.emit({result: res, file: this.file});
    }, (err) => {this.showError(err)});
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

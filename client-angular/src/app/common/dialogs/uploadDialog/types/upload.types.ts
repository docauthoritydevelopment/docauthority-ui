export enum UploadSteps {
  UPLOAD = 'upload',
  FAILED = 'failed',
  VALID = 'valid',
  SUCCESS = 'success'
}

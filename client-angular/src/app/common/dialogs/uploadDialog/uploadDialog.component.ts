import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {UploadSteps} from "@app/common/dialogs/uploadDialog/types/upload.types";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {UploadService} from './upload.service';
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";

@Component({
  selector: 'upload-dialog',
  templateUrl: './uploadDialog.component.html',
  //styleUrls: ['./uploadDialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UploadDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  uploadFn:any;
  validateUploadFn:any;
  extractValidateUploadDataFn:any;
  component:string;
  fileType:string;
  file:File = null;
  validationInfo:any = null;
  importInfo:any = null;
  newFileSelected:boolean = false;
  allowDuplicates:boolean = true;
  updateDuplicates:boolean = false;

  uploadStep:string = UploadSteps.UPLOAD;
  nextStep:string = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(
    private activeModal: NgbActiveModal,
    private uploadService: UploadService,
    private dialogService: DialogService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    if(_.has(this.data, 'uploadFn')) {
      this.uploadFn = this.data.uploadFn;
    }
    if(_.has(this.data, 'validateUploadFn')) {
      this.validateUploadFn = this.data.validateUploadFn;
    }
    if(_.has(this.data, 'component')) {
      this.component = this.data.component;
    }
    if(_.has(this.data, 'fileType')) {
      this.fileType = this.data.fileType.substr(1);
    }
    if (_.has(this.data, 'extractValidateUploadDataFn')) {
      this.extractValidateUploadDataFn = this.data.extractValidateUploadDataFn;
    }
    if (_.has(this.data, 'allowDuplicates')) {
      this.allowDuplicates = this.data.allowDuplicates;
    }
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  //---------------------------------------
  // comparators
  //---------------------------------------

  isUploadStage() {
    return this.uploadStep == UploadSteps.UPLOAD;
  }

  //---------------------------------------

  isValidStage() {
    return this.uploadStep == UploadSteps.VALID;
  }

  //---------------------------------------

  isSuccessStage() {
    return this.uploadStep == UploadSteps.SUCCESS;
  }

  //---------------------------------------

  isFailureStage() {
    return this.uploadStep == UploadSteps.FAILED;
  }

  //---------------------------------------
  // validators
  //---------------------------------------

  onFileValidated(validationRet:any) {
    this.validationInfo = validationRet.result;
    this.newFileSelected = true;
    if (!this.validationInfo.importFailed) {
      this.file = validationRet.file;
      this.uploadStep = UploadSteps.VALID;
    }
  }

  //---------------------------------------
  // actions
  //---------------------------------------

  setUpdateDuplicates(updateDuplicates:boolean) {
    this.updateDuplicates = updateDuplicates;
  }

  //---------------------------------------

  back() {
    this.newFileSelected = false;
    this.nextStep = this.uploadStep;
    this.uploadStep = UploadSteps.UPLOAD;
  }

  //---------------------------------------

  next() {
    this.uploadStep = this.nextStep;
    this.nextStep = null;
  }

  //---------------------------------------

  downloadValidationLog() {
    this.uploadService.exportImportValidationData(this.validationInfo, this.extractValidateUploadDataFn, "Validation", this.file.name).subscribe(() => {});
  }

  //---------------------------------------

  downloadImportLog() {
    this.uploadService.exportImportValidationData(this.importInfo, this.extractValidateUploadDataFn, "Import", this.file.name).subscribe(() => {});
  }

  //---------------------------------------

  import() {
    if (this.validationInfo.importedRowCount > 0 || (this.updateDuplicates && this.validationInfo.duplicateRowCount > 0)) {
      let formData = new FormData();
      formData.append("file", this.file);
      this.uploadFn(formData, this.updateDuplicates).subscribe((res) => {
        this.importInfo = res;
        if (!this.importInfo.importFailed) {
          this.uploadStep = UploadSteps.SUCCESS;
        }
      }, (err) => {this.showError(err)});
    }
  }

  //---------------------------------------

  cancel(res) {
    this.activeModal.close(res);
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.dialogService.showAlert("Error", 'Sorry, an error occurred.', AlertType.Error, err);
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

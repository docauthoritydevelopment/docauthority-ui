import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ExcelService} from "@services/excel.service";

@Injectable()
export class UploadService {

  constructor(
    private excelService: ExcelService) {
  }

  exportImportValidationData = (validationInfo:any, extractImportValidationData:any, action:string, fileName:string): Observable<any> =>  {
    let selectedItems = [];
    validationInfo.errorImportRowResultDtos.forEach(row => {
      selectedItems.push({type: "Error", data: row});
    });
    validationInfo.warningImportRowResultDtos.forEach(row => {
      selectedItems.push({type: "Warning", data: row});
    });
    validationInfo.duplicateImportRowResultDtos.forEach(row => {
      selectedItems.push({type: "Duplicate", data: row});
    });
    return this.excelService.exportSelectedData(selectedItems, extractImportValidationData, `${action} log for ${fileName}`);
  };
}

import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import {DialogBaseComponent} from '../../../../shared-ui/components/modal-dialogs/base/dialog-component.base';
import {DialogService} from "../../../../shared-ui/components/modal-dialogs/dialog.service";

@Component({
  selector: 'upload-file-valid-form',
  templateUrl: './uploadValidForm.component.html',
  //styleUrls: ['./uploadSuccessForm.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UploadValidFormComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  @Input() file:File;
  @Input() validationInfo:any;
  @Input() component:string;
  @Input() allowDuplicates:boolean;

  @Output() updateDuplicates = new EventEmitter<boolean>();

  updateDuplicatesVal:boolean = false;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private dialogService: DialogService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  //---------------------------------------
  // comparators
  //---------------------------------------

  //---------------------------------------
  // actions
  //---------------------------------------

  checkUpdateDuplicates() {
    this.updateDuplicatesVal = !this.updateDuplicatesVal;
    this.updateDuplicates.emit(this.updateDuplicatesVal);
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

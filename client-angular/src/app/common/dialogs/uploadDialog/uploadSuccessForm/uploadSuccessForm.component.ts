import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, Input} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";

@Component({
  selector: 'upload-file-success-form',
  templateUrl: './uploadSuccessForm.component.html',
  //styleUrls: ['./uploadSuccessForm.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UploadSuccessFormComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  @Input() file:File;
  @Input() importInfo:any;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private dialogService: DialogService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  //---------------------------------------
  // comparators
  //---------------------------------------

  //---------------------------------------
  // actions
  //---------------------------------------

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

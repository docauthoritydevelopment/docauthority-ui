import {Component, Input, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgForm} from '@angular/forms';


@Component({
  selector: 'date-range-dialog',
  templateUrl: './date-range-dialog.html',
  styleUrls: ['./date-range-dialog.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DateRangeDialog implements OnInit {

  fromDate: NgbDateStruct;
  toDate;
  fromTime;
  toTime;
  invalidMsg = null;
  data;

  @ViewChild('myForm')
  public myForm: NgForm = null;


  constructor(private activeModal: NgbActiveModal, private calendar: NgbCalendar) { }

  ngOnInit() {
    if (this.data.from) {
      let fromDateObj = new Date(Number(this.data.from));
      this.fromDate = {year : fromDateObj.getFullYear(), month : fromDateObj.getMonth()+1, day : fromDateObj.getDate()};
      this.fromTime = {hour : fromDateObj.getHours() ,minute :  fromDateObj.getMinutes()}
      if (this.data.to) {
        let toDateObj = new Date(Number(this.data.to));
        this.toTime = {hour: toDateObj.getHours(), minute: toDateObj.getMinutes()}
        this.toDate = {year : toDateObj.getFullYear(), month : toDateObj.getMonth()+1, day : toDateObj.getDate()};
      }
      else {
        this.toTime = {hour: 0, minute: 0} ;
      }
    }
    else {
      this.fromTime = {hour: 0, minute: 0} ;
      this.toTime = {hour: 0, minute: 0} ;
    }
  }

  close() {
    this.activeModal.close(null);
  }

  saveDate() {
    if (this.myForm.valid) {
      let fromDateObj = new Date(this.fromDate.year, this.fromDate.month-1, this.fromDate.day, this.fromTime.hour, this.fromTime.minute, 0, 0);
      let toDateObj = this.toDate == null ? null : new Date(this.toDate.year, this.toDate.month-1, this.toDate.day, this.toTime.hour, this.toTime.minute, 0, 0);
      let now = new Date();


      if (toDateObj && fromDateObj > toDateObj) {
        this.invalidMsg = 'From date must be before To date'
      }
      else if (toDateObj && (now < toDateObj)) {
        this.invalidMsg = 'To date cannot be in the future'
      }
      else {
        let ans:any  = {
          from : fromDateObj.getTime()
        }
        if (toDateObj) {
          ans.to = toDateObj.getTime();
        }
        this.activeModal.close(ans);
      }
    }
    else {
      this.invalidMsg = 'Missing fields'
    }
  }

}

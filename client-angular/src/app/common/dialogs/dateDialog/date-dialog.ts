import {Component, Input, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { NgForm} from '@angular/forms';


@Component({
  selector: 'date-dialog',
  templateUrl: './date-dialog.html',
  styleUrls: ['./date-dialog.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DateDialog implements OnInit {

  fromDate: NgbDateStruct;
  fromTime;
  invalidMsg = null;
  data;

  @ViewChild('myForm')
  public myForm: NgForm = null;


  constructor(private activeModal: NgbActiveModal, private calendar: NgbCalendar) { }

  ngOnInit() {
    if (this.data.date) {
      let fromDateObj = new Date(Number(this.data.date));
      this.fromDate = {year : fromDateObj.getFullYear(), month : fromDateObj.getMonth()+1, day : fromDateObj.getDate()};
      this.fromTime = {hour : fromDateObj.getHours() ,minute :  fromDateObj.getMinutes()}
    }
    else {
      this.fromTime = {hour: 0, minute: 0} ;
    }
  }

  close() {
    this.activeModal.close(null);
  }

  saveDate() {
    if (this.myForm.valid) {
      let fromDateObj = new Date(this.fromDate.year, this.fromDate.month-1, this.fromDate.day, this.fromTime.hour, this.fromTime.minute, 0, 0);
      let now = new Date();
      if (now < fromDateObj) {
        this.invalidMsg = 'To date cannot be in the future'
      }
      else {
        let ans:any  = {
          date : fromDateObj.getTime()
        }
        this.activeModal.close(ans);
      }
    }
    else {
      this.invalidMsg = 'Missing fields'
    }
  }

}

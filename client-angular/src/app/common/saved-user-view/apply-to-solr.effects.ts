import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action} from '@ngrx/store';
import {APPLY_TO_SOLR_POLLING_SUCCESS,Empty,PollApplyToSolrSuccess,StartPollApplyToSolr} from '@core/store/core.actions';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class ApplyToSolrEffects {
  @Effect() applyToSolrSucceess: Observable<Action> = this.actions$.ofType(APPLY_TO_SOLR_POLLING_SUCCESS)
    .pipe(map((action: PollApplyToSolrSuccess) => action.result),
      map(result => {
        if (result.applyToSolrStatus === 'DONE') {
          const newAction = new StartPollApplyToSolr(90000);
          newAction.pollingMetaData.pollingStateClearingAction = null;
          newAction.replaceAction = false;
          return newAction;
        }
        return new Empty();
      }));

  constructor(private actions$: Actions) {}
}

import {DtoUser} from "@users/model/user.model";
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";

export class UserViewDataDto {
  id: number;
  name: string;
  displayData: string;
  icon?: string;
  filter?: SavedFilterDto;
  displayType?: EFilterDisplayType;
  scope?: string;
  globalFilter?: boolean;
  owner?: DtoUser;
  containerId?:number;

}

export class SavedFilterDto {
  id?: number;
  name?: string;
  rawFilter?: string;
  filterDescriptor: FilterDescriptor;
  globalFilter?: boolean;
  scope?: string;
}

export enum EFilterDisplayType {
  CHART = 'CHART',
  DISCOVER = 'DISCOVER',
  DASHBOARD_WIDGET = 'DASHBOARD_WIDGET',
  DISCOVER_VIEW = 'DISCOVER_VIEW',
  DASHBOARD = 'DASHBOARD'
}

export var filterDisplayTypeIcon = {
  "CHART": "fa fa-bar-chart",
  "DISCOVER": "fa fa-filter",
  "DASHBOARD_WIDGET": "fa fa-th-large",
  "DISCOVER_VIEW": "fa fa-image",
  "DASHBOARD": "fa fa-television"
};

export var filterDisplayTypeHref = {
  "CHART": "/charti",
  "DISCOVER": "/report/folders/files",
  "DASHBOARD_WIDGET": "/dashboard/DashboardCharts",
  "DISCOVER_VIEW": "/report",
  "DASHBOARD": "/dashboard/DashboardCharts"
};

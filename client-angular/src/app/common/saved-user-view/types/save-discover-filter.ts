import {SavedFilterDto} from './saved-filters-dto';

export class SaveDiscoverFilter {
  id: number;
  name: string;
  savedFilterDto: SavedFilterDto;
  leftEntityDisplayTypeName: string;
  rightEntityDisplayTypeName: string;
}

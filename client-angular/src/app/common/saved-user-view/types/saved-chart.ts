import {SavedFilterDto} from './saved-filters-dto';

export class SavedChart {
  static DETACHED_DASHBOARD_ID = -1;
  static sortType_SIZE = 'SIZE';
  static chartType_FILES_BY_ENTITY = 'FILES_BY_ENTITY';
  static chartDisplayType_BAR_HORIZONTAL = 'BAR_HORIZONTAL';
  static chartDisplayType_PIE = 'PIE';
  static chartDisplayType_BAR_VERTICAL = 'BAR_VERTICAL';
  name: string;
  chartDisplayType: string;
  chartType: string;
  mainEntityType: string;
  maximumNumberOfEntries: number;
  minimumCount: number;
  showOthers: boolean;
  savedFilterDto: SavedFilterDto;
  populationJsonUrl: string;
  populationSubsetJsonFilter: SavedChartViewConfiguration;
  showTabularView: boolean;
  sortType: string;
  id: number;
  hidden: boolean;
}

export class SavedChartViewConfiguration {
  showUnassociated: boolean;
  removedValueIds: string[];
  showRemovedValues: boolean;
  forceDisableUnassociated: boolean;
}


import {EChartDisplayType} from './EChartDisplayType';

export class TrendChartDto {
  name: string;
  chartDisplayType: EChartDisplayType;
  chartValueFormatType: EChartValueFormatType;
  ownerId: number;
  trendChartCollectorId: number;
  seriesType: string;
  valueType: string;
  sortType: ESortType;
  filters: string;
  viewConfiguration: TrendChartViewConfiguration | string;
  viewTo: string;
  viewSampleCount: number;
  viewResolutionMs: number;
  viewFrom: string;
  id: number;
  hidden: boolean;
}

export enum EChartValueFormatType {  //
  // In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  NUMERIC = 'NUMERIC',
  PERCENTAGE = 'PERCENTAGE'
}

export enum ESortType {
  NAME = 'NAME'
}

export class TrendChartViewConfiguration {
  totalSeriesHidden: boolean;
  otherSeriesHidden: boolean;
  totalSeriesType: string;
  otherSeriesType: string;
  viewResolutionStep: number;
  showInMenu: boolean;
}

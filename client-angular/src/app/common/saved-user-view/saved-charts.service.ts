import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SavedChart} from './types/saved-chart';
import {map, switchMap, concatMap} from 'rxjs/operators';
import { of } from 'rxjs';
import {Observable} from 'rxjs/index';
import {fromPromise} from 'rxjs/internal/observable/fromPromise';
import {TrendChartDto} from './types/trendChart-dto';
import {V1RouteUrl} from "@app/doc-1/angular-1-routes/model/v1.route.url";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {DisplayType} from "@app/types/display.type";
import {JSONUtilities} from "@utilities/json-tryparse";
import {ServerUrls} from "@app/common/configuration/common-config";

@Injectable()
export class SavedChartsService {

  constructor(private http: HttpClient, private authorizationService: UrlAuthorizationService) {}

  loadChartsList(): Observable<SavedChart[]> {
    return this.authorizationService.authorizeUrl(V1RouteUrl.chartView).pipe(
      concatMap((isValidate) => {
          if (isValidate) {
            return this.http.get(ServerUrls.USER_VIEW,{params:{displayType:DisplayType.CHART}}).pipe(
              map((userViews: any) => {
                if (userViews.content) {
                  return  userViews.content.map(u=>{
                    const chartObj: SavedChart = JSON.parse(u.displayData);
                    chartObj.id = u.id;
                    chartObj.name = u.name;
                    return chartObj;
                  }).filter(c=>!c.hidden);
                } else {
                  return [];
                }
              }));
          }
          else {
            return of([]);
          }
    }));
  }

  loadTrendCharsList(): Observable<TrendChartDto[]> {
    return this.authorizationService.authorizeUrl(V1RouteUrl.trendChartView).pipe(
      concatMap((isValidate) => {
      if (isValidate) {
        return this.http.get(ServerUrls.TREND_CHARTS).pipe(map((charts: any) => {
          return charts.content ? charts.content.filter(chart => !chart.hidden) : [];
        }), switchMap(charts => {
          const promises = [];
          charts.forEach(chart => {
            promises.push(JSONUtilities.tryParse(chart.viewConfiguration).then(result => {
              chart.viewConfiguration = result
              return chart;
            }));
          });
          return fromPromise(Promise.all(promises));
        }), map(charts => {

          return charts.filter(chart => chart && chart.viewConfiguration && chart.viewConfiguration.showInMenu)
        }));
      }
      else {
        return of([]);
      }
    }));
  }
}

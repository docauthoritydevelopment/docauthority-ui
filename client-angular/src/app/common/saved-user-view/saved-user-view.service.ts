import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {concatMap, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs/internal/Observable';
import {UserViewDataDto} from './types/saved-filters-dto';
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {TreeGridAdapterMapper, TreeGridData} from "@tree-grid/types/tree-grid.type";
import {CommonConfig, ExportType, ServerUrls} from "@app/common/configuration/common-config";
import {DTOPagingResults} from "@core/types/query-paging-results.dto";
import {UrlParamsService} from "@services/urlParams.service";
import {TreeGridAdapterService} from "@tree-grid/services/tree-grid-adapter.service";
import {RdbService} from "@services/rdb.service";
import {V1RouteUrl} from "@angular-1-routes/model/v1.route.url";
import {of} from "rxjs";
import {ServerExportService} from "@services/serverExport.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {CommonConfigurationService} from "@app/common/configuration/services/common-configuration.service";
import {Subject} from "rxjs/index";


@Injectable()
export class SavedUserViewService {
  private refreshUserViewListEvent = new Subject<number>();
  refreshUserViewListEvent$ = this.refreshUserViewListEvent.asObservable();



  constructor(private http: HttpClient,
              private authorizationService: UrlAuthorizationService,
              private urlParamsService: UrlParamsService,
              private dialogService:DialogService,
              private treeGridAdapter: TreeGridAdapterService,
              private rdbService: RdbService,
              private commonConfigurationService : CommonConfigurationService,
              private serverExportService: ServerExportService) {}

  //----------------------------------------

  refreshUserViewDataList(){
    this.refreshUserViewListEvent.next();
  }

  getSavedUserViewDataGrid(params, selectedItemId:number, displayType:string, byUser:boolean):Observable<TreeGridData<UserViewDataDto>> {
    if (selectedItemId) {
      params.findId = selectedItemId;
    }
    params = this.urlParamsService.fixedPagedParams(params,30);
    params.displayType = displayType;
    params.byUser = byUser;

    return this.http.get(ServerUrls.USER_VIEW,{params}).pipe(map((data: DTOPagingResults<UserViewDataDto>)=> {
      let ans: TreeGridData<UserViewDataDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }


  getDetachedSavedUserViewDataGrid(params, selectedItemId:number, displayType:string, byUser:boolean):Observable<TreeGridData<UserViewDataDto>> {
    params = this.urlParamsService.fixedPagedParams(params,30);
    params.displayType = displayType;
    params.byUser = byUser;

    return this.http.get(ServerUrls.USER_VIEW_DETACHED,{params}).pipe(map((data: DTOPagingResults<UserViewDataDto>)=> {
      let ans: TreeGridData<UserViewDataDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }

  getAllDetachedSavedUserViewDataList(params, selectedItemId:number, displayType:string, byUser:boolean):Observable<UserViewDataDto[]> {
    params = this.urlParamsService.fixedPagedParams(params,30);
    params.displayType = displayType;
    params.byUser = byUser;
    params.pageSize = CommonConfig.MAX_FIRST_LEVEL_ITEMS_IN_TREE;
    return this.http.get(ServerUrls.USER_VIEW_DETACHED,{params}).pipe(map((data: DTOPagingResults<UserViewDataDto>)=> {
      return data.content;
    }));
  }


  getSavedUserViewData(params, displayType:string, byUser:boolean, containerId = null):Observable<UserViewDataDto[]> {
    let hParams = new HttpParams();
    if (params) {
      Object.keys(params).forEach((key) => {
        hParams = hParams.set(key ,params[key]);
      });
    }
    if (containerId) {
      hParams = hParams.set("containerId", containerId);
    }
    hParams = hParams.set("displayType",displayType);
    hParams = hParams.set("byUser",byUser+"");
    return this.http.get(this.rdbService.getQueryForAllItems(ServerUrls.USER_VIEW),{
      params : hParams
    }).pipe(map((data: DTOPagingResults<UserViewDataDto>) => {
      if (data.content) {
        return data.content;
      }
      return [];
    }));
  }

  //----------------------------------------

  getDetachedSavedUserViewData(params, displayType:string):Observable<Object> {
    let hParams = new HttpParams();
    hParams = hParams.set("take",Number.MAX_SAFE_INTEGER+"");
    if (params) {
      Object.keys(params).forEach((key) => {
        hParams = hParams.set(key ,params[key]);
      });
    }
    hParams = hParams.set("displayType",displayType);
    return this.http.get(this.rdbService.getQueryForAllItems(ServerUrls.USER_VIEW_DETACHED),{
      params : hParams
    }).pipe(map((data: DTOPagingResults<UserViewDataDto>) => {
      if (data.content) {
        return data.content;
      }
      return [];
    }));
  }

  //----------------------------------------

  getSavedUserViewDataById(params, userViewDataId:number):Observable<Object> {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.USER_VIEW_WITH_ID,{userViewId : userViewDataId}));
  }


  //----------------------------------------

  addSavedUserViewData(savedUserView:UserViewDataDto) {
    return this.http.put(this.rdbService.getQueryForAllItems(ServerUrls.USER_VIEW), savedUserView).pipe(
      tap({
        next: (resultObject: UserViewDataDto) => {
          this.commonConfigurationService.refreshTopMenu();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  //----------------------------------------

  cloneSavedUserViewData(origId: number, newName: string, removeContainer:boolean) {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.USER_VIEW_CLONE,{userViewId : origId, removeContainer : removeContainer}), newName, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).pipe(
      tap({
        next: (resultObject: UserViewDataDto) => {
          this.commonConfigurationService.refreshTopMenu();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  //----------------------------------------

  editSavedUserViewData(savedUserView:UserViewDataDto) {
    return this.http.post(this.rdbService.getQueryForAllItems(ServerUrls.USER_VIEW), savedUserView).pipe(
      tap({
        next: (resultObject: UserViewDataDto) => {
          this.commonConfigurationService.refreshTopMenu();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  //----------------------------------------

  deleteSavedUserViewData(savedUserViewId:number):Observable<any> {

    return this.http.delete(this.rdbService.parseServerURL(ServerUrls.USER_VIEW_WITH_ID,{userViewId : savedUserViewId})).pipe(
      tap({
        next: (resultObject: UserViewDataDto) => {
          this.commonConfigurationService.refreshTopMenu();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  //----------------------------------------

  exportData = (itemsNum:number, params:any, displayType:string, fileName:string, byUser:boolean) =>  {
    params.displayType = displayType;
    params.byUser = byUser;
    this.serverExportService.prepareExportFile(fileName,ExportType.USER_VIEW_DATA,params,{ itemsNumber : itemsNum },(itemsNum  && itemsNum > CommonConfig.minimum_items_to_open_export_popup));
  };

  //----------------------------------------

  extractImportValidationData = (item) => {
    const partialItem: any = {};
    partialItem['Msg Type'] = item.type;
    partialItem['Line number'] = item.data.rowDto.lineNumber;
    partialItem['Msg Description'] = item.data.message;
    partialItem['NAME'] = item.data.rowDto.fieldsValues[0];
    partialItem['DISPLAY_TYPE'] = item.data.rowDto.fieldsValues[1];
    partialItem['DISPLAY_DATA'] = item.data.rowDto.fieldsValues[2];
    partialItem['FILTER_NAME'] = item.data.rowDto.fieldsValues[3];
    partialItem['FILTER_DESCRIPTOR'] = item.data.rowDto.fieldsValues[4];
    partialItem['FILTER_RAW'] = item.data.rowDto.fieldsValues[5];
    //partialItem['IS_FILTER_GLOBAL'] = item.data.rowDto.fieldsValues[6];
    return partialItem;
  };

  //----------------------------------------

  validateImportFile = (formData:FormData, maxErrorRows:number) =>  {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.USER_VIEW_VALIDATE_IMPORT,{maxErrorRows : maxErrorRows}), formData);
  };

  //----------------------------------------

  importData = (fileData:File, updateDuplicates:boolean) => {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.USER_VIEW_IMPORT,{updateDuplicates : updateDuplicates}), fileData);
  };
}

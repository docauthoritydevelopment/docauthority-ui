import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SavedChartsService} from './saved-charts.service';
import {CommonDiscoverModule} from "@app/common/discover/common-discover.module";
import {DiscoverActionsService} from "@app/common/discover/services/discover-actions.service";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {CommonConfigurationModule} from "@app/common/configuration/common-configuration.module";

/**
 * This module is used for discover services
 * that needed for the entire app.
 */
@NgModule({
  imports: [
    CommonModule,
    CommonDiscoverModule
  ],
  providers: [SavedUserViewService],
  exports: [],
  declarations: []
})
 export class SavedUserViewModule {
  public static forRoot() {
    return { ngModule: SavedUserViewModule, providers: [SavedUserViewService, SavedChartsService, DiscoverActionsService] }
  }

  public static forChild() {
    return { ngModule: SavedUserViewModule, providers:  [SavedUserViewService, SavedChartsService] }
  }

}

import {ChartTypes} from "@app/common/charts/types/common-charts-enums";

export interface SingleChartSeries {
  data:number[],
  name? : string,
  color?: string
}

export interface ChartSeries{
  [index:number]: SingleChartSeries
}


export interface ChartData {
  series : ChartSeries,
  categories : string[],
  type : ChartTypes
}


export interface SingleChartSeriesData {
  id : string;
  name? : string,
  color? : string,
}

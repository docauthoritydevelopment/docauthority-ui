export enum ChartTypes {
  BAR = 'BAR',
  LINE = 'LINE',
  COLUMNS = 'COLUMNS',
  PIE = 'PIE',
  PERCENTAGE_BAR = 'PERCENTAGE_BAR',
}

import {NgModule} from '@angular/core';
import {ChartComponent} from "@app/common/charts/chart-component/chart.component";
import { HighchartsChartModule } from 'highcharts-angular';
import {ChartService} from "@app/common/charts/services/chart.service";
import {AdvancedChartComponent} from "@app/common/charts/advanced-chart-component/advanced-chart.component";
import {CommonModule, DecimalPipe} from "@angular/common";
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {BytesPipe} from "@shared-ui/pipes/bytes.pipe";
import {ChartHeaderComponent} from "@app/common/charts/advanced-chart-component/chart-header/chart-header.component";
import {ClipboardModule} from "ngx-clipboard";
const SHARED_UI_COMPONENTS = [ChartComponent,AdvancedChartComponent, ChartHeaderComponent];


@NgModule({

  imports: [
    CommonModule,
    SharedUiModule,
    HighchartsChartModule,
    ClipboardModule
  ],
  declarations: SHARED_UI_COMPONENTS,
  exports: SHARED_UI_COMPONENTS,
  entryComponents: [],
  providers: [
    ChartService,
    BytesPipe,
    DecimalPipe
  ]
})
export class CommonChartsModule {
}

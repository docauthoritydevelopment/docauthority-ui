import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ClipboardService} from "ngx-clipboard";


@Component({
  selector: 'chart-header',
  templateUrl: './chart-header.component.html',
  styleUrls: ['./chart-header.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ChartHeaderComponent extends BaseComponent{
  _path = null;
  pathItems:string[] = [];

  @Input()
  bColor:string = "#000000"

  @Input()
  set path(newPath:string ) {
    this._path = newPath;
    this.updatePathItems(newPath);
  };

  get path():string {
    return this._path;
  }

  updatePathItems(newPath) {
    if (newPath == null) {
      this.pathItems=[];
    }
    else {
      let seperator = null;
      if (newPath.indexOf("/") > 1) {
        seperator = '/';
      }
      else {
        seperator = '\\';
      }
      if (newPath.startsWith(seperator)) {
        this.pathItems = newPath.substring(1).split(seperator);
      }
      else {
        this.pathItems = newPath.split(seperator);
      }
    }
  }

  constructor(private clipboardService: ClipboardService) {
    super();
  }

  copyToClipBoard() {
    this.clipboardService.copyFromContent(this.path);
  }
}

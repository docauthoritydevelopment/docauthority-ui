import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {
  DashboardChartDataDto, ChartInfo, DashboardChartPointDataDto,
} from "@app/features/dashboard/types/dashboard-interfaces";
import * as Highcharts from 'highcharts';
import {CommonConfig} from "@app/common/configuration/common-config";
import {ChartDisplayType, ChartValueType} from "@app/features/dashboard/types/dashboard-enums";
import {BytesPipe} from "@shared-ui/pipes/bytes.pipe";
import {DatePipe, DecimalPipe} from "@angular/common";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {AppConfig} from "@services/appConfig.service";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import * as _ from 'lodash';



@Component({
  selector: 'advanced-chart-component',
  templateUrl: './advanced-chart.component.html',
  styleUrls: ['./advanced-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AdvancedChartComponent implements OnInit {
  _chartData: DashboardChartDataDto;

  options: any  = {};
  Highcharts = Highcharts;
  myChart = null;
  chartDisplayType:ChartDisplayType = null;
  textMessage:string="";
  parsedDataSeries: string[][];
  showNoChartMsg = false;
  headerPath:string = null;
  headerPathColor: string = null;


  ChartDisplayType = ChartDisplayType;

  @Input()
  chartInfo:ChartInfo<any,any>;

  @Input()
  animation:boolean = true;

  @Input()
  chartName:string;

  @Output()
  selectionChanged = new EventEmitter();


  @Input()
  set chartData(newChartData: DashboardChartDataDto) {
    this._chartData = newChartData;
    if (this._chartData != null) {
      this.showData();
    }
  }

  get chartData():DashboardChartDataDto {
    return this._chartData;
  }

  chartCallback = (createdChart) =>  {
    if (this.myChart == null) {
      this.myChart = createdChart;
    }
  }

  exportAsPdf = (fileName:string)=>  {
    this.myChart.exportChartLocal({
      type: 'application/pdf',
      filename: fileName
    });
  }

  exportAsImage= (fileName:string)=>  {
    this.myChart.exportChartLocal({
      filename: fileName
    });
  }

  public redrawChart(){
    if (this.myChart) {
      this.myChart.reflow();
      setTimeout(() => {
        this.myChart.reflow();
      }, 100);
    }
  }


  getPercentageStr(number, total) {
    if (number ==0 && total == 0 ) {
      return "0%";
    }
    let final = 100*number/total;
    return(Math.floor(final) == 0 ? Math.ceil(final) +"%" : Math.floor(final) + "%");
  }

  getParsedValue = (val)=>  {
    if (this.chartInfo.valueType  == ChartValueType.FILE_SIZE) {
      return this.bytesPipe.transform(val);
    }
    else if (this.chartInfo.valueType  == ChartValueType.DATE) {
      return this.datePipe.transform(val,"dd-MMM-yyyy");
    }
    else {
      return this.numSuffixPipe.transform(val);
    }
  }

  nameTrancateLabelFormatter = (theValue)=>{
    if (theValue.length > 30 ) {
      return "<span title='"+ theValue +"'>"+theValue.substring(0,30) + "..."+"</span>";
    }
    return theValue;
  }


  valueLabelFormatter = (theValue)=>{
    let ans = this.getParsedValue(theValue.value);
    if (ans == "0") {
      return "";
    }
    return ans;
  }

  dataLabelFormatter = (value)=>{
    let ans = this.getParsedValue(value);
    if (ans == "0") {
      return "";
    }
    return ans;
  }

  valueTooltipFormater = (theValue)=>{
    let mainThis = this;
    return theValue.points.reduce(function (s, point) {
      let ans =  s + '<br/>' + point.series.name + ': ' ;
      ans = ans + mainThis.getParsedValue(point.y);
      return ans;
    }, '<b>' + theValue.x + '</b>');
  }

  addColorsToArray(colorPaleteId:string, series : any[] ) {
    if (colorPaleteId != null) {
      let palleteList:string[]  = DashboardService.getColorPalete(colorPaleteId);
      if (palleteList != null) {
        for (let colorIndex = 0; colorIndex < series.length; colorIndex++) {
          let line: any = series[colorIndex];
          if (colorIndex < palleteList.length) {
            line.color = palleteList[colorIndex];
          }
          else {
            line.color = CommonConfig.INITIAL_HIGHCHARTS_COLORS[colorIndex - palleteList.length];
          }
        }
      }
    }
  }


  getParsedCountText = (field , count, valueType)=>  {
    if (valueType  == ChartValueType.FILE_SIZE) {
      return this.bytesPipe.transform(count);
    }
    else {
      let ans = this.decimalPipe.transform(count) + " " + field;
      if (field.toLowerCase()=="department" && this.appConfig.isInstallationModeLegal()) {
        field = "Matter";
      }
      let lastDigit: string = field.substring(field.length - 1).toLowerCase();
      if (field.toLowerCase() == "metadata") {
        return ans;
      }
      else if (lastDigit == "s" || lastDigit == "h") {
        return ans + "es";
      }
      return ans + "s";
    }
  }

  getGaugeChart = ()=> {
    let mainThis = this;
    let theValue = this.chartData.series[0].data[0];
    let totalValue = this.chartData.series.length > 1 ? this.chartData.series[1].data[0] : theValue;
    let subTitleText:string  = this.getParsedCountText(this.chartData.categories[0], theValue, this.chartInfo.valueType);
    let ans:Highcharts.Options = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        margin: [0, 0, 50, 0 ]
      },
      tooltip: {
        backgroundColor: 'white',
        formatter: function () {
          let valueStr = this.y;
          let totalStr = mainThis.getParsedValue(this.point.total);
          if (!this.key) {
            return '<b>Total</b>: <b>' + totalStr +'</b>';
          }
          return '<b>' + mainThis.getParsedCountText(this.key, valueStr, mainThis.chartInfo.valueType) + ' (Out of '+ totalStr +')</b>';
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        verticalAlign: 'middle',
        style: {
          fontWeight: 'normal',
          fontSize: '28px'
        },
        y: -15,
        text: this.getPercentageStr(theValue,totalValue)
      },
      subtitle: {
        text: subTitleText,
        verticalAlign: 'bottom',
        style: {
          fontWeight: 'bold',
          fontSize: '16px'
        },
        y: -10
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      plotOptions: {
        series: {
          animation: mainThis.animation
        },
        pie: {
          dataLabels: {
            enabled: false
          },

        }
      },
      credits: {
        enabled: false
      },
      series: [{
        innerSize: '70%',
        data : [{y : theValue,  name : this.chartData.categories[0]}, {y : totalValue - theValue,  name : '' , color : "#dddddd"}]}]
    }
    return ans;
  }

  hexToRgbA(hex, opacity){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
      c= hex.substring(1).split('');
      if(c.length== 3){
        c= [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c= '0x'+c.join('');
      return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+ opacity +')';
    }
    throw new Error('Bad Hex');
  }


  getSunburstChart(){
    let thePoints = this.chartData.points;
    if (thePoints.length == 0 ) {
      return null;
    }
    let colorCount = 0 ;
    let colorsMap = {};
    let totalValue = 0 ;
    let palleteList:string[]  = this.chartInfo.colorPalete ? DashboardService.getColorPalete(this.chartInfo.colorPalete) : CommonConfig.INITIAL_HIGHCHARTS_COLORS;
    thePoints.sort(function(a1, a2){
      return a2.value - a1.value;
    });
    thePoints.forEach((point )=> {
      if (point.parent == "") {
        totalValue = totalValue + point.value;
        point.parent = "0" ;
      }
      else {
        point.value = Number(point.value);
      }
      if (!colorsMap[point.colorIndex]) {
        colorsMap[point.colorIndex] = palleteList[colorCount++];
        if (colorCount == palleteList.length) {
          colorCount = 0 ;
        }
      }
      point.color = this.hexToRgbA(colorsMap[point.colorIndex],  point.percentageValue > 0 ? point.percentageValue / 100 : 0);
      point.fullColor = colorsMap[point.colorIndex];
    });

    let mainThis = this;
    thePoints.unshift(
    {
      id: '0',
      name: mainThis.getParsedValue(totalValue),
      value: totalValue,
      directValue : totalValue,
      isTotal: true
    });
    let ans:Highcharts.Options = {
      chart: {
        marginTop: 50
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
          dataLabels: {
            enabled: false
          }
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      credits: {
        enabled: false
      },

      tooltip: {
        headerFormat: "",
        formatter: function() {
          let directValueStr = mainThis.getParsedValue(this.point.directValue);
          let valueStr = mainThis.getParsedValue(this.point.value);
          let ans = "";
          if (this.point.description) {
            ans = ans + '<div>Folder: ' + this.point.description  + '</div><br>';
          }
          ans = ans + '<div>Direct files: ' + directValueStr+ '</div><br><div>Sub folder files: ' + valueStr+ '</div>';
          return ans;
        }
      },


      series: [{
        allowPointSelect: true,
        type: "sunburst",
        data: thePoints,
        events: {
          mouseOut: function () {
            if (this.chart.lbl) {
              this.chart.lbl.hide();
            }
          }
        },
        point: {
          events: {
            select: function (e:any) {
              mainThis.headerPath = this.description;
              mainThis.headerPathColor = this.fullColor;
              mainThis.lastSelectedIndex = e.target.id;
              mainThis.selectionChanged.emit({
                id : e.target.id,
                name:  e.target.name
              });
            },
            unselect: function (e:any) {
              if (mainThis.lastSelectedIndex == e.target.id) {
                mainThis.lastSelectedIndex = null;
                mainThis.headerPath = null;
                mainThis.headerPathColor = null;
                mainThis.selectionChanged.emit({
                  id: null,
                  name: null,
                  targetItem : null,
                  seriesIndex: null
                });
              }
            },
            mouseOver: function () {
            }
          }
        },
        colorKey:'directValue',
        allowDrillToNode: true,
        cursor: 'pointer',
        dataLabels: {
          enabled : true,
          formatter: function() {
            if (mainThis.chartInfo.withLabels || this.point.isTotal) {
              return this.point.name;
            }
            return "";
          },
          filter: {
            property: 'innerArcLength',
            operator: '>',
            value: 16
          }
        },
        levels: [{
          level: 1,
          levelIsConstant: false,
          dataLabels: {
            enabled : true,
            filter: {
              property: 'outerArcLength',
              operator: '>',
              value: 64
            }
          }
        }, {
          level: 2,
          colorByPoint: true
        }]}]

    }
    return ans;
  }


  getTreeMapChart(){
    let thePoints = this.chartData.points;
    let colorCount = 0 ;
    let colorsMap = {};
    let palleteList:string[]  = this.chartInfo.colorPalete ? DashboardService.getColorPalete(this.chartInfo.colorPalete) : CommonConfig.INITIAL_HIGHCHARTS_COLORS;
    thePoints.forEach((point )=> {
      point.value = Number(point.value);
      if (!colorsMap[point.colorIndex]) {
        colorsMap[point.colorIndex] = palleteList[colorCount++];
        if (colorCount == palleteList.length) {
          colorCount = (thePoints.length % palleteList.length) == 0  ? 1 :  0 ;
        }
      }
      point.color = this.hexToRgbA(colorsMap[point.colorIndex],  point.percentageValue > 0 ? 0.5 + point.percentageValue / 200 : 0.25);
    });
    let mainThis = this;
    let ans:Highcharts.Options = {
      chart: {

      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'treemap',
        layoutAlgorithm: 'squarified',
        allowDrillToNode: true,
        animationLimit: 1000,
        dataLabels: {
          enabled: false
        },
        levelIsConstant: false,
        levels: [{
          level: 1,
          dataLabels: {
            enabled: true
          },
          borderWidth: 3
        }],
        data: thePoints
      }],

      tooltip: {
        backgroundColor: 'white',
        formatter: function() {
          let valueStr = mainThis.getParsedValue(this.point.directValue);
          return '<b>' + this.key + '</b>: <b>' + valueStr + '</b>';
        }
      }
    }
    return ans;
  }

  getLineChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    let mainThis = this;
    this.addColorsToArray(this.chartInfo.colorPalete, this.chartData.series);
    let ans:Highcharts.Options = {
      chart: {
        type: "line",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        },
        labels : {
          formatter : this.valueLabelFormatter
        }
      },
      plotOptions: {
        series: {
          animation: mainThis.animation,
          dataLabels: {
            enabled : this.chartInfo.withLabels,
            formatter : function(){return mainThis.dataLabelFormatter(this.y)}
          }
        },
      },
      tooltip: {
        shared: true,
        crosshairs: true,
        backgroundColor: 'white',
        formatter: function() {return mainThis.valueTooltipFormater(this)}
      },
      credits: {
        enabled: false
      },
      series : _.cloneDeep(this.chartData.series)
    }
    return ans;
  }


  getPieChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    let mainThis = this;
    let valuesList = [];
    for (let count = 0; count < this.chartData.series[0].data.length; count++) {
      valuesList.push({
        name : this.chartData.categories[count],
        y: this.chartData.series[0].data[count]
      });
    }
    this.addColorsToArray(this.chartInfo.colorPalete , valuesList);
    let tmpSeries = [{
      data: valuesList
    }];
    let ans:Highcharts.Options = {
      chart: {
        type: "pie",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        },
        options3d: {
          enabled: this.chartInfo.show3d,
          alpha: 45,
          beta: 0
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        }
      },
      tooltip: {
        backgroundColor: 'white',
        formatter: function () {
          let valueStr = mainThis.getParsedValue(this.y);
          return '<b>' + this.key + '</b>: <b>' + (mainThis.chartInfo.yAxisTitle ? mainThis.chartInfo.yAxisTitle.toLowerCase() + ' - ' : '') + valueStr +' ('+ Number(this.point.percentage).toFixed(1) +'%)</b>';
        },
        positioner: function (labelWidth, labelHeight, point) {
          var actualXLocation: number = point.plotX ;
          var actualYLocation: number = point.plotY ;

          var tooltipX, tooltipY;
          tooltipX = actualXLocation < this.chart.plotSizeX/2 ?  actualXLocation -  30 - labelWidth : actualXLocation + 40;
          if (tooltipX< 0 ) {
            tooltipX= 0 ;
          }
          if (tooltipX > this.chart.plotSizeX - labelWidth ) {
            tooltipX= this.chart.plotSizeX - labelWidth;
          }

          tooltipY = actualYLocation < this.chart.plotSizeY/2 ?  actualYLocation -  40 : actualYLocation + 30;
          if (tooltipY< 0 ) {
            tooltipY =  actualYLocation + 30
          }

          if (tooltipY > this.chart.plotSizeY - labelHeight) {
            tooltipY = actualYLocation -  40;
          }


          return {
            x: tooltipX,
            y: tooltipY
          };
        }
      },
      plotOptions: {
        series: {
          animation: mainThis.animation
        },
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            padding: 0,
            enabled: true,
            style: {
              fontSize: '10px',
              fontWeight: 'normal'
            },
            formatter: function () {
              return this.key + '<br><b>' + this.y + ' (' + Number(this.point.percentage).toFixed(1) + '%)</b>'
            }
          }
        }
      },
      credits: {
        enabled: false
      },
      series : tmpSeries
    }
    return ans;
  }


  getBarChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    let mainThis = this;
    this.addColorsToArray(this.chartInfo.colorPalete, this.chartData.series);
    let ans:Highcharts.Options = {
      chart: {
        options3d: {
          enabled: this.chartInfo.show3d,
          alpha: 15,
          beta: 15,
          depth: 50
        },
        type: "bar",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories,
        labels : {
          useHTML : false,
          formatter : function(){
            return (mainThis.nameTrancateLabelFormatter(this.value));
          }
        }
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        },
        labels : {
          formatter : this.valueLabelFormatter
        }
      },
      plotOptions: {
        series: {
          animation: mainThis.animation,
          dataLabels: {
            enabled : this.chartInfo.withLabels,
            formatter : function(){return mainThis.dataLabelFormatter(this.y)}
          }
        }
      },
      tooltip: {
        shared: true,
        crosshairs: true,
        backgroundColor: 'white',
        useHTML: true,
        formatter: function() {return mainThis.valueTooltipFormater(this)}
      },
      credits: {
        enabled: false
      },
      series : _.cloneDeep(this.chartData.series)
    }
    return ans;
  }



  getAreaChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    if (this.chartInfo.colorPalete) {
      this.addColorsToArray(this.chartInfo.colorPalete, this.chartData.series);
    }
    else {
      for (let count = 0; count < this.chartData.series.length; count++) {
        this.chartData.series[count].color = CommonConfig.INITIAL_HIGHCHARTS_COLORS[count];
      }
    }
    let mainThis = this;
    let ans:Highcharts.Options = {
      chart: {
        type: "area",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      plotOptions: {
        series: {
          fillOpacity: 0.3,
          animation : this.animation,
          dataLabels: {
            enabled : this.chartInfo.withLabels,
            formatter : function(){return mainThis.dataLabelFormatter(this.y)}
          }
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        },
        labels : {
          formatter : this.valueLabelFormatter
        }
      },
      tooltip: {
        shared: true,
        crosshairs: true,
        backgroundColor: 'white',
        formatter: function() {return mainThis.valueTooltipFormater(this)}
      },
      credits: {
        enabled: false
      },
      series : _.cloneDeep(this.chartData.series)
    }
    return ans;
  }

  getColumnChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    let mainThis = this;
    this.addColorsToArray(this.chartInfo.colorPalete, this.chartData.series);
    let ans:Highcharts.Options = {
      chart: {
        options3d: {
          enabled: this.chartInfo.show3d,
          alpha: 15,
          beta: 15,
          depth: 50
        },
        type: "column",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        },
        labels : {
          formatter : this.valueLabelFormatter
        }
      },
      plotOptions: {
        series: {
          animation: mainThis.animation,
          dataLabels: {
            enabled : this.chartInfo.withLabels,
            formatter : function(){return mainThis.dataLabelFormatter(this.y)}
          }
        }
      },
      tooltip: {
        shared: true,
        backgroundColor: 'white',
        crosshairs: true,
        formatter: function() {return mainThis.valueTooltipFormater(this)}
      },
      credits: {
        enabled: false
      },
      series : _.cloneDeep(this.chartData.series)
    }
    return ans;
  }



  getStackedBarChart(){
    if (!this.chartData.series || this.chartData.series.length == 0){
      return null;
    }
    let mainThis = this;
    this.addColorsToArray(this.chartInfo.colorPalete, this.chartData.series);
    let ans:Highcharts.Options = {
      chart: {
        options3d: {
          enabled: this.chartInfo.show3d,
          alpha: 15,
          beta: 15,
          depth: 50
        },
        type: "column",
        plotShadow: false,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartName
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      title: {
        text: this.chartInfo.title ? this.chartInfo.title : ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories : this.chartData.categories
      },
      yAxis: {
        title : {
          text : this.chartInfo.yAxisTitle ? this.chartInfo.yAxisTitle : ''
        },
        labels : {
          formatter : this.valueLabelFormatter
        },
        stackLabels: {
          enabled: true
        }
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: this.chartInfo.withLabels
          }
        },
        series: {
          animation: mainThis.animation,
          dataLabels: {
            enabled : this.chartInfo.withLabels,
            formatter : function(){return mainThis.dataLabelFormatter(this.y)}
          }
        }
      },
      tooltip: {
        shared: true,
        backgroundColor: 'white',
        crosshairs: true,
        formatter: function() {return mainThis.valueTooltipFormater(this)}
      },
      credits: {
        enabled: false
      },
      series : _.cloneDeep(this.chartData.series)
    }
    return ans;
  }

  showData = ()=>  {
    this.showNoChartMsg = false;
    this.headerPath = null;
    let initialChart: Highcharts.Options = null;
    if (this._chartData) {
      if (this.chartInfo.displayTypes.length == 1) {
        this.chartDisplayType  = this.chartInfo.displayTypes[0];
        if (this.chartDisplayType == ChartDisplayType.SUNBURST) {
          initialChart = this.getSunburstChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.TREE_MAP) {
          initialChart = this.getTreeMapChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.GAUGE) {
          initialChart = this.getGaugeChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.LINE) {
          initialChart = this.getLineChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.PIE) {
          initialChart = this.getPieChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.BAR) {
          initialChart = this.getBarChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.AREA) {
          initialChart = this.getAreaChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.COLUMN) {
          initialChart = this.getColumnChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.STACKED_COLUMN) {
          initialChart = this.getStackedBarChart();
        }
        else if (this.chartDisplayType == ChartDisplayType.GRID) {
          this.parsedDataSeries = [];
          for (let seriesObj of this._chartData.series) {
            let parsedDataArray:string[] = [];
            for (let grow = 0 ; grow < seriesObj.data.length ; grow++) {
              parsedDataArray.push(this.getParsedValue(seriesObj.data[grow]));
            }
            this.parsedDataSeries.push(parsedDataArray);
          }
        }
        else {
          if (this.chartDisplayType == ChartDisplayType.SINGLE_VALUE) {
            let theValue = this.chartData.series[0].data[0];
            let displayValue:string  = this.chartInfo.valueType  == ChartValueType.FILE_SIZE ? this.bytesPipe.transform(theValue) : this.decimalPipe.transform(theValue);
            this.textMessage = displayValue; // this.getParsedCountText(this.chartData.categories[0],displayValue);
          }
        }
      }
    }
    if (initialChart != null && this.chartDisplayType != ChartDisplayType.TREE_MAP && this.chartDisplayType != ChartDisplayType.SUNBURST) {
      this.addAdditionalAttributes(initialChart);
    }
    if (initialChart == null) {
      this.showNoChartMsg = true;
    }
    else {
      if (this.options) {
        this.options = null;
        setTimeout(() => {
          this.options = _.cloneDeep(initialChart);
          ;
        })
      }
      else {
        this.options = initialChart;
      }
    }
  }

  lastSelectedIndex = null;
  addAdditionalAttributes(initialChart) {
    if (!initialChart.plotOptions) {
      initialChart.plotOptions = {};
    }
    if (!initialChart.plotOptions.series) {
      initialChart.plotOptions.series = {};
    }
    initialChart.plotOptions.cursor = 'pointer';
    initialChart.plotOptions.series.allowPointSelect = true;
    let mainThis = this;
    initialChart.plotOptions.series.point  = {
      events: {
        select: function (e:any) {
          mainThis.lastSelectedIndex = e.target.index+"_"+e.target.series.index;
          mainThis.selectionChanged.emit({
            id : mainThis._chartData.categoryIds[e.target.index],
            seriesIndex : e.target.series.index,
            targetItem : e.target,
            name:  mainThis._chartData.categories[e.target.index]
          });
        },
        unselect: function (e:any) {
          if (mainThis.lastSelectedIndex == e.target.index+"_"+e.target.series.index) {
            mainThis.lastSelectedIndex = null;
            mainThis.selectionChanged.emit({
              id: null,
              name: null,
              targetItem : null,
              seriesIndex: null
            });
          }
        }
      }
    };
  }


  constructor(private bytesPipe: BytesPipe , private datePipe: DatePipe, private appConfig: AppConfig, private numSuffixPipe: NumSuffixPipe, private decimalPipe : DecimalPipe ) {
    let highchartInitialColors :any[] = CommonConfig.INITIAL_DASHBOARD_WIDGET_COLORS;
    let isIe =  (document['documentMode'] || /Edge/.test(navigator.userAgent));
    Highcharts.setOptions({
      lang: {
        thousandsSep: ''
      },
      colors: highchartInitialColors
    });

    if (isIe) {
      Highcharts.Chart.prototype.exportChartLocal = function (exportingOptions, chartOptions) {
        var chart = this,
          options = Highcharts.merge(chart.options.exporting, exportingOptions);

        chart.getSVGForLocalExport(
          options,
          chartOptions,
          function () {
            console.error("Something went wrong");
          },
          function (svg) {
            Highcharts.downloadSVGLocal(
              svg,
              options,
              function () {
                console.error("Something went wrong");
              }
            );
          }
        );
      };
    }
  }


  ngOnInit() {
  }


}


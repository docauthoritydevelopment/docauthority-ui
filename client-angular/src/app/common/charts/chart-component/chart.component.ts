import {Component, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import { ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {ChartSeries} from "@app/common/charts/types/common-charts-interfaces";
import {CommonConfig} from "@app/common/configuration/common-config";
import {DtoCounterReport} from "@app/features/dashboard/types/dashboard-interfaces";
import * as Highcharts from 'highcharts';


@Component({
  selector: 'chart-component',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ChartComponent implements OnInit {
  _chartType:ChartTypes = null;
  _chartSeries:ChartSeries = null;
  _categories:string[] = null;
  _chartData: DtoCounterReport[] = null;

  myChart = null;

  options: any  = {};
  Highcharts = Highcharts;


  @Input()
  chartTitle = '';

  @Input()
  set chartSeries(newSeries: ChartSeries) {
    this._chartSeries = newSeries;
    this.showData();
  }

  get chartSeries(): ChartSeries {
    return this._chartSeries;
  }

  @Input()
  set categories(newCategories: string[]) {
    this._categories = newCategories;
    this.showData();
  }

  get categories(): string[] {
    return this._categories;
  }

  @Input()
  set chartData(newData:DtoCounterReport[]) {
    this._chartData = newData;
    this.showData();
  }

  get chartData(): DtoCounterReport[] {
    return this._chartData;
  }


  @Input()
  set chartType(newType : ChartTypes) {
    this._chartType = newType;
    this.showData();
  }

  get discoverType(): ChartTypes {
    return this._chartType;
  }

  exportAsPdf = (fileName:string)=>  {
    this.myChart.exportChartLocal({
      type: 'application/pdf',
      filename: fileName
    });
  }

  exportAsImage= (fileName:string)=>  {
    this.myChart.exportChartLocal({
      filename: fileName
    });
  }

  chartCallback = (createdChart) =>  {
    if (this.myChart == null) {
      this.myChart = createdChart;
    }
  }


  getStackedBarPercent() {
    if (this.chartData != null) {
      let valuesList = [];
      for (let count = this.chartData.length - 1; count >= 0; count--) {
        valuesList.push({
          name: this.chartData[count].name,
          data: [this.chartData[count].counter],
          color: this.chartData[count].color,
          legendIndex: count
        });
      }
      this._chartSeries = valuesList;
    }

    let ans = {
      chart: {
        type: 'bar',
        spacing: [0,10,0,10],
        marginTop: 25,
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      yAxis: {
        min: 0,
        visible: false,
        minPadding: 0,
        maxPadding: 0
      },
      xAxis: {
        visible: false,
        stackLabels: {
          enabled: true
        }
      },
      tooltip: {
        formatter: function () {
          return '<b>' + this.series.name +
            '</b>: <b>' + this.y + '</b>';
        }
      },
      plotOptions: {
        bar: {
          dataLabels : {
            enabled : true,
            inside: true
          }
        },
        series: {
          stacking: 'percent'
        }
      },
      legend: {
        enabled: true,
        verticalAlign: 'top',
        y : -5
      },
      credits: {
        enabled: false
      },
      series: this.chartSeries
    };
    return ans;
  }



  getPieChart(){
    if (this.chartData != null) {
      let valuesList = [];
      for (let count = 0; count < this.chartData.length; count++) {
        valuesList.push({
          name : this.chartData[count].name + '----' + ( this.chartData[count].description ? this.chartData[count].description : ''),
          y: this.chartData[count].counter,
          sliced : this.chartData[count].selected
        });
      }
      this._chartSeries = [{
        data: valuesList
      }];
    }
    let ans = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      title: {
        text: ''
      },
      tooltip: {
        formatter: function () {
          var values = this.key.split("----");
          var theText = '';
          if (values.length > 1 && values[1] && values[1]!== '') {
            theText = values[1];
          } else {
            // if only one value then format as normal
            theText = values[0];
          }
          return '<b>' + theText + '</b>: <b>' + this.y + ' ('+ Number(this.point.percentage).toFixed(1) +'%)</b>';
        },
        positioner: function (labelWidth, labelHeight, point) {
          var actualXLocation: number = point.plotX ;
          var actualYLocation: number = point.plotY ;

          var tooltipX, tooltipY;
          tooltipX = actualXLocation < this.chart.plotSizeX/2 ?  actualXLocation -  30 - labelWidth : actualXLocation + 40;
          if (tooltipX< 0 ) {
            tooltipX= 0 ;
          }
          if (tooltipX > this.chart.plotSizeX - labelWidth ) {
            tooltipX= this.chart.plotSizeX - labelWidth;
          }

          tooltipY = actualYLocation < this.chart.plotSizeY/2 ?  actualYLocation -  40 : actualYLocation + 30;
          if (tooltipY< 0 ) {
            tooltipY =  actualYLocation + 30
          }

          if (tooltipY > this.chart.plotSizeY - labelHeight) {
            tooltipY = actualYLocation -  40;
          }


          return {
            x: tooltipX,
            y: tooltipY
          };
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            padding: 0,
            enabled: true,
            style : {
              fontSize:'10px',
              fontWeight: 'normal'
            },
            formatter : function () {
              var values = this.key.split("----");
              var theText = values[0];
              return theText +'<br><b>' + this.y + ' (' + Number(this.point.percentage).toFixed(1) +'%)</b>'
            }
          }
        }
      },
      credits: {
        enabled: false
      },
      series: this.chartSeries
    };
    return ans;
  }


  getLineChart(){
    let ans = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'line',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      title: {
        text: ''
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
          month: '%e. %b',
          year: '%b'
        },
        title: {
          text: 'Date'
        }
      },
      yAxis: {
      },
      plotOptions: {
        line: {
          marker: {
            enabled: false
          }
        }
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
      },
      credits: {
        enabled: false
      },
      series: this.chartSeries
    }
    return ans;
  }





  getBarsChart() {
    if (this.chartData != null) {
      this._categories = [];
      let valuesList = [];
      for (let count = 0 ; count < this.chartData.length ; count++) {
        this._categories.push(this.chartData[count].name + '----' + ( this.chartData[count].description ? this.chartData[count].description : ''));
        valuesList.push(
          {
            y: this.chartData[count].counter,
            color  : this.chartData[count].selected  ? '#1ab394' : '#4F6990'
          });
      }
      this._chartSeries = [{
        data: valuesList
      }];
    }
    let ans = {
      chart: {
        type: 'bar',
        style: {
          fontFamily: 'Arial,Helvetica,sans-serif'
        }
      },
      title: {
        text: ''
      },
      exporting: {
        chartOptions: {
          title: {
            text: this.chartTitle
          },
        },
        fallbackToExportServer: false,
        url: 'none'
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      xAxis: {
        categories: this._categories,
        labels : {
          useHTML : false,
          formatter: function() {
            // split using -
            var values = this.value.split("----");
            return values[0];
          },
        },
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        backgroundColor: "#FFFFFF",
        formatter: function () {
          var values = this.x.split("----");
          let theText='';
          if (values.length > 1 && values[1] && values[1]!== '') {
            theText = values[1];
          } else {
            // if only one value then format as normal
            theText = values[0];
          }
          return '<b>' + theText +
            '</b>: <b>' + this.y + '</b>';
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        /* series: {
          stacking: 'normal'
        },*/
        bar: {
          dataLabels: {
            enabled: true,
            color: this._chartSeries[0].data.length > 20 ? 'black' : null
          },
          maxPointWidth: 30
        }
      },
      series: this.chartSeries,
      credits: {
        enabled: false
      }
    }
    return ans;
  }

  showData() {
    if (this._chartType && (this._chartSeries || this._chartData)) {
      let initialChart = null;
      if (this._chartType == ChartTypes.BAR){
        initialChart = this.getBarsChart();
      }
      else if (this._chartType == ChartTypes.LINE){
        initialChart = this.getLineChart();
      }
      else if (this._chartType == ChartTypes.PIE){
        initialChart = this.getPieChart();
      }
      else if (this._chartType == ChartTypes.PERCENTAGE_BAR){
        initialChart = this.getStackedBarPercent();
      }
      this.options = initialChart;
    }
  }

  constructor() {
    let highchartInitialColors :any[] = CommonConfig.INITIAL_HIGHCHARTS_COLORS;
    let isIe =  (document['documentMode'] || /Edge/.test(navigator.userAgent));
    Highcharts.setOptions({
      lang: {
        thousandsSep: ''
      },
      colors: isIe ? highchartInitialColors :  Highcharts.map(highchartInitialColors, function (color) {
        return {
          radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
          },
          stops: [
            [0, color],
            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
          ]
        };
      })
    });

    if (isIe) {
      Highcharts.Chart.prototype.exportChartLocal = function (exportingOptions, chartOptions) {
        var chart = this,
          options = Highcharts.merge(chart.options.exporting, exportingOptions);

        chart.getSVGForLocalExport(
          options,
          chartOptions,
          function () {
            console.error("Something went wrong");
          },
          function (svg) {
            Highcharts.downloadSVGLocal(
              svg,
              options,
              function () {
                console.error("Something went wrong");
              }
            );
          }
        );
      };
    }
  }


  ngOnInit() {
  }


}


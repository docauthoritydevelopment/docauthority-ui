import {Injectable} from '@angular/core';
import {
  ChartData,
  ChartSeries,
  SingleChartSeries,
  SingleChartSeriesData
} from "@app/common/charts/types/common-charts-interfaces";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {I18nService} from "@app/common/translation/services/i18n-service";
import * as Highcharts from 'highcharts';
import {TreeGridData} from "@tree-grid/types/tree-grid.type";


@Injectable()
export class ChartService {

  constructor(private i18nService: I18nService) {
  }

  refreshAllHighcharts()  {
    Highcharts.charts.forEach(function(chart) {
      chart.reflow();
    });
  }

  refreshHighchartByOutsideDOM(domObj) {
    let allCharts = domObj.getElementsByTagName("highcharts-chart");
    for (let count = 0 ; count < allCharts.length ; count++) {
      let hChart = Highcharts.charts[Highcharts.attr(allCharts[count], 'data-highcharts-chart')];
      hChart.reflow();
      setTimeout(()=> {
        hChart.reflow();
      },500);
    }
  }


  convertKeyValueObjectToBarChart(theObject: any ): ChartData {
    let ans:ChartData = {
      type : ChartTypes.BAR,
      categories : [],
      series : []
    };
    let theSeries:SingleChartSeries = {
      data:[],
      name: ''
    }
    Object.keys(theObject).forEach(key => {
      ans.categories.push(this.i18nService.translateId(key));
      theSeries.data.push(theObject[key]);
    });
    ans.series = [theSeries];
    return ans;
  }

  convertTreeGridDataToChartSeriesData(gridData: TreeGridData<any>, dateFieldName:string,valuesFields:SingleChartSeriesData[]): ChartSeries {
    let ans: any[] = [];
    valuesFields.forEach((valueFieldItem)=> {
      let dataArray:any[] = [];
      if (gridData.items[0].item[valueFieldItem.id]) {
        gridData.items.forEach((gridItem: any) => {
          dataArray.push([(new Date(gridItem.item[dateFieldName])).getTime(), Number(gridItem.item[valueFieldItem.id])])
        });
        ans.push({
          name: valueFieldItem.name ? valueFieldItem.name : valueFieldItem.id,
          data: dataArray,
          color : valueFieldItem.color
        })
      }
    })
    return ans;
  }
}

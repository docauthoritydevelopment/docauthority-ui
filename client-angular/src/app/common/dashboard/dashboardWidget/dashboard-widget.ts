import {Component, ViewEncapsulation, ViewChild, Input, EventEmitter, Output, ElementRef} from '@angular/core';
import {BaseComponent} from "@app/core/base/baseComponent";
import {
  DashboardChartDataDto,
  ChartInfo,
  GridItem,
  FixedHistogramDataDto,
  FixedHistogramBeforeTransform, ChartWidgetOptionItem
} from "@app/features/dashboard/types/dashboard-interfaces";
import {DashboardService} from "../services/dashboard.service";
import {AdvancedChartComponent} from "@app/common/charts/advanced-chart-component/advanced-chart.component";
import {AddEditChartWidgetDialogComponent} from "../popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {DialogService} from "@app/shared-ui/components/modal-dialogs/dialog.service";
import {ChartDisplayType, ChartType} from "@app/features/dashboard/types/dashboard-enums";
import {SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";
import {PlatformLocation} from "@angular/common";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {LoginService} from "@app/core/services/login.service";
import * as _ from 'lodash';
import {ESystemRoleName} from "@app/common/users/model/system-role-name";
import {UrlAuthorizationService} from "@app/core/services/url-authorization.service";
import {ViewChartWidgetDialogComponent} from "@app/common/dashboard/popups/viewChartWidgetDialog/viewChartWidgetDialog.component";
import {CommonConfig} from "@app/common/configuration/common-config";
import {DrillDownDialogComponent} from "@app/common/dashboard/popups/drillDownDialog/drillDownDialog.component";


@Component({
  selector: 'dashboard-widget',
  templateUrl: './dashboard-widget.html',
  styleUrls: ['./dashboard-widget.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardWidget extends BaseComponent{
  @ViewChild(AdvancedChartComponent)
  chartComponent: AdvancedChartComponent;
  loading: boolean = false;
  showDrillDown: boolean = false;
  removePointNum: number = 0;

  totalPages:any = null;
  widgetId: string = null;

  @Input()
  userViewDataDto : UserViewDataDto;

  @Input()
  disableOperations : boolean = false;


  @Input()
  filterList:SavedFilterDto[];

  @Input()
  currUserIsDashboardOwner:boolean = false;


  selectionId: string = null;
  selectionTarget: any = null;
  selectionName: string = null;
  selectionSeriesIndex: number = null;

  @Input()
  set gridItem(newItem :GridItem) {
    this._gridItem = newItem;
    this.checkOpenMenuOnRight();
  }

  get gridItem():GridItem {
    return this._gridItem;
  }

  @Input()
  filter:FilterDescriptor;

  @Output() widgetStatusChanged = new EventEmitter();

  @Output() widgetListUpdated = new EventEmitter();

  @Output() mainLoaderUpdated = new EventEmitter();

  @Output() widgetItemRemoved = new EventEmitter();

  @Output() widgetItemAdded = new EventEmitter();

  _gridItem:GridItem;
  chartData: DashboardChartDataDto;
  chartInfo:ChartInfo<FixedHistogramDataDto, FixedHistogramBeforeTransform>;
  currentPage: number = 1;
  withPager:boolean = false;
  withLabels:boolean = false;
  openSubMenuOnRight: boolean = true;
  dataTypeList: ChartWidgetOptionItem[] = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST;
  CommonConfig = CommonConfig;


  constructor(private loginService : LoginService, private savedUserViewService:SavedUserViewService,
              private urlAuthorizationService : UrlAuthorizationService,
              private platformLocation: PlatformLocation, private dashboardService:DashboardService, private dialogService:DialogService) {
    super();

    this.on(this.dashboardService.refreshChartsEvent$, ()=>{
      this.chartData = null;
      this.loadData();
    });
  }

  removeWidget = ()=>  {
    this.dialogService.openConfirm('Confirmation', "Are you sure you want to remove the widget '"+ this.userViewDataDto.name +"' ?").then(result => {
      if (result) {
        this.mainLoaderUpdated.emit(true);
        this.savedUserViewService.deleteSavedUserViewData(this.userViewDataDto.id).subscribe(()=>{
          this.widgetItemRemoved.emit(this.userViewDataDto.id);
          this.mainLoaderUpdated.emit(false);
        }, ()=>{
          this.mainLoaderUpdated.emit(false);
        });
      }
    });
  }

  onPageChanged(newPage) {
    this.currentPage = newPage;
    this.chartData = null;
    this.loadData();
  }

  checkOpenMenuOnRight = ()=> {
    if (this.gridItem) {
      if (this.gridItem.x + this.gridItem.cols < 6) {
        this.openSubMenuOnRight = true;
      }
      else {
        this.openSubMenuOnRight = false;
      }
    }  else {
      this.openSubMenuOnRight = false;
    }
  }



  daOnInit(){
    this.chartInfo = this.dashboardService.convertToChartInfoItem(this.userViewDataDto);
    this.currentPage =1;
    this.widgetId = "widget_item_"+ ( this.userViewDataDto.id  ? this.userViewDataDto.id+"_"  : "") + new Date().getTime();
    this.checkOpenMenuOnRight();
    this.loadData();
  }

  loadData() {
    this.loading = true;
    let dataParams = this.dashboardService.getWidgetChartDataQueryParams(this.chartInfo.additionalQueryParams, this.chartInfo.beforeTransformQueryParams);
    dataParams.page = this.currentPage;
    this.totalPages = null;
    this.dashboardService.getDashboardWidgetChartData(this.chartInfo.chartType, dataParams, this.filter ).subscribe((dashboardChartDataDto:DashboardChartDataDto)=>{
      this.chartData = dashboardChartDataDto;
      this.initChartOptions();
      this.totalPages = dashboardChartDataDto.totalElements == 0 ? null : Math.ceil(dashboardChartDataDto.totalElements / this.chartInfo.additionalQueryParams.take) ;
      this.loading = false;
    });
  }

  chartOptions;
  discoverOptions;

  initChartOptions = ()=> {
    this.chartOptions = [];
    this.withPager = this.chartInfo.withPagenation;
    this.withLabels = this.chartInfo.withLabels;
    let isSupportMngUserViewData: boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
    this.showDrillDown = this.chartInfo.chartType == ChartType.MULTIPLE_FIXED_HISTOGRAM;
    if (!this.disableOperations) {
      if (this.chartInfo.chartType == ChartType.MULTIPLE_FIXED_HISTOGRAM || this.chartInfo.chartType == ChartType.SINGLE_VALUE_CHART || this.chartInfo.chartType == ChartType.SUNBURST_CHART) {
        if ((this.userViewDataDto.owner && this.loginService.getCurrentUser().name == this.userViewDataDto.owner.name) || isSupportMngUserViewData) {
          this.chartOptions.push({
            title: "Edit",
            iconClass: "fa fa-pencil",
            actionFunc: this.editWidget
          });
          this.chartOptions.push({
            title: "Clone",
            iconClass: "fa fa-clone",
            actionFunc: this.cloneWidget
          });

        }
      }
      this.chartOptions.push({
        title: "Delete",
        disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData,
        iconClass: "fa fa-trash-o",
        actionFunc: this.removeWidget
      });
      this.chartOptions.push({});
    }
    let exportOptions = [];
    exportOptions.push({
      title: "Export as Excel",
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportAsExcel
    });

    if (this.chartInfo.displayTypes[0] != ChartDisplayType.SINGLE_VALUE  && this.chartInfo.displayTypes[0] != ChartDisplayType.GRID) {
      let isMsie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
      exportOptions.push(
        {
          title: "Export as Image",
          iconClass: "fa fa-image",
          actionFunc: this.exportAsImage
        });
      if (!isMsie11) {
        exportOptions.push({
          title: "Export as PDF",
          iconClass: "fa fa-file-pdf-o",
          actionFunc: this.exportAsPdf
        });
      }
    }
    this.chartOptions.push({
      title: "Export",
      children : exportOptions
    });

    if ((this.chartInfo.chartType == ChartType.MULTIPLE_FIXED_HISTOGRAM || this.chartInfo.chartType == ChartType.SINGLE_VALUE_CHART || this.chartInfo.chartType == ChartType.SUNBURST_CHART) && this.userViewDataDto.owner != null) {
      this.discoverOptions = [];
      let segmentList:SavedFilterDto[] = this.chartInfo.beforeTransformQueryParams && this.chartInfo.beforeTransformQueryParams.segmentList ? _.cloneDeep(this.chartInfo.beforeTransformQueryParams.segmentList) : [];
      if (this.chartInfo.chartType == ChartType.SINGLE_VALUE_CHART) {
        if (this.chartInfo.beforeTransformQueryParams.filterField) {
          segmentList = [this.chartInfo.beforeTransformQueryParams.filterField];
        }
        else {
          segmentList = [];
        }
      }
      if (!segmentList || segmentList.length == 0 || (segmentList.length == 1 && segmentList[0].name == "Total") || (!this.chartInfo.beforeTransformQueryParams.filterField && this.chartInfo.chartType == ChartType.SINGLE_VALUE_CHART)) {
        this.chartOptions.push({
          title: "Go discover",
          iconClass: "ion-android-exit",
          actionFunc: ()=> {
            window.location.href = "/report/" + DashboardService.getFieldDiscoverName(this.chartInfo.chartType, this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix) + "/files?pageLeft=1&pageRight=1&activePan=left" + (this.selectionId ? '&selectedItemLeft='+this.selectionId+'&findIdLeft='+this.selectionId : '');
          }
        });

        this.discoverOptions.push({
          title: "Go discover",
          iconClass: "ion-android-exit",
          actionFunc: ()=> {
            window.location.href = "/report/" + DashboardService.getFieldDiscoverName(this.chartInfo.chartType, this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix) + "/files?pageLeft=1&pageRight=1&activePan=left"+ (this.selectionId ? '&selectedItemLeft='+this.selectionId+'&findIdLeft='+this.selectionId : '');
          }
        });
      }
      else {
        let childOptions:any[] = [];
        for (let count = 0 ; count < segmentList.length ; count++) {
          let segmentFilter:SavedFilterDto = segmentList[count];
          let hrefUrl:string = "/report/" + DashboardService.getFieldDiscoverName(this.chartInfo.chartType, this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix) + "/files?pageLeft=1&pageRight=1&activePan=left"+ (this.selectionId ? '&selectedItemLeft='+this.selectionId+'&findIdLeft='+this.selectionId : '');
          if (segmentFilter.name != "Total") {
            hrefUrl = hrefUrl + "&filter=" + segmentFilter.rawFilter;
            console.log(hrefUrl);
          }
          childOptions.push({
            title: segmentFilter.name ,
            iconClass: "ion-android-exit",
            actionFunc: ()=> {
              window.location.href = this.platformLocation['location'].origin+ hrefUrl;
            }
          });

        }
        this.chartOptions.push({});
        let mainGoDiscoverItem:any  = {
          title: "Go discover for ",
          children : childOptions
        };
        if (childOptions.length == 1) {
          mainGoDiscoverItem.children = null;
          mainGoDiscoverItem.title = "Go discover";
          mainGoDiscoverItem.actionFunc = childOptions[0].actionFunc;
        }
        this.chartOptions.push(mainGoDiscoverItem);

        if (this.chartInfo.chartType == ChartType.MULTIPLE_FIXED_HISTOGRAM) {
          let filterField = this.dashboardService.getDataTypeObjectByIdAndPrefix(this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix).filterField;
          this.chartOptions.push({
            title: "Drill down",
            disabled: !this.selectionId || !filterField || (this.selectionId == DashboardService.OTHER_FILTER_ID),
            actionFunc: this.openDrillDown
          });
        }

        this.chartOptions.push({
          title: "Remove selected",
          disabled: !this.selectionId,
          actionFunc: this.removeSelectedPoint
        });

        this.chartOptions.push({
          title: "Restore "+ this.removePointNum + " points",
          disabled: !this.removePointNum,
          actionFunc: this.restoreRemoved
        });

        this.discoverOptions.push(mainGoDiscoverItem);
      }
    }
  }

  exportAsExcel  = ()=> {
    let qParams:any = this.dashboardService.getWidgetChartDataQueryParams(this.chartInfo.additionalQueryParams, this.chartInfo.beforeTransformQueryParams);
    this.dashboardService.exportDashboardWidgetChartData(this.chartInfo.chartType,
      qParams,
      this.userViewDataDto.name, this.chartData.totalElements, this.chartInfo.additionalQueryParams.take);
  };


  exportAsPdf = ()=> {
    if (this.userViewDataDto) {
      var fileName = this.userViewDataDto.name + '_chart';
      this.chartComponent.exportAsPdf(fileName);
    }
  };

  exportAsImage = ()=> {
    if (this.userViewDataDto) {
      var fileName = this.userViewDataDto.name + '_chart';
      this.chartComponent.exportAsImage(fileName);
    }
  };

  onChartSelectionChange(selectionItem:any) {
    this.selectionId = selectionItem.id;
    this.selectionName = selectionItem.name;
    this.selectionSeriesIndex = selectionItem.seriesIndex;
    this.selectionTarget = selectionItem.targetItem;
    this.initChartOptions();
  }

  cloneWidget = ()=> {
    let clonedUserViewDataDto:UserViewDataDto = _.cloneDeep(this.userViewDataDto);
    clonedUserViewDataDto.id = null;
    clonedUserViewDataDto.name = clonedUserViewDataDto.name + " - copy";
    let data = { data: {
        userView : clonedUserViewDataDto,
        filterList : this.filterList,
        containerId : clonedUserViewDataDto.containerId
      } };
    let modelParams = {
      windowClass: 'modal-xl-window'
    };
    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams).result.then((updatedUserViewDataDto: UserViewDataDto) => {
      if (updatedUserViewDataDto != null) {
        this.widgetItemAdded.emit(updatedUserViewDataDto);
      }
    });
  }


  editWidget = ()=>{
    //let userViewDataDto:UserViewDataDto =  this.dashboardService.convertToUserViewDataItem(this.chartInfo);
    //userViewDataDto.displayType = EFilterDisplayType.DASHBOARD_WIDGET;
    let data = { data: {
        userView : this.userViewDataDto,
        filterList : this.filterList,
        containerId : this.userViewDataDto.containerId
      } };
    let modelParams = {
      windowClass: 'modal-xl-window'
    };
    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams).result.then((updatedUserViewDataDto: UserViewDataDto) => {
      if (updatedUserViewDataDto != null ) {
        this.currentPage = 1;
        this.userViewDataDto = updatedUserViewDataDto;
        this.chartData = null;
        this.chartInfo = this.dashboardService.convertToChartInfoItem(updatedUserViewDataDto);
        if (this.chartInfo.displayTypes && this.chartInfo.displayTypes[0] == ChartDisplayType.SINGLE_VALUE) {
          this.gridItem.minItemRows = 1;
          this.gridItem.minItemCols = 3;
        }
        else if (this.chartInfo.displayTypes && this.chartInfo.displayTypes[0] == ChartDisplayType.GAUGE) {
          this.gridItem.minItemRows = 3;
          this.gridItem.minItemCols = 3;
        }
        else {
          this.gridItem.minItemRows = 3;
          this.gridItem.minItemCols = 3;
        }
        this.widgetStatusChanged.emit();
        this.loadData();
      }
    },(reason) => {});
  };

  onMouseDown(event) {
    this.closeAllDaDropDown();
  }

  restoreRemoved = ()=>{
    this.chartComponent.showData();
    this.removePointNum = 0 ;
    this.initChartOptions();
  }


  removeSelectedPoint = ()=>{
    this.selectionTarget.remove();
    this.removePointNum++;
    this.initChartOptions();
  }

  openDrillDown = ()=> {
    let data = {data: {selectionName : this.selectionName}};
    let modelParams = {windowClass: 'drill-down-dialog-modal-content', size : 'sm', centered : false, container : '#'+this.widgetId};
    this.dialogService.openModalPopup(DrillDownDialogComponent, data, modelParams).result.then((selectDataType:ChartWidgetOptionItem) => {
      if (selectDataType) {
        this.onDrillDownChoose(selectDataType);
      }
    });
  }

  onDrillDownChoose = (selectDataType:ChartWidgetOptionItem)=>  {

    let newChartInfo:ChartInfo<FixedHistogramDataDto, FixedHistogramBeforeTransform> = _.cloneDeep(this.chartInfo);
    let newSavedUser:UserViewDataDto = _.cloneDeep(this.userViewDataDto);

    let previousFilterField = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter((item) => {
      return (item.id == this.chartInfo.additionalQueryParams.groupedField && (!this.chartInfo.additionalQueryParams.queryPrefix || this.chartInfo.additionalQueryParams.queryPrefix == item.prefix));
    })[0].filterField;
    newChartInfo.additionalQueryParams.groupedField = selectDataType.id;
    if (selectDataType.prefix) {
      newChartInfo.additionalQueryParams.queryPrefix = selectDataType.prefix;
    }
    else {
      delete newChartInfo.additionalQueryParams.queryPrefix;
    }
    let continueFilter:FilterDescriptor  = this.chartInfo.beforeTransformQueryParams.segmentList[this.selectionSeriesIndex].filterDescriptor;
    let drillFilter = {
      "field":previousFilterField,
      "operator":"eq",
      "value":this.selectionId
    }
    if (continueFilter.field || continueFilter.filters) {
      continueFilter = {
        logic: "and",
        filters: [continueFilter , drillFilter]
      }
    }
    else {
      continueFilter = drillFilter;
    }
    let savedFilter: SavedFilterDto = {
      name : "Current filter",
      globalFilter : false,
      filterDescriptor : continueFilter
    };
    savedFilter.filterDescriptor.name = "Current filter";
    newChartInfo.beforeTransformQueryParams.segmentList = [savedFilter];
    newChartInfo.additionalQueryParams.withTotal = false;
    newChartInfo.additionalQueryParams.returnOther = false;
    savedFilter.rawFilter = JSON.stringify(savedFilter);
    newSavedUser.displayData = JSON.stringify(newChartInfo);
    newSavedUser.name = this.selectionName + " - " + selectDataType.name.toLowerCase();
    newSavedUser.id = null;
    let data = {data: {userView: newSavedUser , enableSave : true , dashboardId : this.userViewDataDto.containerId}};
    let modelParams = {windowClass: 'modal-xl-window'};
    this.dialogService.openModalPopup(ViewChartWidgetDialogComponent, data, modelParams).result.then((updatedUserViewDataDto: UserViewDataDto) => {
      if (updatedUserViewDataDto != null) {
        this.widgetItemAdded.emit(updatedUserViewDataDto);
      }
    });
  }
}

import {Component, EventEmitter, Input, OnChanges, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'dashboard-widget-pager',
  templateUrl: './dashboard-widget-pager.template.html',
  styleUrls: ['./dashboard-widget-pager.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardWidgetPagerComponent  {

  public _currentPage = null;

  @Input()
  set currentPage(newPage: number) {
    this._currentPage = newPage;
    this._displayPageNumber = this._currentPage;
  }

  get currentPage():number {
    return this._currentPage;
  }

  @Input()
  public totalPages: number;


  @Output()
  changePage = new EventEmitter();

  _displayPageNumber:number = null;


  get displayPageNumber() {
    return this._displayPageNumber;
  }

  set displayPageNumber(newPageNum:number) {
    this._displayPageNumber = newPageNum;
  }

  verifyKey = (event):boolean =>  {
    if (event.key === "Enter") {
      if (this.displayPageNumber === null || this.displayPageNumber+'' === '' || isNaN(this.displayPageNumber)) {
        this._displayPageNumber = this.currentPage;
      }
      else if (this.totalPages && (Number(this.displayPageNumber) > this.totalPages))
      {
        this.changePage.emit(this.totalPages);
        return true;
      }
      else {
        this.changePage.emit(Number(this.displayPageNumber));
        return true;
      }
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  moveToPage(option) {
    switch (option) {
      case 'first': {
        this.changePage.emit(1);
        break
      }
      case 'prev': {
        this.changePage.emit(Math.max(this.currentPage - 1, 1));
        break
      }
      case 'next': {
        this.changePage.emit(Math.min(this.currentPage + 1, this.totalPages ? this.totalPages : this.currentPage + 1));
        break
      }
      case 'last': {
        this.changePage.emit(this.totalPages);
        break
      }
    }
  }
}

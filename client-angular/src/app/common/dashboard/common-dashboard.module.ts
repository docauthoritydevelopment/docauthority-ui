import {NgModule} from '@angular/core';
import {ChartService} from "@app/common/charts/services/chart.service";
import {CommonModule} from "@angular/common";
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {AddEditDashboardDialogComponent} from "@app/common/dashboard/popups/addEditDashboardDialog/addEditDashboardDialog.component";
import {LoaderModule} from "@shared-ui/components/loader/loader.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {FormsModule} from "@angular/forms";
import {CommonChartsModule} from "@app/common/charts/common-charts.module";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import {ViewChartWidgetDialogComponent} from "@app/common/dashboard/popups/viewChartWidgetDialog/viewChartWidgetDialog.component";
import {DashboardWidget} from "@app/common/dashboard/dashboardWidget/dashboard-widget";
import {DashboardWidgetPagerComponent} from "@app/common/dashboard/dashboardWidgetPager/dashboard-widget-pager.component";
import {DrillDownDialogComponent} from "@app/common/dashboard/popups/drillDownDialog/drillDownDialog.component";
import {DynamicModule} from "ng-dynamic-component";
import {EditSimpleChartComponent} from "@app/common/dashboard/edit-components/editSimpleChart/editSimpleChart.component";
import {EditTreeMapChartComponent} from "@app/common/dashboard/edit-components/editTreeMapChart/editTreeMapChart.component";
const SHARED_UI_COMPONENTS = [DashboardWidget, DashboardWidgetPagerComponent, AddEditChartWidgetDialogComponent, AddEditDashboardDialogComponent, ViewChartWidgetDialogComponent, DrillDownDialogComponent, EditSimpleChartComponent, EditTreeMapChartComponent];


@NgModule({

  imports: [
    CommonModule,
    SharedUiModule,
    LoaderModule,
    NgbModule,
    DaDraggableModule,
    FormsModule,
    CommonChartsModule,
    DynamicModule.withComponents([EditSimpleChartComponent])
  ],
  declarations: SHARED_UI_COMPONENTS,
  exports: SHARED_UI_COMPONENTS,
  entryComponents: [ViewChartWidgetDialogComponent,DrillDownDialogComponent,EditSimpleChartComponent,EditTreeMapChartComponent],
  providers: [DashboardService, NumSuffixPipe]
})
export class CommonDashboardModule {
}

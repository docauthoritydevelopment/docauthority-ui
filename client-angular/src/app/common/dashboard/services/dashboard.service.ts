import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import * as _ from 'lodash';
import {
  DashboardChartDataDto,
  ChartInfo,
  DtoCounterReport,
  DtoMultiValueList,
  DTOSummaryReport, ChartWidgetOptionItem, FixedHistogramBeforeTransform, FixedHistogramDataDto,
} from "@app/features/dashboard/types/dashboard-interfaces";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {Observable, Subject} from "rxjs/index";
import {ChartDisplayType, ChartType} from "@app/features/dashboard/types/dashboard-enums";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {ServerExportService} from "@core/services/serverExport.service";
import {DatePipe} from "@angular/common";
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";
import {AppConfig} from "@services/appConfig.service";
import {DateRangePartitionDto} from "@app/common/settings/types/dateFilters.dto";

@Injectable()
export class DashboardService {

  private refreshChartsEvent = new Subject<any>();
  refreshChartsEvent$ = this.refreshChartsEvent.asObservable();

  private static sortOptionItemByName = function(item1:ChartWidgetOptionItem , item2:ChartWidgetOptionItem) {
    return item1.name.localeCompare(item2.name);
  }

  public static MULTIPLE_FIXED_HISTOGRAM:string = "MULTIPLE_FIXED_HISTOGRAM";
  public static SINGLE_VALUE_CHART:string = "SINGLE_VALUE_CHART";
  public static ALL_FILES_DISCOVER_NAME = "all_files";
  public static OTHER_FILTER_ID = "OTHER";

  public static dashboardColorsMap:any  = {
    "Spring" :  ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066','#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    "Strong" : ["#FF2700", "#008FD5", "#77AB43", "#636464", "#C4C4C4"],
    "Red black" : ["#E10033", "#000000", "#767676", "#E4E4E4"],
    "Summer" : ["#0266C8", "#F90101", "#F2B50F", "#00933B"],
    "Blue" : ["#6794a7", "#014d64", "#76c0c1", "#01a2d9", "#7ad2f6", "#00887d", "#adadad", "#7bd3f6", "#7c260b", "#ee8f71", "#76c0c1", "#a18376"],
    "All Blue" : ["#6794a7", "#6794a7"],
    "Beach towels" : ["#00AACC", "#FF4E00", "#B90000", "#5F9B0A", "#CD6723"],
    "Light pink" : ["#eee3e7","#ead5dc","#eec9d2","#f4b6c2","#f6abb6"],
    "Beautiful blues" : ["#011f4b","#03396c","#005b96","#6497b1","#b3cde0"],
    "Rainbow dash" : ["#ee4035","#f37736","#fdf498","#7bc043","#0392cf"],
    "Cappuccino" : ["#4b3832","#854442","#fff4e6","#3c2f2f","#be9b7b"]
  }


  public static SINGLE_VALUE_DATA_TYPE_LIST: ChartWidgetOptionItem[] = [
    {
      name : 'Sender address',
      id : 'SENDER_ADDRESS',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'sender_address'
    },
    {
      name : 'Recipient address',
      id : 'RECIPIENTS_ADDRESSES',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'recipient_address'
    },
    {
      name : 'Sender domain',
      id : 'SENDER_DOMAIN',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'sender_domain'
    },
    {
      name : 'Recipient domain',
      id : 'RECIPIENTS_DOMAINS',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'recipient_domain'
    },
    {
      name: 'File',
      id: 'FILE_ID',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC","SIZE;SUM;DESC"],
      discoverName: 'all_files'
    },
    {
      name: 'File group',
      id: 'USER_GROUP_ID',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'groups'
    },
    {
      name: 'Data role',
      id: 'TAGS_2',
      prefix : 'frole.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'functional_roles'
    },
    {
      name: 'Acl write',
      id: 'ACL_WRITE',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'acl_writes'
    },
    {
      name : 'Acl read',
      id : 'ACL_READ',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'acl_reads'
    },
    {
      name : 'Root folder',
      id : 'ROOT_FOLDER_ID',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'folders'
    },
    {
      name : 'File extension',
      id : 'EXTENSION',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'extensions'
    },
    {
      name: 'Owner',
      id : 'OWNER',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'owners'
    },
    {
      name : 'Folder list',
      id : 'FOLDER_ID',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'folders_flat'
    },
    {
      name : 'DocType',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'dt.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'doc_types'
    },
    {
      name : 'Tag',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'g.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'tags'
    },
    {
      name : 'Pattern match',
      id : 'MATCHED_PATTERNS_SINGLE',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'patterns'
    },
    {
      name : 'Pattern match multi',
      id : 'MATCHED_PATTERNS_MULTI',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'patterns_multi'
    },
    {
      name : 'Department list',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'dept.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'departments'
    },
    {
      name : 'Clients association',
      id : 'BIZLIST_ITEM_ASSOCIATION',
      prefix : 'bli.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'bizlist_items_clients'
    },
    {
      name : 'Clients extraction',
      id : 'EXTRACTED_ENTITIES_IDS',
      prefix : 'bli.1.',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'bizlist_item_clients_extractions'
    },
    {
      name : 'Duplicate file',
      id : 'CONTENT_ID',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'contents'
    },
    {
      name : 'File metadata',
      id : 'DA_METADATA',
      anotherOptionListFilter: ["FILE_ID;COUNT;DESC"],
      discoverName: 'metadatas'
    }
  ].sort(DashboardService.sortOptionItemByName);


  public static chartDisplayTypeList: ChartWidgetOptionItem[] = [
    {
      name : "Bar",
      id: {
        chartDisplayType : ChartDisplayType.BAR,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-bars",
      description : "Multi series horizontal bar chart"
    },
    {
      name : "Column",
      id: {
        chartDisplayType : ChartDisplayType.COLUMN,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-bar-chart",
      description : "Multi series vertical bar chart"
    },
    {
      name : "Stacked column",
      id: {
        chartDisplayType : ChartDisplayType.STACKED_COLUMN,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-th",
      description : "Multi series vertical bar chart"
    },
    {
      name : "Line",
      icon : "fa fa-line-chart",
      id: {
        chartDisplayType : ChartDisplayType.LINE,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      description : "Multi series line chart"
    },
    {
      name : "Area",
      id: {
        chartDisplayType : ChartDisplayType.AREA,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-area-chart",
      description : "Multi series area chart"
    },
    {
      name : "Grid",
      id: {
        chartDisplayType : ChartDisplayType.GRID,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-table",
      description : "Multi series data table"
    },
    {
      name : "Pie",
      id: {
        chartDisplayType : ChartDisplayType.PIE,
        chartType : DashboardService.MULTIPLE_FIXED_HISTOGRAM
      },
      icon : "fa fa-pie-chart",
      onlyForSingleSeries : true,
      description : "Single series pie chart"
    },
    {
      name : "Single value",
      id: {
        chartDisplayType : ChartDisplayType.SINGLE_VALUE,
        chartType : DashboardService.SINGLE_VALUE_CHART
      },
      icon : "fa fa-align-center",
      description : "Single accumulated value"
    },
    {
      name : "Gauge",
      id: {
        chartDisplayType : ChartDisplayType.GAUGE,
        chartType : DashboardService.SINGLE_VALUE_CHART
      },
      icon : "fa fa-adjust",
      needFilters: true,
      description : "Single accumulated value as percentage"
    }
  ];


  public static DATE_PARTITION_FIELDS_BY_DATA_TYPE: any = {
    'CREATION_DATE' : {
        name : 'Created date partition',
        id : 'CREATION_DATE',
        prefix : 'datePartition',
        discoverName: 'file_created_dates',
        isDatePartition: true
    },
    'LAST_ACCESS' : {
      name : 'Accessed date partition',
      id : 'LAST_ACCESS',
      prefix : 'datePartition',
      discoverName: 'file_accessed_dates',
      isDatePartition: true
    },
    'LAST_MODIFIED' : {
      name : 'Modified date partition',
      id : 'LAST_MODIFIED',
      prefix : 'datePartition',
      discoverName: 'file_modified_dates',
      isDatePartition: true
    }
  }


  public static MULTIPLE_HISTOGRAM_DATA_TYPE_LIST: ChartWidgetOptionItem[] = [
    {
      name : 'File size',
      id : 'SIZE_PARTITION',
      discoverName: 'file_sizes',
      filterField :  'file.size-partition'
    },
    {
      name : 'Sender address',
      id : 'SENDER_ADDRESS',
      discoverName: 'sender_address',
      filterField :  'file.senderAddress'
    },
    {
      name : 'Recipient address',
      id : 'RECIPIENTS_ADDRESSES',
      discoverName: 'recipient_address',
      filterField :  'file.recipientAddress'
    },
    {
      name : 'Sender domain',
      id : 'SENDER_DOMAIN',
      discoverName: 'sender_domain',
      filterField :  'file.senderDomain'
    },
    {
      name : 'Recipient domain',
      id : 'RECIPIENTS_DOMAINS',
      discoverName: 'recipient_domain',
      filterField :  'file.recipientDomain'
    },
    {
      name : 'Read share permission',
      id : 'SHARED_PERMISSION',
      prefix: 'readSharePermission',
      discoverName: 'share_permissions_allow_read',
      filterField :  'sharedPermission'
    },
    {
      name: 'File group',
      id: 'USER_GROUP_ID',
      discoverName: 'groups',
      filterField :  'groupId'
    },
    {
      name: 'Data role',
      id: 'TAGS_2',
      prefix : 'frole.',
      discoverName: 'functional_roles',
      filterField :  'file.functionalRoleAssociation'
    },
    {
      name: 'Acl write',
      id: 'ACL_WRITE',
      discoverName: 'acl_writes',
      filterField :  'aclWriteId'
    },
    {
      name : 'Acl read',
      id : 'ACL_READ',
      discoverName: 'acl_reads',
      filterField :  'aclReadId'
    },
    {
      name : 'Root folder',
      id : 'ROOT_FOLDER_ID',
      discoverName: 'folders',
      filterField :  'file.rootFolderId'
    },
    {
      name : 'File extension',
      id : 'EXTENSION',
      discoverName: 'extensions',
      filterField :  'extension'
    },
    {
      name: 'Owner',
      id : 'OWNER',
      discoverName: 'owners',
      filterField :  'ownerId'
    },
    {
      name : 'Folder list',
      id : 'FOLDER_ID',
      discoverName: 'folders_flat',
      filterField :  'folder.subfolders'
    },
    {
      name : 'DocType',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'dt.',
      discoverName: 'doc_types',
      filterField :  'file.docType'
    },
    {
      name : 'Tag',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'g.',
      discoverName: 'tags',
      filterField :  'file.tags'
    },
    {
      name : 'Pattern match',
      id : 'MATCHED_PATTERNS_SINGLE',
      discoverName: 'patterns',
      filterField :  'patternId'
    },
    {
      name : 'Pattern match multi',
      id : 'MATCHED_PATTERNS_MULTI',
      discoverName: 'patterns_multi',
      filterField :  'patternMultiId'
    },
    {
      name : 'Department list',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'dept.',
      discoverName: 'departments_flat',
      filterField :  'file.department'
    },
    {
      name : 'Department tree',
      id : 'ACTIVE_ASSOCIATIONS',
      prefix : 'deptTree',
      discoverName: 'departments',
      filterField :  'file.subDepartment'
    },
    {
      name : 'Duplicate file',
      id : 'CONTENT_ID',
      discoverName: 'contents',
      filterField :  'file.contentId'
    },
    {
      name : 'Creation date',
      id : 'CREATION_DATE',
      discoverName: 'file_created_dates',
      isDateField: true
    },
    {
      name : 'Accessed date',
      id : 'LAST_ACCESS',
      discoverName: 'file_accessed_dates',
      isDateField: true
    },
    {
      name : 'Modified date',
      id : 'LAST_MODIFIED',
      discoverName: 'file_modified_dates',
      isDateField: true
    },
/*     {
      name : 'Created date partition',
      id : 'CREATION_DATE',
      prefix : 'datePartition',
      discoverName: 'file_created_dates',
      isDatePartition: true
    },
    {
      name : 'Modified date partition',
      id : 'LAST_MODIFIED',
      prefix : 'datePartition',
      discoverName: 'file_modified_dates',
      isDatePartition: true
    },
    {
      name : 'Accessed date partition',
      id : 'LAST_ACCESS',
      prefix : 'datePartition',
      discoverName: 'file_accessed_dates',
      isDatePartition: true
    },
*/
    {
      name : 'Clients association',
      id : 'BIZLIST_ITEM_ASSOCIATION',
      prefix : 'bli.',
      discoverName: 'bizlist_items_clients',
      filterField :  'bizListItemAssociationId'
    },
    {
      name : 'Clients extraction',
      id : 'EXTRACTED_ENTITIES_IDS',
      prefix : 'bli.1.',
      discoverName: 'bizlist_item_clients_extractions',
      filterField :  'bizListItemExtractionId'
    },
    {
      name : 'Regulation',
      id : 'REGULATION_ID',
      prefix : 'regulation',
      discoverName: 'regulations',
      filterField :  'regulationId'
    },
    {
      name : 'File type category',
      id : 'FILE_TYPE_CATEGORY_ID',
      prefix : 'fileTypeCategory',
      discoverName: 'file_type_categories',
      filterField :  'fileTypeCategoryId'
    },
    {
      name : 'File metadata',
      id : 'DA_METADATA',
      discoverName: 'metadatas',
      filterField :  'file.metadataType'
    },
  ].sort(DashboardService.sortOptionItemByName);

  constructor(private http: HttpClient, private i18nService : I18nService, private serverExportService : ServerExportService, private datePipe: DatePipe, private appConfig: AppConfig)
  {
    if (appConfig.isInstallationModeLegal()) {

      let singleValueDepartmentItem = DashboardService.SINGLE_VALUE_DATA_TYPE_LIST.filter((dataTypeItem => dataTypeItem.name=="Department list"))[0];
      if (singleValueDepartmentItem) {
        singleValueDepartmentItem.name = "Matter list";
        singleValueDepartmentItem.discoverName = "matters_flat";
      }

      let multipleValueDepartmentListItem = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter((dataTypeItem => dataTypeItem.name=="Department list"))[0];
      if (multipleValueDepartmentListItem) {
        multipleValueDepartmentListItem.name = "Matter list";
        multipleValueDepartmentListItem.discoverName = "matters_flat";
      }

      let multipleValueDepartmentTreeItem = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter((dataTypeItem => dataTypeItem.name=="Department tree"))[0];
      if (multipleValueDepartmentTreeItem) {
        multipleValueDepartmentTreeItem.name = "Matter tree";
        multipleValueDepartmentTreeItem.discoverName = "matters";
      }
    }
  }

  public static getColorPalete(palleteId:string) {
    return this.dashboardColorsMap[palleteId];
  }

  getUserDocumentStatisticsRequest() {
    return this.http.get('/api/reports/user/documents/statistics').pipe(map((result:DTOSummaryReport) => {
      if (result) {
        return result.counterReportDtos;
      }
      return null;
    }));
  }

  getDataTypeByDiscoverName(discoverName:string):ChartWidgetOptionItem {
    let filteredList:ChartWidgetOptionItem[] = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter(item => item.discoverName == discoverName);
    if (filteredList.length > 0 ) {
      return filteredList[0];
    }
    return null;
  }

  getSingleDataTypeByDiscoverName(discoverName:string):ChartWidgetOptionItem {
    let filteredList:ChartWidgetOptionItem[] = DashboardService.SINGLE_VALUE_DATA_TYPE_LIST.filter(item => item.discoverName == discoverName);
    if (filteredList.length > 0 ) {
      return filteredList[0];
    }
    return null;
  }


  getDisplayTypeById(displayTypeId:string):ChartWidgetOptionItem {
    let filteredList:ChartWidgetOptionItem[] = DashboardService.chartDisplayTypeList.filter(item => item.id.chartDisplayType == displayTypeId);
    if (filteredList.length > 0 ) {
      return filteredList[0];
    }
    return null;
  }


  getDataTypeObjectByIdAndPrefix(id:string, prefix : string): any  {
    let notCheckPrefix = (id == "CREATION_DATE" || id == "LAST_ACCESS" || id  == "LAST_MODIFIED");
    return DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter((item) => {
      return (item.id == id && (notCheckPrefix || !prefix  || prefix == item.prefix));
    })[0]
  }


  getIconOfUserViewDataWidget(item:UserViewDataDto){
    let chartInfo: ChartInfo<FixedHistogramDataDto,FixedHistogramBeforeTransform> = JSON.parse(item.displayData);
    let foundDisplayType = DashboardService.chartDisplayTypeList.filter((item: ChartWidgetOptionItem)=> {
      return ((item.id.chartDisplayType == chartInfo.displayTypes[0]) && (item.id.chartType == chartInfo.chartType));
    });
    if (foundDisplayType.length > 0 ){
      return foundDisplayType[0].icon;
    }
    return "fa fa-window-maximize";
  }

  static getFieldDiscoverName(chartType:string, fieldId:string, prefix:string):string {
    if (chartType == ChartType.SUNBURST_CHART) {
      return this.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter(dataTypeItem => dataTypeItem.id == 'FOLDER_ID')[0].discoverName;
    }
    else if (chartType == ChartType.SINGLE_VALUE_CHART) {
      return this.SINGLE_VALUE_DATA_TYPE_LIST.filter(dataTypeItem => dataTypeItem.id == fieldId && dataTypeItem.prefix == prefix)[0].discoverName;
    }
    else if (fieldId == "LAST_ACCESS" || fieldId == "LAST_MODIFIED" || fieldId == "CREATION_DATE") {
      return this.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter(dataTypeItem => dataTypeItem.id == fieldId)[0].discoverName;
    }
    else {
      return this.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter(dataTypeItem => dataTypeItem.id == fieldId && dataTypeItem.prefix == prefix)[0].discoverName;
    }
  }

  getFilesAccessHistogram(filterByRole: boolean = false){
    if (filterByRole) {
      return this.http.get('/api/reports/file/access/histogram/role?files=true');
    }
    else {
      return this.http.get('/api/reports/file/access/histogram');
    }
  }
  getGroupSizeHistogramRequest() {
    return this.http.get('/api/reports/group/size/histogram?files=true');
  }

  getFileTypeHistogramRequest(filterByRole: boolean = false) {
    if (filterByRole) {
      return this.http.get('/api/reports/file/types/histogram/role?files=true');
    }
    else {
      return this.http.get('/api/reports/file/types/histogram');
    }
  }

  getScanStatisticsRequest() {
    return this.http.get('/api/reports/scan/statistics').pipe(map((result:DTOSummaryReport) => {
      if (result) {
        return result.counterReportDtos;
      }
      return null;
    }));
  }

  getAdminStatisticsRequest() {
    return this.http.get('/api/reports/admin/documents/statistics').pipe(map((result:DTOSummaryReport) => {
      if (result) {
        return result.counterReportDtos;
      }
      return null;
    }));
  }


  getScanCriteriaRequest() {
    return this.http.get('/api/reports/scan/criteria').pipe(map((result:DTOSummaryReport) => {
      if (result) {
        return result.multiValueListDto;
      }
      return null;
    }));
  }

  getScanConfigurationRequest() {
    return this.http.get('/api/reports/scan/configuration');
  }


  parseCountList = function(fieldsList:any[], counterReportDtos:DtoCounterReport[]) {
    let ans:DtoCounterReport[] = [];
    fieldsList.forEach(fieldObj=>{
      if (fieldObj.field === '') {
        ans.push({
          id: null,
          name: null,
          description: null,
          counter: null
        });
      }
      else {
        let foundItem = counterReportDtos.filter(crDto => crDto.name === fieldObj.field);
        if (foundItem.length > 0) {
          ans.push({
            id: foundItem[0].id,
            name: fieldObj.text,
            description: foundItem[0].description,
            counter: foundItem[0].counter
          });
        }
      }
    })
    return ans;
  }

  getCounter = function(counterReportDtos:DtoCounterReport[], itemName:string, displayName : string = null) : DtoCounterReport {
    for (var i=0;i<counterReportDtos.length;++i) {
      var item:DtoCounterReport = counterReportDtos[i];
      if (item.name==itemName) {
        let newItem = _.cloneDeep(item);
        if (displayName) {
          newItem.name = displayName;
        }
        return newItem;
      }
    }
    return null;
  };



  translateDtoMultiValueList = (val:DtoMultiValueList):DtoMultiValueList => {
    var res:DtoMultiValueList = <DtoMultiValueList>{
      name: val.name,
      type: val.type,
      data: [],
    };

    for (var i:number=0;i<val.data.length;++i) {
      var counterItem:DtoCounterReport = val.data[i];
      let nameTranslate = this.i18nService.translateId(counterItem.name);
      res.data.push({
        counter:counterItem.counter,
        name:nameTranslate ? nameTranslate : counterItem.name,
        description:counterItem.description
      });
    }
    return res;
  };

  unique = function (arr:any[],func?) {
    if (arr && arr.length > 0) {
      var a = [], l = arr.length;
      for (var i = 0; i < l; i++) {
        for (var j = i + 1; j < l; j++) {
          if(func)
          {
            if(func(arr[i]) == func(arr[j]))
            {
              j = ++i
            }
          }
          else {
            if (arr[i] === arr[j]) {
              j = ++i
            }
            ;
          }
        }
        a.push(arr[i]);
      }
      return a;
    }
    return null;
  };

  getDashboardWidgetChartData(chartType  : ChartType, params : any, filter : FilterDescriptor): Observable<DashboardChartDataDto> {
    let hParams = new HttpParams();
    if (params) {
      Object.keys(params).forEach((key) => {
        hParams = hParams.set(key ,params[key]);
      });
    }
    if (filter) {
      hParams = hParams.set("filter" ,JSON.stringify(filter));
    }
    return <Observable<DashboardChartDataDto>>this.http.get('/api/dashboardchart/'+ chartType.toString()+'/data',{
      params : hParams
    });
  }


  exportDashboardWidgetChartData(chartType  : ChartType, params : any, chartName:string, totalItemsNum: number, suggestedNum : number) {
    params.chartType= chartType.toString();
    this.serverExportService.addExportFile(chartName + '_'+this.datePipe.transform((new Date()).getTime(),CommonConfig.DATE_FORMAT),ExportType.DASHBOARD_CHART,params,{
      itemsNumber : totalItemsNum
    },totalItemsNum != null && totalItemsNum > suggestedNum, suggestedNum);
  }


  convertToChartInfoItem(userViewData:UserViewDataDto): ChartInfo<any,any> {
    const dashboardWidgetItem: ChartInfo<any,any> = JSON.parse(userViewData.displayData);
    return dashboardWidgetItem;
  }


  refreshAllDasgboardWidgets(){
    this.refreshChartsEvent.next();
  }

  getWidgetChartDataQueryParams(additionalQueryParams:FixedHistogramDataDto , beforeTransformQueryParams:FixedHistogramBeforeTransform): any  {
    let ans: FixedHistogramDataDto = _.cloneDeep(additionalQueryParams);
    if (beforeTransformQueryParams) {
      if (beforeTransformQueryParams.segmentList) {
        ans.segmentList = JSON.stringify(beforeTransformQueryParams.segmentList.map(segmentItem => segmentItem.filterDescriptor));
      }
      if (beforeTransformQueryParams.filterField) {
        ans.filterField = JSON.stringify(beforeTransformQueryParams.filterField.filterDescriptor);
      }
    }
    return ans;
  }


  getNextChartType(chartType:ChartType) : ChartType {
    if (chartType == ChartType.SUNBURST_CHART) {
      return ChartType.MULTIPLE_FIXED_HISTOGRAM;
    }
    return ChartType.SUNBURST_CHART;
  }

  getDatePartitionListRequest(): Observable<DateRangePartitionDto[]> {
    return this.http.get('/api/daterange/partitions').pipe(map((result:any) => {
      if (result) {
        return result.content;
      }
      return null;
    }));
  }

}

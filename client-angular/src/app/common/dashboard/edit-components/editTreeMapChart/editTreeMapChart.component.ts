import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  ChartInfo,
  ChartWidgetOptionItem, DashboardChartDataDto, DashboardChartValueTypeDto, FixedHistogramBeforeTransform,
   SunburstdHistogramDataDto, SunburstdHistogramDataDtoBeforeTransform
} from "@app/features/dashboard/types/dashboard-interfaces";
import {
  ChartDisplayType,
  ChartType,
  ChartValueType,
  SORT_DIRECTION, VALUE_FUNCTION
} from "@app/features/dashboard/types/dashboard-enums";
import {EFilterDisplayType, SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {DaSelectInputComponent} from "@shared-ui/components/da-select-input/daSelectInput.component";
import {BaseEditWidgetComponent} from "@core/base/baseEditWidgetComponent";

@Component({
  selector: 'edit-tree-map-chart',
  templateUrl: './editTreeMapChart.component.html',
  styleUrls: ['./editTreeMapChart.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EditTreeMapChartComponent extends BaseEditWidgetComponent  {

  public userViewDataDtoItem: UserViewDataDto = null;
  public title: string = "";
  public  data:{userView : UserViewDataDto, filterList: SavedFilterDto[], containerId:number, globalFilter:boolean, filter?: SavedFilterDto, minDepth?: any};
  public selectedDataUnit:ChartWidgetOptionItem = null;
  public selectedDisplayType:ChartWidgetOptionItem = null;
  public selectedFilter: SavedFilterDto = DaSelectInputComponent.NONE_VALUE_ITEM;
  public selectedColorPalete:ChartWidgetOptionItem = null;
  public withPagenation: boolean = false;
  public withLabels:boolean = true;
  public showOther: boolean = true;
  public mainLoading: boolean = false;
  public itemNumber:string = '10';
  public maxDepth:string = '8';
  public minDepth:string = null;
  public maxChildItems:string = null;
  public filterList:SavedFilterDto[] = [];
  public chartInfo: ChartInfo<SunburstdHistogramDataDto,SunburstdHistogramDataDtoBeforeTransform> = null;
  public loading:boolean = false;
  public activeTab:string = null;


  ChartDisplayType =  ChartDisplayType;
  DashboardService = DashboardService;

  colorPaletteList:ChartWidgetOptionItem[] = Object.keys(DashboardService.dashboardColorsMap).map((colorName) => {
    return {
      name : colorName ,
      id  : colorName
    }
  });
  chartData:DashboardChartDataDto = null;
  dataUnitList : ChartWidgetOptionItem[] = [
    {
      name : "File count",
      id: {
        solrField: "FILE_ID" ,
        solrFunction : "COUNT" ,
        sortDirection : "DESC"
      },
      axisText: "Files"
    },
    {
      name : "File size",
      id: {
        solrField: "SIZE" ,
        solrFunction : "SUM" ,
        sortDirection : "DESC"
      },
      axisText: "Volume"
    }
  ];



  groupByList : ChartWidgetOptionItem[] = [
    {
      name : "Folder",
      id: "0"
    }]


  maxChildItemsList : ChartWidgetOptionItem[] = [
    {
      name : "2",
      id: "2"
    },
    {
      name : "3",
      id: "3"
    },
    {
      name : "4",
      id: "4"
    },
    {
      name : "5",
      id: "5"
    },
    {
      name : "6",
      id: "6"
    },
    {
      name : "7",
      id: "7"
    },
    {
      name : "8",
      id: "8"
    },
    {
      name : "9",
      id: "9"
    },
    {
      name : "10",
      id: "10"
    },
    {
      name : "11",
      id: "11"
    },
    {
      name : "12",
      id: "12"
    },
    {
      name : "13",
      id: "13"
    },
    {
      name : "14",
      id: "14"
    },
    {
      name : "15",
      id: "15"
    },
    {
      name : "16",
      id: "16"
    },
    {
      name : "17",
      id: "17"
    },
    {
      name : "18",
      id: "18"
    },
    {
      name : "19",
      id: "19"
    },
    {
      name : "20",
      id: "20"
    },
    {
      name : "21",
      id: "21"
    },
    {
      name : "22",
      id: "22"
    }
  ];


  minDepthList : ChartWidgetOptionItem[] = [
    {
      name : "0",
      id: null
    },
    {
      name : "1",
      id: "1"
    },
    {
      name : "2",
      id: "2"
    }
  ];


  maxDepthList : ChartWidgetOptionItem[] = [
    {
      name : "3",
      id: "3"
    },
    {
      name : "4",
      id: "4"
    },
    {
      name : "5",
      id: "5"
    },
    {
      name : "6",
      id: "6"
    },
    {
      name : "7",
      id: "7"
    },
    {
      name : "8",
      id: "8"
    },
    {
      name : "9",
      id: "9"
    },
    {
      name : "10",
      id: "10"
    },
    {
      name : "All levels",
      id: null
    }
  ];


  itemNumberList: ChartWidgetOptionItem[] = [
    {
      name : "5",
      id: "5"
    },
    {
      name : "10",
      id: "10"
    },
    {
      name : "20",
      id: "20"
    },
    {
      name : "30",
      id: "30"
    },
    {
      name : "40",
      id: "40"
    },
    {
      name : "50",
      id: "50"
    }
  ];

  chartDisplayTypeList  : ChartWidgetOptionItem[] = [
    {
      name : "Sunburst",
      id: ChartDisplayType.SUNBURST,
      icon : "fa fa-bars",
      description : "Sunburst chart"
    },
    {
      name : "Hit map",
      id: ChartDisplayType.TREE_MAP,
      icon : "fa fa-bar-chart",
      description : "Hit map chart"
    }
  ]

  @ViewChild('mainParamForm') public mainParamForm: NgForm = null;


  constructor(private dashboardService: DashboardService) {
    super(dashboardService);
    this.colorPaletteList.unshift({
      id : null,
      name : "Default" });
  }

  convertToSavedFilterItem(userViewData:UserViewDataDto):SavedFilterDto {
    return userViewData.filter;
  }


  onColorPaleteChanged =  (newColorPalete:any) => {
    this.selectedColorPalete = newColorPalete;
    this.updatePreview(this.chartInfo);
  };


  onItemNumberChanged =  (newItemNumber:string) => {
    let theActualNumber = Number(newItemNumber);
    if (!isNaN(theActualNumber)  && Number.isInteger(theActualNumber)  && (theActualNumber > 0) && (theActualNumber <= 100) && (this.itemNumber != newItemNumber)) {
      this.itemNumber = newItemNumber;
      this.updatePreview(this.chartInfo);
    }
  };

  onMaxDepthChanged =  (newMaxDepth:string) => {
    if (newMaxDepth === null) {
      this.maxDepth = null;
    }
    else {
      let theActualNumber = Number(newMaxDepth);
      if (!isNaN(theActualNumber) && Number.isInteger(theActualNumber) && (theActualNumber > 0) && (theActualNumber <= 100) && (this.maxDepth != newMaxDepth)) {
        this.maxDepth = newMaxDepth;
      }
    }
    this.updatePreview(this.chartInfo);
  };

  onMaxChildItemsChanged = (newMaxChildItems:string)=> {
    if (newMaxChildItems === null) {
      this.maxChildItems = null;
    }
    else {
      let theActualNumber = Number(newMaxChildItems);
      if (!isNaN(theActualNumber) && Number.isInteger(theActualNumber) && (theActualNumber > 0) && (theActualNumber <= 100) && (this.maxChildItems != newMaxChildItems)) {
        this.maxChildItems = newMaxChildItems;
      }
    }
    this.updatePreview(this.chartInfo);
  }


  onMinDepthChanged =  (newMinDepth:string) => {
    if (newMinDepth === null) {
      this.minDepth = null;
    }
    else {
      let theActualNumber = Number(newMinDepth);
      if (!isNaN(theActualNumber) && Number.isInteger(theActualNumber) && (theActualNumber > 0) && (theActualNumber <= 100) && (this.minDepth != newMinDepth)) {
        this.minDepth = newMinDepth;
      }
    }
    this.updatePreview(this.chartInfo);
  };


  onDisplayTypeChanged = (newDisplayType:any) => {
    this.selectedDisplayType = newDisplayType;
    this.updatePreview(this.chartInfo);
  };

  onDataUnitChanged = (newDataUnit:any) => {
    this.selectedDataUnit = newDataUnit;
    this.updatePreview(this.chartInfo);
  };


  onFilterChanged = (selectFilter:any)=>{
    this.selectedFilter = selectFilter;
    this.updatePreview(this.chartInfo);
  };



  getItemByDashboardChartValueTypeDto(itemList: ChartWidgetOptionItem[], solrField:string, solrFunction: string, sortDirection: string):ChartWidgetOptionItem {
    if (!solrFunction) {
      return itemList[0];
    }
    return itemList.filter(item => (item.id.solrField == solrField && item.id.solrFunction == solrFunction &&  item.id.sortDirection == sortDirection))[0];
  }

  getOptionItemById(itemList: ChartWidgetOptionItem[], itemId:string):ChartWidgetOptionItem {
    return itemList.filter(item => item.id == itemId)[0];
  }

  getItemByChartDisplayType(itemList: ChartWidgetOptionItem[], chartDisplayType: ChartDisplayType):ChartWidgetOptionItem {
    return itemList.filter(item => (item.id == chartDisplayType))[0];
  }

  setActiveTab = (tabName: string) =>
  {
    this.activeTab = tabName;
  };

  protected setInitData() {
    this.activeTab="chartType";
    this.userViewDataDtoItem=<UserViewDataDto>{};
    this.filterList = this.data.filterList;
    this.withPagenation = false;
    this.withLabels = false;
    this.showOther = true;
    if (this.data.userView) {
      this.userViewDataDtoItem = _.cloneDeep(this.data.userView);
      this.chartInfo = JSON.parse(this.userViewDataDtoItem.displayData);
      this.withPagenation = this.chartInfo.withPagenation;
      this.withLabels = this.chartInfo.withLabels;
      this.showOther = this.chartInfo.additionalQueryParams.returnOther;
      this.selectedDisplayType = this.getItemByChartDisplayType(this.chartDisplayTypeList, this.chartInfo.displayTypes[0]);
      this.selectedDataUnit = this.getItemByDashboardChartValueTypeDto(this.dataUnitList, this.chartInfo.additionalQueryParams.valueField, this.chartInfo.additionalQueryParams.valueFunction, this.chartInfo.additionalQueryParams.valueDirection);
      this.itemNumber = this.chartInfo.additionalQueryParams.take+"";
      this.maxDepth = this.chartInfo.additionalQueryParams.maxDepth ? this.chartInfo.additionalQueryParams.maxDepth + "" : null;
      this.minDepth = this.chartInfo.additionalQueryParams.minDepth ? this.chartInfo.additionalQueryParams.minDepth + "" : null;
      this.maxChildItems = this.chartInfo.additionalQueryParams.maxChildItems ? this.chartInfo.additionalQueryParams.maxChildItems + "" : null;
      this.selectedColorPalete = this.getOptionItemById(this.colorPaletteList, ""+this.chartInfo.colorPalete  );
      if (!this.chartInfo.beforeTransformQueryParams) {
        this.chartInfo.beforeTransformQueryParams = {};
      }
      this.selectedFilter = this.chartInfo.beforeTransformQueryParams.filterField ? this.chartInfo.beforeTransformQueryParams.filterField : DaSelectInputComponent.NONE_VALUE_ITEM;
      this.activeTab=this.selectedDisplayType ? "mainParam" : "chartType";
    }
    else  {

      this.selectedDisplayType = this.chartDisplayTypeList[0];
      this.selectedDataUnit = this.dataUnitList[0];
      this.userViewDataDtoItem.name = "";
      this.userViewDataDtoItem.globalFilter = this.data.globalFilter;
      this.itemNumber = "10";
      this.maxDepth = null;
      this.minDepth = null;
      this.maxChildItems = null;
      this.chartInfo ={
        chartType : ChartType.SUNBURST_CHART,
        beforeTransformQueryParams: {
        },
        additionalQueryParams: {
          valueField: "FILE_ID",
          valueDirection: SORT_DIRECTION.DESC,
          valueFunction: VALUE_FUNCTION.COUNT,
          take: 10,
          maxDepth: null,
          minDepth: null
        }
      };
    }
    if (this.data.filter) {
      this.selectedFilter = this.data.filter;
    }
    if (this.data.minDepth){
      this.minDepth = this.data.minDepth;
    }
    if (this.selectedColorPalete == null) {
      this.selectedColorPalete = this.colorPaletteList[0];
    }
    this.userViewDataDtoItem.containerId = (this.data.containerId != -1) ? this.data.containerId : null;
    setTimeout(() => {
      this.updatePreview(this.chartInfo);
    });
  }

  public isValid(onlyForPreview:boolean = false):boolean {
    return (onlyForPreview || this.mainParamForm.valid ) && this.selectedDisplayType != null && this.selectedDataUnit != null && this.selectedFilter != null  && this.itemNumber != null  && this.selectedColorPalete != null;
  }


  updateCurrUserViewDataItem = () => {
    this.userViewDataDtoItem.displayType = EFilterDisplayType.DASHBOARD_WIDGET;
    this.chartInfo.displayTypes = [this.selectedDisplayType.id];
    this.chartInfo.additionalQueryParams.valueField = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrField;
    this.chartInfo.additionalQueryParams.valueFunction = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrFunction;
    this.chartInfo.additionalQueryParams.valueDirection = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).sortDirection;
    this.chartInfo.valueType = null;
    this.chartInfo.yAxisTitle = null;
    let valueField = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrField;
    this.chartInfo.yAxisTitle = this.selectedDataUnit.axisText;
    if (valueField == "SIZE") {
      this.chartInfo.valueType = ChartValueType.FILE_SIZE;
    }
    else if (valueField == "CREATION_DATE" || valueField == "LAST_ACCESS" || valueField == "LAST_MODIFIED") {
      this.chartInfo.valueType = ChartValueType.DATE;
    }
    this.chartInfo.withPagenation = this.withPagenation;
    this.chartInfo.withLabels = this.withLabels;
    this.chartInfo.additionalQueryParams.returnOther = this.showOther;
    this.chartInfo.additionalQueryParams.take = Number(this.itemNumber);
    if (this.maxDepth) {
      this.chartInfo.additionalQueryParams.maxDepth = Number(this.maxDepth);
    }
    else {
      delete this.chartInfo.additionalQueryParams.maxDepth;
    }
    this.chartInfo.additionalQueryParams.minDepth = Number(this.minDepth);
    if (this.maxChildItems) {
      this.chartInfo.additionalQueryParams.maxChildItems = Number(this.maxChildItems);
    }
    else {
      delete this.chartInfo.additionalQueryParams.maxChildItems;
    }
    this.chartInfo.colorPalete = this.selectedColorPalete.id;
    if (this.selectedFilter != DaSelectInputComponent.NONE_VALUE_ITEM) {
      this.selectedFilter.filterDescriptor.name = this.selectedFilter.name;
      this.chartInfo.beforeTransformQueryParams.filterField = this.selectedFilter;
    }
    else {
      delete this.chartInfo.beforeTransformQueryParams.filterField;
    }
    this.userViewDataDtoItem.displayData = JSON.stringify(this.chartInfo);
  }

}

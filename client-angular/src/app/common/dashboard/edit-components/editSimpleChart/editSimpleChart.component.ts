import * as _ from 'lodash';
import {Component, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  ChartInfo,
  ChartWidgetOptionItem,  DashboardChartValueTypeDto, FixedHistogramBeforeTransform,
  FixedHistogramDataDto
} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartDisplayType, ChartType, ChartValueType, SeriesType} from "@app/features/dashboard/types/dashboard-enums";
import {EFilterDisplayType, SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {DaSelectInputComponent} from "@shared-ui/components/da-select-input/daSelectInput.component";
import {BaseEditWidgetComponent} from "@core/base/baseEditWidgetComponent";

@Component({
  selector: 'edit-simple-chart',
  templateUrl: './editSimpleChart.component.html',
  styleUrls: ['./editSimpleChart.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EditSimpleChartComponent extends BaseEditWidgetComponent  {

  public chartInfo: ChartInfo<FixedHistogramDataDto,FixedHistogramBeforeTransform> = null;
  public selectedDataType:ChartWidgetOptionItem = null;
  public selectedDataUnit:ChartWidgetOptionItem = null;
  public selectedSeriesType:ChartWidgetOptionItem = null;
  public selectedSeriesField: ChartWidgetOptionItem = null;
  public selectedFilter: SavedFilterDto = null;
  public selectedSegmentList:SavedFilterDto[] = null;
  public selectedDisplayType:ChartWidgetOptionItem = null;
  public selectedColorPalete:ChartWidgetOptionItem = null;
  public selectedDatePartition:ChartWidgetOptionItem = null;
  public withTotal:boolean = true;
  public withPagenation: boolean = false;
  public withLabels: boolean = true;
  public filterList:SavedFilterDto[] = [];
  public activeTab:string = null;
  public chartTypeValid: boolean = true;
  public mainParamsValid: boolean = true;
  public moreParamsValid: boolean = true;
  public selectedChartDisplayTypeId:ChartDisplayType = null;
  public selectedChartTypeId:ChartType = null;
  public itemNumber:string = null;
  public seriesItemsNum:string = '5';
  public segmentsAreValid:boolean = true;
  public totalDisplayName:string="Unfiltered";
  public returnOther = false;
  public show3d = false;
  public datePartitionList = null;

  SeriesType = SeriesType;
  ChartDisplayType =  ChartDisplayType;
  dataUnitList : ChartWidgetOptionItem[] = [];

  chartDisplayTypeList = DashboardService.chartDisplayTypeList;
  DashboardService = DashboardService;
  colorPaletteList:ChartWidgetOptionItem[] = Object.keys(DashboardService.dashboardColorsMap).map((colorName) => {
    return {
      name : colorName ,
      id  : colorName
    }
  });
  previewIsValid:boolean = false;
  originDataUnitListForMultipleValue : ChartWidgetOptionItem[] = [
    {
      name : "File count",
      id: {
        solrField: "FILE_ID" ,
        solrFunction : "COUNT" ,
        sortDirection : "DESC"
      },
      axisText: "Files"
    },
    {
      name : "File size",
      id: {
        solrField: "SIZE" ,
        solrFunction : "SUM" ,
        sortDirection : "DESC"
      },
      axisText: "Volume"
    },
    {
      name : "Last created date",
      id: {
        solrField: "CREATION_DATE" ,
        solrFunction : "MAX" ,
        sortDirection : "ASC"
      },
      axisText: "Date"
    },
    {
      name : "Last access date",
      id: {
        solrField: "LAST_ACCESS" ,
        solrFunction : "MAX" ,
        sortDirection : "ASC"
      },
      axisText: "Date"
    },
    {
      name : "Last modified date",
      id: {
        solrField: "LAST_MODIFIED" ,
        solrFunction : "MAX" ,
        sortDirection : "ASC"
      },
      axisText: "Date"
    },
    {
      name : "Average access date",
      id: {
        solrField: "LAST_ACCESS" ,
        solrFunction : "AVG" ,
        sortDirection : "ASC"
      },
      axisText: "Date"
    },
    {
      name : "Unique file groups",
      id: {
        solrField: "USER_GROUP_ID" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "File groups"
    },
    {
      name : "Unique folders",
      id: {
        solrField: "FOLDER_ID" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Folders"
    },
    {
      name : "Unique root folders",
      id: {
        solrField: "ROOT_FOLDER_ID" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Root folders"
    },
    {
      name : "Unique ACL read",
      id: {
        solrField: "ACL_READ" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Acl read"
    },
    {
      name : "Unique ACL write",
      id: {
        solrField: "ACL_WRITE" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Acl write"
    },
    {
      name : "Unique Owners",
      id: {
        solrField: "OWNER" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Owners"
    },
    {
      name : "Unique file extensions",
      id: {
        solrField: "EXTENSION" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Extensions"
    },
    {
      name : "Unique patterns",
      id: {
        solrField: "MATCHED_PATTERNS_SINGLE" ,
        solrFunction : "UNIQUE" ,
        sortDirection : "DESC"
      },
      axisText: "Patterns"
    }
  ];

  itemNumberList: ChartWidgetOptionItem[] = [
    {
      name : "1",
      id: "1"
    },
    {
      name : "2",
      id: "2"
    },
    {
      name : "3",
      id: "3"
    },
    {
      name : "4",
      id: "4"
    },
    {
      name : "5",
      id: "5"
    },
    {
      name : "6",
      id: "6"
    },
    {
      name : "7",
      id: "7"
    },
    {
      name : "8",
      id: "8"
    },
    {
      name : "9",
      id: "9"
    },
    {
      name : "10",
      id: "10"
    }
  ];


  public PERIOD_METHOD_TIME_SEGMENTS = "TIME_SEGMENTS";
  public PERIOD_METHOD_PREDEFINED_PARTITIONS = "PREDEFINED_PARTITIONS";

  periodTypeList: ChartWidgetOptionItem[] = [
    {
      name : "Years",
      id: "YEAR",
      type : this.PERIOD_METHOD_TIME_SEGMENTS
    },
    {
      name : "Months",
      id: "MONTH",
      type : this.PERIOD_METHOD_TIME_SEGMENTS
    },
    {
      name : "Days",
      id: "DAY",
      type : this.PERIOD_METHOD_TIME_SEGMENTS
    }
  ];



  periodMethodList: ChartWidgetOptionItem[] = [
    {
      name : "By time segments",
      id: this.PERIOD_METHOD_TIME_SEGMENTS
    },
    {
      name : "By predefined partitions",
      id: this.PERIOD_METHOD_PREDEFINED_PARTITIONS
    }
  ];

  seriesTypeList : ChartWidgetOptionItem[] = [
    {
      name : "Selected manually",
      id: SeriesType.MANUALY
    },
    {
      name : "Top items by field",
      id: SeriesType.BY_TOP_ITEMS
    }];


  originDataUnitListForSingleValue: ChartWidgetOptionItem[] = [
    {
      name : "Count",
      id: {
        solrField: "FILE_ID" ,
        solrFunction : "COUNT" ,
        sortDirection : "DESC"
      },
      axisText: "Files"
    },
    {
      name : "Size",
      id: {
        solrField: "SIZE" ,
        solrFunction : "SUM" ,
        sortDirection : "DESC"
      },
      axisText: "Volume"
    }
  ];
  dataTypeList: ChartWidgetOptionItem[] = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST;
  dataTypeListForSingleValue: ChartWidgetOptionItem[] = DashboardService.SINGLE_VALUE_DATA_TYPE_LIST;
  seriesList: ChartWidgetOptionItem[] = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST.filter((item)=>{
    return (item.filterField != null);
  })

  getOptionItemById(itemList: ChartWidgetOptionItem[], itemId:string):ChartWidgetOptionItem {
    return itemList.filter(item => item.id == itemId)[0];
  }

  getItemByDashboardChartValueTypeDto(itemList: ChartWidgetOptionItem[], solrField:string, solrFunction: string, sortDirection: string):ChartWidgetOptionItem {
    if (!solrFunction) {
      return itemList[0];
    }
    return itemList.filter(item => (item.id.solrField == solrField && item.id.solrFunction == solrFunction &&  item.id.sortDirection == sortDirection))[0];
  }

  getItemByChartDisplayType(itemList: ChartWidgetOptionItem[], chartDisplayType: ChartDisplayType, chartType:ChartType):ChartWidgetOptionItem {
    return itemList.filter(item => (item.id.chartDisplayType == chartDisplayType && item.id.chartType == chartType))[0];
  }

  getOptionItemByIdAndPrefix(itemList: ChartWidgetOptionItem[], itemId:string, prefix:string):ChartWidgetOptionItem {
    return itemList.filter(item => item.id == itemId && item.prefix == prefix)[0];
  }

  @ViewChild('mainParamForm') public mainParamForm: NgForm = null;


  constructor(private dashboardService: DashboardService) {
    super(dashboardService);
    this.colorPaletteList.unshift({
      id : null,
      name : "Default" });
  }

  isTotalFilter(filter:SavedFilterDto) {
    return (!filter.rawFilter);
  }

  onDataTypeChanged = (newDataType:any) => {
    this.selectedDataType = newDataType;
    this.updatePreview(this.chartInfo);
  };

  onSingleDataTypeChanged = (newDataType:any, refreshPreview:boolean = false) => {
    this.selectedDataType = newDataType;
    if (newDataType != null) {
      this.dataUnitList = this.originDataUnitListForSingleValue.filter((dataUnitItem) => {
        let valueTypeDto: DashboardChartValueTypeDto = (<DashboardChartValueTypeDto>dataUnitItem.id);
        return this.selectedDataType.anotherOptionListFilter.indexOf(valueTypeDto.solrField + ";" + valueTypeDto.solrFunction + ";" + valueTypeDto.sortDirection) > -1
      });
      this.selectedDataUnit = null;
      if (this.dataUnitList.length == 1) {
        this.selectedDataUnit = this.dataUnitList[0];
      }
    }
    if (refreshPreview) {
      this.updatePreview(this.chartInfo);
    }
  };


  updateAfterChartTypeChanged = (newChartTypeId:string) => {
    let tmpDataTypeList: ChartWidgetOptionItem[] =  (newChartTypeId == DashboardService.SINGLE_VALUE_CHART ? this.dataTypeListForSingleValue : this.dataTypeList);
    this.dataUnitList = (newChartTypeId == DashboardService.SINGLE_VALUE_CHART ? this.originDataUnitListForSingleValue  : this.originDataUnitListForMultipleValue);
    let tmpFilteredList = tmpDataTypeList.filter((dataTypeItem) => {
      return this.selectedDataType && dataTypeItem.id ==  this.selectedDataType.id && dataTypeItem.prefix == this.selectedDataType.prefix;
    });
    if (tmpFilteredList.length == 0 ) {
      this.selectedDataType = null;
      this.selectedDataUnit = null;
    }
    else {
      this.selectedDataType = tmpFilteredList[0];
    }
    if (newChartTypeId == DashboardService.SINGLE_VALUE_CHART && this.selectedDataType) {
      this.onSingleDataTypeChanged(this.selectedDataType);
    }
    if (this.selectedDataUnit != null) {
      let tmpFilteredUnitList = this.dataUnitList.filter(dataUnitItem => (dataUnitItem.id.solrFunction  == this.selectedDataUnit.id.solrFunction &&  dataUnitItem.id.solrField  == this.selectedDataUnit.id.solrField ));
      if (tmpFilteredUnitList.length > 0) {
        this.selectedDataUnit = tmpFilteredUnitList[0];
      }
      else {
        if (this.dataUnitList.length == 1 ){
          this.selectedDataUnit = this.dataUnitList[0];
        }
        else {
          this.selectedDataUnit = null;
        }
      }
    }
    else {
      if (this.dataUnitList.length == 1 ){
        this.selectedDataUnit = this.dataUnitList[0];
      }
    }
    this.updatePreview(this.chartInfo);
  };



  onDisplayTypeChanged = (newDisplayType:any) => {
    this.selectedDisplayType = newDisplayType;
    this.selectedChartDisplayTypeId = newDisplayType.id.chartDisplayType;
    this.selectedChartTypeId = newDisplayType.id.chartType;
    if (this.selectedChartDisplayTypeId == ChartDisplayType.PIE || this.selectedChartDisplayTypeId == ChartDisplayType.SINGLE_VALUE || this.selectedChartDisplayTypeId == ChartDisplayType.GAUGE ) {
      if (this.selectedSegmentList.length > 0 ) {
        this.selectedFilter = this.selectedSegmentList[0];
        this.selectedSegmentList = [];
      }
      else if (!this.selectedFilter || this.selectedFilter == DaSelectInputComponent.NONE_VALUE_ITEM) {
        if (this.selectedChartDisplayTypeId == ChartDisplayType.GAUGE ){
          this.selectedFilter = this.filterList[0];
        }
        else
        {
          this.selectedFilter = DaSelectInputComponent.NONE_VALUE_ITEM;
        }
      }
    }
    else {
      if ((!this.selectedSegmentList || this.selectedSegmentList.length == 0 ) && this.selectedFilter && this.selectedFilter != DaSelectInputComponent.NONE_VALUE_ITEM) {
        this.selectedSegmentList = [this.selectedFilter];
      }
      this.selectedFilter = null;
    }
    this.updateAfterChartTypeChanged(this.selectedChartTypeId);
  };



  onColorPaleteChanged =  (newColorPalete:any) => {
    this.selectedColorPalete = newColorPalete;
    this.updatePreview(this.chartInfo);
  };



  onSeriesItemsNumberChanged =  (newSeriesItemsNumber:string) => {
    let theActualNumber = Number(newSeriesItemsNumber);
    if (!isNaN(theActualNumber)  && Number.isInteger(theActualNumber)  && (theActualNumber > 0) && (theActualNumber <= 100) && (this.seriesItemsNum != newSeriesItemsNumber)) {
      this.seriesItemsNum = newSeriesItemsNumber;
      this.updatePreview(this.chartInfo);
    }
  };


  onItemNumberChanged =  (newItemNumber:string) => {
    let theActualNumber = Number(newItemNumber);
    if (!isNaN(theActualNumber)  && Number.isInteger(theActualNumber)  && (theActualNumber > 0) && (theActualNumber <= 100) && (this.itemNumber != newItemNumber)) {
      this.itemNumber = newItemNumber;
      this.updatePreview(this.chartInfo);
    }
  };

  onDatePartitionChanged =  (newDatePartition:any) => {
    this.selectedDatePartition = newDatePartition;
    this.updatePreview(this.chartInfo);
  };


  onDataUnitChanged = (newDataUnit:any) => {
    this.selectedDataUnit = newDataUnit;
    this.updatePreview(this.chartInfo);
  };

  onSeriesFieldChanged = (newSeriesField:any) => {
    this.selectedSeriesField= newSeriesField;
    this.updatePreview(this.chartInfo);
  };


  onSeriesTypeChanged = (newSeriesType:any) => {
    this.selectedSeriesType = newSeriesType;
    if (newSeriesType.id != SeriesType.MANUALY) {
      this.selectedSegmentList = [];
    }
    this.updatePreview(this.chartInfo);
  };

  onSegmentChanged = (selectSegment:SavedFilterDto,index: number)=>{
    this.selectedSegmentList[index] = _.cloneDeep(selectSegment);
    this.selectedSegmentList[index].filterDescriptor.name = this.selectedSegmentList[index].name;
    this.updatePreview(this.chartInfo);
  };

  onFilterChanged = (selectFilter:any)=>{
    this.selectedFilter = selectFilter;
    this.updatePreview(this.chartInfo);
  };

  removeSegment = (index:number)=>{
    this.selectedSegmentList.splice(index, 1);
    this.updatePreview(this.chartInfo);
  };


  gotEmptySegmentFilter = (sList)=>{
    if (sList == null ) {
      return true;
    }
    for (let segFilter of sList){
      if (segFilter == null) {
        return true;
      }
    }
    return false;
  }

  gotEmptySegmentNameFilter = (sList)=>{
    if (!sList) {
      return false;
    }
    for (let segFilter of sList){
      if (segFilter && (!segFilter.filterDescriptor || !segFilter.filterDescriptor.name)) {
        return true;
      }
    }
    return false;
  }


  private updateInitDataAfterGetPartitions() {
    this.activeTab="chartType";
    this.userViewDataDtoItem=<UserViewDataDto>{};
    this.filterList = this.data.filterList;
    this.withPagenation = false;
    this.show3d = false;
    this.withLabels = true;
    this.returnOther = false;
    if (this.data.userView) {
      this.userViewDataDtoItem = _.cloneDeep(this.data.userView);
      this.chartInfo = JSON.parse(this.userViewDataDtoItem.displayData);
      this.selectedChartDisplayTypeId = this.chartInfo.displayTypes[0];
      this.selectedChartTypeId = this.chartInfo.chartType;
      this.selectedDisplayType = this.getItemByChartDisplayType(DashboardService.chartDisplayTypeList, this.selectedChartDisplayTypeId, this.selectedChartTypeId);

      if (this.chartInfo.chartType == ChartType.MULTIPLE_FIXED_HISTOGRAM) {
        this.dataUnitList = this.originDataUnitListForMultipleValue;
        this.withPagenation = this.chartInfo.withPagenation;
        this.show3d = this.chartInfo.show3d;
        this.withLabels = this.chartInfo.withLabels;
        this.returnOther = this.chartInfo.additionalQueryParams.returnOther;
        let neededDatePartitionId = null;
        if (this.chartInfo.additionalQueryParams.queryPrefix == "datePartition") {
          this.selectedDataType = this.getOptionItemById(this.dataTypeList, this.chartInfo.additionalQueryParams.groupedField);
          neededDatePartitionId = this.chartInfo.additionalQueryParams.datePartitionId;
        }
        else {
          this.selectedDataType = this.getOptionItemByIdAndPrefix(this.dataTypeList, this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix);
          neededDatePartitionId = this.chartInfo.additionalQueryParams.periodType;
        }
        let filteredList = this.datePartitionList.filter((item:any)=> {
          return (item.id == neededDatePartitionId);
        })
        if (filteredList.length > 0 ) {
          this.selectedDatePartition = filteredList[0];
        }
        else {
          this.selectedDatePartition = this.datePartitionList[0];
        }
        this.selectedDataUnit = this.getItemByDashboardChartValueTypeDto(this.dataUnitList, this.chartInfo.additionalQueryParams.valueField, this.chartInfo.additionalQueryParams.valueFunction, this.chartInfo.additionalQueryParams.valueDirection);
        this.itemNumber = this.chartInfo.additionalQueryParams.take+"";
        this.seriesItemsNum = this.chartInfo.additionalQueryParams.seriesItemsNum ? this.chartInfo.additionalQueryParams.seriesItemsNum +"" : '5';
        this.selectedColorPalete = this.getOptionItemById(this.colorPaletteList, ""+this.chartInfo.colorPalete  );
        //this.selectedPeriodType = this.chartInfo.additionalQueryParams.periodType? this.getOptionItemById(this.periodTypeList, this.chartInfo.additionalQueryParams.periodType) : this.periodTypeList[0];
        if (this.chartInfo.additionalQueryParams.seriesField) {
          this.selectedSeriesType = this.getOptionItemById(this.seriesTypeList, SeriesType.BY_TOP_ITEMS);
          this.selectedSeriesField = this.getOptionItemByIdAndPrefix(this.dataTypeList, this.chartInfo.additionalQueryParams.seriesField, this.chartInfo.additionalQueryParams.seriesFieldPrefix);
          this.selectedSegmentList = [];
        }
        else {
          this.selectedSeriesType = this.getOptionItemById(this.seriesTypeList, SeriesType.MANUALY);
          this.withTotal = this.chartInfo.additionalQueryParams.withTotal;
          let savedFilterList = this.chartInfo.beforeTransformQueryParams.segmentList.filter((savedFilter) => {
            if (this.isTotalFilter(savedFilter)) {
              this.totalDisplayName = savedFilter.filterDescriptor.name;
              return false;
            }
            return true;
          });
          this.selectedSegmentList = _.cloneDeep(savedFilterList);
          this.selectedFilter = this.selectedSegmentList.length > 0 ? this.selectedSegmentList[0] : DaSelectInputComponent.NONE_VALUE_ITEM;
        }
      }
      else if (this.chartInfo.chartType == ChartType.SINGLE_VALUE_CHART) {
        this.dataUnitList = this.originDataUnitListForSingleValue;
        this.onSingleDataTypeChanged(this.getOptionItemByIdAndPrefix(this.dataTypeListForSingleValue, this.chartInfo.additionalQueryParams.groupedField, this.chartInfo.additionalQueryParams.queryPrefix));
        this.selectedDataUnit = this.getItemByDashboardChartValueTypeDto(this.dataUnitList, this.chartInfo.additionalQueryParams.valueField, this.chartInfo.additionalQueryParams.valueFunction, this.chartInfo.additionalQueryParams.valueDirection);
        this.withTotal = this.chartInfo.additionalQueryParams.compareToTotal;
        if (this.chartInfo.beforeTransformQueryParams.filterField) {
          this.selectedFilter = _.cloneDeep(this.chartInfo.beforeTransformQueryParams.filterField);
        }
        else {
          this.selectedFilter = DaSelectInputComponent.NONE_VALUE_ITEM;
        }
        this.selectedSegmentList = [];
        this.itemNumber = this.itemNumberList[0].id;
        this.seriesItemsNum = '5';
        //this.selectedPeriodType = this.periodTypeList[0];
      }
      this.activeTab=this.selectedDisplayType ? "mainParam" : "chartType";
    }
    else  {
      this.userViewDataDtoItem.name = "";
      this.userViewDataDtoItem.globalFilter = this.data.globalFilter;
      this.selectedSegmentList = [];
      this.selectedDisplayType = null;
      //this.selectedPeriodType = this.periodTypeList[0];
      this.selectedSeriesType = this.seriesTypeList[0];
      this.selectedDatePartition = null;
      this.itemNumber = "20";
      this.seriesItemsNum = "5";
      this.chartInfo = {
        beforeTransformQueryParams: {
          segmentList : []
        },
        additionalQueryParams: {
          withTotal:true,
          segmentList: "",
          groupedField: null,
          valueField: null,
          valueDirection: null,
          valueFunction: null,
          datePartitionId: null,
          take: 10,
          periodType: null
        }
      };
    }
    if (this.selectedDatePartition == null) {
      this.selectedDatePartition = this.datePartitionList[0];
    }
    if (this.selectedColorPalete == null) {
      this.selectedColorPalete = this.colorPaletteList[0];
    }
    this.userViewDataDtoItem.containerId = (this.data.containerId != -1) ? this.data.containerId : null;
    if (this.data.discoverName) {
      if (this.data.discoverName == DashboardService.ALL_FILES_DISCOVER_NAME) {
        this.activeTab = "mainParam";
        if (this.data.filter) {
          this.onDisplayTypeChanged(this.dashboardService.getDisplayTypeById(ChartDisplayType.GAUGE));
          this.selectedFilter = this.data.filter;
        }
        else {
          this.onDisplayTypeChanged(this.dashboardService.getDisplayTypeById(ChartDisplayType.SINGLE_VALUE));
        }
        let dataTypeObj: ChartWidgetOptionItem = this.dashboardService.getSingleDataTypeByDiscoverName(this.data.discoverRightName);
        if (dataTypeObj == null) {
          dataTypeObj = this.dashboardService.getSingleDataTypeByDiscoverName('file');
        }
        this.onSingleDataTypeChanged(dataTypeObj);
        this.onDataUnitChanged(this.originDataUnitListForSingleValue[0]);
      }
      else {
        let dataTypeObj: ChartWidgetOptionItem = this.dashboardService.getDataTypeByDiscoverName(this.data.discoverName);
        if (dataTypeObj != null) {
          this.activeTab = "mainParam";
          this.onDisplayTypeChanged(this.dashboardService.getDisplayTypeById(ChartDisplayType.BAR));
          this.onDataTypeChanged(dataTypeObj);
          if (dataTypeObj.isDateField  && this.data.datePartitionId) {
            let filteredList = this.datePartitionList.filter((item:any)=> {
              return (item.id == this.data.datePartitionId);
            })
            if (filteredList.length > 0 ) {
              this.selectedDatePartition = filteredList[0];
            }
          }
          if (this.data.filter) {
            this.selectedSegmentList = [this.data.filter];
          }
          this.onDataUnitChanged(this.originDataUnitListForMultipleValue[0]);
        }
      }
    }
    else {
      this.updatePreview(this.chartInfo);
    }


  }

  protected setInitData() {
    if (this.datePartitionList) {
      this.updateInitDataAfterGetPartitions();
    }
    else {
      this.dashboardService.getDatePartitionListRequest().subscribe((datePartitionList: ChartWidgetOptionItem[]) => {
        datePartitionList.map((datePartitionItem) => {
          datePartitionItem.type = this.PERIOD_METHOD_PREDEFINED_PARTITIONS;
        })
        this.datePartitionList = _.concat([], this.periodTypeList);
        this.datePartitionList.push({});
        this.datePartitionList = _.concat(this.datePartitionList, datePartitionList);
        this.updateInitDataAfterGetPartitions();
      });
    }
  }


  addHistogram = ()=>{
    this.selectedSegmentList.push(null);
  };

  public isValid(onlyForPreview:boolean = false):boolean {
    this.updateValidators(onlyForPreview);
    this.segmentsAreValid = true;
    this.selectedSegmentList.forEach((selSegment)=> {
      if (selSegment == null || !selSegment.filterDescriptor.name) {
        this.segmentsAreValid = false;
      }
    })
    if (!this.totalDisplayName && this.selectedDisplayType && this.selectedChartTypeId == DashboardService.MULTIPLE_FIXED_HISTOGRAM && this.selectedChartDisplayTypeId != ChartDisplayType.PIE ){
      this.segmentsAreValid = false;
    }
    return this.chartTypeValid && this.mainParamsValid && this.moreParamsValid && this.segmentsAreValid ;
  };

  updateValidators = (onlyForPreview:boolean = false)=>  {
    this.mainParamsValid = (onlyForPreview || this.mainParamForm.valid) && this.selectedDataType!=null  && this.selectedDataUnit != null && (this.selectedSeriesType.id != SeriesType.BY_TOP_ITEMS || this.selectedSeriesField != null);
    this.mainParamsValid = this.mainParamsValid  && (!this.selectedDataType.isDateField || this.selectedDatePartition != null);
    this.chartTypeValid = this.selectedDisplayType != null && !this.gotEmptySegmentFilter(this.selectedSegmentList);
    this.moreParamsValid = true;
  }

  setActiveTab = (tabName: string) =>
  {
    this.activeTab = tabName;
  };


  updateCurrUserViewDataItem = () => {
    this.userViewDataDtoItem.displayType = EFilterDisplayType.DASHBOARD_WIDGET;
    this.chartInfo.chartType = this.selectedChartTypeId;
    this.chartInfo.displayTypes = [this.selectedChartDisplayTypeId];
    let selectedDataType:ChartWidgetOptionItem = this.selectedDataType ;
    this.chartInfo.additionalQueryParams.groupedField = selectedDataType.id;
    if (selectedDataType.prefix) {
      this.chartInfo.additionalQueryParams.queryPrefix = selectedDataType.prefix;
    }
    else {
      delete this.chartInfo.additionalQueryParams.queryPrefix;
    }
    this.chartInfo.additionalQueryParams.valueField = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrField;
    this.chartInfo.additionalQueryParams.valueFunction = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrFunction;
    this.chartInfo.additionalQueryParams.valueDirection = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).sortDirection;
    this.chartInfo.valueType = null;
    this.chartInfo.yAxisTitle = null;


    this.chartInfo.xAxisTitle = selectedDataType.name;
    let valueField = (<DashboardChartValueTypeDto>this.selectedDataUnit.id).solrField;
    this.chartInfo.yAxisTitle = this.selectedDataUnit.axisText;
    if (valueField == "SIZE") {
      this.chartInfo.valueType = ChartValueType.FILE_SIZE;
    }
    else if (valueField == "CREATION_DATE" || valueField == "LAST_ACCESS" || valueField == "LAST_MODIFIED") {
      this.chartInfo.valueType = ChartValueType.DATE;
    }
    if (this.selectedChartTypeId == DashboardService.MULTIPLE_FIXED_HISTOGRAM) {
      this.chartInfo.withPagenation= this.withPagenation;
      this.chartInfo.show3d= this.show3d;
      this.chartInfo.withLabels= this.withLabels;
      this.chartInfo.additionalQueryParams.returnOther = this.returnOther;
      this.chartInfo.additionalQueryParams.take = Number(this.itemNumber);
      this.chartInfo.colorPalete = this.selectedColorPalete.id;

      if (this.selectedDataType.isDateField) {
        if (this.selectedDatePartition.type == this.PERIOD_METHOD_PREDEFINED_PARTITIONS) {
          this.chartInfo.additionalQueryParams.datePartitionId = this.selectedDatePartition.id;
          delete this.chartInfo.additionalQueryParams.periodType;
          this.chartInfo.additionalQueryParams.queryPrefix = "datePartition";
        }
        else {
          this.chartInfo.additionalQueryParams.periodType = this.selectedDatePartition.id;
          delete this.chartInfo.additionalQueryParams.datePartitionId;
        }
      }


      if (this.selectedChartDisplayTypeId == ChartDisplayType.PIE) {
        if (this.selectedFilter && this.selectedFilter.filterDescriptor) {
          this.selectedFilter.filterDescriptor.name = this.selectedFilter.name;
          this.chartInfo.beforeTransformQueryParams.segmentList = [this.selectedFilter];
          this.chartInfo.additionalQueryParams.withTotal = false;
        }
        else {
          let savedFilter: SavedFilterDto = {
            filterDescriptor: {
              name : "Unfiltered"
            },
            name: this.totalDisplayName
          }
          this.chartInfo.beforeTransformQueryParams.segmentList = [savedFilter];
          this.chartInfo.additionalQueryParams.withTotal = true;
        }
      }
      else {
        if (this.selectedSeriesType.id == SeriesType.MANUALY) {
          let filterDescriptionList: SavedFilterDto[] = _.cloneDeep(this.selectedSegmentList);
          let rowFilterList: string[] = [];
          if (this.withTotal || this.selectedSegmentList.length == 0) {
            let savedFilter: SavedFilterDto = {
              filterDescriptor: {
                name: this.totalDisplayName
              },
              name: "Unfiltered"
            }
            if (filterDescriptionList.length < 2) {
              filterDescriptionList.push(savedFilter);
              rowFilterList.push("Unfiltered");
            }
            else {
              filterDescriptionList.unshift(savedFilter);
              rowFilterList.unshift("Unfiltered");
            }
            this.chartInfo.additionalQueryParams.withTotal = true;
          }
          else {
            this.chartInfo.additionalQueryParams.withTotal = false;
          }
          this.chartInfo.beforeTransformQueryParams.segmentList = filterDescriptionList;
          delete this.chartInfo.additionalQueryParams.seriesField;
          delete this.chartInfo.additionalQueryParams.seriesFieldPrefix;
        }
        else {
          this.chartInfo.additionalQueryParams.seriesItemsNum = Number(this.seriesItemsNum);
          this.chartInfo.additionalQueryParams.seriesField = this.selectedSeriesField.id;
          if (this.selectedSeriesField.prefix) {
            this.chartInfo.additionalQueryParams.seriesFieldPrefix = this.selectedSeriesField.prefix;
          }
          else {
            delete this.chartInfo.additionalQueryParams.seriesFieldPrefix;
          }
          delete this.chartInfo.beforeTransformQueryParams.segmentList;
          delete this.chartInfo.additionalQueryParams.segmentList;
        }
      }
    }
    else if (this.selectedChartTypeId == DashboardService.SINGLE_VALUE_CHART) {
      this.chartInfo.withPagenation= false;
      this.chartInfo.show3d= false;
      if (this.selectedFilter && this.selectedFilter.filterDescriptor) {
        let sDescriptor: SavedFilterDto = <SavedFilterDto>this.selectedFilter;
        this.chartInfo.beforeTransformQueryParams.filterField = sDescriptor;
        this.chartInfo.additionalQueryParams.compareToTotal = this.selectedChartDisplayTypeId == ChartDisplayType.GAUGE ? true : false;
      }
      else {
        delete this.chartInfo.beforeTransformQueryParams.filterField;
        this.chartInfo.additionalQueryParams.compareToTotal = false;
      }
    }

    this.userViewDataDtoItem.displayData = JSON.stringify(this.chartInfo);
  };


}

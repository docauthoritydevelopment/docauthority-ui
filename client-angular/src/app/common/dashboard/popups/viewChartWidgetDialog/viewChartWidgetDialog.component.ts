import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {BaseComponent} from "@core/base/baseComponent";
import {UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";

@Component({
  selector: 'view-chart-widget-dialog',
  templateUrl: './viewChartWidgetDialog.component.html',
  styleUrls: ['./viewChartWidgetDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ViewChartWidgetDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public  data:{userView : UserViewDataDto, dashboardId : number , enableSave:boolean};
  public userViewDataDtoItem: UserViewDataDto;


  constructor(private activeModal: NgbActiveModal, private savedUserViewService: SavedUserViewService) {
    super();
  }


  daOnInit(): void {
    this.setInitData();
  }



  private setInitData() {
    if (this.data.userView) {
      this.userViewDataDtoItem = _.cloneDeep(this.data.userView);
    }
  }




  close() {
    this.activeModal.close(null);
  }

  save() {
    this.savedUserViewService.addSavedUserViewData(this.userViewDataDtoItem).subscribe((newUserViewData: UserViewDataDto)=>{
      this.activeModal.close(newUserViewData);
    },()=>{
    });
  }



  ngOnDestroy() {
  }

}

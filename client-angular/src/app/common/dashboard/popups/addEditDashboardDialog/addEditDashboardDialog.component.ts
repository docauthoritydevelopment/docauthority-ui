import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {BaseComponent} from "@core/base/baseComponent";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";

@Component({
  selector: 'add-edit-dashboard-dialog',
  templateUrl: './addEditDashboardDialog.component.html',
  styleUrls: ['./addEditDashboardDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditDashboardDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public dashboardItem: UserViewDataDto= null;
  public title: string = "";
  public  data:{dashboard : UserViewDataDto};

  @ViewChild('dashboardGeneralForm') public dashboardGeneralForm: NgForm = null;


  constructor(private activeModal: NgbActiveModal, private savedUserViewService: SavedUserViewService, private dialogService: DialogService) {
    super();
  }


  daOnInit(): void {
    this.setInitData();
  }



  private setInitData() {
    this.dashboardItem=<UserViewDataDto>{};
    if (this.data.dashboard) {
      this.dashboardItem = _.cloneDeep(this.data.dashboard);
    }
    if (this.dashboardItem.id){
      this.title = "Edit dashboard";
    }
    else  {
      this.title = "Add dashboard";
      this.dashboardItem.name = "";
      this.dashboardItem.displayType = EFilterDisplayType.DASHBOARD;
    }
  }



  isValid = () => {
    return this.dashboardGeneralForm.valid;
  };


  cancel() {
    this.activeModal.close(null);
  }


  save = () => {
    if (this.isValid()) {
      if (this.dashboardItem.id) {
        this.savedUserViewService.editSavedUserViewData(this.dashboardItem).subscribe((updateDashboard: UserViewDataDto) => {
          this.activeModal.close(updateDashboard);
        });
      }
      else {
        this.savedUserViewService.addSavedUserViewData(this.dashboardItem).subscribe((addedDashbaord: UserViewDataDto) => {
          this.activeModal.close(addedDashbaord);
        });
      }
    }
  };

  ngOnDestroy() {
  }

}

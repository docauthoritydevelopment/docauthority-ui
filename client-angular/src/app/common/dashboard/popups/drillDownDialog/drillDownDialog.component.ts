import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {BaseComponent} from "@core/base/baseComponent";
import {UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  ChartInfo,
  ChartWidgetOptionItem,
  FixedHistogramBeforeTransform,
  FixedHistogramDataDto
} from "@app/features/dashboard/types/dashboard-interfaces";

@Component({
  selector: 'drill-down-dialog',
  templateUrl: './drillDownDialog.component.html',
  styleUrls: ['./drillDownDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DrillDownDialogComponent extends BaseComponent implements OnInit, OnDestroy {

  dataTypeList: ChartWidgetOptionItem[] = DashboardService.MULTIPLE_HISTOGRAM_DATA_TYPE_LIST;
  selectDrillDown:ChartWidgetOptionItem = null;
  drillDownSelectName = null;
  public  data:{selectionName: string};


  constructor(private activeModal: NgbActiveModal, private savedUserViewService: SavedUserViewService) {
    super();
  }


  daOnInit(): void {
  }


  close() {
    this.activeModal.close(null);
  }

  save() {
    this.activeModal.close(this.selectDrillDown);
  }

  onDrillDownChoose = (selectDataType:ChartWidgetOptionItem)=> {
    this.selectDrillDown = selectDataType;
    this.drillDownSelectName = selectDataType.name;
  }


    ngOnDestroy() {
  }

}

import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  ViewChild,
  EventEmitter,
  Output,
  ChangeDetectorRef
} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  ChartInfo,
  DashboardChartDataDto,
} from "@app/features/dashboard/types/dashboard-interfaces";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {EFilterDisplayType, SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {AdvancedChartComponent} from "@app/common/charts/advanced-chart-component/advanced-chart.component";
import {EditSimpleChartComponent} from "@app/common/dashboard/edit-components/editSimpleChart/editSimpleChart.component";
import {ChartType} from "@app/features/dashboard/types/dashboard-enums";
import {EditTreeMapChartComponent} from "@app/common/dashboard/edit-components/editTreeMapChart/editTreeMapChart.component";
import {MainLoaderService} from "@services/main-loader.service";


@Component({
  selector: 'add-edit-chart-widget-dialog',
  templateUrl: './addEditChartWidgetDialog.component.html',
  styleUrls: ['./addEditChartWidgetDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditChartWidgetDialogComponent  implements OnInit, OnDestroy {

  @ViewChild(AdvancedChartComponent)
  previewChartComponent:AdvancedChartComponent;

  public userViewDataDtoItem: UserViewDataDto = null;
  public title: string = "";
  public data:{userView : UserViewDataDto, filterList: SavedFilterDto[], containerId:number, globalFilter:boolean, discoverName?:string, discoverRightName?:string, filter? : SavedFilterDto, initialChartType? : ChartType};
  public chartInfo: ChartInfo<any,any> = null;
  public chartData: DashboardChartDataDto = null;
  public showPreview:boolean = true;
  public loading:boolean = false;
  public mainLoading:boolean = false;
  public inPreviewMode:boolean = false;
  previewIsValid:boolean = false;
  editComponent;
  editInputs;
  editOutputs;
  public renderScreen: boolean = false;


  constructor(private activeModal: NgbActiveModal,
              private dashboardService: DashboardService,
              private dialogService: DialogService,
              private mainLoaderService: MainLoaderService,
              private ref: ChangeDetectorRef,
              private savedUserViewService:SavedUserViewService) {
    ref.detach();
  }

  ngOnInit(): void {
    this.renderScreen = true;
    this.ref.reattach();
    this.mainLoaderService.setMainLoader(false);
    if (this.data.filterList == null) {
      this.mainLoading = true;
      this.savedUserViewService.getSavedUserViewData({}, EFilterDisplayType.DISCOVER, true).subscribe((filterUserViewDataList)=>{
        this.data.filterList = filterUserViewDataList.map(c => this.convertToSavedFilterItem(c));
        this.setInitData();
        this.mainLoading = false;
      });
    }
    else {
      this.setInitData();
    }
  }

  convertToSavedFilterItem(userViewData:UserViewDataDto):SavedFilterDto {
    return userViewData.filter;
  }

  private setInitData() {
    if (this.data.userView) {
      if (this.data.userView.id) {
        this.title = "Edit chart widget";
      }
      else {
        this.title = "Clone chart widget";
      }
    }
    else  {
      this.title = "Add chart widget";
    }

    this.editComponent = EditSimpleChartComponent;
    if(this.data.userView) {
      let chartInfo: ChartInfo<any, any> = JSON.parse(this.data.userView.displayData);
      this.editComponent = this.getChartTypeComponentByType(chartInfo.chartType);
    }
    else if (this.data.initialChartType) {
      this.editComponent = this.getChartTypeComponentByType(this.data.initialChartType);
    }

    this.editInputs = {
      data : this.data
    };
    this.editOutputs = {
      onUpdatePreview: (updateObject : {chartData: DashboardChartDataDto, chartInfo : ChartInfo<any, any>, userView : UserViewDataDto}) => {
        if (updateObject !== null) {
          if (updateObject.chartData != null) {
            this.previewIsValid = true;
            this.chartData = updateObject.chartData;
            this.chartInfo = updateObject.chartInfo;
          }
          this.userViewDataDtoItem = updateObject.userView;
        }
        else {
          this.previewIsValid = false;
          this.chartData = null;
          this.chartInfo = null;
          this.userViewDataDtoItem = null;
        }
      },
      onSetLoader: (showLoader:boolean) => {
        this.loading = showLoader;
      }
    }
  }


  cancel() {
    this.activeModal.close(null);
  }

  save = () => {
    if (this.userViewDataDtoItem) {
      this.mainLoading = true;
      if (this.userViewDataDtoItem.id) {
        this.savedUserViewService.editSavedUserViewData(this.userViewDataDtoItem).subscribe((newUserViewData: UserViewDataDto)=>{
          this.activeModal.close(newUserViewData);
        },()=>{
          this.mainLoading = false;
        });
      }
      else {
        this.savedUserViewService.addSavedUserViewData(this.userViewDataDtoItem).subscribe((newUserViewData: UserViewDataDto)=>{
          this.activeModal.close(newUserViewData);
        },()=>{
          this.mainLoading = false;
        });
      }
    }
  };


  showNextChartTypes = () => {
    let nextChartType:ChartType = this.dashboardService.getNextChartType(this.chartInfo  ? this.chartInfo.chartType : (this.data.initialChartType ? this.data.initialChartType : ChartType.MULTIPLE_FIXED_HISTOGRAM));
    if(this.data.userView) {
      let chartInfo: ChartInfo<any, any> = JSON.parse(this.data.userView.displayData);
      chartInfo.chartType = nextChartType;
      chartInfo.beforeTransformQueryParams.segmentList = [];
      this.data.userView.displayData = JSON.stringify(chartInfo);
    }
    else {
      this.data.initialChartType = nextChartType;
    }
    this.setInitData();
  }

  getChartTypeComponentByType(chartType:ChartType) : any {
    if (chartType == ChartType.SUNBURST_CHART) {
      return EditTreeMapChartComponent;
    }
    return EditSimpleChartComponent;
  }


  ngOnDestroy() {
  }

}

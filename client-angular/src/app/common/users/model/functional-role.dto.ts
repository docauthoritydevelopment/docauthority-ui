import {ITemplateRoleContainer} from './template-role-container';
import {DtoTemplateRole} from './template-role';

export interface DtoFunctionalRole extends ITemplateRoleContainer {
  id: number;
  name: string;
  displayName: string;
  description: string;
  managerUsername: string;
  implicit: boolean;
  template: DtoTemplateRole;
}

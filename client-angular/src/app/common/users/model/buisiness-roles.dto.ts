import {ITemplateRoleContainer} from './template-role-container';
import {DtoTemplateRole} from './template-role';
import {DtoLdapGroup} from './ldap-group.dto';

export interface DtoBizRole extends ITemplateRoleContainer {
  name: string;
  displayName: string;
  id: number;
  description: string;
  ldapGroup: DtoLdapGroup;
  template: DtoTemplateRole;
}

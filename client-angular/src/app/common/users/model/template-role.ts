import {DtoSystemRole} from './system-role.dto';
import {ETemplateType} from './template-type.enum';

export interface DtoTemplateRole {
  name: string;
  displayName: string;
  id: number;
  description: string;
  templateType: ETemplateType;
  systemRoleDtos: DtoSystemRole[];


}

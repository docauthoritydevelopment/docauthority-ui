import {DtoTemplateRole} from './template-role';

export interface ITemplateRoleContainer
{
  template: DtoTemplateRole;
}


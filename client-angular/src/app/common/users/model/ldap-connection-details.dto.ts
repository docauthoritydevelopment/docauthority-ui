import {ELdapServerDialect} from './ldap-server-dialect.enum';

export interface DtoLdapConnectionDetails {
  id: number;
  name: string;
  isSSL: boolean;
  host: string;
  manageDN: string;
  port: number;
  managerPass: string;
  serverDialect: ELdapServerDialect
}

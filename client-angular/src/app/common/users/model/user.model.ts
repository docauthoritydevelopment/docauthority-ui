/**
 * This interface represents the user info model
 */
import {EUserState} from './user-state.enum';
import {DtoBizRole} from './buisiness-roles.dto';
import {DtoFunctionalRole} from './functional-role.dto';
import {DtoUIWrapper} from '@wrappers/server-object-wrapper';
import {DtoSystemRole} from './system-role.dto';
import {ESystemRoleName} from "@users/model/system-role-name";
import {SystemSettingsDto} from "@core/types/systemSettings.dto";

export class DtoUserWrapper extends DtoUIWrapper<DtoUser> {
  bizRolesMap: { [name: string]: DtoSystemRole };
  funcRoleMap: { [name: string]: DtoSystemRole };

  get name() {
    return this.internalDto.name;
  }
  get username() {
    return this.internalDto.username;
  }
}



export interface DtoUser {
  username: string;
  name: string;
  id: number;
  password?: string;
  roleName?: string;
  lastPasswordChangeTimeStamp?: number;
  userState?: EUserState;
  userType: string;
  email?: string;
  passwordMustBeChanged: boolean;
  bizRoleDtos: DtoBizRole[];
  funcRoleDtos: DtoFunctionalRole[];
  accountNonExpired: boolean;
  accountNonLocked: boolean;
  credentialsNonExpired: boolean;
  consecutiveLoginFailuresCount: number;
  ldapUser: boolean;
  loginLicenseExpired: boolean;
  dashboards: {[id: string]: ESystemRoleName};
  configurationList: SystemSettingsDto[];
}

import {DtoLdapConnectionDetails} from './ldap-connection-details.dto';

export interface DtoLdapGroup {
  id: number;
  groupName: string;
  groupDN: string;
  connectionDetailsDto: DtoLdapConnectionDetails;
  daRolesIds: number[];

}

import {ESystemRoleName} from './system-role-name';

export interface DtoSystemRole {
  name: ESystemRoleName;
  id: number;
  description: string;
  displayName: string;
  authorities: string[];
}

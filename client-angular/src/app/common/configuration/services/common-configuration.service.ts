import {Injectable} from '@angular/core';
import {Subject} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class CommonConfigurationService {
  private refreshTopMenuEvent = new Subject<any>();
  refreshTopMenuEvent$ = this.refreshTopMenuEvent.asObservable();

  refreshTopMenu(){
    this.refreshTopMenuEvent.next();
  }

}

import {NgModule} from '@angular/core';
import {CommonConfigurationService} from "@app/common/configuration/services/common-configuration.service";

@NgModule({

  imports: [
  ],
  entryComponents: [],
  providers: [CommonConfigurationService]
})
export class CommonConfigurationModule {
}

import {ToolbarActionTypes} from "@app/types/display.type";
import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";

export interface ButtonConfigDto extends MenuItemConfigDto{
  type:ToolbarActionTypes;
  href?:string;
  children?:DropDownConfigDto[];
  btnType?:string;
}

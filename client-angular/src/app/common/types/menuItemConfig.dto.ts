export interface MenuItemConfigDto {
  title?:string;
  name?:string;
  actionFunc?:(params?:any)=>void;
  disabled?:boolean;
  order?:number;
}

import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";

export interface SearchBoxConfigDto extends MenuItemConfigDto {
  searchFunc?:(searchText:string)=>void;
  searchTerm:string;
  placeholder:string;
  notifyOnKeyup?: boolean
}

import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";

export interface DropDownConfigDto extends MenuItemConfigDto {
  iconClass?:string;
  tooltip?:string;
  href?:string;
  disabled? : boolean;
  hrefParams?:any;
  useRegularHref?:boolean;
  children?:DropDownConfigDto[];
}

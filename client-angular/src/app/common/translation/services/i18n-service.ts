import {Injectable} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {commonTranslate} from "@app/translate/common.translate";

@Injectable()
export class I18nService {


  constructor(private translateService: TranslateService) {
  }


  translateId(id:  string, params: any = null): string {
    let ans = this.translateService.instant(id,params);
    return ans;
  }

  translateIdWithoutNull(id:  string, params: any = null): string {
    let ans = this.translateService.instant(id,params);
    if (!ans) {
      return id;
    }
    return ans;
  }

  async loadLanguage() {
    this.translateService.setDefaultLang('en');
    await this.translateService.use('en').toPromise<void>();
  }

  getCommonTranslate() {
    return commonTranslate;
  }

}

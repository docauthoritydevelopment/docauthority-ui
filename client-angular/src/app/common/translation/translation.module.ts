import {NgModule} from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [TranslateModule],
  declarations: [],
  providers : [I18nService],
  exports: []
})

export class TranslationModule {
}

import {Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {RoutingService} from '@services/routing.service';
import {Router} from '@angular/router';
import * as fromV1Routes from './doc-1/angular-1-routes/v1.routes';
import {DtoUserWrapper} from '@users/model/user.model';
import {Observable, Subscription} from 'rxjs/index';
import {TooltipService} from '@services/tooltip.service';
import {LoginService} from '@core/services/login.service';
import {LicenseService} from '@core/services/license.service';
import {PageTitleService} from '@core/services/page-title.service';
import {UiStateService} from '@core/services/ui-state.service';
import {MetadataService} from "@services/metadata.service";
import {MainLoaderService} from "@services/main-loader.service";
import {TranslateService} from "@ngx-translate/core";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {appTranslate} from "@app/translate/app.translate";
import {V1RouteUrl} from "@angular-1-routes/model/v1.route.url";
import {DaDropDownService} from "@shared-ui/components/da-drop-down/services/da-drop-down.service";
import * as Highcharts from 'highcharts';
import * as HC_exporting from 'highcharts/modules/exporting';
import * as HC_offline_exporting from 'highcharts/modules/offline-exporting';
import * as  highcharts3D from 'highcharts/highcharts-3d.src';
import * as  HC_sunburst from 'highcharts/modules/sunburst.src';
import * as  HC_treemap from 'highcharts/modules/treemap.src';
import * as  HC_heatmap from 'highcharts/modules/heatmap.src';


HC_exporting(Highcharts);
HC_offline_exporting(Highcharts);
highcharts3D(Highcharts);
HC_sunburst(Highcharts);
HC_treemap(Highcharts);
HC_heatmap(Highcharts);


export let AppInjector: Injector;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  isV1Route$: Observable<boolean>;
  subscriptions: Subscription;
  loaderSubscriptions: Subscription;
  dialogOpen$: Observable<boolean>;
  currentUser$: Observable<DtoUserWrapper>;
  showLoader:boolean = false;
  msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;



  constructor(/*private appInitializer: AppInitializer,*/ private routingService: RoutingService, private licenseService: LicenseService,
              private loginService: LoginService, private pageTitleService: PageTitleService, private uiStateService: UiStateService,
              private router: Router, private metadataService : MetadataService,private mainLoaderService:MainLoaderService,
              private tooltipService: TooltipService,
              private readonly translate: TranslateService,
              private i18nService : I18nService,
              private daDropDownService : DaDropDownService,
              private injector: Injector,
              private translateService: TranslateService) {
    this.translateService.setTranslation('en', appTranslate,false);
    this.translateService.setTranslation('en', i18nService.getCommonTranslate(),true);
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');

    AppInjector = this.injector;
    this.router.config.push(...fromV1Routes.addV1ToRoute(fromV1Routes.v1Routes));
    let defaultRoutes = [
      {path: '',
        redirectTo: V1RouteUrl.DashboardForwarder,
        pathMatch: 'full'
      },
    {
      path: '**',
      redirectTo: V1RouteUrl.DashboardForwarder,
      pathMatch: 'full'
    }];
    this.router.config.push(...defaultRoutes);

    this.loaderSubscriptions = this.mainLoaderService.loaderChangeEvent$.subscribe((doShow) => {
      this.showLoader = doShow;
    });
  }

  scroll = (): void => {
    this.daDropDownService.closeAllDropDowns();
  };


  ngOnInit() {
    this.currentUser$ = this.loginService.currentUser$;
  //  this.appInitializer.initUserDetails();
    this.routingService.setInitialQueryParams();
    this.isV1Route$ = this.routingService.isV1Route$;
    this.subscriptions = this.pageTitleService.getTitile$().subscribe(title => document.title = title);
    this.dialogOpen$ = this.uiStateService.isDialogOpen$();
    //
    this.tooltipService.init();
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    if (this.loaderSubscriptions) {
      this.loaderSubscriptions.unsubscribe();
    }
    window.removeEventListener('scroll', this.scroll, true);
  }


}

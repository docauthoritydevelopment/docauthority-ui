import { TestBed, inject } from '@angular/core/testing';

import { AppServicesManagerService } from './app-services-manager.service';

describe('AppServicesManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppServicesManagerService]
    });
  });

  it('should be created', inject([AppServicesManagerService], (service: AppServicesManagerService) => {
    expect(service).toBeTruthy();
  }));
});

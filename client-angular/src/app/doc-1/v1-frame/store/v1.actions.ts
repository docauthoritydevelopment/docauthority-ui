import {Action} from '@ngrx/store';
import {APP_PREFIX} from '@core/store/core.actions';

const PREFIX = `${APP_PREFIX} V1 Actions - `;

export const SET_LOADING_STATE = `${PREFIX}Set loading State`

export class SetLoadingState implements Action {
  readonly type = SET_LOADING_STATE;
  constructor(public loadState: boolean) {

  }
}

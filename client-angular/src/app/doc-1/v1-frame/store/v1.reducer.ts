import {Action} from '@ngrx/store';
import {SET_LOADING_STATE, SetLoadingState} from '@app/doc-1/v1-frame/store/v1.actions';

export interface V1State {
  v1Loaded: boolean;
}

const initialState: V1State = {
  v1Loaded: false,
}

export function v1Reducer(state: V1State = initialState, action: Action): V1State {
  switch(action.type) {
    case SET_LOADING_STATE: {
      return {...state, v1Loaded: (action as SetLoadingState).loadState};
    }
    default: {
      return state;
    }
  }
}

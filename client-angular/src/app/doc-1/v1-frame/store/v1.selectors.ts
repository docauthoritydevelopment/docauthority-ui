import {createFeatureSelector, createSelector} from '@ngrx/store';
import {V1State} from './v1.reducer';

export const v1StateSelector = createFeatureSelector<V1State>('v1State');

export const isV1loadedSelector = createSelector(v1StateSelector, state => state.v1Loaded);

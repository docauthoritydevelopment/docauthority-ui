/**
 * This class represents the API calls for doc1
 */
export interface Doc1Api {
  getDiscoverFilter: () => any;
}

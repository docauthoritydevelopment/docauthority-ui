/**
 * This const contains event names for v1
 * @type {{Loaded: string}}
 */

export const V1Events = {
  Loaded: 'loaded'
}

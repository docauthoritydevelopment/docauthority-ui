import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Ng1RoutePrefix, V1Service} from '@app/doc-1/v1-frame/v1.service';
import * as _ from 'lodash';
import {RoutingService} from '@services/routing.service';
import {UiStateService} from '@core/services/ui-state.service';
import {Observable, Subscription} from 'rxjs/index';

@Component({
  selector: 'da-doc-1-frame',
  templateUrl: './v1.frame.component.html',
  styleUrls: ['./v1.frame.component.scss']
})
export class V1FrameComponent implements OnInit, OnDestroy {

  onWindowResize: any;
  innerRoute: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(Ng1RoutePrefix);
  hidden: boolean;
  modalOpen$: Observable<boolean>
  frameStyle: { [key: string]: string };
  private _subscriptions: Subscription;

  constructor(private v1Service: V1Service,
              private sanitizer: DomSanitizer, private routingService: RoutingService,
              private uiStateService: UiStateService) {
  }

  /**
   * Gets whether the app runs in an iframe
   * This is to avoid a situation of endless iframes
   * @returns {boolean}
   */
  get amIOnIframe(): boolean {
    return window !== window.parent;
  }

  private _v1ContentWindow: any;

  private get v1ContentWindow(): any {
    if (this._v1ContentWindow) {
      return this._v1ContentWindow;

    } else {
      const frameElement = document.querySelector('iframe');
      this._v1ContentWindow = frameElement ? frameElement.contentWindow : null;

      return this._v1ContentWindow;
    }

  }


  ngOnInit() {


    this.frameStyle = {}// {height: `${window.innerHeight - 80}px`};


    this.modalOpen$ = this.uiStateService.isDialogOpen$();

    this.connectWithV1Service();


    this.hidden = true;

    if (!window.onresize) {
      window.onresize = () => {
        if (_.isFunction(this.onWindowResize)) {
          this.onWindowResize();
        } else {
          this.frameStyle = {height: `${window.innerHeight}px`}
        }
      }
    }
  }


  private connectWithV1Service() {

    this.v1Service.setContentWindowGetter(() => this.v1ContentWindow);
    this._subscriptions = this.v1Service.changeLocation$.subscribe(value => {
      this.frameStyle = {...this.frameStyle, ...value}
    });
    this._subscriptions.add(this.v1Service.changeVisibility$.subscribe(value => {
      this.hidden = !value;
      // Clear style if hidden

    }))
    this._subscriptions.add(this.v1Service.clearStyle$.subscribe(() => {
      this.frameStyle = {};
    }))

    this._subscriptions.add(this.v1Service.windowResizeSubject.subscribe(onResize => {
      this.onWindowResize = () => onResize();
    }))
  }

  ngOnDestroy() {
    if (this._subscriptions) {
      this._subscriptions.unsubscribe();
    }
  }

}

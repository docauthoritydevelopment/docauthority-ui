import {inject, TestBed} from '@angular/core/testing';

import {V1Service} from './v1.service';
import {MockV1Window} from '@app/doc-1/v1-frame/mocks/mock-v1-window';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CoreModule} from '@core/core.module';
import {coreReducer} from '@core/store/core.reducer';

describe('Doc1MessagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), StoreModule.forRoot(coreReducer)
      , CoreModule.forRoot(), NgbModule.forRoot()],
      providers: [V1Service, {provide: APP_BASE_HREF, useValue: '/'}]
    });
  });

  it('should be created', inject([V1Service], (service: V1Service) => {
    expect(service).toBeTruthy();
  }));
  it('Should have v1 content windie', inject([V1Service], (service: V1Service) => {
    service.setContentWindowGetter(() => new MockV1Window());
    expect(service.v1ContentWindow()).toBeTruthy();
  }));
});

import {Injectable, OnDestroy} from '@angular/core';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';
import {isNg2Route} from '@app/store/router.selectors';
import {Doc1Api} from '@app/doc-1/v1-frame/doc-1-api';
import {EDialogTypes} from '@angular-1-routes/model/dialog-types.enum';
import {V1RouteStateName} from '@app/doc-1/angular-1-routes/model/v1.route.state.name';
import * as _ from 'lodash';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs/index';
import {V1Events} from '@app/doc-1/v1-frame/config/v1-events';
import {SetLoadingState} from '@app/doc-1/v1-frame/store/v1.actions';
import {isV1loadedSelector} from '@app/doc-1/v1-frame/store/v1.selectors';
import {filter, first, skipWhile} from 'rxjs/operators';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {UrlUtility} from '@utilities/UrlUtility';
import {AlertType} from '@shared-ui/components/modal-dialogs/types/alert-type.enum';
import {isDialogOpenSelector} from '@core/store/core.selectors';
import {SetDialogState} from '@core/store/core.actions';
import {PageTitleService} from '@core/services/page-title.service';
import {reject} from 'q';
import {RoutingService, V1RouteVersion} from "@services/routing.service";
import {ActivatedRoute, ActivationEnd, ActivationStart, NavigationEnd, NavigationStart, Router} from "@angular/router";
import {V1RouteData} from "@angular-1-routes/model/v1.route.data";
import {ServerExportService} from "@services/serverExport.service";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";
import {MainMenuItemsService} from "@app/navigation/header/main-menu-items.service";
import {SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {CommonConfigurationService} from "@app/common/configuration/services/common-configuration.service";
import {ScheduledOperationsService} from "@services/scheduledOperations.service";

declare var $: any;
export const Ng1RoutePrefix = '/v1/';

@Injectable()
export class V1Service implements OnDestroy {

  changeLocation$: Subject<any>;
  changeVisibility$: BehaviorSubject<boolean>;
  clearStyle$: Subject<void>;
  v1StateChanged$: BehaviorSubject<string>;
  windowResizeSubject: Subject<any>;
  openDropDowns:NgbDropdown[] = [];

  /**
   * Content window getter expression
   */
  public v1ContentWindow: () => any;
  private _doc1Api: Doc1Api;
  private _subscriptions: Subscription;
  private isIE:boolean;

  constructor(private  location: Location, private    store: Store<any>,
              private router: Router,
              private commonConfiguration:CommonConfigurationService,
              private mainMenuItemsService: MainMenuItemsService,
              private serverExportService: ServerExportService,

              private activeRoute: ActivatedRoute,
              private routingService:RoutingService,
              private dialogService: DialogService, private titlesService: PageTitleService,
              private scheduledOperationsService: ScheduledOperationsService) {
              //private appServiceManager: AppServicesManagerService) {
    this.isIE =  (document['documentMode'] || /Edge/.test(navigator.userAgent));
    const windowAsAny = window as any;
    if (!windowAsAny.stateChanged) {
      windowAsAny.stateChanged = (url: string) => this.stateChanged(url);
    }

    this.setGlobalScopeApi();
    this.changeLocation$ = new Subject<any>();
    this.changeVisibility$ = new BehaviorSubject<boolean>(false);
    this.clearStyle$ = new Subject<void>();
    this.v1StateChanged$ = new BehaviorSubject<any>('');
    this.windowResizeSubject = new Subject<boolean>();

    this.setVisibility(false);
    this._subscriptions = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
     //   console.log(" NavigationEnd changed. Set V1 frame to false!!!!!!!!" +event.url);
        //   this.setVisibility(true);

      });

    this._subscriptions.add(this.router.events.pipe(
      filter(event => event instanceof NavigationStart))
      .subscribe((event: NavigationStart) => {

      //  console.log(" NavigationStart event.snapshot.data!!!!!!!!" +event.navigationTrigger);
      }));

    this._subscriptions.add(this.router.events.pipe(
      filter(event => event instanceof ActivationStart)) //using NavigationEnd since ActivationEnd is called multiple times per route segment
      .subscribe((event: ActivationStart) => {
        const routeData = <V1RouteData>event.snapshot.data;
        if (routeData && routeData.version == V1RouteVersion) {
          this.setVisibility(true);
        }
        else {
          this.setVisibility(false);
          //    this.changeFrameNRoute(V1RouteStateName.unauthorized, null)
        }
        if(event.snapshot.children.length==0) { //do it only for leaf url segment
          if (routeData && routeData.version == V1RouteVersion) {
            //    this.setVisibility(true);
          }
          else {
       //         this.setVisibility(false);
            this.changeFrameRoute(<V1RouteStateName>'empty', null)
          }
        }
        //console.log(" ActivationStart event.snapshot.data!!!!!!!!" +event.snapshot.data);
      }));
    // this._subscriptions =this.routingService.getLastRouteData$().subscribe(() => {
    //   this.setVisibility(false);
    // });

  }


  get Doc1Api() {
    return this._doc1Api;
  }

  /**
   * When called, it suppose to refresh the custom menus
   */
  refreshMenue() {
    this.commonConfiguration.refreshTopMenu();
  }

  addFileToProcess(fileName,type, exportParams, userParams,useAdvnacedMode:boolean = false) {
    this.serverExportService.addExportFile(fileName,type, exportParams, userParams, useAdvnacedMode);
  }

  updateAsyncUserOperations() {
    this.scheduledOperationsService.updateOperations()
  }

  openAddEditChartWidgetDialog(data:{userView : UserViewDataDto, filterList: SavedFilterDto[], containerId:number, globalFilter:boolean}) {
    let modelParams = {
      windowClass: 'modal-xl-window'
    };
    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams);
  }

  /**
   * Unfocusing inner appp
   */
  unfocusInnerApp() {
    if (this.v1ContentWindow().unfocus) {
      this.v1ContentWindow().unfocus();
    }
  }

  fireEvent(eventType, eventArgs) {
    if (this.v1ContentWindow() && this.v1ContentWindow().fireEvent) {
      this.v1ContentWindow().fireEvent(eventType, eventArgs);
    }
  }

  reload() {
    this.v1ContentWindow().reload();
  }

  /**
   * Sets the content window getter
   * @param frameElement
   */
  setContentWindowGetter(contentWindowGetter: () => any) {
    if (!contentWindowGetter) {
      throw {error: 'Empty getter'};
    }
    this.v1ContentWindow = contentWindowGetter;
  }

  changeParamsOfCurrentRoute(query: { [key: string]: any; }) {
    if (query) {
      const v1Url = this.v1StateChanged$.getValue();
      const urlObject = UrlUtility.parse(decodeURI(v1Url), true);

      const params = _.merge({}, urlObject.query, query);
      this.changeFrameRoute(<V1RouteStateName>'', params);
    }
    ;

  }

  /**
   * Changes doc-1 frame state
   * @param data
   * @param params
   */
  changeFrameRoute(stateName: V1RouteStateName, params: { [key: string]: any; }): Promise<any> {
    return new Promise((resolve) => {
      this.store.select(isV1loadedSelector).pipe(skipWhile(res => !res), first())
        .subscribe(() => {
          console.log('changing v1 route', stateName, params)
          const promise = this.v1ContentWindow().setAngularJsRoute(stateName, params);
          promise.then(
            res => resolve(res))
            .catch(err => {
              console.error(err);
              reject(err);
            });
        });
    });

  }


  notifyInnerAppFocus() {
    this.store.select(isDialogOpenSelector).pipe(first()).subscribe(dialogOpen => {
      if (!dialogOpen) {
        this.mainMenuItemsService.closeAllOpenMenues();
      }
    });
  }

  raiseEvent(eventName: string, args: any) {
    if (eventName === V1Events.Loaded) {
      this.store.dispatch(new SetLoadingState(true))
    }
  }

  notifyError(title, message) {
    //  this.store.dispatch(new NotifyErrorAction(title, message))
    this.dialogService.showAlert(title, message, AlertType.Error);
  }

  /**
   * Sets doc1-app-frame-position
   */

  setFramePosition(position: { [key: string]: string }): Promise<void> {

    this.changeLocation$.next(position);

    return new Promise<void>(resolve => resolve());


  }

  openDialog(dialogType: EDialogTypes) {
    const contentWindow = this.v1ContentWindow();
    if (contentWindow && contentWindow.openDialog) {
      contentWindow.openDialog(dialogType);
    }
  }

  clearStyle() {
    this.clearStyle$.next();
  }

  /**
   * sets doc1 frame visibility
   * @param {boolean} visible
   */
  setVisibility(visible: boolean) {
    if (!visible) {
      $('iframe').hide(); //use jquery to fix I.E 11 slowns in setting show/hide v1 v2
    } else {
      $('iframe').show();
    }
    this.changeVisibility$.next(visible);
  }

  setOnWindowResize(onResize: () => void) {
    this.windowResizeSubject.next(onResize);
  }

  /**
   * Sets the app title
   * @param {string} title
   */
  setTitle(title: string) {
    this.store.select(isNg2Route).pipe(first()).subscribe(isNg2 => {
      // set v1 title only if your are in it
      if (!isNg2) {
        this.titlesService.setTitle(title);
      }
    });
  }

  changeBrowserUrl(url: string) {
    url = url || '';
    // remove v1 prefix and set in browser address bar
    url = `${url.replace(Ng1RoutePrefix, '')}`;
    this.routingService.updateUrlNoReload(url);
    console.log('changeBrowserUrl ' + url);
  }

  /**
   * Handles doc-1 state changed notification
   * @param state
   * @param params
   */
  stateChanged(url: string) {
    if (typeof url !== 'string') {
      const urlObj = url as any;
      if (urlObj.url) {
        url = urlObj.url;
      }
    }
    this.v1StateChanged$.next(url)

  }

  get v1StateChanged_obs$():Observable<string>{
    return this.v1StateChanged$.asObservable();
  }


  private setGlobalScopeApi() {

    const windowAsAny = window as any;
    if (!windowAsAny.stateChanged) {
      windowAsAny.stateChanged = (url: string) => this.stateChanged(url);
    }

    if (!windowAsAny.refreshMemuItems) {
      windowAsAny.refreshMenuItems = () => this.refreshMenue();
    }

    if (!windowAsAny.addFileToProcess) {
      windowAsAny.addFileToProcess = (fileName,type, queryParams,infoParams,useAdvnacedMode) => this.addFileToProcess(fileName,type, queryParams,infoParams,useAdvnacedMode);
    }

    if (!windowAsAny.openAddEditChartWidgetDialog) {
      windowAsAny.openAddEditChartWidgetDialog = (data:any) => this.openAddEditChartWidgetDialog(data);
    }

    if (!windowAsAny.updateAsyncUserOperations) {
      windowAsAny.updateAsyncUserOperations = (data:any) => this.updateAsyncUserOperations();
    }


    if (!windowAsAny.setTitle) {
      windowAsAny.setTitle = (title: string) => this.setTitle(title)
    }

    if (!windowAsAny.changeUrl) {
      windowAsAny.changeUrl = (url) => {
        this.router.navigate([url]);
      }
    }

    if (!windowAsAny.onDialogOpened) {
      windowAsAny.onDialogOpened = () => this.store.dispatch(new SetDialogState(true));
    }

    if (!windowAsAny.onDialogClosed) {
      windowAsAny.onDialogClosed = () => this.store.dispatch(new SetDialogState(false));
    }

    if (!windowAsAny.notifyInnerAppFocus) {
      windowAsAny.notifyInnerAppFocus = () => this.notifyInnerAppFocus();

    }
    if (!windowAsAny.raiseEvent) {
      windowAsAny.raiseEvent = (eventName, args) => this.raiseEvent(eventName, args);
    }

    // if (!windowAsAny.appServiceManager) {
    //   windowAsAny.appServiceManager = this.appServiceManager;
    // }

    document.body.addEventListener('click', (evt) => {
      if (evt.target === document.body) {
        return;
      }
      this.unfocusInnerApp();
    });
  }


  ngOnDestroy() {
    if (this._subscriptions) {
      this._subscriptions.unsubscribe();
    }
  }
}

import { Injectable } from '@angular/core';
import { resolveDefinition } from '../../../../node_modules/@angular/core/src/view/util';


//Not in use
@Injectable({
  providedIn: 'root'
})
export class AppServicesManagerService {

  private services: {[serviceId: string]: (...args) => Promise<any>};
  constructor() {
    this.services = {};
   }

   /**
    * Register service for both appication to use
    * @param serviceId Service Id
    * @param executorFn service executor
    */
   registerService(serviceId: string,  executorFn: (...args) => Promise<any>) {
     if (this.services[serviceId]) {
       console.error(`Service ${serviceId} already exist`);
       return;
     }
     this.services[serviceId] = executorFn;
   }

  /**
   * Request data service from the service manager
   * @param serviceId
   * @param args
   */
   requestService(serviceId: string, ...args): Promise<any> {
    if (!this.services[serviceId]) {
      return new Promise<any>((resolve, reject) => {
        reject({message: `Service ${serviceId} does not exist`})
      })
    }
    return this.services[serviceId](...args);
   }
}

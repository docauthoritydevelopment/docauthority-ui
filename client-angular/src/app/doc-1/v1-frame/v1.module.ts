import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {V1Service} from './v1.service';
import {V1FrameComponent} from './v1.frame.component';
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [V1FrameComponent],
  exports: [V1FrameComponent],
  entryComponents:[AddEditChartWidgetDialogComponent]
 })
export class V1Module {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: V1Module,
      providers: [V1Service/*, AppServicesManagerService*/],

    };
  }
}

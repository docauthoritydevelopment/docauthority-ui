import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {V1PageComponent} from '@angular-1-routes/v1-page/v1-page.component';
import {SetV1FrameRouteResolver} from '@angular-1-routes/set-v1-frame-route.resolver';
import {V1Module} from '@app/doc-1/v1-frame/v1.module';

@NgModule({
  imports: [
    CommonModule,
    V1Module
  ],
  declarations: [V1PageComponent],
  exports: [V1PageComponent],
  entryComponents: [V1PageComponent],
  providers: [SetV1FrameRouteResolver]
})
export class Angular1RoutesModule {

}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';

import {BehaviorSubject, combineLatest, Subscription} from 'rxjs/index';
import {first, skip, skipWhile, withLatestFrom} from 'rxjs/internal/operators';
import {RoutingService} from '@services/routing.service';
import {ActivatedRoute, ActivationStart, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {filter} from "rxjs/operators";
import {V1RouteData} from "@angular-1-routes/model/v1.route.data";
import {Params} from "@angular/router/src/shared";
const footerHeight = 80;

@Component({
  selector: 'da-v1-page',
  templateUrl: './v1-page.component.html',
  styleUrls: ['./v1-page.component.scss']
})
export class V1PageComponent implements OnInit, OnDestroy {
  private _subscriptions: Subscription;
  private _subscriptions2: Subscription;

  constructor(private v1Service: V1Service,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private router: Router,) {
  }

  ngOnInit() {
    this.v1Service.setOnWindowResize(() => this.v1Service.setFramePosition({'height': `${window.innerHeight - footerHeight}px`}))
    // this._subscriptions =
    //   this.v1Service.v1StateChanged$.subscribe(url => {
    //     this.v1Service.changeBrowserUrl(url);
    //
    //   });

    const routeDataObservable = this.routingService.getLastRouteData$()
    // let routeSubscription = this.routingService.getRouteQueryParams$().pipe(
    //     first(), withLatestFrom(routeDataObservable))
    //     .subscribe(queryParamsData => {
    //       const [queryParams, data] = queryParamsData;
    //       setTimeout(() => {
    //         // timeout is needed cause 2 excessive states: unauthorized -> report do not actually replace each other if in same digest loop
    //           this.v1Service.changeFrameRoute(data.stateName, {...data.stateParams, ...queryParams, isEmbedded: false})
    //           .then(() => {this.v1Service.clearStyle();
    //           this.v1Service.setFramePosition({'height': `${window.innerHeight - footerHeight}px`});
    //           this.v1Service.setVisibility(true);
    //         });
    //       }
    //     )});

    // this._subscriptions.add(routeSubscription);

  //  this._subscriptions2=combineLatest(this.route.params,this.route.queryParams,this.route.data)
    //(
     // filter(event => event instanceof ActivationStart),
      // filter(event=> (<ActivationStart>event).snapshot.children.length==0),
      // first()
     //  )
    //  .subscribe(([params,queryParams,routeData1]) => {
        const routeData = <V1RouteData>(this.route.snapshot.data);

       // if(event.snapshot.children.length==0) {
          this._subscriptions= //only after V1 is navigated to the right page, start listening to its change events
            this.v1Service.v1StateChanged_obs$.pipe(skip(1)).subscribe(url => { //skip(1) cause the first is the current url
              this.v1Service.changeBrowserUrl(url);

            });
          // this.v1Service.changeFrameRoute(routeData.stateName, {
          //   ...routeData.stateParams, ...this.route.snapshot.params, ...this.route.snapshot.queryParams,
          //   isEmbedded: false
          // })
          //   .then(() => {

       //       this.v1Service.clearStyle();
       //       this.v1Service.setFramePosition({'height': `${window.innerHeight - footerHeight}px`});
        //    });

          console.log(" ActivationStart event.snapshot.queryParams!!!!!!!!" + this.route.snapshot.queryParams);
      //  }
    //  });
    /*
    this._subscriptions2=this.router.events.pipe(
      filter(event => event instanceof NavigationStart))
      .subscribe((event: NavigationStart) => {
        console.log("V1 Page Comp $$$$  NavigationStart changed. Set V1 frame to false!!!!!!!!" +event.url);
           this._subscriptions.unsubscribe(); //destroyed is called only after route end. Need to supress changing browser url to unauthorized
        //   this.setVisibility(true);

      });
      */

    // this._subscriptions2=this.router.events.pipe(
    //   filter(event => event instanceof NavigationEnd),
    //   first())
    //   .subscribe((event: NavigationEnd) => {
    //     this._subscriptions2.unsubscribe();
    //   });


    //     const routeData = <V1RouteData>(this.activeRoute.snapshot.data);
    // this._subscriptions=//only after V1 is navigated to the right page, start listening to its change events
    //   this.v1Service.v1StateChanged_obs$.pipe(skip(1)).subscribe(url => { //skip(1) cause the first is the current url
    //     this.v1Service.changeBrowserUrl(url);
    //
    //   });
    //     this.v1Service.changeFrameRoute(routeData.stateName, {...routeData.stateParams,...(<BehaviorSubject<Params>>this.activeRoute.params).getValue(), ...(<BehaviorSubject<Params>>this.activeRoute.queryParams).getValue(), isEmbedded: false})
    //       .then(() => {
    //
    //         this.v1Service.clearStyle();
    //         this.v1Service.setFramePosition({'height': `${window.innerHeight - footerHeight}px`});
    //       });
    //
    //     console.log("ActivationStart event.snapshot.queryParams!!!!!!!!" +this.activeRoute.snapshot.queryParams);

    //  }));
  }



  ngOnDestroy() {
    if (this._subscriptions) {
      this._subscriptions.unsubscribe();
    }
    if (this._subscriptions2) {
      this._subscriptions2.unsubscribe();
    }
  }
}

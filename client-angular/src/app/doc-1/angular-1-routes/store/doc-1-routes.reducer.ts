import {Action} from '@ngrx/store';
import {
  ALLOW_STATE_CHANGE, SET_ROUTE_PARAMS,
  SetRoute
} from '@app/doc-1/angular-1-routes/store/doc-1-routes.actions';
import {Doc1RouteState} from '@angular-1-routes/model/doc1-route-state';


function getParamsFromUrl(): Doc1RouteState {
  let search = window.location.search;
  const result: Doc1RouteState = {
    url: window.location.pathname,
    parameters: {}
  }
  if (!search) {
    return result;
  }

  search = search.substring(1);
  const paramsArray = search.split('&');
  const paraneters = {};
  paramsArray.forEach(item => {
    const [key, value] = item.split('=');
    paraneters[key] = value;
  })
  result.parameters = paraneters;
  return result;
}

export interface V1RouteState {
  routeState: Doc1RouteState;
  allowStateChanges: boolean;
}

const  initialState: V1RouteState  = {
  routeState: getParamsFromUrl(),
  allowStateChanges: false
}

export function reducer(state: V1RouteState = initialState, action: Action ) {
  switch (action.type) {
    case SET_ROUTE_PARAMS: {
      return {...state, routeState: (<SetRoute>action).routeState}
    }
    case ALLOW_STATE_CHANGE: {
      return {...state, allowStateChanges: true}
    }
    default: {
      return state;
    }
  }
}

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {V1RouteState} from '@app/doc-1/angular-1-routes/store/doc-1-routes.reducer';

export const getState = createFeatureSelector<V1RouteState>('doc1Router');

export const getDoc1RouteState = createSelector(getState, state => state.routeState);

export const getAllowStateChanges = createSelector(getState, state => state.allowStateChanges);

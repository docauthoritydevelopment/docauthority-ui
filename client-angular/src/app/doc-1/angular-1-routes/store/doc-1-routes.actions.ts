import {Action} from '@ngrx/store';
import {Doc1RouteState} from '@angular-1-routes/model/doc1-route-state';
import {APP_PREFIX} from '@core/store/core.actions';

const DOC_1_PREFIX = `${APP_PREFIX} Doc 1 Routes`
export const SET_ROUTE_PARAMS = `${DOC_1_PREFIX} Set routes parameters`;
export const ALLOW_STATE_CHANGE =  `${DOC_1_PREFIX} Allow state change`;

export class SetRoute implements Action {
  readonly type = SET_ROUTE_PARAMS;
  constructor(public routeState: Doc1RouteState) {

  }
}

export class AllowStateChange implements  Action {
  readonly type = ALLOW_STATE_CHANGE;
}



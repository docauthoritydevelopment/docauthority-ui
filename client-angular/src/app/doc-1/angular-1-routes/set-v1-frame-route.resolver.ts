import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';
import {V1RouteData} from "@angular-1-routes/model/v1.route.data";


@Injectable()
export class SetV1FrameRouteResolver implements Resolve<any> {

  constructor(private v1Service: V1Service) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    const routeData = <V1RouteData>(route.data);
    const footerAndHeaderHeight = 80;
  //  this.v1Service.clearStyle();
    this.v1Service.setFramePosition({'height': `${window.innerHeight - footerAndHeaderHeight}px`});
    this.v1Service.changeFrameRoute(routeData.stateName, {
      ...routeData.stateParams, ...route.params, ...route.queryParams,
      isEmbedded: false
    }); //do not return this in order not to wait till v1 route really changes
    return new Promise<boolean>((resolve)=>resolve(true));

  }

}

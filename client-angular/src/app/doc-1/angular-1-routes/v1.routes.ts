import {Route} from '@angular/router';
import {V1PageComponent} from '@angular-1-routes/v1-page/v1-page.component';
import {V1RouteStateName} from '@app/doc-1/angular-1-routes/model/v1.route.state.name';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {RouteStateData} from '@app/store/router.selectors';
import {AuthorizationGuard} from '@core/authorization.guard';
import {V1RouteUrlParts} from "@angular-1-routes/model/v1.route.url";
import {V1RouteData} from "@angular-1-routes/model/v1.route.data";
import {SetV1FrameRouteResolver} from "@angular-1-routes/set-v1-frame-route.resolver";
import {V1RouteVersion} from "@services/routing.service";


export const v1Routes: Route[] = [
  // {
  //   path: V1RouteUrl.Unauthorized,
  //   component: V1PageComponent,
  //   data: <V1RouteData>{stateName: V1RouteStateName.unauthorized}
  // },
  {
    path: V1RouteUrl.DashboardForwarder,
    component: V1PageComponent,
    data: <V1RouteData>{stateName: V1RouteStateName.dashboardForwarder}
  },
  {
    path: V1RouteUrl.DashboardIT,
    component: V1PageComponent,
    data: <V1RouteData>{stateName: V1RouteStateName.dashboardIT}
  },
  {
    path: V1RouteUrl.DashboardSystem,
    component: V1PageComponent,
    data: {stateName: V1RouteStateName.dashboardSystem}
  },
  {
    path: V1RouteUrl.DashboardUser,
    component: V1PageComponent,
    data: {stateName: V1RouteStateName.dashboardUser}
  },
  {
    path: V1RouteUrl.chartView,
    component: V1PageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      setV1Frame: SetV1FrameRouteResolver
    },
    data: {stateName: V1RouteStateName.chartView}
    },
  {
    path: V1RouteUrl.trendChartView,
    component: V1PageComponent,
    data: {stateName: V1RouteStateName.trendChartView}},
  {
    path: 'details/:context/:id',
    component: V1PageComponent,
    data: {stateName: V1RouteStateName.details}

  },
  {
    path: V1RouteUrlParts.Users,
//    component: V1PageComponent,
    children: [
      {
        path: V1RouteUrlParts.Manage,
        component: V1PageComponent,
        data: {stateName: V1RouteStateName.usersManagement}
      },
      {
        path: 'templateRoles',
 //       component: V1PageComponent,
        children: [{
          path: V1RouteUrlParts.Manage,
          component: V1PageComponent,
          data: {stateName: V1RouteStateName.templateRolesManagement}
        }]

      },
      {
        path: V1RouteUrlParts.OperationalRoles,
 //       component: V1PageComponent,
        children: [{
          path: V1RouteUrlParts.Manage,
          component: V1PageComponent,
          runGuardsAndResolvers: 'always',
          resolve: {
            setV1Frame: SetV1FrameRouteResolver
          },
          data: {stateName: V1RouteStateName.bizRolesManagement}
        }]
      },
      {
        path: V1RouteUrlParts.DataRoles,
   //     component: V1PageComponent,
        children: [{
          path: V1RouteUrlParts.Manage,
          component: V1PageComponent,
          data: {stateName: V1RouteStateName.functionalRolesManagement}
        }]
      }
    ]
  },
  {
    path: `${V1RouteUrl.Discover}/${V1RouteUrl.Search}`,
    component: V1PageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      setV1Frame: SetV1FrameRouteResolver
    },
    data: {stateName: V1RouteStateName.search}
  },
  {
    path: `${V1RouteUrl.Discover}/:left/:right`,
    component: V1PageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      setV1Frame: SetV1FrameRouteResolver
    },
    data: {stateName: V1RouteStateName.discover}
  }
  ,
  {
    path: V1RouteUrl.Datamap,
    component: V1PageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      setV1Frame: SetV1FrameRouteResolver
    },
    data: {
      stateName: V1RouteStateName.docTypesChart
    }
  }
  ,
  {
    path: V1RouteUrl.ManageCharts,
    component: V1PageComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      setV1Frame: SetV1FrameRouteResolver
    },
    data:
      {
        stateName: V1RouteStateName.chartsManagement
      }
  }
  ,
  {
    path: V1RouteUrl.ManageTrendCharts,
    component: V1PageComponent,
    data:
      {
        stateName: V1RouteStateName.trendChartsManagement
      }
  }
  ,
  {
    path: V1RouteUrl.Profile,
    component: V1PageComponent,
    data:
      {
        stateName: V1RouteStateName.profile
      }
    ,

  }
  ,
  {
    path:V1RouteUrl.Settings,
    component:V1PageComponent,
    data:
      {
        stateName: V1RouteStateName.settings
      }
  },
  {
    path: 'settings/:activeTab', component:
    V1PageComponent, data:
      {
        stateName: V1RouteStateName.settings
      }
  },
    {

    path: V1RouteUrlParts.Scan,
    children:
      [
        {
          path: V1RouteUrlParts.Manage,
          children: [
            {
              path: V1RouteUrlParts.Rootfolders,
              component: V1PageComponent,
              data: {stateName: V1RouteStateName.rootFoldersManagement}
            }
          ]
        },
        {
          path: V1RouteUrlParts.Monitor,
          children: [
            {
              path: V1RouteUrlParts.serverComponents,
              component: V1PageComponent,
              data: {stateName: V1RouteStateName.serverComponentsMonitor}
            },
          ]
        },
        {
          path: 'configuration', children: [
            {
              path: 'searchPatterns',
              component: V1PageComponent,
              data: {
                stateName: 'searchPatternsManagement'
              }
            },
            {
              path: 'bizLists',
              component: V1PageComponent,
              data: {
                stateName: 'bizListsManagement'
              }
            }
          ]
        }
      ]

  },
  {
    path: 'processingErrors/:phase',
    component: V1PageComponent,
    data: {stateName: 'processingErrors', stateParams: {phase: 'scan', page: 1}}
  },
  // {
  //   path: '**',
  //   redirectTo: V1RouteUrl.DashboardForwarder
  // }


]

export function addV1ToRoute(v1Routes: Route[]): Route[] {
  v1Routes.forEach(route => {

    route.data = <V1RouteData>(route.data) || {};
    if (route.component /* instanceof V1PageComponent*/) {
      route.runGuardsAndResolvers= 'always';
      route.resolve= {
        setV1Frame: SetV1FrameRouteResolver
      }
    }
    if (!<V1RouteData>(route.data).stateName && !route.children && !route.redirectTo) {
      console.error('Missing stateName for route', route);
    }
    (<V1RouteData>route.data).version = V1RouteVersion;
    route.canActivate = route.canActivate || [];
    if (!(<RouteStateData>route.data).withoutAuth) {
      route.canActivate.push(AuthorizationGuard);
    }

    if (route.children) {
      addV1ToRoute(route.children);
    }

  })

  return v1Routes;
}




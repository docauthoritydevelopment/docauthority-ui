export enum V1RouteUrl {
  DashboardIT = 'dashboardIT',
  DashboardSystem = 'dashboardSystem',
  DashboardUser = 'dashboardUser',
  DashboardForwarder = 'dashboardForwarder',

  Discover = 'report',
  Search = 'search',
  chartView = 'charti',
  trendChartView = 'trendcharti',
  ServerComponents = 'scan/monitor/serverComponents',
  RootFolders = 'scan/manage/rootfolders',
  SearchPatterns = 'scan/configuration/searchPatterns',
  BizLists = 'scan/configuration/bizLists',

  Profile = 'profile',
  Users = 'users/manage',
  RoleTemplates = 'users/templateRoles/manage',
  OperationalRoles = 'users/operationalRoles/manage',
  DataRoles = 'users/dataRoles/manage',

  Datamap = 'docTypesHier',
  ManageCharts = 'charts',
  ManageTrendCharts = 'trendCharts',
  Settings = 'settings',
  IngestErrors = 'processingErrors/ingestion',
  ScanErrors = 'processingErrors/scan',
  GetAllLogs = 'api/logs/fetchErrMon',
  GetLogs = 'api/logs/fetch',
  GetErrorLogs = 'api/logs/fetchAll',
  ViewScanReport = 'api/communication/email/notification/sample',
  Unauthorized = 'unauthorized'


}
export enum V1RouteUrlParts {
  Scan='scan',
  Monitor='monitor',
  ProcessingErrors='processingErrors',
  OperationalRoles='operationalRoles',
  DataRoles='dataRoles',
  Manage='manage',
  Users='users',
  Rootfolders='rootfolders',
  serverComponents='serverComponents',
  history='history'
}

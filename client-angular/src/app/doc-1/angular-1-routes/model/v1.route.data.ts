import {V1RouteStateName} from "@app/doc-1/angular-1-routes/model/v1.route.state.name";

export interface V1RouteData {
  stateName: V1RouteStateName;
  version: string;
  stateParams?:any
  withoutAuth?: boolean;
}

export enum EEventServiceEventTypes {
  ReportSearchBoxUserAction = 10000,            // Generator: searchBox; Consumer: any; Purpose: signal user search action
  ReportSearchBoxSetState = 10001,            // Generator: any; Consumer: searchBox; Purpose: set search box state from another component
  ReportFilterTagsUserAction = 10002,            // Generator: filterTags; Consumer: any; Purpose:signal user action
  ReportFilterTagsSetState = 10003,            // Generator: any; Consumer: filterTags; Purpose:set filter tags state from another component
  ReportTagsMenuUserAction = 10004,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportTagsMenuSetState = 10005,            // Generator: any; Consumer: tagsMenu; Purpose:set tagsMenu state from another component
  ReportSetSearchClearUserInput = 10006,    // Generator: any; Consumer: searchBox; Purpose: make search box clear user input
  //ReportNotifyReady = 10007,                // Generator: ?; Consumer: ?; Purpose: ?
  ReportGridTagsUserAction = 10008,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridFilterUserAction = 10009,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportDocTypeMenuUserAction = 10010,            // Generator: docTypesMenu; Consumer: any; Purpose:signal user action
  ReportHeaderSetState = 10011,            // Generator: any; Consumer: header; Purpose:set saved filters after edit them in report page
  ReportHeaderUserAction = 10311,            // Generator: any; Consumer: header; Purpose:set saved filters after edit them in report page
  ReportGridDocTypesUserAction = 10012,            // Generator: grid; Consumer: docTypesMenu; Purpose:signal user action
  ReportDocTypesManageMenuUserAction = 10013,            // Generator: docTypesManageMenu; Consumer: docTypesMenu; Purpose:signal user action

  ReportGridBizListUserAction = 10014,            // Generator: grid; Consumer: bizListMenu; Purpose:signal user action
  ReportGridManageBizListUserAction = 10014,            // Generator: grid; Consumer: bizListMenu; Purpose:signal user action
  ReportBizListAssociationMenuUserAction = 10015,            // Generator: menu; Consumer: bizListMenu; Purpose:signal user action

  ReportReportDDLMenuUserAction = 10017,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportActionsRunningTasksStatusUserAction = 10018,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportChartUserAction = 10019,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportGridPoliciesUserAction = 10020,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportPolicyItemDialogUserAction = 10021,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridRootFoldersUserAction = 10022,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridScheduleGroupUserAction = 10023,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action

  ReportDateFilterMenuUserAction = 10024,            // Generator: docTypesMenu; Consumer: any; Purpose:signal user action
  ReportGridSearchPatternsUserAction = 10025,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridDataChangeUserAction = 10026,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridUsersUserAction = 10027,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportTrendChartUserAction = 10028,            // Generator: tagsMenu; Consumer: any; Purpose:signal user action
  ReportGridMediaTypeConnectionsUserAction = 10029,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportProfileUserUserAction = 10030,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridBizRolesUserAction = 10031,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridMarkItemUserAction = 10032,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportMarkedItemMenuUserAction = 10033,            // Generator: grid; Consumer: any; Purpose:signal user action
  ReportGridTagTypesManageUserAction = 10034,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGDPRGridTagsUserAction = 10035,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGDPRGridProgressUserAction = 10036,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action
  ReportGridServerComponentsUserAction = 10037,            // Generator: grid; Consumer: tagsMenu; Purpose:signal user action

  ReportBizListAutoGenerateAction = 10038,
  ReportBizListApproveAllPendingAction = 10039,
  ReportBizlistUploadAction = 10040,

  ReportFunctionalRolesAction = 10041,
  ReportGridRootFoldersActiveScanActions = 10042,
  ReportGridRootFoldersActions = 10043,
  ReportGridCustomerDataCenterUserAction = 10044,
  ReportGridTemplateRolesUserAction = 10045,
  ReportRootFolderByTexSetState = 10046
}

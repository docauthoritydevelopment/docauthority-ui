import {ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {DtoMailNotificationConfiguration, MenuItem} from '@app/navigation/types/menu-item.interface';
import {discoverItems, policyItems, reportItems, scanItems, userItems} from '@app/navigation/config/menu-items';
import {EDialogTypes} from '@angular-1-routes/model/dialog-types.enum';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';
import {RouteUrls} from '@app/types/routing.urls';
import {first, catchError} from 'rxjs/operators';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {DiscoverActionsService} from '@app/common/discover/services/discover-actions.service';
import {Observable, Subscription, forkJoin} from 'rxjs/index';
import {ServerErrorsCountService} from '@app/core/server-errors-count.service';
import {NotificationState} from '@app/core/types/notification-state';
import {DtoUserWrapper} from '@app/common/users/model/user.model';
import {UrlAuthorizationService} from '@services/url-authorization.service';
import {LoginService} from '@services/login.service';
import {LicenseService} from '@services/license.service';
import {UiStateService} from '@services/ui-state.service';
import {DialogService} from '@app/shared-ui/components/modal-dialogs/dialog.service';
import {ESystemRoleName} from '@app/common/users/model/system-role-name';
import {dashboardItems} from "@app/navigation/config/dashboard-menu-items";
import {DASHBOARD_MENU_ITEM_KEYS, USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";
import {SavedChartsService} from "@app/common/saved-user-view/saved-charts.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {UrlParamsService} from "@services/urlParams.service";
import {SaveDiscoverFilter} from "@app/common/saved-user-view/types/save-discover-filter";
import {AddEditDashboardDialogComponent} from "@app/common/dashboard/popups/addEditDashboardDialog/addEditDashboardDialog.component";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from "@angular/router";
import {CommonConfigurationService} from "@app/common/configuration/services/common-configuration.service";
import {BaseComponent} from "@core/base/baseComponent";

interface ItemAuthorization {
  item: MenuItem,
  authorized: boolean;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],

})


export class HeaderComponent extends BaseComponent  {

  V1Urls: any = V1RouteUrl;
  RouteUrls = RouteUrls;
  dialogOpen$: Observable<boolean>;
  isErrorCounterEnabled: boolean;
  provision: any;
  isCollapsed: boolean;
  trendChartsForTopMenu: Observable<any>;
  notifications$: Observable<NotificationState>;
  user: DtoUserWrapper;
  currentUser$: Observable<DtoUserWrapper>;
  reportItems: MenuItem[] = reportItems;
  policyItems: MenuItem[] = policyItems;
  scanItems = scanItems;
  dashboardItems = dashboardItems;
  userItems: MenuItem[] = userItems;
  savedFilters: MenuItem[];
  mainMenuLists:any;
  permanentDiscoverItems: MenuItem[] = discoverItems;
  private dynamicDisconverItemns: MenuItem[];
  private subscription: Subscription;

  constructor(private errorCountsService: ServerErrorsCountService,
              private routingService: RoutingService,
              private urlAuthorizationService: UrlAuthorizationService,
              private commonConfigurationService: CommonConfigurationService,
              private activatedRoute: ActivatedRoute,
              private loginService: LoginService, private v1Service: V1Service, private element: ElementRef,
              private dialogService: DialogService, private authorizationService: UrlAuthorizationService, private licenseService: LicenseService,
              private savedUserViewService: SavedUserViewService, private urlParamsService: UrlParamsService, private chartsService: SavedChartsService,
              private administrativeActionsService: DiscoverActionsService, private uiStateService: UiStateService
    , private cd: ChangeDetectorRef) {
    super();
    this.provision = {};
  }

  daOnInit() {
    this.on(this.commonConfigurationService.refreshTopMenuEvent$, () => {
      this.loadCustomCharts(false);
      this.loadCustomTrendCharts(false);
      this.loadCustomDashboards(false);
      this.loadDiscoverViews(false);
    });

    this.mainMenuLists = [
      { title:'Dashboard', iconClass:'fa fa-th-large', menuItems:dashboardItems},
      { title:'Discover', iconClass:'', menuItems:discoverItems},
      { title:'Reports', iconClass:'fa fa-bar-chart', menuItems:reportItems},
      { title:'Scan', iconClass:'', menuItems:scanItems},
      { title:'GDPR', iconClass:'fa fa-user', routerLink:RouteUrls.GDPR},
    ]; //Not in use: Bug in angular, menu not working when generated from array

    this.dialogOpen$ = this.uiStateService.isDialogOpen$();

    this.currentUser$ = this.loginService.currentUser$;
    this.setUserMenuItems();


    this.loadDiscoverViews(true);
    this.loadCustomCharts(true);
    this.loadCustomTrendCharts(true);
    this.loadCustomDashboards(true);




  }
  get discoverItems(): MenuItem[] {
    return [...(this.dynamicDisconverItemns || []), {divider: true}, ...this.permanentDiscoverItems];
  }


  clearErrors() {
    this.errorCountsService.clearErrors();
  }

  clearFatals() {
    this.errorCountsService.clearFatals();
  }

  private setUserMenuItems() {
    this.authorizationService.authorizeRolesForCurrentUser([ESystemRoleName.TechSupport,ESystemRoleName.GeneralAdmin,ESystemRoleName.RunScans ])
      .subscribe(isAuth =>{
        this.isErrorCounterEnabled = isAuth;
        if(isAuth) {
          this.notifications$ = this.errorCountsService.serverErrorCounts$;
          setTimeout(() => {
            this.errorCountsService.start();
          }, 7000); //postpone loading of charts menu items till app lis loaded
        }
    });

    const userAndLic = forkJoin(
      this.currentUser$,
      this.licenseService.loginExpired$
    );
    this.subscription =   userAndLic.pipe(
   //   skipWhile(result => !result[0]),
 //     first()
     )
      .subscribe(result => {
        let user = result[0];
        let loginExpired = result[1];

        if (loginExpired && user.bizRolesMap[ESystemRoleName.GeneralAdmin] ) {
          this.userItems.unshift({
            key:  null,
            title: 'License key...',
            clickExpression: () => this.openLicenseDialog()
          })
        }


    });


    this.dashboardItems.find(item => item.key === DASHBOARD_MENU_ITEM_KEYS.ADD_NEW_DASHBOARD).clickExpression = () => this.addNewDashboard();
    this.userItems.find(item => item.key === USER_MENU_ITEM_KEYS.LOGOUT).clickExpression = () => this.logout();
  //  this.userItems.find(item => item.key === USER_MENU_ITEM_KEYS.APPLY_ASSIGNMENT_UPDATES).clickExpression = () => this.applyAssignmentUpdates();
    let  c = this.recursiveFindItem (this.userItems,USER_MENU_ITEM_KEYS.SEND_REPORT_NOW);
    if(c) {
      c.clickExpression = () => this.sendReportNow();
    }
  }


  recursiveFindItem = (itemsList: MenuItem[], findKey)=> {
    for (let ind=0 ; ind < itemsList.length ; ind++) {
      let item:MenuItem = itemsList[ind];
      if (item.key == findKey) {
        return item;
      }
      else if (item.children) {
        let ans = this.recursiveFindItem(item.children,findKey);
        if (ans) {
          return ans;
        }
      }
    }
    return null;
  };

  private applyAssignmentUpdates() {
    this.administrativeActionsService.applyAssignmentUpdates();
  }

  private sendReportNow() {
    this.administrativeActionsService.getMailNotificationSettings().subscribe((settings: DtoMailNotificationConfiguration) => {
      if (!settings.enabled) {
        this.dialogService.showAlert('Note', 'Mail was not sent.<br>Mail notifications is disabled.<br>To enable mails, go to Settings > Mail settings > General ', AlertType.Error);
      }
      else if (!settings.addresses) {
        this.dialogService.showAlert('Note', 'Mail was not sent.<br>Missing destination address configuration.<br>Go to Settings > Mail settings > Mail Addresses ', AlertType.Error);
      }
      else {
        this.administrativeActionsService.sendMailNotificationNow().subscribe( ()=>  {
          this.dialogService.showAlert('Note', 'Scan Progress Report was sent as configured', AlertType.Info);
          });
      }
    });
  }


  private addNewDashboard = ()=>{
    let data = { data: {
      }};
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditDashboardDialogComponent, data, modelParams).result.then((newDashboard:UserViewDataDto) => {
      if (newDashboard != null) {
        this.routingService.navigate(RouteUrls.DASHBOARD_CHARTS, {dashboardId :  newDashboard.id});
      }
    },(reason) => {});
  }

  private logout() {
    this.loginService.logout();
  }

  private openLicenseDialog(){
    this.v1Service.openDialog(EDialogTypes.LicenceKeyDialog)
  }

  daOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  isAuthorized(url): Observable<boolean> {
    return this.authorizationService.authorizeUrl(url).pipe(first());
  }

  openDialog(dialogType: EDialogTypes) {
    this.v1Service.openDialog(dialogType);
  }

  private loadCustomCharts(delayLoad:boolean) {
    const customChartsParentItem = this.reportItems.filter(item => item.key == USER_MENU_ITEM_KEYS.CUSTOM_CHARTS)[0];
    if(customChartsParentItem) {
      if (delayLoad) {
        customChartsParentItem.children = [<MenuItem>{
          title: 'Loading items...', clickExpression: () => {
          }
        }];
        setTimeout(() => {
          this.loadCustomCharts(false)
        }, 6000); //postpone loading of charts menu items till app lis loaded
      }
      else {
        let isSupportMngUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
        this.savedUserViewService.getAllDetachedSavedUserViewDataList({}, null, EFilterDisplayType.DASHBOARD_WIDGET, !isSupportMngUserViewData).pipe(
          catchError((err, caught) => {
            return []
          })
        )
          .subscribe(savedCharts => {
            const chartMenuItems = savedCharts.sort((a, b) => a.name.localeCompare(b.name)).map(item => <MenuItem>{
              title: item.name,
              itemUrl: customChartsParentItem.itemUrl,
              showTooltip: true,
              queryParams: {selectedItemId: item.id.toString(),
              view : true
              }
            });
            if (chartMenuItems[0]) {
              customChartsParentItem.children = chartMenuItems;
            }
            else {
              customChartsParentItem.children = [<MenuItem>{
                title: 'No items', clickExpression: () => {
                  this.dialogService.showAlert('Note', 'To save chart, go to "Discover" page and click on the chart button, then save the chart. You can set any chart via the "Manage charts" page to be shown in menu.', AlertType.Info);
                }
              }];
            }
          })
      }
    }
  }



  private loadCustomDashboards(delayLoad:boolean) {
    const customDashboardParentItem = this.dashboardItems.filter(item => item.key == USER_MENU_ITEM_KEYS.CUSTOM_DASHBOARDS)[0];
    if(customDashboardParentItem) {
      if (delayLoad) {
        customDashboardParentItem.children = [<MenuItem>{
          title: 'Loading items...', clickExpression: () => {
          }
        }];
        setTimeout(() => {
          this.loadCustomDashboards(false)
        }, 6000); //postpone loading of charts menu items till app lis loaded
      }
      else {
        const params: any ={sort: JSON.stringify([{"dir":"asc","field":"name"}])};
        this.savedUserViewService.getSavedUserViewData(params, EFilterDisplayType.DASHBOARD, true).pipe(
          catchError((err, caught) => {
            return []
          })
        )
          .subscribe(savedDashboards => {
            const dashboardMenuItems = savedDashboards.map(chart => <MenuItem>{
              title: chart.name,
              itemUrl: customDashboardParentItem.itemUrl,
              showTooltip: true,
              queryParams: {dashboardId: chart.id.toString()}
            });
            if (dashboardMenuItems[0]) {
              customDashboardParentItem.children = dashboardMenuItems;
            }
            else {
              customDashboardParentItem.children = [<MenuItem>{
                title: 'No items', clickExpression: () => {
                  this.dialogService.showAlert('Note', 'To create new dashboard click on the "Add dashboard" menu item.', AlertType.Info);
                }
              }];
            }
          })
      }
    }
  }



  private loadCustomTrendCharts(delayLoad:boolean) {
    const customChartsParentItem = this.reportItems.filter(item => item.key == USER_MENU_ITEM_KEYS.CUSTOM_TREND_CHARTS)[0];
    if (customChartsParentItem) {
      if(delayLoad) {
        customChartsParentItem.children = [<MenuItem>{
          title: 'Loading items...', clickExpression: () => {
          }
        }];
        setTimeout(() => {
         this.loadCustomTrendCharts(false);
        }, 6500); //postpone loading of charts menu items till app lis loaded
      }
      else {
        this.chartsService.loadTrendCharsList().pipe(
          catchError((err, caught) => {
            return []
          })
        )
          .subscribe(savedTrendCharts => {
            const trendChartMenuItems = savedTrendCharts.map(chart => <MenuItem>{
              title: chart.name,
              itemUrl: customChartsParentItem.itemUrl,
              showTooltip: true,
              queryParams: {id: chart.id.toString()}
            });
            if (trendChartMenuItems[0]) {
              customChartsParentItem.children = trendChartMenuItems;
            }
            else {
              customChartsParentItem.children = [<MenuItem>{
                title: 'No items', clickExpression: () => {
                  this.dialogService.showAlert('Note', 'To save trend chart, go to "Manage trend charts" page, in the right drop down select "Show in menu".', AlertType.Info);
                }
              }];
            }
            this.cd.detectChanges();
          })
      }
    }
  }

  convertToSavedDiscoverFilterItem(userView:UserViewDataDto):SaveDiscoverFilter {
    const discoverFilter:SaveDiscoverFilter = JSON.parse(userView.displayData);
    discoverFilter.id = userView.id;
    discoverFilter.name = userView.name;
    discoverFilter.savedFilterDto = userView.filter;
    return discoverFilter;
  }

  private loadDiscoverViews(delayLoad:boolean) {
    const customFiltersParentItem = this.discoverItems.filter(item => item.key == USER_MENU_ITEM_KEYS.CUSTOM_SAVED_VIEWS)[0];
    if (customFiltersParentItem) {
      if(delayLoad) {
        customFiltersParentItem.children =  [<MenuItem>{title: 'Loading items...', clickExpression:()=>{}   }];
        setTimeout(() => {
          this.loadDiscoverViews(false);
        }, 4600);
      }
      else {
        const params: any ={sort: JSON.stringify([{"dir":"asc","field":"name"}])};
        this.savedUserViewService.getSavedUserViewData(params, EFilterDisplayType.DISCOVER_VIEW, true, null).pipe(
          catchError((err, caught) => {
            return []
          })
        ).subscribe(savedUserViewData => {
          const savedFilters: SaveDiscoverFilter[] = savedUserViewData.map(c => this.convertToSavedDiscoverFilterItem(c));
          const savedFiltersMenuItems = savedFilters.map(filter => ({
            title: filter.name,
            showTooltip: true,
            clickExpression: () => {
              localStorage.setItem("onSelectedView", JSON.stringify(true));
              this.routingService.navigate(`/${this.V1Urls.Discover}/${filter.leftEntityDisplayTypeName}/${filter.rightEntityDisplayTypeName}`, {view: filter.id.toString(), filter: filter.savedFilterDto?filter.savedFilterDto.rawFilter:null});
            }
          }));
          if (savedFiltersMenuItems[0]) {
            customFiltersParentItem.children = savedFiltersMenuItems;
          } else {
            customFiltersParentItem.children = [<MenuItem>{
              title: 'No items', clickExpression: () => {
                this.dialogService.showAlert('Note', 'To save views, go to "Discover" page, filter by items, and click "Save as" next to view title.', AlertType.Info);
              }
            }];
          }
          this.cd.detectChanges();
        });
      }
    }
  }



}


export enum USER_MENU_ITEM_KEYS {
//  APPLY_ASSIGNMENT_UPDATES= 'applyAssignmentUpdates',
  SEND_REPORT_NOW= 'sendReportNow',
  LOGOUT= 'logout',
  CUSTOM_CHARTS= 'CUSTOM_CHARTS',
  CUSTOM_DASHBOARDS= 'CUSTOM_DASHBOARDS',
  CUSTOM_TREND_CHARTS= 'CUSTOM_TREND_CHARTS',
  CUSTOM_SAVED_FILTERS= 'CUSTOM_SAVED_FILTERS',
  CUSTOM_SAVED_VIEWS    = 'CUSTOM_SAVED_VIEWS'
}


export enum DASHBOARD_MENU_ITEM_KEYS {
  ADD_NEW_DASHBOARD = 'ADD_NEW_DASHBOARD'
}









import {DASHBOARD_MENU_ITEM_KEYS, USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";

export interface DtoSmtpConfiguration {
  host:string;
  port:number;
  username:string;
  password:string;
  smtpAuthentication:boolean;
  useTls:boolean;
  useSsl:boolean;
}

export interface  DtoMailNotificationConfiguration  {
  smtpConfigurationDto:DtoSmtpConfiguration;
  addresses:string;
  enabled:boolean;
}

export interface MenuItem {
  title?: string;
  itemUrl?: string;

  /**
   * Unique identifier for the menu item
   */
  key?: USER_MENU_ITEM_KEYS | DASHBOARD_MENU_ITEM_KEYS;

  /**
   * If true, the item acts as divider betweeen two groups
   */
  divider?: boolean;
  queryParams?:  any;
  clickExpression?: () => void;
  children?: MenuItem[];
  iconClass?: string;
  /**
   * If true, the link would not be rendered as router-link but as regular href.
   * You may use it to link directly to file download
   */
  overrideRouter?: boolean;
  target?: '_self' | '_new';

  /**
   * If true, the item is hidden when login license is expired
   */
  showOnLoginLicenseExpired?: boolean;

  /**
   * If true, show the item's title as tooltip
   */
  showTooltip?: boolean;

  isDynamic?:boolean;

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotificationState} from '@core/types/notification-state';

@Component({
  selector: 'da-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Output() clearErrors = new EventEmitter();
  @Output() clearFatals = new EventEmitter();

  @Input() notification: NotificationState = {error: 0, fatal: 0};
  constructor() { }

  ngOnInit() {

  }

  clearRunErrors() {
    this.clearErrors.emit();
  }

  clearRunFatals() {
    this.clearFatals.emit();
  }

}

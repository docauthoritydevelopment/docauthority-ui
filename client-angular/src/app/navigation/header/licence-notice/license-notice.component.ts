import {Component, Input, OnInit} from '@angular/core';
import {DtoDocauthorityLicenseWrapper} from '@core/types/dto-docauthority-license';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';
import {EDialogTypes} from 'app/doc-1/angular-1-routes/model/dialog-types.enum';
import {map, skipWhile, switchMap, tap} from "rxjs/internal/operators";
import {first} from "rxjs/operators";
import {LicenseService} from "@services/license.service";
import {Observable} from "rxjs/internal/Observable";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {LoginService} from "@services/login.service";
import {RoutingService} from "@services/routing.service";
import {V1RouteUrl} from "@angular-1-routes/model/v1.route.url";

const DAYS_TO_NOTICE = 5;

@Component({
  selector: 'license-notice',
  templateUrl: './license-notice.component.html',
  styleUrls: ['./license-notice.component.scss']
})
export class LicenseNoticeComponent implements OnInit {

  loginExpired: boolean;
  daysToLoginExpiration: number
  daysToScanExpiration: number;
  initiate$:Observable<boolean>
  isGeneralAdminUser:boolean;

  constructor(private v1Service: V1Service,private licenseService: LicenseService, private urlAuthorizationService : UrlAuthorizationService, private loginService: LoginService,
              private routingService:RoutingService) {
  }

  ngOnInit() {
    this.initiate$ = this.licenseService.license$.pipe(
      skipWhile(lic=>!lic),
      tap(lic=>{
          this.daysToLoginExpiration = this.getDateDiff(lic.loginExpireDate);
          this.daysToScanExpiration = this.getDateDiff(lic.scanExpire);
          this.loginExpired = lic.loginExpired;
       }),
      map(_=> true)
    );

    this.loginService.currentUser$.pipe(
      skipWhile(user => !user),
      first()
    ).subscribe((currentUser) => {
        this.isGeneralAdminUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.GeneralAdmin, ESystemRoleName.TechSupport]);
      })

  }


  openLicenseKeyDialog() {

    if(this.isGeneralAdminUser) {
      this.routingService.navigate(V1RouteUrl.Settings+'/licenseKey',null);
    }
  }

  willLoginExpire():boolean{
    return  !this.loginExpired  && this.daysToLoginExpiration >= 0 && this.daysToLoginExpiration <= DAYS_TO_NOTICE;
  }

  willScanExpire():boolean{
    return this.daysToScanExpiration >= 0 && this.daysToScanExpiration <= DAYS_TO_NOTICE;
  }

  isScanExpired():boolean{
    return this.daysToScanExpiration < 0 ;
  }

  isLoginExpired():boolean{
    return this.loginExpired;
  }

  private getDateDiff(expiration: Date) {
    return Math.floor((expiration.getTime() - Date.now()) / (24 * 60 * 60 * 1000));
  }


}

import {Injectable} from '@angular/core';
import {concatMap, first} from 'rxjs/operators';
import {forkJoin, Observable, of, zip} from 'rxjs/index';
import {LoginService} from '@app//core/services/login.service';
import {LicenseService} from '@app//core/services/license.service';
import {map, switchMap} from 'rxjs/internal/operators';
import {DtoDocauthorityLicenseWrapper} from '@app/core/types/dto-docauthority-license';
import {UrlAuthorizationService} from '@app/core/services/url-authorization.service';
import {MenuItem} from '../types/menu-item.interface';
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";

interface ItemAuthorization {
  item: MenuItem,
  authorized: boolean;
  children?:ItemAuthorization[];
}

@Injectable({
  providedIn: 'root'
})
export class MainMenuItemsService {
  openMenusMap: { [name: string]: NgbDropdown; } = {};


  addOpenMenuItem(name:string , menu:NgbDropdown) {
    this.openMenusMap[name] = menu;
  }

  removeOpenMenuItem(name:string) {
    delete this.openMenusMap[name];
  }

  getAllOpenMenuItems():NgbDropdown[] {
    let ans:NgbDropdown[] = [];
    Object.keys(this.openMenusMap).forEach((menuName:string)=>{
      ans.push(this.openMenusMap[menuName]);
    });
    return ans;
  }

  closeAllOpenMenues(){
    let openMenus:NgbDropdown[] =  this.getAllOpenMenuItems();
    openMenus.forEach((dDown:NgbDropdown)=> {
      if (dDown && dDown.isOpen()) {
        setTimeout(()=> {
          (<any>dDown)._menuElement.nativeElement.style.display = 'none';
          dDown.close();
        });
      }
    })
    this.openMenusMap={};
  }


  constructor(private loginService: LoginService, private licenseService: LicenseService,
              private authorizationService: UrlAuthorizationService) {
  }



  public getAuthorizedMenuItems(items: MenuItem[]): Observable<MenuItem[]> {
    return this.licenseService.loginExpired$.pipe(
  //    first(),
      map(loginExpired => {
        return !loginExpired ? items : items/*.filter(item => item.showOnLoginLicenseExpired)*/;
      }),
     switchMap(filteredItems => this.filterByAuthorizedUrl(filteredItems)))

  }

  public getAuthorizedRouterLink(routerLinkUrl: string): Observable<boolean> {
    return this.licenseService.loginExpired$.pipe(
   //   first(),
      map(loginExpired => {
        return !loginExpired ? routerLinkUrl :null;
      }),
      switchMap(filteredItem => filteredItem? this.authorizationService.authorizeUrl(routerLinkUrl):of(false))
    );

  }





  private authorizeItem(item: MenuItem): Observable<ItemAuthorization> {
    if (item.children  && item.children.length > 0 ) {
      const childObservable = item.children.map(childItem => this.authorizeItem(childItem));
      return forkJoin(childObservable).pipe(
        concatMap((children:ItemAuthorization[]) => {
          let filterChildren = children.filter(child => {
            return (child.authorized &&  !child.item.divider);
          });
          if (filterChildren && filterChildren.length > 0) {
            let childWithDivider = children.filter(child => {
              return (child.authorized || child.item.divider);
            });
            return of (<ItemAuthorization> {item, authorized: true, children:childWithDivider})
          }
          else if (item.itemUrl || item.key){
            return this.authorizationService.authorizeUrl(item.itemUrl?item.itemUrl:item.key).pipe(
              map(result => {
                return <ItemAuthorization> {item, authorized: result}
              }));
          }
          else {
            return of({item: item, authorized: false});
          }
        }));
    }
    else if (item.itemUrl || item.key){
      return this.authorizationService.authorizeUrl(item.itemUrl?item.itemUrl:item.key).pipe(
        map(result => {
          return <ItemAuthorization> {item, authorized: result}
        }));
    }
    else {
      return of({item: item, authorized: item.divider ? true : false});
    }
  }


  private filterByAuthorizedUrl(menuItems: MenuItem[]): Observable<MenuItem[]> {
    const observables = menuItems.map(item =>  this.authorizeItem(item));
    return forkJoin(observables).pipe(
      map((result: ItemAuthorization[]) => {
          return this.filterByItemAuthorizationList(result);
    }));
  }



  filterByItemAuthorizationList(items: ItemAuthorization[]):MenuItem[] {
    let ans = [];
    items.forEach((item:ItemAuthorization)=> {
      if (item.authorized) {
        let resItem = item.item;
        if (item.children) {
          resItem.children = this.filterByItemAuthorizationList(item.children);
        }
        ans.push(resItem);
      }
    })
    return ans;
  }


  /*
  private authorizeItem(item: MenuItem): Observable<ItemAuthorization> {
    if (item.itemUrl || item.clickExpression ) {
      return this.authorizationService.authorizeUrl(item.itemUrl?item.itemUrl:item.key).pipe(
        map(result => {
          return <ItemAuthorization> {item, authorized: result}
      }));
    }
    if (item.children) {


     const childObservable = item.children.map(childItem => this.authorizeItem(childItem));
      const allAuthResults =  forkJoin(childObservable).pipe(
        map(children => {
          return children.filter(child => child.authorized);
        }));

      return allAuthResults.pipe(
        map(childAuthItems =>{
            return <ItemAuthorization> {item: item, authorized:childAuthItems.filter(a=>!a.item.divider).length> 0}
         }));
    }
    return of({item: item, authorized: item.divider});
  } */


}

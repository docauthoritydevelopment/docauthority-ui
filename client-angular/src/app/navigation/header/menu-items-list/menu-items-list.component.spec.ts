import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MenuItemsListComponent} from './menu-items-list.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {V1Module} from '@app/doc-1/v1-frame/v1.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CoreModule} from '@core/core.module';
import {SharedUiModule} from '@shared-ui/shared-ui.module';
import {coreReducer} from '@core/store/core.reducer';

describe('MenuItemsListComponent', () => {
  let component: MenuItemsListComponent;
  let fixture: ComponentFixture<MenuItemsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedUiModule, RouterModule.forRoot([]), StoreModule.forRoot(coreReducer)
        , V1Module.forRoot(), NgbModule.forRoot(), CoreModule.forRoot()],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}],
      declarations: [MenuItemsListComponent, MenuItemComponent,]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();
  });

  it('should keep items intact', async () => {
    const itemA = [{'title': ''}, {'title': 'File groups', 'itemUrl': '/report/groups/files'}, {
      'title': 'Folders',
      'itemUrl': '/report/folders/files'
    }, {'title': null}, {
      'title': 'Advanced',
      'children': [{
        'title': 'Files by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/files'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/files'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/files'}]
      }, {
        'title': 'File groups by',
        'children': [{'title': 'Folders', 'itemUrl': '/report/folders/groups'}, {
          'title': 'Folders list',
          'itemUrl': '/report/folders_flat/groups'
        }, {'title': 'Owners', 'itemUrl': '/report/owners/groups'}, {
          'title': 'Read ACL',
          'itemUrl': '/report/acl_reads/groups'
        }, {'title': 'Write ACL', 'itemUrl': '/report/acl_writes/groups'}]
      }, {
        'title': 'Folders by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/folders'}, {
          'title': 'Owners',
          'itemUrl': '/report/owners/folders'
        }, {'title': 'Read ACL', 'itemUrl': '/report/acl_reads/folders'}, {
          'title': 'Write ACL',
          'itemUrl': '/report/acl_writes/folders'
        }]
      }, {
        'title': 'Owners by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/owners'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/owners'
        }]
      }, {
        'title': 'Read ACLs by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/acl_reads'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/acl_reads'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/acl_reads'}]
      }, {
        'title': 'Write ACLs by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/acl_writes'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/acl_writes'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/acl_writes'}]
      }]
    }];

    const itemB = [{'title': 'File groups', 'itemUrl': '/report/groups/files'}, {
      'title': 'Folders',
      'itemUrl': '/report/folders/files'
    }, {'title': null}, {
      'title': 'Advanced',
      'children': [{
        'title': 'Files by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/files'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/files'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/files'}]
      }, {
        'title': 'File groups by',
        'children': [{'title': 'Folders', 'itemUrl': '/report/folders/groups'}, {
          'title': 'Folders list',
          'itemUrl': '/report/folders_flat/groups'
        }, {'title': 'Owners', 'itemUrl': '/report/owners/groups'}, {
          'title': 'Read ACL',
          'itemUrl': '/report/acl_reads/groups'
        }, {'title': 'Write ACL', 'itemUrl': '/report/acl_writes/groups'}]
      }, {
        'title': 'Folders by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/folders'}, {
          'title': 'Owners',
          'itemUrl': '/report/owners/folders'
        }, {'title': 'Read ACL', 'itemUrl': '/report/acl_reads/folders'}, {
          'title': 'Write ACL',
          'itemUrl': '/report/acl_writes/folders'
        }]
      }, {
        'title': 'Owners by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/owners'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/owners'
        }]
      }, {
        'title': 'Read ACLs by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/acl_reads'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/acl_reads'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/acl_reads'}]
      }, {
        'title': 'Write ACLs by',
        'children': [{'title': 'File groups', 'itemUrl': '/report/groups/acl_writes'}, {
          'title': 'Folders',
          'itemUrl': '/report/folders/acl_writes'
        }, {'title': 'Folders list', 'itemUrl': '/report/folders_flat/acl_writes'}]
      }]
    }];

    component.items = itemA;

    const componentItemA = component.items;

    component.items = itemB;
    const promise = new Promise(resolve => {
      setTimeout(() => {
        // Expect component.items not to be changed
        resolve(component.items === componentItemA)
      }, 1000)
    });


    const result = await promise;

    expect(result).toBeTruthy()
  });
})

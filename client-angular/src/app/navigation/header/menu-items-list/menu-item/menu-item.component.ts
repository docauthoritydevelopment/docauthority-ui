import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MenuItem} from '../../../types/menu-item.interface';

@Component({
  selector: '[da-menu-item]',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() item: MenuItem;
  @Input() isRight: boolean;
  @Input() iconClass: string;
  @Output() itemClicked = new EventEmitter<MenuItem>()

  constructor() {

  }

  ngOnInit() {

  }

  getTitle(): string {
    if (this.item && this.item.showTooltip) {
      return this.item.title;
    }
    return null;
  }

  menuItemClick() {
    this.itemClicked.emit(this.item);
  }

  clickOnSubDiv(event) {
    if (event.target.className .indexOf('sub-menu-container')>-1) {
      event.stopImmediatePropagation();
    }
  }

}

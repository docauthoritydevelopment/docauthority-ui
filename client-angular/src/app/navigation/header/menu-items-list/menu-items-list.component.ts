import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MenuItem} from '../../types/menu-item.interface';
import * as _ from 'lodash';
import {MainMenuItemsService} from "@app/navigation/header/main-menu-items.service";
import {first} from "rxjs/operators";
import {Observable} from "rxjs/index";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {V1Service} from "@app/doc-1/v1-frame/v1.service";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'main-menu-item',
  templateUrl: './menu-items-list.component.html',
  styleUrls: ['./menu-items-list.component.scss'],

})
export class MenuItemsListComponent implements OnInit {
  menuItemGroups: MenuItem[][];
  routerLinkAuthorized$:Observable<boolean>
  routerLinkUrl:string;

  @ViewChild('myDrop')
  myDrop: NgbDropdown;



  // private _items:MenuItem[];

  @Input() menuHeader: string;
  @Input() iconClass: string;
  @Input() isRight: boolean;
  @Input() disable: boolean;
  @Input() set routerLink(routerLinkUrl: string){
    if(routerLinkUrl) {
      this.routerLinkUrl = routerLinkUrl;
      this.routerLinkAuthorized$ =   this.mainMenuItemsService.getAuthorizedRouterLink(routerLinkUrl);
    }
  };
  @Input() set items(items: MenuItem[]) {
    if(items) {

      this.mainMenuItemsService.getAuthorizedMenuItems(items).subscribe(authItems => {
       // console.log("getAuthorizedMenuItems "+authItems[0].itemUrl);
        const filteredItems = this.filterAdjacentDividers(authItems);
        // if (_.isEqual(filteredItems, this._items)) {
        //   return;
        // }
     //   this._items = filteredItems;
       this.menuItemGroups = this.buildItemGroups(filteredItems)
     //   this.menuItemGroups = this.buildItemGroups(items)
     //
     //
       })
    }
  }

  constructor(private mainMenuItemsService: MainMenuItemsService,
              private v1Service : V1Service,
              private authorizationService: UrlAuthorizationService) {

  }


  ngOnInit() {
  }


  private filterAdjacentDividers(items: MenuItem[]): MenuItem[] {
    const result = [];
    items.forEach((item, index) => {
      if (item.children && item.children.length) {
        item.children = this.filterAdjacentDividers(item.children)
      }
      if (item.title || items[index - 1] && items[index - 1].title) {
        result.push(item);
      }
    })

    return result;
  }

  onDropDownOpenChanged(isOpen) {
    if (isOpen) {
      (<any>this.myDrop)._menuElement.nativeElement.style.display = '';
      this.mainMenuItemsService.addOpenMenuItem(this.menuHeader,this.myDrop);
    }
    else {
      this.mainMenuItemsService.removeOpenMenuItem(this.menuHeader);
    }
  }

  private buildItemGroups(filteredItems:  MenuItem[]): MenuItem[][] {

    const result = [];
    let tempArray: MenuItem[];
    filteredItems.forEach(item => {
      if (!tempArray) {
        tempArray = [];
        result.push(tempArray);
      }
      if (!item.divider) {
        tempArray.push(item)
      } else {
        tempArray = null;
      }
    });
    return result;
  }


}

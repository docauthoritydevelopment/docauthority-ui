import {ESystemRoleName} from '@users/model/system-role-name';

/**
 * This interface uses for url permission configuration
 */

export interface UrlRoles {
  /**
   * Url string
   */
  url: string;

  /**
   * Roles collection
   */
  roles: ESystemRoleName[];

  showOnLoginLicenseExpired?:boolean;

}



import {MenuItem} from '@app/navigation/types/menu-item.interface';
import {UrlRoles} from '@app/navigation/config/url-roles';
import {ESystemRoleName} from '@users/model/system-role-name';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {RouteUrls} from "@app/types/routing.urls";
import {DASHBOARD_MENU_ITEM_KEYS, USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";


export const dashboardItems: MenuItem[] = [
  /* {title: 'dystem Dashboard', itemUrl: RouteUrls.DASHBOARD_SYSTEM},
  {title: 'IT dashboard', itemUrl: RouteUrls.DASHBOARD_IT},
  {title: 'User dashboard', itemUrl: RouteUrls.DASHBOARD_USER}, */
  {title: 'System dashboard', itemUrl: V1RouteUrl.DashboardSystem, },
  {title: 'IT dashboard', itemUrl: V1RouteUrl.DashboardIT},
  {title: 'User dashboard', itemUrl: V1RouteUrl.DashboardUser},
  {title: '', divider: true},
  {title: 'Custom dashboard', isDynamic: true, key:USER_MENU_ITEM_KEYS.CUSTOM_DASHBOARDS, itemUrl: RouteUrls.DASHBOARD_CHARTS},
  {title: '', divider: true},
  {title: 'Manage dashboards', itemUrl: RouteUrls.SavedDashboards},
  {title: 'Add dashboard', key: DASHBOARD_MENU_ITEM_KEYS.ADD_NEW_DASHBOARD, clickExpression: () => {}}
];

let
  isViewSystemDashBoard = [ESystemRoleName.ViewContent],
  isViewITDashBoard = [ESystemRoleName.RunScans],
  isViewDashboardCharts = [ESystemRoleName.None],
  isViewUserDashBoard = [ESystemRoleName.Not,ESystemRoleName.RunScans,ESystemRoleName.ViewContent],
  isManageDashBoard = [ESystemRoleName.None];

export const dashboardUrlRolesConfig: UrlRoles[] =
  [
    {url: RouteUrls.DASHBOARD_SYSTEM, roles: isViewSystemDashBoard},
    {url: RouteUrls.DASHBOARD_USER, roles: isViewUserDashBoard},
    {url: RouteUrls.DASHBOARD_IT, roles: isViewITDashBoard},
    {url: RouteUrls.DASHBOARD_CHARTS, roles: isViewDashboardCharts},
    {url: RouteUrls.SavedDashboards, roles: isManageDashBoard},
    {url: V1RouteUrl.DashboardForwarder, roles: isManageDashBoard},
    {url: V1RouteUrl.DashboardSystem, roles: isViewSystemDashBoard},
    {url: V1RouteUrl.DashboardIT, roles: isViewITDashBoard},
    {url: V1RouteUrl.DashboardUser, roles: isViewUserDashBoard  }
  ];

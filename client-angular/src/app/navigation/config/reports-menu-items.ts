import {MenuItem} from '@app/navigation/types/menu-item.interface';
import {UrlRoles} from '@app/navigation/config/url-roles';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {ESystemRoleName} from '@app/common/users/model/system-role-name';
import {USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";
import {RouteUrls} from "@app/types/routing.urls";

export const reportItems: MenuItem[] = [

  {title: 'Custom charts',  isDynamic: true, key:USER_MENU_ITEM_KEYS.CUSTOM_CHARTS, itemUrl:RouteUrls.SavedWidgets},
  {title: 'Custom trend charts',  isDynamic: true, key:USER_MENU_ITEM_KEYS.CUSTOM_TREND_CHARTS, itemUrl:V1RouteUrl.trendChartView},
  {title: '', divider: true},
  {title: 'Datamap', itemUrl: V1RouteUrl.Datamap},
  {title: '', divider: true},
  {title: 'Manage charts', itemUrl: RouteUrls.SavedWidgets},
  {title: 'Manage trend charts', itemUrl: V1RouteUrl.ManageTrendCharts}];

const isViewReportsUser = [ESystemRoleName.ViewReports, ESystemRoleName.MngReports];
const isMngReportsUser = [ESystemRoleName.MngReports];
const isManageDashBoardWidget = [ESystemRoleName.None];

export const reportUrlRolesConfig: UrlRoles[] =
  [{url: V1RouteUrl.Datamap, roles: isViewReportsUser},
    {url: RouteUrls.SavedWidgets, roles: isManageDashBoardWidget},
    {url: V1RouteUrl.trendChartView, roles: isViewReportsUser},
    {url: V1RouteUrl.ManageCharts, roles: isMngReportsUser},
    {url: V1RouteUrl.ManageTrendCharts, roles: isMngReportsUser},
  ];

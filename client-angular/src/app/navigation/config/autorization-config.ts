import {UrlRoles} from '@app/navigation/config/url-roles';
import {ESystemRoleName} from '@users/model/system-role-name';
import {scanUrlRolesConfig} from '@app/navigation/config/scan-menu-items';
import {reportUrlRolesConfig} from '@app/navigation/config/reports-menu-items';
import {RouteUrls} from '@app/types/routing.urls';
import {userUrlRolesConfig} from '@app/navigation/config/user-item-authorization';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {dashboardUrlRolesConfig} from "@app/navigation/config/dashboard-menu-items";


export const localConfig: UrlRoles [] =
  [
    {url: RouteUrls.GDPR, roles: [ESystemRoleName.MngGDPR]},
    {url: RouteUrls.DISCOVER, roles: [ESystemRoleName.None]},
    {url: V1RouteUrl.Discover, roles: [ESystemRoleName.None]},
  ];
export const authorizationConfig: UrlRoles [] = [...scanUrlRolesConfig, ...localConfig, ...reportUrlRolesConfig, ...userUrlRolesConfig, ...dashboardUrlRolesConfig]

;



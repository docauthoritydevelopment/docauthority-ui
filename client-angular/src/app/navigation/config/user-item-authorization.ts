import {UrlRoles} from '@app/navigation/config/url-roles';
import {ESystemRoleName} from '@users/model/system-role-name';
import {RouteUrls} from '@app/types/routing.urls';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";

const isViewUsersUser = [ESystemRoleName.ViewUsers],
  isMngUserAndRolesUser = [ESystemRoleName.MngUserRoles, ESystemRoleName.MngRoles, ESystemRoleName.MngUsers],
  isGeneralAdminUser = [ESystemRoleName.GeneralAdmin, ESystemRoleName.TechSupport],
  isRunScansUser = [ESystemRoleName.RunScans],
  isViewDocTypeTreeUser = [ESystemRoleName.ViewDocTypeTree],
  isViewLogLevelUser = [ESystemRoleName.TechSupport],
  isViewSearchPatternsUser = [ESystemRoleName.ManageSearchPatterns],
  isViewPatternCategoriesUser = [ESystemRoleName.ActivatePatternCategories],
  isViewRegulationsUser = [ESystemRoleName.ActivateRegulations],
  isViewAuditTrailUser = [ESystemRoleName.ViewAuditTrail];


export const userUrlRolesConfig: UrlRoles[] =
  [
    {url: V1RouteUrl.Settings, roles: [ESystemRoleName.None], showOnLoginLicenseExpired:true},
    {url: V1RouteUrl.Users, roles: [...isMngUserAndRolesUser, ...isViewUsersUser]},
    {url: V1RouteUrl.RoleTemplates, roles: isMngUserAndRolesUser},
    {url: V1RouteUrl.DataRoles, roles: isMngUserAndRolesUser},
    {url: V1RouteUrl.OperationalRoles, roles: isMngUserAndRolesUser},
    {url: V1RouteUrl.Profile, roles: [ESystemRoleName.None], showOnLoginLicenseExpired:true},
    {url: RouteUrls.ScanErrors, roles: isRunScansUser},
    {url: RouteUrls.Audit, roles: isViewAuditTrailUser},
    {url: RouteUrls.DocTypes, roles: isViewDocTypeTreeUser},
    {url: RouteUrls.LogLevels, roles: isViewLogLevelUser},
    {url: RouteUrls.SettingsScheduleGroups, roles: [ESystemRoleName.None]},
    {url: RouteUrls.DateFilters, roles: [ESystemRoleName.None]},
    {url: RouteUrls.SearchPatterns, roles: isViewSearchPatternsUser},
    {url: RouteUrls.PatternCategories, roles: isViewPatternCategoriesUser},
    {url: RouteUrls.Regulations, roles: isViewRegulationsUser},
    {url: RouteUrls.IngestErrors, roles: isRunScansUser},
    {url: V1RouteUrl.GetErrorLogs, roles: isGeneralAdminUser},
    {url: V1RouteUrl.GetLogs, roles: isGeneralAdminUser},
    {url: V1RouteUrl.GetAllLogs, roles: isGeneralAdminUser},
    {url: V1RouteUrl.ViewScanReport, roles: isRunScansUser},
    {url: USER_MENU_ITEM_KEYS.SEND_REPORT_NOW, roles: isRunScansUser},
    {url: USER_MENU_ITEM_KEYS.LOGOUT, roles: [ESystemRoleName.None], showOnLoginLicenseExpired:true},
 //   {url: USER_MENU_ITEM_KEYS.APPLY_ASSIGNMENT_UPDATES, roles: [ESystemRoleName.None]},


  ];

import {MenuItem} from '@app/navigation/types/menu-item.interface';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {RouteUrls} from '@app/types/routing.urls';
import {USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";


export const discoverItems: MenuItem[] = [
  // {title: 'Saved Filters', clickExpression: () => loadSavedFilters()},
  {title: 'Views',  isDynamic: true, key:USER_MENU_ITEM_KEYS.CUSTOM_SAVED_VIEWS, itemUrl:V1RouteUrl.Discover},
  {title: '', divider: true},
  {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'files'})},
  {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'files'})},
  {divider: true},
  {title: 'Discover', itemUrl: `/${V1RouteUrl.Discover}/${V1RouteUrl.Search}`},
  // {title: 'Search', itemUrl: RouteUrls.SEARCH},
  // {title: 'New File Groups', itemUrl: getNewReportLink({left: 'groups', right: 'files'})},

  // {
  //   title: 'Advanced', children: [
  //     {
  //       title: 'Files by', children: [
  //         {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'files'})},
  //         {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'files'})},
  //         {title: 'Folders list', itemUrl: createDiscoverLink({left: 'folders_flat', right: 'files'})}
  //       ]
  //     },
  //     {
  //       title: 'File groups by', children: [
  //         {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'groups'})},
  //         {title: 'Folders list', itemUrl: createDiscoverLink({left: 'folders_flat', right: 'groups'})},
  //         {title: 'Owners', itemUrl: createDiscoverLink({left: 'owners', right: 'groups'})},
  //         {title: 'Read ACL', itemUrl: createDiscoverLink({left: 'acl_reads', right: 'groups'})},
  //         {title: 'Write ACL', itemUrl: createDiscoverLink({left: 'acl_writes', right: 'groups'})},
  //       ]
  //     },
  //     {
  //       title: 'Folders by', children: [
  //         {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'folders'})},
  //         {title: 'Owners', itemUrl: createDiscoverLink({left: 'owners', right: 'folders'})},
  //         {title: 'Read ACL', itemUrl: createDiscoverLink({left: 'acl_reads', right: 'folders'})},
  //         {title: 'Write ACL', itemUrl: createDiscoverLink({left: 'acl_writes', right: 'folders'})},
  //       ]
  //     },
  //     {
  //       title: 'Owners by', children: [
  //         {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'owners'})},
  //         {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'owners'})},
  //       ]
  //     },
  //     {
  //       title: 'Read ACLs by', children: [
  //         {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'acl_reads'})},
  //         {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'acl_reads'})},
  //         {title: 'Folders list', itemUrl: createDiscoverLink({left: 'folders_flat', right: 'acl_reads'})},
  //       ]
  //     },
  //     {
  //       title: 'Write ACLs by', children: [
  //         {title: 'File groups', itemUrl: createDiscoverLink({left: 'groups', right: 'acl_writes'})},
  //         {title: 'Folders', itemUrl: createDiscoverLink({left: 'folders', right: 'acl_writes'})},
  //         {title: 'Folders list', itemUrl: createDiscoverLink({left: 'folders_flat', right: 'acl_writes'})},
  //       ]
  //     }
  //
  //
  //   ]
  // }
];

export interface ReportOptions {
  left: string;
  right: string;
  filter?: string;
}

function createDiscoverLink(reportOptions: ReportOptions): string {

  return `/${V1RouteUrl.Discover}/${reportOptions.left}/${reportOptions.right}`;
}

function getNewReportLink(reportOptions: ReportOptions): string {

  return `/${RouteUrls.DISCOVER_SPLIT}`;
}


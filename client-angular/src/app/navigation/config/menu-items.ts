import {MenuItem} from 'app/navigation/types/menu-item.interface';


export * from './user-menu-items';
export * from './reports-menu-items';

export * from './discover-menu-items';


export const policyItems: MenuItem[] = [{title: 'Manage', itemUrl: '/policy'}];

export * from './scan-menu-items';







import {MenuItem} from '@app/navigation/types/menu-item.interface';
import {RouteUrls} from '@app/types/routing.urls';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {USER_MENU_ITEM_KEYS} from "@app/navigation/types/navigation.types";


export const userItems: MenuItem[] = [
  // {
  //    key: USER_MENU_ITEM_KEYS.APPLY_ASSIGNMENT_UPDATES,
  //   title: 'Apply assignment...',
  //   clickExpression: () => {}
  // },
  {
    title: 'System',
    children: [
      {title: 'Ingestion errors', itemUrl: RouteUrls.IngestErrors},
//      {title: 'Ingestion errors', itemUrl: RouteUrls.IngestErrors},
      {title: 'Map errors', itemUrl: RouteUrls.ScanErrors},
      {title: 'Audit trail', itemUrl: RouteUrls.Audit},
      {divider: true},
      {title: 'Get error log', itemUrl: '/api/logs/fetchErrMon', overrideRouter: true, target: '_self'},
      {title: 'Get logs', itemUrl: '/api/logs/fetch', overrideRouter: true},
      {title: 'Get all logs', itemUrl: '/api/logs/fetchAll', overrideRouter: true},

    ]
  },
  {title: 'Settings', itemUrl: `${V1RouteUrl.Settings}`, iconClass: 'fa-cogs'},
  {divider: true},
  {
    title: 'Mail notifications',  children: [
      {
        key: USER_MENU_ITEM_KEYS.SEND_REPORT_NOW,
        title: 'Send report now', clickExpression: () => {}
      },
      {title: 'View report', itemUrl: V1RouteUrl.ViewScanReport, overrideRouter: true, target: '_new'},
    ]
  },
  {divider: true},
  {
    title: 'Users & Roles', children: [
      {title: 'Users', itemUrl: V1RouteUrl.Users, iconClass: 'fa-user'},
      {title: 'Role templates', itemUrl: V1RouteUrl.RoleTemplates, iconClass: 'fa-building-o'},
      {title: 'Operational roles', itemUrl: V1RouteUrl.OperationalRoles, iconClass: 'fa-key'},
      {title: 'Data roles', itemUrl: V1RouteUrl.DataRoles, iconClass: 'fa-address-book'}

    ]
  },
  {title: 'Profile', itemUrl: V1RouteUrl.Profile ,/* showAlways:true*/},
  // {divider: true},
  // {title: 'Active MQ', itemUrl: '/active-mq-url/', overrideRouter: true},
  {divider: true},
  {title: 'Logout', key: USER_MENU_ITEM_KEYS.LOGOUT, clickExpression: () => { }},
];



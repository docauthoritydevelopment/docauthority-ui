import {MenuItem} from '@app/navigation/types/menu-item.interface';
import {UrlRoles} from '@app/navigation/config/url-roles';
import {ESystemRoleName} from '@users/model/system-role-name';
import {V1RouteUrl} from '@app/doc-1/angular-1-routes/model/v1.route.url';
import {RouteUrls} from "@app/types/routing.urls";


export const scanItems: MenuItem[] =
  [
  //  {title: 'Scan Center', itemUrl: RouteUrls.SCAN_CENTER},
    {title: 'Active scans', itemUrl: RouteUrls.ACTIVE_SCANS},
    {title: 'Schedule groups', itemUrl: RouteUrls.ScheduleGroups},
    // {title: 'Active scans', itemUrl: V1RouteUrl.ActiveScans},
    {title: 'Scan history', itemUrl: RouteUrls.SCAN_HISTORY},
    {title: 'Root folders', itemUrl: V1RouteUrl.RootFolders},
    {title: 'System components', itemUrl: V1RouteUrl.ServerComponents},
//    {title: 'System components new', itemUrl: RouteUrls.SERVER_COMPONENTS},
    {title: 'Business lists monitor', itemUrl: RouteUrls.BUSINESS_LIST_MONITOR},
    {divider: true},
    {
      title: 'Configuration', children: [
//        {title: 'Search patterns', itemUrl: V1RouteUrl.SearchPatterns},
        {title: 'Business lists settings', itemUrl: V1RouteUrl.BizLists},
      ]
    }];

const
  isRunScansUser = ESystemRoleName.RunScans,
  isMngScanConfigUser = ESystemRoleName.MngScanConfig,
   isScanUser=[isRunScansUser, isMngScanConfigUser];

export const scanUrlRolesConfig: UrlRoles[] =
  [
    {url: RouteUrls.SCAN_CENTER, roles: isScanUser},
    {url: RouteUrls.ACTIVE_SCANS, roles: isScanUser},
    {url: RouteUrls.ScheduleGroups, roles: isScanUser},
    {url: V1RouteUrl.RootFolders, roles: isScanUser},
    {url: V1RouteUrl.ServerComponents, roles: isScanUser},
    {url: V1RouteUrl.SearchPatterns, roles: isScanUser},
    {url: V1RouteUrl.BizLists, roles: isScanUser},
    {url: RouteUrls.BUSINESS_LIST_MONITOR, roles: isScanUser},
    {url: RouteUrls.SCAN_HISTORY, roles: isScanUser},
  ]

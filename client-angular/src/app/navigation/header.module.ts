import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header.component';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MenuItemsListComponent} from './header/menu-items-list/menu-items-list.component';
import {MenuItemComponent} from './header/menu-items-list/menu-item/menu-item.component';
import {NotificationComponent} from './header/notification/notification.component';
import {LicenseNoticeComponent} from './header/licence-notice/license-notice.component';
import {SharedUiModule} from '@app/shared-ui/shared-ui.module';
import {SavedUserViewModule} from '@app/common/saved-user-view/saved-user-view.module';
import {MainMenuItemsService} from "@app/navigation/header/main-menu-items.service";
import {CommonDashboardModule} from "@app/common/dashboard/common-dashboard.module";
import {AddEditDashboardDialogComponent} from "@app/common/dashboard/popups/addEditDashboardDialog/addEditDashboardDialog.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedUiModule,
    NgbModule,
    SavedUserViewModule,
    CommonDashboardModule
  ],
  providers: [MainMenuItemsService],
  entryComponents: [AddEditDashboardDialogComponent],
  declarations: [HeaderComponent, MenuItemsListComponent, MenuItemComponent, NotificationComponent, LicenseNoticeComponent],
  exports: [HeaderComponent]
})
export class HeaderModule {
}

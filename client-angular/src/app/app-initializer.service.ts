import {Injectable} from '@angular/core';
import {AppInitializer} from '@app/app-initializer';
import { GetUserDetailsAttemptAction} from '@core/store/authentication.actions';
import {GetNotifications} from '@core/store/core.actions';
import {Store} from '@ngrx/store';
import { DoctypesService } from './common/discover/services/doctypes.service';
import {Router} from "@angular/router";
import {RoutingService} from "@services/routing.service";
//import {AppServicesManagerService} from '@app/doc-1/v1-frame/app-services-manager.service';
//Not in use
@Injectable()
export class AppInitializerService extends AppInitializer {

  constructor(private store: Store<any>,private routingService : RoutingService, private docTypeService: DoctypesService /*, private appManagerService: AppServicesManagerService*/) {
    super()
    // this.appManagerService.registerService(
    //   'add-doctypes-to-file',
    //   (docTypeId: string, displayType: string, fileIds: string[]) =>
    //     this.docTypeService.addDocTypesToFiles(docTypeId, displayType, fileIds)
    // )
  }

  initUserDetails() {

    const action = new GetUserDetailsAttemptAction(this.routingService.getCurrentUrl());
  //  action.metadata.successSideActions = [() => new GetNotifications(), () => new GetLoginLicence()];
    this.store.next(action);
  }

}

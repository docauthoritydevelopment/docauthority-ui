import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {HeaderModule} from './navigation/header.module';
import {ExtraOptions, PreloadingStrategy, Route, RouterModule} from '@angular/router';
import {appRoutes} from './app.routing';
import {FooterComponent} from './layout/footer/footer.component';
import {SubHeaderComponent} from './layout/sub-header/sub-header.component';
import {BreadcrumbComponent} from './layout/breadcrumb/breadcrumb.component';
import {combinedReducers} from './store/app.reducers';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Angular1RoutesModule} from '@angular-1-routes/angular-1-routes.module';
import {CustomRouterStateSerializer} from '@app/store/router-state-serializer';
import {V1Module} from '@app/doc-1/v1-frame/v1.module';
import {RunningTaskStatusComponent} from '@app/layout/footer/running-task-status/running-task-status.component';
import {ApiCallsEffect} from '@app/core/store/api-calls.effect';
import {AuthenticationEffects} from '@app/core/store/authentication.effects';
import {PollingEffects} from '@app/core/store/polling.effects';
import {CoreModule} from '@app/core/core.module';
import {ApplyToSolrEffects} from '@app/common/saved-user-view/apply-to-solr.effects';
import {SavedUserViewModule} from "@app/common/saved-user-view/saved-user-view.module";

import {MetadataModule} from "@app/common/modules/metadata.module";
import {LoaderModule} from "@shared-ui/components/loader/loader.module";
import {MainLoaderModule} from "@app/common/modules/mainLoader.module";
import {Observable} from "rxjs/internal/Observable";
import {timer} from "rxjs/internal/observable/timer";
import {flatMap} from "rxjs/operators";
import {of} from "rxjs/internal/observable/of";
import {DownloadIndicatorComponent} from "@app/layout/footer/downloadIndicator/downloadIndicator.component";
import {AppConfig} from "@services/appConfig.service";
import {TranslateModule} from "@ngx-translate/core";



import { NgCircleProgressModule } from 'ng-circle-progress';
import {CommonConfigurationModule} from "@app/common/configuration/common-configuration.module";

declare var require:any;

export class AppPreloadingStrategy implements PreloadingStrategy {
  preload(route: Route, load: Function): Observable<any> {
    const loadRoute = (delay) => delay
      ? timer(1450).pipe(flatMap(_ => load()))
      : load();
    return route.data && route.data.preload
      ? loadRoute(route.data.delay)
      : of(null);
  }
}
export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always',
  onSameUrlNavigation: 'reload',
  enableTracing: false,
  preloadingStrategy: AppPreloadingStrategy
};

/*
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function appInitializerFactory(translate: TranslateService) {
  return () => new Promise<any>((resolve: any) => {
    let langToSet = "en";
    translate.setDefaultLang('en');
    translate.use(langToSet).subscribe(() => {
      console.info(`Successfully initialized '${langToSet}' language.'`);
    }, err => {
      console.error(`Problem with '${langToSet}' language initialization.'`);
    }, () => {
      resolve(null);
    });
  });
}
*/

@NgModule({

  imports: [
    BrowserModule,
    HttpClientModule,
    HeaderModule,
    V1Module.forRoot(),
    SavedUserViewModule.forRoot(),
    Angular1RoutesModule,
    MetadataModule.forRoot(),
    MainLoaderModule.forRoot(),
    LoaderModule,
    CommonConfigurationModule,
    NgCircleProgressModule.forRoot({
      backgroundPadding: 0,
      radius: 12,
      space: -3,
      maxPercent: 100,
      outerStrokeGradient: true,
      outerStrokeWidth: 3,
      outerStrokeColor: "#400040",
      outerStrokeGradientStopColor: "#808080",
      innerStrokeColor: "#e7e8ea",
      innerStrokeWidth: 3,
      title: "UI",
      imageHeight: 30,
      imageWidth: 30,
      animation: false,
      animateTitle: false,
      animationDuration: 1000,
      showTitle: false,
      showSubtitle: false,
      showUnits: false,
      showBackground: false
    }),
    RouterModule.forRoot(appRoutes, routingConfiguration),
    StoreModule.forRoot(combinedReducers),
    StoreRouterConnectingModule,
    EffectsModule.forRoot([PollingEffects, ApiCallsEffect, ApplyToSolrEffects, AuthenticationEffects]),
    !environment.production ? StoreDevtoolsModule.instrument({
      maxAge: 60,

    }) : [],
    CoreModule.forRoot(), //register with the forRoot() method to import the providers globally as singletones ensure that all child NgModules have access to the same provider instances without requiring the consumer to handle the provider registration explicitly.
    NgbModule.forRoot(),
    TranslateModule.forRoot()
/*
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
*/
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    SubHeaderComponent,
    BreadcrumbComponent,
    RunningTaskStatusComponent,
    DownloadIndicatorComponent
  ],
  providers: [
    /*
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [TranslateService],
      multi: true
    }, */
    AppPreloadingStrategy,
    AppConfig,
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {

  }
}

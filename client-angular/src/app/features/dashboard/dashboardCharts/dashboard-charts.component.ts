import {Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {ChartService} from "@app/common/charts/services/chart.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {BaseDashboard} from "@app/features/dashboard/base/BaseDashboard";
import {
  ChartInfo,
  DashboardInfo, FixedHistogramBeforeTransform,
  FixedHistogramDataDto,
  GridsterHash
} from "@app/features/dashboard/types/dashboard-interfaces";
import {DisplayGrid, GridsterConfig, GridType} from "angular-gridster2";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {UrlParamsService} from "@services/urlParams.service";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import * as _ from "lodash";
import {ActivatedRoute} from "@angular/router";
import {forkJoin} from "rxjs/index";
import {SavedFilterDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {DaSelectInputComponent} from "@shared-ui/components/da-select-input/daSelectInput.component";
import {ChartDisplayType} from "@app/features/dashboard/types/dashboard-enums";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {AddEditDashboardDialogComponent} from "@app/common/dashboard/popups/addEditDashboardDialog/addEditDashboardDialog.component";
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {
  DashboardQueryParams,
  DashboardRouteManager
} from "@app/features/dashboard/dashboard.route.manager";
import {RoutingService} from "@services/routing.service";
import {LoginService} from "@services/login.service";
import {DtoUserWrapper} from "@users/model/user.model";
import {AddPublicWidgetDialogComponent} from "@app/features/dashboard/popups/addPublicWidgetDialog/addPublicWidgetDialog.component";
import {SubHeaderService} from "@services/subHeader.service";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {CommonConfig, ServerUrls} from "@app/common/configuration/common-config";

@Component({
  selector: 'dashboard-charts',
  templateUrl: './dashboard-charts.component.html',
  styleUrls: ['./dashboard-charts.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardChartsComponent extends BaseDashboard{
  widgetItemsMap:any = {};
  widgetItemKeys:String[] = [];
  public filterList:SavedFilterDto[] = [];
  selectedFilter:any = DaSelectInputComponent.NONE_VALUE_ITEM;
  gridOptions: GridsterConfig = null;
  isBigScreen: boolean = false;
  loading:boolean = false;
  dropDownItems: DropDownConfigDto[]  = null ;
  itemWasInit:boolean = false;
  dashboardItem:UserViewDataDto = null;
  currUser:DtoUserWrapper = null;
  currUserIsDashboardOwner = false;

  onResize = ()=>  {
    if (this.itemWasInit) {
      this.updateGridOptions();
    }
  }

  constructor(private chartService: ChartService,
              private urlAuthorizationService: UrlAuthorizationService,
              private subHeaderService: SubHeaderService,
              private routingService: RoutingService,
              private dashboardRouteManager: DashboardRouteManager,
              private dashboardService: DashboardService,
              private loginService:LoginService,
              private dialogService: DialogService,
              private urlParamsService: UrlParamsService,
              private savedUserViewService: SavedUserViewService,
              private activatedRoute: ActivatedRoute = null) {
    super({}, chartService, "DASHBOARD_CHARTS");
    this.currUser = this.loginService.getCurrentUser();
  }

  updateLoader = (showLoader:boolean)=> {
    if (!showLoader) {
        setTimeout(()=>{
          this.loading = false;
        },100);
    }
    else {
      this.loading = showLoader;
    }
  }

  savePosition = ()=>{
    let dInfo:DashboardInfo = {
      widgetLocations :  this.gridItems
    }
    this.dashboardItem.displayData = JSON.stringify(dInfo);
    this.savedUserViewService.editSavedUserViewData(this.dashboardItem).subscribe((updateDashboard: UserViewDataDto) => {
    });
    this.updateDropDownItems();
  }

  resetPosition = ()=>{
    this.gridItems = {};
    this.widgetItemKeys = [];
    setTimeout(() => {
      let newLocationsMap: GridsterHash = (<DashboardInfo>JSON.parse(this.dashboardItem.displayData)).widgetLocations;
      this.updatePreviousGridItems(newLocationsMap);
      this.setWidgetsPositions(newLocationsMap);
    }, 100);
  }


  arrangeWidgets = ()=>  {
    this.gridItems = {};
    this.widgetItemKeys = [];
    setTimeout(() => {
      this.setWidgetsPositions({});
      this.updatePreviousGridItems(this.gridItems);
    }, 100);
  }


  onWidgetStatusChanged() {
    this.gridsterOptionsChanged();
    this.updateItemsStatus();
  }

  onWidgetListChanged() {
    this.gridItems = {};
    this.widgetItemsMap = {};
    this.loadData();
  }


  convertToSavedFilterItem(userViewData:UserViewDataDto):SavedFilterDto {
    let ans: SavedFilterDto = userViewData.filter;
    ans.name = userViewData.name;
    ans.filterDescriptor.name = ans.name;
    return ans;
  }

  setWidgetsPositions = (prevGridItemsHash : GridsterHash)=>  {
    let newGridItems: GridsterHash = {};
    let xLoc = 0 ;
    const widgetItemsKeys = Object.keys(this.widgetItemsMap);
    widgetItemsKeys.forEach((key:string) => {
      let widgetItem: UserViewDataDto = this.widgetItemsMap[key];
      let cInfo:ChartInfo<any, any> = this.dashboardService.convertToChartInfoItem(widgetItem);
      this.widgetItemsMap["widget_" + widgetItem.id] = widgetItem;
      if (prevGridItemsHash && prevGridItemsHash["widget_" + widgetItem.id] != null) {
        newGridItems["widget_" + widgetItem.id] = prevGridItemsHash["widget_" + widgetItem.id];
      }
      else {
        if (cInfo.displayTypes && cInfo.displayTypes[0] == ChartDisplayType.SINGLE_VALUE) {
          newGridItems["widget_" + widgetItem.id] = {
            cols: this.isBigScreen ? 3 : 4,
            rows: 2,
            x: 0,
            y: 0,
            minItemRows : 2,
            minItemCols : 3
          };
        }
        else if (cInfo.displayTypes && cInfo.displayTypes[0] == ChartDisplayType.GAUGE) {
          newGridItems["widget_" + widgetItem.id] = {
            cols: this.isBigScreen ? 3 : 4,
            rows: 4,
            x: 0,
            y: 0,
            minItemRows : 4,
            minItemCols : 3
          };
        }
        else {
          newGridItems["widget_" + widgetItem.id] = {
            cols: this.isBigScreen ? 6 : 9,
            rows: 6,
            x: xLoc,
            y: 0,
            minItemRows : 3,
            minItemCols : 3
          };
          xLoc = xLoc + (this.isBigScreen ? 6 : 9);
          if (xLoc == 18) {
            xLoc = 0 ;
          }
        }
      }
    });
    this.gridItems = newGridItems;
    this.widgetItemKeys = Object.keys(newGridItems);
  }

  loadData = ()=> {
    let queriesArray: any[] = [];
    const params: any = _.cloneDeep(this.dashboardRouteManager.getLatestQueryParams()) || {};
    queriesArray.push(this.savedUserViewService.getSavedUserViewData(params, EFilterDisplayType.DASHBOARD_WIDGET, false, params[DashboardQueryParams.DASHBOARD_ID]));
    queriesArray.push(this.savedUserViewService.getSavedUserViewData(params, EFilterDisplayType.DISCOVER, true));
    queriesArray.push(this.savedUserViewService.getSavedUserViewDataById(params, params[DashboardQueryParams.DASHBOARD_ID]));
    forkJoin(queriesArray).subscribe(([widgetItems, filterUserViewDataList, currDashboardItem]) => {
      this.dashboardItem = currDashboardItem;
      this.setSessionKey(this.currUser.username+"_"+this.dashboardItem.id);
      this.filterList = filterUserViewDataList.map(c => this.convertToSavedFilterItem(c));
      let prevGridItemsHash: GridsterHash = this.getPreviousGridItems();
      if (!prevGridItemsHash && this.dashboardItem.displayData) {
        let newLocationsMap: GridsterHash = (<DashboardInfo>JSON.parse(this.dashboardItem.displayData)).widgetLocations;
        prevGridItemsHash = newLocationsMap;
      }
      this.widgetItemsMap = {};
      widgetItems.forEach((widgetItem: UserViewDataDto) => {
        this.widgetItemsMap["widget_" + widgetItem.id] = widgetItem;
      });
      this.setWidgetsPositions(prevGridItemsHash);
      this.updateDropDownItems();
    }, (err) => {
      //this.dialogService.showAlert('Error', 'Sorry, could not get dashboard.', AlertType.Error);
      this.routingService.navigate(ServerUrls.DASHBOARD_FORWARDER,{});
    });
  }

  setInitialParameters(){
    const params: any = _.cloneDeep(this.dashboardRouteManager.getLatestQueryParams()) || {};
    this.selectedFilter = params[DashboardQueryParams.SELECTED_FILTER] ? JSON.parse(params[DashboardQueryParams.SELECTED_FILTER]) : DaSelectInputComponent.NONE_VALUE_ITEM;
  }

  daOnInit() {
    this.subHeaderService.setSubHeader('Custom analytics');
    this.itemWasInit = true;
    this.setInitialParameters();
    this.updateGridOptions();
    this.loadData();
    this.on(this.dashboardRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.setInitialParameters();
        this.loadData();
      }
    }));
  }

  updateDropDownItems(){
    let isSupportMngUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
    let dashboardInfo:DashboardInfo = (<DashboardInfo>JSON.parse(this.dashboardItem.displayData));
    this.currUserIsDashboardOwner =  this.dashboardItem.owner && (this.currUser.username == this.dashboardItem.owner.username);
    this.dropDownItems = [{
        title: "Add new widget",
        iconClass: "fa fa-plus",
        disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData,
        actionFunc: this.createNewChart
      },
      {
        title: "Choose from widgets library",
        iconClass: "fa fa-th-large",
        disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData,
        actionFunc: this.addWidgetFromList
      },
      {},
      {
        title: "Dashboard",
        children: [
          {
            title: "Edit",
            iconClass: "fa fa-pencil",
            disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData,
            actionFunc: this.editDashboard
          },
          {
            title: "Delete",
            iconClass: "fa fa fa-trash-o",
            disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData,
            actionFunc: this.removeDashboard
          },
          {},
          {
            title: "More settings",
            iconClass: "fa fa fa-cogs",
            href: ServerUrls.DASHBOARDS_SETTINGS_PAGE,
            hrefParams: {
              selectedItemId: this.dashboardItem.id
            }
          }
        ]
      },
      {
        title: "Layout",
        disabled: this.widgetItemKeys.length == 0 ,
        children : [{
          title: "Auto arrange layout",
          iconClass: "fa fa-sort-amount-asc",
          disabled: this.widgetItemKeys.length == 0 ,
          actionFunc: this.arrangeWidgets
          },
          {
            title: "Save as default layout",
            iconClass: "fa fa-floppy-o",
            disabled: !this.currUserIsDashboardOwner && !isSupportMngUserViewData|| this.widgetItemKeys.length == 0,
            actionFunc: this.savePosition
          },
          {
            title: "Restore default layout",
            iconClass: "fa fa-undo",
            disabled: this.widgetItemKeys.length == 0 || !dashboardInfo || !dashboardInfo.widgetLocations ,
            actionFunc: this.resetPosition
          }]
      },
      {},
      {
        title: "Create new dashboard",
        iconClass: "fa fa-television",
        actionFunc: this.addNewDashboard
      }];

  }


  refreshCharts = ()=> {
    this.dashboardService.refreshAllDasgboardWidgets();
  }

  editDashboard = ()=>{
    let data = { data: {
        dashboard : this.dashboardItem
      }};
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditDashboardDialogComponent, data, modelParams).result.then((updateDashboard:UserViewDataDto) => {
      if (updateDashboard != null) {
        this.dashboardItem = updateDashboard;
      }
    },(reason) => {});
  }

  removeDashboard = ()=> {
    this.dialogService.openConfirm('Confirmation', "Are you sure you want to delete this dashboard '"+ this.dashboardItem.name +"', with all its widgets  ?").then(result => {
      if (result) {
        this.savedUserViewService.deleteSavedUserViewData(this.dashboardItem.id).subscribe(()=>{
          this.routingService.navigate('/dashboardForwarder',{});
        });
      }
    });
  }


  addNewDashboard = ()=>{
    let data = { data: {
      }};
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditDashboardDialogComponent, data, modelParams).result.then((newDashboard:UserViewDataDto) => {
      if (newDashboard != null) {
        this.routingService.updateParam(this.activatedRoute, DashboardQueryParams.DASHBOARD_ID, newDashboard.id);
      }
    },(reason) => {});
  }

  addWidgetFromList = ()=> {
    const params: any = _.cloneDeep(this.dashboardRouteManager.getLatestQueryParams()) || {};
    let data = { data: {
        containerId : Number(params[DashboardQueryParams.DASHBOARD_ID])
      }};
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddPublicWidgetDialogComponent, data, modelParams).result.then((newWidget) => {
      this.addWidgetToScreen(newWidget);
    },(reason) => {});
  }


  addWidgetToScreen = (newWidget:UserViewDataDto)=> {
    let cInfo:ChartInfo<FixedHistogramDataDto,FixedHistogramBeforeTransform>  = JSON.parse(newWidget.displayData);
    if (cInfo.displayTypes && cInfo.displayTypes[0] == ChartDisplayType.SINGLE_VALUE) {
      this.gridItems["widget_" + newWidget.id] =
        {
          cols: this.isBigScreen ? 3 : 4,
          rows: 2,
          x: 0,
          y: 0,
          minItemRows : 2,
          minItemCols : 3
        };
    }
    else if (cInfo.displayTypes && cInfo.displayTypes[0] == ChartDisplayType.GAUGE) {
      this.gridItems["widget_" + newWidget.id] =
        {
          cols: this.isBigScreen ? 3 : 4,
          rows: 4,
          x: 0,
          y: 0,
          minItemRows : 4,
          minItemCols : 3
        };
    }
    else {
      this.gridItems["widget_" + newWidget.id] =
        {
          cols: this.isBigScreen ? 6 : 9,
          rows: 6,
          x: 0,
          y: 0,
          minItemRows : 3,
          minItemCols : 3
        };
    }
    this.widgetItemKeys.push("widget_" + newWidget.id);
    this.widgetItemsMap["widget_" + newWidget.id] = newWidget;
  }


  afterAddWidget = (newWidget)=> {
    this.addWidgetToScreen(newWidget);
  }


  afterRemoveWidget = (widgetId)=> {
    this.widgetItemKeys.splice(this.widgetItemKeys.indexOf("widget_" + widgetId), 1);
    delete this.gridItems["widget_" + widgetId];
    delete this.widgetItemsMap["widget_" + widgetId];
  }

  createNewChart = ()=>  {
    const params: any = _.cloneDeep(this.dashboardRouteManager.getLatestQueryParams()) || {};
    let data = { data: {
        filterList : this.filterList,
        containerId : Number(params[DashboardQueryParams.DASHBOARD_ID])
      }};
    let modelParams = {
      windowClass: 'modal-xl-window'
    };
    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams).result.then((createdWidget) => {
      this.addWidgetToScreen(createdWidget);
    },(reason) => {});
  }

  onMainFilterChanged = (filterObject:any) => {
    const params: any = this.dashboardRouteManager.getLatestQueryParams() || {};
    this.routingService.updateQueryParamsWithoutReloading(params,
      DashboardQueryParams.SELECTED_FILTER, filterObject ? JSON.stringify(filterObject) : null);
    this.selectedFilter = filterObject;
    setTimeout(() => {
      this.dashboardService.refreshAllDasgboardWidgets();
    });
  }

  updateGridOptions() {
    let currWidth = window.innerWidth;
    let columnWidth = Number((currWidth -20 ) / 18-10);
    this.isBigScreen =  columnWidth > 75;
    this.gridOptions  = {
      gridType: GridType.Fixed,
      displayGrid: DisplayGrid.None,
      fixedColWidth: columnWidth,
      fixedRowHeight: 50,
      maxCols: 18 ,
      itemResizeCallback: this.itemResize,
      itemChangeCallback : this.itemResize,
      pushItems: true,
      scrollToNewItems : true,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: true
      }
    };
  }


}

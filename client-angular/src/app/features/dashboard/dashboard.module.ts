import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {TranslationModule} from "@app/common/translation/translation.module";
import {CommonChartsModule} from "@app/common/charts/common-charts.module";
import { GridsterModule } from 'angular-gridster2';
import {DashboardComponent} from "@app/features/dashboard/dashboard.component";
import {dashboardRoutes} from "@app/features/dashboard/dashboard-routing.module";
import {SystemDashboardComponent} from "@app/features/dashboard/systemDashboard/system-dashboard.component";
import {DashboardRouteManager} from "@app/features/dashboard/dashboard.route.manager";
import {ListReportComponent} from "@app/features/dashboard/components/listReport/list-report.component";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {DashboardChart} from "@app/features/dashboard/components/dashboardChart/dashboard-chart";
import {ItDashboardComponent} from "@app/features/dashboard/itDashboard/it-dashboard.component";
import {ItemReportComponent} from "@app/features/dashboard/components/itemReport/item-report.component";
import {MultiListReportComponent} from "@app/features/dashboard/components/multiListReport/multi-list-report.component";
import {UserDashboardComponent} from "@app/features/dashboard/userDashboard/user-dashboard.component";
import {RootFolderListComponent} from "@app/features/dashboard/components/rootFoldersList/root-folder-list.component";
import {CommonDiscoverModule} from "@app/common/discover/common-discover.module";
import {DatePipe} from '@angular/common';
import {DashboardItem} from "@app/features/dashboard/components/dashboardItem/dashboardItem";
import {TranslateModule} from "@ngx-translate/core";
import {TranslationGuard} from "@app/share-services/translation.guard";
import {DashboardChartsComponent} from "@app/features/dashboard/dashboardCharts/dashboard-charts.component";
import {SavedUserViewModule} from "@app/common/saved-user-view/saved-user-view.module";
import {DashboardWidgetPagerComponent} from "@app/common/dashboard/dashboardWidgetPager/dashboard-widget-pager.component";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {AddPublicWidgetDialogComponent} from "@app/features/dashboard/popups/addPublicWidgetDialog/addPublicWidgetDialog.component";
import {CommonDashboardModule} from "@app/common/dashboard/common-dashboard.module";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {AddEditDashboardDialogComponent} from "@app/common/dashboard/popups/addEditDashboardDialog/addEditDashboardDialog.component";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";


@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    SavedUserViewModule,
    HttpClientModule,
    FormsModule,
    LoaderModule,
    NgbModule,
    TranslationModule,
    RouterModule.forChild(dashboardRoutes),
    CommonChartsModule,
    GridsterModule,
    CommonDiscoverModule,
    DaDraggableModule,
    CommonDashboardModule,
    TranslateModule.forChild()
  ],
  declarations: [
    DashboardComponent,
    SystemDashboardComponent,
    UserDashboardComponent,
    ItDashboardComponent,
    ListReportComponent,
    ItemReportComponent,
    MultiListReportComponent,
    DashboardChart,
    RootFolderListComponent,
    DashboardItem,
    DashboardChartsComponent,
    AddPublicWidgetDialogComponent
  ],
  providers: [
    TranslationGuard,
    DashboardRouteManager,
    DashboardService,
    DatePipe,
    NumSuffixPipe
  ],
  entryComponents: [
    AddPublicWidgetDialogComponent,
    AddEditChartWidgetDialogComponent,
    AddEditDashboardDialogComponent
  ],
})
export class DashboardModule {
  constructor() {
  }
}


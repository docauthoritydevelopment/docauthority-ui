import {Component, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import { GridsterConfig, GridsterItem, GridType, DisplayGrid, GridsterComponentInterface }  from 'angular-gridster2';
import {ChartService} from "@app/common/charts/services/chart.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {DtoCounterReport, DtoMultiValueList} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {BaseDashboard} from "@app/features/dashboard/base/BaseDashboard";
import {DASHBOARD_TYPES} from "@app/features/dashboard/types/dashboard-enums";

@Component({
  selector: 'system-dashboard',
  templateUrl: './system-dashboard.component.html',
  styleUrls: ['./system-dashboard.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SystemDashboardComponent extends BaseDashboard{
  analyedSystemStatistics = null;
  systemProgressStatistics = null;
  scannedFilesCounter;
  analyzedFilesCounter;
  aggregatedProcessedSize;
  processedFilesCount;
  processedFilesPercentStr;
  ingestionErrorsCount;
  analysisMaxFilesCount;
  documentsIngestionErrorCount;
  analyzedFilesPercentStr;
  fileTypeHistogram;
  groupSizeHistogram: DtoMultiValueList;
  fileAccessHistogram;
  showGroupSizeHistogram : boolean = false;
  analyzedFilesFields:any[]  = [
    {
      field : 'Documents in groups' ,
      text : 'Documents in groups'
    },
    {
      field : 'Groups' ,
      text : 'Total groups'
    },
    {
      field : 'Ungrouped documents' ,
      text : 'Ungrouped documents'
    },
    {
      field : 'Non documents' ,
      text : 'Non documents'
    }
  ];


  systemProgressFields:any[]  = [
    {
      field : 'Documents with Doc Types' ,
      text : 'Documents with DocTypes'
    },
    {
      field : 'Groups with Doc Types' ,
      text : 'Groups with DocTypes'
    },
    {
      field : 'Documents with no Doc Types' ,
      text : 'Documents without DocTypes'
    },
    {
      field : 'Groups with no Doc Types' ,
      text : 'Groups without DocTypes'
    }
  ];

  ChartTypes = ChartTypes;


  constructor(private chartService : ChartService,private dashboardService : DashboardService) {
    super({
      'totalFiles' : {cols: 3, rows: 2,x :0,y : 0,minItemRows: 2},
      'documentStatistics' : {cols: 3, rows: 2,x :3,y : 0,minItemRows: 2},
      'datamapProgress' : {cols: 3, rows: 2,x :6,y : 0,minItemRows: 2},
      'documentsPerGroup' : {cols: 3, rows: 6,x :0,y : 2},
      'filesPerModification' : {cols: 3, rows: 6,x :3,y : 2},
      'scannedFiles' : {cols: 3, rows: 6,x :6,y : 2}
    }, chartService, DASHBOARD_TYPES.SYSTEM);
  }


  loadData() {
    this.dashboardService.getScanStatisticsRequest().subscribe(this.onGetScanStatisticsCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getAdminStatisticsRequest().subscribe(this.onGetAdminDocumentCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getFileTypeHistogramRequest().subscribe(this.onFileTypeHistogramCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getGroupSizeHistogramRequest().subscribe(this.onGroupSizeHistogramCompleted,this.onGetScanStatisticsError)
    this.dashboardService.getFilesAccessHistogram().subscribe(this.onGetFilesAccessHistogramCompleted,this.onGetScanStatisticsError)
  }


  daOnInit() {
    this.loadData();
  }


  onGetAdminDocumentCompleted = (counterReportDtos:DtoCounterReport[]) => {
    this.analyedSystemStatistics = this.dashboardService.parseCountList(this.analyzedFilesFields,counterReportDtos);
    this.systemProgressStatistics = this.dashboardService.parseCountList(this.systemProgressFields,counterReportDtos);;
  }


  onGetScanStatisticsCompleted= (counterReportDtos:DtoCounterReport[]) =>
  {
    this.scannedFilesCounter = this.dashboardService.getCounter(counterReportDtos, "scanned files");
    this.analyzedFilesCounter = this.dashboardService.getCounter(counterReportDtos, "analyzed files");
    this.aggregatedProcessedSize = this.dashboardService.getCounter(counterReportDtos, "aggregated processed size");
    this.processedFilesCount = this.dashboardService.getCounter(counterReportDtos, "processed files");
    this.processedFilesPercentStr = (this.scannedFilesCounter.counter>0)?("("+(Math.floor(1000*this.processedFilesCount.counter/this.scannedFilesCounter.counter)/10)+"%)"):"";
    this.ingestionErrorsCount = this.dashboardService.getCounter(counterReportDtos, "files with ingestion errors");
    this.analysisMaxFilesCount = this.dashboardService.getCounter(counterReportDtos, "documents files with content");
    this.documentsIngestionErrorCount = this.dashboardService.getCounter(counterReportDtos, "documents with errors");
    var analysisPotential:number = this.analysisMaxFilesCount.counter - this.documentsIngestionErrorCount.counter;
    this.analyzedFilesPercentStr = (analysisPotential>0)?("("+(Math.floor(1000*this.analyzedFilesCounter.counter/analysisPotential)/10)+"%)"):"";
  };

  onFileTypeHistogramCompleted= (fileTypeHistogram:DtoMultiValueList)=>
  {
    this.fileTypeHistogram = this.dashboardService.translateDtoMultiValueList(fileTypeHistogram);
  };


  onGroupSizeHistogramCompleted= (groupSizeHistogram:DtoMultiValueList) =>
  {
    this.groupSizeHistogram = this.dashboardService.translateDtoMultiValueList(groupSizeHistogram);
    this.showGroupSizeHistogram = false;
    this.groupSizeHistogram.data.forEach((dcReport : DtoCounterReport) =>{
      if (dcReport.counter > 0) {
        this.showGroupSizeHistogram = true;
      }
    });
  };

  onGetFilesAccessHistogramCompleted= (fileAccessHistogram:DtoMultiValueList) =>
  {
    this.fileAccessHistogram = this.dashboardService.translateDtoMultiValueList(fileAccessHistogram);
  };

  onGetScanStatisticsError=function(e)
  {
  };


}

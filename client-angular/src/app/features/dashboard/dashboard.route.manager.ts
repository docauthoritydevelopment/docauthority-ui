import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';


declare var _: any;

export enum DashboardRouteEvents {
  STATE_CHANGED
}

export enum DashboardQueryParams {
  PAGE_NUM = 'page',
  BASE_FILTER_FIELD = 'baseFilterField',
  BASE_FILTER_VALUE = 'baseFilterValue',
  SELECTED_FILTER = 'selectedFilter',
  DASHBOARD_ID = 'dashboardId'
}

@Injectable()
export class DashboardRouteManager {
  latestQueryParams: any = null;
  private routeChangeEvent= new Subject<DashboardRouteEvents>();
  routeChangeEvent$ = this.routeChangeEvent.asObservable();


  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.getRoute().subscribe((route) => {
      this.latestQueryParams = route.snapshot.queryParams;
      this.routeChangeEvent.next(DashboardRouteEvents.STATE_CHANGED);
    });
  }


  getLatestQueryParams() {
    return this.latestQueryParams;
  }


  getRoute() {
    return this.router.events.pipe(
      filter(e => e instanceof NavigationEnd)
      , map(() => this.activatedRoute)
      , map((route: any ) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
  }
}


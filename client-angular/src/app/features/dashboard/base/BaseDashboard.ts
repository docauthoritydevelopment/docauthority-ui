import {BaseComponent} from "@core/base/baseComponent";
import { GridsterConfig, GridsterItem, GridType, DisplayGrid, GridsterComponentInterface }  from 'angular-gridster2';
import {GridItem, GridsterHash} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartService} from "@app/common/charts/services/chart.service";
import * as _ from 'lodash';



export class BaseDashboard extends BaseComponent{
  public gridItems: any;

  constructor(private initialGridItems: GridsterHash, private myChartService:ChartService, private sessionKey: string) {
    super();
    if (Object.keys(initialGridItems).length > 0 ) {
      if (this.getPreviousGridItems() != null) {
        this.gridItems = this.getPreviousGridItems();
      }
      else {
        this.gridItems = initialGridItems;
      }
    }
  }

  setSessionKey(newKey:string) {
    this.sessionKey = newKey;
  }


  clearPreviousGridItems() {
    localStorage.removeItem('Dashboard_' + this.sessionKey);
  }

  updatePreviousGridItems(gridItems:GridsterHash)  {
    localStorage.setItem('Dashboard_' + this.sessionKey, JSON.stringify(this.gridItems));
  }


  getPreviousGridItems():GridsterHash  {
    if (localStorage.getItem('Dashboard_' + this.sessionKey) != null)
    {
      return JSON.parse(localStorage.getItem('Dashboard_' + this.sessionKey));
    }
    return null;
  }


  resetWidgets() {
    this.gridItems = _.cloneDeep(this.initialGridItems);
    this.gridOptions.api.optionsChanged();
    setTimeout(() => {
      this.gridItems = _.cloneDeep(this.initialGridItems);
      this.gridOptions.api.optionsChanged();
      setTimeout(()=> {
        this.gridOptions.api.optionsChanged();
        this.gridOptions.api.resize();
      },100);
    },500);
  }


  itemResize = (item, itemComponent) =>  {
    this.myChartService.refreshHighchartByOutsideDOM(itemComponent.el);
    this.gridItems[itemComponent.el.id] = item;
    this.gridItems = _.cloneDeep(this.gridItems);
    this.updateItemsStatus();
  }

  gridsterOptionsChanged = ()=>  {
    this.gridOptions.api.optionsChanged();
  }

  updateItemsStatus = ()=> {
    localStorage.setItem('Dashboard_'+this.sessionKey,JSON.stringify(this.gridItems));
  }

  gridOptions : GridsterConfig = {
    gridType: GridType.VerticalFixed,
    displayGrid: DisplayGrid.None,
    fixedRowHeight: 80,
    itemResizeCallback: this.itemResize,
    itemChangeCallback : this.itemResize,
    pushItems: true,
    draggable: {
      enabled: true
    },
    resizable: {
      enabled: true
    }
  };
}


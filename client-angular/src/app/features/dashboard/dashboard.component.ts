import {Component, OnInit, ViewEncapsulation,ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BaseComponent} from "@core/base/baseComponent";
import {SystemDashboardComponent} from "@app/features/dashboard/systemDashboard/system-dashboard.component";
import {RoutingService} from "@services/routing.service";
import {UtilService} from "@services/util.service";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {PageTitleService} from "@services/page-title.service";
import {DashboardRouteEvents, DashboardRouteManager} from "@app/features/dashboard/dashboard.route.manager";
import {RouteUrls} from "@app/types/routing.urls";
import {DASHBOARD_TYPES} from "@app/features/dashboard/types/dashboard-enums";
import {UserDashboardComponent} from "@app/features/dashboard/userDashboard/user-dashboard.component";
import {ItDashboardComponent} from "@app/features/dashboard/itDashboard/it-dashboard.component";
import {first} from "rxjs/internal/operators";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {dashboardTranslate} from "@app/translate/dashboard.translate";
import {TranslateService} from "@ngx-translate/core";
import {DashboardChartsComponent} from "@app/features/dashboard/dashboardCharts/dashboard-charts.component";


@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardComponent extends BaseComponent {

  @ViewChild(SystemDashboardComponent)
  systemDashboardComponent: SystemDashboardComponent;

  @ViewChild(ItDashboardComponent)
  itDashboardComponent: ItDashboardComponent;

  @ViewChild(UserDashboardComponent)
  userDashboardComponent: UserDashboardComponent;

  @ViewChild(DashboardChartsComponent)
  dashboardChartsComponent: DashboardChartsComponent;


  showUserDashboard:boolean = false;
  showSystemDashboard: boolean = false;
  showItDashboard:boolean = false;
  showChartWidgetsDashboard:boolean = false;

  constructor(private dashboardRouteManager: DashboardRouteManager,
              private urlAuthorizationService: UrlAuthorizationService,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private i18nService : I18nService,
              private translateService : TranslateService,
              private utilService: UtilService,private pageTitleService: PageTitleService) {

    super();
    this.translateService.setTranslation('en', dashboardTranslate,false);
    this.translateService.setTranslation('en', this.i18nService.getCommonTranslate(),true);
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }


  daOnInit() {
    this.on(this.dashboardRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === DashboardRouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    }));

    this.pageTitleService.setTitle('Dashboard');
    this.loadData();
  }



  loadData() {
    this.showUserDashboard = false;
    this.showSystemDashboard = false;
    this.showItDashboard = false;
    this.showChartWidgetsDashboard = false;
    let dashboardType:string = this.route.snapshot.params['dashboardType'];

    this.urlAuthorizationService.authorizeUrl(RouteUrls.DASHBOARD_SYSTEM).pipe(first()).subscribe((isSystem:boolean)=> {
      if (dashboardType &&  dashboardType==DASHBOARD_TYPES.CHARTS) {
        this.showChartWidgetsDashboard = true;
        this.loadInnerData();
      }
      else if (isSystem && (!dashboardType || dashboardType==DASHBOARD_TYPES.SYSTEM)) {
        this.showSystemDashboard = true;
        this.loadInnerData();
      }
      else {
        this.urlAuthorizationService.authorizeUrl(RouteUrls.DASHBOARD_IT).pipe(first()).subscribe((isIT:boolean)=> {
          if (isIT && (!dashboardType || dashboardType==DASHBOARD_TYPES.IT)) {
            this.showItDashboard = true;
            this.loadInnerData();
          }
          else if (!dashboardType || dashboardType==DASHBOARD_TYPES.USER){
            this.showUserDashboard = true;
            this.loadInnerData();
          }
        });
      }
    });
  }

  loadInnerData() {
    if (this.showSystemDashboard && this.systemDashboardComponent) {
      this.systemDashboardComponent.loadData();
    }
    else if (this.showItDashboard && this.itDashboardComponent) {
      this.itDashboardComponent.loadData();
    }
    else if (this.showUserDashboard && this.userDashboardComponent) {
      this.userDashboardComponent.loadData();
    }
    else if (this.showChartWidgetsDashboard && this.dashboardChartsComponent) {
      this.dashboardChartsComponent.loadData();
    }
  }


}


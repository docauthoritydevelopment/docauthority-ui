import {Component, ViewEncapsulation} from '@angular/core';
import {ChartService} from "@app/common/charts/services/chart.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  DtoCounterReport,
  DtoItemCounter,
  DtoMultiValueList
} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {BaseDashboard} from "@app/features/dashboard/base/BaseDashboard";
import {DASHBOARD_TYPES} from "@app/features/dashboard/types/dashboard-enums";

@Component({
  selector: 'it-dashboard',
  templateUrl: './it-dashboard.component.html',
  styleUrls: ['./it-dashboard.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ItDashboardComponent extends BaseDashboard{

  scanStatistics;
  scanStatisticsAcls;
  scannedFilesCounter;
  analyzedFilesCounter;
  sensitiveFilesCounter;
  sensitiveGroupsCounter;
  aggregatedProcessedSize;
  processedFilesCount;
  ingestionErrorsCount;
  documentsIngestionErrorCount;
  analysisMaxFilesCount;
  processedFilesPercentStr;
  analyzedFilesPercentStr;
  sensitiveHistogram;
  scanCriteria;
  scanConfigurationList;
  scanConfiguration;
  fileTypeHistogram;
  groupSizeHistogram;
  fileAccessHistogram;

  ChartTypes = ChartTypes;


  constructor(private chartService : ChartService,private dashboardService : DashboardService) {
    super({
      'totalFiles' : {cols: 2, rows: 2,x :0,y : 0,minItemRows: 2},
      'sensitiveFiles' : {cols: 2, rows: 2,x :2,y : 0,minItemRows: 2},
      'sensitiveStatistics' : {cols: 2, rows: 2,x :4,y : 0,minItemRows: 2},
      'scanStatistics' : {cols: 3, rows: 2,x :6,y : 0,minItemRows: 2},


      'specificScan' : {cols: 4, rows: 2,x :0,y : 2,minItemRows: 2},
      'scanConfiguration' : {cols: 4, rows: 2,x :0,y : 4,minItemRows: 2},
      'rfStatus' : {cols: 5, rows: 4,x :4,y : 2,minItemRows: 2},


      'fileCountPerGroup'  : {cols: 3, rows: 4,x :0,y : 6,minItemRows: 2},
      'fileCountPerModification' : {cols: 3, rows: 4,x :3,y : 6,minItemRows: 2},
      'scannedFileTypes' : {cols: 3, rows: 4,x :6,y : 6,minItemRows: 2}
    }, chartService, DASHBOARD_TYPES.IT);
  }


  loadData() {
    this.dashboardService.getScanStatisticsRequest().subscribe(this.onGetScanStatisticsCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getScanCriteriaRequest().subscribe(this.onGetScanCriteriaCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getScanConfigurationRequest().subscribe(this.onGetScanConfigurationCompleted,this.onGetScanStatisticsError);
    this.dashboardService.getFileTypeHistogramRequest().subscribe(this.onFileTypeHistogramCompleted,this.onGetScanStatisticsError)
    this.dashboardService.getGroupSizeHistogramRequest().subscribe(this.onGroupSizeHistogramCompleted,this.onGetScanStatisticsError)
    this.dashboardService.getFilesAccessHistogram().subscribe(this.onGetFilesAccessHistogramCompleted,this.onGetScanStatisticsError)
  }


  daOnInit() {
    this.loadData();
  }


  // $scope.scanStatisticsHTML = '<list-report report-counter-data="scanStatistics"></list-report>';

  onGetScanStatisticsCompleted=(counterReportDtos:DtoCounterReport[]) =>
  {
    this.scanStatistics = counterReportDtos;
    this.scanStatisticsAcls = [
      this.dashboardService.getCounter(counterReportDtos, "distinct owners","Distinct owners"),
      this.dashboardService.getCounter(counterReportDtos, "distinct acl read","Distinct acl read"),
      this.dashboardService.getCounter(counterReportDtos, "distinct acl write","Distinct acl write")
    ];
    this.scannedFilesCounter = this.dashboardService.getCounter(counterReportDtos, "scanned files");
    this.analyzedFilesCounter = this.dashboardService.getCounter(counterReportDtos, "analyzed files");
    this.sensitiveFilesCounter = this.dashboardService.getCounter(counterReportDtos, "sensitive data files");
    this.sensitiveGroupsCounter = this.dashboardService.getCounter(counterReportDtos, "sensitive data groups");
    this.aggregatedProcessedSize = this.dashboardService.getCounter(counterReportDtos, "aggregated processed size");
    this.processedFilesCount = this.dashboardService.getCounter(counterReportDtos, "processed files");
    this.ingestionErrorsCount = this.dashboardService.getCounter(counterReportDtos, "files with ingestion errors");
    this.documentsIngestionErrorCount =  this.dashboardService.getCounter(counterReportDtos, "documents with errors");
    this.analysisMaxFilesCount = this.dashboardService.getCounter(counterReportDtos, "documents files with content");
    var analysisPotential:number = this.analysisMaxFilesCount.counter - this.documentsIngestionErrorCount.counter;
    this.processedFilesPercentStr = (this.scannedFilesCounter.counter>0)?("("+(Math.floor(1000*this.processedFilesCount.counter/this.scannedFilesCounter.counter)/10)+"%)"):"";
    this.analyzedFilesPercentStr = (analysisPotential>0)?("("+(Math.floor(1000*this.analyzedFilesCounter.counter/analysisPotential)/10)+"%)"):"";

    var sensitiveHistogram:DtoMultiValueList = <DtoMultiValueList>{
      name: '',
      data: [],
    };

    sensitiveHistogram.data.push({
      counter:this.analyzedFilesCounter.counter  - this.sensitiveFilesCounter.counter,
      name:'Regular files',
      color: '#A0A700'
    });
    sensitiveHistogram.data.push({
      counter:this.sensitiveFilesCounter.counter,
      name:'Sensitive files',
      color: '#B52E31'
    });


    this.sensitiveHistogram= sensitiveHistogram;
  };

  onGetScanCriteriaCompleted=(scanCriterias:DtoMultiValueList) =>
  {
    this.scanCriteria = this.dashboardService.translateDtoMultiValueList(scanCriterias);
  }

  onGetScanConfigurationCompleted=(summaryReport:any) =>
  {
    this.scanConfigurationList = summaryReport.stringListReportDtos;
    this.scanConfiguration = summaryReport.stringReportDtos;
  };

  onFileTypeHistogramCompleted=(fileTypeHistogram:DtoMultiValueList)=>
  {
    this.fileTypeHistogram = this.dashboardService.translateDtoMultiValueList(fileTypeHistogram);
  };
  onGroupSizeHistogramCompleted=(groupSizeHistogram:DtoMultiValueList)=>
  {
    this.groupSizeHistogram = this.dashboardService.translateDtoMultiValueList(groupSizeHistogram);
  };

  onGetFilesAccessHistogramCompleted=(fileAccessHistogram:DtoMultiValueList) =>
  {
    this.fileAccessHistogram = this.dashboardService.translateDtoMultiValueList(fileAccessHistogram);
  };

  onGetScanStatisticsError=(e) =>
  {
  };


}

import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {BaseComponent} from "@core/base/baseComponent";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {forkJoin} from "rxjs/index";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";

@Component({
  selector: 'add-public-widget-dialog',
  templateUrl: './addPublicWidgetDialog.component.html',
  styleUrls: ['./addPublicWidgetDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddPublicWidgetDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public data:{containerId : number};
  public widgetItems:UserViewDataDto[] = null ;
  public searchWidgetTxt:string = '';
  selectedItem: UserViewDataDto = null;
  loading:boolean = false;

  constructor(private activeModal: NgbActiveModal, private savedUserViewService: SavedUserViewService, private dashboardService : DashboardService) {
    super();
  }


  daOnInit(): void {
    this.loadData();
  }

  cancel() {
    this.activeModal.close(null);
  }

  save() {
    this.loading = true;
    let saveItem: UserViewDataDto = _.cloneDeep(this.selectedItem);
    delete saveItem.icon;
    saveItem.globalFilter = false;
    saveItem.containerId = this.data.containerId;
    this.savedUserViewService.addSavedUserViewData(saveItem).subscribe((newUserViewData: UserViewDataDto)=>{
      this.activeModal.close(newUserViewData);
      this.loading = false;
    });
  }


  onSelectedChanged = (newItem:any) => {
    this.selectedItem = newItem;
  };



  loadData() {
    this.loading = true;
    let queriesArray: any[] = [];
    queriesArray.push(this.savedUserViewService.getDetachedSavedUserViewData({}, EFilterDisplayType.DASHBOARD_WIDGET));
    forkJoin(queriesArray).subscribe(([widgetItems]) => {
      widgetItems.forEach((item:UserViewDataDto)=>{
        item.icon = this.dashboardService.getIconOfUserViewDataWidget(item);
      })
      this.widgetItems = widgetItems;
      this.loading = false;
    });
  }


  ngOnDestroy() {
  }

}

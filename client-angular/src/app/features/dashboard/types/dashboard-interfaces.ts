import {
  ChartDisplayType,
  ChartType,
  ChartValueType,
  EFileState,
  EFileType, SORT_DIRECTION, VALUE_FUNCTION
} from "@app/features/dashboard/types/dashboard-enums";
import { GridsterItem}  from 'angular-gridster2';
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";
import {SavedFilterDto} from "@app/common/saved-user-view/types/saved-filters-dto";


export interface GridsterHash {
  [key: string]: GridsterItem
}

export interface ICategoryChartData
{
  name:string;
  data?:any;
}
export interface IStackedChartData {
  categories: ICategoryChartData[];
  series: ISeriesChartData[];
}

export interface ISeriesChartData
{
  name:string;
  data:any[];
}

export interface DTOSummaryReport  {
  counterReportDtos:DtoCounterReport[];
  stringReportDtos: DtoStringReport[];
  stringListReportDtos: DtoStringListReport[];
  multiValueListDto: DtoMultiValueList;
}

export interface DtoCounterReport  {
  counter:number;
  name:string;
  description?:string;
  id?:any;
  selected?: boolean;
  color?: string;
}

export interface DtoItemCounter<T>  {
  count:number;
  item:T;
  timestamp?:Date;
  description:string;
}


export interface DtoStringReport  {
  name:string;
  description:string;
  value:number;
}

export interface DtoStringListReport  {
  stringList:string[];
  name:string;
  description:string;
}

export interface DtoListReport<T>  {
  data:T[];
  name:string;
  description:string;
}

export interface DtoMultiValueList  {
  data:DtoCounterReport[];
  name:string;
  type:string;
}

export interface DtoFileTypeState {
  fileState: EFileState;
  fileType: EFileType;
}

export interface DtoCrawlFileChangeLog  {
  id:number;
  fileId:number;
  crawlEventType:string;
  fileName:string;
  baseName:string;
  rootFolderId:number;
  runId:number;
  timeStamp:number;
  processed:boolean;
}


export interface DtoProcessingError   {
  id:number;
  type:string;
  fileName:string;
  baseName:string;
  fileId:number;
  text:string;
  detailText:string;
  rootFolder:string;
  rootFolderId:number;
  folder:string;
  fileType:string;
  runId:number;
  contentId:number;
  timeStamp:number;
  duplicationsCount:number;
}


export interface TrendChartViewConfiguration
{
  totalSeriesHidden:boolean;
  otherSeriesHidden:boolean;
  totalSeriesType:string;
  otherSeriesType:string;
  viewResolutionStep:number;
  showInMenu:boolean;
}

export interface GridItem {
  cols: number,
  rows: number,
  x: number,
  y: number,
  minItemRows: number,
  minItemCols: number,
  isHidden?: boolean
}

export interface ChartInfo<T, U> {
  chartType?: ChartType;
  additionalQueryParams? : T;
  beforeTransformQueryParams? : U;
  displayTypes?: ChartDisplayType[];
  title?:string;
  xAxisTitle?:string;
  yAxisTitle?:string;
  withPagenation?:boolean;
  show3d?:boolean;
  withLabels?:boolean;
  valueType?:ChartValueType;
  colorPalete?:string;
}


export interface DashboardInfo {
  widgetLocations?: GridsterHash;
}



export interface DashboardChartSeriesDataDto {
  name: string;
  color: string;
  data: number[];
}

export interface DashboardChartPointDataDto {
  name : string;
  id : string;
  description?:string;
  parent? : string;
  color? : string;
  fullColor? : string;
  value? : number;
  directValue? : number;
  percentageValue? : number;
  colorIndex? : number;
  rfId?: number;
  isTotal?:boolean;
}

export interface DashboardChartDataDto {
  series : DashboardChartSeriesDataDto[] ;
  points: DashboardChartPointDataDto[];
  categories : string[];
  categoryIds : string[];
  totalElements : number;
  dataMayNotBeAccurate : boolean;
}

export interface SunburstdHistogramDataDto {
  valueField: string;
  valueFunction: VALUE_FUNCTION;
  valueDirection: SORT_DIRECTION;
  take: number;
  maxDepth: number;
  minDepth: number;
  page?:number;
  filterField?:string;
  groupedField?:string;
  maxChildItems? :number;
  returnOther?: boolean;
}

export interface SunburstdHistogramDataDtoBeforeTransform {
  filterField? : SavedFilterDto;
}


export interface FixedHistogramDataDto {
  withTotal:boolean;
  segmentList: string;
  groupedField: string;
  valueField: string;
  valueFunction: string;
  valueDirection: string;
  take: number;
  seriesItemsNum? : number;
  returnOther?: boolean;
  page?:number;
  periodType:string;
  datePartitionId? : number;
  queryPrefix?:string;
  filterField?:string;
  compareToTotal?:boolean;
  seriesField?:string;
  seriesFieldPrefix?:string;
}

export interface FixedHistogramBeforeTransform {
  segmentList: SavedFilterDto[];
  filterField? : SavedFilterDto;
}


export interface ChartWidgetOptionItem {
  name : string;
  id : any ;
  icon? : string;
  description? :string;
  prefix? : string;
  anotherOptionListFilter? : string[];
  onlyForSingleSeries? : boolean;
  discoverName? : string;
  axisText? : string;
  needFilters? : boolean;
  filterField? : string;
  isDatePartition? : boolean;
  isDateField? : boolean;
  type? : string;
}


export interface DashboardChartValueTypeDto {
  solrField : string;
  solrFunction? : VALUE_FUNCTION;
  sortDirection? : SORT_DIRECTION;
}




export enum DASHBOARD_TYPES {
  SYSTEM= "System",
  USER = "User",
  IT = "It",
  CHARTS = "DashboardCharts"
}

export enum VALUE_FUNCTION {
  COUNT = "count",
  SUM = "sum",
  MIN = "min",
  MAX = "max",
  AVG = "avg",
  UNIQUE = "unique",
  HLL = "hll"
}


export enum SORT_DIRECTION {
  ASC = "ASC",
  DESC = "DESC"
}
export enum ChartType {
  YEARLY_TOP_EXTENSIONS = "YEARLY_TOP_EXTENSIONS",
  MULTIPLE_FIXED_HISTOGRAM= "MULTIPLE_FIXED_HISTOGRAM",
  SINGLE_VALUE_CHART = "SINGLE_VALUE_CHART",
  SUNBURST_CHART = "SUNBURST_CHART"
}

export enum SeriesType {
  MANUALY = "MANUALY",
  BY_TOP_ITEMS= "BY_TOP_ITEMS"
}

export enum ChartDisplayType {
  LINE =  "line",
  BAR = "bar",
  COLUMN = "column",
  AREA = "area",
  GRID = "grid",
  PIE = "pie",
  STACKED_COLUMN = "stackedColumn",
  GAUGE = "gauge",
  SINGLE_VALUE = "singleValue",
  SUNBURST = "sunburst",
  TREE_MAP = "treemap"
}

export enum ChartValueType {
  FILE_SIZE = "fileSize",
  DATE = "date"
}


export enum  EFileState {
  SCANNED,
  INGESTED,
  ANALYSED,
  ERROR
}
export enum  EFileType {
  msWord= "WORD",
  pdf= "PDF",
  text= "TEXT",
  msExcel= "EXCEL",
  csv= "CSV",
  other= "OTHER",
  scannedPDF= "SCANNED_PDF",
  package= "PACKAGE"
}


export enum  EChartValueFormatType {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  NUMERIC= <any>"NUMERIC",
  PERCENTAGE= <any>"PERCENTAGE",
}

export enum  EChartDisplayType  {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  PIE= <any>"PIE",
  BAR_HORIZONTAL= <any>"BAR_HORIZONTAL",
  BAR_VERTICAL= <any>"BAR_VERTICAL",
  TREND_BAR= <any>"TREND_BAR",
  TREND_BAR_STACKED= <any>"TREND_BAR_STACKED",
  TREND_LINE= <any>"TREND_LINE",
}

export enum  ESortType  {  //In syntax, typescript doesn't allow us to create an enum with string values, but we can hack the compiler
  NAME= <any>"NAME",

}

export enum EFieldTypes {
  type_string = "string",
  type_number = "number",
  type_boolean = "boolean",
  type_date = "date",
  type_dateTime = "dateTime",
  type_other = ""
}



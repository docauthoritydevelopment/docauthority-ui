import {Component, ViewEncapsulation} from '@angular/core';
import {ChartService} from "@app/common/charts/services/chart.service";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {
  DtoCounterReport,
  DtoMultiValueList
} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {BaseDashboard} from "@app/features/dashboard/base/BaseDashboard";
import {DASHBOARD_TYPES} from "@app/features/dashboard/types/dashboard-enums";

@Component({
  selector: 'user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UserDashboardComponent extends BaseDashboard{

  fileTypeHistogram;
  fileAccessHistogram;
  userDocumentStatistics;

  ChartTypes = ChartTypes;


  constructor(private chartService : ChartService,private dashboardService : DashboardService) {
    super({
      'userDocumentStat'  : {cols: 3, rows: 5,x :0,y : 0,minItemRows: 2},
      'fileCountPerModification' : {cols: 3, rows: 5,x :3,y : 0,minItemRows: 2},
      'scannedFileTypes' : {cols: 3, rows: 5,x :6,y : 0,minItemRows: 2}
    }, chartService, DASHBOARD_TYPES.USER);
  }


  loadData() {
    this.dashboardService.getUserDocumentStatisticsRequest().subscribe(this.onGetUserDocumentStatisticsCompleted,this.onGetUserStatisticsError);
    this.dashboardService.getFileTypeHistogramRequest(true).subscribe(this.onFileTypeHistogramCompleted,this.onGetUserStatisticsError);
    this.dashboardService.getFilesAccessHistogram(true).subscribe(this.onGetFilesAccessHistogramCompleted,this.onGetUserStatisticsError);
  }


  daOnInit() {
    this.loadData();
  }


  onGetUserDocumentStatisticsCompleted=(counterReportDtos:DtoCounterReport[]) =>
  {
    this.userDocumentStatistics = counterReportDtos;
  };

  onFileTypeHistogramCompleted=(fileTypeHistogram:DtoMultiValueList) =>
  {
    this.fileTypeHistogram = this.dashboardService.translateDtoMultiValueList(fileTypeHistogram);
  };

  onGetFilesAccessHistogramCompleted=(fileAccessHistogram:DtoMultiValueList)=>
  {
    this.fileAccessHistogram = this.dashboardService.translateDtoMultiValueList(fileAccessHistogram);
  };

  onGetUserStatisticsError=(e) =>
  {
  };


}

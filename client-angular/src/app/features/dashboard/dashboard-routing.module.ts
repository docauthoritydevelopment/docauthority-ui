import {Routes} from '@angular/router';
import {DashboardComponent} from "@app/features/dashboard/dashboard.component";
import {TranslationGuard} from "@app/share-services/translation.guard";


export const dashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: ':dashboardType',
    component: DashboardComponent
  }
];

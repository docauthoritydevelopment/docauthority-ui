import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {EFieldTypes} from "@app/features/dashboard/types/dashboard-enums";
import {GridItem} from "@app/features/dashboard/types/dashboard-interfaces";


@Component({
  selector: 'item-report',
  templateUrl: './item-report.component.html',
  styleUrls: ['./item-report.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ItemReportComponent extends BaseComponent{
  _value;
  textValue;

  @Input()
  subTitle;

  @Input()
  caption;

  @Input()
  boxClass;

  @Input()
  titleValue;

  @Input()
  valueHtml;

  @Input()
  label;

  @Input()
  labelClass;

  @Input()
  fieldType;

  @Input()
  titleValueFieldType;

  @Input()
  gridItem:GridItem;


  @Input()
  subTitleHtml;

  @Input()
  set value(newValue:any ) {
    this._value = newValue;
    var val = this._value;
    if(this.fieldType ==EFieldTypes.type_dateTime)
    {
      // val =$filter('asDate')($scope.value);
    }
    if(this.fieldType ==EFieldTypes.type_string)
    {
      this.textValue = val;
    }
    else {
      this.textValue = val;
    }
  };

  get value():any {
    return this._value;
  }

  constructor() {
    super();
  }
}

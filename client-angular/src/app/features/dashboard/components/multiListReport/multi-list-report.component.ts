import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";


@Component({
  selector: 'multi-list-report',
  templateUrl: './multi-list-report.component.html',
  styleUrls: ['./multi-list-report.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MultiListReportComponent extends BaseComponent{
  @Input()
  reportData;

  constructor() {
    super();
  }
}

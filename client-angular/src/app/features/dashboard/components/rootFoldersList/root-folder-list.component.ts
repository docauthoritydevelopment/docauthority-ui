import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {DiscoverType} from "@app/common/discover/types/common-enums";
import {DiscoverQueryParams} from "@app/features/discover/discover.route.manager";
import * as _ from 'lodash';

@Component({
  selector: 'root-folder-list',
  templateUrl: './root-folder-list.component.html',
  styleUrls: ['./root-folder-list.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RootFolderListComponent extends BaseComponent{

  DiscoverType = DiscoverType;
  constructor() {
    super();
  }


  inViewParams = {
    [DiscoverQueryParams.PAGE_NUM]: 1,
    [DiscoverQueryParams.PAGE_SIZE] : 30
  }

  onPageChanged = (newPage)=>{
    let newParams = _.cloneDeep(this.inViewParams);
    newParams[DiscoverQueryParams.PAGE_NUM] =  newPage;
    this.inViewParams = newParams;
  }

}

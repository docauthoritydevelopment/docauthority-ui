import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {ChartComponent} from "@app/common/charts/chart-component/chart.component";
import {ExcelService} from "@services/excel.service";
import {DtoCounterReport, GridItem} from "@app/features/dashboard/types/dashboard-interfaces";


@Component({
  selector: 'dashboard-chart',
  templateUrl: './dashboard-chart.html',
  styleUrls: ['./dashboard-chart.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardChart extends BaseComponent{
  @ViewChild(ChartComponent)
  chartComponent: ChartComponent;

  @Input()
  chartType:ChartTypes;

  @Input()
  chartData:DtoCounterReport[];

  @Input()
  chartTitle:string;

  @Input()
  gridItem:GridItem;

  @Input()
  disabled:boolean = false;

  removeWidget = ()=>  {
    this.gridItem.isHidden = true;
  }

  chartOptions;
  isLoading:boolean = false;

  constructor(private excelService : ExcelService) {
    super();
    this.chartOptions = [{
      title: "Export as Excel",
      iconClass : "fa fa-file-excel-o",
      actionFunc : this.exportAsExcel
    },
      {
        title: "Export as Image",
        iconClass : "fa fa-image",
        actionFunc : this.exportAsImage
      },
      {
        title: "Export as PDF",
        iconClass : "fa fa-file-pdf-o",
        actionFunc : this.exportAsPdf
      },
      {
        title: "Remove widget",
        iconClass : "fa fa-trash-o",
        actionFunc : this.removeWidget
      }];
  }

  exportAsExcel  = ()=> {
    let exportObjList: any[] = [];
    if (Array.isArray(this.chartData)) {
      for (let count = 0; count < this.chartData.length; count++)
        exportObjList.push({
          ['Label']: this.chartData[count].name,
          ['Count']: this.chartData[count].counter,
          ['Details']: this.chartData[count].description
        });
    }
    this.excelService.exportAsExcelFile(exportObjList,this.chartTitle);
  }


  exportAsPdf = ()=> {
    if (this.chartComponent) {
      var fileName = this.chartTitle + ' ' + this.chartType + ' chart';
      this.chartComponent.exportAsPdf(fileName);
    }
  }

  exportAsImage = ()=> {
    if (this.chartComponent) {
      var fileName = this.chartTitle + ' ' + this.chartType + ' chart';
      this.chartComponent.exportAsImage(fileName);
    }
  }

  onMouseDown(event) {
    this.closeAllDaDropDown();
  }
}

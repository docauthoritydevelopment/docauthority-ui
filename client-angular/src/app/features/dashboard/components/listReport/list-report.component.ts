import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {GridItem} from "@app/features/dashboard/types/dashboard-interfaces";


@Component({
  selector: 'list-report',
  templateUrl: './list-report.component.html',
  styleUrls: ['./list-report.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ListReportComponent extends BaseComponent{
  @Input()
  reportCounterData;

  @Input()
  stringListReport;

  @Input()
  stringReport;

  @Input()
  countReport;

  @Input()
  countCenterReport;

  @Input()
  caption;

  @Input()
  label;

  @Input()
  labelClass;

  @Input()
  reportCounterFieldType;

  @Input()
  gridItem:GridItem;

  constructor() {
    super();
  }
}

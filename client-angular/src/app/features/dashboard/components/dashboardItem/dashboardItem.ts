import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, Input} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {SystemDashboardComponent} from "@app/features/dashboard/systemDashboard/system-dashboard.component";
import {ChartComponent} from "@app/common/charts/chart-component/chart.component";
import {ExcelService} from "@services/excel.service";
import {GridItem} from "@app/features/dashboard/types/dashboard-interfaces";
import {DaDropDownService} from "@shared-ui/components/da-drop-down/services/da-drop-down.service";


@Component({
  selector: 'dashboard-item',
  templateUrl: './dashboardItem.html',
  styleUrls: ['./dashboardItem.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardItem extends BaseComponent{

  @Input()
  chartTitle:string;

  @Input()
  isLoading:boolean = false;

  @Input()
  gridItem:GridItem;

  chartOptions = [];

  removeWidget = ()=>  {
    this.gridItem.isHidden = true;
  }

  onMouseDown(event) {
    this.closeAllDaDropDown();
  }

  constructor(private excelService : ExcelService) {
    super();
    this.chartOptions = [{
      title: "Remove widget",
      iconClass : "fa fa-trash-o",
      actionFunc : this.removeWidget
    }];
  }


}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {environment} from '../../../../../environments/environment';
import {ScanErrorDTO} from '../../types/scanError.types';
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {ScanErrorService} from "@app/features/system/services/scanError.service";

declare let $: any;

@Component({
  selector: 'scanError-item-grid',
  templateUrl: './scanError-item-grid.component.html'
})
export class ScanErrorItemGridComponent extends TreeGridInnerItemBaseComponent<ScanErrorDTO> implements OnInit {


  @ViewChild('el') el: ElementRef;

  myEnvironment = environment;


  constructor(private scanErrorService: ScanErrorService) {
    super();
  }


  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
  }


}

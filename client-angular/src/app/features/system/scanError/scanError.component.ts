import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {
  ITreeGridEvents,
  TreeGridData,
  TreeGridSortingInfo,
  TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {ActivatedRoute} from '@angular/router';
import {ScanErrorItemGridComponent} from './item-grid/scanError-item-grid.component';
import {ScanErrorGridHeaderComponent} from './grid-header/scanError-grid-header.component';
import {RouteEvents, SystemRouteManager} from '../system.route.manager';
import {ScanErrorDTO} from '../types/scanError.types';
import {ScanErrorService} from '../services/scanError.service';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {Subscription} from 'rxjs/index';
import {RoutingService} from '@services/routing.service';
import {UtilService} from '@services/util.service';
import {PageTitleService} from '@core/services/page-title.service';
import {BaseComponent} from "@core/base/baseComponent";
import {SubHeaderService} from "@services/subHeader.service";

declare let _: any;

@Component({
  selector: 'scanError-module',
  templateUrl: './scanError.component.html',
  styleUrls: ['./_scanError.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScanErrorComponent extends  BaseComponent{

  currentData: TreeGridData<ScanErrorDTO>;
  totalItems = 0;
  loading = false;
  isExporting = false;

  selectedItem: string;
  selectedItemId: any;
  currentPage: number;
  sortInfo: TreeGridSortingInfo;
  treeGridEvents: ITreeGridEvents<ScanErrorDTO>;
  breadCrumbs:any = [{title : 'Map errors'}];

  ScanErrorItemGridComponent = ScanErrorItemGridComponent;
  ScanErrorGridHeaderComponent = ScanErrorGridHeaderComponent;
  routeSubscription: Subscription;
  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;
  exportSubscription = null;
  filterMediaTypeOptions: any[];

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);
  private onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, RoutePathParams.page, pageNum);
  };
  private onSortByChanged = (sortObj: any) => {
    this.routingService.updateParamList(this.route, ['sortBy', 'sortOrder', RoutePathParams.page], [sortObj.sortBy, sortObj.sortOrder, 1]);
  }
  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;
    this.routingService.updateQueryParamsWithoutReloading(
      this.route.snapshot.queryParams, RoutePathParams.selectedItemId, this.selectedItemId);
  };

  constructor(private systemRouteManager: SystemRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private subHeaderService: SubHeaderService,
              private utilService: UtilService,
              private scanErrorService: ScanErrorService,
              private pageTitleService: PageTitleService) {
    super();
  }

  daOnDestroy() {
    this.routeSubscription.unsubscribe();
    if (this.exportSubscription) {
      this.exportSubscription.unsubscribe();
    }
  }


  daOnInit() {
    this.filterMediaTypeOptions = this.utilService.getCheckArrayOfMediaType(this.systemRouteManager.getLatestQueryParams());
    this.subHeaderService.setSubHeader("System > Map errors");
    this.routeSubscription = this.systemRouteManager.getEmitter().subscribe(eventName => {
      if (eventName === RouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    });


    this.treeGridEvents = {
      onPageChanged: this.onPageChanged,
      onItemSelected: this.onItemSelected,
      onSortByChanged: this.onSortByChanged
    };
    this.pageTitleService.setTitle('Map errors')
    const params: any = this.systemRouteManager.getLatestQueryParams();
    this.initialSortFieldName = params.sortBy ? params.sortBy : null;
    this.initialSortDirection = params.sortOrder ? params.sortOrder : null;
    this.loadData();
  }

  loadData() {
    this.showLoader();
    const params = this.systemRouteManager.getLatestQueryParams() || {};
    this.breadCrumbs = [{title : 'Map errors'}]
    if (params['rootFolderName']) {
      this.breadCrumbs = [
        {title : 'Map errors',
          clickFunc: this.removeRfFilter},
        {
          title: params['rootFolderName']
        }
      ]
    }
    this.addQuerySubscription(this.scanErrorService.load(params).subscribe((data: TreeGridData<ScanErrorDTO>) => {
      this.showLoader(false);
      this.currentData = data;
      this.totalItems = data.totalElements;
    }));
  }

  removeRfFilter = ()=> {
    this.routingService.updateParamList(
      this.route, ['rootFolderId', 'rootFolderName'],
      ['', '']);
  }

  onMediaTypeFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.route,
      [RoutePathParams.page, RoutePathParams.mediaType], [1, this.utilService.getFilterString(filterList)]);
  }


  exportData() {
    const params = this.systemRouteManager.getLatestQueryParams() || {};
    this.scanErrorService.exportData(this.totalItems,params);
  }

}


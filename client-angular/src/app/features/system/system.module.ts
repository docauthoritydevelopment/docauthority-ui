import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuditService} from './services/audit.service';
import {AuditComponent} from './audit/audit.component';
import {AuditItemGridComponent} from './audit/item-grid/audit-item-grid.component';
import {AuditGridHeaderComponent} from './audit/grid-header/audit-grid-header.component';
import {auditRoutes} from './system.routing';
import {RouterModule} from '@angular/router';
import {IngestErrorComponent} from './ingestError/ingestError.component';
import {IngestErrorItemGridComponent} from './ingestError/item-grid/ingestError-item-grid.component';
import {IngestErrorGridHeaderComponent} from './ingestError/grid-header/ingestError-grid-header.component';
import {IngestErrorService} from './services/ingestError.service';
import {ClipboardModule} from 'ng2-clipboard/ng2-clipboard';
import {SystemRouteManager} from './system.route.manager';
import {ScanErrorItemGridComponent} from './scanError/item-grid/scanError-item-grid.component';
import {ScanErrorGridHeaderComponent} from './scanError/grid-header/scanError-grid-header.component';
import {ScanErrorService} from './services/scanError.service';
import {ScanErrorComponent} from './scanError/scanError.component';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {SharedUiModule} from '@shared-ui/shared-ui.module';
import {FilterModule} from "@app/common/filters/filter.module";
import {DialogsModule} from "@app/common/dialogs/dialogs.module";
import {TranslationModule} from "@app/common/translation/translation.module";
import {TranslateModule} from "@ngx-translate/core";
import {SystemComponent} from "@app/features/system/system.component";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    LoaderModule,
    TreeGridModule,
    NgbModule,
    RouterModule.forChild(auditRoutes),
    SharedUiModule,
    ClipboardModule,
    FilterModule,
    DialogsModule,
    TranslationModule,
    TranslateModule
  ],
  declarations: [
    AuditComponent,
    SystemComponent,
    ScanErrorComponent,
    AuditItemGridComponent,
    AuditGridHeaderComponent,
    ScanErrorItemGridComponent,
    ScanErrorGridHeaderComponent,
     IngestErrorComponent,
    IngestErrorItemGridComponent,
    IngestErrorGridHeaderComponent,
   // FilterBoxComponent
  ],
  providers: [
    AuditService,
    IngestErrorService,
    SystemRouteManager,
    ScanErrorService,
  ],
  entryComponents: [
    AuditItemGridComponent,
    AuditGridHeaderComponent,
    IngestErrorItemGridComponent,
    IngestErrorGridHeaderComponent,
    ScanErrorItemGridComponent,
    ScanErrorGridHeaderComponent
  ],
})
export class SystemModule {

}


import {Routes} from '@angular/router';
import {SystemRoutePaths} from './types/system-route-pathes';
import {AuditComponent} from './audit/audit.component';
import {IngestErrorComponent} from './ingestError/ingestError.component';
import {ScanErrorComponent} from './scanError/scanError.component';
import {AuthorizationGuard} from "@core/authorization.guard";
import {SystemComponent} from "@app/features/system/system.component";

export const auditRoutes: Routes = [
  {
    path: '',
    component: SystemComponent,
    children: [
      {
        path: SystemRoutePaths.audit,
        component: AuditComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SystemRoutePaths.ingestError,
        component: IngestErrorComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SystemRoutePaths.scanError,
        component: ScanErrorComponent,
        canActivate: [AuthorizationGuard]
      },
    ]
  }
];

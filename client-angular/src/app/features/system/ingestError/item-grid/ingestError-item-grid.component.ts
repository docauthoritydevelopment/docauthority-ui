import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {IngestErrorDto} from '../../types/ingestError.types';
import {TreeGridDataItem} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {ClipboardService} from 'ng2-clipboard/ng2-clipboard';
import {environment} from '../../../../../environments/environment';
import {UtilService} from '@services/util.service';
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import * as _ from "lodash";
import {I18nService} from "@app/common/translation/services/i18n-service";
declare let $: any;

@Component({
  selector: 'ingestError-item-grid',
  templateUrl: './ingestError-item-grid.component.html'
})
export class IngestErrorItemGridComponent extends TreeGridInnerItemBaseComponent<IngestErrorDto> implements OnInit {

  canViewfiles:boolean = false;
  @ViewChild('el') el: ElementRef;

  file_download_url = '/api/files/content/{id}';
  myEnvironment = environment;
  errorType:string;

  openFileUrlTarget(dataItem: TreeGridDataItem<IngestErrorDto>) {
    if (dataItem.item.fileName && dataItem.item.fileName.toLowerCase().startsWith('http')) {
      return '_blank';
    }
    return '_self'
  }
  openFileUrl(dataItem: TreeGridDataItem<IngestErrorDto>) {
    if (dataItem.item.fileName && dataItem.item.fileName.toLowerCase().startsWith( 'http')) {
      return dataItem.item.fileName;
    }
    return this.file_download_url.replace('{id}', dataItem.item.fileId.toString())
  }
  copyFileNameToClipboard = function (dataItem: TreeGridDataItem<IngestErrorDto>) {
    this.clipboard.copy(dataItem.item.fileName);
  }

  copyRootFolderToClipboard = function (dataItem: TreeGridDataItem<IngestErrorDto>) {
    this.clipboard.copy(dataItem.item.rootFolder);
  }

  constructor(private clipboard: ClipboardService, private utilService: UtilService, private urlAuthorizationService: UrlAuthorizationService, private i18nSevice: I18nService) {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.canViewfiles = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ViewContent]);
    if (this.dataItem.item.type) {
      let displayText:string = this.dataItem.item.type.toString();
      let translateText = this.i18nSevice.translateId(displayText);
      if (translateText == displayText) {
        this.errorType = _.capitalize(this.i18nSevice.translateId(displayText).replace(new RegExp('_', 'g'), ' ').toLowerCase());
      }
      else {
        this.errorType = translateText;
      }
    }
  }



  getFileTypeClassName(fileType): string {
    return this.utilService.getClassByFileType(fileType);
  }

}

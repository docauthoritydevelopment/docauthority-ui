import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IngestErrorItemGridComponent} from './item-grid/ingestError-item-grid.component';
import {IngestErrorGridHeaderComponent} from './grid-header/ingestError-grid-header.component';
import {IngestErrorDto} from '../types/ingestError.types';
import {IngestErrorService} from '../services/ingestError.service';
import {RouteEvents, SystemRouteManager} from '../system.route.manager';
import {FileTypes} from '@core/types/file.types';
import {ITreeGridEvents, TreeGridData, TreeGridSortingInfo, TreeGridSortOrder} from '@tree-grid/types/tree-grid.type';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {RoutingService} from '@services/routing.service';
import {UtilService} from '@services/util.service';
import {PageTitleService} from '@core/services/page-title.service';
import * as _ from 'lodash';
import {Subscription} from 'rxjs/index';
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {DateRangeDialog} from "@app/common/dialogs/dateRangeDialog/date-range-dialog";
import {BaseComponent} from "@core/base/baseComponent";
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig} from "@app/common/configuration/common-config";




@Component({
  selector: 'ingestError-module',
  templateUrl: './ingestError.component.html',
  styleUrls: ['./_ingestError.scss'],
  encapsulation: ViewEncapsulation.None
})

export class IngestErrorComponent extends BaseComponent{

  currentData: TreeGridData<IngestErrorDto>;
  totalItems = 0;
  loading = false;
  isExporting = false;
  dateFrom = null;
  dateTo = null;
  exportSubscription = null;

  selectedItem: string;
  selectedItemId: any;
  currentPage: number;
  sortInfo: TreeGridSortingInfo;
  treeGridEvents: ITreeGridEvents<IngestErrorDto>;
  CommonConfig = CommonConfig;
  filterErrorTypeOptions: any[];
  filterMediaTypeOptions: any[];

  IngestErrorItemGridComponent = IngestErrorItemGridComponent;
  IngestErrorGridHeaderComponent = IngestErrorGridHeaderComponent;
  routeSubscription: Subscription;
  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;
  fileTypeList = [FileTypes.msWord, FileTypes.pdf, FileTypes.text,
    FileTypes.msExcel, FileTypes.csv,
    FileTypes.other, FileTypes.scannedPDF, FileTypes.package];
  fileTypeOptions = [];
  breadCrumbs:any = [{title : 'Ingestion errors'}];
  nowDate = new Date();
  errorTypesMap:any = null;

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);
  private onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, RoutePathParams.page, pageNum);
  };
  private onSortByChanged = (sortObj: any) => {
    this.routingService.updateParamList(this.route, ['sortBy', 'sortOrder', RoutePathParams.page], [sortObj.sortBy, sortObj.sortOrder, 1]);
  }
  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;

    this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams,
      RoutePathParams.selectedItemId, this.selectedItemId);
  };

  constructor(private systemRouteManager: SystemRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private utilService: UtilService,
              private subHeaderService: SubHeaderService,
              private ingestErrorService: IngestErrorService,
              private pageTitleService: PageTitleService) {
    super();

  }

  daOnDestroy() {
    this.routeSubscription.unsubscribe();
    if (this.exportSubscription) {
      this.exportSubscription.unsubscribe();
    }
  }

  daOnInit() {
    this.subHeaderService.setSubHeader("System > Ingestion errors");
    this.routeSubscription = this.systemRouteManager.getEmitter().subscribe(eventName => {
      if (eventName === RouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    });


    this.treeGridEvents = {
      onPageChanged: this.onPageChanged,
      onItemSelected: this.onItemSelected,
      onSortByChanged: this.onSortByChanged
    };
    this.pageTitleService.setTitle('Ingestion errors')
    const params: any = this.systemRouteManager.getLatestQueryParams();
    this.initialSortDirection = (params.sortOrder  ? params.sortOrder : null);
    this.initialSortFieldName = params.sortBy;
    this.fileTypeOptions = this.utilService.convertIntoCheckArray(
      this.systemRouteManager.getLatestQueryParams(), this.fileTypeList, 'fileType');
    this.filterMediaTypeOptions = this.utilService.getCheckArrayOfMediaType(this.systemRouteManager.getLatestQueryParams());
    this.ingestErrorService.getMetadataOptions('types').subscribe((data: any) => {
      this.errorTypesMap = data;
      this.filterErrorTypeOptions = this.utilService.convertIntoCheckArray(this.systemRouteManager.getLatestQueryParams(), data, RoutePathParams.errorType, true, "INGEST_ERR_TYPE.");
    });
    this.loadData();
  }



  loadData() {
    this.showLoader(true);

    const params = this.systemRouteManager.getLatestQueryParams() || {};
    this.dateFrom =  params['dateFrom'];
    this.dateTo = params['dateTo'];
    this.breadCrumbs = [{title : 'Ingestion errors'}]
    if (params['rootFolderName']) {
      this.breadCrumbs = [
        {title : 'Ingestion errors',
          clickFunc: this.removeRfFilter},
        {
          title: params['rootFolderName']
        }
        ]
    }

    this.ingestErrorService.load(params).subscribe((data: TreeGridData<IngestErrorDto>) => {
      this.showLoader(false);
      this.currentData = data;
      this.totalItems = data.totalElements;
      this.nowDate = new Date();
    });
  }

  clearDate() {
    this.routingService.updateParamList(this.route,
      [RoutePathParams.dateFrom, RoutePathParams.dateTo, RoutePathParams.page], ['','', 1]);
  }

    removeRfFilter = ()=>  {
    this.routingService.updateParamList(
      this.route, ['rootFolderId', 'rootFolderName'],
      ['', '']);
  }

  exportData() {
    const params = this.systemRouteManager.getLatestQueryParams() || {};
    this.ingestErrorService.exportData(this.totalItems,params);
  }

  chooseDates() {
    this.dialogService.openModalPopup(DateRangeDialog,{data : {from : this.dateFrom, to : this.dateTo}}).result.then((result : any) =>
    {
      if (result != null) {
        this.routingService.updateParamList(this.route,
          [RoutePathParams.dateFrom, RoutePathParams.dateTo, RoutePathParams.page], [result.from, result.to, 1]);
      }
    }, (reason) => {
    });
  }

  onErrorTypeFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.route,
      [RoutePathParams.page, RoutePathParams.errorType], [1, this.utilService.getFilterString(filterList)]);
  }

  onMediaTypeFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.route,
      [RoutePathParams.page, RoutePathParams.mediaType], [1, this.utilService.getFilterString(filterList)]);
  }

}


import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuditDto} from '../../types/audit.types';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import * as _ from 'lodash';
import {CommonConfig} from "@app/common/configuration/common-config";

declare let $: any;

@Component({
  selector: 'audit-item-grid',
  templateUrl: './audit-item-grid.component.html'
})
export class AuditItemGridComponent extends TreeGridInnerItemBaseComponent<AuditDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  auditType = '';
  CommonConfig = CommonConfig;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    if (this.dataItem.item.auditType) {
      this.auditType = _.startCase(this.dataItem.item.auditType.toString().replace('_', ' ').toLowerCase());
    }

  }
}

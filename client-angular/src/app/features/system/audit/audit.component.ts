import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AuditDto} from '../types/audit.types';

import {AuditService} from '../services/audit.service';
import {AuditGridHeaderComponent} from './grid-header/audit-grid-header.component';
import {AuditItemGridComponent} from './item-grid/audit-item-grid.component';
import {ActivatedRoute} from '@angular/router';
import {RouteEvents, SystemRouteManager} from '../system.route.manager';
import * as _ from 'lodash'
import {ITreeGridEvents, TreeGridData, TreeGridSortingInfo, TreeGridSortOrder} from '@tree-grid/types/tree-grid.type';
import {Subscription} from 'rxjs';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {UtilService} from '@services/util.service';
import {BaseComponent} from "@core/base/baseComponent";
import {SubHeaderService} from "@services/subHeader.service";



@Component({
  selector: 'audit-module',
  templateUrl: './audit.component.html',
  styleUrls: ['./_audit.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AuditComponent extends BaseComponent {

  currentData: TreeGridData<AuditDto>;
  totalItems = 0;
  loading = false;
  isExporting = false;

  selectedItem: string;
  selectedItemId: any;
  currentPage: number;
  sortInfo: TreeGridSortingInfo;
  treeGridEvents: ITreeGridEvents<AuditDto>;

  AuditItemGridComponent = AuditItemGridComponent;
  AuditGridHeaderComponent = AuditGridHeaderComponent;
  routeSubscription: Subscription;
  exportSubscription: Subscription;

  filterAuditTypeOptions: any[];
  filterActionOptions: any[];

  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;

  origQueryParams: any = null;
  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);
  private onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, RoutePathParams.page, pageNum);
  };
  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;

    this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams,
      RoutePathParams.selectedItemId, this.selectedItemId);
  };

  constructor(private systemRouteManager: SystemRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private subHeaderService: SubHeaderService,
              private auditService: AuditService, private pageTitleService: PageTitleService) {
    super();
  }

  daOnDestroy() {
    this.routeSubscription.unsubscribe();
    if (this.exportSubscription) {
      this.exportSubscription.unsubscribe();
    }
  }

  daOnInit() {
    this.subHeaderService.setSubHeader("System > Audit trail");
    this.routeSubscription = this.systemRouteManager.getEmitter().subscribe(eventName => {
      if (eventName === RouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    });

    this.treeGridEvents = {
      onPageChanged: this.onPageChanged,
      onItemSelected: this.onItemSelected,
      onSortByChanged: this.onSortByChanged
    };
    this.pageTitleService.setTitle('Audit');
    const params: any = this.systemRouteManager.getLatestQueryParams();
    this.initialSortFieldName = params.sortBy ? params.sortBy : null;
    this.initialSortDirection = params.sortOrder ? params.sortOrder : null;
    this.loadData();

    this.auditService.getMetadataOptions('actions').subscribe((data: string[]) => {
      this.filterActionOptions = this.utilService.convertIntoCheckArray(this.systemRouteManager.getLatestQueryParams(), data, 'action', true, 'AUDIT_ACTIONS.');
    });


    this.auditService.getMetadataOptions('types').subscribe((data: string[]) => {
      this.filterAuditTypeOptions = this.utilService.convertIntoCheckArray(this.systemRouteManager.getLatestQueryParams(), data, 'auditType', true, 'AUDIT_TYPES.');
    });
  }

  onAuditTypeFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.route,
      [RoutePathParams.page, RoutePathParams.auditType], [1, this.utilService.getFilterString(filterList)]);
  }

  onAuditActionFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.route, [RoutePathParams.page,
      RoutePathParams.action], [1, this.utilService.getFilterString(filterList)]);
  }

  loadData() {
    this.showLoader(true);
    this.utilService.updateCheckArray(this.systemRouteManager.getLatestQueryParams(), 'action', this.filterActionOptions);
    this.utilService.updateCheckArray(this.systemRouteManager.getLatestQueryParams(), 'auditType', this.filterAuditTypeOptions);
    this.filterActionOptions = _.cloneDeep(this.filterActionOptions);
    this.filterAuditTypeOptions = _.cloneDeep(this.filterAuditTypeOptions);
    const params = this.systemRouteManager.getLatestQueryParams() || {};
    this.auditService.load(params).subscribe((data: TreeGridData<AuditDto>) => {
      this.showLoader(false);
      this.currentData = data;
      this.totalItems = data.totalElements;
    });
  }

  private onSortByChanged = (sortObj: any) => {
    this.routingService.updateParamList(this.route, ['sortBy', 'sortOrder', RoutePathParams.page], [sortObj.sortBy, sortObj.sortOrder, 1]);
  }

  exportData() {
    const params: any = _.cloneDeep(this.systemRouteManager.getLatestQueryParams()) || {};
    if (params.selectedItemId) {
      delete params.selectedItemId;
    }
    this.auditService.exportData(this.totalItems, params)
  }
}

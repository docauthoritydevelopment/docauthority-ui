import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper, TreeGridData} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {ScanErrorDTO} from '../types/scanError.types';
import {DefaultPagedReqParams, UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {map, timeout} from 'rxjs/internal/operators';
import {Observable} from 'rxjs/index';
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {ServerExportService} from "@services/serverExport.service";

@Injectable()
export class ScanErrorService {

  private pipe;

  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private serverExportService : ServerExportService,
    private treeGridAdapter: TreeGridAdapterService,
    private datePipe: DatePipe,
    private excelService: ExcelService) {

    this.pipe = new DatePipe('en-US');
  }

  //---------------------------------------------------

  load(params): Observable<TreeGridData<ScanErrorDTO>> {
    let selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    return this.http.get(`/api/scan/errors?`, {params}).pipe(
      map((data: DTOPagingResults<ScanErrorDTO>) => {
        return this.treeGridAdapter.adapt(
          data,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
      }))
  }

  //---------------------------------------------------

  exportData(totalItemsNum, params) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    }
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    this.serverExportService.addExportFile('Map_errors_'+this.datePipe.transform((new Date()).getTime(),CommonConfig.DATE_FORMAT),ExportType.MAP_ERROR,params,{
      itemsNumber : totalItemsNum
    },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }


}

import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper, TreeGridData} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {IngestErrorDto} from '../types/ingestError.types';
import {UrlParamsService} from '@services/urlParams.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {map} from 'rxjs/operators';
import {ExcelService} from '@core/services/excel.service';
import {Observable} from 'rxjs/index';
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {ServerExportService} from "@services/serverExport.service";



@Injectable()
export class IngestErrorService {

  private pipe;


  extractSingleData = (item) => {
    const partialItem: any = {};
    partialItem.Name = item[IngestErrorDto.names.baseName] || '';
    partialItem.Type = item[IngestErrorDto.names.fileType] || '';
    partialItem['Root Folder'] = item[IngestErrorDto.names.rootFolder] || '';
    partialItem.Folder = item[IngestErrorDto.names.folder] || '';
    partialItem.Text = item[IngestErrorDto.names.text] || '';
    partialItem.Details = item[IngestErrorDto.names.detailText] || '';
    partialItem.RunId = item[IngestErrorDto.names.runId] || '';
    partialItem.Date = this.pipe.transform(item[IngestErrorDto.names.timeStamp], 'dd-MMM-yy HH:mm');
    return partialItem;
  };


  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private excelService: ExcelService,
    private serverExportService: ServerExportService,
    private datePipe : DatePipe) {
    this.pipe = new DatePipe('en-US');
  }

  load(params): Observable<TreeGridData<IngestErrorDto>> {
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    return this.http.get(`/api/ingest/errors?`, {params}).pipe(
      map((data: DTOPagingResults<IngestErrorDto>) => {
        return this.treeGridAdapter.adapt(
          data,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
      })
    )
  }

  getMetadataOptions(fieldName): Observable<string[]> {
    return this.http.get(`/api/ingest/error/` + fieldName, {})
      .pipe(
        map((data: string[]) => {
          return data;
        })
      )

  }



  exportData(totalItemsNum, params) {
    params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    this.serverExportService.addExportFile('Ingest_errors_'+this.datePipe.transform((new Date()).getTime(),CommonConfig.DATE_FORMAT),ExportType.INGEST_ERROR,params,{
      itemsNumber : totalItemsNum
    },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  extractData(data) {
    return _.map(data.content, (item) => {
      return this.extractSingleData(item);
    });
  };
}

import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';

import {AuditDto} from '../types/audit.types';

import {map} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
  TreeGridData,
  TreeGridSortingInfo,
  TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {ServerExportService} from "@services/serverExport.service";


@Injectable()
export class AuditService {
  private pipe;

  extractSingleData = (item) => {
    const partialItem: any = {};

    partialItem.Audit = item[AuditDto.names.auditType] || '';
    partialItem.Action = item[AuditDto.names.crudAction] || '';
    partialItem.Date = this.pipe.transform(item[AuditDto.names.timestamp], 'dd-MMM-yy HH:mm');
    partialItem.Details = item[AuditDto.names.message] || '';
    partialItem.Username = item[AuditDto.names.username] || '';

    return partialItem;
  }

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private serverExportService: ServerExportService) {

    this.pipe = new DatePipe('en-US');
  }


  getMetadataOptions(fieldName): Observable<string[]> {
    return this.http.get(`/api/audit/` + fieldName, {})
      .pipe(
        map((data: string[]) => {
          return data;
        })
      )

  }

  //

  load(params): Observable<TreeGridData<AuditDto>> {
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params);
    params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    return this.http.get(`/api/audit?`, {params})
      .pipe(
        map((data: DTOPagingResults<AuditDto>) => {
          return this.treeGridAdapter.adapt(
            data,
            selectedItemId,
            new TreeGridAdapterMapper()
          );
        }));
  }


  exportData(totalItemsNum,params: any) {
    params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    this.serverExportService.addExportFile('Audit_'+this.datePipe.transform((new Date()).getTime(),CommonConfig.DATE_FORMAT),ExportType.AUDIT,params,{
      itemsNumber : totalItemsNum
    },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  extractData(data) {
    return _.map(data.content, (item) => {
      return this.extractSingleData(item);
    });
  };
}

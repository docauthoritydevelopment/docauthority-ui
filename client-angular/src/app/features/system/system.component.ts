import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TranslateService} from "@ngx-translate/core";
import {systemTranslate} from "@app/translate/system.translate";



@Component({
  selector: 'system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SystemComponent implements OnInit {

  constructor(private i18nService: I18nService, private translateService : TranslateService) {
    this.translateService.setTranslation('en', systemTranslate,false);
    this.translateService.setTranslation('en', i18nService.getCommonTranslate(),true);
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }

  ngOnInit() {
  }
}


export class ScanErrorDTO{

  id: string;
  rootFolder: string;
  text: number;
  detailText: string;
  folder: string;
  runId: string;
  timeStamp: number;
  static names = {
    id: 'id',
    rootFolder: 'rootFolder',
    text: 'text',
    detailText: 'detailText',
    folder: 'folder',
    runId: 'runId',
    timeStamp: 'timeStamp',
    mediaType: 'mediaType'
  };
  mediaType:string;
}







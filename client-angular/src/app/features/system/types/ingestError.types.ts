export class IngestErrorDto {

  baseName: string;
  fileType: string;
  duplicationsCount: number;
  contentId: string;
  folder: string;
  text: string;
  detailText: string;
  runId: string;
  timeStamp: number;
  fileName:string;
  fileId:string;
  rootFolder:string;
  static names = {
    baseName: 'baseName',
    fileType: 'fileType',
    duplicationsCount: 'duplicationsCount',
    contentId: 'contentId',
    folder: 'folder',
    rootFolder: 'rootFolder',
    text: 'text',
    detailText: 'detailText',
    runId: 'runId',
    timeStamp: 'timeStamp',
    fileName:'fileName',
    fileId:'fileId',
    extension:'extension',
    type:'type',
    mediaType:'mediaType'
  };
  extension:string;
  type:string;
  mediaType:string;
}








export enum AUDIT_ACTION {
  CREATE,
  CREATE_ITEM,
  UPDATE,
  UPDATE_ITEM,
  DELETE,
  DELETE_ITEM,
  ASSIGN,
  UNASSIGN,
  IMPORT,
  IMPORT_ITEMS,
  IMPORT_ITEM_ATTACH,
  IMPORT_ITEM_CREATE,
  IMPORT_ITEM_UPDATE,
  START,
  STOP,
  PAUSE,
  RESUME,
  ATTACH,
  DETACH,
  VIEW,
  SUCCEEDED,
  FAILED,
  AUTO,
  VIEW_INFO,
  UNDELETE
}

export enum AUDIT_TYPE {
  ACTION_EVENT,
  GROUP,
  FILE,
  FOLDER,
  BIZ_LIST,
  ROOT_FOLDER,
  MEDIA_PROCESSOR,
  MEDIA_CONNECTION_DETAILS,
  SEARCH_PATTERN,
  SCHEDULE_GROUP,
  DOC_TYPE,
  TAG_TYPE,
  TAG,
  DATE_PARTITION,
  CUSTOMER_DATA_CENTER,
  SCAN,
  SOLR,
  USER,
  BIZ_ROLE,
  FUNCTIONAL_ROLE,
  ROLE_TEMPLATE,
  SETTINGS,
  CHART,
  TREND_CHART,
  QUERY,
  LOGIN,
  LOGOUT
}

export class AuditDto {

  auditType: AUDIT_TYPE;
  crudAction: string;
  message: string;
  objectId: number;
  objectType: string;
  eventId: number;
  onObjectType: string;
  onObjectId: string;
  params: string;
  timestamp: number;
  username : string;

  static names = {
    auditType: 'auditType',
    crudAction: 'crudAction',
    message: 'message',
    objectId: 'objectId',
    objectType: 'objectType',
    eventId: 'eventId',
    onObjectType: 'onObjectType',
    onObjectId: 'onObjectId',
    params: 'params',
    timestamp: 'timestamp',
    username : 'username'
  };
}







import * as _ from 'lodash'
import {forkJoin} from "rxjs";
import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {DateFiltersService} from "@app/features/settings/services/dateFilters.service";
import {DateFilterItemGridComponent} from "@app/features/settings/dateFilters/item-grid/date-filter-item-grid.component";
import {ITreeGridEvents, TreeGridData, TreeGridDataItem} from "@tree-grid/types/tree-grid.type";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePartitionDialogComponent} from "@app/features/settings/popups/datePartitionDialog/datePartitionDialog.component";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {DateRangePartitionDto} from "@app/common/settings/types/dateFilters.dto";
import {RegulationDto} from "@app/common/settings/types/searchPattern.dto";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ToolbarActionTypes} from "@app/types/display.type";
import {DateFilterGridHeaderComponent} from "@app/features/settings/dateFilters/grid-header/date-filter-grid-header.component";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";


@Component({
  selector: 'date-filters',
  templateUrl: './dateFilters.component.html',
  styleUrls: ['./dateFilters.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DateFiltersComponent extends BaseComponent {

  params: any = null;
  loading: boolean = false;
  dateFilterData: TreeGridData<DateRangePartitionDto> = null;
  isExporting: boolean = false;
  DataFilterItemGridComponent = DateFilterItemGridComponent;
  DateFilterGridHeaderComponent = DateFilterGridHeaderComponent;
  treeGridEvents: ITreeGridEvents<object>;
  selectedItemId: number;
  private selectedItem:DateRangePartitionDto;
  public settingsActionItems: ButtonConfigDto[];
  public totalElements: number = 0;
  public exportActionItem: ButtonConfigDto;
  public isMngScansConfigUser:boolean;

  constructor(private dateFiltersService: DateFiltersService, private routingService: RoutingService, private activatedRoute: ActivatedRoute,
              private urlAuthorizationService : UrlAuthorizationService,
              private dialogService: DialogService, private router: Router) {
    super(router, activatedRoute);
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  daOnInit() {
    this.initializeRoles();
    this.setActionItems();
    this.setTreeGridEvent();
    this.on(this.dateFiltersService.dateFiltersRefreshedEvent$, () => {
      this.loadData(true);
    });
  }

  initializeRoles() {
    this.isMngScansConfigUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngScanConfig]);
  }

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<RegulationDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }


  routeChanged(){
    this.loadData(true);
  }

  //----------------------------------------

  loadData = (withLoader: boolean = false) => {
    if (withLoader) {
      this.showLoader(true);
    }
    let params = this.getQueryParams();
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    let queriesArray = [];
    queriesArray.push(this.dateFiltersService.getDateFilterData(params, this.selectedItemId));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([dateFilter]) => {
      this.dateFilterData = dateFilter;
      if (withLoader) {
        this.showLoader(false);
      }
    }));
  };

  private setActionItems() {
    this.settingsActionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: "Add date partition",
        name: 'Add',
        actionFunc: this.addNewDatePartition,
        disabled: false,
        href: ToolbarActionTypes.ADD,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: "Edit date partition",
        name: 'Edit',
        actionFunc: this.editSelectedDatePartition,
        disabled: !this.selectedItem || this.selectedItem.system,
        href: ToolbarActionTypes.EDIT,
        order: 1,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: "Delete date partition",
        name: 'Delete',
        actionFunc: this.deleteSelectedDatePartition,
        disabled: !this.selectedItem || this.selectedItem.system,
        href: ToolbarActionTypes.DELETE,
        order: 2,
        btnType: 'link'
      },
    ];

    let exportDropDownItems:DropDownConfigDto[] = [{
      title: "As Excel",
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: "Export options",
      disabled: this.isExporting,
      children: exportDropDownItems,
      href: ToolbarActionTypes.EXPORT
    }
  }


  private onItemSelected = (itemId, item) => {
    if (!this.selectedItem || this.selectedItemId != itemId) {
      this.selectedItemId = itemId;
      this.routingService.replaceQueryParamsWithoutReloading(this.activatedRoute.snapshot.queryParams,
        RoutePathParams.selectedItemId, this.selectedItemId);
    }
    this.selectedItem = item;
    this.setActionItems();
  };

  exportData = ()=>  {
    this.isExporting = true;
    let itemsList = [];
    this.dateFilterData.items.forEach((dataItem: TreeGridDataItem<DateRangePartitionDto>)=> {
      itemsList.push(dataItem.item);
    })
    this.dateFiltersService.exportData(itemsList).subscribe(() => {
      this.isExporting = false;
    });
  }

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  private onPageChanged = (pageNum) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.page],[pageNum]);
  };


  private onSortByChanged = (sortObj: any) => {
  };


  addNewDatePartition = () => {
    this.dialogService.openModalPopup(DatePartitionDialogComponent, {data: {datePartitionDto: null}}, {size: 'md'});
  }


  getDatePartitionById(id) {
    for (let count = 0; count < this.dateFilterData.items.length; count++) {
      let dataItem: TreeGridDataItem<DateRangePartitionDto> = this.dateFilterData.items[count];
      if (dataItem.item.id == id) {
        return dataItem.item;
      }
    }
    ;
    return null;
  }

  editSelectedDatePartition = () => {
    let selectedDatePartition = _.cloneDeep(this.getDatePartitionById(this.selectedItemId));
    this.dialogService.openModalPopup(DatePartitionDialogComponent, {data: {datePartitionDto: selectedDatePartition}}, {size: 'md'});
  }


  deleteSelectedDatePartition = ()=> {
    var dlg = this.dialogService.openConfirm('Confirmation', "You are about to delete date partition named: '"+this.selectedItem.name+"'. Continue ?").then(result => {
      if (result) {
        this.dateFiltersService.deleteDatePartition(this.selectedItemId);
      }
    });
  }
}

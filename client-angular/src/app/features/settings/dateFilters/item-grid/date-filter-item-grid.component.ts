import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DateRangePartitionDto} from "@app/common/settings/types/dateFilters.dto";
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {DateRangePointType} from "@app/common/settings/types/dateFilters.enums";




@Component({
  selector: 'date-filter-item-grid',
  templateUrl: './date-filter-item-grid.component.html',
})
export class DateFilterItemGridComponent extends TreeGridInnerItemBaseComponent<DateRangePartitionDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;
  DateRangePointType = DateRangePointType;
  hasFromData:boolean = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}


import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {ScheduleConfigDto, ScheduleGroupDto} from "@app/common/scan/types/scan.dto";
import {EDayOfTheWeek, EScheduleConfigType} from "@app/common/scan/types/scanEnum.types";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {AbstractControl, FormControl, FormGroup, NgForm, ValidatorFn, Validators} from "@angular/forms";
import {ScheduleHourValidatorDirective} from "@app/features/settings/scheduleGroups/scheduleGroupValidator/scheduleHourValidator.directive"
import {ScheduleMinutesValidatorDirective} from "@app/features/settings/scheduleGroups/scheduleGroupValidator/scheduleMinutesValidator.directive"
import {TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {ScheduleGroupsService} from "@app/features/settings/scheduleGroups/scheduleGroups.service";

@Component({
  selector: 'add-edit-schedule-group-dialog',
  templateUrl: './addEditScheduleGroupDialog.component.html',
  //styleUrls: ['./addEditScheduleGroupDialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditScheduleGroupDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  scheduleGroupItem: ScheduleGroupDto = null;
  title: string = "";
  activeTab: string = "recurrence";
  public inEditMode:boolean = false;
  cronExpressionText: string = "";

  @ViewChild('scheduleGroupGeneralForm') public scheduleGroupGeneralForm: NgForm = null;
  @ViewChild('scheduleGroupRecurrenceForm') public scheduleGroupRecurrenceForm: NgForm = null;

  startingTimeHoursRange = null;
  startingTimeMinutesRange = null;

  scheduleConfigJson: string = "";

  scheduleTypeOptions = [
    EScheduleConfigType.WEEKLY,
    EScheduleConfigType.DAILY,
    EScheduleConfigType.HOURLY,
    EScheduleConfigType.CUSTOM
  ];

  daysOptions = [
    {value: EDayOfTheWeek.MON, selected: false},
    {value: EDayOfTheWeek.TUE, selected: false},
    {value: EDayOfTheWeek.WED, selected: false},
    {value: EDayOfTheWeek.THU, selected: false},
    {value: EDayOfTheWeek.FRI, selected: false},
    {value: EDayOfTheWeek.SAT, selected: false},
    {value: EDayOfTheWeek.SUN, selected: false}
  ];

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal: NgbActiveModal,private scheduleGroupsService: ScheduleGroupsService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setScheduleGroup();
    this.initTimes();
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  private setScheduleGroup() {
    if (_.has(this.data, 'scheduleGroup')) {
      this.title = "Edit schedule group";
      this.scheduleGroupItem =  (<ScheduleGroupDto>this.data)['scheduleGroup'];
      this.inEditMode = true;
      this.initDaysOfTheWeek();
    }
    else {
      this.title = "Add schedule group";
      this.scheduleGroupItem = <ScheduleGroupDto>{};
      this.scheduleGroupItem.active = true;
      this.scheduleGroupItem.name = "";
      this.scheduleGroupItem.schedulingDescription = "";
      this.scheduleGroupItem.scheduleConfigDto = <ScheduleConfigDto>{};
      this.scheduleGroupItem.scheduleConfigDto.scheduleType = EScheduleConfigType.DAILY;
      this.scheduleGroupItem.scheduleConfigDto.every = 1;
      this.scheduleGroupItem.scheduleConfigDto.hour = -1;
      this.scheduleGroupItem.scheduleConfigDto.minutes = -1;
      this.scheduleConfigJson = JSON.stringify(this.scheduleGroupItem.scheduleConfigDto);
    }
  }

  //---------------------------------------

  private initTimes() {
    let timeHoursRange = [];
    let timeMinutesRange = [];
    for (let i = 0; i < 24; i++) {
      timeHoursRange.push({value: i, display: (i < 10) ? ('0' + i) : i + ''});
    }

    for (let i = 0; i < 60; i++) {
      timeMinutesRange.push({value: i, display: (i < 10) ? ('0' + i) : i + ''});
    }
    this.startingTimeHoursRange = timeHoursRange;
    this.startingTimeMinutesRange = timeMinutesRange;
  }

  //---------------------------------------

  private initDaysOfTheWeek() {
    if (this.isWeekly() && this.scheduleGroupItem.scheduleConfigDto.daysOfTheWeek) {
      this.scheduleGroupItem.scheduleConfigDto.daysOfTheWeek.forEach(d => {
        this.daysOptions.filter(f => f.value == d)[0].selected = true;
      });
    }
  }

  private setDaysOfTheWeek() {
    if (this.isWeekly()) {
      this.scheduleGroupItem.scheduleConfigDto.daysOfTheWeek = [];
      this.daysOptions.forEach(d => {
        if (d.selected) {
          this.scheduleGroupItem.scheduleConfigDto.daysOfTheWeek.push(d.value);
        }
      });
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------

  isWeekly = () => {
    return this.scheduleGroupItem.scheduleConfigDto.scheduleType == EScheduleConfigType.WEEKLY;
  };

  isDaily = () => {
    return this.scheduleGroupItem.scheduleConfigDto.scheduleType == EScheduleConfigType.DAILY;
  };

  isHourly = () => {
    return this.scheduleGroupItem.scheduleConfigDto.scheduleType == EScheduleConfigType.HOURLY;
  };

  isCustom = () => {
    return this.scheduleGroupItem.scheduleConfigDto.scheduleType == EScheduleConfigType.CUSTOM;
  };

  //---------------------------------------
  // operations
  //---------------------------------------

  onReoccurChange = () => {
    this.scheduleGroupItem.scheduleConfigDto.every = 1;
    if (this.scheduleGroupItem.scheduleConfigDto.scheduleType == EScheduleConfigType.HOURLY) {
      this.scheduleGroupItem.scheduleConfigDto.hour = 1;
    }
    else {
      this.scheduleGroupItem.scheduleConfigDto.hour = -1;
    }

  };

  //---------------------------------------

  onEveryChange = () => {
    if (this.isHourly() && this.scheduleGroupItem.scheduleConfigDto.every > 24) {
      this.scheduleGroupItem.scheduleConfigDto.every = 24;
    }
    else if (this.isDaily() && this.scheduleGroupItem.scheduleConfigDto.every > 31) {
      this.scheduleGroupItem.scheduleConfigDto.every = 31;
    }
  };

  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.scheduleGroupItem.name && this.scheduleGroupItem.name.length > 0;
  }

  private isValidEvery() {
    return this.scheduleGroupItem.name && this.scheduleGroupItem.name.length > 0;
  }

  private isValidMinutes() {
    return this.scheduleGroupItem.scheduleConfigDto.minutes != null;
  };

  private isValidHour() {
    return this.isHourly() || (!this.isHourly() && this.scheduleGroupItem.scheduleConfigDto.hour != null);
  };

  private isValidTime() {
    return this.isValidHour() && this.isValidMinutes();
  };

  private isValidCustom() {
    // Get cron expression validation check
    this.cronExpressionText = this.scheduleGroupsService.customCronAsText(this.scheduleGroupItem.scheduleConfigDto.customCronConfig);
    const spacesRegex = /\s+/g; // remove spaces sequences

    function noCronSpecialChars(cronExp: string) {
      let lower:string = cronExp.toLowerCase();
      return (lower.indexOf("l") < 0 &&
          lower.indexOf("w") < 0 &&
          lower.indexOf("#") < 0);
    }

    return !this.isCustom() ||
        (this.cronExpressionText.length > 0 && noCronSpecialChars(this.scheduleGroupItem.scheduleConfigDto.customCronConfig) &&
            (<string>this.scheduleGroupItem.scheduleConfigDto.customCronConfig.trim()).split(spacesRegex, 10).length == 6);
  };

  isValidDayOfTheWeek = () => {
    return !this.isWeekly() || (this.isWeekly() && this.daysOptions.filter(f => f.selected)[0]);
  };

  isValid = () => {
    if (!this.scheduleGroupItem || !this.startingTimeHoursRange) {
      return true;
    }
    return this.isCustom() ? this.isValidCustom() :
        (this.isValidName() && this.isValidEvery() && this.isValidTime() && this.isValidDayOfTheWeek());
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  setActiveTab = (tabName: string) =>
  {
    this.activeTab = tabName;
  };

  cancel() {
    this.activeModal.close(null);
  }

  save = () => {
    this.setDaysOfTheWeek();
    if (this.isValid()) {

      if (this.inEditMode) {
        this.scheduleGroupsService.editScheduleGroup(this.scheduleGroupItem).subscribe((ans) => {
          this.activeModal.close(ans);
        });
      }
      else {
        this.scheduleGroupsService.addNewScheduleGroup(this.scheduleGroupItem).subscribe((ans) => {
          this.activeModal.close(ans);
        });
      }
    }
  };

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

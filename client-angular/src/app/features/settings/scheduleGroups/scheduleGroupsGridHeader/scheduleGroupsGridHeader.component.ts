import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'schedule-groups-grid-header',
  templateUrl: './scheduleGroupsGridHeader.component.html',
  styleUrls: ['./scheduleGroupsGridHeader.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ScheduleGroupsGridHeaderComponent{

  constructor() {
  }
}

import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
  TreeGridData,
  TreeGridDataItem, TreeGridSortingInfo, TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {Observable, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {ScheduleConfigDto, ScheduleGroupDto, ScheduleGroupSummaryInfoDto} from "@app/common/scan/types/scan.dto";
//import {PeriodPipe} from "@shared-ui/pipes/periodPipe.pipe";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {MetadataService} from "@services/metadata.service";
import {UtilService} from "@services/util.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ServerOperations} from "@services/server-operations";
import {ExcelService} from "@services/excel.service";
import {EScheduleConfigType} from "@app/common/scan/types/scanEnum.types";
import {IDataService} from "@core/base/IDataService";
import {TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {CommonConfig, ExportType, ServerUrls} from "@app/common/configuration/common-config";
import {RdbService} from "@services/rdb.service";
import {ServerExportService} from "@services/serverExport.service";
import {AddEditScheduleGroupDialogComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsDialog/addEditScheduleGroupDialog/addEditScheduleGroupDialog.component";
import * as cronsTrue from 'cronstrue';

@Injectable()
export class ScheduleGroupsService implements IDataService<ScheduleGroupSummaryInfoDto,ScheduleGroupDto>{


  private scheduleGroupRefreshedEvent = new Subject<ScheduleGroupDto>();
  scheduleGroupRefreshedEvent$ = this.scheduleGroupRefreshedEvent.asObservable();
  public updatedDataEvent$= this.scheduleGroupRefreshedEvent.asObservable();

  private newEvent = new Subject<ScheduleGroupDto>();
  public newDataEvent$ = this.newEvent.asObservable();

  private deleteEvent = new Subject<ScheduleGroupDto>();
  public deleteDataEvent$ = this.deleteEvent.asObservable();

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
    private rdbService: RdbService,
    //private periodPipe: PeriodPipe,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private metadataService: MetadataService,
    private utilService: UtilService,
    private dialogService: DialogService,
    private serverOperations: ServerOperations,
    private serverExportService: ServerExportService,
    private excelService: ExcelService) {
    //this.pipe = new DatePipe('en-US');
  }
  getData(params):Observable<DTOPagingResults<ScheduleGroupSummaryInfoDto>>{
    params = this.urlParamsService.fixedPagedParams(params,30);
    return this.http.get<DTOPagingResults<ScheduleGroupSummaryInfoDto>>(ServerUrls.SGROUPS_REQUEST, {params}).pipe(
      tap({
        next: () => {

        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }
  // getTreeGridScheduleGroups(params): Observable<TreeGridData<ScheduleGroupSummaryInfoDto>> {
  //   const selectedItemId = params.selectedItemId;
  //   params = this.urlParamsService.fixedPagedParams(params,30);
  //   if (!params.sort) {
  //     params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
  //   }
  //   return this.http.get(ServerUrls.SGROUPS_REQUEST, {params}).pipe(
  //     map((data: DTOPagingResults<ScheduleGroupSummaryInfoDto>) => {
  //       let ans: TreeGridData<ScheduleGroupSummaryInfoDto> = this.treeGridAdapter.adapt(
  //         data,
  //         selectedItemId,
  //         new TreeGridAdapterMapper()
  //       );
  //       ans.items.forEach((dataItem: TreeGridDataItem<any>) => {
  //         dataItem.id = dataItem.item.scheduleGroupDto.id;
  //       });
  //       return ans;
  //     }));
  // }

  addNewScheduleGroup(newScheduleGroup: ScheduleGroupDto):Observable<ScheduleGroupDto> {
      return this.http.put<ScheduleGroupDto>(ServerUrls.SCHEDULE_GROUP_ADD, newScheduleGroup).pipe(
      tap({
        next: (data:ScheduleGroupDto) => {
          this.newEvent.next(data);
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  editScheduleGroup(scheduleGroup):Observable<ScheduleGroupDto> {
    return this.http.post<ScheduleGroupDto>(this.rdbService.parseServerURL(ServerUrls.SCHEDULE_GROUP_BY_ID,{sgId : scheduleGroup.id}), scheduleGroup).pipe(
      tap({
        next: (data) => {
           this.scheduleGroupRefreshedEvent.next(data);
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
    ;
  }

  setScheduleGroupActiveState(sGroupId:number, isActive:boolean) {
    this.http.post('/api/schedule/groups/'+sGroupId+'/active', isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.scheduleGroupRefreshedEvent.next();
    });
  }

  deleteScheduleGroup(scheduleGroupId):Observable<ScheduleGroupDto> {
    return this.http.delete<ScheduleGroupDto>(this.rdbService.parseServerURL(ServerUrls.SCHEDULE_GROUP_BY_ID,{sgId : scheduleGroupId})).pipe(
      tap({
        next: (data) => {
          this.deleteEvent.next(data);
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  extractImportValidationData = (item) => {
    const partialItem: any = {};
    partialItem['Msg Type'] = item.type;
    partialItem['Line number'] = item.data.rowDto.lineNumber;
    partialItem['Msg Description'] = item.data.message;
    partialItem['NAME'] = item.data.rowDto.fieldsValues[0];
    partialItem['ACTIVE'] = item.data.rowDto.fieldsValues[1];
    partialItem['TYPE'] = item.data.rowDto.fieldsValues[2];
    partialItem['DESCRIPTION'] = item.data.rowDto.fieldsValues[3];
    partialItem['SCHEDULE_EVERY'] = item.data.rowDto.fieldsValues[4];
    partialItem['SCHEDULE_HOUR'] = item.data.rowDto.fieldsValues[5];
    partialItem['SCHEDULE_MINUTES'] = item.data.rowDto.fieldsValues[6];
    return partialItem;
  };

  validateImportFile = (formData:FormData, maxErrorRows:number) =>  {
    return this.http.post(`/api/schedule/groups/import/csv/validate?maxErrorRowsReport=${maxErrorRows}`, formData);
  };

  importData = (fileData:File, updateDuplicates:boolean) => {
    return this.http.post(`/api/schedule/groups/import/csv?updateDuplicates=${updateDuplicates}`, fileData);
  };

  getScheduleConfigSummaryText = (scheduleConfigDto: ScheduleConfigDto) =>
  {
    let pad2 = function(number) {
      return (number < 10 ? '0' : '') + number;
    };
    var scheduleTypePart = scheduleConfigDto ? this.i18nService.translateId(scheduleConfigDto.scheduleType.toString()) : '';
    if (scheduleConfigDto.scheduleType==EScheduleConfigType.CUSTOM) {
      var cronDesc = this.customCronAsText(scheduleConfigDto.customCronConfig);
      return (cronDesc.length==0?scheduleTypePart:cronDesc);
    } else {
      var scheduleTimePart = scheduleConfigDto ? ', every ' + (scheduleConfigDto.scheduleType == EScheduleConfigType.WEEKLY ? "" : scheduleConfigDto.every + ' ') + (scheduleConfigDto.scheduleType == EScheduleConfigType.DAILY ?
          (scheduleConfigDto.every == 1 ? 'day' : 'days') : scheduleConfigDto.scheduleType == EScheduleConfigType.HOURLY ? (scheduleConfigDto.every == 1 ? 'hour' : 'hours') : 'week') + ' at ' + (scheduleConfigDto.hour != null ?
          pad2(scheduleConfigDto.hour) : 'xx') + ':' + pad2(scheduleConfigDto.minutes) : '';
      var scheduleDaysOfWeekPart = scheduleConfigDto && scheduleConfigDto.daysOfTheWeek ? ' on ' + scheduleConfigDto.daysOfTheWeek.join() : '';
      return scheduleTypePart + scheduleTimePart + scheduleDaysOfWeekPart;
    }
  };

  extractScheduleGroupData = (item) => {
    const partialItem: any = {};
    partialItem['NAME'] = item.scheduleGroupDto.name ? item.scheduleGroupDto.name : '';
    partialItem['Schedule details'] = this.getScheduleConfigSummaryText(item.scheduleGroupDto.scheduleConfigDto);
    partialItem['ACTIVE'] = item.scheduleGroupDto.active;
    partialItem['TYPE'] = item.scheduleGroupDto.scheduleConfigDto.scheduleType;
    partialItem['DESCRIPTION'] = item.scheduleGroupDto.schedulingDescription;
    partialItem['SCHEDULE_EVERY'] = item.scheduleGroupDto.scheduleConfigDto.every;
    partialItem['SCHEDULE_HOUR'] = item.scheduleGroupDto.scheduleConfigDto.hour;
    partialItem['SCHEDULE_MINUTES'] = item.scheduleGroupDto.scheduleConfigDto.minutes;
    partialItem['SCHEDULE_DAYS_OF_THE_WEEK'] = item.scheduleGroupDto.scheduleConfigDto.daysOfTheWeek;
    partialItem['Root folders'] = item.scheduleGroupDto.rootFolderIds.length;
    return partialItem;
  };

  exportData(totalItemsNum:number,params ):void{
    this.serverExportService.prepareExportFile('Schedule groups',ExportType.SCHEDULED_GROUPS_SETTINGS,params,{ itemsNumber : totalItemsNum },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  exportSelectedData = (selectedScheduleGroups): Observable<any> =>  {
    let selectedItems = [];
    selectedScheduleGroups.forEach(scheduleGroup => {
      selectedItems.push(scheduleGroup.item);
    });
    return this.excelService.exportSelectedData(selectedItems, this.extractScheduleGroupData, 'schedule_groups');
  };

  public customCronAsText(exp:string) : string {
    let res = "";
    if (exp) {
      try {
        res = cronsTrue.toString(exp.trim(), {locale: "en"});
        // console.log("CRON: " + res);

      } catch (e) {
        // console.error('Failed cronstrue.toString( "' + exp.trim() + '")');
      }
    }
    return res;
  }

}

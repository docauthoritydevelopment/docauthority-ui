import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from '@angular/router';
import {DialogService} from "@app/shared-ui/components/modal-dialogs/dialog.service";
import {ScheduleGroupsService} from "@app/features/settings/scheduleGroups/scheduleGroups.service";
import {Router} from '@angular/router';
import {ScheduleGroupSummaryInfoDto} from "@app/common/scan/types/scan.dto";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {ESystemRoleName} from "@users/model/system-role-name";

@Component({
  selector: 'schedule-groups-item-grid',
  templateUrl: './scheduleGroupsItemGrid.component.html',
  styleUrls: ['./scheduleGroupsItemGrid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScheduleGroupsItemGridComponent extends TreeGridInnerItemBaseComponent<ScheduleGroupSummaryInfoDto> implements OnInit {

  @ViewChild('el') el: ElementRef;
  summaryText: string =  '';

  constructor(private routingService: RoutingService,private route: ActivatedRoute, private dialogService: DialogService,
              private scheduleGroupsService: ScheduleGroupsService, private urlAuthorizationService:UrlAuthorizationService) {
    super();
  }

  ngOnInit(): void {
    this.summaryText = this.scheduleGroupsService.getScheduleConfigSummaryText(this.dataItem.item.scheduleGroupDto.scheduleConfigDto);
  }

  setActive(isActive:boolean) {
    if(this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngScanConfig,ESystemRoleName.RunScans])) {
      this.scheduleGroupsService.setScheduleGroupActiveState(this.dataItem.item.scheduleGroupDto.id, isActive);
    }
  }

  openEditDialog() {
  }
}


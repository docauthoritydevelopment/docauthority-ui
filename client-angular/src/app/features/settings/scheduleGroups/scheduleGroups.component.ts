
import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";
import {TreeGridConfig, TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {UtilService} from '@services/util.service';
import {AddEditScheduleGroupDialogComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsDialog/addEditScheduleGroupDialog/addEditScheduleGroupDialog.component";
import {ScheduleGroupsService} from './scheduleGroups.service';
import {ScheduleGroupsGridHeaderComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsGridHeader/scheduleGroupsGridHeader.component";
import {ScheduleGroupsItemGridComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsItemGrid/scheduleGroupsItemGrid.component";

import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";

import {ScheduleGroupDto, ScheduleGroupSummaryInfoDto} from "@app/common/scan/types/scan.dto";

import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";

import {BaseGridManagementComponent} from "@core/base/baseGridManagementComponent";
import {BaseGridManagementConfig} from "@core/base/baseGridManagementConfig";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {TreeGridAdapterService} from "@tree-grid/services/tree-grid-adapter.service";
import * as _ from "lodash";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";

@Component({
  selector: 'schedule-groups',
  templateUrl: './scheduleGroups.component.html',
  styleUrls: ['./scheduleGroups.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScheduleGroupsComponent extends BaseGridManagementComponent<ScheduleGroupSummaryInfoDto,ScheduleGroupDto> {


  public scheduleGroupsGridHeaderComponent = ScheduleGroupsGridHeaderComponent;
  public scheduleGroupsItemGridComponent = ScheduleGroupsItemGridComponent;

  public isMngScanConfigUser:boolean;

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;

  treeConfig = <TreeGridConfig> {
    enableSwitchSelectionMode:false
  };



  private config =< BaseGridManagementConfig>{
    entityName: 'Schedule group',
    searchBoxPlaceHolder: 'name ',
    initialSortField: 'name',
    initialSortDir: TreeGridSortOrder.ASC
  };
  constructor(private scheduleGroupsService: ScheduleGroupsService,
              private urlAuthorizationService : UrlAuthorizationService,
              private searchPatterService: SearchPatternService,  routingService: RoutingService,  activatedRoute: ActivatedRoute, private dialogService: DialogService, router: Router,  utilService: UtilService,protected treeGridAdapter: TreeGridAdapterService) {
    super(router, activatedRoute, routingService, utilService, activatedRoute,treeGridAdapter);

    this.baseGridManagementConfig = this.config;
    this.dataService = scheduleGroupsService;
    this.on(this.scheduleGroupsService.scheduleGroupRefreshedEvent$, ()=>{
      this.loadData();
    });
  }

  protected initializeRoles() {
    this.isMngScanConfigUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngScanConfig]);
  }

  protected parseData(data:ScheduleGroupSummaryInfoDto[]){
    data.forEach(s=>{
      (<any>s).id = s.scheduleGroupDto.id; //TODO: set mapper to use configurable id field
    })
    return data;
  }
  openAddNewDialog = () => {
    let data = { data: {} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(AddEditScheduleGroupDialogComponent, data, modelParams)
      .result.then((data : any) => {

    }, (reason) => {
    });
  };

  openEditDialog = () => {
    let data = { data: { scheduleGroup: _.cloneDeep(this.selectedItem.scheduleGroupDto)} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(AddEditScheduleGroupDialogComponent, data, modelParams).result.then((data : any) => {

    },(reason) => {});
  };

  openDeleteDialog = () => {
    if (this.totalElements > 1) {
      let msg = `You are about to delete schedule group named: '${this.selectedItem.scheduleGroupDto.name}'. Continue?`;
      this.dialogService.openConfirm('Confirmation', msg).then(result => {
        if (result) {
          this.scheduleGroupsService.deleteScheduleGroup(this.selectedItem.scheduleGroupDto.id).subscribe();
        }
      });
    }
    else {
      this.dialogService.showAlert("Note", "Cannot delete the default schedule group.", AlertType.Info);
    }
  };

  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.scheduleGroupsService.importData, validateUploadFn: this.scheduleGroupsService.validateImportFile, component: "schedule groups", fileType: ".csv", extractValidateUploadDataFn: this.scheduleGroupsService.extractImportValidationData} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.loadData();
      }
    },(reason) => {});
  };




  //
  // private updateActionItems(changeCase:string) {
  //      this.settingsActionItems.find(item => item.type === ToolbarActionTypes.EDIT).disabled = !this.selectedItem;
  //     this.settingsActionItems.find(item => item.type === ToolbarActionTypes.DELETE).disabled = !this.selectedItem;
  //      this.exportActionItem.disabled = this.isExporting;
  // }


}

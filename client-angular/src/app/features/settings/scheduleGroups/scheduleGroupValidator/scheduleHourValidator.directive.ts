import * as _ from 'lodash';
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
import { Directive } from '@angular/core';
import {EScheduleConfigType} from "@app/common/scan/types/scanEnum.types";

@Directive({
  selector: '[schedule_hour_validator]',
  providers: [{provide: NG_VALIDATORS,useExisting: ScheduleHourValidatorDirective,multi: true}]
})

export class ScheduleHourValidatorDirective implements Validator {
  validate(c: FormControl) {
    if (c.root.value.scheduleGroupType != EScheduleConfigType.HOURLY && (c.value == null || c.value < 0)) {
      return {
        schedule_hour_validator: {
          valid: false
        }
      };
    }
    return null;
  }
}

import * as _ from 'lodash';
import { NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

@Directive({
  selector: '[schedule_minutes_validator]',
  providers: [{provide: NG_VALIDATORS,useExisting: ScheduleMinutesValidatorDirective,multi: true}]
})

export class ScheduleMinutesValidatorDirective implements Validator {
  validate(c: FormControl) {
    if (c.value == null  || c.value < 0) {
      return {
        schedule_minutes_validator: {
          valid: false
        }
      };
    }
    return null;
  }
}

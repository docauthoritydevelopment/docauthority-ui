import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DocTypesComponent} from './doctypes/docTypes.component';
import {settingsRoutes} from './settings.routing';
import {RouterModule} from '@angular/router';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {SharedUiModule} from '@shared-ui/shared-ui.module';
import {FilterModule} from "@app/common/filters/filter.module";
import {DialogsModule} from "@app/common/dialogs/dialogs.module";
import {SettingsComponent} from "./settings.component"
import {DocTypesService} from "@app/features/settings/doctypes/docTypes.service";
import {DocTypeItemGridComponent} from "@app/features/settings/doctypes/doc-type-item/docType-item-grid.component";
import {DdlTagItemsComponent} from "@app/features/settings/doctypes/doc-type-item/ddl-tag-items/ddl-tag-items.component";
import {TagTypesService} from "@app/features/settings/_common/tag-types/tag-types.service";
import {AddTagItemDialogComponent} from "@app/features/settings/_common/tag-item/add-tag-item-dialog/addTagItemDialog.component";
import {AddDocTypeDialogComponent} from "@app/features/settings/doctypes/add-doctype-dialog/addDocTypeDialog.component";
import {DocTypesExportService} from "@app/features/settings/doctypes/docTypes.export.service";
import {DateFiltersComponent} from "@app/features/settings/dateFilters/dateFilters.component";
import {DateFilterItemGridComponent} from "@app/features/settings/dateFilters/item-grid/date-filter-item-grid.component";
import {DateFiltersService} from "@app/features/settings/services/dateFilters.service";
import {TranslationModule} from "@app/common/translation/translation.module";
import {DatePartitionDialogComponent} from "@app/features/settings/popups/datePartitionDialog/datePartitionDialog.component";
import {DateRelativeAbsoluteSelectorComponent} from "@app/features/settings/components/dateRelativeAbsoluteSelector/dateRelativeAbsoluteSelector.component";
import {DaDatePipe} from "@shared-ui/pipes/da-date.pipe";
import {TranslateModule} from "@ngx-translate/core";
import {LogLevelsModule} from "@app/features/settings/logLevels/log-levels.module";

import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {ScheduleGroupsService} from "@app/features/settings/scheduleGroups/scheduleGroups.service";
import {ScheduleGroupsGridHeaderComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsGridHeader/scheduleGroupsGridHeader.component";
import {ScheduleGroupsItemGridComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsItemGrid/scheduleGroupsItemGrid.component";
import {ScheduleGroupsComponent} from './scheduleGroups/scheduleGroups.component';
import {AddEditScheduleGroupDialogComponent} from "@app/features/settings/scheduleGroups/scheduleGroupsDialog/addEditScheduleGroupDialog/addEditScheduleGroupDialog.component";
import {ScheduleHourValidatorDirective} from "@app/features/settings/scheduleGroups/scheduleGroupValidator/scheduleHourValidator.directive"
import {ScheduleMinutesValidatorDirective} from "@app/features/settings/scheduleGroups/scheduleGroupValidator/scheduleMinutesValidator.directive"

import {DepartmentsService} from "@app/features/settings/departments/departments.service";
import {DepartmentsGridHeaderComponent} from "@app/features/settings/departments/departmentsGridHeader/departmentsGridHeader.component";
import {DepartmentsItemGridComponent} from "@app/features/settings/departments/departmentsItemGrid/departmentsItemGrid.component";
import {DepartmentsComponent} from "@app/features/settings/departments/departments.component";
import {AddEditDepartmentDialogComponent} from "@app/features/settings/departments/departmentsDialog/addEditDepartmentDialog/addEditDepartmentDialog.component";
import {CloneDepartmentDialogComponent} from "@app/features/settings/departments/departmentsDialog/cloneDepartmentDialog/cloneDepartmentDialog.component";
import {SearchPatternAdminComponent} from "@app/features/settings/searchPatterns/searchPatternAdmin/searchPatternAdmin.component";
import {SearchPatternAdminItemGridComponent} from "@app/features/settings/searchPatterns/searchPatternAdmin/item-grid/search-pattern-admin-item-grid.component";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {SearchPatternAdminGridHeaderComponent} from "@app/features/settings/searchPatterns/searchPatternAdmin/grid-header/search-pattern-admin-grid-header.component";
import {SearchPatternComponent} from "@app/features/settings/searchPatterns/searchPattern.component";
import {PatternCategoriesComponent} from "@app/features/settings/patternCategories/patternCategories.component";
import {PatternCategoriesItemGridComponent} from "@app/features/settings/patternCategories/item-grid/pattern-categories-item-grid.component";
import {RegulationsItemGridComponent} from "@app/features/settings/regulations/item-grid/regulations-item-grid.component";
import {RegulationsComponent} from "@app/features/settings/regulations/regulations.component";
import {AddEditSearchPatternDialogComponent} from "@app/features/settings/popups/addEditSearchPatternDialog/addEditSearchPatternDialog.component";
import {AddEditPatternCategoryDialogComponent} from "@app/features/settings/popups/addEditPatternCategoryDialog/addEditPatternCategoryDialog.component";
import {InnerPatternsListComponent} from "@app/features/settings/components/innerPatternsList/inner-patterns-list.component";
import {AddEditRegulationDialogComponent} from "@app/features/settings/popups/addEditRegulationDialog/addEditRegulationDialog.component";
import {PatternCategoriesGridHeaderComponent} from "@app/features/settings/patternCategories/grid-header/pattern-categories-grid-header.component";
import {RegulationGridHeaderComponent} from "@app/features/settings/regulations/grid-header/regulation-grid-header.component";
import {DateFilterGridHeaderComponent} from "@app/features/settings/dateFilters/grid-header/date-filter-grid-header.component";
import {DocTypesGridHeaderComponent} from "@app/features/settings/doctypes/docTypeGridHeader/docTypesGridHeader.component";
import {CloneMatterDialogComponent} from "@app/features/settings/matters/mattersDialog/cloneMatterDialog/cloneMatterDialog.component";
import {MattersGridHeaderComponent} from "@app/features/settings/matters/mattersGridHeader/mattersGridHeader.component";
import {MattersItemGridComponent} from "@app/features/settings/matters/mattersItemGrid/mattersItemGrid.component";
import {AddEditMatterDialogComponent} from "@app/features/settings/matters/mattersDialog/addEditMatterDialog/addEditMatterDialog.component";
import {MattersComponent} from "@app/features/settings/matters/matters.component";
import {MattersService} from "@app/features/settings/matters/matters.service";
import {SavedUserViewEditorDialogComponent} from "@app/features/settings/savedUserView/savedUserViewDialog/savedUserViewEditorDialog/savedUserViewEditorDialog.component";
import {SavedUserViewGridHeaderComponent} from "@app/features/settings/savedUserView/savedUserViewGridHeader/savedUserViewGridHeader.component";
import {SavedUserViewItemGridComponent} from "@app/features/settings/savedUserView/savedUserViewItemGrid/savedUserViewItemGrid.component";
import {SavedFiltersComponent} from "@app/features/settings/savedUserView/savedFilters/savedFilters.component";
import {SavedChartsComponent} from "@app/features/settings/savedUserView/savedCharts/savedCharts.component";
import {SavedWidgetsComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgets.component";
import {SavedViewsComponent} from "@app/features/settings/savedUserView/savedViews/savedViews.component";
import {SavedDashboardsComponent} from "@app/features/settings/savedUserView/savedDashboards/savedDashboards.component";
import {CommonDashboardModule} from "@app/common/dashboard/common-dashboard.module";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {SavedWidgetGridHeaderComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgetGridHeader/savedWidgetGridHeader.component";
import {SavedWidgetItemGridComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgetItemGrid/savedWidgetItemGrid.component";
import {ViewChartWidgetDialogComponent} from "@app/common/dashboard/popups/viewChartWidgetDialog/viewChartWidgetDialog.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LoaderModule,
    TreeGridModule,
    NgbModule,
    RouterModule.forChild(settingsRoutes),
    SharedUiModule,
    FilterModule,
    DialogsModule,
    TranslationModule,
    TranslateModule,
    DaDraggableModule,
    LogLevelsModule,
    CommonDashboardModule
  ],
  declarations: [
    DocTypesComponent,
    AddDocTypeDialogComponent,
    SettingsComponent,
    DocTypeItemGridComponent,
    DdlTagItemsComponent,
    AddTagItemDialogComponent,
    DateFiltersComponent,
    DateFilterItemGridComponent,
    DatePartitionDialogComponent,
    DateRelativeAbsoluteSelectorComponent,
    ScheduleGroupsComponent,
    AddEditScheduleGroupDialogComponent,
    ScheduleGroupsGridHeaderComponent,
    ScheduleGroupsItemGridComponent,
    ScheduleHourValidatorDirective,
    ScheduleMinutesValidatorDirective,
    DepartmentsGridHeaderComponent,
    DepartmentsItemGridComponent,
    DepartmentsComponent,
    AddEditDepartmentDialogComponent,
    CloneDepartmentDialogComponent,
    MattersGridHeaderComponent,
    MattersItemGridComponent,
    MattersComponent,
    AddEditMatterDialogComponent,
    CloneMatterDialogComponent,
    SearchPatternAdminComponent,
    SearchPatternAdminItemGridComponent,
    SearchPatternAdminGridHeaderComponent,
    SearchPatternComponent,
    PatternCategoriesComponent,
    PatternCategoriesItemGridComponent,
    RegulationsItemGridComponent,
    RegulationsComponent,
    AddEditSearchPatternDialogComponent,
    AddEditPatternCategoryDialogComponent,
    InnerPatternsListComponent,
    AddEditRegulationDialogComponent,
    PatternCategoriesGridHeaderComponent,
    RegulationGridHeaderComponent,
    DateFilterGridHeaderComponent,
    DocTypesGridHeaderComponent,
    SavedFiltersComponent,
    SavedViewsComponent,
    SavedChartsComponent,
    SavedDashboardsComponent,
    SavedWidgetsComponent,
    SavedUserViewGridHeaderComponent,
    SavedUserViewItemGridComponent,
    SavedUserViewEditorDialogComponent,
    SavedWidgetGridHeaderComponent,
    SavedWidgetItemGridComponent
  ],
  providers: [
    DocTypesService,
    DocTypesExportService,
    TagTypesService,
    DateFiltersService,
    DaDatePipe,
//    {provide: NgbDateParserFormatter, useClass: NgbUpdatedDateParserFormatter}  use if want to change formatter of ngb drop down
    ScheduleGroupsService,
    DepartmentsService,
    MattersService,
    SearchPatternService
  ],
  entryComponents: [
    AddTagItemDialogComponent,
    AddDocTypeDialogComponent,
    DocTypeItemGridComponent,
    DateFilterItemGridComponent,
    DatePartitionDialogComponent,
    AddEditScheduleGroupDialogComponent,
    ScheduleGroupsGridHeaderComponent,
    ScheduleGroupsItemGridComponent,
    DepartmentsGridHeaderComponent,
    DepartmentsItemGridComponent,
    AddEditDepartmentDialogComponent,
    CloneDepartmentDialogComponent,
    MattersGridHeaderComponent,
    MattersItemGridComponent,
    AddEditMatterDialogComponent,
    CloneMatterDialogComponent,
    SearchPatternAdminItemGridComponent,
    SearchPatternAdminGridHeaderComponent,
    PatternCategoriesItemGridComponent,
    RegulationsItemGridComponent,
    AddEditSearchPatternDialogComponent,
    AddEditPatternCategoryDialogComponent,
    AddEditRegulationDialogComponent,
    PatternCategoriesGridHeaderComponent,
    RegulationGridHeaderComponent,
    DateFilterGridHeaderComponent,
    DocTypesGridHeaderComponent,
    SavedUserViewGridHeaderComponent,
    SavedUserViewItemGridComponent,
    SavedUserViewEditorDialogComponent,
    AddEditChartWidgetDialogComponent,
    ViewChartWidgetDialogComponent,
    SavedWidgetGridHeaderComponent,
    SavedWidgetItemGridComponent
  ],
})
export class SettingsModule {

}


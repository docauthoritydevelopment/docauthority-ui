export enum SettingPathParams {
  searchText = 'namingSearchTerm',
  sort = 'sort',
  page = 'page',
  regulationFilter = 'regulationFilter',
  selectedItemId = 'selectedItemId',
  findId = 'findId',
  dashboardId =  'dashboardId',
  containerId = 'containerId'
}

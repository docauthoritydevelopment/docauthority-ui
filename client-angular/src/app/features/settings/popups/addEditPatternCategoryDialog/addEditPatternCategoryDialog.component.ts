import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {PatternCategoryDto} from "@app/common/settings/types/searchPattern.dto";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {BaseComponent} from "@core/base/baseComponent";

@Component({
  selector: 'add-edit-pattern-category-dialog',
  templateUrl: './addEditPatternCategoryDialog.component.html',
  styleUrls: ['./addEditPatternCategoryDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditPatternCategoryDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public patternCategoryItem: PatternCategoryDto= null;
  public title: string = "";
  public data:{patternCategory : PatternCategoryDto};

  @ViewChild('categoryGeneralForm') public categoryGeneralForm: NgForm = null;


  constructor(private activeModal: NgbActiveModal, private searchPatternService: SearchPatternService, private dialogService: DialogService) {
    super();
  }


  daOnInit(): void {
    this.setInitData();
  }



  private setInitData() {
    this.patternCategoryItem=<PatternCategoryDto>{};
    if (this.data.patternCategory) {
      this.patternCategoryItem = _.cloneDeep(this.data.patternCategory);
    }
    if (this.patternCategoryItem.id){
      this.title = "Edit pattern category";
    }
    else  {
      this.title = "Add pattern category";
      this.patternCategoryItem.name = "";
    }
  }



  isValid = () => {
    return this.categoryGeneralForm.valid;
  };


  cancel() {
    this.activeModal.close(null);
  }


  save = () => {
    if (this.isValid()) {
      if (this.patternCategoryItem.id) {
        this.searchPatternService.updatePatternCategoryData(this.patternCategoryItem).subscribe((addedCategory: PatternCategoryDto) => {
          this.activeModal.close(addedCategory);
        });
      }
      else {
        this.searchPatternService.addNewPatternCategoryData(this.patternCategoryItem).subscribe((addedCategory: PatternCategoryDto) => {
          this.activeModal.close(addedCategory);
        });
      }
    }
  };

  ngOnDestroy() {
  }

}

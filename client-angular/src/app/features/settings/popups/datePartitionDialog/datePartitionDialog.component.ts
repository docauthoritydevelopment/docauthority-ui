import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DateRangeItemDto, DateRangePartitionDto, DateRangePointDto} from "@app/common/settings/types/dateFilters.dto";
import {DateRangePointType, TimePeriod} from "@app/common/settings/types/dateFilters.enums";
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {NgForm} from "@angular/forms";
import {DateFiltersService} from "@app/features/settings/services/dateFilters.service";


@Component({
  selector: 'date-partition-dialog',
  templateUrl: './datePartitionDialog.component.html',
  styleUrls: ['./datePartitionDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatePartitionDialogComponent implements OnInit {
  data : {datePartitionDto: DateRangePartitionDto};
  activeTab = "general";
  editMode:boolean = false;
  initiated:boolean = false;
  title: string ='';
  activeDateRangeItem: DateRangeItemDto;
  rangeIsValid:boolean = true;
  newEverTitle:string;
  editEverTitleMode:boolean = false;

  @ViewChild('datePartitionForm') public datePartitionForm: NgForm = null;

  constructor(private activeModal: NgbActiveModal, private datePipe: DatePipe, private i18nService : I18nService,private dateFiltersService: DateFiltersService) { }

  ngOnInit() {
    this.setInitData();
  }


  private setInitData() {
    if (!this.data.datePartitionDto) {
      this.editMode = false;
      this.title='Add date partition';
      var datePartitionDto=new DateRangePartitionDto();
      var ever = new DateRangeItemDto();
      ever.start =  new DateRangePointDto();
      ever.start.type = DateRangePointType.EVER;
      var today = new DateRangeItemDto();
      today.start =  new DateRangePointDto();
      today.start.relativePeriodAmount = 0;
      today.start.relativePeriod = TimePeriod.DAYS;
      today.start.type =  DateRangePointType.RELATIVE;
      datePartitionDto.continuousRanges = true;
      datePartitionDto.dateRangeItemDtos=[ever,today];
      this.data.datePartitionDto = datePartitionDto;
      this.setSummaryText(datePartitionDto.dateRangeItemDtos[0]);
      this.initiated=true;
    }
    else {
      this.title='Edit date partition';
      this.editMode=true;
      this.initiated = true;
      this.setActiveTab('rangeItems');
    }
  }


  close() {
    this.activeModal.close(null);
  }

  isValid(){
    return this.rangeIsValid && this.datePartitionForm.valid;
  }

  saveDatePartition() {
    if (this.isValid()) {
      let tmpDateFilter:DateRangePartitionDto = _.cloneDeep(this.data.datePartitionDto);
      this.addUnavailableRange(tmpDateFilter);
      this.removeEndDates(tmpDateFilter);
      if (!tmpDateFilter.id) {
        this.dateFiltersService.addNewDatePartition(tmpDateFilter).subscribe((ans: DateRangePartitionDto) => {
          this.activeModal.close(ans);
        });
      }
      else {
        this.dateFiltersService.editDatePartition(tmpDateFilter).subscribe((ans: DateRangePartitionDto) => {
          this.activeModal.close(ans);
        });
      }
    }
  }

  addUnavailableRange = (tmpDateFilter:DateRangePartitionDto)=>  {
    var newRange = new DateRangeItemDto();
    newRange.name = "Unavailable";
    newRange.start = new DateRangePointDto();
    newRange.start.type = DateRangePointType.UNAVAILABLE;
    tmpDateFilter.dateRangeItemDtos.push(newRange);
  };

  removeEndDates = (tmpDateFilter:DateRangePartitionDto)=>  {
    let items = tmpDateFilter.dateRangeItemDtos;
    _.forEach(items, (item) => {
      item.end = null;
    });
  };


  setActiveTab=function(tabName:string)
  {
    this.activeTab=tabName;
  }

  isStartFromEver = (): boolean =>
  {
    return this.initiated &&(< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[0]!=null &&
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[0].start.type == DateRangePointType.EVER;
  }

  toggleStartFromEver = () =>
  {
    if( this.isStartFromEver()) {
      (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.splice(0,1);
      this.pointChange((< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[0]);
    }
    else {
      var ever = new DateRangeItemDto();
      ever.start =  new DateRangePointDto();
      ever.start.type = DateRangePointType.EVER;
      ever.start.absoluteTime = (new Date(" 1/1/1971")).getTime();
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.unshift(ever);
      this.pointChange(ever);
    }
  }

  toggleUpToToday = () =>
  {
    if( this.isUpToToday()) {
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.splice( (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1,1);
      this.pointChange( (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[(< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1]);
    }
    else {
      var today = new DateRangeItemDto();
      today.start =  new DateRangePointDto();
      today.start.type = DateRangePointType.RELATIVE;
      today.start.relativePeriod = TimePeriod.DAYS;
      today.start.relativePeriodAmount = 0;
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.push(today);
      this.pointChange(today);
    }
  }


  pointChange = (dateRangeDto:DateRangeItemDto) =>
  {
    var index = (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeDto);
    if(index-1>=0) {
      var prevItem = (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index-1];
      this.setSummaryText(prevItem);
    }
    this.setSummaryText(dateRangeDto);
  }

  pointTitleChange = ([dateRangeDto, newName] ) => {
    (<DateRangeItemDto>dateRangeDto.name) = newName;
  }



  setSummaryText = (dateRangeDto:DateRangeItemDto) =>
  {
    var index = (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeDto);
    if(index == (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1)
    {
      (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index].name = null;
      return; //Last range item do not have name
    }
    if ((<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length>index+1) //At least two items exists in order to write title
    {
      var title;
      var valid = true;
      var item =  (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index];
      var nextItem =  (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index+1];
      if(item.start.type == DateRangePointType.EVER)
      {
        title = 'Ever';
      }
      else if(item.start.type == DateRangePointType.RELATIVE)
      {
        valid =valid && item.start.relativePeriodAmount!=null && item.start.relativePeriod!=null;
        title = item.start.relativePeriodAmount + ' ' + this.i18nService.translateId(item.start.relativePeriod);
      }
      else {
        valid =valid && item.start.absoluteTime!=null ;
        title = this.datePipe.transform(item.start.absoluteTime,'dd-MMM-yy HH:mm') ;
      }
      title+=' till ';
      if(nextItem.start.type == DateRangePointType.RELATIVE &&nextItem.start.relativePeriodAmount == 0)
      {
        title += 'today';
      }
      else if(nextItem.start.type == DateRangePointType.RELATIVE)
      {
        valid =valid && nextItem.start.relativePeriodAmount!=null && nextItem.start.relativePeriod!=null;
        title += nextItem.start.relativePeriodAmount + ' ' +  this.i18nService.translateId(nextItem.start.relativePeriod).toLowerCase() ;
      }
      else {
        valid =valid && nextItem.start.absoluteTime!=null ;
        title += this.datePipe.transform(nextItem.start.absoluteTime,'dd-MMM-yy HH:mm')   ;
      }
      (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index].name = valid?title:null;
    }
  }



  shouldBe = ($index,$last) => {
    if (this.isStartFromEver() && $index == 0) {
      return false;
    }

    if (this.isUpToToday() && $last) {
      return false;
    }
    return true;
  }


  verifyRange = ()=> {
    this.rangeIsValid = true;
    this.data.datePartitionDto.dateRangeItemDtos.forEach((item: DateRangeItemDto)=> {
      this.rangeIsValid = this.rangeIsValid && !item.isInValid;
    })
  }


  setActiveDateRangeItem = (rangeItem)=>  {
    this.activeDateRangeItem = rangeItem;
    this.data.datePartitionDto.dateRangeItemDtos.forEach(()=> {

    });
  }


  isUpToToday = ()=>
  {
    return this.initiated && (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[0]!=null &&
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[ (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1].start.type == DateRangePointType.RELATIVE&&
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[ (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1].start.relativePeriodAmount == 0;
  }


  setEditEverTitleMode = (value)=>  {
    if (value) {
      this.newEverTitle = this.data.datePartitionDto.dateRangeItemDtos[0].name;
    }
    this.editEverTitleMode = value;
  };
  cancelEditEverTitle = ()=>  {
    this.newEverTitle = null;
    this.setEditEverTitleMode(false);
  };

  editEverTitle = ()=>  {
    if (!this.newEverTitle || this.newEverTitle == '') {
      this.setSummaryText(this.data.datePartitionDto.dateRangeItemDtos[0]);
    }
    else {
      this.data.datePartitionDto.dateRangeItemDtos[0].name = this.newEverTitle;
    }
    this.setEditEverTitleMode(false);
  };


  createNewPoint = ()=>
  {
    var newPoint = new DateRangeItemDto();

    newPoint.start = new DateRangePointDto();
    newPoint.start.type = DateRangePointType.RELATIVE;
    let theIndex:number = null;
    if(this.activeDateRangeItem)
    {
      theIndex =   (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.indexOf(this.activeDateRangeItem);
      newPoint.start.type = (<DateRangeItemDto>this.activeDateRangeItem).start.type == DateRangePointType.ABSOLUTE?DateRangePointType.ABSOLUTE:DateRangePointType.RELATIVE; //not ever
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.splice(theIndex+1,0,newPoint);

    }
    else {
      if(this.isUpToToday()) {
        theIndex =  (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length-1;
        (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.splice(theIndex,0,newPoint);
      }
      else {
        (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.push(newPoint);
      }
    }
    if (theIndex == 1) {
      (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[0].name = null;
    }
    this.activeDateRangeItem = newPoint;
    this.rangeIsValid = false;
  }

  deletePoint = (dateRangeItem) =>
  {
    var index =   (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.indexOf(dateRangeItem);
    if (index > -1 ) {
      (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.splice(index, 1);
      if (index - 1 >= 0) {
        var prevItem = (<DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index - 1];
        this.setSummaryText(prevItem);
      }
      if (index < (< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos.length) {
        this.setSummaryText((< DateRangePartitionDto>this.data.datePartitionDto).dateRangeItemDtos[index]);
      }
    }
    this.activeDateRangeItem = null;
    this.verifyRange();
  }

  onRangeItemValidityChange = (item:DateRangeItemDto , isValid)=>  {
    item.isInValid = !isValid;
    this.verifyRange();
  };

}

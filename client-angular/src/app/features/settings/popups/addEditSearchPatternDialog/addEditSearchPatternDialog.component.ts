import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {PatternCategoryDto, RegulationDto, TextSearchPatternDto} from "../../../../common/settings/types/searchPattern.dto";
import {PatternType} from "../../../../common/settings/types/searchPattern.enums";
import {NgForm} from "@angular/forms";
import {forkJoin} from "rxjs";
import {SearchPatternService} from "../../services/searchPattern.service";
import {DialogService} from "../../../../shared-ui/components/modal-dialogs/dialog.service";
import {AddEditPatternCategoryDialogComponent} from "@app/features/settings/popups/addEditPatternCategoryDialog/addEditPatternCategoryDialog.component";
import {BaseComponent} from "@core/base/baseComponent";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";


@Component({
  selector: 'add-edit-search-pattern-dialog',
  templateUrl: './addEditSearchPatternDialog.component.html',
  styleUrls: ['./addEditSearchPatternDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditSearchPatternDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public data:{textSearchPattern : TextSearchPatternDto};
  public title: string = "";
  public activeTab: string = "pattern";
  public textSearchPattern: TextSearchPatternDto;
  public inEditMode:boolean = false;
  public loading: boolean = false;
  public isTechSupport:boolean = false;
  private regulationsMap:any  = {};
  public newSubCategoryData:any = {};
  public newCategoryData:any = {
    patternCategory : <PatternCategoryDto>{
      active : true
    }
  };
  private previousActiveState:boolean = false;

  @ViewChild('patternForm') public patternForm: NgForm = null;

  private _allCategoriesList: PatternCategoryDto[] = null;
  public _categoriesList: PatternCategoryDto[] = null;
  public _subCategoriesList: PatternCategoryDto[] = null;
  public regulationsList: RegulationDto[] = [];

  public AddEditPatternCategoryDialogComponent = AddEditPatternCategoryDialogComponent;

  constructor(private activeModal: NgbActiveModal, private searchPatterService: SearchPatternService, private dialogService: DialogService, private urlAuthorizationService : UrlAuthorizationService) {
    super();
  }

  ngOnInit(): void {
    this.isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    this.setInitData();
    this.showLoader(true);
    setTimeout(()=>{
      this.loadData();
    });
  }

  sortCategories(catList : PatternCategoryDto[]) :PatternCategoryDto[] {
    return catList.sort((cat1:PatternCategoryDto, cat2:PatternCategoryDto) => {
      return cat1.name.toLowerCase().localeCompare(cat2.name.toLowerCase());
    });
  }

  private loadData(){
    let queriesArray:any[] = [];
    queriesArray.push(this.searchPatterService.getPatternCategoriesList());
    queriesArray.push(this.searchPatterService.getRegulationsList());
    forkJoin(queriesArray).subscribe(([categoriesList, regulationsList]) => {
      this._allCategoriesList = categoriesList;
      this.regulationsList = regulationsList;
      this._categoriesList = this.sortCategories(this._allCategoriesList.filter((catItem : PatternCategoryDto)=> {
        return (catItem.parentId == null || catItem.parentId == 0 );
      }));
      this._subCategoriesList = this.sortCategories(this._allCategoriesList.filter((catItem : PatternCategoryDto)=> {
        return (this.textSearchPattern.categoryId != null && catItem.parentId == this.textSearchPattern.categoryId);
      }));
      this.newSubCategoryData = {
        patternCategory : <PatternCategoryDto>{
          parentId : this.textSearchPattern.categoryId,
          active : true
        }
      };
      this.showLoader(false);
    })
  }


  private setInitData() {
    if (this.data.textSearchPattern) {
      this.title = "Edit search pattern";
      this.textSearchPattern = _.cloneDeep(this.data.textSearchPattern);
      this.inEditMode = true;
      this.previousActiveState = this.textSearchPattern.active;
    }
    else {
      this.title = "Add search pattern";
      this.textSearchPattern = new TextSearchPatternDto();
      this.textSearchPattern.pattern="";
      this.textSearchPattern.active = true;
      this.textSearchPattern.regulationIds = [];
      this.textSearchPattern.patternType = PatternType.REGEX;
      this.inEditMode = false;
    }
    this.regulationsMap = {};
    this.textSearchPattern.regulationIds.forEach((regId)=> {
      this.regulationsMap[regId] = true;
    });
  }

  updateRegulationsFromMap(){
    this.textSearchPattern.regulationIds = [];
    this.regulationsList.forEach((regulation:RegulationDto)=> {
      if (this.regulationsMap[regulation.id]) {
        this.textSearchPattern.regulationIds.push(regulation.id);
      }
    });
  }

  setActiveTab = (tabName: string) =>
  {
    this.activeTab = tabName;
  };

  isValid = ()=> {
    return (this.textSearchPattern.subCategoryId && this.patternForm.valid);
  }

  cancel() {
    this.activeModal.close(null);
  }

  save = () => {
    this.checkValidatorClass();
    if (!this.loading && this.isValid()) {
      this.updateRegulationsFromMap();
      if (this.inEditMode) {
        this.searchPatterService.updateSearchPatternData(this.textSearchPattern,this.previousActiveState).subscribe((ans: TextSearchPatternDto) => {
          this.activeModal.close(ans);
        });
      }
      else {
        this.searchPatterService.addNewSearchPatternData(this.textSearchPattern).subscribe((ans: TextSearchPatternDto) => {
          this.activeModal.close(ans);
        });
      }
    }
  };

  onCategoryChanged(newCategory:PatternCategoryDto ) {
    let checkIsNewList: PatternCategoryDto[]  = this._allCategoriesList.filter((catItem : PatternCategoryDto)=> {
      return (catItem.id == newCategory.id);
    });
    if (checkIsNewList.length == 0 ){
      this._allCategoriesList.push(newCategory);
    }

    this.textSearchPattern.categoryName = newCategory.name;
    this.textSearchPattern.categoryId = newCategory.id;
    this._subCategoriesList = this.sortCategories(this._allCategoriesList.filter((catItem : PatternCategoryDto)=> {
      return (catItem.parentId == this.textSearchPattern.categoryId);
    }));
    if (this._subCategoriesList.length > 0 ) {
      this.textSearchPattern.subCategoryId = this._subCategoriesList[0].id;
      this.textSearchPattern.subCategoryName = this._subCategoriesList[0].name;
    }
    else {
      this.textSearchPattern.subCategoryId = null;
      this.textSearchPattern.subCategoryName = '';
    }
    this.newSubCategoryData = {
      patternCategory : {
        parentId : this.textSearchPattern.categoryId,
        active : true
      }
    };
  }

  onSubCategoryChanged(newSubCategory:PatternCategoryDto ) {
    let checkIsNewList: PatternCategoryDto[]  = this._allCategoriesList.filter((catItem : PatternCategoryDto)=> {
      return (catItem.id == newSubCategory.id);
    });
    if (checkIsNewList.length == 0 ){
      this._allCategoriesList.push(newSubCategory);
    }
    this.textSearchPattern.subCategoryName = newSubCategory.name;
    this.textSearchPattern.subCategoryId = newSubCategory.id;
  }

  private showLoader = (show: boolean) => {
    this.loading = show;
  }

  clickOnRegulation(regulationId) {
    if (this.regulationsMap[regulationId]) {
      delete this.regulationsMap[regulationId];
    }
    else {
      this.regulationsMap[regulationId] = true;
    }
  }

  checkValidatorClass() {
    let trimClass: string = this.textSearchPattern.validatorClass ? this.textSearchPattern.validatorClass.trim() : null;
    if (trimClass != null && trimClass.length>0) {
      this.textSearchPattern.patternType = PatternType.PREDEFINED;
    }
    else {
      this.textSearchPattern.patternType = PatternType.REGEX;
    }
  }


  ngOnDestroy() {
  }
}

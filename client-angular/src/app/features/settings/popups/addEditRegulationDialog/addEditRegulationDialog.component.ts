import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {RegulationDto} from "@app/common/settings/types/searchPattern.dto";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {BaseComponent} from "@core/base/baseComponent";

@Component({
  selector: 'add-edit-pattern-category-dialog',
  templateUrl: './addEditRegulationDialog.component.html',
  styleUrls: ['./addEditRegulationDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddEditRegulationDialogComponent extends BaseComponent implements OnInit, OnDestroy {
  public regulationItem: RegulationDto= null;
  public title: string = "";
  public  data:{regulation : RegulationDto};

  @ViewChild('regulationGeneralForm') public regulationGeneralForm: NgForm = null;


  constructor(private activeModal: NgbActiveModal, private searchPatternService: SearchPatternService, private dialogService: DialogService) {
    super();
  }


  daOnInit(): void {
    this.setInitData();
  }



  private setInitData() {
    this.regulationItem=<RegulationDto>{};
    if (this.data.regulation) {
      this.regulationItem = _.cloneDeep(this.data.regulation);
    }
    if (this.regulationItem.id){
      this.title = "Edit regulation";
    }
    else  {
      this.title = "Add regulation";
      this.regulationItem.name = "";
    }
  }



  isValid = () => {
    return this.regulationGeneralForm.valid;
  };


  cancel() {
    this.activeModal.close(null);
  }


  save = () => {
    if (this.isValid()) {
      if (this.regulationItem.id) {
        this.searchPatternService.updateRegulationData(this.regulationItem).subscribe((updateRegulation: RegulationDto) => {
          this.activeModal.close(updateRegulation);
        });
      }
      else {
        this.searchPatternService.addNewRegulationData(this.regulationItem).subscribe((addedRegulation: RegulationDto) => {
          this.activeModal.close(addedRegulation);
        });
      }
    }
  };

  ngOnDestroy() {
  }

}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {RegulationDto, TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";




@Component({
  selector: 'regulations-item-grid',
  templateUrl: './regulations-item-grid.component.html',
})
export class RegulationsItemGridComponent extends TreeGridInnerItemBaseComponent<RegulationDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;

  regulationStr: string = "";
  inSetActive:boolean = false;

  constructor(private searchPatternService : SearchPatternService) {
    super();
  }

  setActive(isActive:boolean) {
    this.inSetActive = true;
    this.searchPatternService.setRegulationActiveState(this.dataItem.item.id,isActive).subscribe((ansObject: TextSearchPatternDto)=>{
      this.dataItem.item.active = ansObject.active;
      this.inSetActive = false;
    }, (err)=> {
      this.inSetActive = false;
    });
  }

  ngOnInit(): void {
  }

}


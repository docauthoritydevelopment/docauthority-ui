import * as _ from 'lodash'
import {forkJoin} from "rxjs";
import {Component, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RegulationsItemGridComponent} from "./item-grid/regulations-item-grid.component";
import {BaseComponent} from "@core/base/baseComponent";
import {RegulationDto} from "@app/common/settings/types/searchPattern.dto";
import {
  ITreeGridEvents,
  TreeGridConfig,
  TreeGridData,
  TreeGridDataItem,
  TreeGridSortOrder
} from "@tree-grid/types/tree-grid.type";
import {MetadataService} from "@services/metadata.service";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ToolbarActionTypes} from "@app/types/display.type";
import {AddEditRegulationDialogComponent} from "@app/features/settings/popups/addEditRegulationDialog/addEditRegulationDialog.component";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {RegulationGridHeaderComponent} from "@app/features/settings/regulations/grid-header/regulation-grid-header.component";


@Component({
  selector: 'regulations',
  templateUrl: './regulations.component.html',
  styleUrls: ['./regulations.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RegulationsComponent extends BaseComponent {

  public loading: boolean = false;
  public regulationsData: TreeGridData<RegulationDto> = null;
  public isExporting: boolean = false;
  RegulationsItemGridComponent = RegulationsItemGridComponent;
  RegulationGridHeaderComponent = RegulationGridHeaderComponent;

  public treeGridEvents: ITreeGridEvents<RegulationDto>;
  private selectedItemId: number;
  private selectedItem:RegulationDto;
  public settingsActionItems: ButtonConfigDto[];
  public totalElements: number = 0;
  public exportActionItem: ButtonConfigDto;
  public initialSortDirection: TreeGridSortOrder = null;
  public initialSortFieldName: string = null;


  constructor(private urlAuthorizationService: UrlAuthorizationService ,private metadataService: MetadataService, private searchPatterService: SearchPatternService, private routingService: RoutingService, private activatedRoute: ActivatedRoute, private dialogService: DialogService, private router: Router) {
    super(router, activatedRoute);
  }


  daOnInit() {
    this.setActionItems();
    this.setTreeGridEvent();
    this.on(this.searchPatterService.regulationRefreshedEvent$, () => {
      this.loadData(true);
    });
  }


  routeChanged(){
    this.loadData(true);
  }

  private initTableSort(params:any) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    }
    let sortObject=JSON.parse(params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  //----------------------------------------

  loadData = (withLoader: boolean = false) => {
    if (withLoader) {
      this.showLoader(true);
    }
    let params = _.cloneDeep(this.getQueryParams()) || {};
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    this.initTableSort(params);
    let queriesArray = [];
    queriesArray.push(this.searchPatterService.getRegulationsData(params, this.selectedItemId));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([regulationsData]) => {
      this.regulationsData = regulationsData;
      this.totalElements = regulationsData.totalElements;
      if (withLoader) {
        this.showLoader(false);
      }
    }));
  };


  exportData = ()=> {
    let params = _.cloneDeep(this.getQueryParams()) || {};
    this.searchPatterService.exportRegulationsData(this.totalElements,params);
  }

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);


  private setActionItems() {
    this.settingsActionItems = [];
    let isSupportAllCrudOperation:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ManageSearchPatterns]);
    if (isSupportAllCrudOperation) {
      this.settingsActionItems = [
        {
          type: ToolbarActionTypes.ADD,
          title: "Add regulation",
          name: 'Add',
          actionFunc: this.openAddNewDialog,
          disabled: false,
          href: ToolbarActionTypes.ADD,
          order: 0,
          btnType: 'flat'
        },
        {
          type: ToolbarActionTypes.EDIT,
          title: "Edit regulation",
          name: 'Edit',
          actionFunc: this.openEditDialog,
          disabled: !this.selectedItem ,
          href: ToolbarActionTypes.EDIT,
          order: 1,
          btnType: 'link'
        },
        /* {
          type: ToolbarActionTypes.DELETE,
          title: "Delete regulation",
          actionFunc: this.openDeleteDialog,
          disabled: !this.selectedItem,
          href: ToolbarActionTypes.DELETE,
          order: 2
        }*/
      ];
    }
    let exportDropDownItems = [{
      title: "As Excel",
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: "Export options",
      disabled: this.isExporting,
      children: exportDropDownItems,
      href: ToolbarActionTypes.EXPORT
    }
  }


  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<RegulationDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  private onItemSelected = (itemId, item) => {
    if (!this.selectedItem || this.selectedItemId != itemId) {
      this.selectedItemId = itemId;
      this.routingService.replaceQueryParamsWithoutReloading(this.getQueryParams(),
        RoutePathParams.selectedItemId, this.selectedItemId);
      this.updateLatestQueryParams(RoutePathParams.selectedItemId, this.selectedItemId);
    }
    this.selectedItem = item;
    this.setActionItems();
  };



  private onPageChanged = (pageNum) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.page],[pageNum]);
  };


  private onSortByChanged = (sortObj: any) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.sort],[JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder)}])]);
  };

  openAddNewDialog = ()=> {
    let data = { data: {} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditRegulationDialogComponent, data, modelParams);
  }

  openEditDialog = ()=> {
    let data = { data: {
        regulation : this.selectedItem
      } };
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditRegulationDialogComponent, data, modelParams);
  }


  openDeleteDialog = ()=> {
    this.dialogService.openConfirm('Confirmation', 'You are about to delete regulation named: '+ this.selectedItem.name +'. Continue?' ).then(result => {
      if (result) {
        this.searchPatterService.deleteRegulationData(this.selectedItem.id).subscribe();
      }
    });
  }

}

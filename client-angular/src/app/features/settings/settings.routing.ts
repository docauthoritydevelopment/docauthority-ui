import {Routes} from '@angular/router';
import {SettingsRoutePaths} from './settings.route.paths';
import {DocTypesComponent} from './doctypes/docTypes.component';
import {ScheduleGroupsComponent} from './scheduleGroups/scheduleGroups.component';
import {DepartmentsComponent} from '@app/features/settings/departments/departments.component';
import {SettingsComponent} from './settings.component';
import {AuthorizationGuard} from "@core/authorization.guard";
import {DateFiltersComponent} from "@app/features/settings/dateFilters/dateFilters.component";
import {SearchPatternAdminItemGridComponent} from "@app/features/settings/searchPatterns/searchPatternAdmin/item-grid/search-pattern-admin-item-grid.component";
import {SearchPatternAdminComponent} from "@app/features/settings/searchPatterns/searchPatternAdmin/searchPatternAdmin.component";
import {PatternCategoriesComponent} from "@app/features/settings/patternCategories/patternCategories.component";
import {RegulationsComponent} from "@app/features/settings/regulations/regulations.component";
import {LogLevelsComponent} from "@app/features/settings/logLevels/log-levels.component";
import {MattersComponent} from "@app/features/settings/matters/matters.component";
import {SavedFiltersComponent} from "@app/features/settings/savedUserView/savedFilters/savedFilters.component";
import {SavedChartsComponent} from "@app/features/settings/savedUserView/savedCharts/savedCharts.component";
import {SavedWidgetsComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgets.component";
import {SavedViewsComponent} from "@app/features/settings/savedUserView/savedViews/savedViews.component";
import {SavedDashboardsComponent} from "@app/features/settings/savedUserView/savedDashboards/savedDashboards.component";

export const settingsRoutes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: SettingsRoutePaths.doctypes,
        component: DocTypesComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.dateFilters,
        component: DateFiltersComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.scheduleGroups,
        component: ScheduleGroupsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.departments,
        component: DepartmentsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.matters,
        component: MattersComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.searchPatterns,
        component: SearchPatternAdminComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.patternCategories,
        component: PatternCategoriesComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.regulations,
        component: RegulationsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.logLevels,
        loadChildren: `app/features/settings/logLevels/log-levels.module#LogLevelsModule`,  // lazy load module
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.savedFilters,
        component: SavedFiltersComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.savedViews,
        component: SavedViewsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.savedCharts,
        component: SavedChartsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.savedDashboards,
        component: SavedDashboardsComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: SettingsRoutePaths.savedWidgets,
        component: SavedWidgetsComponent,
        canActivate: [AuthorizationGuard]
      }
    ]
  }
];

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TreeGridData, } from "@tree-grid/types/tree-grid.type";
import {DateRangeItemDto, DateRangePartitionDto} from "@app/common/settings/types/dateFilters.dto";
import {Observable, Subject} from 'rxjs/index';
import {DateRangePointType} from "@app/common/settings/types/dateFilters.enums";
import {DaDatePipe} from "@shared-ui/pipes/da-date.pipe";
import {RdbService} from "@services/rdb.service";
import {ServerUrls} from "@app/common/configuration/common-config";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";






@Injectable()
export class DateFiltersService {


  private dateFiltersRefreshedEvent = new Subject<number>();
  dateFiltersRefreshedEvent$ = this.dateFiltersRefreshedEvent.asObservable();


  constructor(
    private dialogService: DialogService,
    private http: HttpClient,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private datePipe : DaDatePipe,
    private excelService: ExcelService,
    private rdbService : RdbService
  )
  {
  }


  getDateFilterData(params, selectedItemId):Observable<TreeGridData<DateRangePartitionDto>>{
    return this.http.get(ServerUrls.DATE_PARTITIONS_FULL,{params}).pipe(map((dateRangeList:DTOPagingResults<DateRangePartitionDto>)=> {
      dateRangeList.content.forEach((datePartItem: DateRangePartitionDto)=> {
          datePartItem.dateRangeItemDtos = datePartItem.dateRangeItemDtos.reverse();
          if(datePartItem.dateRangeItemDtos.length > 0) {
            if(datePartItem.dateRangeItemDtos[0].start.type ===  DateRangePointType.UNAVAILABLE) {
              datePartItem.dateRangeItemDtos.shift();
            }
          }
        });
      let ans: TreeGridData<DateRangePartitionDto> = this.treeGridAdapter.adapt(
          dateRangeList,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
      return ans;
    }));
  }


  addNewDatePartition(dateFilter:DateRangePartitionDto):Observable<any>{
    dateFilter.dateRangeItemDtos = dateFilter.dateRangeItemDtos.reverse();
    return this.http.put(ServerUrls.DATE_PARTITIONS,dateFilter).pipe(
      tap({
        next: () => {
          this.dateFiltersRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showAlert("Error","Error on adding new date partition.",AlertType.Error,error);
        }
      }));
  }


  deleteDatePartition(datePartitionId) {
    this.http.delete(this.rdbService.parseServerURL(ServerUrls.SINGLE_DATE_PARTITION, {dpId : datePartitionId})).subscribe((result)=> {
      this.dateFiltersRefreshedEvent.next();
    });
  }


  editDatePartition(dateFilter:DateRangePartitionDto):Observable<any>{
    dateFilter.dateRangeItemDtos = dateFilter.dateRangeItemDtos.reverse();
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.SINGLE_DATE_PARTITION, {dpId : dateFilter.id}),dateFilter).pipe(
      tap({
        next: () => {
          this.dateFiltersRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showAlert("Error","Error on editing date partition.",AlertType.Error,error.error);
        }
      }));
  }


  exportData(dateFilters:DateRangePartitionDto[]) {
    let data = this.buildData(dateFilters);
    return new Observable((observer) => {
      this.excelService.exportAsExcelFile(data, 'dataFilters', ['ID', 'Name', 'Description']);
      observer.next();
    });
  }


  private buildData(dateFilters:DateRangePartitionDto[]) {

    let res = [];
    dateFilters.forEach((item : DateRangePartitionDto)=> {
      res.push(this.setDataFilter(item));
    });
    return res;
  }

  private setDataFilter(item: DateRangePartitionDto) {
    let resItem = <any>{};
    resItem.ID =  item.id;
    resItem.Name = item.name;
    resItem.Description = item.description;
    let periodsStr = '';
    item.dateRangeItemDtos.forEach((rangeItem:DateRangeItemDto)=> {
      if (rangeItem.name) {
        if (periodsStr != '') {
          periodsStr = periodsStr + ",";
        }
        periodsStr = periodsStr + rangeItem.name;
      }
    });
    resItem.Periods = periodsStr;
    return resItem;
  }

}

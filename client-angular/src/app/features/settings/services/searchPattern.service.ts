import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams, HttpRequest} from '@angular/common/http';
import {map, finalize, tap} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {AggregationCountItemDTO, DTOPagingResults} from '@core/types/query-paging-results.dto';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TreeGridData} from "@tree-grid/types/tree-grid.type";
import {Observable, Subject} from 'rxjs/index';
import {RdbService} from "@services/rdb.service";
import {CommonConfig, ExportType, ServerUrls} from "@app/common/configuration/common-config";
import {PatternCategoryDto, RegulationDto, TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {ServerExportService} from "@services/serverExport.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";
import {IDataService} from "@core/base/IDataService";




@Injectable()
export class SearchPatternService implements IDataService<TextSearchPatternDto,TextSearchPatternDto> {


  private searchPatternRefreshedEvent = new Subject<TextSearchPatternDto>();
  searchPatternRefreshedEvent$ = this.searchPatternRefreshedEvent.asObservable();
  public updatedDataEvent$= this.searchPatternRefreshedEvent.asObservable();

  private searchPatternActiveChangedEvent = new Subject<TextSearchPatternDto>();
  searchPatternActiveChangedEvent$ = this.searchPatternActiveChangedEvent.asObservable();

  private searchPatternNewEvent = new Subject<TextSearchPatternDto>();
  searchPatternNewEvent$ = this.searchPatternNewEvent.asObservable();
  public newDataEvent$ = this.searchPatternNewEvent.asObservable();

  private searchPatternDeleteEvent = new Subject<TextSearchPatternDto>();
  public deleteDataEvent$ = this.searchPatternDeleteEvent.asObservable();

  private patternCategoryRefreshedEvent = new Subject<number>();
  patternCategoryRefreshedEvent$ = this.patternCategoryRefreshedEvent.asObservable();

  private patternCategoryActiveChangedEvent = new Subject<number>();
  patternCategoryActiveChangedEvent$ = this.patternCategoryActiveChangedEvent.asObservable();

  private regulationRefreshedEvent = new Subject<number>();
  regulationRefreshedEvent$ = this.regulationRefreshedEvent.asObservable();


  constructor(
    private dialogService: DialogService,
    private http: HttpClient,
    private i18nService: I18nService,
    private serverExportService: ServerExportService,
    private urlParamsService: UrlParamsService,
    private rdbService: RdbService,
    private treeGridAdapter: TreeGridAdapterService
  )
  {
  }

  setPatternActiveState(patternId:number, isActive:boolean): Observable<Object> {
    return this.http.post(this.rdbService.parseServerURL((isActive ? ServerUrls.SEARCH_PATTERN_ACTIVE_STATE_TRUE : ServerUrls.SEARCH_PATTERN_ACTIVE_STATE_FALSE), {patternId: patternId}), isActive, {headers: {'Content-Type': 'application/json;charset=UTF-8'}}).pipe(
      tap({
        next: (resultObj:TextSearchPatternDto) => {
          if (resultObj.active) {
            this.searchPatternActiveChangedEvent.next();
          }
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }

  setPatternCategoriesActiveState(patternCategoryId:number, isActive:boolean): Observable<Object> {
    return this.http.post(this.rdbService.parseServerURL((isActive ? ServerUrls.PATTERN_CATEGORY_ACTIVE_STATE_TRUE : ServerUrls.PATTERN_CATEGORY_ACTIVE_STATE_FALSE),{patternCategoryId : patternCategoryId}), isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).pipe(
      tap({
        next: (ansObject:PatternCategoryDto) => {
          if (ansObject.active) {
            this.patternCategoryActiveChangedEvent.next();
          }
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }


  setRegulationActiveState(regulationId:number, isActive:boolean): Observable<Object> {
    return this.http.post(this.rdbService.parseServerURL((isActive ? ServerUrls.REGULATION_ACTIVE_STATE_TRUE : ServerUrls.REGULATION_ACTIVE_STATE_FALSE),{regulationId : regulationId}), isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).pipe(
      tap({
        next: (ansObject:RegulationDto) => {
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }


  getRegulationsList(): Observable<RegulationDto[]>{
    return this.http.get(this.rdbService.getQueryForAllItems(ServerUrls.REGULATION_DATA),{}).pipe(map((data: DTOPagingResults<RegulationDto>)=> {
      return data.content;
    }));
  }

  getPatternCategoriesList(): Observable<PatternCategoryDto[]>{
    return this.http.get(this.rdbService.getQueryForAllItems(ServerUrls.SEARCH_PATTERN_CATEGORY),{}).pipe(map((data: DTOPagingResults<PatternCategoryDto>)=> {
      return data.content;
    }));
  }

  getRegulationsData(params, selectedItemId):Observable<TreeGridData<RegulationDto>>{
    params = this.urlParamsService.fixedPagedParams(params,30);
    return this.http.get(ServerUrls.REGULATION_DATA, {params}).pipe(map((data: DTOPagingResults<RegulationDto>)=> {
      let ans: TreeGridData<RegulationDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }


  getData(params):Observable<DTOPagingResults<TextSearchPatternDto>>{
    params = this.urlParamsService.fixedPagedParams(params,30);
    return this.http.get<DTOPagingResults<TextSearchPatternDto>>(ServerUrls.SEARCH_PATTERN_DATA, {params});
  }

  getPatternCategoryData(params, selectedItemId):Observable<TreeGridData<PatternCategoryDto>>{
    params.take = CommonConfig.MAX_FIRST_LEVEL_ITEMS_IN_TREE;
    params.pageSize = CommonConfig.MAX_FIRST_LEVEL_ITEMS_IN_TREE;
    return this.http.get(ServerUrls.SEARCH_PATTERN_CATEGORY_FULL, {params}).pipe(map((data: DTOPagingResults<PatternCategoryDto>)=> {
      let ans: TreeGridData<PatternCategoryDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }


  addNewSearchPatternData(searchPattern:TextSearchPatternDto):Observable<TextSearchPatternDto>{
    return this.http.put<TextSearchPatternDto>(ServerUrls.SEARCH_PATTERN_DATA, searchPattern).pipe(
      tap({
        next: (resultObj:TextSearchPatternDto) => {
          this.searchPatternNewEvent.next(resultObj);
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  updateSearchPatternData(searchPattern:TextSearchPatternDto,previousActiveState:boolean):Observable<any>{
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.SEARCH_PATTERN_DATA_WITH_ID,{patternId : searchPattern.id}), searchPattern).pipe(
      tap({
        next: (resultObject:TextSearchPatternDto) => {
          if (resultObject.active && !previousActiveState) {
            this.searchPatternActiveChangedEvent.next();
          }
          this.searchPatternRefreshedEvent.next(resultObject);
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
    ;
  }

  deleteSearchPattern(searchPatterId:number):Observable<any>{
    return this.http.delete(this.rdbService.parseServerURL(ServerUrls.SEARCH_PATTERN_DATA_WITH_ID,{patternId : searchPatterId})).pipe(finalize(()=> {
      this.searchPatternDeleteEvent.next();
    }));
  }

  addNewPatternCategoryData(patternCategory:PatternCategoryDto):Observable<any>{
    return this.http.put(ServerUrls.SEARCH_PATTERN_CATEGORY, patternCategory).pipe(
      tap({
        next: () => {
          this.patternCategoryRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        },
      }));
  }

  updatePatternCategoryData(patternCategory:PatternCategoryDto):Observable<any>{
    return this.http.post(ServerUrls.SEARCH_PATTERN_CATEGORY, patternCategory).pipe(
      tap({
          next: () => {
            this.patternCategoryRefreshedEvent.next();
          },
          error: (error: HttpErrorResponse) => {
            this.dialogService.showCrudOperationsError(error.error);
          }
      }));
  }

  deletePatternCategory(patternCategoryId:number):Observable<any>{
    return this.http.delete(this.rdbService.parseServerURL(ServerUrls.SEARCH_PATTERN_CATEGORY_WITH_ID,{categoryId : patternCategoryId})).pipe(
      tap({
        next: () => {
          this.patternCategoryRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }


  addNewRegulationData(regulation:RegulationDto):Observable<any>{
    return this.http.put(ServerUrls.REGULATION_DATA, regulation).pipe(
      tap({
        next: () => {
          this.regulationRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }

  updateRegulationData(regulation:RegulationDto):Observable<any>{
    return this.http.post(ServerUrls.REGULATION_DATA, regulation).pipe(
      tap({
        next: () => {
          this.regulationRefreshedEvent.next();
        },
        error: (error: HttpErrorResponse) => {
          this.dialogService.showCrudOperationsError(error.error);
        }
      }));
  }

  deleteRegulationData(regulationId:number):Observable<any>{
    return this.http.delete(this.rdbService.parseServerURL(ServerUrls.REGULATION_DATA_WITH_ID,{regulationId : regulationId})).pipe(
    tap({
      next: () => {
        this.regulationRefreshedEvent.next();
      },
      error: (error: HttpErrorResponse) => {
        this.dialogService.showCrudOperationsError(error.error);
      }
    }));
  }

  exportData(totalItemsNum:number,params ):void{
    this.serverExportService.prepareExportFile('Search patterns',ExportType.TEXT_SEARCH_PATTERNS_SETTINGS,params,{ itemsNumber : totalItemsNum },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  exportPatternCategoriesData(totalItemsNum,params ){
    this.serverExportService.prepareExportFile('Patterns categories',ExportType.PATTERN_CATEGORY,params,{ itemsNumber : totalItemsNum },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  exportRegulationsData(totalItemsNum,params ){
    this.serverExportService.prepareExportFile('Regulation settings',ExportType.REGULATIONS_SETTINGS,params,{ itemsNumber : totalItemsNum },totalItemsNum > CommonConfig.minimum_items_to_open_export_popup);
  }

  extractImportValidationData = (item) => {
    const partialItem: any = {};
    partialItem['Msg Type'] = item.type;
    partialItem['Line number'] = item.data.rowDto.lineNumber;
    partialItem['Msg Description'] = item.data.message;
    partialItem['NAME'] = item.data.rowDto.fieldsValues[0];
    partialItem['ACTIVE'] = item.data.rowDto.fieldsValues[1];
    partialItem['TYPE'] = item.data.rowDto.fieldsValues[2];
    partialItem['DESCRIPTION'] = item.data.rowDto.fieldsValues[3];
    partialItem['SCHEDULE_EVERY'] = item.data.rowDto.fieldsValues[4];
    partialItem['SCHEDULE_HOUR'] = item.data.rowDto.fieldsValues[5];
    partialItem['SCHEDULE_MINUTES'] = item.data.rowDto.fieldsValues[6];
    return partialItem;
  };

  validateImportFile = (formData:FormData, maxErrorRows:number) =>  {
    return this.http.post(`/api/searchpattern/import/csv/validate?maxErrorRowsReport=${maxErrorRows}`, formData);
  };

  importData = (fileData:File, updateDuplicates:boolean) => {
    return this.http.post(`/api/searchpattern/import/csv?updateDuplicates=${updateDuplicates}`, fileData);
  };

  getPatternAccociatedFiles(patternId):Observable<number>{
    let neededFilter:FilterDescriptor  = {"logic":"and","filters":[{"field":"patternId","operator":"eq","value":patternId}]};
    let params:HttpParams = new HttpParams().set('filter', JSON.stringify(neededFilter)).set('withoutValidation','true');
    return this.http.get(ServerUrls.SEARCH_PATTERN_COUNT,{params : params}).pipe(map((result:DTOPagingResults<AggregationCountItemDTO<TextSearchPatternDto>>)=> {
      if (!result || result.content==null || result.content.length ==0 ) {
        return 0;
      }
      let filteredList:any[] = result.content.filter((dataItem)=> {
        return (dataItem.item.id == patternId);
      });

      if (filteredList.length == 0 ) {
        return 0;
      }
      return filteredList[0].count;
    }))
  }

}



import * as _ from 'lodash'
import {Component,  ViewEncapsulation} from '@angular/core';
import {
   TreeGridSortOrder
} from "../../../../shared-ui/components/tree-grid/types/tree-grid.type";
import {RoutingService} from "../../../../core/services/routing.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DialogService} from "../../../../shared-ui/components/modal-dialogs/dialog.service";
import {SearchPatternService} from "../../services/searchPattern.service";
import {SearchPatternAdminItemGridComponent} from "./item-grid/search-pattern-admin-item-grid.component";
import {TextSearchPatternDto} from "../../../../common/settings/types/searchPattern.dto";
import {SearchPatternAdminGridHeaderComponent} from "./grid-header/search-pattern-admin-grid-header.component";
import {AddEditSearchPatternDialogComponent} from "@app/features/settings/popups/addEditSearchPatternDialog/addEditSearchPatternDialog.component";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";
import {FilterOption} from "@app/common/filters/types/filter-interfaces";
import {UtilService} from "@services/util.service";
import {BaseGridManagementComponent} from "@core/base/baseGridManagementComponent";
import {BaseGridManagementConfig} from '@app/core/base/baseGridManagementConfig';
import {TreeGridAdapterService} from "@tree-grid/services/tree-grid-adapter.service";


@Component({
  selector: 'search-pattern-admin',
  templateUrl: './searchPatternAdmin.component.html',
  styleUrls: ['./searchPatternAdmin.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SearchPatternAdminComponent extends BaseGridManagementComponent<TextSearchPatternDto,TextSearchPatternDto> {

  public SearchPatternItemGridComponent = SearchPatternAdminItemGridComponent;
  public SearchPatternGridHeaderComponent = SearchPatternAdminGridHeaderComponent;

  private showChangedAlert: boolean = true;
  public filterRegulationOptions:FilterOption[] = [];

  private config =< BaseGridManagementConfig>{
    entityName: 'search pattern',
    searchBoxPlaceHolder: 'name , category',
    initialSortField: 'sCat.parent.name;sCat.name;name',
    initialSortDir: TreeGridSortOrder.ASC
  };

  constructor(private searchPatterService: SearchPatternService,  routingService: RoutingService,  activatedRoute: ActivatedRoute, private dialogService: DialogService, router: Router,  utilService: UtilService,protected treeGridAdapter: TreeGridAdapterService) {
    super(router, activatedRoute, routingService, utilService, activatedRoute,treeGridAdapter);

    this.baseGridManagementConfig = this.config;
    this.dataService = searchPatterService;
  }

  onAfterLoadData(){
    this.loadPrimaryFilterData(); //TODO: do this after first load and when new regulationEvent is fired
  }

  protected registerDataServiceEvents() {
    super.registerDataServiceEvents();
    this.on(this.searchPatterService.searchPatternActiveChangedEvent$, () => {
      if (this.showChangedAlert) {
        this.dialogService.showAlert("Search pattern state changed", "Search patterns were activated.<br><br>In order for this change to take effect, full re-scan is needed.", AlertType.Info);
        this.showChangedAlert = false;
      }
    });
  }

  protected onAfterDataAdded(addedItem:TextSearchPatternDto){
    if (this.showChangedAlert && addedItem.active) { //this is not correct when new pattern is added in not active state. Also should change default active state to true
      this.dialogService.showAlert("New search pattern created", "A new search pattern was created.<br><br>In order for this change to take effect, full re-scan is needed.", AlertType.Info);
      this.showChangedAlert = false;
    }
  }


  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.searchPatterService.importData, validateUploadFn: this.searchPatterService.validateImportFile, component: "search patterns", fileType: ".csv", extractValidateUploadDataFn: this.searchPatterService.extractImportValidationData} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.loadData();
      }
    },(reason) => {});
  };

  openEditDialog = ()=> {
    let data = { data: {
        textSearchPattern : this.selectedItem
      } };
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditSearchPatternDialogComponent, data, modelParams);
  }


  openDeleteDialog = ()=> {
    this.searchPatterService.getPatternAccociatedFiles(this.selectedItem.id).subscribe((ans)=> {
      this.dialogService.openConfirm('Confirmation', 'You are about to delete search pattern named: \''+ this.selectedItem.name +'\' which has '+ans+' associated files. Continue?' ).then(result => {
        if (result) {
          this.searchPatterService.deleteSearchPattern(this.selectedItem.id).subscribe();
        }
      });
    });
  }

  openAddNewDialog = () => {
    let data = { data: {} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditSearchPatternDialogComponent, data, modelParams)
  }

  loadPrimaryFilterData = () => {

    this.searchPatterService.getRegulationsList().subscribe((regulationList) => {
      let params = _.cloneDeep(this.getQueryParams()) || {};
      this.filterRegulationOptions = this.utilService.convertFilterOptionListIntoCheckArray(params,regulationList.map((regItem:any)=> {
        let newObj:FilterOption = <FilterOption>{};
        newObj.value = regItem.id+'';
        newObj.displayText = regItem.name;
        return newObj;
      }),SettingPathParams.regulationFilter);

    });
  };


  onRegulationFilterChanged(filterList: any[]): void {
    this.routingService.updateParamList(this.activatedRoute,
      [SettingPathParams.page, SettingPathParams.regulationFilter], [1, this.utilService.getFilterString(filterList)]);
  }


}

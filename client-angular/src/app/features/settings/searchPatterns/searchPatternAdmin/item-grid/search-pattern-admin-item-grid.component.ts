import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from "../../../../../shared-ui/components/tree-grid/base/tree-grid-inner-item.base";
import {DateRangePointType} from "../../../../../common/settings/types/dateFilters.enums";
import {TextSearchPatternDto} from "../../../../../common/settings/types/searchPattern.dto";
import {MetadataService} from "../../../../../core/services/metadata.service";
import {SearchPatternService} from "../../../services/searchPattern.service";




@Component({
  selector: 'search-pattern-admin-item-grid',
  templateUrl: './search-pattern-admin-item-grid.component.html',
})
export class SearchPatternAdminItemGridComponent extends TreeGridInnerItemBaseComponent<TextSearchPatternDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;
  DateRangePointType = DateRangePointType;
  inSetActive:boolean = false;

  regulationStrs: string[]=[];



  constructor(private metadataService: MetadataService, private searchPatternService : SearchPatternService) {
    super();
  }

  setActive(isActive:boolean) {
    this.inSetActive = true;
    this.searchPatternService.setPatternActiveState(this.dataItem.item.id,isActive).subscribe((ansObject: TextSearchPatternDto)=>{
      this.dataItem.item.active = ansObject.active;
      this.inSetActive = false;
    }, (err)=> {
      this.inSetActive = false;
    });
  }


  ngOnInit(): void {
    this.regulationStrs = this.dataItem.item.regulationStr.split(',');
  }

}


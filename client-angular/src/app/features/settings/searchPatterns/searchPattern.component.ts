import {Component, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";

@Component({
  selector: 'search-pattern',
  templateUrl: './searchPattern.component.html',
  styleUrls: ['./searchPattern.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SearchPatternComponent extends BaseComponent {

  constructor() {
    super();
  }

}

import {EventEmitter, Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map} from 'rxjs/internal/operators';
import * as _ from 'lodash'


export enum RouteEvents {
  STATE_CHANGED
}


@Injectable()
export class SettingsRouteManager {
  dispatcher: EventEmitter<number> = new EventEmitter();
  latestQueryParams: any = null;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.getRoute().subscribe((route) => {
      this.latestQueryParams = _.cloneDeep(route.snapshot.queryParams);
      this.dispatcher.emit(RouteEvents.STATE_CHANGED);
    });
  }

  getEmitter() {
    return this.dispatcher;
  }

  getLatestQueryParams() {
    return this.latestQueryParams;
  }

  getRoute() {
    return this.router.events
      .pipe(filter(e => e instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
  }
}


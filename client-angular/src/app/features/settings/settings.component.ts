import {I18nService} from "@app/common/translation/services/i18n-service";
import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from "@angular/core";
import {V1Service} from "@app/doc-1/v1-frame/v1.service";
import {LoginService} from "@services/login.service";
import {TranslateService} from "@ngx-translate/core";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {settingTranslate} from "@app/translate/settings.translate";
import {ESystemRoleName} from "@users/model/system-role-name";
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig} from "@app/common/configuration/common-config";
import {AppConfig} from "@services/appConfig.service";


@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SettingsComponent implements OnInit {
  @ViewChild('sideMenu')
  sideMenu:ElementRef;


  isSearchPatternsUser = false;
  isDepartmentsUser = false;
  isMattersUser = false;
  isDateFiltersUser = false;
  isViewTagTypesUser = false;
  isViewDocTypeTreeUser = false;
  isGeneralAdminUser = false;
  isViewLdapConnections= false;
  isMngUserAndRolesUser = false;
  isScanUser = false;
  isPatternCategoriesUser = false;
  isRegulationsUser = false;
  isFiltersUser = false;
  isDashboardsUser = false;
  isTechSupportUser = false;
  scrollTopUpdated:boolean = false;

  constructor(
    private subHeaderService: SubHeaderService,
    private v1Service: V1Service,
    private loginService:LoginService,
    private i18nService : I18nService,
    private translateService : TranslateService,
    private urlAuthorizationService : UrlAuthorizationService,
    private appConfig : AppConfig) {
      this.translateService.setTranslation('en', settingTranslate,false);
      this.translateService.setTranslation('en', this.i18nService.getCommonTranslate(),true);
      this.translateService.setDefaultLang('en');
      this.translateService.use('en');
    }

  onSideMenuScroll($event) {
    if (this.scrollTopUpdated) {
      sessionStorage.setItem('settingScrollTop', this.sideMenu.nativeElement.scrollTop);
    }
  }

  ngOnInit() {

    this.subHeaderService.setSubHeader("Settings");
    this.isSearchPatternsUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ManageSearchPatterns]);
    this.isPatternCategoriesUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ActivatePatternCategories]);
    this.isRegulationsUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ActivateRegulations]);
    this.isDepartmentsUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ViewDepartmentTree]) && this.appConfig.isInstallationModeStandard();
    this.isMattersUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ViewDepartmentTree]) && this.appConfig.isInstallationModeLegal();
    this.isDateFiltersUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.None]);
    this.isViewTagTypesUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ViewTagTypes]);
    this.isViewDocTypeTreeUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ViewDocTypeTree]);
    this.isGeneralAdminUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.GeneralAdmin, ESystemRoleName.TechSupport]);
    this.isTechSupportUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    this.isScanUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngScanConfig, ESystemRoleName.RunScans]);
    this.isMngUserAndRolesUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserRoles, ESystemRoleName.MngRoles, ESystemRoleName.MngUsers]);
    this.isFiltersUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.None]);
    this.isDashboardsUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.None]);
    this.isViewLdapConnections = this.isGeneralAdminUser ||  this.isMngUserAndRolesUser;
    if (sessionStorage.getItem('settingScrollTop') != null) {
      setTimeout(()=> {
        this.sideMenu.nativeElement.scrollTop = Number(sessionStorage.getItem('settingScrollTop'));
        this.scrollTopUpdated = true;
      });
    }
  }

}


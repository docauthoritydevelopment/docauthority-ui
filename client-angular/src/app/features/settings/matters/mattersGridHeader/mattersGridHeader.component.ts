import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'matters-grid-header',
  templateUrl: './mattersGridHeader.component.html',
  styleUrls: ['./mattersGridHeader.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MattersGridHeaderComponent{
  constructor() {
  }
}

import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from '@angular/router';
import {DialogService} from "@app/shared-ui/components/modal-dialogs/dialog.service";
import {MattersService} from "@app/features/settings/matters/matters.service";
import {MatterDto} from "@app/features/settings/matters/matters.dto";

@Component({
  selector: 'matters-item-grid',
  templateUrl: './mattersItemGrid.component.html',
  styleUrls: ['./mattersItemGrid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MattersItemGridComponent extends TreeGridInnerItemBaseComponent<MatterDto> implements OnInit {

  @ViewChild('el') el: ElementRef;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private mattersService: MattersService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {

  }

  //---------------------------------------

  getMatterStyleId(matter) {
    return this.mattersService.getMatterStyleId(matter);
  }
}


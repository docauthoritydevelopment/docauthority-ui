import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {UtilService} from "@services/util.service";
import {FilterOption} from "@app/common/filters/types/filter-interfaces";
import {MatterDto} from "@app/features/settings/matters/matters.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'clone-depatment-dialog',
  templateUrl: './cloneMatterDialog.component.html',
  encapsulation: ViewEncapsulation.None
})

export class CloneMatterDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  matterItem: MatterDto = null;
  title: string = "";
  subMatters: any[];
  subMattersOptions: FilterOption[] = [];

  @ViewChild('matterGeneralForm') public matterGeneralForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal:NgbActiveModal,
              private utilService:UtilService,
              private i18nService:I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setMatter();
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  private setMatter() {
    if (_.has(this.data, 'matter')) {
      this.matterItem =  (<MatterDto>this.data)['matter'];
      this.title = this.i18nService.translateId('CLONE_DIALOG_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT'), name: this.matterItem.name});
      this.matterItem.name += " - copy";
      this.subMatters = (this.data)['subMatters'];

      this.subMattersOptions = this.utilService.convertFilterOptionListIntoCheckArray(null,this.subMatters.map((item:any)=> {
        let newObj:FilterOption = <FilterOption>{};
        newObj.value = item.name;
        newObj.displayText = item.name;
        return newObj;
      }),"");
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------


  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.matterItem.name && this.matterItem.name.length > 0 && this.matterItem.name.length < 101;
  }

  //---------------------------------------

  isValid = () => {
    if (!this.matterItem ) {
      return true;
    }
    return this.isValidName();
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  save = () => {
    if (this.isValid()) {
      let subMattersSelect:string[] = [];
      this.subMattersOptions.forEach(d => {
        if (d.selected) {
          subMattersSelect.push(d.value);
        }
      });
      let result:any  = { matter: this.matterItem, subMatters: subMattersSelect };
      this.activeModal.close(result);
    }
  };

  //---------------------------------------

  onSubMatterSelectionChanged(subMatters: any[]): void {
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

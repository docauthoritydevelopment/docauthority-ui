import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {MatterDto} from "@app/features/settings/matters/matters.dto";
import {MattersService} from "@app/features/settings/matters/matters.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {DefaultPagedReqParams} from "@services/urlParams.service";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'add-edit-depatment-dialog',
  templateUrl: './addEditMatterDialog.component.html',
  encapsulation: ViewEncapsulation.None
})

export class AddEditMatterDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  matterItem: MatterDto = null;
  title: string = "";
  params: DefaultPagedReqParams = null;
  mainMatters: any[] = [];
  selectedParent: MatterDto = null;
  onRefresh: any;
  isNew: boolean = false;

  @ViewChild('matterGeneralForm') public matterGeneralForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal:NgbActiveModal,
              private mattersService:MattersService,
              private dialogService:DialogService,
              private i18nService:I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setMatter();
    this.loadMatters();
    this.setModalEvents();
  }

  //---------------------------------------

  onParentChanged(matter:MatterDto) {
    this.selectedParent = matter;
    this.matterItem.parentId = matter.id;
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  private loadMatters() {
    this.mattersService.getTreeGridMatters(this.params,null).subscribe((matters) => {
      this.setMainMatters(matters);
    },(err) => {this.showError(err)});
  }

  //---------------------------------------

  private setMainMatters(matters) {
    this.mainMatters = [];
    this.mainMatters.push({ name: "---------- No Parent ----------", id: null});
    matters.items.forEach(d => {
      if (d.item.parentId == null) {
        this.mainMatters.push({ name: d.item.name, id: d.item.id });
      }
    });

    this.selectedParent = this.mainMatters.find(item => item.id === this.matterItem.parentId);
    if (!this.selectedParent) {
      this.matterItem.parentId = null;
    }
  }

  //---------------------------------------

  private setMatter() {
    if (_.has(this.data, 'matter')) {
      this.title = this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')});
      this.matterItem =  (<MatterDto>this.data)['matter'];
    }
    else if (_.has(this.data, 'parentId')) {
      this.isNew = true;
      this.title = this.i18nService.translateId('ADD_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')});
      this.matterItem = <MatterDto>{};
      this.matterItem.name = "";
      this.matterItem.parentId = (this.data)['parentId'];
      this.matterItem.description = "";
      this.params = (this.data)['params']
    }
  }

  //---------------------------------------

  private setModalEvents() {
    if (_.has(this.data, 'onRefresh')) {
      this.onRefresh = (this.data)['onRefresh'];
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------


  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.matterItem.name && this.matterItem.name.length > 0 && this.matterItem.name.length < 101;
  }

  //---------------------------------------

  isValid = () => {
    if (!this.matterItem ) {
      return true;
    }
    return this.isValidName();
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  save = (createAnother?:boolean) => {
    if (this.isValid()) {
      if (this.isNew) {
        this.mattersService.addNewMatter(this.matterItem).subscribe((addedMatter:MatterDto) => {
          let result:any  = { matter: addedMatter };
          if (createAnother) {
            this.onRefresh(result);
            this.matterItem.name = "";
            this.matterItem.description = "";
            this.loadMatters();
          }
          else {
            this.activeModal.close(result);
          }
        },(err) => {this.showError(err)});
      }
      else {
        this.mattersService.editMatter(this.matterItem).subscribe((editedMatter:MatterDto) => {
          let result:any  = { matter: editedMatter };
          this.activeModal.close(result);
        },(err) => {this.showError(err)});
      }
    }
  };

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message}), AlertType.Error, err);
  }
}

import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper, TreeGridData, TreeGridDataItem} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MatterDto} from "@app/features/settings/matters/matters.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {ServerExportService} from "@services/serverExport.service";
import {CommonConfig, ExportType, ServerUrls} from "@app/common/configuration/common-config";
import {RdbService} from "@services/rdb.service";

@Injectable()
export class MattersService {

  private max_matter_colors = 18;

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private serverExportService: ServerExportService,
    private rdbService: RdbService) {
  }

  //----------------------------------------
  // getTreeGridMatters
  //----------------------------------------

  getTreeGridMatters(params,selectedItemId:number): Observable<TreeGridData<MatterDto>> {
    if (params) {
      params = this.urlParamsService.fixedPagedParams(params,30);
      if (!params.sort) {
        params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
      }
    }
    else {
      params = { page: 1, sort: JSON.stringify([{dir: 'desc', field: 'id'}])};
    }
    return this.http.get(ServerUrls.MATTER_LIST, {params}).pipe(
      map((data: DTOPagingResults<MatterDto>) => {
        let ans: TreeGridData<MatterDto> = this.treeGridAdapter.adapt(
          data,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
        ans.items.forEach((dataItem:  TreeGridDataItem<any>)=> {
          dataItem.id = dataItem.item.id ;
        });
        return ans;
      }));
  }

  //----------------------------------------
  // Matters actions
  //----------------------------------------

  addNewMatter(newMatter) {
    return this.http.put(ServerUrls.MATTER, newMatter);
  }

  //----------------------------------------

  editMatter(matter) {
    return this.http.post(`${ServerUrls.MATTER}/${matter.id}`, matter);
  }

  //----------------------------------------

  cloneMatter(newMatter) {
    return this.http.put(ServerUrls.MATTER_CLONE, newMatter);
  }

  //----------------------------------------

  deleteMatter(matterId) {
    return this.http.delete(`${ServerUrls.MATTER}/${matterId}`);
  }

  //----------------------------------------

  exportData = (mattersNum, params) =>  {
    params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    this.serverExportService.prepareExportFile(this.i18nService.translateId('MATTERS.TITLE'),ExportType.MATTER_SETTINGS,params,{ itemsNumber : mattersNum },mattersNum > CommonConfig.minimum_items_to_open_export_popup);
  };

  //----------------------------------------

  /*exportSelectedData = (selectedDepartments): Observable<any> =>  {
    let selectedItems = [];
    selectedDepartments.forEach(department => {
      selectedItems.push(department.item);
    });
    return this.excelService.exportSelectedData(selectedItems, this.extractDepartmentData, 'departments');
  };*/

  //----------------------------------------

  extractImportValidationData = (item) => {
    const partialItem: any = {};
    partialItem['Msg Type'] = item.type;
    partialItem['Line number'] = item.data.rowDto.lineNumber;
    partialItem['Msg Description'] = item.data.message;
    partialItem['FULL_NAME'] = item.data.rowDto.fieldsValues[0];
    partialItem['DESCRIPTION'] = item.data.rowDto.fieldsValues[1];
    return partialItem;
  };

  //----------------------------------------

  validateImportFile = (formData:FormData, maxErrorRows:number) =>  {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.MATTER_VALIDATE_IMPORT,{maxErrorRows : maxErrorRows}), formData);
  };

  //----------------------------------------

  importData = (fileData:File, updateDuplicates:boolean) => {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.DEPARTMENT_IMPORT,{updateDuplicates : updateDuplicates}), fileData);
  };

  //----------------------------------------
  // getMatterStyleId
  //----------------------------------------

  getMatterStyleId(matter) {
    let colors = this.max_matter_colors;
    if(matter.parentId != null) {
      let result = matter.parentId%colors+1;
      return result;
    }
    return matter.id%colors+1;
  }

  //----------------------------------------

  getAssignedMatters(matterId) {
    return this.http.get(ServerUrls.MATTER_COUNT).pipe(map((data: DTOPagingResults<any>)=> {
      return _.filter(data.content, (c) => {
        return c.item.id === matterId;
      })[0];
    }));
  }
}

import * as _ from 'lodash'
import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from "@angular/router";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";
import {ITreeGridEvents, TreeGridData, TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {UtilService} from '@services/util.service';
import {AddEditMatterDialogComponent} from "@app/features/settings/matters/mattersDialog/addEditMatterDialog/addEditMatterDialog.component";
import {CloneMatterDialogComponent} from "@app/features/settings/matters/mattersDialog/cloneMatterDialog/cloneMatterDialog.component";
import {MattersService} from './matters.service';
import {MattersGridHeaderComponent} from "@app/features/settings/matters/mattersGridHeader/mattersGridHeader.component";
import {MattersItemGridComponent} from "@app/features/settings/matters/mattersItemGrid/mattersItemGrid.component";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AlertType} from '@shared-ui/components/modal-dialogs/types/alert-type.enum';
import {UserSettingsService} from "@services/user.settings.service";
import {BaseComponent} from "@core/base/baseComponent";
import {MatterDto} from "@app/features/settings/matters/matters.dto";
import {DefaultPagedReqParams} from '@services/urlParams.service';
import {ToolbarActionTypes} from "@app/types/display.type";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'matters',
  templateUrl: './matters.component.html',
  styleUrls: ['./matters.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MattersComponent extends BaseComponent implements OnInit, OnDestroy {

  matters:TreeGridData<MatterDto>;

  loading = true;
  params: DefaultPagedReqParams = null;
  actionItems:ButtonConfigDto[];
  exportActionItem:ButtonConfigDto;
  selectedItem:any;
  selectedItemId:number;
  totalElements:number = 0;
  filterCount: number = null;

  initialSortDirection:TreeGridSortOrder = null;
  initialSortFieldName:string = null;

  mattersGridHeaderComponent = MattersGridHeaderComponent;
  mattersItemGridComponent = MattersItemGridComponent;

  treeGridEvents:ITreeGridEvents<MatterDto>;
  localFilterFunction:any = null;

  public isMngMattersUser:boolean;

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;

  treeConfig: any = {
    showMarkCheckbox: true
  };

  //---------------------------------------

  constructor(
    private urlAuthorizationService : UrlAuthorizationService,
    private route: ActivatedRoute,
    private routingService: RoutingService,
    private mattersService: MattersService,
    private utilService: UtilService,
    private userSettingsService: UserSettingsService,
    private dialogService: DialogService,
    private i18nService: I18nService) {
    super();
  }

  //---------------------------------------
  // daOnInit
  //---------------------------------------

  daOnInit() {
    this.showLoader(true);
    this.initializeRoles();
    this.setSelectedItem();
    this.setTreeGridEvent();
    this.setParams();
    this.setActionItems();
    this.loadData();
  }
  initializeRoles() {
    this.isMngMattersUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngDepartmentConfig]);
  }
  //---------------------------------------
  // actions
  //---------------------------------------

  refreshGridAfterAdd = (data: any) => {
    let newMatter:MatterDto = data.matter;
    if(newMatter) {
      this.onItemSelected(newMatter.id, newMatter);
      this.loadData();
    }
  };

  //---------------------------------------

  openAddNewDialog = () => {
    let data = { data: { parentId: this.selectedItemId, params: this.params, onRefresh: this.refreshGridAfterAdd } };
    let modelParams = { windowClass: 'modal-md-window' };

    if (this.selectedItem.parentId) {
      data.data.parentId = this.selectedItem.parentId;
    }

    this.dialogService.openModalPopup(AddEditMatterDialogComponent, data, modelParams).result.then(this.refreshGridAfterAdd, (reason) => {});
  };

  //--------------------------------------

  openEditDialog = () => {
    let data = { data: { matter: _.cloneDeep(this.selectedItem)} };
    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(AddEditMatterDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        this.loadData();
      }
    },(reason) => {});
  };

  //--------------------------------------

  openCloneDialog = () => {
    let cloneMatter = <MatterDto>{};

    cloneMatter.name = this.selectedItem.name;
    cloneMatter.description = this.selectedItem.description;

    let subMatters:any[] = [];
    this.matters.items.find(dep => dep.id === this.selectedItem.id).children.forEach(d => {
      subMatters.push({ name: d.item.name });
    });

    let data = { data: { matter: cloneMatter, subMatters: subMatters } };
    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(CloneMatterDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        data.matter.childrenName = data.subMatters;
        this.mattersService.cloneMatter(data.matter).subscribe((addedMatter:MatterDto) => {
          if (addedMatter) {
            this.onItemSelected(addedMatter.id, addedMatter);
            this.loadData();
          }
        },(err) => {this.showError(err)});
      }
    },(reason) => {});
  };

  //---------------------------------------

  openDeleteDialog = () => {
    let msg = this.i18nService.translateId('MESSAGES.DELETE_CONFIRMATION', {component: this.i18nService.translateId('MATTERS.COMPONENT'), name: this.selectedItem.name});

    this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), msg).then(result => {
      if (result) {
        this.deleteMatter(this.selectedItem.id);
      }
    });
  };

  //---------------------------------------

  exportData = () => {
    this.mattersService.exportData(this.matters.totalElements, {sort: this.params.sort});
  };

  //---------------------------------------

  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.mattersService.importData, validateUploadFn: this.mattersService.validateImportFile, component: this.i18nService.translateId('MATTERS.COMPONENT_MULTI'), fileType: ".csv", extractValidateUploadDataFn: this.mattersService.extractImportValidationData} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.loadData();
      }
    },(reason) => {});
  };

  //---------------------------------------

  filterCountChange(newCount:number) {
    this.filterCount = newCount;
  }

  //---------------------------------------

  onSearchChanged = (newText:string) => {
    this.localFilterFunction = (newText ? (item)=> {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  //---------------------------------------

  public searchBoxConfig : SearchBoxConfigDto  ={
    searchFunc: this.onSearchChanged,
    searchTerm: '',
    placeholder: this.i18nService.translateId('SEARCHBOX_PLACEHOLDER', {field: 'name'}),
    notifyOnKeyup: false
  };

  //---------------------------------------
  // privates
  //---------------------------------------

  private setSelectedItem() {
    const queryParams = this.route.snapshot.queryParams;
    if (queryParams) {
      this.selectedItemId = parseInt(queryParams[RoutePathParams.findId] || queryParams[RoutePathParams.selectedItemId], 10);
      this.updateActionItems('selection');
    }
  }

  //---------------------------------------

  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;
    this.updateActionItems('selection');

    this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams,
      RoutePathParams.selectedItemId, this.selectedItemId);
  };

  //----------------------------------------

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<MatterDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  //----------------------------------------

  private onPageChanged = (pageNum) => {
    this.params.page = pageNum;
    this.loadData();
  };

  //----------------------------------------

  private onSortByChanged = (sortObj: any) => {
    this.params.sort = JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder).toLowerCase()}]);
    this.loadData();
  };

  //---------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //---------------------------------------

  private setParamsSortDefault() {
    this.params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
  }

  //---------------------------------------

  private setParamsPageDefault() {
    this.params.page = 1;
  }

  //---------------------------------------

  private setParams() {
    if (this.params) {
      if (!this.params.sort) {
        this.setParamsSortDefault();
      }
      else {
        let sortObj;
        if (typeof this.params.sort === "string") {
          sortObj = JSON.parse(this.params.sort);
        }
        else {
          sortObj = {field: this.params.sort.sortBy, dir: this.params.sort.sortOrder};
        }
        this.initialSortFieldName = sortObj[0].field;
        this.initialSortDirection = sortObj[0].dir;
      }

      if (!this.params.page) {
        this.setParamsPageDefault();
      }
    }
    else {
      this.params = {};
      this.setParamsSortDefault();
      this.setParamsPageDefault();
    }
  }

  //----------------------------------------

  private deleteMatter(matterId) {
    this.mattersService.deleteMatter(matterId).subscribe(()=> {
      this.loadData();
    },(err) => {this.showInfo(err)});
  }

  //----------------------------------------

  private setActionItems() {
    this.actionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: this.i18nService.translateId('ADD_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')}),
        name: this.i18nService.translateId('ADD'),
        actionFunc: this.openAddNewDialog,
        disabled: false,
        href: ToolbarActionTypes.ADD,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.CLONE,
        title: this.i18nService.translateId('CLONE_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')}),
        name: this.i18nService.translateId('CLONE'),
        actionFunc: this.openCloneDialog,
        disabled: !this.selectedItem || this.selectedItem.parentId != null,
        href: ToolbarActionTypes.CLONE,
        order: 1,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')}),
        name: this.i18nService.translateId('EDIT'),
        actionFunc: this.openEditDialog,
        disabled: !this.selectedItem,
        href: ToolbarActionTypes.EDIT,
        order: 2,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: this.i18nService.translateId('DELETE_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT')}),
        name: this.i18nService.translateId('DELETE'),
        actionFunc: this.openDeleteDialog,
        disabled: !this.selectedItem,
        href: ToolbarActionTypes.DELETE,
        order: 3,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.IMPORT,
        title: this.i18nService.translateId('IMPORT_TITLE', {component: this.i18nService.translateId('MATTERS.COMPONENT_MULTI')}),
        name: this.i18nService.translateId('IMPORT'),
        actionFunc: this.openUploadItemsDialog,
        disabled: false,
        href: ToolbarActionTypes.IMPORT,
        order: 4,
        btnType: 'flat'
      }
    ];

    let exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_OPTION_EXCEL'),
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: this.i18nService.translateId('EXPORT_TITLE'),
      disabled: false,
      href: ToolbarActionTypes.EXPORT,
      children: exportDropDownItems
    }
  }

  //---------------------------------------

  private updateActionItems(changeCase:string) {
    if (this.actionItems && changeCase === 'selection') {
      this.actionItems.find(item => item.type === ToolbarActionTypes.EDIT).disabled = !this.selectedItem;
      this.actionItems.find(item => item.type === ToolbarActionTypes.DELETE).disabled = !this.selectedItem;
      this.actionItems.find(item => item.type === ToolbarActionTypes.CLONE).disabled = !this.selectedItem || this.selectedItem.parentId != null;
    }
    else {
      this.setActionItems();
    }
  }

  //---------------------------------------

  private loadData(callback?) {
    this.mattersService.getTreeGridMatters(this.params,this.selectedItemId ).subscribe((matters) => {
      this.matters = matters;
      this.totalElements = matters.totalElements;
      if(callback) {
        callback();
      }
      this.showLoader(false);
    },(err) => {this.showError(err)});
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showInfo(info) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('NOTE'), `${info.error.message}`, AlertType.Info, info);
  }

  //---------------------------------------

  showError(err) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message ? err.error.message.toLowerCase() : ''}), AlertType.Error, err);
  }

  //---------------------------------------
  // destroy
  //---------------------------------------

  ngOnDestroy() {
  }
}

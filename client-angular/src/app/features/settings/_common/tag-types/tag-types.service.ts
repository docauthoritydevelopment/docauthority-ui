import * as _ from 'lodash';
import {Injectable, Input} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {UtilService} from '@services/util.service';
import {DTOFileTagTypeAndTags} from "@app/features/settings/_common/tag-types/tag-types.dto";
import {UserSettingsService} from "@services/user.settings.service";

@Injectable()
export class TagTypesService {

  _tagTypes;
  max_tags_colors = 18;
  recently_DocType_Items_Used = "recentlyDocTypeItemsUsed";

  @Input()
  get tagTypes() {
    return this._tagTypes;
  }

  //-----------------------------------

  constructor(
    private http: HttpClient,
    private userSettingsService: UserSettingsService,
    private utilService: UtilService) {
  }

  //-----------------------------------

  loadTagTypes(params) {
    return this.http.get(`/api/filetag/type/tree`, {params}).pipe(
      map((res:DTOFileTagTypeAndTags []) => {
        res = _.orderBy(res, function(item) { return item.fileTagTypeDto.name}, ['asc']);

        _.forEach(res, (item) => {
            item.fileTags = _.orderBy(item.fileTags,  ['name'],['asc']);
        });

        this._tagTypes = res;
        return res;
      })
    );
  }

  //-----------------------------------

  getTagTypeStyleId(tagId) {
    tagId = _.isFinite(tagId) ? tagId : 0;
    return tagId % this.max_tags_colors;
  }

  //-----------------------------------

  updateRecentlyUsedTagItems(tagTypeId, itemId, callback) {
    this.getAllRecentlyItemsUsed((tagTypes) => {
      if (!_.includes(tagTypes[tagTypeId], itemId)) {

        tagTypes[tagTypeId] = tagTypes[tagTypeId] || [];
        tagTypes[tagTypeId].push(itemId);

        if (tagTypes[tagTypeId].length > 4) {
          tagTypes[tagTypeId] = tagTypes[tagTypeId].slice(1);
        }

        this.userSettingsService.addToLocalStorageUser(this.recently_DocType_Items_Used, JSON.stringify(tagTypes)).subscribe((sTags:any) => {
          if (sTags == null ) {
            sTags = '{}';
          }
          let tags = JSON.parse(sTags );
          callback(tags[tagTypeId] || []);
        });
      }
      if (callback) {
        callback(tagTypes);
      }
    });
  };

  //-----------------------------------

  getAllRecentlyItemsUsed (callback) {
    this.userSettingsService.getFromLocalStorageUser(this.recently_DocType_Items_Used).subscribe((sTags) => {
      let tags = JSON.parse(sTags || '{}');
      callback(tags || {});
    });
  };

  //-----------------------------------


  getRecentlyItemsUsed (tagTypeId, callback) {
    this.getAllRecentlyItemsUsed((tagTypes) => {
      if(callback) {
        callback(tagTypes[tagTypeId] || {});
      }
    });
  };

  //-----------------------------------

  addTagItem(tagItem){
    let params = `name=${encodeURIComponent(tagItem.name)}`;

    if(!_.isUndefined(tagItem.description) && !_.isNull(tagItem.description)) {
      params += `&description=${encodeURIComponent(tagItem.description)}`;
    }
    return this.http.put(`api/filetag/type/${tagItem.fileTagTypeId}/new?${params}`, tagItem);
  }
}

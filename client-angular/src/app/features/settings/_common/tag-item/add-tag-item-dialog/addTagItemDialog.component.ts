import * as _ from 'lodash';
import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgForm} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {DialogBaseComponent} from "@shared-ui/components/modal-dialogs/base/dialog-component.base";
import {DTOFileTag, DTOFileTagTypeAndTags} from "@app/features/settings/_common/tag-types/tag-types.dto";
import {TagTypesService} from "@app/features/settings/_common/tag-types/tag-types.service";

@Component({
  selector: 'add-tagItem-dialog',
  templateUrl: './addTagItemDialog.component.html',
  styleUrls: ['./_addTagItemDialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddTagItemDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {

  title;
  tagTypeId;
  inputText;
  description;
  tagTypeName;
  tagTypeStyleId;

  @ViewChild('tagItemForm') public tagItemForm: NgForm = null;
  @ViewChild('tagItemName') private tagItemName: ElementRef;

  constructor(
    private activeModal: NgbActiveModal,
    private tagTypesService: TagTypesService) {
    super();
  }

  //------------------------------------

  ngOnInit() {
    this.title = 'Add tag';

    let tagType = _.filter(this.tagTypesService.tagTypes, (tagType: DTOFileTagTypeAndTags) => {
      return tagType.fileTagTypeDto.id === this.data.tagTypeId
    })[0];

    if(tagType) {
      this.tagTypeId = tagType.fileTagTypeDto.id;
      this.tagTypeName = tagType.fileTagTypeDto.name;
      this.tagTypeStyleId = this.tagTypesService.getTagTypeStyleId(tagType.fileTagTypeDto.id);
    }
  }

  //------------------------------------

  ngAfterViewInit(): void {
    this.tagItemName.nativeElement.focus();
  }

  //------------------------------------

  save(e, createAnother?:boolean){

    if(this.data.stopPropagation) {
      e.stopPropagation()
    }

    if(this.tagItemForm.valid) {
      let tagItem = <any> {};

      tagItem.fileTagTypeId = this.tagTypeId;
      tagItem.name = this.inputText.replace(new RegExp("[;,:\"']", "gm"), " ").trim();
      tagItem.description = this.description?( this.description.replace(new RegExp("[']", "gm"), "\"")):null;

      this.activeModal.close({
        tagItem: tagItem,
        createAnother:createAnother
      });
    }
  };

  //------------------------------------

  cancel(e){
    if(this.data.stopPropagation) {
      e.stopPropagation();
    }

    this.activeModal.close(null);
  };

  ngOnDestroy() {
  }
}

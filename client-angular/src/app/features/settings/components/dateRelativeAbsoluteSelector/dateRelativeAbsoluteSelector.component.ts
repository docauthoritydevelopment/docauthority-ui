import {Component, Input, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {DateRangePointType, TimePeriod} from "@app/common/settings/types/dateFilters.enums";
import {CommonConfig} from "@app/common/configuration/common-config";
import {DateRangeItemDto, DateRangePointDto} from "@app/common/settings/types/dateFilters.dto";

@Component({
  selector: 'date-relative-absolute-selector',
  templateUrl: './dateRelativeAbsoluteSelector.component.html',
  styleUrls: ['./dateRelativeAbsoluteSelector.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DateRelativeAbsoluteSelectorComponent extends BaseComponent {

  relativeTimePeriodOptions = [TimePeriod.YEARS, TimePeriod.MONTHS, TimePeriod.WEEKS, TimePeriod.DAYS, TimePeriod.HOURS];
  dateFormat = CommonConfig.inputDateFormat;
  titleTimeFormat = CommonConfig.dateFormat;
  absoluteDate = null;
  editTitleMode: boolean = false;
  newTitle: string;
  absoluteopened: boolean = false;


  @Input()
  dateRangeItem: DateRangeItemDto;

  @Input()
  itemTitle : string;

  @Input()
  dateRangePoint: DateRangePointDto;

  @Input()
  disableTitleEditMode: boolean;

  @Output()
  onDeletePoint = new EventEmitter();

  @Output()
  onPointChange = new EventEmitter();

  @Output()
  onValidityChange = new EventEmitter();

  @Output()
  onPointTitleChange = new EventEmitter();


  isValid:boolean = true;
  DateRangePointType = DateRangePointType;

  msie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;




  daOnInit() {
    if (this.dateRangePoint) {
      this.absoluteDate = null ;
      if (this.dateRangePoint.type == DateRangePointType.ABSOLUTE) {
        let tmpDate = new Date((<DateRangePointDto>this.dateRangePoint).absoluteTime);
        this.absoluteDate = {
          year : tmpDate.getUTCFullYear(),
          month : tmpDate.getUTCMonth()+1,
          day : tmpDate.getUTCDate()
        }
      }
      if (this.dateRangePoint.type == DateRangePointType.ABSOLUTE) {
        this.validateAbsoluteDates();
      }
      else {
        this.validateRelativeDates();
      }
    }
    else {
      var point = new DateRangePointDto();
      point.type = DateRangePointType.RELATIVE;
      point.relativePeriod = TimePeriod.MONTHS;
      point.relativePeriodAmount = 1;
      this.dateRangePoint = point;
      this.changeValid(false);
    }
  }


  changeRelativePeriod = (period: TimePeriod, relativePeriodAmount) => {
    (<DateRangePointDto> this.dateRangePoint).relativePeriod = period;
    //    (<Entities.DTODateRangePoint> $scope.dateRangePoint).relativePeriodAmount =relativePeriodAmount;
    this.validateRelativeDates();
    this.onPointChange.emit(this.dateRangeItem);
  }


  relativeStepChanged = () => {
    this.validateRelativeDates();
    this.onPointChange.emit(this.dateRangeItem);
  }

  validateRelativeDates= ()=>
  {
    this.changeValid((<DateRangePointDto> this.dateRangePoint).relativePeriod != null && (<DateRangePointDto> this.dateRangePoint).relativePeriodAmount != null);
  }

  validateAbsoluteDates = ()=> {
    this.changeValid(this.absoluteDate && (typeof this.absoluteDate === 'object'));
  }

  changeValid(newValidity:boolean) {
    if (this.isValid != newValidity) {
      this.isValid = newValidity;
      this.onValidityChange.emit(this.isValid)
    }
  }

  absoluteDateChanged = () => {
    if (this.absoluteDate) {
      var startTimeAsUtc = new Date(Date.UTC(this.absoluteDate.year, this.absoluteDate.month - 1, this.absoluteDate.day, 0, 0, 0));
      (<DateRangePointDto>this.dateRangePoint).absoluteTime = startTimeAsUtc.getTime();

    }
    else {
      (<DateRangePointDto>this.dateRangePoint).absoluteTime = null;
    }
    this.validateAbsoluteDates()
    this.onPointChange.emit(this.dateRangeItem);
  }

  toggleIsAbsolute = () => {
    if (this.dateRangePoint.type == DateRangePointType.ABSOLUTE) {
      (<DateRangePointDto>this.dateRangePoint).type = DateRangePointType.RELATIVE;
      this.validateRelativeDates();
    }
    else {
      (<DateRangePointDto>this.dateRangePoint).type = DateRangePointType.ABSOLUTE;
      this.validateAbsoluteDates();
    }
    this.onPointChange.emit(this.dateRangeItem);
  }


  setEditTitleMode = (value: boolean) => {
    if (value) {
      this.newTitle = this.itemTitle;
    }
    this.editTitleMode = value;
  }


  cancelEditTitle = () => {
    this.newTitle = null;
    this.setEditTitleMode(false);
  }

  editTitle = () => {
    this.itemTitle = this.newTitle;
    this.onPointTitleChange.emit([this.dateRangeItem, this.newTitle]);
    this.setEditTitleMode(false);
  }

  openAbsolute = () => {
    this.absoluteopened = true;
  };

  constructor() {
    super();
  }

}


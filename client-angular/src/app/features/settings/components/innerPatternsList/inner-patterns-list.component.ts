import {Component, Input, ViewEncapsulation} from '@angular/core';
import {TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {MetadataService} from "@services/metadata.service";



@Component({
  selector: 'inner-patterns-list',
  templateUrl: './inner-patterns-list.component.html',
  styleUrls: ['./inner-patterns-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InnerPatternsListComponent  {
  _patternList: TextSearchPatternDto[] = [];

  @Input()
  set patternList(newList: TextSearchPatternDto[]) {
    this._patternList = newList;
  }

  get patternList(): TextSearchPatternDto[] {
    return this._patternList;
  }

  ngOnInit() {
  }


  constructor(private metadataService: MetadataService) {

  }
}


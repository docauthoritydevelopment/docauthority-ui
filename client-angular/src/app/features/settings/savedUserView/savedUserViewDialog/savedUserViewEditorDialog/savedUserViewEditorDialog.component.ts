import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {DialogBaseComponent} from "@shared-ui/components/modal-dialogs/base/dialog-component.base";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {UtilService} from "@services/util.service";
import {ToolbarActionTypes} from "@app/types/display.type";
import {forkJoin} from "rxjs";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";

@Component({
  selector: 'saved-user-view-editor-dialog',
  templateUrl: './savedUserViewEditorDialog.component.html',
  styleUrls: ['./savedUserViewEditorDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SavedUserViewEditorDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  userViewItem: UserViewDataDto = null;
  action: string = "";
  isScopeEditor = false;
  title: string = "";
  globalFilter: string = "true";
  showRemoveContainerDiv:boolean = false;
  removeContainer:string = "false";

  @ViewChild('userViewGeneralForm') public userViewGeneralForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private savedUserViewService: SavedUserViewService, private activeModal: NgbActiveModal, private utilService: UtilService, private i18nService: I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setUserView();
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  save = () => {
    if (this.isValid()) {
      if (this.isScopeEditor) {
        this.userViewItem.globalFilter = (this.globalFilter === "true");
      }
      let queriesArray = [];
      if (this.action == ToolbarActionTypes.EDIT || this.action == ToolbarActionTypes.SCOPE) {
        queriesArray.push(this.savedUserViewService.editSavedUserViewData(this.userViewItem));
      }
      else if (this.action == ToolbarActionTypes.CLONE) {
        queriesArray.push(this.savedUserViewService.cloneSavedUserViewData(this.userViewItem.id, this.userViewItem.name, this.removeContainer == "true"));
      }
      forkJoin(queriesArray).subscribe(([addedUserView]) => {
        let result:any  = { userView: addedUserView };
        this.activeModal.close(result);
      }, (err) => {});
    }
  };

  //---------------------------------------

  private setUserView() {
    this.showRemoveContainerDiv = this.data['canRemoveContainer'];
    if (_.has(this.data, 'userView')) {
      this.userViewItem = (<UserViewDataDto>this.data)['userView'];
    }
    if (_.has(this.data, 'action')) {
      this.action = (<string>this.data)['action'];
      let component = this.getComponent();
      if (this.action == ToolbarActionTypes.EDIT) {
        this.title = this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId(component)});
      }
      else if (this.action == ToolbarActionTypes.SCOPE) {
        this.title = this.i18nService.translateId('SCOPE');
        this.isScopeEditor = true;
        this.globalFilter = this.userViewItem.globalFilter ? "true" : "false";
      }
      else if (this.action == ToolbarActionTypes.CLONE) {
        this.title = this.i18nService.translateId('CLONE_DIALOG_TITLE', {component: this.i18nService.translateId(component), name: this.userViewItem.name});
        this.userViewItem.name += " - copy";
        this.userViewItem.globalFilter = false;
      }
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------


  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.userViewItem.name && this.userViewItem.name.length > 0;
  }

  //---------------------------------------

  isValid = () => {
    if (!this.userViewItem ) {
      return true;
    }
    return this.isValidName();
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  private getComponent() {
    switch(this.userViewItem.displayType) {
      case EFilterDisplayType.DISCOVER:
        return 'USER_VIEW_DATA.SAVED_FILTERS.COMPONENT_BASIC';
      case EFilterDisplayType.CHART:
        return 'USER_VIEW_DATA.SAVED_CHARTS.COMPONENT_BASIC';
      case EFilterDisplayType.DISCOVER_VIEW:
        return 'USER_VIEW_DATA.SAVED_VIEWS.COMPONENT_BASIC';
      case EFilterDisplayType.DASHBOARD_WIDGET:
        return 'USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT_BASIC';
      case EFilterDisplayType.DASHBOARD:
        return 'USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT_BASIC';
      default:
        return '';
    }
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

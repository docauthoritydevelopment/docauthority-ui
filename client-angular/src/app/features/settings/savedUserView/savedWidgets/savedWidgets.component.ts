import * as _ from 'lodash'
import {forkJoin} from "rxjs";
import {Component, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BaseComponent} from "@core/base/baseComponent";
import {ITreeGridEvents,TreeGridData,TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ToolbarActionTypes} from "@app/types/display.type";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {SavedUserViewEditorDialogComponent} from "@app/features/settings/savedUserView/savedUserViewDialog/savedUserViewEditorDialog/savedUserViewEditorDialog.component";
import {LoginService} from "@services/login.service";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {CommonConfigurationService} from "@app/common/configuration/services/common-configuration.service";
import {SavedWidgetItemGridComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgetItemGrid/savedWidgetItemGrid.component";
import {SavedWidgetGridHeaderComponent} from "@app/features/settings/savedUserView/savedWidgets/savedWidgetGridHeader/savedWidgetGridHeader.component";
import {SavedChart} from "@app/common/saved-user-view/types/saved-chart";
import {ViewChartWidgetDialogComponent} from "@app/common/dashboard/popups/viewChartWidgetDialog/viewChartWidgetDialog.component";

@Component({
  selector: 'saved-widgets',
  templateUrl: './savedWidgets.component.html',
  styleUrls: ['./savedWidgets.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SavedWidgetsComponent extends BaseComponent {

  private UNRELATED_DASHBOARD_ITEM = {
    name : 'Widgets library',
    id : SavedChart.DETACHED_DASHBOARD_ID
  };

  public loading:boolean = false;
  public savedWidgetsData:TreeGridData<UserViewDataDto> = null;

  SavedWidgetGridHeaderComponent = SavedWidgetGridHeaderComponent;
  SavedWidgetItemGridComponent= SavedWidgetItemGridComponent;

  public treeGridEvents:ITreeGridEvents<UserViewDataDto>;
  private selectedItemId:number;
  private selectedItem:UserViewDataDto;
  public totalElements:number = 0;
  public settingsActionItems:ButtonConfigDto[];
  public exportActionItem:ButtonConfigDto;
  public initialSortDirection:TreeGridSortOrder = null;
  public initialSortFieldName:string = null;

  isSupportMngUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
  isSupportAssignPublicUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.AssignPublicUserViewData]);
  username:string = this.loginService.getCurrentUser().name;
  dashboardList = [];
  selectedDashboard:any  = this.UNRELATED_DASHBOARD_ITEM;
  localFilterFunction: (dataItem:UserViewDataDto) => boolean = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private urlAuthorizationService: UrlAuthorizationService,
              private loginService: LoginService,
              private savedUserViewService: SavedUserViewService,
              private routingService: RoutingService,
              private activatedRoute: ActivatedRoute,
              private dialogService: DialogService,
              private commonConfigurationService : CommonConfigurationService,
              private router: Router,
              private i18nService: I18nService) {
    super(router, activatedRoute);
  }

  //---------------------------------------
  // daOnInit
  //---------------------------------------

  daOnInit() {
    this.on(this.savedUserViewService.refreshUserViewListEvent$, () => {
      this.loadData();
    });

    this.setActionItems();
    this.setTreeGridEvent();
  }

  //---------------------------------------
  // routeChanged
  //---------------------------------------

  routeChanged() {
    this.loadData();
  }


  //---------------------------------------
  // actions
  //---------------------------------------

  loadData = () => {
    this.showLoader(true);
    let params = _.cloneDeep(this.getQueryParams()) || {};
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    this.initTableSort(params);
    let queriesArray = [];
    if (params == null || params[SettingPathParams.containerId] == null || params[SettingPathParams.containerId] == SavedChart.DETACHED_DASHBOARD_ID) {
      queriesArray.push(this.savedUserViewService.getDetachedSavedUserViewDataGrid(params, this.selectedItemId, EFilterDisplayType.DASHBOARD_WIDGET, !this.isSupportMngUserViewData));
    }
    else {
      queriesArray.push(this.savedUserViewService.getSavedUserViewDataGrid(params, this.selectedItemId, EFilterDisplayType.DASHBOARD_WIDGET, !this.isSupportMngUserViewData && !this.isSupportAssignPublicUserViewData));
    }
    queriesArray.push(this.savedUserViewService.getSavedUserViewData({}, EFilterDisplayType.DASHBOARD, !this.isSupportMngUserViewData));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([savedWidgetsData, dashboardsList]) => {
      this.dashboardList = dashboardsList;
      this.dashboardList.unshift(this.UNRELATED_DASHBOARD_ITEM);
      let filteredDashboards = this.dashboardList.filter((dashboardItem) => dashboardItem.id == params[SettingPathParams.containerId]);
      this.selectedDashboard = filteredDashboards && filteredDashboards.length > 0 ? filteredDashboards[0] : this.dashboardList[0];
      this.savedWidgetsData = savedWidgetsData;
      this.totalElements = savedWidgetsData.totalElements;
      this.setActionItems();
      this.showLoader(false);
    }));
  };

  //---------------------------------------

  openDeleteDialog = () => {
    let msg = this.i18nService.translateId('MESSAGES.DELETE_CONFIRMATION', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT'), name: this.selectedItem.name});

    this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), msg).then(result => {
      if (result) {
        this.deleteSavedWidget(this.selectedItem.id);
      }
    });
  };

  //---------------------------------------

  openDialog(action:string) {
    let cloneWidget = _.cloneDeep(this.selectedItem) || <UserViewDataDto>{};
    let data = { data: {userView: cloneWidget, action: action, canRemoveContainer: action  == ToolbarActionTypes.CLONE && this.selectedDashboard != this.UNRELATED_DASHBOARD_ITEM} };

    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(SavedUserViewEditorDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        switch(action) {
          case ToolbarActionTypes.SCOPE:
            this.loadData();
            break;
          case ToolbarActionTypes.CLONE:
            this.routingService.updateParam(this.activatedRoute, RoutePathParams.selectedItemId, data.userView.id);
            break;
        }
      }
    },(reason) => {});
  }

  //---------------------------------------

  createNewChart = ()=>  {
    let data = { data: {globalFilter: false, containerId: this.selectedDashboard.id} };

    let modelParams = { windowClass: 'modal-xl-window' };

    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams).result.then((createdUserViewDataDto: UserViewDataDto) => {
      if (createdUserViewDataDto) {
        this.routingService.updateParamList(this.activatedRoute, [RoutePathParams.selectedItemId, RoutePathParams.containerId], [createdUserViewDataDto.id, createdUserViewDataDto.containerId ? createdUserViewDataDto.containerId : -1]);
      }
    },(reason) => {});
  };

  //---------------------------------------


  openEditDialog = () => {
    let cloneWidget = _.cloneDeep(this.selectedItem) || <UserViewDataDto>{};
    let data = { data: {userView: cloneWidget, filterList: null} };

    let modelParams = { windowClass: 'modal-xl-window' };

    this.dialogService.openModalPopup(AddEditChartWidgetDialogComponent, data, modelParams).result.then((updatedUserViewDataDto: UserViewDataDto) => {
      if (updatedUserViewDataDto) {
        this.loadData();
      }
    },(reason) => {});
  };

  //---------------------------------------

  openSetScopeDialog = () => {
    this.openDialog(ToolbarActionTypes.SCOPE);
  };

  //---------------------------------------

  openCloneDialog = () => {
    this.openDialog(ToolbarActionTypes.CLONE);
  };

  //---------------------------------------

  exportData = () => {
    let params = _.cloneDeep(this.getQueryParams()) || {};
    this.savedUserViewService.exportData(this.savedWidgetsData.totalElements, params, EFilterDisplayType.DASHBOARD_WIDGET,this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.TITLE'), !this.isSupportMngUserViewData);
  };

  //---------------------------------------

  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.savedUserViewService.importData, validateUploadFn: this.savedUserViewService.validateImportFile, component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT_MULTI'), fileType: ".csv", extractValidateUploadDataFn: this.savedUserViewService.extractImportValidationData, allowDuplicates: false} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.commonConfigurationService.refreshTopMenu();
        this.loadData();
      }
    },() => {});
  };

  //---------------------------------------

  onSearchChanged = (newText:string) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.searchText,SettingPathParams.page], [newText,1]);
    this.localFilterFunction = (newText ? (item) => {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  //---------------------------------------

  public searchBoxConfig:SearchBoxConfigDto = {
    searchFunc: this.onSearchChanged,
    searchTerm: '',
    placeholder: this.i18nService.translateId('SEARCHBOX_PLACEHOLDER', {field: 'name'}),
    notifyOnKeyup: false
  };

  //---------------------------------------
  // privates
  //---------------------------------------

  private initTableSort(params:any) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    }
    let sortObject=JSON.parse(params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  //----------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //----------------------------------------

  private setActionItems = ()=> {
    let disableEditDelete = !this.selectedItem || !this.selectedItem.owner ||
      (this.selectedItem.owner.name != this.username && !this.isSupportMngUserViewData);
    let isDisableAdd = !this.isSupportMngUserViewData && (this.selectedDashboard != this.UNRELATED_DASHBOARD_ITEM ) && (this.username !=   this.selectedDashboard.owner.name);
    let disableSetScope = !this.selectedItem || !this.isSupportAssignPublicUserViewData || (this.selectedItem.containerId != null);

    this.settingsActionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: this.i18nService.translateId('ADD_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT')}),
        name: this.i18nService.translateId('ADD'),
        actionFunc: this.createNewChart,
        disabled: isDisableAdd,
        href: ToolbarActionTypes.ADD,
        order: 1,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.CLONE,
        title: this.i18nService.translateId('CLONE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT')}),
        name: this.i18nService.translateId('CLONE'),
        actionFunc: this.openCloneDialog,
        disabled: !this.selectedItem || !this.selectedItem.owner,
        href: ToolbarActionTypes.CLONE,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT')}),
        name: this.i18nService.translateId('EDIT'),
        actionFunc: this.openEditDialog,
        disabled: disableEditDelete,
        href: ToolbarActionTypes.EDIT,
        order: 1,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('SCOPE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT')}),
        name: this.i18nService.translateId('SCOPE'),
        actionFunc: this.openSetScopeDialog,
        disabled: disableSetScope,
        href: ToolbarActionTypes.EDIT,
        order: 2,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: this.i18nService.translateId('DELETE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT')}),
        name: this.i18nService.translateId('DELETE'),
        actionFunc: this.openDeleteDialog,
        disabled: disableEditDelete,
        href: ToolbarActionTypes.DELETE,
        order: 3,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.IMPORT,
        title: this.i18nService.translateId('IMPORT_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_WIDGETS.COMPONENT_MULTI')}),
        name: this.i18nService.translateId('IMPORT'),
        actionFunc: this.openUploadItemsDialog,
        disabled: false,
        href: ToolbarActionTypes.IMPORT,
        order: 4,
        btnType: 'flat'
      }
    ];

    let exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_OPTION_EXCEL'),
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: this.i18nService.translateId('EXPORT_TITLE'),
      disabled: false,
      href: ToolbarActionTypes.EXPORT,
      children: exportDropDownItems
    };
  }

  //----------------------------------------

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<UserViewDataDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  //----------------------------------------

  private onItemSelected = (itemId, item) => {
    if (this.savedWidgetsData) {
      if (_.isUndefined(this.selectedItem) || !this.selectedItem || this.selectedItemId != itemId) {
        this.selectedItemId = itemId;
        this.routingService.replaceQueryParamsWithoutReloading(this.getQueryParams(), RoutePathParams.selectedItemId, this.selectedItemId);
        this.updateLatestQueryParams(RoutePathParams.selectedItemId, this.selectedItemId);
      }
      this.selectedItem = item;
      this.setActionItems();
      let params = _.cloneDeep(this.getQueryParams()) || {};
      if (params && params[RoutePathParams.view] == "true" && this.selectedItem) {
        let data = {data: {userView: this.selectedItem}};
        let modelParams = {windowClass: 'modal-xl-window'};
        this.dialogService.openModalPopup(ViewChartWidgetDialogComponent, data, modelParams).result.then((res) => {
          this.updateLatestQueryParams(RoutePathParams.view, null);
        },(reason) => {});
      }
    }
  };

  //----------------------------------------

  private onPageChanged = (pageNum:number) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.page],[pageNum]);
  };

  //----------------------------------------

  private onSortByChanged = (sortObj:any) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.sort],[JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder)}])]);
  };

  //----------------------------------------

  private deleteSavedWidget(widgetId) {
    this.savedUserViewService.deleteSavedUserViewData(widgetId).subscribe(()=> {
      this.selectedItem = null;
      this.setActionItems();
      this.routingService.updateParamList(this.activatedRoute,[RoutePathParams.selectedItemId],[null]);
    });
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showInfo(info) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('NOTE'), `${info.error.message}`, AlertType.Info, info);
  }

  //---------------------------------------

  showError(err) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message}), AlertType.Error, err);
  }


  dashboardChanged = (dashboardItem:any)=>  {
    this.selectedDashboard = dashboardItem;
    this.savedWidgetsData = null;
    this.selectedItem = null;
    this.selectedItemId = null;
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.page, SettingPathParams.containerId, RoutePathParams.selectedItemId],[1, dashboardItem.id, '']);
  }
}

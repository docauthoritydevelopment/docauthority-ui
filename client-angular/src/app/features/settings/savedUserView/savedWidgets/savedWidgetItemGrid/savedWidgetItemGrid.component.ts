import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {filterDisplayTypeHref,filterDisplayTypeIcon,UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {LoginService} from "@services/login.service";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {SavedChart} from "@app/common/saved-user-view/types/saved-chart";
import {ViewChartWidgetDialogComponent} from "@app/common/dashboard/popups/viewChartWidgetDialog/viewChartWidgetDialog.component";

@Component({
  selector: 'saved-widget-item-grid',
  templateUrl: './savedWidgetItemGrid.component.html',
  styleUrls: ['./savedWidgetItemGrid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SavedWidgetItemGridComponent extends TreeGridInnerItemBaseComponent<UserViewDataDto> implements OnInit {

  userViewIcon:string = "";
  queryParams:any = {};
  title:string = "";
  isSupportMngUserViewData:boolean;
  isSupportAssignPublicUserViewData:boolean;
  username:string;
  isPublic:boolean;
  scopeText:string;

  @ViewChild('el') el: ElementRef;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private loginService:LoginService,
              private savedUserViewService: SavedUserViewService,
              private urlAuthorizationService: UrlAuthorizationService,
              private dashboardService: DashboardService,
              private dialogService: DialogService) {
    super();
    this.isSupportMngUserViewData  = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
    this.isSupportAssignPublicUserViewData = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.AssignPublicUserViewData]);
    this.username  = this.loginService.getCurrentUser().name;
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------


  itemClicked() {
      let userViewData: UserViewDataDto = this.dataItem.item;
      let data = {data: {userView: userViewData}};
      let modelParams = {windowClass: 'modal-xl-window'};
      this.dialogService.openModalPopup(ViewChartWidgetDialogComponent, data, modelParams).result.then((res) => {
        this.savedUserViewService.refreshUserViewDataList();
      },(reason) => {});
  }

  ngOnInit(): void {
    this.userViewIcon = filterDisplayTypeIcon[this.dataItem.item.displayType];
    this.queryParams = {};
    this.title = "";

    let parentDashboard: UserViewDataDto = (<UserViewDataDto>this.hostComponentParams.dashboard);
    if (parentDashboard.id == SavedChart.DETACHED_DASHBOARD_ID) {
      this.isPublic = this.dataItem.item.globalFilter;
      this.scopeText =  this.isPublic ? 'Public' :'Private'
    }
    else {
      this.isPublic = parentDashboard.globalFilter;
      this.scopeText =  'Inherited (' + (this.isPublic ? 'Public' :'Private')+  ')';
    }

  }
}


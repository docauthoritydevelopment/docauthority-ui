import * as _ from 'lodash'
import {forkJoin} from "rxjs";
import {Component, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BaseComponent} from "@core/base/baseComponent";
import {ITreeGridEvents,TreeGridData,TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ToolbarActionTypes} from "@app/types/display.type";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {EFilterDisplayType, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {SavedUserViewItemGridComponent} from "@app/features/settings/savedUserView/savedUserViewItemGrid/savedUserViewItemGrid.component";
import {SavedUserViewGridHeaderComponent} from "@app/features/settings/savedUserView/savedUserViewGridHeader/savedUserViewGridHeader.component";
import {SavedUserViewEditorDialogComponent} from "@app/features/settings/savedUserView/savedUserViewDialog/savedUserViewEditorDialog/savedUserViewEditorDialog.component";
import {LoginService} from "@services/login.service";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";

@Component({
  selector: 'saved-dashboards',
  templateUrl: './savedDashboards.component.html',
  styleUrls: ['./savedDashboards.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SavedDashboardsComponent extends BaseComponent {

  public loading:boolean = false;
  public savedDashboardsData:TreeGridData<UserViewDataDto> = null;

  SavedUserViewItemGridComponent = SavedUserViewItemGridComponent;
  SavedUserViewGridHeaderComponent = SavedUserViewGridHeaderComponent;

  public treeGridEvents:ITreeGridEvents<UserViewDataDto>;
  private selectedItemId:number;
  private selectedItem:UserViewDataDto;
  public totalElements:number = 0;
  public settingsActionItems:ButtonConfigDto[];
  public exportActionItem:ButtonConfigDto;
  public initialSortDirection:TreeGridSortOrder = null;
  public initialSortFieldName:string = null;

  isSupportMngUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
  isSupportAssignPublicUserViewData:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.AssignPublicUserViewData]);
  username:string = this.loginService.getCurrentUser().name;

  localFilterFunction: (dataItem:UserViewDataDto) => boolean = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private urlAuthorizationService: UrlAuthorizationService,
              private loginService: LoginService,
              private savedUserViewService: SavedUserViewService,
              private routingService: RoutingService,
              private activatedRoute: ActivatedRoute,
              private dialogService: DialogService,
              private router: Router,
              private i18nService: I18nService) {
    super(router, activatedRoute);
  }

  //---------------------------------------
  // daOnInit
  //---------------------------------------

  daOnInit() {
    this.setActionItems();
    this.setTreeGridEvent();
  }

  //---------------------------------------
  // routeChanged
  //---------------------------------------

  routeChanged() {
    this.loadData();
  }

  //---------------------------------------
  // actions
  //---------------------------------------

  loadData = () => {
    this.showLoader(true);
    let params = _.cloneDeep(this.getQueryParams()) || {};
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    this.initTableSort(params);
    let queriesArray = [];
    queriesArray.push(this.savedUserViewService.getSavedUserViewDataGrid(params, this.selectedItemId, EFilterDisplayType.DASHBOARD, !this.isSupportMngUserViewData && !this.isSupportAssignPublicUserViewData));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([savedDashboardsData]) => {
      this.savedDashboardsData = savedDashboardsData;
      this.totalElements = savedDashboardsData.totalElements;
      this.showLoader(false);
    }));
  };

  //---------------------------------------

  openDeleteDialog = () => {
    let msg = this.i18nService.translateId('MESSAGES.DELETE_CONFIRMATION', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT'), name: this.selectedItem.name});

    this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), msg).then(result => {
      if (result) {
        this.deleteSavedDashboard(this.selectedItem.id);
      }
    });
  };

  //---------------------------------------

  openDialog(action:string) {
    let cloneDashboard = _.cloneDeep(this.selectedItem) || <UserViewDataDto>{};
    let data = { data: {userView: cloneDashboard, action: action} };

    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(SavedUserViewEditorDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        switch(action) {
          case ToolbarActionTypes.SCOPE:
          case ToolbarActionTypes.EDIT:
            this.loadData();
            break;
          case ToolbarActionTypes.CLONE:
            this.routingService.updateParam(this.activatedRoute, RoutePathParams.selectedItemId, data.userView.id);
            break;
        }
      }
    },(reason) => {});
  }

  //---------------------------------------

  openEditDialog = () => {
    this.openDialog(ToolbarActionTypes.EDIT);
  };

  //---------------------------------------

  openSetScopeDialog = () => {
    this.openDialog(ToolbarActionTypes.SCOPE);
  };

  //---------------------------------------

  openCloneDialog = () => {
    this.openDialog(ToolbarActionTypes.CLONE);
  };

  //---------------------------------------

  exportData = () => {
    let params = _.cloneDeep(this.getQueryParams()) || {};
    params["withChildData"]="true";
    this.savedUserViewService.exportData(null, params, EFilterDisplayType.DASHBOARD,this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.TITLE'), !this.isSupportMngUserViewData);
  };

  //---------------------------------------

  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.savedUserViewService.importData, validateUploadFn: this.savedUserViewService.validateImportFile, component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT_MULTI'), fileType: ".csv", extractValidateUploadDataFn: this.savedUserViewService.extractImportValidationData, allowDuplicates: false} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.loadData();
      }
    },(reason) => {});
  };

  //---------------------------------------

  onSearchChanged = (newText:string) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.searchText,SettingPathParams.page], [newText,1]);
    this.localFilterFunction = (newText ? (item) => {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  //---------------------------------------

  public searchBoxConfig:SearchBoxConfigDto = {
    searchFunc: this.onSearchChanged,
    searchTerm: '',
    placeholder: this.i18nService.translateId('SEARCHBOX_PLACEHOLDER', {field: 'name'}),
    notifyOnKeyup: false
  };

  //---------------------------------------
  // privates
  //---------------------------------------

  private initTableSort(params:any) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    }
    let sortObject=JSON.parse(params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  //----------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //----------------------------------------

  private setActionItems() {
    let disableEditDelete = !this.selectedItem || !this.selectedItem.owner ||
      (this.selectedItem.owner.name != this.username && !this.isSupportMngUserViewData);

    let disableSetScope = !this.selectedItem || !this.isSupportAssignPublicUserViewData;

    this.settingsActionItems = [
      {
        type: ToolbarActionTypes.CLONE,
        title: this.i18nService.translateId('CLONE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT')}),
        name: this.i18nService.translateId('CLONE'),
        actionFunc: this.openCloneDialog,
        disabled: !this.selectedItem || !this.selectedItem.owner,
        href: ToolbarActionTypes.CLONE,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT')}),
        name: this.i18nService.translateId('EDIT'),
        actionFunc: this.openEditDialog,
        disabled: disableEditDelete,
        href: ToolbarActionTypes.EDIT,
        order: 1,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('SCOPE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT')}),
        name: this.i18nService.translateId('SCOPE'),
        actionFunc: this.openSetScopeDialog,
        disabled: disableSetScope,
        href: ToolbarActionTypes.EDIT,
        order: 2,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: this.i18nService.translateId('DELETE_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT')}),
        name: this.i18nService.translateId('DELETE'),
        actionFunc: this.openDeleteDialog,
        disabled: disableEditDelete,
        href: ToolbarActionTypes.DELETE,
        order: 3,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.IMPORT,
        title: this.i18nService.translateId('IMPORT_TITLE', {component: this.i18nService.translateId('USER_VIEW_DATA.SAVED_DASHBOARDS.COMPONENT_MULTI')}),
        name: this.i18nService.translateId('IMPORT'),
        actionFunc: this.openUploadItemsDialog,
        disabled: false,
        href: ToolbarActionTypes.IMPORT,
        order: 4,
        btnType: 'flat'
      }
    ];

    let exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_OPTION_EXCEL'),
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: this.i18nService.translateId('EXPORT_TITLE'),
      disabled: false,
      href: ToolbarActionTypes.EXPORT,
      children: exportDropDownItems
    };
  }

  //----------------------------------------

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<UserViewDataDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  //----------------------------------------

  private onItemSelected = (itemId, item) => {
    if (!this.selectedItem || this.selectedItemId != itemId) {
      this.selectedItemId = itemId;
      this.routingService.replaceQueryParamsWithoutReloading(this.getQueryParams(), RoutePathParams.selectedItemId, this.selectedItemId);
      this.updateLatestQueryParams(RoutePathParams.selectedItemId, this.selectedItemId);
    }
    this.selectedItem = item;
    this.setActionItems();
  };

  //----------------------------------------

  private onPageChanged = (pageNum:number) => {
    this.selectedItem = null;
    this.routingService.updateParamList(this.activatedRoute,[RoutePathParams.selectedItemId, SettingPathParams.page],[null, pageNum]);
  };

  //----------------------------------------

  private onSortByChanged = (sortObj:any) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.sort],[JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder)}])]);
  };

  //----------------------------------------

  private deleteSavedDashboard(dashboardId) {
    this.savedUserViewService.deleteSavedUserViewData(dashboardId).subscribe(()=> {
      this.selectedItem = null;
      this.setActionItems();
      this.routingService.updateParamList(this.activatedRoute,[RoutePathParams.selectedItemId],[null]);
    });
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showInfo(info) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('NOTE'), `${info.error.message}`, AlertType.Info, info);
  }

  //---------------------------------------

  showError(err) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message}), AlertType.Error, err);
  }
}

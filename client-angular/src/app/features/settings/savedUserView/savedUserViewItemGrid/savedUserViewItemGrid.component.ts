import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {EFilterDisplayType,filterDisplayTypeHref,filterDisplayTypeIcon,UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import * as _ from "lodash";
import {AddEditChartWidgetDialogComponent} from "@app/common/dashboard/popups/addEditChartWidgetDialog/addEditChartWidgetDialog.component";
import {ChartType} from "@app/features/dashboard/types/dashboard-enums";
import {ChartInfo} from "@app/features/dashboard/types/dashboard-interfaces";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {SavedUserViewEditorDialogComponent} from "@app/features/settings/savedUserView/savedUserViewDialog/savedUserViewEditorDialog/savedUserViewEditorDialog.component";
import {ToolbarActionTypes} from "@app/types/display.type";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {LoginService} from "@services/login.service";
import {SavedUserViewService} from "@app/common/saved-user-view/saved-user-view.service";

@Component({
  selector: 'saved-user-view-item-grid',
  templateUrl: './savedUserViewItemGrid.component.html',
  styleUrls: ['./savedUserViewItemGrid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SavedUserViewItemGridComponent extends TreeGridInnerItemBaseComponent<UserViewDataDto> implements OnInit {

  userViewIcon:string = "";
  routerLink:string = "";
  queryParams:any = {};
  title:string = "";
  isSupportMngUserViewData:boolean;
  isSupportAssignPublicUserViewData:boolean;
  username:string;

  @ViewChild('el') el: ElementRef;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private loginService:LoginService,
              private savedUserViewService: SavedUserViewService,
              private urlAuthorizationService: UrlAuthorizationService,
              private dashboardService: DashboardService,
              private dialogService: DialogService) {
    super();
    this.isSupportMngUserViewData  = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngUserViewData]);
    this.isSupportAssignPublicUserViewData = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.AssignPublicUserViewData]);
    this.username  = this.loginService.getCurrentUser().name;
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------


  itemClicked() {
    let userViewData: UserViewDataDto = this.dataItem.item;
    let disableEditDelete =
      ((userViewData.owner == null || userViewData.owner.name != this.username ) && !this.isSupportMngUserViewData) ||
      ((userViewData.owner != null && userViewData.owner.name == this.username ) && userViewData.globalFilter && !this.isSupportAssignPublicUserViewData);
  }

  ngOnInit(): void {
    this.userViewIcon = filterDisplayTypeIcon[this.dataItem.item.displayType];
    this.routerLink = filterDisplayTypeHref[this.dataItem.item.displayType];
    this.queryParams = {};
    if (this.dataItem.item.displayType == EFilterDisplayType.DISCOVER) {
      this.queryParams = {pageRight: 1,activePan: 'left', selectedItemLeft: 1, filter: this.dataItem.item.filter.rawFilter};
      this.title = "Go to discover";
    }
    else if (this.dataItem.item.displayType == EFilterDisplayType.CHART) {
      this.queryParams = {id: this.dataItem.item.id};
      this.title = "Go to manage charts";
    }
    else if (this.dataItem.item.displayType == EFilterDisplayType.DASHBOARD) {
      this.queryParams = {dashboardId: this.dataItem.item.id};
      this.title = "Go to dashboard";
    }
    else if (this.dataItem.item.displayType == EFilterDisplayType.DISCOVER_VIEW) {
      let displayData =  JSON.parse(this.dataItem.item.displayData);
      this.routerLink = `${this.routerLink}/${displayData.leftEntityDisplayTypeName}/${displayData.rightEntityDisplayTypeName}`;
      this.queryParams = {pageRight: 1,activePan: 'left', selectedItemLeft: 1, filter: this.dataItem.item.filter ? this.dataItem.item.filter.rawFilter : null, view: this.dataItem.item.id};
      this.title = "Go to discover";
    }
  }
}


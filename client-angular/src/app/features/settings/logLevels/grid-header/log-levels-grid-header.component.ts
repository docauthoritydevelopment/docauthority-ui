import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'log-levels-grid-header',
  templateUrl: './log-levels-grid-header.component.html',
  styleUrls: ['./log-levels-grid-header.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LogLevelsGridHeaderComponent{

  constructor() {
  }
}

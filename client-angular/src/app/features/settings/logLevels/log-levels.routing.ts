import {Routes} from '@angular/router';
import {LogLevelsComponent} from "@app/features/settings/logLevels/log-levels.component";

export const logLevelsRoutes: Routes = [
  {
    path: '',
    component: LogLevelsComponent
  }
];

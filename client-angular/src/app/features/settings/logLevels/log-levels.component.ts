import * as _ from 'lodash'
import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {BaseComponent} from "@core/base/baseComponent";
import {LogLevelsService} from "@app/features/settings/logLevels/log-levels.service";
import {LogLevelItemGridComponent} from "@app/features/settings/logLevels/item-grid/log-level-item-grid.component";
import {LogLevelsGridHeaderComponent} from "@app/features/settings/logLevels/grid-header/log-levels-grid-header.component";
import {AddLogLevelComponent} from "@app/features/settings/logLevels/dialogs/add/add-log-level.component";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {ITreeGridEvents, TreeGridConfig, TreeGridData} from "@tree-grid/types/tree-grid.type";
import {LogLevelDto} from "@app/features/settings/logLevels/log-levels.dto";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {ToolbarActionTypes} from "@app/types/display.type";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from "@angular/router";
import {ELogLevelType} from "@app/features/settings/logLevels/log-levels.type";

@Component({
  selector: 'log-levels',
  templateUrl: './log-levels.component.html',
  styleUrls: ['./_log-levels.scss'],
  encapsulation: ViewEncapsulation.None
})

export class LogLevelsComponent extends BaseComponent {

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;

  logLevelItemGridComponent = LogLevelItemGridComponent;
  logLevelsGridHeaderComponent = LogLevelsGridHeaderComponent;
  logLevelsData: TreeGridData<LogLevelDto>;
  treeGridEvents: ITreeGridEvents<object>;
  actionItems:ButtonConfigDto[];

  hideActionItems = false;
  totalElements:number = 0;
  enumLevelTypes = _.keys(ELogLevelType);
  loading: boolean = false;
  selectedItem: any;
  selectedItemId: number;
  treeConfig = {};
  filterCount:number = null;
  localFilterFunction:any = null;

  constructor(
    private route: ActivatedRoute,
    private routingService: RoutingService,
    private logLevelsService: LogLevelsService,
    private dialogService: DialogService,
    private i18nService:I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  daOnInit() {

    this.treeGridEvents = {
      onItemSelected: this.onItemSelected
    };

    this.treeConfig = <TreeGridConfig>{
        doLocalSorting: true
    };

    this.on(this.logLevelsService.logLevelsRefreshedEvent$, () => {
      this.loadData(true);
    });

    this.setActionItems();
    this.setSelectedItem();
    this.loadData();
  }

  //----------------------------------------

  setSelectedItem() {
    const queryParams = this.route.snapshot.queryParams;
    if (queryParams) {
      this.selectedItemId = parseInt(queryParams[RoutePathParams.findId] || queryParams[RoutePathParams.selectedItemId], 10);
    }
  }

  //----------------------------------------

  loadData = (hideLoader?) => {
    if(!hideLoader) {
      this.showLoader(true);
    }
    this.logLevelsService.getData(this.selectedItemId).subscribe((data: TreeGridData<LogLevelDto>) => {
      this.totalElements = data.totalElements;
      this.logLevelsData = data;
      this.showLoader(false);
    });
  };

  //----------------------------------------

  private onItemSelected = (itemId, item) => {
    this.selectedItem = item;
    this.selectedItemId = itemId;
    this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams, RoutePathParams.selectedItemId, this.selectedItemId);
  };

  //----------------------------------------

  private setActionItems() {

    this.actionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: "Add log level",
        name: 'Add',
        actionFunc: this.openAddNewDialog,
        disabled: false,
        href: ToolbarActionTypes.ADD,
        order: 0,
        btnType: 'flat'
      }
    ];
  }

  //----------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //---------------------------------------

  //---------------------------------------

  changeLevel(configuredLevel) {
    if(this.selectedItem) {
      this.logLevelsService.updateLogLevel({
        name: this.selectedItem.name,
        configuredLevel: configuredLevel,
      }).subscribe((addedLogLevel:LogLevelDto) => {
        debugger;
      },(err) => {});
    }
  }

  //---------------------------------------

  filterCountChange(newCount:number) {
    this.filterCount = newCount;
  }

  onSearchChanged = (newText:string) => {
    this.localFilterFunction = (newText ? (item)=> {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  //---------------------------------------

  openAddNewDialog = () => {
    let data = { data: { } };
    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(AddLogLevelComponent, data, modelParams);
  };

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.showLoader(false);
    this.dialogService.showAlert("Error", `Sorry, an error occurred - ${err.error.message}`, AlertType.Error, err);
  }

}

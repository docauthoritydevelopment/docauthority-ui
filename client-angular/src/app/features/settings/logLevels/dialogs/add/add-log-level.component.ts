import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {LogLevelDto} from "@app/features/settings/logLevels/log-levels.dto";
import {LogLevelsService} from "@app/features/settings/logLevels/log-levels.service";
import {ELogLevelType} from "@app/features/settings/logLevels/log-levels.type"

@Component({
  selector: 'add-log-level',
  templateUrl: './add-log-level.component.html',
  styleUrls: ['./_add-log-level.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddLogLevelComponent extends DialogBaseComponent implements OnInit, OnDestroy {

  title: string = "";
  logLevelItem: LogLevelDto = null;
  enumLevelTypes = _.keys(ELogLevelType);
  inEditMode:boolean;

  @ViewChild('logLevelForm') public logLevelForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal: NgbActiveModal, private logLevelsService: LogLevelsService, private dialogService: DialogService) {
    super();
  }

  //---------------------------------------

  ngOnInit(): void {
    this.setLogLevel();
  }

  //---------------------------------------

  private setLogLevel() {
    if (_.has(this.data, 'logLevel')) {
      this.title = "Edit log level";
      this.logLevelItem =  (<LogLevelDto>this.data)['logLevel'];
      this.inEditMode = true;
    }
    else {
      this.title = "Add log level";
      this.logLevelItem = <LogLevelDto>{};
      this.logLevelItem.name = "";
      this.logLevelItem.configuredLevel = ELogLevelType.INFO;
      this.inEditMode = false;
    }
  }

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  save = (createAnother?:boolean) => {
    if (this.logLevelForm.valid) {
      this.logLevelsService.updateLogLevel(this.logLevelItem).subscribe((addedLogLevel: LogLevelDto) => {
        if (createAnother) {
          this.logLevelItem.name = "";
        }
        else {
          let result: any = {logLevel: addedLogLevel};
          this.activeModal.close(result);
        }
      }, (err) => {
        this.showError(err)
      });
    }

  };

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.dialogService.showAlert("Error", `Sorry, an error occurred - ${err.error.message}`, AlertType.Error, err);
  }
}

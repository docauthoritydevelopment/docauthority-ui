import {ELogLevelType} from "@app/features/settings/logLevels/log-levels.type";

export class LogLevelDto {
  name : string;
  configuredLevel : ELogLevelType;
  effectiveLevel: ELogLevelType;
}

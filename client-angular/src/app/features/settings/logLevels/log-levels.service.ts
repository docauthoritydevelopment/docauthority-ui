import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {finalize, map} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TreeGridData, TreeGridDataItem, TreeGridSortingInfo, TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {LogLevelDto} from "@app/features/settings/logLevels/log-levels.dto";
import {Observable, Subject} from 'rxjs/index';
import {ELogLevelType} from "@app/features/settings/logLevels/log-levels.type";
import {ServerUrls} from "@app/common/configuration/common-config";


@Injectable()
export class LogLevelsService {

  private data;

  private logLevelsRefreshedEvent = new Subject<any>();
  logLevelsRefreshedEvent$ = this.logLevelsRefreshedEvent.asObservable();

  constructor(
    private http: HttpClient,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private excelService: ExcelService) {
  }

  //---------------------------------------

  getData(selectedItem):Observable<TreeGridData<LogLevelDto>>{
    return this.http.get('/api/custom-loggers/loggers').pipe(
      map((data) => {
        this.data = this.convertToPagingFormat(data, selectedItem);
        return this.data;
      }));
  }

  //----------------------------------------

  convertToPagingFormat(data, selectedItemId): TreeGridData<LogLevelDto>{

    let loggerClasses = _.keys(data.loggers);

    let pagingData:DTOPagingResults<LogLevelDto> = {
      content: [],
      totalPages: 1,
      size: null,
      last: null,
      first: null,
      sort: null,
      number: null,
      pageable : null,
      numberOfElements: loggerClasses.length,
      totalElements: loggerClasses.length
    };

    _.forEach(loggerClasses, (key, index) => {
      pagingData.content.push(<LogLevelDto>{
        id: index,
        name: key,
        configuredLevel: data.loggers[key].configuredLevel,
        effectiveLevel: data.loggers[key].effectiveLevel
      })
    });

    return this.treeGridAdapter.adapt(pagingData, selectedItemId, new TreeGridAdapterMapper());
  }

  //----------------------------------------

  updateLogLevel(logLevel) {
    let url = `/api/custom-loggers/configure?name=${logLevel.name}&configuredLevel=${logLevel.configuredLevel}`;

    return this.http.post(url,{}).pipe(finalize(()=> {
      this.logLevelsRefreshedEvent.next();
    }));
  }
}

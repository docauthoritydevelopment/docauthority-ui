import * as _ from 'lodash';
import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {LogLevelDto} from "@app/features/settings/logLevels/log-levels.dto";
import {ELogLevelType} from "@app/features/settings/logLevels/log-levels.type"
import {LogLevelsService} from "@app/features/settings/logLevels/log-levels.service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'log-level-item-grid',
  templateUrl: './log-level-item-grid.component.html',
  styleUrls: ['./log-level-item-grid.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LogLevelItemGridComponent extends TreeGridInnerItemBaseComponent<LogLevelDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;

  key= "LOG_LEVELS.";
  selectedType = ELogLevelType.INFO.toString();
  enumLevelTypes = _.keys(ELogLevelType);
  levelDropDownItems: DropDownConfigDto[]  = null ;


  constructor(private logLevelsService:LogLevelsService,private i18nService: I18nService) {
    super();
  }

  ngOnInit(): void {
    this.buildLevelsDropDownItems();
  }

  buildLevelsDropDownItems() {
    this.levelDropDownItems = [];
    this.enumLevelTypes.forEach((logLevel)=>{
      this.levelDropDownItems.push({
        title: this.i18nService.translateIdWithoutNull('LOG_LEVELS.TYPES.'+logLevel),
        iconClass: logLevel ==  this.dataItem.item.configuredLevel.toString() ? 'fa fa-check' : '',
        actionFunc: () => {
          this.onLevelClick(logLevel);
        }
      })
    });
  }




  onLevelClick(type:string) {
    this.selectedType = type;

    this.logLevelsService.updateLogLevel({
      name: this.dataItem.item.name,
      configuredLevel: type
    }).subscribe((addedLogLevel:LogLevelDto) => {},(err) => {});
  }
}


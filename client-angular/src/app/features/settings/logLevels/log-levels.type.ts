export enum  ELogLevelType {
  TRACE = <any>"TRACE",
  DEBUG = <any>"DEBUG",
  INFO = <any>"INFO",
  WARN = <any>"WARN",
  ERROR = <any>"ERROR",
  OFF = <any>"OFF",
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {SharedUiModule} from '@shared-ui/shared-ui.module';
import {DialogsModule} from "@app/common/dialogs/dialogs.module";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {TranslateModule} from "@ngx-translate/core";
import {LogLevelItemGridComponent} from "@app/features/settings/logLevels/item-grid/log-level-item-grid.component";
import {LogLevelsComponent} from "@app/features/settings/logLevels/log-levels.component";
import {LogLevelsService} from "@app/features/settings/logLevels/log-levels.service";
import {LogLevelsGridHeaderComponent} from "@app/features/settings/logLevels/grid-header/log-levels-grid-header.component";
import {RouterModule} from "@angular/router";
import {logLevelsRoutes} from "@app/features/settings/logLevels/log-levels.routing";
import {AddLogLevelComponent} from "@app/features/settings/logLevels/dialogs/add/add-log-level.component";
import {LogLevelContextMenuComponent} from "@app/features/settings/logLevels/context-menu/log-level-context-menu.component";
import {ContextMenuModule} from "ngx-contextmenu";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    LoaderModule,
    TreeGridModule,
    NgbModule,
    SharedUiModule,
    DialogsModule,
    TranslateModule,
    DaDraggableModule,
    RouterModule.forChild(logLevelsRoutes),
    ContextMenuModule.forRoot({useBootstrap4: true})
  ],
  declarations: [
    LogLevelItemGridComponent,
    LogLevelsComponent,
    LogLevelsGridHeaderComponent,
    AddLogLevelComponent,
    LogLevelContextMenuComponent
  ],
  providers: [
    LogLevelsService
  ],
  entryComponents: [
    LogLevelItemGridComponent,
    LogLevelsGridHeaderComponent,
    LogLevelContextMenuComponent,
    AddLogLevelComponent
  ],
})
export class LogLevelsModule {

}


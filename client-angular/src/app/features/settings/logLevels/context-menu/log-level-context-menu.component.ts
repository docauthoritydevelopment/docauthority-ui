import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ContextMenuComponent} from "ngx-contextmenu";

@Component({
  selector: 'log-level-context-menu',
  templateUrl: './log-level-context-menu.component.html',
  styleUrls: ['./log-level-context-menu.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LogLevelContextMenuComponent  implements OnInit {

  @ViewChild(ContextMenuComponent) public logLeveContextMenu: ContextMenuComponent;

  constructor() {
  }

  ngOnInit(): void {
  }

  edit(){
  }

  delete(){
  }
}


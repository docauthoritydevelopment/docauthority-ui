import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'departments-grid-header',
  templateUrl: './departmentsGridHeader.component.html',
  styleUrls: ['./departmentsGridHeader.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DepartmentsGridHeaderComponent{
  constructor() {
  }
}

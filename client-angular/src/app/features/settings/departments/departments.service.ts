import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper, TreeGridData, TreeGridDataItem} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {DepartmentDto} from "@app/features/settings/departments/departments.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {ServerExportService} from "@services/serverExport.service";
import {CommonConfig, ExportType, ServerUrls} from "@app/common/configuration/common-config";
import {RdbService} from "@services/rdb.service";

@Injectable()
export class DepartmentsService {

  private max_department_colors = 18;

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private serverExportService: ServerExportService,
    private rdbService: RdbService) {
  }

  //----------------------------------------
  // getTreeGridDepartments
  //----------------------------------------

  getTreeGridDepartments(params,selectedItemId:number): Observable<TreeGridData<DepartmentDto>> {
    if (params) {
      params = this.urlParamsService.fixedPagedParams(params,30);
      if (!params.sort) {
        params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
      }
    }
    else {
      params = { page: 1, sort: JSON.stringify([{dir: 'desc', field: 'id'}])};
    }
    return this.http.get(ServerUrls.DEPARTMENT_LIST, {params}).pipe(
      map((data: DTOPagingResults<DepartmentDto>) => {
        let ans: TreeGridData<DepartmentDto> = this.treeGridAdapter.adapt(
          data,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
        ans.items.forEach((dataItem:  TreeGridDataItem<any>)=> {
          dataItem.id = dataItem.item.id ;
        });
        return ans;
      }));
  }

  //----------------------------------------
  // Departments actions
  //----------------------------------------

  addNewDepartment(newDepartment) {
    return this.http.put(ServerUrls.DEPARTMENT, newDepartment);
  }

  //----------------------------------------

  editDepartment(department) {
    return this.http.post(`${ServerUrls.DEPARTMENT}/${department.id}`, department);
  }

  //----------------------------------------

  cloneDepartment(newDepartment) {
    return this.http.put(ServerUrls.DEPARTMENT_CLONE, newDepartment);
  }

  //----------------------------------------

  deleteDepartment(departmentId) {
    return this.http.delete(`${ServerUrls.DEPARTMENT}/${departmentId}`);
  }

  //----------------------------------------

  exportData = (departmentsNum, params) =>  {
    params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    if (params.sortBy ) {
      params.sort = JSON.stringify([{dir: (params.sortOrder ? (<string>params.sortOrder).toLowerCase() :  'desc' ), field: params.sortBy}]);
    }
    this.serverExportService.prepareExportFile(this.i18nService.translateId('DEPARTMENTS.TITLE'),ExportType.DEPARTMENT_SETTINGS,params,{ itemsNumber : departmentsNum },departmentsNum > CommonConfig.minimum_items_to_open_export_popup);
  };

  //----------------------------------------

  /*exportSelectedData = (selectedDepartments): Observable<any> =>  {
    let selectedItems = [];
    selectedDepartments.forEach(department => {
      selectedItems.push(department.item);
    });
    return this.excelService.exportSelectedData(selectedItems, this.extractDepartmentData, 'departments');
  };*/

  //----------------------------------------

  extractImportValidationData = (item) => {
    const partialItem: any = {};
    partialItem['Msg Type'] = item.type;
    partialItem['Line number'] = item.data.rowDto.lineNumber;
    partialItem['Msg Description'] = item.data.message;
    partialItem['FULL_NAME'] = item.data.rowDto.fieldsValues[0];
    partialItem['DESCRIPTION'] = item.data.rowDto.fieldsValues[1];
    return partialItem;
  };

  //----------------------------------------

  validateImportFile = (formData:FormData, maxErrorRows:number) =>  {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.DEPARTMENT_VALIDATE_IMPORT,{maxErrorRows : maxErrorRows}), formData);
  };

  //----------------------------------------

  importData = (fileData:File, updateDuplicates:boolean) => {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.DEPARTMENT_IMPORT,{updateDuplicates : updateDuplicates}), fileData);
  };

  //----------------------------------------
  // getDepartmentStyleId
  //----------------------------------------

  getDepartmentStyleId(department) {
    let colors = this.max_department_colors;
    if(department.parentId != null) {
      return department.parentId%colors+1;
    }
    return department.id%colors+1;
  }

  //----------------------------------------

  getAssignedDepartments(departmentId) {
    return this.http.get(ServerUrls.DEPARTMENT_COUNT).pipe(map((data: DTOPagingResults<any>)=> {
      return _.filter(data.content, (c) => {
        return c.item.id === departmentId;
      })[0];
    }));
  }
}

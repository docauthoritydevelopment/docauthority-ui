import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from '@angular/router';
import {DialogService} from "@app/shared-ui/components/modal-dialogs/dialog.service";
import {DepartmentsService} from "@app/features/settings/departments/departments.service";
import {DepartmentDto} from "@app/features/settings/departments/departments.dto";

@Component({
  selector: 'departments-item-grid',
  templateUrl: './departmentsItemGrid.component.html',
  styleUrls: ['./departmentsItemGrid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DepartmentsItemGridComponent extends TreeGridInnerItemBaseComponent<DepartmentDto> implements OnInit {

  @ViewChild('el') el: ElementRef;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private departmentsService: DepartmentsService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {

  }

  //---------------------------------------

  getDepartmentStyleId(department) {
    return this.departmentsService.getDepartmentStyleId(department);
  }
}


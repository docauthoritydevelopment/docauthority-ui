import * as _ from 'lodash'
import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute} from "@angular/router";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";
import {ITreeGridEvents, TreeGridData, TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";
import {UtilService} from '@services/util.service';
import {AddEditDepartmentDialogComponent} from "@app/features/settings/departments/departmentsDialog/addEditDepartmentDialog/addEditDepartmentDialog.component";
import {CloneDepartmentDialogComponent} from "@app/features/settings/departments/departmentsDialog/cloneDepartmentDialog/cloneDepartmentDialog.component";
import {DepartmentsService} from './departments.service';
import {DepartmentsGridHeaderComponent} from "@app/features/settings/departments/departmentsGridHeader/departmentsGridHeader.component";
import {DepartmentsItemGridComponent} from "@app/features/settings/departments/departmentsItemGrid/departmentsItemGrid.component";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AlertType} from '@shared-ui/components/modal-dialogs/types/alert-type.enum';
import {UserSettingsService} from "@services/user.settings.service";
import {BaseComponent} from "@core/base/baseComponent";
import {DepartmentDto} from "@app/features/settings/departments/departments.dto";
import {DefaultPagedReqParams} from '@services/urlParams.service';
import {ToolbarActionTypes} from "@app/types/display.type";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {UploadDialogComponent} from "@app/common/dialogs/uploadDialog/uploadDialog.component";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DepartmentsComponent extends BaseComponent implements OnInit, OnDestroy {

  departments:TreeGridData<DepartmentDto>;

  loading = true;
  params: DefaultPagedReqParams = null;
  actionItems:ButtonConfigDto[];
  exportActionItem:ButtonConfigDto;
  selectedItem:any;
  selectedItemId:number;
  totalElements:number = 0;
  filterCount: number = null;
  localFilterFunction: (dataItem: DepartmentDto) => boolean = null;

  initialSortDirection:TreeGridSortOrder = null;
  initialSortFieldName:string = null;

  departmentsGridHeaderComponent = DepartmentsGridHeaderComponent;
  departmentsItemGridComponent = DepartmentsItemGridComponent;

  treeGridEvents:ITreeGridEvents<DepartmentDto>;

  public isMngDepartmentsUser:boolean;

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;

  treeConfig: any = {
    showMarkCheckbox: true
  };

  //---------------------------------------

  constructor(
    private urlAuthorizationService : UrlAuthorizationService,
    private route: ActivatedRoute,
    private routingService: RoutingService,
    private departmentsService: DepartmentsService,
    private utilService: UtilService,
    private userSettingsService: UserSettingsService,
    private dialogService: DialogService,
    private i18nService: I18nService) {
    super();
  }

  //---------------------------------------
  // daOnInit
  //---------------------------------------

  daOnInit() {
    this.showLoader(true);
    this.initializeRoles();
    this.setSelectedItem();
    this.setTreeGridEvent();
    this.setParams();
    this.setActionItems();
    this.loadData();
  }
  initializeRoles() {
    this.isMngDepartmentsUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngDepartmentConfig]);
  }
  //---------------------------------------
  // actions
  //---------------------------------------

  refreshGridAfterAdd = (data: any) => {
    let newDepartment:DepartmentDto = data.department;
    if(newDepartment) {
      this.onItemSelected(newDepartment.id, newDepartment);
      this.loadData();
    }
  };

  //---------------------------------------

  openAddNewDialog = () => {
    let data = { data: { parentId: this.selectedItemId, params: this.params, onRefresh: this.refreshGridAfterAdd } };
    let modelParams = { windowClass: 'modal-md-window' };

    if (this.selectedItem.parentId) {
      data.data.parentId = this.selectedItem.parentId;
    }

    this.dialogService.openModalPopup(AddEditDepartmentDialogComponent, data, modelParams).result.then(this.refreshGridAfterAdd, () => {});
  };

  //--------------------------------------

  openEditDialog = () => {
    let data = { data: { department: _.cloneDeep(this.selectedItem)} };
    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(AddEditDepartmentDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        this.loadData();
      }
    },() => {});
  };

  //--------------------------------------

  openCloneDialog = () => {
    let cloneDepartment = <DepartmentDto>{};

    cloneDepartment.name = this.selectedItem.name;
    cloneDepartment.description = this.selectedItem.description;

    let subDepartments:any[] = [];
    this.departments.items.find(dep => dep.id === this.selectedItem.id).children.forEach(d => {
      subDepartments.push({ name: d.item.name });
    });

    let data = { data: { department: cloneDepartment, subDepartments: subDepartments } };
    let modelParams = { windowClass: 'modal-md-window' };

    this.dialogService.openModalPopup(CloneDepartmentDialogComponent, data, modelParams).result.then((data : any) => {
      if (data) {
        data.department.childrenName = data.subDepartments;
        this.departmentsService.cloneDepartment(data.department).subscribe((addedDepartment:DepartmentDto) => {
          if (addedDepartment) {
            this.onItemSelected(addedDepartment.id, addedDepartment);
            this.loadData();
          }
        },(err) => {this.showError(err)});
      }
    },() => {});
  };

  //---------------------------------------

  openDeleteDialog = () => {
    let msg = this.i18nService.translateId('MESSAGES.DELETE_CONFIRMATION', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT'), name: this.selectedItem.name});

    this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), msg).then(result => {
      if (result) {
        this.deleteDepartment(this.selectedItem.id);
      }
    });
  };

  //---------------------------------------

  exportData = () => {
    this.departmentsService.exportData(this.departments.totalElements, {sort: this.params.sort});
  };

  //---------------------------------------

  openUploadItemsDialog = () => {
    let data = { data: { uploadFn: this.departmentsService.importData, validateUploadFn: this.departmentsService.validateImportFile, component: this.i18nService.translateId('DEPARTMENTS.COMPONENT_MULTI'), fileType: ".csv", extractValidateUploadDataFn: this.departmentsService.extractImportValidationData} };
    let modelParams = {
      windowClass: 'modal-md-window'
    };

    this.dialogService.openModalPopup(UploadDialogComponent, data, modelParams).result.then((res) => {
      if (res == 'ok') {
        this.loadData();
      }
    },() => {});
  };

  //---------------------------------------

  filterCountChange(newCount:number) {
    this.filterCount = newCount;
  }

  //---------------------------------------

  onSearchChanged = (newText:string) => {
    this.localFilterFunction = (newText ? (item)=> {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  //---------------------------------------

  public searchBoxConfig:SearchBoxConfigDto = {
    searchFunc: this.onSearchChanged,
    searchTerm: '',
    placeholder: this.i18nService.translateId('SEARCHBOX_PLACEHOLDER', {field: 'name'}),
    notifyOnKeyup: false
  };

  //---------------------------------------
  // privates
  //---------------------------------------

  private setSelectedItem() {
    const queryParams = this.route.snapshot.queryParams;
    if (queryParams) {
      this.selectedItemId = parseInt(queryParams[RoutePathParams.findId] || queryParams[RoutePathParams.selectedItemId], 10);
      this.updateActionItems('selection');
    }
  }

  //---------------------------------------

  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;
    this.updateActionItems('selection');

    this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams,
      RoutePathParams.selectedItemId, this.selectedItemId);
  };

  //----------------------------------------

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<DepartmentDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  //----------------------------------------

  private onPageChanged = (pageNum) => {
    this.params.page = pageNum;
    this.loadData();
  };

  //----------------------------------------

  private onSortByChanged = (sortObj: any) => {
    this.params.sort = JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder).toLowerCase()}]);
    this.loadData();
  };

  //---------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //---------------------------------------

  private setParamsSortDefault() {
    this.params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    let sortObject=JSON.parse(this.params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  //---------------------------------------

  private setParamsPageDefault() {
    this.params.page = 1;
  }

  //---------------------------------------

  private setParams() {
    if (this.params) {
      if (!this.params.sort) {
        this.setParamsSortDefault();
      }
      else {
        let sortObj;
        if (typeof this.params.sort === "string") {
          sortObj = JSON.parse(this.params.sort);
        }
        else {
          sortObj = {field: this.params.sort.sortBy, dir: this.params.sort.sortOrder};
        }
        this.initialSortFieldName = sortObj[0].field;
        this.initialSortDirection = sortObj[0].dir;
      }

      if (!this.params.page) {
        this.setParamsPageDefault();
      }
    }
    else {
      this.params = {};
      this.setParamsSortDefault();
      this.setParamsPageDefault();
    }
  }

  //----------------------------------------

  private deleteDepartment(departmentId) {
    this.departmentsService.deleteDepartment(departmentId).subscribe(()=> {
      this.loadData();
    },(err) => {this.showError(err)});
  }

  //----------------------------------------

  private setActionItems() {
    this.actionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: this.i18nService.translateId('ADD_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')}),
        name: this.i18nService.translateId('ADD'),
        actionFunc: this.openAddNewDialog,
        disabled: false,
        href: ToolbarActionTypes.ADD,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.CLONE,
        title: this.i18nService.translateId('CLONE_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')}),
        name: this.i18nService.translateId('CLONE'),
        actionFunc: this.openCloneDialog,
        disabled: !this.selectedItem || this.selectedItem.parentId != null,
        href: ToolbarActionTypes.CLONE,
        order: 1,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')}),
        name: this.i18nService.translateId('EDIT'),
        actionFunc: this.openEditDialog,
        disabled: !this.selectedItem,
        href: ToolbarActionTypes.EDIT,
        order: 2,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: this.i18nService.translateId('DELETE_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')}),
        name: this.i18nService.translateId('DELETE'),
        actionFunc: this.openDeleteDialog,
        disabled: !this.selectedItem,
        href: ToolbarActionTypes.DELETE,
        order: 3,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.IMPORT,
        title: this.i18nService.translateId('IMPORT_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT_MULTI')}),
        name: this.i18nService.translateId('IMPORT'),
        actionFunc: this.openUploadItemsDialog,
        disabled: false,
        href: ToolbarActionTypes.IMPORT,
        order: 4,
        btnType: 'flat'
      }
    ];

    let exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_OPTION_EXCEL'),
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: this.i18nService.translateId('EXPORT_TITLE'),
      disabled: false,
      href: ToolbarActionTypes.EXPORT,
      children: exportDropDownItems
    }
  }

  //---------------------------------------

  private updateActionItems(changeCase:string) {
    if (this.actionItems && changeCase === 'selection') {
      this.actionItems.find(item => item.type === ToolbarActionTypes.EDIT).disabled = !this.selectedItem;
      this.actionItems.find(item => item.type === ToolbarActionTypes.DELETE).disabled = !this.selectedItem;
      this.actionItems.find(item => item.type === ToolbarActionTypes.CLONE).disabled = !this.selectedItem || this.selectedItem.parentId != null;
    }
    else {
      this.setActionItems();
    }
  }

  //---------------------------------------

  private loadData(callback?) {
    this.departmentsService.getTreeGridDepartments(this.params,this.selectedItemId ).subscribe((departments) => {
      this.departments = departments;
      this.totalElements = departments.totalElements;
      if(callback) {
        callback();
      }
      this.showLoader(false);
    },(err) => {this.showError(err)});
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showInfo(info) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('NOTE'), `${info.error.message}`, AlertType.Info, info);
  }

  //---------------------------------------

  showError(err) {
    this.showLoader(false);
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message ? err.error.message.toLowerCase() : ''}), AlertType.Error, err);
  }

  //---------------------------------------
  // destroy
  //---------------------------------------

  ngOnDestroy() {
  }
}

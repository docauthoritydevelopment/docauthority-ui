import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {DepartmentDto} from "@app/features/settings/departments/departments.dto";
import {UtilService} from "@services/util.service";
import {FilterOption} from "@app/common/filters/types/filter-interfaces";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'clone-depatment-dialog',
  templateUrl: './cloneDepartmentDialog.component.html',
  encapsulation: ViewEncapsulation.None
})

export class CloneDepartmentDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  departmentItem: DepartmentDto = null;
  title: string = "";
  subDepartments: any[];
  subDepartmentsOptions: FilterOption[] = [];

  @ViewChild('departmentGeneralForm') public departmentGeneralForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal:NgbActiveModal,
              private utilService:UtilService,
              private i18nService:I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setDepartment();
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  private setDepartment() {
    if (_.has(this.data, 'department')) {
      this.departmentItem =  (<DepartmentDto>this.data)['department'];
      this.title = this.i18nService.translateId('CLONE_DIALOG_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT'), name: this.departmentItem.name});
      this.departmentItem.name += " - copy";
      this.subDepartments = (this.data)['subDepartments'];

      this.subDepartmentsOptions = this.utilService.convertFilterOptionListIntoCheckArray(null,this.subDepartments.map((item:any)=> {
        let newObj:FilterOption = <FilterOption>{};
        newObj.value = item.name;
        newObj.displayText = item.name;
        return newObj;
      }),"");
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------


  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.departmentItem.name && this.departmentItem.name.length > 0 && this.departmentItem.name.length < 101;
  }

  //---------------------------------------

  isValid = () => {
    if (!this.departmentItem ) {
      return true;
    }
    return this.isValidName();
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  save = () => {
    if (this.isValid()) {
      let subDepartmentsSelect:string[] = [];
      this.subDepartmentsOptions.forEach(d => {
        if (d.selected) {
          subDepartmentsSelect.push(d.value);
        }
      });
      let result:any  = { department: this.departmentItem, subDepartments: subDepartmentsSelect };
      this.activeModal.close(result);
    }
  };

  //---------------------------------------

  onSubDepartmentSelectionChanged(subDepartments: any[]): void {
  }

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }
}

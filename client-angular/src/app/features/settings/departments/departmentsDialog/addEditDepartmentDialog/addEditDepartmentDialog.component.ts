import * as _ from 'lodash';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {NgForm} from "@angular/forms";
import {DepartmentDto} from "@app/features/settings/departments/departments.dto";
import {DepartmentsService} from "@app/features/settings/departments/departments.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {DefaultPagedReqParams} from "@services/urlParams.service";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'add-edit-depatment-dialog',
  templateUrl: './addEditDepartmentDialog.component.html',
  encapsulation: ViewEncapsulation.None
})

export class AddEditDepartmentDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {
  departmentItem: DepartmentDto = null;
  title: string = "";
  params: DefaultPagedReqParams = null;
  mainDepartments: any[] = [];
  selectedParent: DepartmentDto = null;
  onRefresh: any;
  isNew: boolean = false;

  @ViewChild('departmentGeneralForm') public departmentGeneralForm: NgForm = null;

  //---------------------------------------
  // constructor
  //---------------------------------------

  constructor(private activeModal:NgbActiveModal,
              private departmentsService:DepartmentsService,
              private dialogService:DialogService,
              private i18nService:I18nService) {
    super();
  }

  //---------------------------------------
  // ngOnInit
  //---------------------------------------

  ngOnInit(): void {
    this.setDepartment();
    this.loadDepartments();
    this.setModalEvents();
  }

  //---------------------------------------

  onParentChanged(department:DepartmentDto) {
    this.selectedParent = department;
    this.departmentItem.parentId = department.id;
  }

  //---------------------------------------
  // privates
  //---------------------------------------

  private loadDepartments() {
    this.departmentsService.getTreeGridDepartments(this.params,null).subscribe((departments) => {
      this.setMainDepartments(departments);
    },(err) => {this.showError(err)});
  }

  //---------------------------------------

  private setMainDepartments(departments) {
    this.mainDepartments = [];
    this.mainDepartments.push({ name: "---------- No Parent ----------", id: null});
    departments.items.forEach(d => {
      if (d.item.parentId == null) {
        this.mainDepartments.push({ name: d.item.name, id: d.item.id });
      }
    });

    this.selectedParent = this.mainDepartments.find(item => item.id === this.departmentItem.parentId);
    if (!this.selectedParent) {
      this.departmentItem.parentId = null;
    }
  }

  //---------------------------------------

  private setDepartment() {
    if (_.has(this.data, 'department')) {
      this.title = this.i18nService.translateId('EDIT_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')});
      this.departmentItem =  (<DepartmentDto>this.data)['department'];
    }
    else if (_.has(this.data, 'parentId')) {
      this.isNew = true;
      this.title = this.i18nService.translateId('ADD_TITLE', {component: this.i18nService.translateId('DEPARTMENTS.COMPONENT')});
      this.departmentItem = <DepartmentDto>{};
      this.departmentItem.name = "";
      this.departmentItem.parentId = (this.data)['parentId'];
      this.departmentItem.description = "";
      this.params = (this.data)['params']
    }
  }

  //---------------------------------------

  private setModalEvents() {
    if (_.has(this.data, 'onRefresh')) {
      this.onRefresh = (this.data)['onRefresh'];
    }
  }

  //---------------------------------------
  // comparators
  //---------------------------------------


  //---------------------------------------
  // validators
  //---------------------------------------

  private isValidName() {
    return this.departmentItem.name && this.departmentItem.name.length > 0 && this.departmentItem.name.length < 101;
  }

  //---------------------------------------

  isValid = () => {
    if (!this.departmentItem ) {
      return true;
    }
    return this.isValidName();
  };

  //---------------------------------------
  // actions
  //---------------------------------------

  cancel() {
    this.activeModal.close(null);
  }

  //---------------------------------------

  save = (createAnother?:boolean) => {
    if (this.isValid()) {
      if (this.isNew) {
        this.departmentsService.addNewDepartment(this.departmentItem).subscribe((addedDepartment:DepartmentDto) => {
          let result:any  = { department: addedDepartment };
          if (createAnother) {
            this.onRefresh(result);
            this.departmentItem.name = "";
            this.departmentItem.description = "";
            this.loadDepartments();
          }
          else {
            this.activeModal.close(result);
          }
        },(err) => {this.showError(err)});
      }
      else {
        this.departmentsService.editDepartment(this.departmentItem).subscribe((editedDepartment:DepartmentDto) => {
          let result:any  = { department: editedDepartment };
          this.activeModal.close(result);
        },(err) => {this.showError(err)});
      }
    }
  };

  //---------------------------------------
  // ngOnDestroy
  //---------------------------------------
  ngOnDestroy() {
  }

  //---------------------------------------
  // error handling
  //---------------------------------------

  showError(err) {
    this.dialogService.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: err.error.message}), AlertType.Error, err);
  }
}

import * as _ from 'lodash'
import {forkJoin} from "rxjs/index";
import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {RoutingService} from "@services/routing.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";
import {ITreeGridEvents, TreeGridConfig} from "@tree-grid/types/tree-grid.type";
import {UtilService} from '@services/util.service';
import {DocTypesService} from './docTypes.service';
import {TagTypesService} from '@app/features/settings/_common/tag-types/tag-types.service';
import {DocTypeItemGridComponent} from "@app/features/settings/doctypes/doc-type-item/docType-item-grid.component";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AlertType} from '@shared-ui/components/modal-dialogs/types/alert-type.enum';
import {AddDocTypeDialogComponent} from "@app/features/settings/doctypes/add-doctype-dialog/addDocTypeDialog.component";
import {DTODocType} from "@app/common/discover/types/discover-interfaces";
import {DocTypesExportService} from "@app/features/settings/doctypes/docTypes.export.service";
import {UserSettingsService} from "@services/user.settings.service";
import {ErrorCodes} from '@core/types/error.codes';
import {DocTypesGridHeaderComponent} from './docTypeGridHeader/docTypesGridHeader.component';
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {BaseComponent} from "@core/base/baseComponent";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {DatePipe} from "@angular/common";
import {ServerExportService} from "@services/serverExport.service";


@Component({
  selector: 'doc-types',
  templateUrl: './docTypes.component.html',
  styleUrls: ['./_docTypes.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DocTypesComponent extends BaseComponent{

  tagTypes;
  docTypes;
  movedItem;

  loading = true;
  moveMode = false;
  showFlags = true;
  doSaving = false;
  isMngTagConfigUser = true;
  isMngDocTypeConfigUser = true;
  totalElements:number = 0;
  actionItems:ButtonConfigDto[];
  exportActionItem:ButtonConfigDto;

  selectedItem;
  selectedItemId;

  selectedTagType;
  selectedTagTypeId;

  docTypesChanges = [];
  DocTypeItemGridComponent = DocTypeItemGridComponent;
  DocTypesGridHeaderComponent = DocTypesGridHeaderComponent;
  selectedTagTypeKey = 'DOC_TYPE_SETTINGS_SELECTED_TAG_TYPE';

  treeConfig = new TreeGridConfig();
  treeGridEvents: ITreeGridEvents<object>;
  filterCount: number = null ;

  localFilterFunction: any = null;

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;

  //---------------------------------------

  constructor(
    private router: Router,
    private datePipe: DatePipe,
    private serverExportService: ServerExportService,
    private activatedRoute: ActivatedRoute,
    private routingService: RoutingService,
    private docTypesExportService:DocTypesExportService,
    private docTypesService:DocTypesService,
    private utilService: UtilService,
    private userSettingsService: UserSettingsService,
    private dialogService: DialogService,
    private urlAuthorizationService : UrlAuthorizationService,
    private tagTypesService: TagTypesService) {
    super(router, activatedRoute);
  }

  ngOnInit() {
    this.setActionItems();
    this.setTreeGridEvent();
    this.setSelectedItem();
    this.setTreeGridEvent();
    this.loadData();
  }

  //----------------------------------------

  setSelectedItem() {
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (queryParams) {
      this.selectedItemId = parseInt(queryParams[RoutePathParams.findId] || queryParams[RoutePathParams.selectedItemId], 10);
    }
  }

  //----------------------------------------

  setTreeGridEvent() {
    this.treeGridEvents = {
      onItemSelected: this.onItemSelected,
    };
  }

  //----------------------------------------

  loadData() {
    let queriesArray = [];

    queriesArray.push(this.tagTypesService.loadTagTypes({}));
    queriesArray.push(this.docTypesService.loadTreeGridDocTypes({selectedItemId:this.selectedItemId}));
    queriesArray.push(this.userSettingsService.getFromLocalStorageUser(this.selectedTagTypeKey));

    forkJoin(queriesArray).subscribe(([tagTypes, docTypes, storedTagTypeId]) => {

      this.tagTypes = tagTypes;
      this.docTypes = docTypes;
      this.totalElements = docTypes.totalElements;

      this.isMngTagConfigUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngTagConfig]);
      this.isMngDocTypeConfigUser = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.MngDocTypeConfig]);

      this.updateSelectedTagTypeId(storedTagTypeId);
      this.showLoader(false);
    });
  }

  //---------------------------------------
  // actions
  //---------------------------------------

  toggleFlags() {
    this.showFlags = !this.showFlags;
  }

  //---------------------------------------

  onSaveChangesClick() {
    this.saveChanges();
  }

  //---------------------------------------

  toggleTagType(tagTypeId) {

    if (!_.isEmpty(this.docTypesChanges)) {
      this.dialogService.openConfirm('Save changes', `The changes done on Tag type '${this.selectedTagType.fileTagTypeDto.name}' have not been saved.<br><br> Save changes?`).then(result => {
        if (!result) {
          this.docTypesChanges = [];
          this.doTagToggling(tagTypeId);
        } else {
          this.saveChanges(()=> {
            this.doTagToggling(tagTypeId);
          });
        }
      });
    }else {
      this.doTagToggling(tagTypeId);
    }
  }

  //---------------------------------------

  openNewDocTypeDialog() {

    let data = { data: { parentDocType: this.selectedItem}};
    let modelParams = {
      windowClass: 'add-docType-window'
    };

    this.dialogService.openModalPopup(AddDocTypeDialogComponent, data, modelParams).result.then((data : any) => {
      if(data) {
        if(data.createAnother){
          this.openNewDocTypeDialog();
        }
        this.docTypesService.addNewDocType(data.docType).subscribe((addedDocType:DTODocType) => {
          this.onItemSelected(addedDocType.id, addedDocType);
          this.refreshGrid(() => {
            setTimeout(() => {
              this.treeGrid.openSelectedNode();
            },10);
          });
        },(err) => {this.showError(err)});
      }
    }, (reason) => {
    });
  }

  //--------------------------------------

  openEditDocTypeDialog() {
    let data = { data: { docType: this.selectedItem}};
    let modelParams = {
      windowClass: 'add-docType-window'
    };

    this.dialogService.openModalPopup(AddDocTypeDialogComponent, data, modelParams).result.then((data : any) => {
      this.docTypesService.editDocType(data.docType).subscribe((editedDocType:DTODocType) => {
          this.onItemSelected(editedDocType.id, editedDocType);
          this.refreshGrid();
        },(err) => {this.showError(err)});
    }, (reason) => {
    });
  }

  //---------------------------------------

  openDeleteDocTypeDialog() {

    this.docTypesService.getAssignedDocTypes(this.selectedItemId).subscribe((assignedItems) => {
      let msg = `You are about to delete DocType named: '${this.selectedItem.name}'` + (this.treeGrid.getItemChildren(this.selectedItemId).length > 0 ?  ' and its children' : '') + '. Continue?';
      this.dialogService.openConfirm('Confirmation', msg).then(result => {
        if (result) {
          this.docTypesService.deleteDocType(this.selectedItemId).subscribe(() => {
            let parent = this.treeGrid.getItemParent(this.selectedItemId);
            this.onItemSelected(parent.id, parent.item);
            this.refreshGrid();
          }, (err) => {
            this.showError(err)
          });
        }
      });
    });
  }

  //---------------------------------------

  onMoveClick() {
    this.movedItem = this.selectedItem;
    this.moveMode = true;
  }

  //---------------------------------------

  moveDocTypeAsFirstLevel () {
    let msg = `You have selected to move DocType '${this.selectedItem.name}' as top level DocType. Continue?`;
    this.dialogService.openConfirm('Confirmation', msg).then(result => {
      if (result) {
        this.moveDocType(this.selectedItem.id, null);
      }else {
        this.cancelMove();
      }
    });
  }

  //---------------------------------------

  cancelMove() {
    this.movedItem = null;
    this.moveMode = false;
  }

  //---------------------------------------

  exportData() {
    this.serverExportService.prepareExportFile('Doc_types_',ExportType.DOC_TYPE_SETTINGS,{},{
      itemsNumber : this.totalElements
    },false);
  }

  //----------------------------------------
  //  Called from docTypeItem
  //----------------------------------------


   onMoveTo(dest) {
    let isDestValid = this.movedItem.id !== dest.id && !_.includes(this.treeGrid.getItemChildren(this.movedItem.id), dest.id);

    if(!isDestValid) {
      this.dialogService.showAlert("Error", 'Destination is invalid.', AlertType.Error , null);
      this.onMovedEnd();
    } else {
      let msg = `You have selected to move DocType '${this.movedItem.name}' to be under '${dest.name}'. Continue?`;
      this.dialogService.openConfirm('Confirmation', msg).then(result => {
        if (result) {
          this.moveDocType(this.movedItem.id, dest.id);
        } else {
          this.cancelMove();
        }
      });
    }
  }


  //---------------------------------------
  // privates
  //---------------------------------------


  private onItemSelected = (itemId, item) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;

    this.routingService.updateQueryParamsWithoutReloading(this.activatedRoute.snapshot.queryParams,
      RoutePathParams.selectedItemId, this.selectedItemId);
  };

  //---------------------------------------
  private setActionItems() {
    // this.actionItems = [
    //   {
    //     type: ToolbarActionTypes.ADD,
    //     title: "Add new docType",
    //     actionFunc: this.openNewDocTypeDialog,
    //     disabled: false,
    //     href: ToolbarActionTypes.ADD,
    //     order: 0
    //   },
    //   {
    //     type: ToolbarActionTypes.EDIT,
    //     title: "Edit docType",
    //     actionFunc: this.openEditDialog,
    //     disabled: !this.selectedItem,
    //     href: ToolbarActionTypes.EDIT,
    //     order: 1
    //   },
    //   {
    //     type: ToolbarActionTypes.CLONE,
    //     title: "Clone department",
    //     actionFunc: this.openCloneDialog,
    //     disabled: !this.selectedItem || this.selectedItem.parentId != null,
    //     href: ToolbarActionTypes.CLONE,
    //     order: 2
    //   },
    //   {
    //     type: ToolbarActionTypes.DELETE,
    //     title: "Delete department",
    //     actionFunc: this.openDeleteDialog,
    //     disabled: !this.selectedItem,
    //     href: ToolbarActionTypes.DELETE,
    //     order: 3
    //   }
    // ];
    //
    // let exportDropDownItems = [{
    //   title: "As Excel",
    //   iconClass: "fa fa-file-excel-o",
    //   actionFunc: this.exportData
    // }];
    //
    // this.exportActionItem = {
    //   type: ToolbarActionTypes.EXPORT,
    //   title: "Export options",
    //   disabled: false,
    //   href: ToolbarActionTypes.EXPORT,
    //   children: exportDropDownItems
    // }
  }

  private getTagStyleId(tagType) {
      return this.tagTypesService.getTagTypeStyleId(tagType.id);
  }

  //---------------------------------------

  private updateSelectedTagTypeId(storedTagTypeId) {

    if(!_.isNull(storedTagTypeId)) {
      this.selectedTagType = _.filter(this.tagTypes, (tagType) => {
        return tagType.fileTagTypeDto.id === storedTagTypeId;
      })[0];
    }

    this.selectedTagTypeId = this.selectedTagType ? this.selectedTagType.fileTagTypeDto.id : null;
  }

  //---------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);


  //---------------------------------------

  private doTagToggling(tagTypeId) {
    this.updateSelectedTagTypeId(tagTypeId);
    this.userSettingsService.addToLocalStorageUser(this.selectedTagTypeKey, tagTypeId).subscribe();
  }

  //---------------------------------------

  private refreshGrid(callback?) {

    this.docTypesService.loadTreeGridDocTypes({selectedItemId:this.selectedItemId}).subscribe((docTypes) => {
      this.docTypes = docTypes;
      if(callback) {
        callback();
      }
    })
  }

  //---------------------------------------

  private onTagItemSelected = (docTypeId, tagType) => {

    let isDiff = this.diffChanges(tagType.originalTagItems, tagType.tagItems);
    if(isDiff) {
      let docType = _.filter(this.docTypesChanges, (docType) => {
        return docType.id == docTypeId;
      })[0];
      if(_.isEmpty(docType)) {
        docType = {
          id: docTypeId,
        };
        this.docTypesChanges.push(docType);
      }
      (<any>docType).tagTypes = [];
      docType.tagTypes.push(tagType);
    }
    else {
      this.removeTagChanges(docTypeId);
    }
  };

  //---------------------------------------

  private diffChanges(originalItems, items) {
    let oldIds = _.map(originalItems, (item) => {
      return item.id;
    });
    let newIds = _.map(items, (item) => {
      return item.id;
    });
    return !_.isEqual(oldIds.sort(), newIds.sort())
  }

  //---------------------------------------

  private removeTagChanges(docTypeId) {
    _.remove(this.docTypesChanges, (docType) => {
      return (<any>docType).id == docTypeId
    });
  }

  //---------------------------------------

  private saveChanges(cb?) {
    this.doSaving = true;

    let onRefreshEnd = () => {
      this.doSaving = false;
      this.docTypesChanges = [];
      if(_.isFunction(cb)) {
        cb();
      }
    };

    if(!_.isEmpty(this.docTypesChanges)) {
      this.docTypesService.saveDocTypeTags(this.docTypesChanges).subscribe(() => {
        this.refreshGrid(() => {
          onRefreshEnd();
        });
      });
    } else {
      this.refreshGrid(() => {
        onRefreshEnd();
      });
    }
  }

  //---------------------------------------

  private moveDocType(srcId, destId) {
    this.docTypesService.moveDocType(srcId, destId).subscribe(() => {
      this.onMovedEnd();
    },(err) => {this.showError(err)});
  }

  //---------------------------------------

  private onMovedEnd() {
    _.defer(() => {
      this.onItemSelected(this.movedItem.id, this.movedItem);
      this.refreshGrid();
      this.cancelMove();
    });
  }

  //--------------------------------------

  showError(err) {
    if (err.status === 400 && err.error.type === ErrorCodes.ITEM_ALREADY_EXISTS) {
       this.dialogService.showAlert("Error", 'DocType name must be unique.', AlertType.Error, null);
    } else {
      this.dialogService.showAlert("Error", err.error ? err.error.message : 'Sorry, an error occurred.', AlertType.Error, null);
    }
  }

  filterCountChange(newCount:number) {
    this.filterCount = newCount;
  }

  onSearchChanged = (newText:string) => {
    this.localFilterFunction = (newText ? (item)=> {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };



  //---------------------------------------
  // destroy
  //---------------------------------------

  ngOnDestroy() {
  }
}

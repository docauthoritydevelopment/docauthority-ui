import * as _ from 'lodash';
import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, ElementRef} from '@angular/core';
import {NgbActiveModal, NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {DialogBaseComponent} from '@shared-ui/components/modal-dialogs/base/dialog-component.base';
import {DocTypeDto} from "@app/common/discover/docTypes/types/docTypes.dto";
import { NgForm} from '@angular/forms';

@Component({
  selector: 'add-docType-dialog',
  templateUrl: './addDocTypeDialog.component.html',
  styleUrls: ['./_addDocTypeDialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AddDocTypeDialogComponent extends DialogBaseComponent implements OnInit, OnDestroy {

  editMode;
  parentId;
  parentName;
  inputText;
  title;
  id;
  description;
  singleValue = false;

  @ViewChild('docTypeForm') public docTypeForm: NgForm = null;
  @ViewChild('docTypeName') private docTypeNameRef: ElementRef;

  constructor(private activeModal: NgbActiveModal) {
     super();
  }

  //------------------------------------

  ngOnInit() {
    if(_.has(this.data, 'docType')) {
      this.setDocType();
    }
    else if(_.has(this.data, 'parentDocType')) {
      this.setParentType();
    }
  }

  //------------------------------------

  ngAfterViewInit(): void {
    this.docTypeNameRef.nativeElement.focus();
  }

  //------------------------------------

  setDocType() {
    let docType =  (<DocTypeDto>this.data)['docType'];

    this.editMode = true;
    this.title='Edit DocType';
    this.id = docType.id;
    this.inputText = docType.name;
    this.description = docType.description;
    this.singleValue = docType.singleValue;
  }

  //------------------------------------

  setParentType() {
    let parent = (<any>this.data).parentDocType;

    this.editMode = false;
    this.parentName = parent.name;
    this.parentId = parent.id;
    this.title =  `Add DocType`;

    if (this.parentName && this.parentName!='') {
      this.title=`Add DocType `;
    } else {
      this.title='Add DocType';
    }
  }

  //------------------------------------

  removeParent() {
    this.parentId = null;
    this.docTypeNameRef.nativeElement.focus();
  }

  //------------------------------------

  save(createAnother?:boolean){
    if(this.docTypeForm.valid)
    {
      this.inputText =  this.inputText?( this.inputText.replace(new RegExp("[;,:\"']", "gm"), " ")):null;

      let docType = new DocTypeDto();
      docType.description = this.description;
      docType.name = this.inputText.trim();
      docType.parentId = this.parentId;
      docType.singleValue = this.singleValue;
      docType.id = this.id;

      this.activeModal.close({
        docType: docType,
        createAnother:createAnother
      });
    }
  };

  //------------------------------------

  cancel(){
    this.activeModal.close(null);
  };

  ngOnDestroy() {
  }
}

import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {TreeGridAdapterMapper} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {DocTypeDetailsDto, DocTypeDto} from "@app/common/discover/docTypes/types/docTypes.dto";
import {TagTypesService} from "@app/features/settings/_common/tag-types/tag-types.service";


@Injectable()
export class DocTypesService {

  private pipe;
  private max_docType_colors = 18;

  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private tagTypesService: TagTypesService) {
    this.pipe = new DatePipe('en-US');
  }

  //----------------------------------------
  // loadTreeGridDocTypes
  //----------------------------------------

  loadTreeGridDocTypes(params) {
    const selectedItemId = params.selectedItemId;

    return this.http.get(`api/doctype/list`, {params}).pipe(
      map((data: DTOPagingResults<DocTypeDto>) => {
        this.adjustData(data);
        return this.treeGridAdapter.adapt(
          data,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
      })
    )
  }

  //----------------------------------------

  adjustData(data) {
    data.content = _.orderBy(data.content, ['name'],['asc']);

    _.forEach(data.content, (docTypeInfo) => {
      docTypeInfo.tagItems = docTypeInfo.tagItems || [];
      _.forEach(docTypeInfo.fileTagTypeSettings, (fileTagSettings) => {
        _.forEach(fileTagSettings.fileTags, (fileTag) => {
          this.addTagItem(docTypeInfo,fileTagSettings, fileTag);
        });
      });
    });
  }


  //----------------------------------------
  // addTagItem
  //----------------------------------------

  addTagItem(docTypeInfo, fileTagSettings, fileTag) {
    docTypeInfo.tagItems.push( {
      id: fileTag.id,
      typeId: fileTagSettings.fileTagType.id,
      name: `${fileTag.name}`,
      typeName: `${fileTag.type.name}`,
      styleId: this.tagTypesService.getTagTypeStyleId(fileTag.type.id)
    })
  }


  //----------------------------------------
  // getDocTypeStyleId
  //----------------------------------------

  getDocTypeStyleId(docType)
  {
    var colors = this.max_docType_colors;
    if(docType.parents&& docType.parents.length>0)
    {
      var result = docType.parents[0]%colors+1;
      return result;
    }
    return docType.id%colors+1;
  }


  //----------------------------------------
  // getDocTypeDepth
  //----------------------------------------

  getDocTypeDepth = function(docType) {
    if (docType.parents && docType.parents.length > 0) {
      return docType.parents.length;
    }
    return 0;
  };


  //----------------------------------------
  // DocType Tags actions
  //----------------------------------------

  saveDocTypeTags(docTypesChanges) {
    let changes = this.buildDocTypesChanges(docTypesChanges);
    return this.http.put('api/doctype/associateTags', changes);
  }

  //--------------------------------------

  buildDocTypesChanges(docTypesChanges) {
    let changes = {};
    _.forEach(docTypesChanges,  (docType) => {
      changes[docType.id] = [];
      _.forEach(docType.tagTypes, (tagType) => {
        let tagList = _.map(tagType.tagItems, (tagItem) =>{
          return {
            id: tagItem.id
          };
        });
        changes[docType.id].push({
          fileTagType: {
            id: tagType.tagTypeId
          },
          fileTags: tagList
        });
      });
    });
    return changes;
  };


  //----------------------------------------
  // DocType actions
  //----------------------------------------

  addNewDocType(newDocType) {
    return this.http.put('api/doctype/', newDocType);
  }

  editDocType(docType) {
    return this.http.post(`api/doctype/${docType.id}`, docType);
  }

  getAssignedDocTypes(docTypeId) {
    return this.http.get('/api/doctype/filecount').pipe(map((data: DTOPagingResults<any>)=> {
      return _.filter(data.content, (c) => {
        return c.item.id === docTypeId;
      })[0];
    }));
  }

  deleteDocType(docTypeId) {
    return this.http.delete(`/api/doctype/${docTypeId}?recursive=true`);
  }

  moveDocType(srcId, destId) {
    if(destId) {
      return this.http.post(`/api/doctype/${srcId}/move?newParentId=${destId}`, {id:srcId, newParentId: destId});
    }
    return this.http.post(`/api/doctype/${srcId}/move`, {id:srcId});
  }

  getDocTypeDetails(docTypeId) {
    return this.http.get(`/api/doctype/${docTypeId}?recursive=true`).pipe(map((data: DTOPagingResults<DocTypeDetailsDto>)=> {
      return data;
    }));
  }

  exportData(data) {
    return new Observable((observer) => {
        observer.next();
    });
  }
}

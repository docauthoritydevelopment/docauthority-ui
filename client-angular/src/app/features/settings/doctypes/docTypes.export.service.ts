import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ExcelService} from '@core/services/excel.service';

@Injectable()
export class DocTypesExportService {

  constructor(
    private excelService: ExcelService) {
  }

  //--------------------------------------------

  private setDocTypeInfo(docType:any, tagTypes) {
    let resItem = <any>{};

    resItem.ID =  docType.item.id;
    resItem.Name = docType.item.name;
    resItem.UniqueName = docType.item.uniqueName;
    resItem.ParentID = docType.item.parentId;

    _.forEach(tagTypes, (tagType) => {
      resItem[tagType.fileTagTypeDto.name] = this.getTagItem(docType, tagType.fileTagTypeDto.id);
    });

    return resItem;
  }

  //--------------------------------------------

  private getTagItem(docType, tagTypeId) {

    let res = [];

    let tagType = _.filter(docType.item.fileTagTypeSettings, (tagType) => {
      return tagType.fileTagType.id === tagTypeId;
    })[0];

    if(tagType) {
      _.forEach(tagType.fileTags, (fileTag) => {
        res.push(fileTag.name);
      });
    }
    return res.join(',');
  }
}

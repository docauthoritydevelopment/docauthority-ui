import * as _ from 'lodash';
import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DocTypeDto} from "@app/common/discover/docTypes/types/docTypes.dto";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {ESystemRoleName} from "@users/model/system-role-name";
import {DocTypesService} from "@app/features/settings/doctypes/docTypes.service";

declare let $: any;

@Component({
  selector: 'docType-item-grid',
  templateUrl: './docType-item-grid.component.html',
  styleUrls: ['./_docType-item-grid.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocTypeItemGridComponent extends TreeGridInnerItemBaseComponent<DocTypeDto> implements OnInit {

  isMngTagConfigUser = false;
  isMngDocTypeConfigUser = false;
  nameWidthStr: string = '' ;

  @ViewChild('el') el: ElementRef;

  constructor(
    private urlAuthorizationService: UrlAuthorizationService,
    private docTypesService:DocTypesService,) {
    super();
  }

  //---------------------------------------

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.nameWidthStr = (500 - this.getLeftPaddingWidth())+'px';
    this.isMngTagConfigUser = this.hostComponent.isMngTagConfigUser;
    this.isMngDocTypeConfigUser = this.hostComponent.isMngDocTypeConfigUser;
  }

  //---------------------------------------

  onTagItemsSelected = (docTypeId, tagType) => {
      this.hostComponent.onTagItemSelected(docTypeId, tagType);
  };

  //---------------------------------------

  getDocTypeStyleId(docType) {
    return this.docTypesService.getDocTypeStyleId(docType);
  }

  //---------------------------------------

  getDocTypeDepth(docType) {
    return this.docTypesService.getDocTypeStyleId(docType);
  }

  //---------------------------------------

  onModalClick(){
    this.hostComponent.onMoveTo({
      id: this.dataItem.item.id,
      name: this.dataItem.item.name
    });
  }
}

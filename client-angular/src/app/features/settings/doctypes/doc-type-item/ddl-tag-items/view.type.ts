export class DtoTagItem {
  id: number;
  name: string;
  styleId: number;
  typeId: number;
}

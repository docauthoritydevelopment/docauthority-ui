import * as _ from 'lodash';
import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TagTypesService} from '@app/features/settings/_common/tag-types/tag-types.service';
import {DTOFileTagTypeAndTags} from "@app/features/settings/_common/tag-types/tag-types.dto";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AddTagItemDialogComponent} from "@app/features/settings/_common/tag-item/add-tag-item-dialog/addTagItemDialog.component";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import {DtoTagItem} from './view.type';

declare let $: any;

@Component({
  selector: 'ddl-tag-items',
  templateUrl: './ddl-tag-items.component.html',
  styleUrls: ['./_ddl-tag-items.scss'],
  encapsulation: ViewEncapsulation.None
})


export class DdlTagItemsComponent implements OnInit, OnChanges {

  tagItems;
  styleId = 0;
  searchedText;
  selectedItems= {};
  originalItemsArr = [];
  selectedItemsArr = [];
  filteredTagItemsArr = [];
  recentlyUsedTagItemsArr = [];

  @Input() docType;
  @Input() selectedTagTypeId;
  @Input() onTagItemSelected;
  @Input() isMngTagConfigUser;

  @ViewChild('dropdown') public dropdown: NgbDropdown;

  constructor(private el: ElementRef,
              private tagTypesService: TagTypesService,
              private dialogService : DialogService) {
  }

  //-------------------------------------------

  ngOnInit(): void {

  }

  //-------------------------------------------

  ngOnChanges(changes: any): void {
    if(changes.selectedTagTypeId) {
      if(this.tagTypesService.tagTypes) {
        this.setStyleId();
        this.setTagItems();
        this.setSelectedItem();
      }
    }
  }

  //-------------------------------------------

  setStyleId() {
    this.styleId = this.tagTypesService.getTagTypeStyleId(this.selectedTagTypeId);
  }

  //-------------------------------------------

  setTagItems() {
    let tagItem = _.filter(this.tagTypesService.tagTypes, (tagItem: DTOFileTagTypeAndTags) => {
      return tagItem.fileTagTypeDto.id === this.selectedTagTypeId;
    })[0];
    if(tagItem) {
      this.tagItems = tagItem.fileTags;
      this.filteredTagItemsArr = this.tagItems;
    }
  }

  //-------------------------------------------

  onDropDownOpen() {
    this.clearSearchText();
    this.updateRecentlyUsedTagItems();
  }

  //-------------------------------------------

  updateRecentlyUsedTagItems() {
    this.tagTypesService.getRecentlyItemsUsed(this.selectedTagTypeId, (itemsIds) => {
      this.recentlyUsedTagItemsArr = _.filter(this.tagItems, (tagItem:DtoTagItem) => {
        return _.includes(itemsIds, tagItem.id);
      });
    });
  }

  //-------------------------------------------

  setSelectedItem() {
    this.selectedItemsArr = _.filter(this.docType.tagItems, (tagItem: DtoTagItem) => {
      return tagItem.typeId === this.selectedTagTypeId;
    });

    this.originalItemsArr = this.selectedItemsArr.slice();

    _.forEach(this.selectedItemsArr, (tag) => {
      this.selectedItems[tag.id] = true;
    });
  }

  //-------------------------------------------

  searchTextChange() {
    let _searchText = this.searchedText || '';

    if (_.isEmpty(_searchText)) {
      this.filteredTagItemsArr = this.tagItems;
    } else {
      _searchText = _searchText.toLowerCase();
      this.filteredTagItemsArr = _.filter(this.tagItems, (tagItem: DtoTagItem) => {
        return tagItem.name.toLowerCase().indexOf(_searchText) === 0;
      });
    }
  }

  //-------------------------------------------

  clearSearchText() {
    this.searchedText = '';
    this.filteredTagItemsArr = this.tagItems;
  }

  //---------------------------------------

  toggleFileTag($event, tagItem) {

    if(this.selectedItems[tagItem.id]) {
      this.removeTagItem(tagItem);
    } else {
      this.addTagItem(tagItem);
    }
    $('body').click();
  }

  //---------------------------------------

  addTagItem(tagItem) {
    this.selectedItems[tagItem.id] = true;

    if(tagItem.type.singleValueTag) {
      if(!_.isEmpty(this.selectedItemsArr)) {
        this.removeTagItem(this.selectedItemsArr[0]);
      }
    }
    this.selectedItemsArr.push(tagItem);
    this.updateTagChange(tagItem);

    this.tagTypesService.updateRecentlyUsedTagItems(this.selectedTagTypeId, tagItem.id, () => {
      this.updateRecentlyUsedTagItems();
    });
  }

  //---------------------------------------

  removeTagItem(tagItem) {
    delete this.selectedItems[tagItem.id];

    this.selectedItemsArr = _.filter(this.selectedItemsArr, (item) => {
      return item.id !== tagItem.id
    });
    this.updateTagChange(tagItem);
  }

  //--------------------------------------

  updateTagChange(tagItem) {

    this.onTagItemSelected(this.docType.id,  {
      tagTypeId: tagItem.typeId || tagItem.type.id,
      tagItems: this.selectedItemsArr,
      originalTagItems: this.originalItemsArr
    });
  }

  //-------------------------------------------

  addNewTagClick() {

    this.dropdown.close();

    let data = { data: { tagTypeId: this.selectedTagTypeId, stopPropagation: true}};
    let modelParams = {
      windowClass: 'add-tagItem-window'
    };

    this.dialogService.openModalPopup(AddTagItemDialogComponent, data, modelParams).result.then((data : any) => {
      this.dropdown.open();

      if(data) {
        this.tagTypesService.addTagItem(data.tagItem).subscribe(() => {
          this.tagTypesService.loadTagTypes({}).subscribe(()=> {
            this.setTagItems();
          },() => {this.showError()})
        },() => {this.showError()});

        if(data.createAnother) {
          this.addNewTagClick();
        }
      }
    });
  }

  //-----------------------------------------

  showError() {
    this.dialogService.showAlert("Error", 'Sorry, an error occurred.', AlertType.Error);
  }
}

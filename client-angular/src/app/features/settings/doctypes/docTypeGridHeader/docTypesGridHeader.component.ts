import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'docTypes-grid-header',
  templateUrl: './docTypesGridHeader.component.html',
  styleUrls: ['./docTypesGridHeader.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocTypesGridHeaderComponent{
  constructor() {
  }
}

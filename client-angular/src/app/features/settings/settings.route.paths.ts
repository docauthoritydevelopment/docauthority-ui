import {UrlRoles} from "@app/navigation/config/url-roles";
import {ESystemRoleName} from "@users/model/system-role-name";

export enum SettingsRoutePaths {
  scheduleGroups= 'ScheduleGroups',
  doctypes= 'DocTypes',
  dateFilters = 'DateFilters',
  searchPatterns = 'SearchPatterns',
  patternCategories = 'PatternCategories',
  regulations = 'Regulations',
  departments = 'Departments',
  matters = 'Matters',
  logLevels = 'LogLevels',
  savedFilters = 'SavedFilters',
  savedViews = 'SavedViews',
  savedCharts = 'SavedCharts',
  savedWidgets = 'SavedWidgets',
  savedDashboards = 'SavedDashboards'
}


export const SettingsRoutePathsRoles: UrlRoles[] =
  [
    {url: SettingsRoutePaths.scheduleGroups, roles: [ESystemRoleName.None]},
    {url: SettingsRoutePaths.doctypes, roles: [ESystemRoleName.ViewDocTypeTree]},
    {url: SettingsRoutePaths.dateFilters, roles: [ESystemRoleName.ViewDocTypeTree]},
    {url: SettingsRoutePaths.patternCategories, roles: [ESystemRoleName.ViewDocTypeTree]},
    {url: SettingsRoutePaths.regulations, roles: [ESystemRoleName.ViewDocTypeTree]},
    {url: SettingsRoutePaths.searchPatterns, roles: [ESystemRoleName.GeneralAdmin]},
    {url: SettingsRoutePaths.departments, roles: [ESystemRoleName.GeneralAdmin,ESystemRoleName.TechSupport]},
    {url: SettingsRoutePaths.matters, roles: [ESystemRoleName.GeneralAdmin,ESystemRoleName.TechSupport]},
    {url: SettingsRoutePaths.logLevels, roles: [ESystemRoleName.GeneralAdmin,ESystemRoleName.TechSupport]},
    {url: SettingsRoutePaths.savedFilters, roles: [ESystemRoleName.None]},
    {url: SettingsRoutePaths.savedViews, roles: [ESystemRoleName.None]},
    {url: SettingsRoutePaths.savedCharts, roles: [ESystemRoleName.ViewReports]},
    {url: SettingsRoutePaths.savedWidgets, roles: [ESystemRoleName.None]},
    {url: SettingsRoutePaths.savedDashboards, roles: [ESystemRoleName.None]}
  ];

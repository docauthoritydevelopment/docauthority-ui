import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {PatternCategoryDto, TextSearchPatternDto} from "@app/common/settings/types/searchPattern.dto";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";




@Component({
  selector: 'pattern-categories-item-grid',
  templateUrl: './pattern-categories-item-grid.component.html',
})
export class PatternCategoriesItemGridComponent extends TreeGridInnerItemBaseComponent<PatternCategoryDto> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;
  inSetActive:boolean = false;

  regulationStr: string = "";

  constructor(private searchPatternService : SearchPatternService) {
    super();
  }

  setActive(isActive:boolean) {
    this.inSetActive = true;
    this.searchPatternService.setPatternCategoriesActiveState(this.dataItem.item.id,isActive).subscribe((ansObject: TextSearchPatternDto)=>{
      this.dataItem.item.active = ansObject.active;
      this.inSetActive = false;
    }, (err)=> {
      this.inSetActive = false;
    });
  }

  ngOnInit(): void {
  }

  expandOrCollapseNode = ()=>  {
    if (this.dataItem.isOpen || this.dataItem.item.patterns!= null) {
      this.dataItem.isOpen = !this.dataItem.isOpen
    }
  }

}


import * as _ from 'lodash'
import {forkJoin} from "rxjs";
import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PatternCategoriesItemGridComponent} from "./item-grid/pattern-categories-item-grid.component";
import {BaseComponent} from "@core/base/baseComponent";
import {PatternCategoryDto, RegulationDto} from "@app/common/settings/types/searchPattern.dto";
import {
  ITreeGridEvents,
  TreeGridConfig,
  TreeGridData,
  TreeGridDataItem,
  TreeGridSortOrder
} from "@tree-grid/types/tree-grid.type";
import {MetadataService} from "@services/metadata.service";
import {SearchPatternService} from "@app/features/settings/services/searchPattern.service";
import {RoutingService} from "@services/routing.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {ToolbarActionTypes} from "@app/types/display.type";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {AddEditPatternCategoryDialogComponent} from "@app/features/settings/popups/addEditPatternCategoryDialog/addEditPatternCategoryDialog.component";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {PatternCategoriesGridHeaderComponent} from "@app/features/settings/patternCategories/grid-header/pattern-categories-grid-header.component";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {TreeGridComponent} from "@tree-grid/components/tree-grid.component";

@Component({
  selector: 'pattern-categories',
  templateUrl: './patternCategories.component.html',
  styleUrls: ['./patternCategories.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PatternCategoriesComponent extends BaseComponent {

  public loading: boolean = false;
  public categoryData: TreeGridData<PatternCategoryDto> = null;
  private isExporting: boolean = false;
  public PatternCategoriesItemGridComponent = PatternCategoriesItemGridComponent;
  public PatternCategoriesGridHeaderComponent = PatternCategoriesGridHeaderComponent;

  public  treeGridEvents: ITreeGridEvents<PatternCategoryDto>;
  private selectedItemId: number;
  public settingsActionItems: ButtonConfigDto[];
  public totalElements: number = 0;
  public exportActionItem: ButtonConfigDto;
  private selectedItem:PatternCategoryDto;
  private showChangedAlert: boolean = true;
  public initialSortDirection: TreeGridSortOrder = null;
  public initialSortFieldName: string = null;
  public filterCount : number;
  public localFilterFunction:any = null;

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<object>;




  constructor(private metadataService: MetadataService, private searchPatterService: SearchPatternService, private routingService: RoutingService, private activatedRoute: ActivatedRoute, private dialogService: DialogService, private router: Router, private urlAuthorizationService: UrlAuthorizationService) {
    super(router, activatedRoute);
  }



  daOnInit() {
    this.setActionItems();
    this.setTreeGridEvent();
    this.on(this.searchPatterService.patternCategoryRefreshedEvent$, () => {
      this.loadData(true);
    });
    this.on(this.searchPatterService.patternCategoryActiveChangedEvent$, () => {
      if (this.showChangedAlert) {
        this.dialogService.showAlert("Pattern category state changed", "Pattern category was activated.<br><br>In order for this change to take effect, full re-scan is needed.", AlertType.Info);
        this.showChangedAlert = false;
      }
    });
  }



  filterCountChange(newCount:number) {
    this.filterCount = newCount;
  }

  onSearchChanged = (newText:string) => {
    this.localFilterFunction = (newText ? (item)=> {
      return (item.name.toLowerCase().indexOf(newText.toLowerCase()) >= 0);
    } : null);
  };

  public searchBoxConfig : SearchBoxConfigDto  ={
    searchFunc: this.onSearchChanged,
    searchTerm:'',
    placeholder:'Search by name',
    notifyOnKeyup: false
  }

  private initTableSort(params:any) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    }
    let sortObject=JSON.parse(params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  routeChanged(){
    this.loadData(true);
  }

  //----------------------------------------

  loadData = (withLoader: boolean = false) => {
    if (withLoader) {
      this.showLoader(true);
    }
    let params = this.getQueryParams();
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    this.initTableSort(params);
    let queriesArray = [];
    queriesArray.push(this.searchPatterService.getPatternCategoryData(params, this.selectedItemId));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([cData]) => {
      this.categoryData = cData;
      this.totalElements = cData.totalElements;
      if (withLoader) {
        this.showLoader(false);
      }
    }));
  };


  exportData = ()=> {
    let params = _.cloneDeep(this.getQueryParams()) || {};
    this.searchPatterService.exportPatternCategoriesData(this.totalElements,params);
  }

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);


  private setActionItems() {
    this.settingsActionItems = [];
    let isSupportAllCrudOperation:boolean = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.ManageSearchPatterns]);
    if (isSupportAllCrudOperation) {
      this.settingsActionItems = [
        {
          type: ToolbarActionTypes.EDIT,
          title: "Edit pattern category",
          name: 'Edit',
          actionFunc: this.openEditDialog,
          disabled: !this.selectedItem,
          href: ToolbarActionTypes.EDIT,
          order: 0,
          btnType: 'link'
        },
        {
          type: ToolbarActionTypes.DELETE,
          title: "Delete pattern category",
          name: 'Delete',
          actionFunc: this.openDeleteDialog,
          disabled: !this.selectedItem,
          href: ToolbarActionTypes.DELETE,
          order: 1,
          btnType: 'link'
        }
      ];
    }

    let exportDropDownItems:DropDownConfigDto[] = [{
      title: "As Excel",
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: "Export options",
      disabled: this.isExporting,
      children: exportDropDownItems,
      href: ToolbarActionTypes.EXPORT
    }
  }

  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<PatternCategoryDto>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }

  private onItemSelected = (itemId, item) => {
    if (!this.selectedItem || this.selectedItemId != itemId) {
      this.selectedItemId = itemId;
      this.routingService.replaceQueryParamsWithoutReloading(this.getQueryParams(),
        RoutePathParams.selectedItemId, this.selectedItemId);
      this.updateLatestQueryParams(RoutePathParams.selectedItemId, this.selectedItemId);
    }
    this.selectedItem = item;
    this.setActionItems();
  };


  private onPageChanged = (pageNum) => {
  };


  private onSortByChanged = (sortObj: any) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.sort],[JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder)}])]);
  };


  /*
  findSelectedItemById(nodesList : TreeGridDataItem<any>[]){
    for (let count = 0 ; count < nodesList.length ; count++)
    {
      if (nodesList[count].item.id == this.selectedItemId) {
        return nodesList[count].item;
      }
      else if (nodesList[count].children && nodesList[count].children.length > 0){
        let ans = this.findSelectedItemById(nodesList[count].children);
        if (ans != null ) {
          return ans;
        }
      }
    }
  }
  */


  openEditDialog = ()=> {
    let data = { data: {
        patternCategory : this.selectedItem
      } };
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(AddEditPatternCategoryDialogComponent, data, modelParams);
  }


  openDeleteDialog = ()=> {
    this.dialogService.openConfirm('Confirmation', "You are about to delete pattern category named: '"+ this.selectedItem.name +"'. Continue?" ).then(result => {
      if (result) {
        this.searchPatterService.deletePatternCategory(this.selectedItem.id).subscribe();
      }
    });
  }


}

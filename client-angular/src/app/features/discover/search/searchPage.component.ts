import {Component,  ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {SearchPageService} from "@app/features/discover/services/searchPage.service";
import {forkJoin} from 'rxjs';
import * as _ from 'lodash';
import {FilterDescriptor} from "@app/features/discover/types/filter.dto";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {map, concatMap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs/index';
import {FilesItemGridComponent} from "@app/common/discover/components/item-grids/files-item-grid/files-item-grid.component";
import {ITreeGridEvents} from "@tree-grid/types/tree-grid.type";



@Component({
  selector: 'search-page',
  templateUrl: './searchPage.component.html',
  styleUrls: ['./searchPage.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SearchPageComponent extends BaseComponent{
  loading:boolean = false;
  _searchText:string='';
  selectedSearchIndex = null;
  searchStage = null;
  docTypeList = [];
  acOptions = [];
  valuesMap:any = {};
  currentData: any = null;
  FilesItemGridComponent = FilesItemGridComponent;
  treeGridEvents: ITreeGridEvents<any> = null;
  lastParams = null;



  startWords = [
    'docType',
    'group',
    'Tag',
    'ACL read',
    'file'
  ]

  startWordsHash = {
    'doctype':'file.docType',
    'group':'group.name',
    'tag' : '',
    'acl read' : '',
    'file' :'contentFilter'
  }

  startOperationsMap: any = {
    'doctype':['is','=','is not','!='],
    'group':['is','=','is not','!='],
    'tag' : ['is','=','is not','!='],
    'acl read' : ['is','=','is not','!='],
    'file' :['contains']
  }

  relationWords = [
    'is',
    '=',
    'is not',
    '!=',
    'contains'
  ]

  relationWordsHash = {
    'is' : 'eq',
    '=' : 'eq' ,
    'is not' : 'neq' ,
    '!=' : 'neq' ,
    'contains' : 'contains'
  }

  connectWords = [
    'and',
    'or',
    ','
  ]

  connectWordsHash = {
    'and': 'and',
    'or': 'or',
    ',': 'and'
  }

  SEARCH_STAGES =  {
    START_QUERY : 'startQuery',
    OPERATOR : 'operator',
    VALUE : 'value',
    CONNECTOR : 'connector'
  }

  valueType:string = null;


  constructor(private searchPageService : SearchPageService, private dialogService: DialogService) {
    super();
  }


  loadInitialData() {
    this.valuesMap['docType'] = [];
    let queriesArray = [];
    queriesArray.push(this.searchPageService.getDoctypesListQuery());
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([dtList]) => {
      dtList.content.forEach((dtItem:any)=> {
        this.valuesMap['docType'][dtItem.name.toLowerCase()] = dtItem;
      })
      this.docTypeList  = dtList.content.map(docTypeItem => docTypeItem.name);
    }));
  }

  insertWord = (theWord)=>{
    let lastCharIsSpace =  this.searchText=='' || this.searchText.substring(this.searchText.length - 1 ) == ' ';
    let allWords = this.searchText.split(' ');
    allWords = allWords.filter(item => item !='');
    let lastWord = allWords.length > 0 ? allWords[allWords.length - 1] : null;
    this.acOptions = [];
    this.valueType = null;
    this.searchStage = null;
    if (lastWord && theWord.toLowerCase().startsWith(lastWord.toLowerCase())) {
      if (lastCharIsSpace) {
        this.searchText = this.searchText + theWord.substring(lastWord.length+1);
      }
      else {
        this.searchText = this.searchText + theWord.substring(lastWord.length);
      }
    }
    else {
      this.searchText = this.searchText + theWord;
    }
  }

  searchKeyDown = (event)=> {
    if (event.key == "Enter") {
      event.preventDefault();
      if (this.selectedSearchIndex != null && this.acOptions.length>0) {
        this.insertWord(this.acOptions[this.selectedSearchIndex]);
      }
      else {
        this.doSearch();
      }
    }
    if (event.key == 'ArrowDown') {
      if (this.selectedSearchIndex == null) {
        this.selectedSearchIndex = 0 ;
      }
      else if (this.selectedSearchIndex < this.acOptions.length-1 ) {
        this.selectedSearchIndex ++ ;
      }
      event.preventDefault();
    }
    if (event.key == 'ArrowUp') {
      if (this.selectedSearchIndex != null) {
        if (this.selectedSearchIndex == 0) {
          this.selectedSearchIndex = null;
        }
        else {
          this.selectedSearchIndex --;
        }
      }
      event.preventDefault();
    }
  }

  set searchText(newText:string) {
    this._searchText = newText;
    this.selectedSearchIndex = null;
    this.updayeACoptions();
  }

  get searchText(): string {
    return this._searchText;
  }

  selectACItem(acOption) {
    this.insertWord(acOption);
  }

  updayeACoptions = ()=> {
    let lastCharIsSpace = this.searchText == '' || this.searchText.substring(this.searchText.length - 1) == ' ';
    let allWords = this.searchText.split(' ');
    allWords = allWords.filter(item => item != '');
    let lastWord = allWords.length > 0 ? allWords[allWords.length - 1] : null;
    let beforeWord = allWords.length > 1 ? allWords[allWords.length - 2] : null;

    if (lastCharIsSpace) {
      let foundBeforeWord: boolean = false;
      let additionOption:string = null;
      if (lastWord) {
        let tmpOptions = this.acOptions.filter(acOption => acOption.toLowerCase().indexOf(lastWord.toLowerCase() + ' ') == 0)
        if (tmpOptions.length > 0) {
          if (tmpOptions.length == 1 && lastWord.toLowerCase() == 'is') {
            additionOption = 'not';
          }
          else {
            this.acOptions = tmpOptions;
            foundBeforeWord = true;
          }
        }
      }
      if (!foundBeforeWord) {
        this.searchStage = null;
        if (allWords.length == 0 || this.connectWordsHash[allWords[allWords.length-1]]) {
          this.acOptions = this.startWords;
          this.searchStage = this.SEARCH_STAGES.START_QUERY;
        }
        else {
          let lastIndex = allWords.length-1;
          let typeIndex = null;
          let theType = null;

          if (this.relationWordsHash[allWords[lastIndex].toLowerCase()]) {
            typeIndex = lastIndex - 1 ;
          }
          else if (allWords.length > 2) {
            if (this.relationWordsHash[allWords[lastIndex-1].toLowerCase()+' '+allWords[lastIndex].toLowerCase()]) {
              typeIndex = lastIndex - 2;
            }
          }
          if (typeIndex !== null && typeIndex >= 0 ) {
            if (this.startWordsHash[allWords[typeIndex].toLowerCase()])
            {
              theType = allWords[typeIndex].toLowerCase();
            }
            else if (typeIndex>0 && this.startWordsHash[allWords[typeIndex -1].toLowerCase()+' '+allWords[typeIndex].toLowerCase()]) {
              theType = allWords[typeIndex -1].toLowerCase()+' '+allWords[typeIndex].toLowerCase();
            }
            this.searchStage = this.SEARCH_STAGES.VALUE;
            this.valueType = null;
            if (theType.toLowerCase() == 'doctype') {
              this.acOptions = _.clone(this.docTypeList);
            }
            if (theType.toLowerCase() == 'group') {
              this.acOptions = [];
              this.valueType = 'group'
            }
            if (theType.toLowerCase() == 'file') {
              this.acOptions = [];
              this.valueType = 'file'
            }
          }
          if (!this.searchStage) {
            if (this.startWordsHash[allWords[lastIndex].toLowerCase()]) {
              this.acOptions  = this.startOperationsMap[allWords[lastIndex].toLowerCase()];
              this.searchStage = this.SEARCH_STAGES.OPERATOR;
            }
            else if (allWords.length > 1 && this.startWordsHash[allWords[lastIndex-1].toLowerCase()+' '+allWords[lastIndex].toLowerCase()]) {
              this.acOptions  = this.startOperationsMap[allWords[lastIndex-1].toLowerCase()+' '+allWords[lastIndex].toLowerCase()];
              this.searchStage = this.SEARCH_STAGES.OPERATOR;
            }
          }
          if (!this.searchStage) {
            this.acOptions  = this.connectWords;
            this.searchStage = this.SEARCH_STAGES.CONNECTOR
          }
        }
        if (additionOption) {
          this.acOptions.unshift(additionOption);
        }
      }
    }
    else {
      if (this.searchStage == this.SEARCH_STAGES.VALUE && this.valueType == 'group') {
        this.acOptions = ['Loading....'];
        this.searchPageService.getFileGroupQuery(lastWord).subscribe((resList)=> {
          this.acOptions = resList.map(resItem => resItem.term).filter(resItem => resItem.startsWith(lastWord));
        });
      }
/*       else if (this.searchStage == this.SEARCH_STAGES.VALUE && this.valueType == 'file') {
        this.acOptions = ['Loading....'];
        this.searchPageService.getContainTextQuery(lastWord).subscribe((resList)=> {
          this.acOptions = resList.map(resItem => resItem.term).filter(resItem => resItem.startsWith(lastWord));
        });
      }  */
      else {
        if (!beforeWord) {
          this.acOptions = this.startWords;
        }

        let tmpOptions = [];
        if (beforeWord) {
          tmpOptions = this.acOptions.filter(acOption => acOption.toLowerCase().indexOf(beforeWord.toLowerCase() + ' ' + lastWord.toLowerCase()) == 0)
        }
        if (tmpOptions.length > 0) {
          this.acOptions = tmpOptions;
        }
        else {
          this.acOptions = this.acOptions.filter(acOption => acOption.toLowerCase().indexOf(lastWord.toLowerCase()) == 0)
        }
      }
    }
    if (this.acOptions.length == 1) {
      this.selectedSearchIndex = 0 ;
    }
  }

  daOnInit() {
    this.treeGridEvents = {
      onPageChanged: this.onPageChanged
    };
    this.loadInitialData();
  }

  setIncorrectFilterMsg(theMsg) {
    this.dialogService.showAlert("Parsing error", theMsg, AlertType.Error );
  }

  getFilter = (searchText): Observable<FilterDescriptor> => {
    return Observable.create((observer)=>  {
      let goText: string = searchText;
      let nextOperation = null;
      let prevOpearion = null;
      let queriesArray = [];
      let resultFilter = null;

      while (goText != '') {
        let connectIndex = goText.length;

        this.connectWords.forEach((connWord) => {
          let theIndex = goText.indexOf(connWord + ' ');
          if (theIndex != -1 && theIndex < connectIndex) {
            connectIndex = theIndex;
            nextOperation = connWord;
          }
        });

        let filterText = goText.substring(0, connectIndex);
        let allWords = filterText.split(' ');
        allWords = allWords.filter(item => item != '');
        let goIndex = 0;
        let fDescriptor: FilterDescriptor = new FilterDescriptor();


        if (this.startWordsHash[allWords[0].toLowerCase() + ' ' + allWords[1].toLowerCase()]) {
          fDescriptor.field = this.startWordsHash[allWords[0].toLowerCase() + ' ' + allWords[1].toLowerCase()];
          goIndex = 2;
        }
        else if (this.startWordsHash[allWords[0].toLowerCase()]) {
          fDescriptor.field = this.startWordsHash[allWords[0].toLowerCase()];
          goIndex = 1;
        }
        else {
          this.setIncorrectFilterMsg('Incorrect field "' + allWords[0] + '"');
          observer.next(null);
        }

        if (this.relationWordsHash[allWords[goIndex].toLowerCase() + ' ' + allWords[goIndex + 1].toLowerCase()]) {
          fDescriptor.operator = this.relationWordsHash[allWords[goIndex].toLowerCase() + ' ' + allWords[goIndex + 1].toLowerCase()];
          goIndex = goIndex + 2;
        }
        else if (this.relationWordsHash[allWords[goIndex].toLowerCase()]) {
          fDescriptor.operator = this.relationWordsHash[allWords[goIndex].toLowerCase()];
          goIndex = goIndex + 1;
        }
        else {
          this.setIncorrectFilterMsg('Incorrect operator "' + allWords[goIndex] + '"');
          observer.next(null);
        }

        allWords.splice(0, goIndex);
        let valueField: string = allWords.join(' ');

        if (fDescriptor.field == 'file.docType') {
          let valueObject = this.valuesMap['docType'][valueField.toLowerCase()];
          if (valueObject == null) {
            this.setIncorrectFilterMsg('Doctype with the name "'+valueField + '" is not available');
            observer.next(null);
          }
          if (valueObject.parentId == null) {
            fDescriptor.field = 'file.subDocType';
          }
          fDescriptor.value = valueObject.id;
        }
        else if (fDescriptor.field == 'group.name') {
          queriesArray.push(this.searchPageService.getFileGroupQuery(valueField).pipe(map((groupItems: any[]) => {
            let theFilterDescriptor = fDescriptor;
            groupItems.forEach((item => {
              if (item.term == valueField) {
                theFilterDescriptor.value = item.term;
              }
            }));
            if (theFilterDescriptor.value == null) {
              this.setIncorrectFilterMsg('Group with the name "'+valueField + '" is not available');
              observer.next(null);
            }
          })))
        }
        else {
          fDescriptor.value =  valueField;
        }

        if (resultFilter == null) {
          resultFilter = fDescriptor;
        }
        else {
          let newFilter : FilterDescriptor  = new FilterDescriptor();
          newFilter.logic = prevOpearion;
          newFilter.filters = [fDescriptor,resultFilter];
          resultFilter = newFilter;
        }

        if (connectIndex == goText.length) {
          goText = ''
        }
        else {
          goText = goText.substring(connectIndex + nextOperation.length + 1);
        }

        prevOpearion = nextOperation;
      }
      if (queriesArray.length ==0 ) {
        observer.next(resultFilter);
      }
      else {
        forkJoin(queriesArray).subscribe((results) => {
          observer.next(resultFilter);
        })
      }
    });
  }

  doSearch() {
    this.acOptions = [];
    this.showLoader(true);
    this.getFilter(this.searchText).subscribe((filter:FilterDescriptor)=> {
      if (filter!= null) {
        let paramObject = {
          filter: JSON.stringify(filter),
        }
        this.lastParams = paramObject;
        this.searchPageService.getFilesTable(paramObject).subscribe((res) => {
          this.currentData = res;
          this.showLoader(false);
        })
      }
      else {
        this.currentData = null;
        this.showLoader(false);
      }
    });
  }

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  onPageChanged = (newPage)=>  {
    this.lastParams.page = newPage;
    this.showLoader(true);
    this.searchPageService.getFilesTable(this.lastParams).subscribe((res) => {
      this.currentData = res;
      this.showLoader(false);
    })
  }

}


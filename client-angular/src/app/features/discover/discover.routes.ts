import {Routes} from '@angular/router';
import {DiscoverComponent} from '@app/features/discover/discover.component';
import {DiscoverRoutePaths} from '@app/features/discover/types/discover-route-pathes';
import {SearchPageComponent} from "@app/features/discover/search/searchPage.component";


export const discoverRoutes: Routes = [
  {
    path: '',
    component: DiscoverComponent,
    children: [
      {
        path: DiscoverRoutePaths.search,
        component: SearchPageComponent
      }
    ]
  }
];

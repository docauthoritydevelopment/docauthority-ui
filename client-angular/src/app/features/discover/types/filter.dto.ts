
export class  FilterDescriptor  {
  logic? : string;
  field? : string;
  value? : any ;
  operator? : string;
  ignoreCase?: boolean ;
  name? : string;
  id? : number;
  filters?: FilterDescriptor[];
}

import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, concatMap} from 'rxjs/operators';
import {Observable, forkJoin, Subject} from 'rxjs/index';
import {DTOPagingResults} from "@core/types/query-paging-results.dto";
import {UrlParamsService} from "@services/urlParams.service";
import {TreeGridAdapterMapper, TreeGridData} from "@tree-grid/types/tree-grid.type";
import {CrawlRunDetailsDto} from "@app/common/scan/types/scan.dto";
import {TreeGridAdapterService} from "@tree-grid/services/tree-grid-adapter.service";


@Injectable()
export class SearchPageService {


  constructor(private http: HttpClient,private urlParamsService: UrlParamsService,private treeGridAdapter : TreeGridAdapterService)
  {
  }

  getFilesTable(params : any ):  Observable<any>
  {
    params = this.urlParamsService.fixedPagedParams(params,30);
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    }
    return this.http.get('/api/file/dn', {params}).pipe(map((data: DTOPagingResults<any>)=> {
      let ans: TreeGridData<any> = this.treeGridAdapter.adapt(
        data,
        null,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }

  getDoctypesListQuery(): Observable<any> {
    return this.http.get('/api/doctype/list');
  }

  getFileGroupQuery(preffix:string): Observable<any> {
    return this.http.get('http://35.165.33.58:8983/solr/CatFiles/suggest?suggest=true&suggest.build=false&suggest.dictionary=mySuggester&wt=json&suggest.q='+ preffix).pipe(map((res)=>
      {
        return res['suggest']['mySuggester'][preffix]['suggestions'];
      }
    ));
  }

  getContainTextQuery(preffix:string): Observable<any> {
    return this.http.get('http://35.165.33.58:8983/solr/Extraction/suggest?suggest=true&suggest.build=true&suggest.dictionary=textSuggester&wt=json&suggest.q='+ preffix).pipe(map((res)=>
      {
        return res['suggest']['textSuggester'][preffix]['suggestions'];
      }
    ));
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {SplitPaneModule} from 'ng2-split-pane/lib/ng2-split-pane';
import {CommonDiscoverModule} from '@app/common/discover/common-discover.module';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {discoverRoutes} from '@app/features/discover/discover.routes';
import {DiscoverComponent} from '@app/features/discover/discover.component';
import {DiscoverRouteManager} from '@app/features/discover/discover.route.manager';
import {UtilService} from '@services/util.service';
import {SearchPageComponent} from "@app/features/discover/search/searchPage.component";
import {SearchPageService} from "@app/features/discover/services/searchPage.service";
import {DiscoverSplitPageComponent} from "@app/features/discover/discover-split-page/discover-split-page.component";

@NgModule({
  imports: [
    CommonModule,
    CommonDiscoverModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    LoaderModule,
    TreeGridModule,
    NgbModule,
    RouterModule.forChild(discoverRoutes)
  ],
  declarations: [
    DiscoverComponent,
    DiscoverSplitPageComponent,
    SearchPageComponent,
  ],
  providers: [
    DiscoverRouteManager,
    UtilService,
    SearchPageService
  ],
  entryComponents: [
  ],
  exports : [
  ]
})
export class DiscoverModule {
  constructor() {
  }
}


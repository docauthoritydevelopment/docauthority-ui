import {EventEmitter, Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map} from 'rxjs/internal/operators';

declare var _: any;

export enum DiscoverRouteEvents {
  STATE_CHANGED
}

export enum DiscoverRouteParams {
  LEFT_PAGE_NUM = 'leftPageNum',
  RIGHT_PAGE_NUM = 'rightPageNum',
  LEFT_SELECTED_ITEM_ID = 'leftSelectedItemId',
  RIGHT_SELECTED_ITEM_ID = 'rightSelectedItemId',
  FILTER = 'filter'
}

export enum DiscoverQueryParams {
  PAGE_NUM = 'page',
  BASE_FILTER_FIELD = 'baseFilterField',
  BASE_FILTER_VALUE = 'baseFilterValue',
  FILTER = 'filter',
  PAGE_SIZE = 'pageSize'
}

@Injectable()
export class DiscoverRouteManager {
  dispatcher: EventEmitter<number> = new EventEmitter();
  latestQueryParams: any = null;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {

    this.getRoute().subscribe((route) => {
      this.latestQueryParams = route.snapshot.queryParams;
      this.dispatcher.emit(DiscoverRouteEvents.STATE_CHANGED);
    });
  }

  getEmitter() {
    return this.dispatcher;
  }

  getLatestQueryParams() {
    return this.latestQueryParams;
  }


  getRoute() {
    return this.router.events.pipe(
      filter(e => e instanceof NavigationEnd)
      , map(() => this.activatedRoute)
      , map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
  }
}


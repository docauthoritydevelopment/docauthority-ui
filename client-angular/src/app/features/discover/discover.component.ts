import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';


@Component({
  selector: 'discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiscoverComponent implements OnInit {

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
  }


}


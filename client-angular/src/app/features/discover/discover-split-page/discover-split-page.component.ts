import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {
  DiscoverQueryParams,
  DiscoverRouteEvents,
  DiscoverRouteManager,
  DiscoverRouteParams
} from "@app/features/discover/discover.route.manager";
import {ActivatedRoute} from "@angular/router";
import {RoutingService} from "@services/routing.service";
import {FilterData} from "@services/filter-data";
import {DiscoverTypeObject} from "@app/common/discover/types/discover-interfaces";
import {NewFilterService} from "@app/common/discover/services/new.filter.service";
import {DiscoverTypesService} from "@app/common/discover/services/discover.types";
import {DiscoverService} from "@app/common/discover/services/discover.service";
import {MetadataService} from "@services/metadata.service";
import {DiscoverType} from "@app/common/discover/types/common-enums";

declare let _: any;


@Component({
  selector: 'discover-split-page',
  templateUrl: './discover-split-page.component.html',
  styleUrls: ['./discover-split-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiscoverSplitPageComponent implements OnInit {
  leftType: DiscoverType = DiscoverType.GROUPS;
  rightType: DiscoverType = DiscoverType.GROUP_DETAILS;

  leftParams: any = null;
  rightParams: any = null;
  routeSubscription: any = null;
  leftTypeObject: DiscoverTypeObject = null;
  showFilterDiv: boolean = true;
  showRightDiv: boolean = true;
  currFilter : FilterData;
  DiscoverType = DiscoverType;
  selectLeftItem: any = null;
  metadataLoaded:boolean = false;

  constructor(private newFilterService : NewFilterService, private routingService : RoutingService, private discoverRouteManager : DiscoverRouteManager,private route: ActivatedRoute, private discoverService : DiscoverService, private discoverTypesService: DiscoverTypesService, private metadataService : MetadataService) {
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  ngOnInit() {

  }

  /*
  updateData() {
    let rightChanged: boolean = false;
    let leftChanged:boolean = false;
    let newParams:any = this.discoverRouteManager.getLatestQueryParams();

    if  (this.leftParams == null) {
      this.leftParams = {};
      leftChanged = true;
    }
    if  (this.rightParams == null && newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID] != null) {
      this.rightParams = {};
      rightChanged = true;
    }

    this.currFilter = this.newFilterService.parseUrlToPageFilter(newParams[DiscoverRouteParams.FILTER]);

    leftChanged =  this.discoverService.updateParamsObject(this.leftParams,{
        [DiscoverQueryParams.PAGE_NUM]: newParams[DiscoverRouteParams.LEFT_PAGE_NUM],
        [DiscoverQueryParams.FILTER]: this.currFilter.getPageFilter() ?  JSON.stringify(this.currFilter.toKendoFilter()) : null
    }) || leftChanged;

    if (newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID] != null) {
      rightChanged = this.discoverService.updateParamsObject(this.rightParams, {
        [DiscoverQueryParams.PAGE_NUM]: newParams[DiscoverRouteParams.RIGHT_PAGE_NUM],
        [DiscoverQueryParams.BASE_FILTER_VALUE]: newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID],
        [DiscoverQueryParams.BASE_FILTER_FIELD]: newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID] != null ? this.leftTypeObject.baseFilterField : null
      }) || rightChanged;
    }

    if (leftChanged) {
      this.leftParams = _.cloneDeep(this.leftParams);
      if (newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID]) {
        this.leftParams.selectedItemId = newParams[DiscoverRouteParams.LEFT_SELECTED_ITEM_ID];
      }
      else if (this.leftParams.selectedItemId) {
        delete this.leftParams.selectedItemId;
      }
    }

    if (rightChanged) {
      this.rightParams = _.cloneDeep(this.rightParams);
      if (newParams[DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID]) {
        this.rightParams.selectedItemId = newParams[DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID];
      }
      else if (this.rightParams.selectedItemId) {
        delete this.rightParams.selectedItemId;
      }
    }
  }

  ngOnInit() {
    this.leftTypeObject = this.discoverTypesService.getDiscoverTypeObject(this.leftType);
    this.routeSubscription = this.discoverRouteManager.getEmitter().subscribe(eventName => {
      if (eventName === DiscoverRouteEvents.STATE_CHANGED) {
        this.updateData();
      }
    });
    this.updateData();
    this.metadataService.waitToMetadataLoaded().subscribe(()=> {
      this.metadataLoaded = true;
    });
  }

  onLeftPageSelectedChanged(selectItem): void {
    this.routingService.updateParam(this.route, DiscoverRouteParams.LEFT_SELECTED_ITEM_ID, selectItem.id);
    this.selectLeftItem = selectItem;
  }

  onRightPageSelectedChanged(selectItem): void {
    this.routingService.updateParam(this.route, DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID, selectItem.id);
  }


  onLeftPageChanged(newPageNum:number):void {
    this.routingService.updateParamList(this.route, [DiscoverRouteParams.LEFT_PAGE_NUM, DiscoverRouteParams.LEFT_SELECTED_ITEM_ID, DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID], [newPageNum,null, null]);
  }

  onRightPageChanged(newPageNum:number):void {
    this.routingService.updateParamList(this.route, [DiscoverRouteParams.RIGHT_PAGE_NUM, DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID], [newPageNum,null]);
  }

  filterChanged() {
    this.routingService.updateParamList(this.route, [DiscoverRouteParams.FILTER, DiscoverRouteParams.LEFT_SELECTED_ITEM_ID, DiscoverRouteParams.RIGHT_SELECTED_ITEM_ID], [JSON.stringify(this.currFilter.getPageFilter()), null, null]);
  }

*/

}


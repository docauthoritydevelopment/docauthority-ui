import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'
import {TreeGridSortOrder} from '@tree-grid/types/tree-grid.type';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {I18nService} from "@app/common/translation/services/i18n-service";



@Component({
  selector: 'schedule-group-screen',
  templateUrl: './schedule-group-screen.component.html',
  styleUrls: ['./schedule-group-screen.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScheduleGroupScreenComponent extends BaseComponent{

  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;

  showFilter:boolean = true;
  storageKey='ACTIVE_SCANS';
  loading: boolean = false;
  inAutomaticRefresh:boolean = true;
  sGroupParams:any = null;
  breadCrumbslist:any[] = [];

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private i18nService : I18nService,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();


    this.on(this.scanService.activeScanFilterChangeEvent$, ([filterField,filterStr])=> {
      this.routingService.updateParamList(this.route,[filterField], [filterStr]);
    })
  }


  daOnInit() {
    this.pageTitleService.setTitle('Schedule groups');
    this.on(this.scanRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData(true);
      }
    }));
    const params: any = this.scanRouteManager.getLatestQueryParams();
    this.initialSortFieldName = params.sortField ? params.sortField : null;
    this.initialSortDirection = params.sortDesc ? params.sortDesc : null;
    this.loadData(true);
  }


  onSetAutomaticRefresh() {
    this.inAutomaticRefresh = !this.inAutomaticRefresh;
  }


  loadData(withLoader:boolean = false) {
    if (withLoader){
      this.showLoader(true);
    }
    let  params = _.cloneDeep(this.scanRouteManager.getLatestQueryParams()) || {};
    this.breadCrumbslist = [
      {
        title : this.i18nService.translateId('SGROUPS.MAIN_TITLE')
      }];
    if (params[ScanPathParams.scheduleGroupName] != null) {
      this.breadCrumbslist = [
        {
          title: this.i18nService.translateId('SGROUPS.MAIN_TITLE'),
          clickFunc: this.clearSGroups
        }, {
          title: params[ScanPathParams.scheduleGroupName]
        }];
    }
    if (params[ScanPathParams.sgPage]) {
      params[ScanPathParams.page] = params[ScanPathParams.sgPage];
    }
    this.sGroupParams = params;
  }

  clearSGroups = ()=> {
    this.routingService.updateParamList(this.route, [ScanPathParams.scheduleGroupName,ScanPathParams.scheduleGroupIds], ['','']);
  }



  onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.sgPage, pageNum);
  }


  onParametersChanged = (newParamsObject : {keys : string[] , values : string[]}) => {
    this.routingService.updateParamList(this.route, newParamsObject.keys, newParamsObject.values);
  }
}

import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {UtilService} from '@services/util.service';
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {CenterActiveScansComponent} from "@app/features/scan/scanCenter/centerActiveScans/center-active-scans.component";


@Component({
  selector: 'scan-center',
  templateUrl: './scan-center.component.html',
  styleUrls: ['./scan-center.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScanCenterComponent extends BaseComponent{

  @ViewChild(CenterActiveScansComponent)
  activeScansComponent: CenterActiveScansComponent;

  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();
  }


  daOnInit() {
    this.on(this.scanRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    }));

    this.pageTitleService.setTitle('Scan center');
    this.loadData();
  }


  loadData() {
    this.activeScansComponent.loadData();
  }

}

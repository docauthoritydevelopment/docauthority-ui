import {Component, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import { GridsterConfig, GridsterItem, GridType, DisplayGrid, GridsterComponentInterface }  from 'angular-gridster2';
import {ScanService} from "@app/features/scan/services/scan.service";
import {forkJoin, Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {ChartService} from "@app/common/charts/services/chart.service";
import {ChartData} from "@app/common/charts/types/common-charts-interfaces";

@Component({
  selector: 'center-active-scans',
  templateUrl: './center-active-scans.component.html',
  styleUrls: ['./center-active-scans.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CenterActiveScansComponent extends BaseComponent{
  itemResize = (item, itemComponent) =>  {
      this.chartService.refreshHighchartByOutsideDOM(itemComponent.el);
      //this.chartService.refreshAllHighcharts();
  }

  rootFoldersData: ChartData = null;
  gridOptions : GridsterConfig = {
    gridType: GridType.Fit,
    displayGrid: DisplayGrid.None,
    itemResizeCallback: this.itemResize,
    itemChangeCallback : this.itemResize,
    pushItems: true,
    draggable: {
      enabled: true
    },
    resizable: {
      enabled: true
    }
  };

  gridItems = {
    'rootFolderHealth' : {cols: 4, rows: 4},
    'runningRate' : {cols: 4, rows: 2},
    'totalFilesFound' : {cols: 4, rows: 2}
  }

  constructor(private scanService: ScanService, private chartService : ChartService) {
    super();
  }


  loadData() {
    let queriesArray = [];
    queriesArray.push(this.scanService.getRootFoldersScanSummaryRequest());
    forkJoin(queriesArray).pipe(map(results => {
       this.rootFoldersData = this.chartService.convertKeyValueObjectToBarChart(results[0]);
    })).subscribe();
  }


  daOnInit() {
  }


}

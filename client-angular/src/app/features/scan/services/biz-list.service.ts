import {Injectable} from '@angular/core';
import {Observable, forkJoin, Subject, of} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {map, concatMap} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
  TreeGridData, TreeGridDataItem,
  TreeGridSortingInfo,
  TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {BizListSummaryInfo} from "@app/common/scan/types/scan.dto";
import {EJobState, EJobType, ERunStatus} from "@app/common/scan/types/scanEnum.types";
import {RdbService} from "@services/rdb.service";
import {ServerUrls} from "@app/common/configuration/common-config";
import {DtoPhaseDetails} from "@app/features/scan/types/scan-interfaces";


@Injectable()
export class BizListService {


  constructor(
    private http: HttpClient,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private excelService: ExcelService,
    private rdbService : RdbService
  )
  {
  }

  private bizListChangedEvent = new Subject<number>();
  bizListChangedEvent$ = this.bizListChangedEvent.asObservable();

  runAllBizList() {
    this.http.post(ServerUrls.START_ALL_BIZ_LIST , {}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }

  resumeAllBizList(runId) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.RESUME_SCAN,{runIds : runId }), {}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }

  pauseAllBizList(runId) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.PAUSE_SCAN,{runIds : runId }), {}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }

  stopAllBizList(runId) {
    this.http.post('/api/scan/process/stop/run?runIds=' + runId, {}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }

  exportBizListData(totalItemsNum,params) {
    return this.getBizListRunsRequest().pipe(concatMap((runObj:any)=> {
      let jobsByBizListIdMap = [];
      if (runObj!= null ) {
        if (runObj.jobList) {
          runObj.jobList.forEach((jobItem: any) => {
            if (jobItem.jobType == EJobType.EXTRACT) {
              jobsByBizListIdMap[jobItem.bizListId] = jobItem;
            }
          });
        }
      }
      return this.excelService.exportData(totalItemsNum, ServerUrls.GET_BIZ_LISTS, params, (item)=> {
        return this.extractSingleBizListData(item,jobsByBizListIdMap);
      }, 'business list', new TreeGridSortingInfo('id', TreeGridSortOrder.DESC));
    }));
  }


  extractSingleBizListData = (item, jobsByBizListIdMap) => {
    let runJob = null;
    if (jobsByBizListIdMap[item.bizListDto.id]) {
      runJob = jobsByBizListIdMap[item.bizListDto.id];
    }
    const partialItem: any = {};
    partialItem.Name = item.bizListDto.name ;
    partialItem['Description']=item.bizListDto.description;
    partialItem['Last full run']=item.bizListDto.lastSuccessfulRunDate ? (new Date(item.bizListDto.lastSuccessfulRunDate)).toDateString() : '';
    partialItem['Type']=this.i18nService.translateId(item.bizListDto.bizListItemType) ;
    partialItem['Status'] = this.i18nService.translateId(this.getJobState(runJob));
    partialItem['Active'] = item.bizListDto.active ? 'True' : 'False';
    return partialItem;
  }


  getJobState(runJob : DtoPhaseDetails):string {
    if (runJob && runJob.jobState) {
      let jobState:string = runJob.jobState.toString();
      if ( runJob.jobState == EJobState.DONE) {
        jobState = runJob.completionStatus.toString();
      }
      return jobState;
    }
    else {
      return '';
    }
  }




  getBizListStateRequest() {
    return this.http.get(ServerUrls.BIZ_LIST_STATE , {});
  }

  getBizListRunsRequest(){
    return this.http.get(ServerUrls.BIZ_LIST_RUN, {}).pipe(
      concatMap((latestRunObj:any) =>
      {
        if (latestRunObj && latestRunObj.id) {
          return this.http.get(this.rdbService.parseServerURL(ServerUrls.RUN_JOB_LIST, {runId :  latestRunObj.id }), {}).pipe(map((jobList) => {
            return {
              jobList: jobList,
              lastRun: latestRunObj
            }
          }));
        }
        else {
          return of({
            jobList: [],
            lastRun: null
          });
        }
      }));
  }


  getBizListMonitorTableRequest(params){
    params = this.urlParamsService.fixedPagedParams(params,30);
    return this.http.get(ServerUrls.GET_BIZ_LISTS, {params}).pipe(map((data: DTOPagingResults<any>)=> {
      let ans: TreeGridData<BizListSummaryInfo> = this.treeGridAdapter.adapt(
        data,
        null,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }

  setAllBizListActiveState(isActive:boolean) {
    this.http.post(ServerUrls.BIZ_LIST_STATE_LIST, isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }

  setSingleBizListActiveState(bizListId:number, isActive:boolean) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.BIZ_LIST_SINGLE_STATE,{blId : bizListId}), isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.bizListChangedEvent.next();
    });
  }
}

import {Injectable} from '@angular/core';
import {UtilService} from "@services/util.service";
import { Observable } from 'rxjs';
import {UserSettingsService} from "@services/user.settings.service";
import {Subject} from 'rxjs/index';

@Injectable()
export class ScanFavoriteService {
  favoriteRootFolderIds = [];
  FAVORITE_ROOT_FOLDERS_KEY = 'RootFolderFavoriteIds';
  dataWasLoaded:boolean = false;
  getUserStorageObserable =null;

  private scanFavoriteChangedEvent = new Subject();
  scanFavoriteChangedEvent$ = this.scanFavoriteChangedEvent.asObservable();


  constructor(private utilService : UtilService,private userSettingsService: UserSettingsService)
  {
    this.getUserStorageObserable = this.userSettingsService.getFromRemoteUserStorage(this.FAVORITE_ROOT_FOLDERS_KEY);
    this.getUserStorageObserable.subscribe((result:string[])=> {
      if (result != null) {
        this.favoriteRootFolderIds = result;
      }
      this.dataWasLoaded = true;
    })
  }


  getAllFavoriteIds(): Promise<any> {
    return new Promise( (resolve, reject)=>  {
      if (!this.dataWasLoaded) {
        this.getUserStorageObserable.subscribe((result:string)=> {
          resolve(this.favoriteRootFolderIds);
        });
      }
      else {
        resolve(this.favoriteRootFolderIds);
      }
    });
  }


  isFavoriteRootFolder(rfId):Promise<boolean> {
    return new Promise( (resolve, reject) => {
      if (!this.dataWasLoaded) {
        this.getUserStorageObserable.subscribe((result:string)=> {
          resolve(this.favoriteRootFolderIds.indexOf(rfId + '') > -1);
        });
      }
      else {
        resolve(this.favoriteRootFolderIds.indexOf(rfId + '') > -1);
      }
    });
  }

  isFavoriteRootFolderAsync(rfId):boolean {
    return this.favoriteRootFolderIds.indexOf(rfId + '') > -1;
  }



  saveRootFoldersFavorite() {
    this.userSettingsService.addToRemoteUserStorage(this.FAVORITE_ROOT_FOLDERS_KEY,this.favoriteRootFolderIds);
  }

  addFavoriteRootFolderArray(rfIds:string[]) {
    rfIds.forEach((rfId)=> {
      let itemIndex = this.favoriteRootFolderIds.indexOf(rfId+'');
      if (itemIndex ==  -1) {
        this.favoriteRootFolderIds.push(rfId + '');
      }
    });
    this.saveRootFoldersFavorite();
    this.scanFavoriteChangedEvent.next();
  }

  addFavoriteRootFolder(rfId) {
    let itemIndex = this.favoriteRootFolderIds.indexOf(rfId+'');
    if (itemIndex ==  -1) {
      this.favoriteRootFolderIds.push(rfId + '');
      this.saveRootFoldersFavorite();
    }
    this.scanFavoriteChangedEvent.next();
  }

  removeFavoriteRootFolder(rfId) {
    let itemIndex = this.favoriteRootFolderIds.indexOf(rfId+'');
    if (itemIndex > -1) {
      this.favoriteRootFolderIds.splice(itemIndex, 1);
      this.saveRootFoldersFavorite();
    }
    this.scanFavoriteChangedEvent.next();
  }

  removeFavoriteRootFolderArray(rfIds:string[]) {
    rfIds.forEach((rfId)=> {
      let itemIndex = this.favoriteRootFolderIds.indexOf(rfId + '');
      if (itemIndex > -1) {
        this.favoriteRootFolderIds.splice(itemIndex, 1);
      }
    });
    this.saveRootFoldersFavorite();
    this.scanFavoriteChangedEvent.next();
  }

}

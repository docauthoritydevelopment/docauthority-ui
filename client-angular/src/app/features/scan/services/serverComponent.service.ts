import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, concatMap} from 'rxjs/operators';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
  TreeGridData, TreeGridDataItem,
  TreeGridSortingInfo,
  TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {PeriodPipe} from "@shared-ui/pipes/periodPipe.pipe";
import {BytesPipe} from "@shared-ui/pipes/bytes.pipe";
import {CommonConfig} from "@app/common/configuration/common-config";
import {
  BizListSummaryInfo,
  ClaComponentDto,
  ClaComponentStatusDto,
  ClaComponentSummaryDto, CustomerDataCenterDto, ScheduleGroupSummaryInfoDto
} from "@app/common/scan/types/scan.dto";
import {ClaComponentType, ComponentState} from "@app/common/scan/types/scanEnum.types";
import {ServerComponentColumnObject, ServerComponentSummary} from "@app/features/scan/types/scan.types";
import {Observable, forkJoin, Subject} from 'rxjs/index';


@Injectable()
export class ServerComponentService {


  constructor(
    private http: HttpClient,
    private i18nService: I18nService,
    private periodPipe : PeriodPipe,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private bytesPipe : BytesPipe,
    private excelService: ExcelService)
  {
  }

  private serverComponentChangedEvent = new Subject<number>();
  serverComponentChangedEvent$ = this.serverComponentChangedEvent.asObservable();




  extractSingleServerComponentData = (item) => {
    const partialItem: any = {};
    partialItem.Name = item.bizListDto.name ;
    partialItem['Description']=item.bizListDto.description;
    partialItem['Last full run']=(new Date(item.bizListDto.lastSuccessfulRunDate)).toDateString();
    partialItem['Type']=item.bizListDto.bizListItemType ;
    partialItem['State']=item.crawlRunDetailsDto ? item.crawlRunDetailsDto.runOutcomeState : ''  ;
    partialItem['Active'] = item.bizListDto.active;
    return partialItem;
  }

  exportServerComponentData(totalItemsNum,params) {
    return this.excelService.exportData(totalItemsNum, '/api/sys/components/listWithStatus?', params, this.extractSingleServerComponentData, 'server components', new TreeGridSortingInfo('id', TreeGridSortOrder.DESC));
  }


  getSercerComponentRequest(id) {
    return this.http.get('/api/sys/components/' + id);
  }


  getServerComponentHistoryTableRequest(params,componentId,dataCenterID) {
    params = this.urlParamsService.fixedPagedParams(params,30);
    if (dataCenterID) {
      params['dataCenterID'] = dataCenterID;
    }
    return this.http.get('/api/sys/components/'+ componentId +'/status/history', {params}).pipe(map((data: DTOPagingResults<ClaComponentStatusDto>)=> {
      data.content = this.parseHistoryData(data.content);
      let ans: TreeGridData<ClaComponentStatusDto> = this.treeGridAdapter.adapt(
        data,
        null,
        new TreeGridAdapterMapper()
      );
      return ans;
    }));
  }


  getDatacenterListRequest() {
    return this.http.get('/api/sys/customerDataCenter/list');
  }


  editServerComponent(updatedComponent:ClaComponentStatusDto) {
    this.http.post("/api/sys/components/"+updatedComponent.id,updatedComponent).subscribe(()=> {
      this.serverComponentChangedEvent.next();
    });
  }




  parseHistoryData = (serverComp: ClaComponentStatusDto[])=>  {
    if(serverComp)
    {
      serverComp.forEach(d=> {
        if(d.extraMetrics) {
          let eMatrix = JSON.parse(d.extraMetrics);
          let extraMetrixKeys = Object.keys(eMatrix);
          for (let count = 0 ; count < extraMetrixKeys.length ; count++) {
            if (extraMetrixKeys[count].indexOf('_')>-1) {
              eMatrix[extraMetrixKeys[count].substring(extraMetrixKeys[count].indexOf('_')+1)] =  eMatrix[extraMetrixKeys[count]];
            }
          }
          _.extend(d, eMatrix);
        }
        if(d.timestamp) {
          (<any>d).timestamp = new Date(d.timestamp);
        }
        if((<any>d).driveData) {
          let trashHoldNum = 10000;
          let driveData:any = JSON.parse((<any>d).driveData);
          if (driveData.drive_capacities) {
            let specificStr = '';
            if (driveData.drive_capacities && driveData.drive_capacities.length > 0 ) {
              driveData.drive_capacities.forEach((dItem: any) => {
                specificStr = specificStr+ dItem.drive_id +' ' +  this.bytesPipe.transform(Number(dItem.total_space-dItem.free_space) * 1024 * 1024) +' / ' + this.bytesPipe.transform(Number(dItem.total_space)  * 1024 * 1024)+ ' ('+ Math.ceil(100*(dItem.total_space-dItem.free_space)/dItem.total_space) +'%)';
                specificStr = specificStr +'<br>';
              })
            }
            (<any>d).diskSizeStr =  specificStr ;
          }

        }
        else {
          (<any>d).diskSizeStr =  'N/A';
        }

        if( d.processCpuLoad && d.processCpuLoad<0 ) {
          d.processCpuLoad=0;
        }
        if( d.systemCpuLoad && d.systemCpuLoad<0 ) {
          d.systemCpuLoad=0;
        }
        d.usedPhysicalMemorySize = d.totalPhysicalMemorySize - d.freePhysicalMemorySize;
        d.memoryPercentage = d.totalPhysicalMemorySize && d.totalPhysicalMemorySize >0 ? Math.floor(d.usedPhysicalMemorySize/ d.totalPhysicalMemorySize * 100):0;
      });
    }
    return serverComp;
  }

  getServerComponentTableRequest(params, filterBySearchDateTime) : Observable<(ServerComponentSummary | TreeGridData<BizListSummaryInfo>)[]>
  {
    params = this.urlParamsService.fixedPagedParams(params,120);
    return this.http.get('/api/sys/components/listWithStatus', {params}).pipe(map((data: DTOPagingResults<ClaComponentSummaryDto>)=> {
      let summaryObject: ServerComponentSummary = this.getServerComponentSummaryObject(data.content);
      data.content = this.parseData(data.content, filterBySearchDateTime);
      let ans: TreeGridData<BizListSummaryInfo> = this.treeGridAdapter.adapt(
        data,
        null,
        new TreeGridAdapterMapper()
      );
      return [ans, summaryObject];
    }));
  }



  treeComponentsOrderArray = [
    ClaComponentType.APPSERVER,
    ClaComponentType.DB,
    ClaComponentType.BROKER_FC,
    ClaComponentType.BROKER_DC,
    ClaComponentType.BROKER,
    ClaComponentType.ZOOKEEPER,
    ClaComponentType.SOLR_CLAFILE,
    ClaComponentType.SOLR_SIMILARITY,
    ClaComponentType.SOLR_SIM,
    ClaComponentType.SOLR_NODE,
    ClaComponentType.SOLR_ROOT_NODE,
    ClaComponentType.MEDIA_PROCESSOR,
    ClaComponentType.DATA_CENTER
  ];



  getMainTitleBubble = (dataItem)=>  {
    let localNetAddressHtml = '';

    if(dataItem.claComponentDto.localNetAddress) {
      localNetAddressHtml = `<div style='padding-top:12px;'>
        <div style='font-weight: 700; padding-bottom: 5px'>Local Net Address:</div>
        <div>${dataItem.claComponentDto.localNetAddress}</div>
        </div>`
    }
    return `<div>
        <div>${dataItem.mainTitle}</div>
        ${localNetAddressHtml}
      </div>`
  };


  parseData = (serverSummaryComp: ClaComponentSummaryDto[],filterBySearchDateTime) => {
    if(serverSummaryComp)
    {
      let totalDcBrokersNum = 0 ;
      let brokerObject = null;
      let foundSolrNode:boolean = false;
      let rootSolrNode: ClaComponentSummaryDto = null;

      serverSummaryComp.forEach(d=>
      {
        if (d.claComponentDto && d.claComponentDto.claComponentType === ClaComponentType.BROKER) {
          brokerObject = d;
          (<any>d).scanAverageEnqueueTime = 0;
          (<any>d).ingestAverageEnqueueTime =  0;
          (<any>d).scanDequeueCount = 0;
          (<any>d).ingestDequeueCount = 0;
          (<any>d).scanEnqueueCount = 0;
          (<any>d).ingestEnqueueCount = 0;
          (<any>d).level = 0;
          (<any>d).parents  = [(<any>d).claComponentDto.id ];
        }
        else if (d.claComponentDto && d.claComponentDto.claComponentType === ClaComponentType.SOLR_NODE) {
          foundSolrNode = true;
        }
      });

      if (foundSolrNode) {
        rootSolrNode = this.buildSolrNodeRoot();
      }

      let dataCenterMap = {};
      let solrNodeIndx= 1;
      serverSummaryComp.forEach(d=>
      {
        (<any>d).id=d.claComponentDto.id;
        (<any>d).elementId=d.claComponentDto.id;
        if(!_.isEmpty(d.extraMetrics)) {
          d.extraMetrics = JSON.parse(d.extraMetrics);

          if(d.extraMetrics ) {
            (<any>d).extraMetrics.usedPhysicalMemorySize = (<any>d).extraMetrics.totalPhysicalMemorySize - (<any>d).extraMetrics.freePhysicalMemorySize;
            (<any>d).extraMetrics.memoryPercentage = (<any>d).extraMetrics.totalPhysicalMemorySize && (<any>d).extraMetrics.totalPhysicalMemorySize >0 ? Math.floor((<any>d).extraMetrics.usedPhysicalMemorySize/ (<any>d).extraMetrics.totalPhysicalMemorySize * 100):0;
          }
        }
        if(!d.claComponentDto.customerDataCenterDto) {
          d.claComponentDto.customerDataCenterDto= new CustomerDataCenterDto();
          d.claComponentDto.customerDataCenterDto.name = '';
        }
        if(d.claComponentDto.createdOnDB) {
          (<any>d).createdOnDBDate = new Date(d.claComponentDto.createdOnDB);
        }

        if (d.claComponentDto.claComponentType == ClaComponentType.APPSERVER) {
          this.addDiskSpace(d);
        }

        let startDateNumber = Date.now();
        if (filterBySearchDateTime !== null && filterBySearchDateTime !== undefined && filterBySearchDateTime !== -1) {
          startDateNumber = filterBySearchDateTime;
        }
        if(d.claComponentDto.stateChangeDate && (filterBySearchDateTime === null || filterBySearchDateTime === undefined || filterBySearchDateTime===-1)) {
          (<any>d).stateChangeDateDate = new Date(d.claComponentDto.stateChangeDate);
          (<any>d).lastStateChangeElapsedTime = startDateNumber - (<any>d).stateChangeDateDate;
          (<any>d).lastStateChangeElapsedTimeSpan= this.periodPipe.transform((<any>d).lastStateChangeElapsedTime);
        }
        if(d.statusTimestamp) {
          (<any>d).lastResponseDateDate = new Date(d.statusTimestamp);
          (<any>d).lastResponseElapsedTime = startDateNumber - (<any>d).lastResponseDateDate;
          (<any>d).lastResponseElapsedTimeSpan= this.periodPipe.transform((<any>d).lastResponseElapsedTime);
        }

        (<any>d).subTitle = d.claComponentDto.externalNetAddress;
        this.addStateParams(d,d,filterBySearchDateTime);


        if(d.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR) {
          this.buildMediaProcessorSubTree(d, serverSummaryComp, d.extraMetrics, dataCenterMap,filterBySearchDateTime);
        }
        else if(d.claComponentDto.claComponentType === ClaComponentType.BROKER) {
          this.buildBrokerSubTree(d, serverSummaryComp, d.extraMetrics, filterBySearchDateTime);
          (<any>d).mainTitle =  'Broker - Datacenter';
          (<any>d).mainTitleBubble =  'Broker - Datacenter';
        }
        else if(d.claComponentDto.claComponentType === ClaComponentType.BROKER_DC) {
          if (!(<any>d).isStateDisabled)
          {
            this.buildBrokerDC(d, d.extraMetrics, brokerObject, (totalDcBrokersNum == 0));
            totalDcBrokersNum++;
          }
        }
        else if(d.claComponentDto.claComponentType === ClaComponentType.SOLR_NODE) {
          (<any>d).mainTitle = this.i18nService.translateIdWithoutNull(d.claComponentDto.componentType)+' '+ solrNodeIndx++;
          (<any>d).mainTitleBubble = this.getMainTitleBubble(d);
          this.buildSolrNodeSubTree(d, rootSolrNode,filterBySearchDateTime);
        }
        else {
          d['parents'] = [(<any>d).id ];
          d['level'] = 0;
        }

        (<any>d).isMediaProcessor =  d.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR;
        (<any>d).isBroker =  d.claComponentDto.claComponentType == ClaComponentType.BROKER;
        (<any>d).isAppServer =  d.claComponentDto.claComponentType == ClaComponentType.APPSERVER;
        (<any>d).isSolrNode =  d.claComponentDto.claComponentType == ClaComponentType.SOLR_NODE;


        if(d.claComponentDto.claComponentType !== ClaComponentType.SOLR_NODE && d.claComponentDto.claComponentType !== ClaComponentType.BROKER_DC && d.claComponentDto.claComponentType !== ClaComponentType.BROKER) {
          (<any>d).mainTitle = this.i18nService.translateIdWithoutNull(d.claComponentDto.componentType);
          (<any>d).mainTitleBubble = this.getMainTitleBubble(d);
        }

        (<any>d).isDataCenterExist = _.isNumber(d.claComponentDto.customerDataCenterDto.id);
        if ((<any>d).isDataCenterExist) {
          (<any>d).dataCenterName = d.claComponentDto.customerDataCenterDto.name;
        }
        (<any>d).isDataCenterCandidate = d.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR;

      });
      brokerObject.scanAverageEnqueueTime = totalDcBrokersNum === 0 ? 0 : brokerObject.scanAverageEnqueueTime !== null ? brokerObject.scanAverageEnqueueTime / totalDcBrokersNum : null;
      brokerObject.ingestAverageEnqueueTime  =  totalDcBrokersNum === 0 ? 0 :  brokerObject.ingestAverageEnqueueTime !== null ? brokerObject.ingestAverageEnqueueTime / totalDcBrokersNum : null;
      if (rootSolrNode) {
        serverSummaryComp.push(rootSolrNode);
      }

      serverSummaryComp.sort((d1,d2): number => {
        let val1 = d1.claComponentDto ? this.treeComponentsOrderArray.indexOf(d1.claComponentDto.claComponentType) : -1;
        let val2 = d2.claComponentDto ? this.treeComponentsOrderArray.indexOf(d2.claComponentDto.claComponentType) : -1;
        return (val1 - val2);
      });

    }

    serverSummaryComp = serverSummaryComp.filter((d) => {
      return (d.extraMetrics ||  d.claComponentDto.claComponentType !== ClaComponentType.BROKER_DC ) && (d.claComponentDto.claComponentType !== ClaComponentType.BROKER_DC || !(<any>d).isStateDisabled)
    });
    return serverSummaryComp;
  }


  addDiskSpace  = (d)=>  {
    d['solrDiskSpace'] = 'N/A';
    if((<any>d).driveData) {
      let trashHoldNum = 10000;
      let driveData:any = JSON.parse((<any>d).driveData);
      if (driveData.drive_capacities) {
        let totalSpace = 0 ;
        let freeSpace = 0 ;
        let specificStr = '';
        let diskSpaceTooltip = '';
        let passTreshold: boolean = false;
        if (driveData.drive_capacities && driveData.drive_capacities.length > 0 ) {
          driveData.drive_capacities.forEach((dItem: any) => {
            totalSpace = totalSpace + dItem.total_space;
            freeSpace = freeSpace + dItem.free_space;

            diskSpaceTooltip = diskSpaceTooltip+  '<b>'+dItem.drive_id +'</b>  '+   this.bytesPipe.transform(1024*1024*Number(dItem.total_space-dItem.free_space))  +' / ' + this.bytesPipe.transform(1024*1024*Number(dItem.total_space))  + ' ('+ Math.ceil(100*(dItem.total_space-dItem.free_space)/dItem.total_space) +'%)';
            if (( totalSpace && totalSpace!= 0 ) && (100*(totalSpace-freeSpace)/totalSpace  > CommonConfig.solr_node_disk_space_threshold)) {
              passTreshold = true;
              diskSpaceTooltip = diskSpaceTooltip+ ' <span title="Free disk space for device is bellow threshold" class="fa fa-exclamation-triangle notice-info"></span>';
            }
            diskSpaceTooltip = diskSpaceTooltip+ '<br>'
          })
        }
        let spacePercentage = ( totalSpace && totalSpace!= 0 )?  Math.ceil(100*(totalSpace-freeSpace)/totalSpace) + "%" : 'N/A';
        d['solrDiskSpace'] = spacePercentage ;
        d['diskSizeWarning'] = passTreshold;
        d['diskSizeTooltip']= diskSpaceTooltip;
      }

    }
  }


  buildSolrNodeSubTree = (d, rootSolrNode,filterBySearchDateTime ) =>  {
    d['parents'] = [(<any>rootSolrNode).id, (<any>d).id ];
    d['parentId'] = (<any>rootSolrNode).id;
    d['level'] = 1;
    if (rootSolrNode.isStatePending) {
      rootSolrNode.isStateDisabled = !d.claComponentDto.active;
      rootSolrNode.isStatePending = !rootSolrNode.isStateDisabled && d.claComponentDto.state == ComponentState.PENDING_INIT;
      rootSolrNode.isStateError = !rootSolrNode.isStateDisabled && d.claComponentDto.state ==  ComponentState.FAULTY;
      rootSolrNode.isStateOk = !rootSolrNode.isStateDisabled &&  d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING;
      rootSolrNode.claComponentDto.state = d.claComponentDto.state;
    }
    else if ((d.claComponentDto.state != ComponentState.PENDING_INIT) && ((!d.claComponentDto.active  && !rootSolrNode.isStateDisabled) ||
      (rootSolrNode.isStateError != (d.claComponentDto.state ==  ComponentState.FAULTY)) ||
      (rootSolrNode.isStateOk != (d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING))))
    {
      rootSolrNode.isStatePending = false;
      rootSolrNode.isStateError = false;
      rootSolrNode.isStateOk = false;
      rootSolrNode.isStateDisabled = false;
      rootSolrNode.isStatePartial = true;
      rootSolrNode.claComponentDto.state = 'Partial';
    }

    if ( d.statusTimestamp && (!rootSolrNode.statusTimestamp || (d.statusTimestamp && rootSolrNode.statusTimestamp >   d.statusTimestamp ))){
      let startDateNumber = Date.now();
      if (filterBySearchDateTime !== null && filterBySearchDateTime !== undefined && filterBySearchDateTime !== -1) {
        startDateNumber = filterBySearchDateTime;
      }
      rootSolrNode.statusTimestamp =   d.statusTimestamp;
      rootSolrNode.lastResponseDateDate = new Date(rootSolrNode.statusTimestamp);
      rootSolrNode.lastResponseElapsedTime = startDateNumber - rootSolrNode.statusTimestamp;
      rootSolrNode.lastResponseElapsedTimeSpan= this.periodPipe.transform(rootSolrNode.lastResponseElapsedTime);
    }
    this.addDiskSpace(d);
    if (d['diskSizeWarning']) {
      rootSolrNode['diskSizeWarning'] = true;
    }
  }


  buildBrokerDC = (d, extraMetrics,brokerObject, isFirstDcBroker) =>  {
    d.parents =  [d.id, brokerObject.id];
    d.parentId = brokerObject.id;
    d.level = 1;
    d.mainTitle = 'Broker - ' + d.claComponentDto.customerDataCenterDto.name;
    d.mainTitleBubble = 'Broker - ' + d.claComponentDto.customerDataCenterDto.name;
    d.isBrokerInfo = true;
    d.dataCenterId  = d.claComponentDto.customerDataCenterDto.id;

    if (extraMetrics) {
      let scanQueue = extraMetrics[d.claComponentDto.customerDataCenterDto.id + '_scan_requests'];
      d.scanAverageEnqueueTime = scanQueue? scanQueue.AverageEnqueueTime:0;
      d.scanDequeueCount = scanQueue? scanQueue.DequeueCount: 0;
      d.scanEnqueueCount =  scanQueue? scanQueue.EnqueueCount: 0;

      let ingestQueue = extraMetrics[d.claComponentDto.customerDataCenterDto.id + '_ingest_requests'];

      d.ingestAverageEnqueueTime = ingestQueue? ingestQueue.AverageEnqueueTime:0;
      d.ingestDequeueCount =  ingestQueue? ingestQueue.DequeueCount:0;
      d.ingestEnqueueCount =  ingestQueue? ingestQueue.EnqueueCount:0;


      if (isFirstDcBroker)
      {
        brokerObject.scanAverageEnqueueTime = d.scanAverageEnqueueTime ;
        brokerObject.ingestAverageEnqueueTime = d.ingestAverageEnqueueTime ;
        brokerObject.scanDequeueCount = d.scanDequeueCount ;
        brokerObject.ingestDequeueCount = d.ingestDequeueCount ;
        brokerObject.scanEnqueueCount = d.scanEnqueueCount ;
        brokerObject.ingestEnqueueCount = d.ingestEnqueueCount;
      }
      else {
        brokerObject.scanAverageEnqueueTime = d.scanAverageEnqueueTime !== null  && brokerObject.scanAverageEnqueueTime !== null ? brokerObject.scanAverageEnqueueTime + d.scanAverageEnqueueTime : null;
        brokerObject.ingestAverageEnqueueTime = d.ingestAverageEnqueueTime !== null  && brokerObject.ingestAverageEnqueueTime !== null ? brokerObject.ingestAverageEnqueueTime + d.ingestAverageEnqueueTime : null;
        brokerObject.scanDequeueCount = brokerObject.scanDequeueCount !== null && d.scanDequeueCount !== null ? brokerObject.scanDequeueCount + d.scanDequeueCount : null;
        brokerObject.ingestDequeueCount = brokerObject.ingestDequeueCount !== null && d.ingestDequeueCount !== null? brokerObject.ingestDequeueCount + d.ingestDequeueCount : null;
        brokerObject.scanEnqueueCount = brokerObject.scanEnqueueCount !== null && d.scanEnqueueCount !== null ? brokerObject.scanEnqueueCount + d.scanEnqueueCount : null;
        brokerObject.ingestEnqueueCount = brokerObject.ingestEnqueueCount !== null && d.ingestEnqueueCount !== null ? brokerObject.ingestEnqueueCount + d.ingestEnqueueCount : null;
      }
    }
    else {
      brokerObject.scanAverageEnqueueTime = null;
      brokerObject.ingestAverageEnqueueTime = null;
      brokerObject.scanDequeueCount = null;
      brokerObject.ingestDequeueCount = null;
      brokerObject.scanEnqueueCount = null;
      brokerObject.ingestEnqueueCount = null;
    }
  }



  buildMediaProcessorSubTree = (d, serverSummaryComp, extraMetrics, dataCenterMap, filterBySearchDateTime)=>  {
    let startDateNumber = Date.now();
    if (filterBySearchDateTime !== null && filterBySearchDateTime !== undefined && filterBySearchDateTime !== -1) {
      startDateNumber = filterBySearchDateTime;
    }
    (<any>d).mainTitle = (<any>d).mainTitle + ': ' + d.claComponentDto.instanceId;
    (<any>d).mainTitleBubble = (<any>d).mainTitle + ': ' + d.claComponentDto.instanceId;
    this.addDiskSpace(d);
    let currentDataCenter:any = null;
    if (dataCenterMap[d.claComponentDto.customerDataCenterDto.id]){
      currentDataCenter = dataCenterMap[d.claComponentDto.customerDataCenterDto.id];

      if (currentDataCenter.isStatePending || currentDataCenter.isStateNone ) {
        currentDataCenter.isStateNone =  d.isStateNone;
        currentDataCenter.isStateDisabled = !d.claComponentDto.active;
        currentDataCenter.isStatePending = !currentDataCenter.isStateDisabled && d.claComponentDto.state == ComponentState.PENDING_INIT;
        currentDataCenter.isStateError = d.claComponentDto.state ==  ComponentState.FAULTY;
        currentDataCenter.isStateOk = !currentDataCenter.isStateDisabled &&  d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING;
        currentDataCenter.claComponentDto.state =  d.claComponentDto.state;
      }
      else if ((d.claComponentDto.state != ComponentState.PENDING_INIT) && (!d.isStateNone) && ((!d.claComponentDto.active  && !currentDataCenter.isStateDisabled) ||
        (currentDataCenter.isStateError != (d.claComponentDto.state ==  ComponentState.FAULTY)) ||
        (currentDataCenter.isStateNone != d.isStateNone) ||
        (currentDataCenter.isStateOk != (d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING))))
      {
        currentDataCenter.isStatePending = false;
        currentDataCenter.isStateError = false;
        currentDataCenter.isStateOk = false;
        currentDataCenter.isStateDisabled = false;
        currentDataCenter.isStatePartial = true;
        currentDataCenter.claComponentDto.state = 'Partial';
      }

      if (d.statusTimestamp && currentDataCenter.statusTimestamp >   d.statusTimestamp ){
        currentDataCenter.statusTimestamp =   d.statusTimestamp;
        currentDataCenter.lastResponseDateDate = new Date(currentDataCenter.statusTimestamp);
        currentDataCenter.lastResponseElapsedTime = startDateNumber - currentDataCenter.statusTimestamp;
        currentDataCenter.lastResponseElapsedTimeSpan= this.periodPipe.transform(currentDataCenter.lastResponseElapsedTime);
      }
    }
    else { //create stub datacenter row
      currentDataCenter = new ClaComponentSummaryDto();
      currentDataCenter.id =  1000000+d.claComponentDto.customerDataCenterDto.id;  //unique ID is needed for treelist
      currentDataCenter.elementId = d.claComponentDto.customerDataCenterDto.id;
      currentDataCenter.parents = [ 1000000+d.claComponentDto.customerDataCenterDto.id];
      currentDataCenter.claComponentDto = new ClaComponentDto();
      currentDataCenter.claComponentDto.claComponentType = ClaComponentType.DATA_CENTER;
      currentDataCenter.claComponentDto.id =  currentDataCenter.id;
      currentDataCenter.mainTitle = 'Datacenter - '+ d.claComponentDto.customerDataCenterDto.name;
      currentDataCenter.mainTitleBubble = 'Datacenter - '+ d.claComponentDto.customerDataCenterDto.name;

      currentDataCenter.scanTaskFinished = 0 ;
      currentDataCenter.ingestTaskFinished = 0 ;
      currentDataCenter.mapRate = 0 ;
      currentDataCenter.ingestRate = 0 ;
      currentDataCenter.isDataCenter = true;


      currentDataCenter.isStateDisabled = !d.claComponentDto.active;
      currentDataCenter.isStateNone = d.isStateNone;
      currentDataCenter.isStatePending = !currentDataCenter.isStateDisabled && d.claComponentDto.state == ComponentState.PENDING_INIT;
      currentDataCenter.isStateError = d.claComponentDto.state ==  ComponentState.FAULTY;
      currentDataCenter.isStateOk = !currentDataCenter.isStateDisabled &&  d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING;

      currentDataCenter.claComponentDto.state = d.claComponentDto.state;
      currentDataCenter.statusTimestamp = d.statusTimestamp;
      if (d.statusTimestamp) {
        currentDataCenter.lastResponseDateDate = new Date(currentDataCenter.statusTimestamp);
        currentDataCenter.lastResponseElapsedTime = startDateNumber - currentDataCenter.lastResponseDateDate;
        currentDataCenter.lastResponseElapsedTimeSpan = this.periodPipe.transform(currentDataCenter.lastResponseElapsedTime);
      }


      dataCenterMap[d.claComponentDto.customerDataCenterDto.id] = currentDataCenter;
      serverSummaryComp.push(currentDataCenter);
    }
    currentDataCenter.mpNumber =  (currentDataCenter.mpNumber ? currentDataCenter.mpNumber+1 : 1);
    currentDataCenter.subTitle = currentDataCenter.mpNumber+ (currentDataCenter.mpNumber ==1 ? ' media processor' : ' media processors');
    d['parentId'] = currentDataCenter.id;
    d['parents'] = [currentDataCenter.id,(<any>d).id ];
    d['level'] = 1;
    if (_.isObject(extraMetrics)) {
      d['scanTaskFinished'] = extraMetrics.scanTaskResponsesFinished + extraMetrics.scanTaskFailedResponsesFinished;
      d['ingestTaskFinished'] = extraMetrics.ingestTaskRequestsFinished + extraMetrics.ingestTaskRequestsFinishedWithError + extraMetrics.ingestTaskRequestFailure;
      d['mapRate'] = extraMetrics.scanRunningThroughput;
      d['ingestRate'] = extraMetrics.ingestRunningThroughput;
    }

    if (d['diskSizeWarning']) {
      currentDataCenter['diskSizeWarning'] = true;
    }
    currentDataCenter.scanTaskFinished = currentDataCenter.scanTaskFinished +  ( d['scanTaskFinished'] ? d['scanTaskFinished'] : 0);
    currentDataCenter.ingestTaskFinished = currentDataCenter.ingestTaskFinished +  ( d['ingestTaskFinished'] ? d['ingestTaskFinished'] : 0 );
    currentDataCenter.mapRate = currentDataCenter.mapRate +  ( d['mapRate'] ? d['mapRate'] : 0 );
    currentDataCenter.ingestRate = currentDataCenter.ingestRate +  ( d['ingestRate'] ? d['ingestRate'] : 0 ) ;
  };

  buildBrokerSubTree = (d, serverSummaryComp, extraMetrics, filterBySearchDateTime)=>  {
    if(_.isObject(extraMetrics)) {
      this.addAppBroker(d, extraMetrics, serverSummaryComp,filterBySearchDateTime);
    }
  };


  buildSolrNodeRoot = (): ClaComponentSummaryDto => {
    let currentSolrRoot= new ClaComponentSummaryDto();
    (<any>currentSolrRoot).id =  2000000;
    (<any>currentSolrRoot).elementId = 2000000;
    (<any>currentSolrRoot).parents = [ 2000000];
    (<any>currentSolrRoot).claComponentDto = new ClaComponentDto();
    (<any>currentSolrRoot).claComponentDto.claComponentType = ClaComponentType.SOLR_ROOT_NODE;
    (<any>currentSolrRoot).mainTitle = 'Solr Nodes';
    (<any>currentSolrRoot).mainTitleBubble = 'Solr node root folder';
    (<any>currentSolrRoot).isStatePending = true;
    (<any>currentSolrRoot).isSolrRootNode  = true;
    return currentSolrRoot;
  }

  addStateParams = (d, target, filterBySearchDateTime) =>  {
    if(d.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR && filterBySearchDateTime!= null && filterBySearchDateTime!= -1 && (d.statusTimestamp===null || d.statusTimestamp=== undefined)) {
      target.isStateNone = true;
      target.isStatePending = false;
      target.isStateError = false;
      target.isStateOk = false;
      target.claComponentDto.state = ComponentState.NA;
    }
    else if(d.claComponentDto.active) {
      target.isStateNone = false
      target.isStatePending = d.claComponentDto.state == ComponentState.PENDING_INIT;
      target.isStateError = d.claComponentDto.state ==  ComponentState.FAULTY;
      target.isStateOk = d.claComponentDto.state == ComponentState.OK || d.claComponentDto.state == ComponentState.STARTING;
      if(d.claComponentDto.claComponentType == ClaComponentType.BROKER && (<any>d).isStateError ) {
        target.isBrokerStatusError = true;
      }
    } else {
      target.isStateNone = false;
      target.isStateDisabled = true;
      target.isStatePending = false;
      target.isStateError = false;
      target.isStateOk = false;
      target.claComponentDto.state = "Disabled";
    }
  }


  addAppBroker = (d, extraMetrics, serverSummaryComp,filterBySearchDateTime) =>  {
    let appBrokerInfo = <any>{};
    let startDateNumber = Date.now();
    if (filterBySearchDateTime !== null && filterBySearchDateTime !== undefined && filterBySearchDateTime !== -1) {
      startDateNumber = filterBySearchDateTime;
    }

    _.forEach(extraMetrics, (value, key) => {
      if(key.indexOf('_responses') > 0) {
        let res = key.split('_');
        appBrokerInfo[res[0]] = value
      }
    });
    if(!_.isEmpty(appBrokerInfo)) {
      let claComponentDto:any  = {};
      claComponentDto.id = 1000000+d.id;
      claComponentDto.claComponentType = ClaComponentType.BROKER_FC;
      claComponentDto.state = d.claComponentDto.state;

      let appBroker:any  = {
        isBrokerInfo: true,
        isBrokerRC: true,
        id:  1000000+'_appBroker_' + d.id,  //unique ID is needed for treelist
        elementId : d.id,
        parentId: d.parentId,
        parents: [ d.id ],
        claComponentDto: claComponentDto,
        mainTitle: 'Broker - File cluster',
        mainTitleBubble: 'Broker - File cluster',
        scanAverageEnqueueTime: appBrokerInfo.scan.AverageEnqueueTime,
        ingestAverageEnqueueTime: appBrokerInfo.ingest.AverageEnqueueTime,
        scanDequeueCount: appBrokerInfo.scan.DequeueCount,
        ingestDequeueCount: appBrokerInfo.ingest.DequeueCount,
        scanEnqueueCount: appBrokerInfo.scan.EnqueueCount,
        ingestEnqueueCount: appBrokerInfo.ingest.EnqueueCount
      };
      if(d.statusTimestamp) {
        appBroker.statusTimestamp = d.statusTimestamp;
        appBroker.lastResponseDateDate = new Date(appBroker.statusTimestamp);
        appBroker.lastResponseElapsedTime = startDateNumber - appBroker.lastResponseDateDate;
        appBroker.lastResponseElapsedTimeSpan= this.periodPipe.transform(appBroker.lastResponseElapsedTime);
      }

      if(d.claComponentDto.stateChangeDate && (filterBySearchDateTime === null || filterBySearchDateTime === undefined || filterBySearchDateTime === -1)) {
        appBroker.stateChangeDateDate = new Date(d.claComponentDto.stateChangeDate);
        appBroker.lastStateChangeElapsedTime = startDateNumber - (<any>d).stateChangeDateDate;
        appBroker.lastStateChangeElapsedTimeSpan= this.periodPipe.transform((<any>d).lastStateChangeElapsedTime);
      }

      this.addStateParams(d,appBroker,filterBySearchDateTime);
      serverSummaryComp.push(appBroker);
    }

    return {};
  };


  getServerComponentSummaryObject(components: any[]):ServerComponentSummary {
    let mpLive = components.filter((c)=> {
      return c.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR &&
        c.claComponentDto.active &&
        (c.claComponentDto.state == ComponentState.STARTING || c.claComponentDto.state == ComponentState.OK)
    });

    let mpFailed = components.filter((c)=> {
      return c.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR &&
        c.claComponentDto.active &&
        (c.claComponentDto.state == ComponentState.FAULTY)
    });

    let mpDisabled = components.filter((c)=> {
      return c.claComponentDto.claComponentType == ClaComponentType.MEDIA_PROCESSOR && !c.claComponentDto.active
    });

    let solrLive = components.filter((c)=> {
      return (c.claComponentDto.claComponentType == ClaComponentType.SOLR_NODE) &&
        (c.claComponentDto.state == ComponentState.STARTING || c.claComponentDto.state == ComponentState.OK)
    });

    let solrFailed = components.filter((c)=> {
      return (c.claComponentDto.claComponentType == ClaComponentType.SOLR_NODE) &&
        (c.claComponentDto.state == ComponentState.FAULTY)
    });

    let brokerFailed = components.filter((c)=> {
      return (c.claComponentDto.claComponentType == ClaComponentType.BROKER) &&
        (c.claComponentDto.state == ComponentState.FAULTY)
    });

    let serverComponentsSummary: ServerComponentSummary= {
      dbs: 1, //appServers.length,
      brokers: 1, //brokers.length,
      appServers: 1, //appServers.length,
      mpLive: mpLive.length,
      mpFailed: mpFailed.length,
      mpDisabled : mpDisabled.length,
      solrLive: solrLive.length,
      solrFailed: solrFailed.length,
      brokerFailed : brokerFailed.length
    };
    return serverComponentsSummary;
  }



  getServerComponentColumnObject(componentType : ClaComponentType):ServerComponentColumnObject {
    let ans = new ServerComponentColumnObject();
    ans.showTimestamp = true;
    if(componentType ==  ClaComponentType.SOLR_NODE){
      ans.showDiskSpace = true;
    }
    else if(componentType ==  ClaComponentType.APPSERVER){
      ans.showIngestTaskRequests = true;
      ans.showAnalyzeTaskRequests = true;
      ans.showAppServerIngestThroughput = true;
      ans.showAppServerThroughput = true;
      ans.showDiskSpace = true;
    }
    else  if(componentType ==  ClaComponentType.BROKER_DC){
      ans.showScanRequests = true;
      ans.showIngestRequests = true;
      ans.showRealTimeRequests = true;
      ans.showContentCheckRequests = true;
    }
    else  if(componentType ==  ClaComponentType.BROKER_FC){
      ans.showScanResponses = true;
      ans.showIngestResponses = true;
      ans.showRealTimeResponses = true;
      ans.showControlResponses = true;
    }
    else  if(componentType ==  ClaComponentType.BROKER){
      ans.showScanRequests = true;
      ans.showIngestRequests = true;
      ans.showRealTimeRequests = true;
      ans.showContentCheckRequests = true;
    }
    else  if(componentType ==  ClaComponentType.MEDIA_PROCESSOR){
      ans.showStartScanTaskRequests = true;
      ans.showIngestTaskRequests = true;
      ans.showThroughput = true;
      ans.showDiskSpace = true;
    }
    else {
      ans.showStartScanTaskRequests = true;
      ans.showIngestTaskRequests = true;
      ans.showThroughput = true;
    }

    if(componentType !=  ClaComponentType.BROKER && componentType !=  ClaComponentType.SOLR_NODE && componentType !=  ClaComponentType.BROKER_FC && componentType !=  ClaComponentType.BROKER_DC) {
      ans.showPhysicalMemory = true;
      ans.showSystemCpuLoad = true;
    }

    return ans;
  }

}

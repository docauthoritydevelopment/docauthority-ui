import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {Injectable} from '@angular/core';
import {Observable, forkJoin, Subject} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {map, concatMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {TreeGridAdapterService} from '@app/shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {
  TreeGridAdapterMapper,
  TreeGridData, TreeGridDataItem,
  TreeGridSortingInfo,
  TreeGridSortOrder
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {UrlParamsService} from '@services/urlParams.service';
import {ExcelService} from '@core/services/excel.service';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {
  EJobCompletionStatus,
  EJobState,
  EMediaType, ERunStatus, EScheduleConfigType, EPauseReason,
  SCAN_OPERATION, EFileType, SCAN_STRATEGY
} from "@app/common/scan/types/scanEnum.types";
import {
  RunSummaryInfoView,
  ScheduleConfigDto,
  ScheduleGroupDto,
  RootFolderRunSummaryInfo,
  ScheduleGroupSummaryInfoDto,
  CrawlRunDetailsDto,
  ActiveScanQueryResult,
  RootFolderSummaryInfo, RootFolderTreeGridData
} from "@app/common/scan/types/scan.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {MetadataService} from "@services/metadata.service";
import {UtilService} from "@services/util.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {PeriodPipe} from "@shared-ui/pipes/periodPipe.pipe";
import {ServerOperations} from "@services/server-operations";
import {CommonConfig, ServerUrls} from "@app/common/configuration/common-config";
import {RdbService} from "@services/rdb.service";
import {LoginService} from "@services/login.service";
import {AppConfig} from "@services/appConfig.service";

@Injectable()
export class ScanService {


  // Observable string sources
  private scheduleGroupChangedEvent = new Subject<number>();
  private startScanRootFolderEvent= new Subject<string[]>();
  private pauseScanEvent= new Subject<string[]>();
  private resumeScanEvent= new Subject<string[]>();
  private stopScanEvent= new Subject<string[]>();
  private markFullEvent= new Subject<string[]>();
  private activeScanFilterChangeEvent = new Subject<string[]>();
  private scheduleGroupRefreshedEvent = new Subject<number>();
  private rootFolderRefreshedEvent = new Subject<number>();
  private rootFolderGetMoreDetailsEvent = new Subject<any>();
  private scanHistoryGetMoreDetailsEvent = new Subject<any>();
  private jobListRefreshedEvent = new Subject<number>();



  // Observable string streams
  scheduleGroupRefreshedEvent$ = this.scheduleGroupRefreshedEvent.asObservable();
  scheduleGroupChangedEvent$ = this.scheduleGroupChangedEvent.asObservable();
  startScanRootFolderEvent$ = this.startScanRootFolderEvent.asObservable();
  resumeScanEvent$ = this.resumeScanEvent.asObservable();
  pauseScanEvent$ = this.pauseScanEvent.asObservable();
  stopScanEvent$ = this.stopScanEvent.asObservable();
  markFullEvent$ = this.markFullEvent.asObservable();
  activeScanFilterChangeEvent$ = this.activeScanFilterChangeEvent.asObservable();
  rootFolderRefreshedEvent$ = this.rootFolderRefreshedEvent.asObservable();
  rootFolderGetMoreDetailsEvent$  = this.rootFolderGetMoreDetailsEvent.asObservable();
  scanHistoryGetMoreDetailsEvent$ = this.scanHistoryGetMoreDetailsEvent.asObservable();
  jobListRefreshedEvent$ = this.jobListRefreshedEvent.asObservable();




  private pipe;
  private  runningScans: any[] = [];


  constructor(
    private rdbService : RdbService,
    private http: HttpClient,
    private datePipe: DatePipe,
    private periodPipe: PeriodPipe,
    private i18nService: I18nService,
    private urlParamsService: UrlParamsService,
    private treeGridAdapter: TreeGridAdapterService,
    private metadataService: MetadataService,
    private utilService: UtilService,
    private dialogService: DialogService,
    private serverOperations: ServerOperations,
    private excelService: ExcelService,
    private loginService:LoginService,
    private appConfig:AppConfig) {
  }


  updatePhaseStatus(phase) {
    if (phase != null) {
      (<any>phase).isRunning = phase.jobState == EJobState.IN_PROGRESS || phase.jobState == EJobState.READY || phase.jobState == EJobState.WAITING;
      (<any>phase).isInProgress= phase.jobState == EJobState.IN_PROGRESS;
      (<any>phase).isNew = phase.jobState == EJobState.NEW;
      (<any>phase).isEnded = phase.jobState == EJobState.DONE;
      (<any>phase).isEndedWithError = phase.jobState == EJobState.DONE &&
        phase.completionStatus != EJobCompletionStatus.OK &&
        phase.completionStatus != EJobCompletionStatus.DONE_WITH_ERRORS &&
        phase.completionStatus != EJobCompletionStatus.UNKNOWN &&
        phase.completionStatus != EJobCompletionStatus.CANCELLED &&
        phase.completionStatus != EJobCompletionStatus.SKIPPED &&
        phase.completionStatus != EJobCompletionStatus.STOPPED;
      (<any>phase).isStopped = phase.jobState == EJobState.DONE && phase.completionStatus == EJobCompletionStatus.STOPPED;
      (<any>phase).isPaused = phase.jobState == EJobState.PAUSED;
      (<any>phase).isPending = phase.jobState == EJobState.PENDING;
      (<any>phase).isPausedWithError = phase.jobState == EJobState.PAUSED && phase.completionStatus == EJobCompletionStatus.ERROR;
      (<any>phase).isTimeOut = phase.jobState == EJobState.PAUSED && phase.completionStatus == EJobCompletionStatus.TIMED_OUT;
      (<any>phase).isCancelled = phase.completionStatus == EJobCompletionStatus.CANCELLED || phase.completionStatus == EJobCompletionStatus.SKIPPED;
    }
  }

  editScheduleGroup(newScheduleGroup:ScheduleGroupDto)  {
    return this.http.post(this.rdbService.parseServerURL(ServerUrls.SCHEDULE_GROUP_BY_ID, { sgId : newScheduleGroup.id}), newScheduleGroup).subscribe((result: ScheduleGroupDto) => {
      this.scheduleGroupChangedEvent.next(result.id);
    });
  }

  getRootFoldersScanSummaryRequest() {
    return this.http.get(ServerUrls.RF_SCAN_SUMMARY_REQUEST);
  }

  getDepartmentListRequest() {
    return this.http.get(ServerUrls.DEPARTMENT_REQUEST).pipe(map((departments:any)=> {
      let ans = [];
      ans.push({
        value : -1,
        displayText : this.appConfig.isInstallationModeLegal() ? "- No Matter -" : "- No Department -",
        tooltipText : ""
      });
      departments.content.forEach(dep => {
        ans.push({
          value : dep.id,
          displayText : dep.name,
          tooltipText : dep.fullName
        });
      });
      return ans;
    }));
  }

  getScheduleGroupListRequest(){
    return this.http.get(ServerUrls.SG_NAME_REQUEST).pipe(map((ansMap:any)=> {
      let ans = [];
      Object.keys(ansMap).forEach((theId)=> {
        ans.push({
          value : theId ,
          displayText : ansMap[theId]
        })
      });
      return ans;
    }));
  }

  changeScanStrategyMode(procMode:string):Observable<SCAN_STRATEGY>{
    return this.http.put<SCAN_STRATEGY>(this.rdbService.parseServerURL(ServerUrls.CHANGE_PROC_MODE,{pMode : procMode}),{});
  }

  getDataCenterListRequest() {
    return this.http.get(ServerUrls.DC_NAME_REQUEST).pipe(map((ansMap:any)=> {
      let ans = [];
      Object.keys(ansMap).forEach((theId)=> {
        ans.push({
          value : theId ,
          displayText : ansMap[theId]
        })
      });
      return ans;
    }));
  }

  getScanStrategyMode():Observable<SCAN_STRATEGY>{
    return this.http.get<SCAN_STRATEGY>(ServerUrls.PROC_MODE_REQUEST,{});
  }

  getScanHistoryTableRequest(params) {
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params,30);
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    }
    return this.http.get(ServerUrls.SCAN_HISTORY_REQUEST, {params}).pipe(map((data: DTOPagingResults<CrawlRunDetailsDto>)=> {
      let ans: TreeGridData<CrawlRunDetailsDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      ans.items.forEach((dataItem:  TreeGridDataItem<any>)=> {
        dataItem.id = dataItem.item.id ;
      });
      return ans;
    }));
  }

  getScheduleGroupTableRequest(params): Observable<TreeGridData<ScheduleGroupSummaryInfoDto>>{
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params,30);
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    }
    params.withRootFolders = true;
    return this.http.get(ServerUrls.SGROUPS_REQUEST, {params}).pipe(map((data: DTOPagingResults<ScheduleGroupSummaryInfoDto>)=> {
      let ans: TreeGridData<ScheduleGroupSummaryInfoDto> = this.treeGridAdapter.adapt(
        data,
        selectedItemId,
        new TreeGridAdapterMapper()
      );
      ans.items.forEach((dataItem:  TreeGridDataItem<any>)=> {
        dataItem.id = dataItem.item.scheduleGroupDto.id ;
      });
      return ans;
    }));
  }


  getActiveScansTableRequest(params): Observable<RootFolderTreeGridData<RootFolderRunSummaryInfo>>{
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params,30);
    params.withRootFolders = true;
    if (params['rootfolderIds'] === '') {
      let ans: RootFolderTreeGridData<RootFolderRunSummaryInfo>  = {
        items : [],
        totalElements: 0,
        stateSummaryInfo : {}
      };
      return of(ans);
    }
    return this.serverOperations.getData(ServerUrls.RF_LIST_REQUEST,{params}).pipe(
      map((dataWrapper: ActiveScanQueryResult) => {
        let data:DTOPagingResults<RootFolderRunSummaryInfo> = dataWrapper.rootFoldersData;
        let newData:DTOPagingResults<RootFolderSummaryInfo> = {
          content: [],
          totalPages: data.totalPages,
          totalElements: data.totalElements,
          size: data.size,
          last: data.last,
          first: data.first,
          sort: data.sort,
          numberOfElements: data.numberOfElements,
          number: data.number,
          pageable : data.pageable
        };
        newData.content = data.content.map((item:RootFolderRunSummaryInfo) => {
          let newItem:RootFolderSummaryInfo = item.rootFolderSummaryInfo;
          newItem.runningPhase = item.currentJob;
          newItem.jobs = item.jobs;
          return newItem;
        });
        let ans: any = this.treeGridAdapter.adapt(
          newData,
          selectedItemId,
          new TreeGridAdapterMapper()
        );
        ans.stateSummaryInfo = dataWrapper.stateSummaryInfo;
        return ans;
      }));
  }

  partialRun(rootFolderIds: string | number, op:SCAN_OPERATION ) {
    if (op == SCAN_OPERATION.ONLY_SCAN) {
      this.http.post(this.rdbService.parseServerURL(ServerUrls.START_RF_SCAN_REQUEST,{rootFolderIds : rootFolderIds, op : op}), {}).subscribe((ans: string[]) => {
        this.startScanRootFolderEvent.next(ans);
      }, (err)=>{
        this.showError(err);
      });
    }
    else if (op == SCAN_OPERATION.ONLY_INGEST) {
      this.http.post(this.rdbService.parseServerURL(ServerUrls.ONLY_INGEST_REQUEST,{rootFolderIds : rootFolderIds}) , {}).subscribe((ans: string[]) => {
        this.startScanRootFolderEvent.next(ans);
      }, (err)=>{
        this.showError(err);
      });
    }
    else if (op == SCAN_OPERATION.ONLY_ANALYZE) {
      this.http.post(this.rdbService.parseServerURL(ServerUrls.ONLY_ANALYZE_REQUEST,{rootFolderIds : rootFolderIds}), {}).subscribe((ans: string[]) => {
        this.startScanRootFolderEvent.next(ans);
      }, (err)=>{
        this.showError(err);
      });
    }
  }

  startScanRootFolder(rootFolderIds: string | number, op:SCAN_OPERATION ) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.START_RF_SCAN_REQUEST,{rootFolderIds : rootFolderIds, op : op}) , {}).subscribe((ans: string[]) => {
      this.startScanRootFolderEvent.next(ans);
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_FAIL_START_SCAN'));
    });
  }

  startRunningScheduleGroupsMap = [];
  startScanGroup(sGroupIds: string , op:SCAN_OPERATION ) {
    sGroupIds.split(',').forEach((groupId)=>{
      this.startRunningScheduleGroupsMap[groupId+''] = true;
    })
    this.http.post(this.rdbService.parseServerURL(ServerUrls.START_SG_SCAN_REQUEST,{sGroupIds : sGroupIds , op : op }),{}).subscribe((ans: string[]) => {
      this.startScanRootFolderEvent.next(ans);
      sGroupIds.split(',').forEach((groupId)=>{
        delete this.startRunningScheduleGroupsMap[groupId+''];
      })
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_FAIL_START_SG'));
    });
  }

  pauseAll() {
    this.http.post(ServerUrls.PAUSE_ALL , {}).subscribe(() => {
      this.pauseScanEvent.next();
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_FAIL_PAUSE_ALL'));
    });
  }

  resumeAll() {
    this.http.post(ServerUrls.RESUME_ALL, {}).subscribe(() => {
      this.resumeScanEvent.next();
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_FAIL_RESUME_ALL'));
    });
  }

  showError(err, msgPreffix = 'Operation failed') {
    this.dialogService.showAlert("Error", msgPreffix, AlertType.Error , err);
  }

  startPauseScheduleScanMap = [];
  pauseScan(runIds: number[]) {
    runIds.forEach((runId)=> {
      this.startPauseScheduleScanMap[runId+''] = true;
    })
    this.http.post(this.rdbService.parseServerURL(ServerUrls.PAUSE_SCAN, {'runIds' : runIds.join()}), {}).subscribe(() => {
      this.pauseScanEvent.next();
      runIds.forEach((runId)=> {
        delete this.startPauseScheduleScanMap[runId+''];
      })
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_PAUSE'));
    });
  }

  startStopScheduleScanMap = [];
  stopScan(runIds: number[]) {
    runIds.forEach((runId)=> {
      this.startStopScheduleScanMap[runId+''] = true;
    });
    this.http.post(this.rdbService.parseServerURL(ServerUrls.STOP_SCAN, {'runIds' : runIds.join()}), {}).subscribe(() => {
      this.stopScanEvent.next();
      runIds.forEach((runId)=> {
        delete this.startStopScheduleScanMap[runId+''];
      })
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_STOP'));
    });
  }


  forceStopScan(runIds: number[]) {
    runIds.forEach((runId)=> {
      this.startStopScheduleScanMap[runId+''] = true;
    });
    this.http.post(this.rdbService.parseServerURL(ServerUrls.STOP_SCAN_FORCE, {'runIds' : runIds.join()}), {}).subscribe(() => {
      this.stopScanEvent.next();
      runIds.forEach((runId)=> {
        delete this.startStopScheduleScanMap[runId+''];
      })
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_STOP'));
    });
  }

  startResumeScheduleScanMap = [];
  resumeScan(runIds: number[]) {
    runIds.forEach((runId)=> {
      this.startResumeScheduleScanMap[runId+''] = true;
    });
    this.http.post(this.rdbService.parseServerURL(ServerUrls.RESUME_SCAN, {'runIds' : runIds.join()}), {}).subscribe(() => {
      this.resumeScanEvent.next();
      runIds.forEach((runId)=> {
        delete this.startResumeScheduleScanMap[runId+''];
      });
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_RESUME'));
    });
  }


  pauseJob(jobId: number) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.PAUSE_JOB,{jobId: jobId}),{}).subscribe(()=> {
      this.jobListRefreshedEvent.next();
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_PAUSE_JOB'));
    });
  }

  resumeJob(jobId: number) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.RESUME_JOB,{jobId: jobId}),{}).subscribe(()=> {
      this.jobListRefreshedEvent.next();
    }, (err)=>{
      this.showError(err,this.i18nService.translateId('SCAN_ERR_RESUME_JOB'));
    });
  }


  forceRerunJobEnqueuedTasks(jobId:number) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.FORCE_RERUN_JOB_ENQUEUED_TASKS,{jobIds : jobId}), {}).subscribe(() => {
      this.jobListRefreshedEvent.next();
    }, (err)=>{
      this.showError(err);
    });
  }

  forceRerunRunEnqueuedTasks(runId:number) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.FORCE_RERUN_RUN_ENQUEUED_TASKS,{runIds : runId}), {}).subscribe(() => {
      this.jobListRefreshedEvent.next();
    }, (err)=>{
      this.showError(err);
    });
  }

  markForFullScan(rootFolderId: number, mark:boolean) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.MARK_FOR_FULL_SCAN,{rootFolderId : rootFolderId}), mark, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.markFullEvent.next();
    }, (err)=>{
      this.showError(err);
    });
  }

  activeScanFilterChange(filterField:string,filterStr:string) {
    this.activeScanFilterChangeEvent.next([filterField,filterStr]);
  }


  getRunningJob(runningInfo : any[]) {
    let goCount = 0 ;
    while (goCount< runningInfo.length && runningInfo[goCount].jobState == 'DONE') {goCount++}
    if (goCount == runningInfo.length) {
      return null;
    }
    else {
      return runningInfo[goCount];
    }
  }


  loadSubScansHistoryByRunId(runId): Observable<any> {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.RUN_JOB_LIST,{runId : runId}), {});
  }


  loadSubActiveScansByRunId(runId): Observable<any> {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.RUN_JOB_LIST,{runId : runId}), {});
  }

  getRunProcessesByIdQuery(runId): Observable<any> {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.RUN_JOB_LIST,{runId : runId}), {});
  }


  extractSingleSHistoryData = (item) => {
    const partialItem: any = {};
    partialItem['Id']= item.id;
    partialItem['Nick Name']= item.rootFolderDto.nickName;
    let isBizListExtraction = item.rootFolderDto.realPath==null;
    partialItem['Path']= isBizListExtraction ? item.stateString : item.rootFolderDto.realPath;
    partialItem['Status']=  item.runOutcomeState ;
    partialItem['Run trigger']=  item.manual ? 'Manual' : 'System';
    partialItem['Schedule Group']=  item.scheduleGroupName ?  item.scheduleGroupName : '';
    partialItem['Start Time']=  item.scanStartTime ? this.datePipe.transform(item.scanStartTime,'dd-MMM-yy HH:mm') : '';
    partialItem['End Time']=  item.scanEndTime ? this.datePipe.transform(item.scanEndTime, 'dd-MMM-yy HH:mm') : '';
    return partialItem;
  }


  extractSingleSGroupData = (item) => {
    const partialItem: any = {};
    partialItem.Name = item.scheduleGroupDto.name ? item.scheduleGroupDto.name : '';
    partialItem['Period']= this.getScheduleConfigSummaryText(item.scheduleGroupDto.scheduleConfigDto);
    partialItem['Number of folders']= item.scheduleGroupDto.rootFolderIds.length;
    partialItem['Status']= item.runStatus == 'NA' ? 'OK' : item.runStatus;
    partialItem['Next scan']=  item.nextPlannedWakeup ?   this.datePipe.transform(item.nextPlannedWakeup,'dd-MMM-yy HH:mm')   : '';
    partialItem['Last scan']= item.lastCrawlRunDetails ? this.datePipe.transform(item.lastCrawlRunDetails.scanStartTime,'dd-MMM-yy HH:mm')   : '';
    return partialItem;
  }

  extractSingleRootFolderData = (item) => {
    const partialItem: any = {};
    partialItem['PATH']=item.rootFolderSummaryInfo.rootFolderDto.realPath;


    let fileTpesNumberStr = '';
    if (item.rootFolderSummaryInfo.crawlRunDetailsDto) {
      let fileTypesMap = {};
      item.rootFolderSummaryInfo.rootFolderDto.fileTypes.forEach((fileType) => {
        fileTypesMap[fileType] = true;
      });
      if (fileTypesMap[EFileType.msWord] && item.rootFolderSummaryInfo.crawlRunDetailsDto.processedWordFiles !== null) {
        fileTpesNumberStr = fileTpesNumberStr + 'WORD files - ' + item.rootFolderSummaryInfo.crawlRunDetailsDto.processedWordFiles + '  ,';
      }
      if (fileTypesMap[EFileType.msExcel] && item.rootFolderSummaryInfo.crawlRunDetailsDto.processedExcelFiles !== null) {
        fileTpesNumberStr = fileTpesNumberStr + 'Excel files - ' + item.rootFolderSummaryInfo.crawlRunDetailsDto.processedExcelFiles + '  ,';
      }
      if (fileTypesMap[EFileType.pdf] && item.rootFolderSummaryInfo.crawlRunDetailsDto.processedPdfFiles !== null) {
        fileTpesNumberStr = fileTpesNumberStr + 'PDF files - ' + item.rootFolderSummaryInfo.crawlRunDetailsDto.processedPdfFiles + '  ,';
      }
      if (item.rootFolderSummaryInfo.crawlRunDetailsDto.processedOtherFiles !== null) {
        fileTpesNumberStr = fileTpesNumberStr + 'Other files - ' + item.rootFolderSummaryInfo.crawlRunDetailsDto.processedOtherFiles;
      }
    }
    partialItem['processed files by type']= fileTpesNumberStr;

    if (item.rootFolderSummaryInfo.crawlRunDetailsDto) {
      let statusStr = item.rootFolderSummaryInfo.crawlRunDetailsDto.runOutcomeState;
      if (statusStr == ERunStatus.PAUSED.toString() && item.rootFolderSummaryInfo.crawlRunDetailsDto.pauseReason != EPauseReason.USER_INITIATED) {
        statusStr = ERunStatus.SUSPENDED.toString();
      }

      partialItem['Status'] = this.i18nService.translateId(statusStr);
    }
    else {
      partialItem['Status'] = 'New';
    }

    partialItem['Progress']= item.currentJob? (item.currentJob.phaseCounter + '/' + item.currentJob.phaseMaxMark) : '';
    partialItem['Phase']= item.currentJob? this.i18nService.translateId(item.currentJob.jobType) : '';
    partialItem['Processed files']= !item.rootFolderSummaryInfo.crawlRunDetailsDto ? '' : item.rootFolderSummaryInfo.crawlRunDetailsDto.totalProcessedFiles;

    partialItem['Last scanned start time']=  item.rootFolderSummaryInfo.crawlRunDetailsDto ? this.datePipe.transform(item.rootFolderSummaryInfo.crawlRunDetailsDto.scanStartTime,'dd-MMM-yy HH:mm') : '';

    let endTime = item.rootFolderSummaryInfo.crawlRunDetailsDto ? item.rootFolderSummaryInfo.crawlRunDetailsDto.scanEndTime : null;
    partialItem['Last scanned processing time']=  item.rootFolderSummaryInfo.crawlRunDetailsDto && endTime ? this.periodPipe.transform(Math.floor(endTime - item.rootFolderSummaryInfo.crawlRunDetailsDto.scanStartTime)) : '';

    partialItem['MEDIA TYPE']= item.rootFolderSummaryInfo.rootFolderDto.mediaType;
    partialItem['NICK NAME'] = item.rootFolderSummaryInfo.rootFolderDto.nickName ? item.rootFolderSummaryInfo.rootFolderDto.nickName : '';
    partialItem['SCANNED FILE TYPES'] = item.rootFolderSummaryInfo.rootFolderDto.fileTypes ? item.rootFolderSummaryInfo.rootFolderDto.fileTypes.join(';') : '';
    partialItem['CUSTOMER DATA CENTER']=this.metadataService.getMetadataKeyValueItem(MetadataType.DATA_CANTER, item.rootFolderSummaryInfo.rootFolderDto.customerDataCenterDto.id).displayText;
    partialItem['RESCAN']= (item.rootFolderSummaryInfo.rootFolderDto.rescanActive+'').toUpperCase();
    partialItem['REINGEST']= (item.rootFolderSummaryInfo.rootFolderDto.reingest+'').toUpperCase();
    partialItem['REINGEST TIME IN SECONDS']=  item.rootFolderSummaryInfo.rootFolderDto.reingestTimeStampMs?Math.round(item.rootFolderSummaryInfo.rootFolderDto.reingestTimeStampMs/1000):null;
    partialItem['STORE LOCATION']=item.rootFolderSummaryInfo.rootFolderDto.storeLocation ? item.rootFolderSummaryInfo.rootFolderDto.storeLocation : '';
    partialItem['STORE PURPOSE']=item.rootFolderSummaryInfo.rootFolderDto.storePurpose ? item.rootFolderSummaryInfo.rootFolderDto.storePurpose : '';
    partialItem['STORE SECURITY']=item.rootFolderSummaryInfo.rootFolderDto.storePurpose ? item.rootFolderSummaryInfo.rootFolderDto.storePurpose : '';
    partialItem['DESCRIPTION']= item.rootFolderSummaryInfo.rootFolderDto.storeSecurity ? item.rootFolderSummaryInfo.rootFolderDto.storeSecurity : '';
    partialItem['MEDIA CONNECTION NAME']=item.rootFolderSummaryInfo.rootFolderDto.mediaConnectionName ? item.rootFolderSummaryInfo.rootFolderDto.mediaConnectionName : '';
    partialItem['EXTRACT BUSINESS LISTS']=item.rootFolderSummaryInfo.rootFolderDto.extractBizLists  ? item.rootFolderSummaryInfo.rootFolderDto.extractBizLists : '';
    partialItem['SCAN_TASK_DEPTH']=item.rootFolderSummaryInfo.rootFolderDto.scanTaskDepth;
    partialItem['SCHEDULE GROUP NAME']= item.rootFolderSummaryInfo.scheduleGroupDtos[0].name;


    return partialItem;
  }


  getScheduleConfigSummaryText = (scheduleConfigDto: ScheduleConfigDto) =>
  {
    var scheduleTypePart = scheduleConfigDto?  this.i18nService.translateId(scheduleConfigDto.scheduleType.toString())  :'';
    if (scheduleConfigDto && scheduleConfigDto.scheduleType==EScheduleConfigType.CUSTOM) {
      return scheduleTypePart;
    }
    let pad2 = function(number) {
      return (number < 10 ? '0' : '') + number
    }
    var scheduleTimePart = scheduleConfigDto?', every '+scheduleConfigDto.every+' '+(scheduleConfigDto.scheduleType==EScheduleConfigType.DAILY?
      (scheduleConfigDto.every == 1 ? 'day' : 'days') :scheduleConfigDto.scheduleType==EScheduleConfigType.HOURLY?   (scheduleConfigDto.every == 1 ?  'hour' : 'hours')  :   'week')+' at '+(scheduleConfigDto.hour!=null?
      pad2(scheduleConfigDto.hour) :'xx')+':'+ pad2(scheduleConfigDto.minutes) :'';
    var scheduleDaysOfWeekPart = scheduleConfigDto&&scheduleConfigDto.daysOfTheWeek?' on '+scheduleConfigDto.daysOfTheWeek.join():'';
    return scheduleTypePart+scheduleTimePart+scheduleDaysOfWeekPart;
  }

  getMediaConnectionRequest(mediaType: EMediaType, connectionId) {
    let mediaTypeStr = mediaType == EMediaType.ONE_DRIVE  ?  'onedrive'  : mediaType == EMediaType.SHARE_POINT  ?   'sharepoint':'box';
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.MEDIA_TYPE_ONE_CONNECTION_REQUEST  , {mediaTypeStr : mediaTypeStr, connectionId: connectionId }));
  }

  getMediaConnectionsRequest(mediaType: EMediaType) {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.MEDIA_TYPE_ALL_CONNECTION_REQUEST,{mediaType : mediaType}));
  }


  getScheduleGroupRequest(scheduleGroupId) {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.SCHEDULE_GROUP_BY_ID,{sgId : scheduleGroupId}));
  }

  getScheduleGroupsRequest() {
    return this.http.get(ServerUrls.ALL_SG_REQUEST);
  }

  getExcludedFolderRulesRequest(rootFolderId,take) {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.EXCLUDE_FOLDER_REQUEST , {rootFolderId : rootFolderId , take : take}));
  }

  getDataCenterRequest(centerId:number) {
    return this.http.get(this.rdbService.parseServerURL(ServerUrls.DATA_CENTER_REQUEST, {dcId : centerId}));
  }

  getDataCentersRequest() {
    return this.http.get(ServerUrls.ALL_DATA_CENTER_REQUEST);
  }

  getSystemComponentsRequest(params){
    const selectedItemId = params.selectedItemId;
    params = this.urlParamsService.fixedPagedParams(params);
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: 'desc', field: 'id'}]);
    }
    return this.http.get(ServerUrls.SYSTEM_COMPONENT_REQUEST, {params});
  }

  setScheduleGroupActiveState(sGroupId:number, isActive:boolean) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.SG_ACTIVE_STATE,{sGroupId : sGroupId}), isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.scheduleGroupRefreshedEvent.next();
    });
  }

  setRootFolderActiveState(rootFolderId:number, isActive:boolean) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.RF_ACTIVE_STATE,{rfId : rootFolderId}) , isActive, {headers:{'Content-Type': 'application/json;charset=UTF-8'}}).subscribe(() => {
      this.rootFolderRefreshedEvent.next();
    });
  }

  getRootFolderMoreDetails(runId,finishFunc) {
    this.rootFolderGetMoreDetailsEvent.next({
      runId:runId,
      finishFunc:finishFunc
    });
  }

  getScanHistoryDetails(runId,finishFunc) {
    this.scanHistoryGetMoreDetailsEvent.next({
      runId:runId,
      finishFunc:finishFunc
    });
  }


  getRunSummaryObject(rootFolderItem:any):RunSummaryInfoView
  {
    let runView:any = {};
    if (!rootFolderItem.runningPhase || !rootFolderItem.crawlRunDetailsDto) {
      return null;
    }

    var aggregatedJob = {
      arrType: [],
      arrName: [],
      total: 0,
      counter: 0,
      startTime: 0,
      tasksPerSecRate: 0,
      tasksPerSecRunningRate: 0
    };

    let activeJobs =[rootFolderItem.runningPhase];

    _.each(activeJobs, function(job) {
      aggregatedJob.arrType.push(job.jobType);
      aggregatedJob.arrName.push(job.name);
      aggregatedJob.counter += job.phaseCounter || 0;
      aggregatedJob.tasksPerSecRate += job.tasksPerSecRate || 0;
      aggregatedJob.tasksPerSecRunningRate += job.tasksPerSecRunningRate || 0;
      aggregatedJob.startTime = Math.min(aggregatedJob.startTime, job.phaseStart);

      if (!_.isNull(aggregatedJob.total)) {
        aggregatedJob.total = job.phaseMaxMark ? aggregatedJob.total + job.phaseMaxMark : null;
      }
    });

    runView.id = rootFolderItem.crawlRunDetailsDto.id;
    runView.isRunning = rootFolderItem.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING;
    if(rootFolderItem.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING)
    {
      runView.elapsedTime = (new Date().getTime())-rootFolderItem.crawlRunDetailsDto.scanStartTime;
    }
    else
    {
      runView.elapsedTime = (rootFolderItem.crawlRunDetailsDto.scanEndTime - rootFolderItem.crawlRunDetailsDto.scanStartTime) ;
    }
    runView.nickName = rootFolderItem.rootFolderDto.nickName?rootFolderItem.rootFolderDto.nickName:rootFolderItem.rootFolderDto.realPath;
    runView.runStatus = rootFolderItem.crawlRunDetailsDto.runOutcomeState;
    runView.startTime = rootFolderItem.crawlRunDetailsDto.scanStartTime?rootFolderItem.crawlRunDetailsDto.scanStartTime:null;
    runView.endTime = rootFolderItem.crawlRunDetailsDto.scanEndTime;
    runView.jobType = aggregatedJob.arrType.join(',');
    runView.jobName = aggregatedJob.arrName.join(',');
    runView.isManualRun = rootFolderItem.crawlRunDetailsDto.manual;
    runView.rootFolderId = rootFolderItem.rootFolderDto.id;
    runView.scheduleGroupName = rootFolderItem.crawlRunDetailsDto.scheduleGroupName;
    runView.scheduleGroupId = rootFolderItem.crawlRunDetailsDto.scheduleGroupId;
    // //limit to no more than 100% progress

    runView.estimatedPhaseFinish = null;
    runView.tasksPerSecRate = aggregatedJob.tasksPerSecRate;
    runView.tasksPerSecRateString = aggregatedJob.tasksPerSecRate ?  Number(aggregatedJob.tasksPerSecRate).toFixed(aggregatedJob.tasksPerSecRate<50 ? 2 : 0): null;
    runView.currentPhaseProgressCounter = aggregatedJob.counter;
    runView.currentPhaseProgressFinishMark =  aggregatedJob.total || 0;
    runView.currentPhaseProgressPercentage = Math.floor( aggregatedJob.counter / (aggregatedJob.total || 1) * 100);
    runView.tasksPerSecRunningRateString = aggregatedJob.tasksPerSecRunningRate ? Number(aggregatedJob.tasksPerSecRunningRate).toFixed(aggregatedJob.tasksPerSecRunningRate<50 ? 2 : 0): null;


    if (runView.currentPhaseProgressFinishMark > 0 && runView.tasksPerSecRate > 0) {
      var counterLeft = runView.currentPhaseProgressFinishMark - runView.currentPhaseProgressCounter;
      var secondsLeft = counterLeft/runView.tasksPerSecRate + 60;  // Add an extra minute
      runView.estimatedPhaseFinish = secondsLeft*1000;  //convert into time in mili secionds
    }
    return runView;
  }


  markForFullScanMany(rfIds: number[],doReIngest: boolean) {
    this.http.post(this.rdbService.parseServerURL(ServerUrls.MARK_FOR_FULL_SCAN_MANY, {'rootFolderIds' : rfIds.join(','),'reIngest' : doReIngest }), {}).subscribe(() => {
      this.rootFolderRefreshedEvent.next();
    }, (err)=>{
      this.showError(err,this.i18nService.translateId(doReIngest ? 'SCAN_ERR_MARK' : 'SCAN_ERR_UNMARK'));
    });
  }


}

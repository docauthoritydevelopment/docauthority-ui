import {Routes} from '@angular/router';
import {ActiveScanComponent} from "@app/features/scan/activeScan/active-scan.component";
import {ScanCenterComponent} from "@app/features/scan/scanCenter/scan-center.component";
import {BusinessListComponent} from "@app/features/scan/businessList/business-list.component";
import {ScanHistoryScreenComponent} from "@app/features/scan/scanHistory/scan-history-screen.component";
import {RouteUrlParts} from "@app/types/routing.urls";
import {ScheduleGroupScreenComponent} from "@app/features/scan/scheduleGroupScreen/schedule-group-screen.component";
import {ServerComponentScreenComponent} from "@app/features/scan/serverComponentsScreen/serverComponentScreen.component";
import {ScanComponent} from "@app/features/scan/scan.component";
import {TranslationGuard} from "@app/share-services/translation.guard";


export const scanRoutes: Routes = [
  {
    path: '',
    component: ScanComponent,
    children: [
      {
        path: RouteUrlParts.ActiveScans,
        component: ActiveScanComponent
      },
      {
        path: RouteUrlParts.ScheduleGroup,
        component: ScheduleGroupScreenComponent
      },
      {
        path: RouteUrlParts.ScanHistory,
        component: ScanHistoryScreenComponent
      },
      {
        path: RouteUrlParts.ScanCenter,
        component: ScanCenterComponent
      },
      {
        path: RouteUrlParts.BizList,
        component: BusinessListComponent
      },
      {
        path: RouteUrlParts.ServerComponent,
        component: ServerComponentScreenComponent
      }
    ]
  }
];

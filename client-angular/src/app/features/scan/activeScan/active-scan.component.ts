import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'
import {
  TreeGridSortOrder
} from '@tree-grid/types/tree-grid.type';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {UtilService} from '@services/util.service';
import {ScanQueryParams, ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {
  ActiveScanMode,
} from "@app/common/scan/types/scanEnum.types";
import {BaseComponent} from "@core/base/baseComponent";
import {DaFilterService} from "@app/common/filters/services/da-filter.service";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {MainLoaderService} from "@services/main-loader.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {MetadataService} from "@services/metadata.service";
import {BreadCrumb} from "@app/layout/breadcrumb/breadcrumb";
import {breadcrumbItem} from "@app/common/scan/types/scan.dto";
import {UserSettingsService} from "@services/user.settings.service";
import {I18nService} from "@app/common/translation/services/i18n-service";


@Component({
  selector: 'active-scan',
  templateUrl: './active-scan.component.html',
  styleUrls: ['./active-scan.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ActiveScanComponent extends BaseComponent{

  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;

  showFilter:boolean = true;
  ActiveScanMode = ActiveScanMode;
  storageKey='ACTIVE_SCANS';

  activeScanMode:string= null;

  rFolderParams: any  = null;
  sGroupParams: any  = null;
  sHistoryParams: any  = null;
  loading: boolean = false;
  inAutomaticRefresh:boolean = true;
  breadCrumbslist: breadcrumbItem[] = [];

  filterKeysList = [ScanPathParams.scheduleGroup,ScanPathParams.dataCenter,ScanPathParams.runStatus,ScanPathParams.runPhase,ScanPathParams.mediaType,ScanPathParams.accessibility,ScanPathParams.scanningOrder,ScanPathParams.onlyFavorite];

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private i18nService : I18nService,
              private utilService: UtilService,
              private userSettingsService : UserSettingsService,
              private dialogService: DialogService,
              private daFilterServer:DaFilterService,
              private mainLoaderService:MainLoaderService,
              private metadataService: MetadataService,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();
  }


  daOnInit() {
    this.pageTitleService.setTitle('Active scans');
    this.on(this.scanRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData(true);
      }
    }));
    const params: any = this.scanRouteManager.getLatestQueryParams();
    this.initialSortFieldName = params.sortField ? params.sortField : null;
    this.initialSortDirection = params.sortDesc ? params.sortDesc : null;
    this.loadData(true);
  }


  onSetAutomaticRefresh() {
    this.inAutomaticRefresh = !this.inAutomaticRefresh;
  }




  private loadData(withLoader:boolean = false) {
    if (withLoader){
      this.showLoader(true);
    }
    let  params = _.cloneDeep(this.scanRouteManager.getLatestQueryParams()) || {};
    this.activeScanMode = ActiveScanMode.ROOT_FOLDERS;
    if (params[ScanPathParams.activeScanMode] ) {
      this.activeScanMode = params[ScanPathParams.activeScanMode];
    }
    if (this.activeScanMode == ActiveScanMode.SCAN_HISTORY) {
        this.breadCrumbslist = [
        {
          title :  params[ScanPathParams.scheduleGroupName] ?   params[ScanPathParams.scheduleGroupName] : this.i18nService.translateId('ROOTFOLDER.MAIN_TITLE') ,
          clickFunc : this.changeToRootFolderMode
        },
        {
          title : params[ScanPathParams.rootFolderName] ? 'Scan history of folder: '+ params[ScanPathParams.rootFolderName] :   'Scan History'
        }];
      this.sHistoryParams = params;
      if (params[ScanPathParams.shPage]) {
        this.sHistoryParams[ScanPathParams.page] = params[ScanPathParams.shPage];
      }
    }
    else if (this.activeScanMode == ActiveScanMode.SCHEDULE_GROUPS) {
      this.breadCrumbslist = [
        {
          title : this.i18nService.translateId('ROOTFOLDER.MAIN_TITLE'),
          clickFunc : this.changeToRootFolderMode
        }];
      this.sGroupParams = params;
      if (params[ScanPathParams.sgPage]) {
        this.sGroupParams[ScanPathParams.page] = params[ScanPathParams.sgPage];
      }
    }
    else if (this.activeScanMode == ActiveScanMode.ROOT_FOLDERS) {
      this.breadCrumbslist = [
          {
            title : this.i18nService.translateId('ROOTFOLDER.MAIN_TITLE')
          }];
      if (params[ScanPathParams.scheduleGroup] != null) {
        this.breadCrumbslist = [
          {
            title : this.i18nService.translateId('ROOTFOLDER.MAIN_TITLE'),
            clickFunc: this.clearSGroups
          }];
        if (params[ScanPathParams.scheduleGroupName]) {
          this.breadCrumbslist.push({
            title : params[ScanPathParams.scheduleGroupName]
          });
        }
        else {
          this.breadCrumbslist.push({
            title : 'Selected schedule groups'
          });
        }
      }
      this.rFolderParams = params;
      if (params[ScanPathParams.rfPage]) {
        this.rFolderParams[ScanPathParams.page] = params[ScanPathParams.rfPage];
      }
    }
  }



  onRfPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.rfPage, pageNum);
  }


  onSgPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.sgPage, pageNum);
  }

  onSHistoryPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.shPage, pageNum);
  }

  onParametersChanged = (newParamsObject : {keys : string[] , values : string[]}) => {
    this.routingService.updateParamList(this.route, newParamsObject.keys, newParamsObject.values);
  }

  changeToGroupMode = ()=>  {
    this.routingService.updateParamList(this.route,[ScanPathParams.activeScanMode, ScanPathParams.scheduleGroup, ScanPathParams.scheduleGroupName],[ActiveScanMode.SCHEDULE_GROUPS,'','']);
  }

  changeToSHistoryMode = ()=>  {
    this.routingService.updateParam(this.route,ScanPathParams.activeScanMode,ActiveScanMode.SCAN_HISTORY);
  }

  changeToRootFolderMode = ()=>  {
    this.routingService.updateParam(this.route,ScanPathParams.activeScanMode,ActiveScanMode.ROOT_FOLDERS);
  }

  clearSGroups = ()=> {
    this.routingService.updateParamList(this.route, [ScanPathParams.scheduleGroupName,ScanPathParams.scheduleGroup], ['','']);
  }
}




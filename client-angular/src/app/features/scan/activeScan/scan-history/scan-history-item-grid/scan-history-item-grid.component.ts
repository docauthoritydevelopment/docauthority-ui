import * as _ from 'lodash';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '../../../../../shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {
  EJobState,
  ERunType
} from "../../../../../common/scan/types/scanEnum.types";
import {ERunStatus} from "../../../../../common/scan/types/scanEnum.types";
import {UtilService} from "@services/util.service";
import {ScanService} from "@app/features/scan/services/scan.service";
import {EPauseReason} from "@app/common/scan/types/scanEnum.types";
import {CrawlRunDetailsDto} from "@app/common/scan/types/scan.dto";
import {CommonConfig} from "@app/common/configuration/common-config";



@Component({
  selector: 'scan-history-item-grid',
  templateUrl: './scan-history-item-grid.component.html'
})
export class ScanHistoryItemGridComponent extends TreeGridInnerItemBaseComponent<CrawlRunDetailsDto> implements OnInit {
  @ViewChild('runs') runs: ElementRef;
  @ViewChild('el') el: ElementRef;
  EJobState = EJobState;
  lastRunElapsedTime: number;
  isBizListExtraction: boolean = false;
  isScheduleGroupRun: boolean = false;
  ERunStatus = ERunStatus;
  CommonConfig = CommonConfig;
  runStatus:ERunStatus;

  constructor(private utilService:UtilService,private scanService: ScanService) {
    super();
  }


  ngOnInit(): void {
    if(_.isEmpty(this.dataItem.item.rootFolderDto.realPath)) {
      this.dataItem.item.rootFolderDto.realPath = this.dataItem.item.rootFolderDto.mailboxGroup || null;
    }
    this.isScheduleGroupRun = this.dataItem.item.runType == ERunType.SCHEDULE_GROUP;
    let isRunning = this.dataItem.item.runOutcomeState == ERunStatus.RUNNING;
    let endTime = isRunning ? Date.now(): this.dataItem.item.scanEndTime;
    this.lastRunElapsedTime =  endTime?   Math.floor(endTime- this.dataItem.item.scanStartTime):null;
    this.isBizListExtraction = this.dataItem.item.rootFolderDto && this.dataItem.item.rootFolderDto.realPath==null;

    this.runStatus = this.dataItem.item.runOutcomeState;
    if (this.runStatus == ERunStatus.RUNNING && this.dataItem.item.gracefulStop) {
      this.runStatus = ERunStatus.STOPPING;
    }
    if (this.runStatus == ERunStatus.PAUSED && this.dataItem.item.pauseReason != EPauseReason.USER_INITIATED) {
      this.runStatus = ERunStatus.SUSPENDED;
    }
  }

  expandOrCollapseNode = ()=>  {
    if (this.dataItem.isOpen || this.dataItem.item.jobs!= null) {
      this.dataItem.isOpen = !this.dataItem.isOpen;
      setTimeout(()=> {
        if (!this.utilService.isInViewport(this.runs.nativeElement)) {
          this.runs.nativeElement.scrollIntoView(false);
        }
      });
    }
    else {
      this.scanService.getScanHistoryDetails(this.dataItem.item.id, (jobs)=> {
        if (jobs ) {
          this.dataItem.item.jobs = jobs;
          this.dataItem.isOpen = true;
          setTimeout(()=> {
            if (!this.utilService.isInViewport(this.runs.nativeElement)) {
              this.runs.nativeElement.scrollIntoView(false);
            }
          });
        }
      });
    }
  }

}


import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridConfig,
  TreeGridData, TreeGridSortOrder
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {
  ActiveScanMode,
  ERunStatus,
  SCAN_SERVER_PARAMS
} from "@app/common/scan/types/scanEnum.types";
import {ScanHistoryGridHeaderComponent} from "@app/features/scan/activeScan/scan-history/scan-history-grid-header/scan-history-grid-header.component";
import {ScanHistoryItemGridComponent} from "@app/features/scan/activeScan/scan-history/scan-history-item-grid/scan-history-item-grid.component";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {MetadataService} from "@services/metadata.service";
import {FilterTypes} from "@app/common/filters/types/filter-enums";
import {UtilService} from "@services/util.service";
import {
  breadcrumbItem, CrawlRunDetailsDto,
  filterChangedOutput,
  RootFoldersAggregatedSummaryInfo
} from "@app/common/scan/types/scan.dto";
import {FilterItem} from "@app/common/filters/types/filter-interfaces";
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ServerExportService} from "@services/serverExport.service";


@Component({
  selector: 'scan-history',
  templateUrl: './scan-history.component.html',
  styleUrls: ['./scan-history.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScanHistoryComponent extends BaseComponent{

  currentShData: TreeGridData<CrawlRunDetailsDto>;
  loading:boolean = false;
  sHistoryTreeGridEvents: ITreeGridEvents<any>;
  ScanHistoryItemGridComponent = ScanHistoryItemGridComponent;
  ScanHistoryGridHeaderComponent = ScanHistoryGridHeaderComponent;
  rootFoldersTitle:string = '';
  scanHistoryTitle: string = '';
  totalSHistoryItems: number = 0;
  showFilter: boolean = true;
  inLoadingSubTree:boolean = false;
  rootFolderSummaryObject:RootFoldersAggregatedSummaryInfo  = null;
  _params: any  = null;
  filtersList: FilterItem[] = [];
  FilterTypes = FilterTypes;
  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;
  exportDropDownItems: DropDownConfigDto[] = [];

  rootFolderName:string = null;
  latestParamsStr:string = null;
  searchTerm: string='';
  cancelCallNotFinishNumber: number = 0 ;
  treeConfig = <TreeGridConfig>{
    enableSwitchSelectionMode: false,
    disableSelection:true
  };

  @Output() pageChanged = new EventEmitter();
  @Output() parametersChanged = new EventEmitter();
  @Output() switchAutomaticRefresh  = new EventEmitter();

  @Input()
  inAutomaticRefresh: boolean;

  @Input()
  withRootFolderSummary: boolean = false;

  @Input()
  breadCrumbs: breadcrumbItem[] = [];


  @Input()
  set params(newParams:any) {
    this._params = newParams;
    if (this._params[ScanPathParams.shRunStatus]) {
      this._params[SCAN_SERVER_PARAMS.STATE] = this._params[ScanPathParams.shRunStatus]
    }
    this.loadData(true);
  }

  get params(): any {
    return this._params;
  }


  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);



  exportSHistoryData = ()=> {
    this.serverExportService.prepareExportFile('Scan_history',ExportType.SCAN_HISTORY,this.params,{itemsNumber : this.totalSHistoryItems},this.totalSHistoryItems > CommonConfig.minimum_items_to_open_export_popup);
  }

  sHistoryDropDownItems = []

  constructor (private serverExportService: ServerExportService,private i18nService : I18nService , private scanService: ScanService, private dialogService: DialogService, private metadataService: MetadataService, private utilService: UtilService, private subHeaderService: SubHeaderService) {
    super();
  }




  daOnInit() {
    this.subHeaderService.setSubHeader(this.i18nService.translateId('HISTORY.SUB_HEADER'));
    this.exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_TO_EXCEL'),
      iconClass: "fa fa-download",
      actionFunc: this.exportSHistoryData
    }];

    this.on(this.scanService.jobListRefreshedEvent$,()=> {
      this.loadData();
    });

    this.on(this.scanService.scanHistoryGetMoreDetailsEvent$, (moreDetailsObj:any ) => {
      this.clearQuerySubscription();
      this.showLoader(true);
      this.inLoadingSubTree = true;
      return this.scanService.loadSubActiveScansByRunId(moreDetailsObj.runId).subscribe((jobs)=>
      {
        this.showLoader(false);
        this.inLoadingSubTree = false;
        moreDetailsObj.finishFunc(jobs);
      });
    });
    this.sHistoryTreeGridEvents = {
      onPageChanged: this.onScanHistoryPageChanged,
      onSortByChanged: this.onSortByChanged
    };
    this.filtersList = [
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, [
          {
            value: ERunStatus.FINISHED_SUCCESSFULLY,
            displayText: 'SCAN.OK',
          },
          {
            value: ERunStatus.FAILED,
            displayText: 'SCAN.FAILED',
          },
          {
            value: ERunStatus.PAUSED,
            displayText: 'SCAN.PAUSED/SUSPENDED',
          },
          {
            value: ERunStatus.RUNNING,
            displayText: 'SCAN.RUNNING',
          },
          {
            value: ERunStatus.STOPPED,
            displayText: 'SCAN.STOPPED',
          }
          ], ScanPathParams.shRunStatus),
          filterTitle : this.i18nService.translateId('SCAN.STATUS'),
          filterField : ScanPathParams.shRunStatus
      }];
    this.loadData(true);
  }

  private onScanHistoryPageChanged = (pageNum) => {
    this.pageChanged.emit(pageNum);
  }


  daOnDestroy() {
  }


  private setBreadcrumbTitle = (params)=> {
    this.rootFoldersTitle = 'All Root Folders';
    if (params[ScanPathParams.scheduleGroup]) {
      let selectedGroups = params[ScanPathParams.scheduleGroup].split(',');
      if (selectedGroups.length == 1) {
        let sGroupItem = this.metadataService.getMetadataKeyValueItem(MetadataType.SCHEDULE_GROUP, selectedGroups[0]);
        this.rootFoldersTitle =  sGroupItem ? sGroupItem.displayText : '';
      }
      else if (selectedGroups.length > 1) {
        this.rootFoldersTitle = 'Root Folders of selected schedule groups';
      }
    }
  }

  private updateFilters() {
    if (this.filtersList && this.filtersList.length>0) {
      this.utilService.updateCheckArray(this.params,  ScanPathParams.shRunStatus, this.filtersList[0].filterOptions);
    }
    this.filtersList = _.cloneDeep(this.filtersList);
  }

  onSearchChange = (newText) => {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.shTextSearch, ScanPathParams.shPage],
        values: [newText, 1]
      });
  }


  loadData = (withLoader:boolean = false)=>  {
    if (!withLoader && (this.inLoadingSubTree || this.isDropDownOpen())) {
      this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_CHECK_PERIOD);
      return;
    }

    this.updateFilters();
    this.searchTerm =  this.params[ScanPathParams.shTextSearch] ? this.params[ScanPathParams.shTextSearch] : '';

    if (!this.params[ScanPathParams.sort]) {
      this.params[ScanPathParams.sort] = JSON.stringify([{"dir": 'desc', "field" : 'id'}]);
      this.initialSortFieldName = 'id';
      this.initialSortDirection = TreeGridSortOrder.DESC;
    }
    if (this.params[ScanPathParams.sort]) {
      let sortObj = JSON.parse(this.params[ScanPathParams.sort]);
      this.initialSortFieldName = sortObj[0].field;
      this.initialSortDirection = sortObj[0].dir;
    }

    this.scanHistoryTitle = 'All Scan History';
    if (this.params[ScanPathParams.rootFolderName]) {
      this.scanHistoryTitle = 'Scan history of '+this.params[ScanPathParams.rootFolderName];
    }

    if (withLoader) {
      this.showLoader(true);
    }
    let queriesArray = [];
    this.setBreadcrumbTitle(this.params);
    queriesArray.push(this.scanService.getScanHistoryTableRequest(this.params), this.scanService.getRootFoldersScanSummaryRequest());

    let openNodes = [];
    if (this.currentShData) {
      openNodes =  this.currentShData.items.filter(dataItem=> dataItem.isOpen).map(dataItem => dataItem.item.id);
    }
    openNodes.forEach((jobId)=> {
      queriesArray.push(this.scanService.getRunProcessesByIdQuery(jobId));
    });

    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([scanHistoryTable,scanSummary ,  ...jobsArrayList]) => {
      this.cancelCallNotFinishNumber = 0 ;
      let jobArrayMap:any = {};

      for (let count = 0 ; count < openNodes.length ; count++) {
        jobArrayMap[openNodes[count]] = jobsArrayList[count];
      };
      scanHistoryTable.items.forEach((historyItem)=> {
        if (jobArrayMap[historyItem.id]) {
          historyItem.item.jobs = jobArrayMap[historyItem.id];
          historyItem.isOpen = true;
        }
      });
      this.currentShData = scanHistoryTable;
      this.totalSHistoryItems = this.currentShData.totalElements;
      this.rootFolderSummaryObject = scanSummary;
      this.showLoader(false);
      if (this.inAutomaticRefresh) {
        this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_PERIOD)
      }
    }));
  }



  changeToAllRootFolderMode(){
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.activeScanMode,ScanPathParams.rootFolderId, ScanPathParams.rootFolderName],
        values: [ActiveScanMode.ROOT_FOLDERS, '' , '' ]
      });
  }

  changeToSGroupMode() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.activeScanMode, ScanPathParams.scheduleGroup,ScanPathParams.rootFolderId, ScanPathParams.rootFolderName],
        values: [ActiveScanMode.SCHEDULE_GROUPS, '', '' , '']
      });
  }

  onSetAutomaticRefresh(){
    this.switchAutomaticRefresh.emit();
  }


  onFilterChanged(filterOutput: filterChangedOutput ) {
    let keysArr = filterOutput.fields;
    keysArr.push(ScanPathParams.shPage);
    let valuesArr = filterOutput.filterOptions.map(filterOption => this.utilService.getFilterString(filterOption));
    valuesArr.push('1');
    this.parametersChanged.emit(
      {
        keys: keysArr,
        values: valuesArr
      });
  }

  private onSortByChanged = (sortObj: any) => {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.sort],
        values: [JSON.stringify([{"dir": sortObj.sortOrder.toLowerCase(), "field" : sortObj.sortBy}])]
      });
  }
}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '../../../../../shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {EJobState, SCAN_OPERATION} from "../../../../../common/scan/types/scanEnum.types";
import {ERunStatus} from "../../../../../common/scan/types/scanEnum.types";
import {RoutingService} from "../../../../../core/services/routing.service";
import {ActivatedRoute} from '@angular/router';
import {DialogService} from "../../../../../shared-ui/components/modal-dialogs/dialog.service";
import {ScanService} from "../../../services/scan.service";
import {RunScanDialogComponent} from "../../../popups/runScanDialog/runScanDialog.component";
import {Router} from '@angular/router';
import {EPauseReason} from "@app/common/scan/types/scanEnum.types";
import {ScheduleGroupSummaryInfoDto} from "@app/common/scan/types/scan.dto";
import {AppUrls} from "@core/types/app-urls";
import {CommonConfig} from "@app/common/configuration/common-config";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {RouteUrls} from "@app/types/routing.urls";


@Component({
  selector: 'schedule-group-item-grid',
  templateUrl: './schedule-group-item-grid.component.html'
})
export class ScheduleGroupItemGridComponent extends TreeGridInnerItemBaseComponent<ScheduleGroupSummaryInfoDto> implements OnInit {

  @ViewChild('el') el: ElementRef;
  summaryText: string =  '';
  nextScanTime: number = null;
  lastScanTime: number = null;
  lastEndTime: number = null;
  EJobState = EJobState;
  additionalDropDownItems: DropDownConfigDto[] = null;
  ERunStatus = ERunStatus;
  afterStartLoading:boolean = false;
  afterPauseLoading:boolean = false;
  afterResumeLoading:boolean = false;
  afterStopLoading:boolean = false;
  CommonConfig = CommonConfig;
  RouteUrls = RouteUrls;

  constructor(private routingService: RoutingService,private route: ActivatedRoute, private dialogService: DialogService, private scanService: ScanService, private router: Router, private i18nService: I18nService) {
    super();
  }


  ngOnInit(): void {
    this.additionalDropDownItems = [
      {
        title: this.i18nService.translateId('SGROUPS.SCAN_DD.ACTIONS'),
        children: [
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.RUN'),
            iconClass: "fa fa-play",
            actionFunc: this.dataItem.item.runStatus && (this.dataItem.item.runStatus == ERunStatus.PAUSED || this.dataItem.item.runStatus == ERunStatus.SUSPENDED)    ? this.resumeScanGroup : this.runScanGroup,
            disabled : this.dataItem.item.runStatus && this.dataItem.item.runStatus == ERunStatus.RUNNING
          },
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.PAUSE'),
            iconClass: "fa fa-pause",
            actionFunc: this.pauseScanGroup,
            disabled: !this.dataItem.item.runStatus || this.dataItem.item.runStatus != ERunStatus.RUNNING
          },
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.STOP'),
            iconClass: "fa fa-stop",
            actionFunc: this.stopScanGroup,
            disabled: !this.dataItem.item.runStatus || !(this.dataItem.item.runStatus == ERunStatus.PAUSED || this.dataItem.item.runStatus == ERunStatus.SUSPENDED)
          },
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.ADVANCED'),
            iconClass: "fa fa-graduation-cap",
            actionFunc: this.moreScanOptions,
            disabled: false
          }]
      },
      {
        title: this.i18nService.translateId('SGROUPS.SCAN_DD.NAVIGATION'),
        children: [
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.SG_SETTING'),
            iconClass: "fa fa-pencil",
            href: AppUrls.SCHEDULE_GROUPS_SETTING,
          },
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.VIEW_RF'),
            iconClass: "ion ion-ios-briefcase-outline",
            href: AppUrls.ACTIVE_SCAN_ROOT_FOLDERS,
            hrefParams : {
              scheduleGroup : this.dataItem.item.scheduleGroupDto.id,
              scheduleGroupName : this.dataItem.item.scheduleGroupDto.name
            }
          }
        ]
      }
    ];

    this.summaryText = this.scanService.getScheduleConfigSummaryText(this.dataItem.item.scheduleGroupDto.scheduleConfigDto);
    if (this.dataItem.item.nextPlannedWakeup){
      this.nextScanTime = Math.floor(this.dataItem.item.nextPlannedWakeup-Date.now());
    }
    let isRunning = (this.dataItem.item.runStatus == ERunStatus.RUNNING);

    if (this.dataItem.item.runStatus == ERunStatus.PAUSED && this.dataItem.item.lastCrawlRunDetails.pauseReason != EPauseReason.USER_INITIATED) {
      this.dataItem.item.runStatus = ERunStatus.SUSPENDED;
    }

    this.lastEndTime  =  isRunning ?   Date.now()  :   (this.dataItem.item.lastCrawlRunDetails ? this.dataItem.item.lastCrawlRunDetails.scanEndTime : null);
    this.lastScanTime = this.lastEndTime ? Math.floor(this.lastEndTime-this.dataItem.item.lastCrawlRunDetails.scanStartTime):null;
    this.initLoadingBtns();
  }


  private initLoadingBtns() {
    this.afterStartLoading = this.scanService.startRunningScheduleGroupsMap[this.dataItem.item.scheduleGroupDto.id+''] == true;
    if (this.dataItem.item.lastCrawlRunDetails) {
      this.afterStopLoading = this.scanService.startStopScheduleScanMap[this.dataItem.item.lastCrawlRunDetails.id + ''] == true;
      this.afterResumeLoading = this.scanService.startResumeScheduleScanMap[this.dataItem.item.lastCrawlRunDetails.id + ''] == true;
      this.afterPauseLoading = this.scanService.startPauseScheduleScanMap[this.dataItem.item.lastCrawlRunDetails.id + ''] == true;
    }
  }

  runScanGroup = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId("SGROUPS.CONFIRM.START",{sg_name : this.dataItem.item.scheduleGroupDto.name})).then(result => {
      if (result) {
        this.scanService.startScanGroup(this.dataItem.item.scheduleGroupDto.id+'',SCAN_OPERATION.FULL);
        this.initLoadingBtns();
      }
    });
  }

  resumeScanGroup = ()=>   {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId("SGROUPS.CONFIRM.RESUME",{sg_name : this.dataItem.item.scheduleGroupDto.name})).then(result => {
      if (result) {
        this.scanService.resumeScan([this.dataItem.item.lastCrawlRunDetails.id]);
        this.initLoadingBtns();
      }
    });
  }

  pauseScanGroup  = ()=>   {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId("SGROUPS.CONFIRM.PAUSE",{sg_name : this.dataItem.item.scheduleGroupDto.name,num_of_days : CommonConfig['handle-stuck-in-pause-runs.stop-threshold-days'] })).then(result => {
      if (result) {
        this.scanService.pauseScan([this.dataItem.item.lastCrawlRunDetails.id]);
        this.initLoadingBtns();
      }
    });
  }

  stopScanGroup = ()=>   {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId("SGROUPS.CONFIRM.STOP",{sg_name : this.dataItem.item.scheduleGroupDto.name})).then(result => {
      if (result) {
        this.scanService.stopScan([this.dataItem.item.lastCrawlRunDetails.id]);
        this.initLoadingBtns();
      }
    });
  }

  editSGroup = ()=>  {
    this.router.navigate(['/'+RouteUrls.SettingsScheduleGroups],{
      queryParams : {
        findId : this.dataItem.item.scheduleGroupDto.id
      }
    });
  }

  setActive(isActive:boolean) {
    this.scanService.setScheduleGroupActiveState(this.dataItem.item.scheduleGroupDto.id,isActive);
  }

  moreScanOptions = ()=>{
    this.dialogService.openModalPopup(RunScanDialogComponent,{data : {onlyMapMode : true, selectedName : 'schedule group: '+this.dataItem.item.scheduleGroupDto.name }}).result.then((result : SCAN_OPERATION) =>
    {
      if (result) {
        this.scanService.startScanGroup(this.dataItem.item.scheduleGroupDto.id+'', result);
      }
    }, (reason) => {
    });
  }

}


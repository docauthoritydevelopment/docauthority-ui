import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridConfig,
  TreeGridData, TreeGridDataItem, TreeGridSelectionMode, TreeGridSortOrder
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ScheduleGroupItemGridComponent} from "@app/features/scan/activeScan/schedule-group/schedule-group-item-grid/schedule-group-item-grid.component";
import {ScheduleGroupGridHeaderComponent} from "@app/features/scan/activeScan/schedule-group/schedule-group-grid-header/schedule-group-grid-header.component";
import {ActiveScanMode, EJobState, SCAN_OPERATION, ERunStatus} from "@app/common/scan/types/scanEnum.types";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {breadcrumbItem, ScheduleGroupSummaryInfoDto, RootFoldersAggregatedSummaryInfo} from "@app/common/scan/types/scan.dto";
import {RunScanDialogComponent} from "@app/features/scan/popups/runScanDialog/runScanDialog.component";
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {AppUrls} from "@core/types/app-urls";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {ToolbarActionTypes} from "@app/types/display.type";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ServerExportService} from "@services/serverExport.service";

@Component({
  selector: 'schedule-group',
  templateUrl: './schedule-group.component.html',
  styleUrls: ['./schedule-group.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScheduleGroupComponent extends BaseComponent {

  currentSgData: TreeGridData<ScheduleGroupSummaryInfoDto>;
  totalSGroupItems = 0;
  loading = false;
  groupTreeGridEvents: ITreeGridEvents<ScheduleGroupSummaryInfoDto>;
  ScheduleGroupItemGridComponent = ScheduleGroupItemGridComponent;
  ScheduleGroupGridHeaderComponent = ScheduleGroupGridHeaderComponent;
  storageKey = 'ACTIVE_SCANS';
  rootFolderSummaryObject: RootFoldersAggregatedSummaryInfo = null;
  _params: any  = null;
  latestParamsStr: string = null;
  searchTerm = '';
  allCanPlay: boolean = false;
  allCanPause: boolean = false;
  allCanStop: boolean = false;
  allCanMore: boolean = false;
  lastSelectedIds = [];
  withRootFolders:boolean = false;
  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;
  TreeGridSelectionMode = TreeGridSelectionMode;

  refreshActionItem:ButtonConfigDto;
  monitorActionItems:ButtonConfigDto[];
  searchBox:SearchBoxConfigDto;
  exportActionItem:ButtonConfigDto;
  sideMenuActionItems:ButtonConfigDto;

  treeConfig = <TreeGridConfig>{
    enableSwitchSelectionMode: true,
    disableSelection:true
  };

  //----------------------------------------------
  // output
  //----------------------------------------------

  @Output() pageChanged = new EventEmitter();
  @Output() parametersChanged = new EventEmitter();
  @Output() switchAutomaticRefresh = new EventEmitter();

  //----------------------------------------------
  // input
  //----------------------------------------------

  @Input() inAutomaticRefresh: boolean;
  @Input() breadCrumbs: breadcrumbItem[] = [];
  @Input() withRootFolderSummary: boolean = false;

  //----------------------------------------------

  @Input()
  set params(newParams: any) {
    this._params = newParams;
    this.loadData(true);
  }

  get params(): any {
    return this._params;
  }

  //----------------------------------------------

  constructor (private serverExportService : ServerExportService, private scanService: ScanService, private dialogService: DialogService, private subHeaderService: SubHeaderService, private i18nService : I18nService) {
    super();
  }

  //----------------------------------------------
  // daOnInit
  //----------------------------------------------

  daOnInit() {
    this.subHeaderService.setSubHeader(this.i18nService.translateId('SGROUPS.SUB_HEADER'));

    this.on(this.scanService.startScanRootFolderEvent$, (ans: string[]) => {
      this.loadData();
    });

    this.on(this.scanService.stopScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.resumeScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.pauseScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.markFullEvent$, ()=>{
      this.loadData();
    });

    this.on(this.scanService.scheduleGroupChangedEvent$, (scheduleGroupId: number) => {
      this.loadData();
    });


    this.on(this.scanService.scheduleGroupRefreshedEvent$, ()=>{
      this.loadData();
    });

    this.buildTreeGridEvents();
    this.setActionItems();
  }

  //----------------------------------------------
  // daOnDestroy
  //----------------------------------------------

  daOnDestroy() {}

  //----------------------------------------------

  loadData = (withLoader:boolean = false) => {
    if (!withLoader && this.isDropDownOpen()) {
      this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_CHECK_PERIOD);
      return;
    }
    if (withLoader) {
      this.showLoader(true);
    }
    this.withRootFolders = this.params[ScanPathParams.withRootFolders] && this.params[ScanPathParams.withRootFolders]== 'true' ? true : false;
    let queriesArray = [];
    if (!this.params.sort) {
      this.params.sort = JSON.stringify([{dir: 'asc', field: 'name'}]);
    }
    else {
      let sortObj = JSON.parse(this.params.sort);
      this.initialSortFieldName = sortObj[0].field;
      this.initialSortDirection = sortObj[0].dir;
    }
    this.searchTerm =  this.params[ScanPathParams.sgTextSearch] ? this.params[ScanPathParams.sgTextSearch] : '';
    queriesArray.push(this.scanService.getRootFoldersScanSummaryRequest(),this.scanService.getScheduleGroupTableRequest(this.params));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([scanSummary , sgTable]) => {
      this.currentSgData = sgTable;
      this.totalSGroupItems = sgTable.totalElements;
      this.rootFolderSummaryObject = scanSummary;
      this.showLoader(false);
      if (this.inAutomaticRefresh) {
        this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_PERIOD)
      }
    }));
  };

  //----------------------------------------------

  onSearchChange = (newText) => {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.sgTextSearch, ScanPathParams.sgPage],
        values: [newText, 1]
      });
  };

  //----------------------------------------------

  exportSGroupData = () => {
    this.serverExportService.prepareExportFile('Active_schedule_group',ExportType.ACTIVE_SCHEDULED_GROUPS,this.params,{itemsNumber : this.totalSGroupItems},this.totalSGroupItems> CommonConfig.minimum_items_to_open_export_popup);
  };

  //----------------------------------------------

  changeToAllRootFolderMode() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.activeScanMode, ScanPathParams.scheduleGroup],
        values: [ActiveScanMode.ROOT_FOLDERS, '']
      });
  }

  //----------------------------------------------

  changeToScanHistoryMode() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.activeScanMode],
        values: [ActiveScanMode.SCAN_HISTORY]
      });
  }

  //----------------------------------------------

  onSetAutomaticRefresh() {
    this.switchAutomaticRefresh.emit();
  }

  //----------------------------------------------

  onCheckedItemsChanged = (items: any []) => {
    this.lastSelectedIds = items;
    setTimeout(()=> {
      let havePaused:boolean = false;
      let haveStopped:boolean = false;
      let haveRunning:boolean = false;
      this.allCanPlay = false;
      this.allCanPause = false;
      this.allCanStop = false;
      this.allCanMore = false;
      if (items.length > 0) {
        items.forEach((dataItem: TreeGridDataItem<any>) => {
          if (dataItem.item.runStatus == EJobState.RUNNING) {
            haveRunning = true;
          }
          else if (dataItem.item.runStatus == EJobState.PAUSED || dataItem.item.runStatus == ERunStatus.SUSPENDED) {
            havePaused = true;
          }
          else {
            haveStopped = true;
          }
        });

        if (havePaused || haveStopped){
          this.allCanMore = true;
          this.allCanPlay = true;
        }
        if (haveRunning){
          this.allCanPause = true;
        }
        if (havePaused){
          this.allCanStop = true;
        }
      }
      this.updateActionItems();
    });
  };

  //----------------------------------------------

  runScanFolder = () => {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId('SGROUPS.CONFIRM.START_SELECTED',{num_of_sg: this.lastSelectedIds.length })).then(result => {
      if (result) {
        let startList = [];
        let resumeList = [];
        this.lastSelectedIds.forEach((dataItem)=> {
          if (dataItem.item.runStatus == EJobState.PAUSED || dataItem.item.runStatus == ERunStatus.SUSPENDED) {
            resumeList.push(dataItem.item.lastCrawlRunDetails.id)
          }
          else {
            startList.push(dataItem.item.scheduleGroupDto.id)
          }
        });
        if (resumeList.length > 0 ) {
          this.scanService.resumeScan(resumeList);
        }
        if (startList.length > 0 ) {
          this.scanService.startScanGroup(startList.join(','),SCAN_OPERATION.FULL);
        }
        this.loadData();
      }
    });
  };

  //----------------------------------------------

  pauseScan = () => {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId('SGROUPS.CONFIRM.PAUSE_SELECTED',{num_of_sg: this.lastSelectedIds.length,num_of_days :  CommonConfig['handle-stuck-in-pause-runs.stop-threshold-days']})).then(result => {
      if (result) {
        this.scanService.pauseScan(this.lastSelectedIds.map(dataItem => dataItem.item.lastCrawlRunDetails.id));
        this.loadData();
      }
    });
  };

  stopScan = () => {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId("CONFIRMATION"), this.i18nService.translateId('SGROUPS.CONFIRM.STOP_SELECTED',{num_of_sg: this.lastSelectedIds.length })).then(result => {
      if (result) {
        this.scanService.stopScan(this.lastSelectedIds.map(dataItem => dataItem.item.lastCrawlRunDetails.id));
        this.loadData();
      }
    });
  };

  //----------------------------------------------

  moreScanOptions = () => {
    this.dialogService.openModalPopup(RunScanDialogComponent,{data : {inScheduleGroupMode : true, selectedName : this.lastSelectedIds.length +' selected schedule groups '}}).result.then((result : SCAN_OPERATION) =>
    {
      if (result) {
        this.scanService.startScanGroup(this.lastSelectedIds.map(dataItem => dataItem.item.scheduleGroupDto.id).join(","), result);
      }
    }, (reason) => {
    });
  };

  //----------------------------------------------

  toggleShowUnused() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.withRootFolders],
        values: [!this.withRootFolders]
      });
  }

  //----------------------------------------------
  // private
  //----------------------------------------------

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  //----------------------------------------------

  private buildTreeGridEvents() {
    this.groupTreeGridEvents = {
      onPageChanged: this.onSgPageChanged,
      onMarkedItemsChanged: this.onCheckedItemsChanged,
      onSortByChanged: this.onSortByChanged
    };
  }

  //----------------------------------------------

  private onSgPageChanged = (pageNum) => {
    this.pageChanged.emit(pageNum);
  };

  //----------------------------------------------

  private onSortByChanged = (sortObj: any) => {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.sort],
        values: [JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder).toLowerCase()}])]
      });
  };

  //----------------------------------------------

  private setMonitorActionItems() {
    this.monitorActionItems = [
      {
        type: ToolbarActionTypes.PLAY,
        title: this.i18nService.translateId('SCAN_OPTION.RUN_SCAN'),
        actionFunc: () => {
          if (this.allCanPlay) {
            this.runScanFolder();
          }
        },
        disabled: !this.allCanPlay,
        href: ToolbarActionTypes.PLAY,
        order: 0
      },
      {
        type: ToolbarActionTypes.PAUSE,
        title: this.i18nService.translateId('SCAN_OPTION.PAUSE_SCAN'),
        actionFunc: () => {
          if (this.allCanPause) {
            this.pauseScan();
          }
        },
        disabled: !this.allCanPause,
        href: ToolbarActionTypes.PAUSE,
        order: 1
      },
      {
        type: ToolbarActionTypes.STOP,
        title: this.i18nService.translateId('SCAN_OPTION.STOP_SCAN'),
        actionFunc: () => {
          if (this.allCanStop) {
            this.stopScan();
          }
        },
        disabled: !this.allCanStop,
        href: ToolbarActionTypes.STOP,
        order: 2
      }
    ];
  }

  //----------------------------------------------

  private setExportActionItem() {
    let exportDropDownItems:DropDownConfigDto[] = [{
      title: this.i18nService.translateId('EXPORT_TO_EXCEL'),
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportSGroupData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: "Export options",
      disabled: false,
      children: exportDropDownItems,
      href: ToolbarActionTypes.EXPORT
    };
  }

  //----------------------------------------------

  private setSideMenuActionItems() {
    let sgIds = '';
    if (this.lastSelectedIds.length > 0)
    {
      sgIds = this.lastSelectedIds.map((dataItem: TreeGridDataItem<any>) => {
        return dataItem.item.scheduleGroupDto.id;
      }).join(',');
    }
    let sgName = '';
    if (this.lastSelectedIds.length == 1) {
      sgName = this.lastSelectedIds[0].item.scheduleGroupDto.name;
    }

    let sideMenuDropDownItems:DropDownConfigDto[] = [{
      title: this.i18nService.translateId('SGROUPS.SCAN_DD.ACTIONS'),
      children: [{
        title: this.i18nService.translateId('SCAN_OPTION.RUN_SCAN'),
        iconClass: "fa fa-play",
        actionFunc: this.runScanFolder,
        disabled: !this.allCanPlay
      },
        {
          title: this.i18nService.translateId('SCAN_OPTION.PAUSE_SCAN'),
          iconClass: "fa fa-pause",
          actionFunc: this.pauseScan,
          disabled: !this.allCanPause
        },
        {
          title: this.i18nService.translateId('SCAN_OPTION.STOP_SCAN'),
          iconClass: "fa fa-stop",
          actionFunc: this.stopScan,
          disabled: !this.allCanStop
        },
        {
          title: this.i18nService.translateId('SCAN_OPTION.ADVANCED_SCAN'),
          iconClass: "fa fa-graduation-cap",
          actionFunc: this.moreScanOptions,
          disabled: !this.allCanMore
        }]},
      {
        title: this.i18nService.translateId('SGROUPS.SCAN_DD.NAVIGATION'),
        children: [{
          title: this.i18nService.translateId('SGROUPS.SCAN_DD.SG_SETTING'),
          iconClass: "fa fa-pencil",
          href: AppUrls.SCHEDULE_GROUPS_SETTING ,
        },
          {
            title: this.i18nService.translateId('SGROUPS.SCAN_DD.VIEW_RF'),
            iconClass: "ion ion-ios-briefcase-outline",
            href: AppUrls.ACTIVE_SCAN_ROOT_FOLDERS ,
            hrefParams: {
              scheduleGroup : sgIds,
              scheduleGroupName : sgName
            }}]}];

    this.sideMenuActionItems = {
      type: ToolbarActionTypes.MORE,
      title: this.i18nService.translateId('ADDITIONAL_OPTIONS'),
      disabled: false,
      children: sideMenuDropDownItems
    };
  }

  //----------------------------------------------

  private setActionItems() {
    this.refreshActionItem = {
      type: ToolbarActionTypes.REFRESH,
      title: "Refresh",
      disabled: false,
      actionFunc: () => { this.loadData(true) },
      href: ToolbarActionTypes.REFRESH
    };

    this.setMonitorActionItems();

    this.setExportActionItem();

    this.searchBox = {
      searchTerm: this.searchTerm,
      title: this.i18nService.translateId('SGROUPS.SEARCH_TITLE'),
      placeholder: this.i18nService.translateId('SGROUPS.SEARCH_PLACE_HOLDER'),
      searchFunc: this.onSearchChange
    };

    this.setSideMenuActionItems();
  }

  //----------------------------------------------

  private updateActionItems() {
    if (this.monitorActionItems) {
      this.monitorActionItems.find(item => item.type === ToolbarActionTypes.PLAY).disabled = !this.allCanPlay;
      this.monitorActionItems.find(item => item.type === ToolbarActionTypes.PAUSE).disabled = !this.allCanPause;
      this.monitorActionItems.find(item => item.type === ToolbarActionTypes.STOP).disabled = !this.allCanStop;
    }
    else {
      this.setMonitorActionItems();
    }

    this.setSideMenuActionItems();
  }
}

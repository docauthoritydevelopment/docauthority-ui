import {Component, OnDestroy, OnInit, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridConfig,
  TreeGridData, TreeGridDataItem, TreeGridSelectionMode, TreeGridSortOrder
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {
  ActiveScanMode,
  DirectoryExistStatus,
  EJobState,
  EMediaType,
  ERunStatus,
  SCAN_OPERATION, SCAN_STRATEGY
} from "@app/common/scan/types/scanEnum.types";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {FilterTypes} from "@app/common/filters/types/filter-enums";
import {MetadataType} from "@app/common/discover/types/common-enums";
import {MetadataService} from "@services/metadata.service";
import {UtilService} from "@services/util.service";
import {ScanQueryParams, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {
  breadcrumbItem,
  filterChangedOutput, RootFolderRunSummaryInfo,
  RootFoldersAggregatedSummaryInfo
} from "@app/common/scan/types/scan.dto";
import {RunScanDialogComponent} from "@app/features/scan/popups/runScanDialog/runScanDialog.component";
import {Observable} from 'rxjs/index';
import {RootFolderItemGridComponent} from "@app/features/scan/activeScan/root-folder/item-grid/root-folder-item-grid.component";
import {RootFolderItemGridHeaderComponent} from "@app/features/scan/activeScan/root-folder/grid-header/root-folder-item-grid-header.component";
import {map} from 'rxjs/operators';
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {EntityDisplayTypes} from "@core/types/discover.entity.types";
import {PlatformLocation} from '@angular/common';
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import {ScanFavoriteService} from "@app/features/scan/services/scan-favorite.service";
import {UserSettingsService} from "@services/user.settings.service";
import {StorageKeys} from "@core/types/storage-keys";
import {FilterItem} from "@app/common/filters/types/filter-interfaces";
import {AppUrls} from "@core/types/app-urls";
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ServerExportService} from "@services/serverExport.service";
import {LoginService} from "@services/login.service";
import {AppConfig} from "@services/appConfig.service";


@Component({
  selector: 'root-folder',
  templateUrl: './root-folder.component.html',
  styleUrls: ['./root-folder.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RootFolderComponent extends BaseComponent{

  currentData: TreeGridData<RootFolderRunSummaryInfo>;
  loading = false;
  treeGridEvents: ITreeGridEvents<any>;
  storageKey: string = StorageKeys.ACTIVE_SCANS;
  rootFoldersTitle: string = '';
  _params: any  = null;
  filtersList: FilterItem[] = null;
  totalRootFolderItems: number = 0 ;
  pageNumber: number = 1;
  allCanPlay: boolean = false;
  allCanPause: boolean = false;
  allCanMore: boolean = false;
  allCanStop: boolean = false;
  allCanMark: boolean = false;
  allCanStar: boolean = false;
  allCanUnstar: boolean = false;
  allCanUnmark: boolean = false;

  filterKeysList = [ScanPathParams.scheduleGroup,ScanPathParams.dataCenter,ScanPathParams.runStatus,ScanPathParams.runPhase,ScanPathParams.mediaType,ScanPathParams.accessibility,ScanPathParams.scanningOrder,ScanPathParams.onlyFavorite,ScanPathParams.searchText];


  FilterTypes = FilterTypes;
  ScanPathParams = ScanPathParams;
  lastSelectedIds = [];
  rootFolderSummaryObject:RootFoldersAggregatedSummaryInfo = {};
  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;
  treeParameters: any  = null;
  treeConfig= <TreeGridConfig>{
    enableSwitchSelectionMode: false,
    disableSelection:true
  }
  rootFolderDropDownItems: DropDownConfigDto[]  = null ;
  exportDropDownItems: DropDownConfigDto[]  = null;
  ActiveScanGridComponent = RootFolderItemGridComponent;
  ActiveScanGridHeaderComponent = RootFolderItemGridHeaderComponent;
  latestParamsStr: string = null;
  inLoadingSubTree: boolean = false;
  isTechSupport: boolean = false;
  scanStrategy: SCAN_STRATEGY  = null;
  searchTerm = '';
  cancelCallNotFinishNumber = 0 ;
  emptyDataError = 'There are no results for this filter';
  isWithFilter:boolean = false;
  finishLoadInitialData:boolean = false;

  TreeGridSelectionMode = TreeGridSelectionMode;


  @Output() pageChanged = new EventEmitter();
  @Output() parametersChanged = new EventEmitter();
  @Output() switchAutomaticRefresh  = new EventEmitter();


  @Input()
  breadCrumbs: breadcrumbItem[] = [];

  @Input()
  inAutomaticRefresh: boolean;

  @Input()
  set params(newParams:any) {
    this._params = newParams;
    this.loadData(true);
  }

  get params(): any {
    return this._params;
  }


  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);



  constructor (private scanFavoriteService : ScanFavoriteService,private numSuffixPipe: NumSuffixPipe, private platformLocation: PlatformLocation,  private scanService: ScanService, private dialogService: DialogService, private metadataService: MetadataService, private utilService: UtilService,private userSettingsService : UserSettingsService,private serverExportService: ServerExportService,
               private scanRouteManager: ScanRouteManager, private urlAuthorizationService: UrlAuthorizationService, private subHeaderService: SubHeaderService, private i18nService : I18nService, private loginService:LoginService, private appConfig:AppConfig) {
    super();
  }

  markSelected = ()=> {
    let rfIds = this.lastSelectedIds.map((dataItem) => {
      return dataItem.item.rootFolderDto.id;
    });
    this.scanService.markForFullScanMany(rfIds,true);
  };


  daOnInit() {
    setTimeout(()=>{
      this.loadInitialMetaData();
    },2000);
    this.on(this.scanService.rootFolderRefreshedEvent$, () => {
      this.loadData(true);
    });

    this.on(this.scanService.jobListRefreshedEvent$, () => {
      this.loadData(true);
    });

    this.on(this.scanFavoriteService.scanFavoriteChangedEvent$, ()=> {
      this.onCheckedItemsChanged(this.lastSelectedIds);
    });

    this.on(this.scanService.startScanRootFolderEvent$, (ans: string[]) => {
      this.loadData();
    });

    this.on(this.scanService.stopScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.resumeScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.pauseScanEvent$, () => {
      this.loadData();
    });

    this.on(this.scanService.activeScanFilterChangeEvent$, ([filterField,filterStr])=> {
      this.parametersChanged.emit(
        {
          keys: [filterField,ScanPathParams.rfPage],
          values: [filterStr, 1]
        });
    });

    this.on(this.scanService.markFullEvent$, ()=>{
      this.loadData();
    });

    this.on(this.scanService.rootFolderGetMoreDetailsEvent$, (moreDetailsObj:any ) => {
      this.clearQuerySubscription();
      this.showLoader(true);
      this.inLoadingSubTree = true;
      return this.scanService.loadSubActiveScansByRunId(moreDetailsObj.runId).subscribe((jobs)=>
      {
        this.showLoader(false);
        this.inLoadingSubTree = false;
        moreDetailsObj.finishFunc(jobs);
      });
    });

    this.exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_TO_EXCEL'),
      iconClass: "fa fa-download",
      actionFunc: this.exportRootFolderData
    }];
    this.subHeaderService.setSubHeader(this.i18nService.translateId('ROOTFOLDER.SUB_HEADER'));
    this.isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    this.treeGridEvents = {
      onSortByChanged: this.onSortByChanged,
      onMarkedItemsChanged: this.onCheckedItemsChanged,
      onPageChanged: this.onRfPageChanged
    };
    if (this.isTechSupport ) {
      this.scanService.getScanStrategyMode().subscribe((sStrategy:SCAN_STRATEGY) => {
        this.scanStrategy = sStrategy;
        this.refreshMainOptionMenu();
      });
    }
    else {
      this.refreshMainOptionMenu();
    }
    this.loadData();
  }


  private loadInitialMetaData(){
    let queriesArray = [];
    queriesArray.push(this.scanService.getDepartmentListRequest(), this.scanService.getScheduleGroupListRequest(), this.scanService.getDataCenterListRequest());
    forkJoin(queriesArray).subscribe(([depList, sgList, dcList]) => {
      depList.sort(function(a, b) {
        return a.displayText.localeCompare(b.displayText);
      });
      sgList.sort(function(a, b) {
        return a.displayText.localeCompare(b.displayText);
      });
      dcList.sort(function(a, b) {
        return a.displayText.localeCompare(b.displayText);
      });
      this.metadataService.addKeyValueMetadataList(depList, MetadataType.DEPARTMENT);
      this.metadataService.addKeyValueMetadataList(sgList, MetadataType.SCHEDULE_GROUP);
      this.metadataService.addKeyValueMetadataList(dcList, MetadataType.DATA_CANTER);
      if (this.filtersList) {
        this.filtersList[1].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), depList, ScanPathParams.department);
        this.filtersList[2].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), sgList, ScanPathParams.scheduleGroup);
        this.filtersList[3].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), dcList, ScanPathParams.dataCenter);
      }
      this.finishLoadInitialData = true;
    });
  }


  private onRfPageChanged = (pageNum) => {
    this.pageChanged.emit(pageNum);
  }


  exportRootFolderData = ()=> {
    this.scanFavoriteService.getAllFavoriteIds().then((favIds)=> {
      const params = _.clone(this.scanRouteManager.getLatestQueryParams());
      if (params && params[ScanPathParams.onlyFavorite] && params[ScanPathParams.onlyFavorite]=='true') {
        params['rootfolderIds'] = favIds.join(',');
      }
      this.serverExportService.prepareExportFile('Active_scans',ExportType.ACTIVE_SCANS,params,{
        itemsNumber : this.totalRootFolderItems
      },this.totalRootFolderItems > CommonConfig.minimum_items_to_open_export_popup);
    });
  }


    openInReportPage =  ()=> {
      let newURL:string = this.platformLocation['location'].origin +'/report/' + EntityDisplayTypes.folders+ '/' + EntityDisplayTypes.files +'?activePan='+ 'left';
      window.open(newURL, '_blank');
    };


  loadData = (withLoader:boolean = false)=>  {
      if (!withLoader && (this.inLoadingSubTree || this.isDropDownOpen())) {
        this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_CHECK_PERIOD);
        return;
      }
      if (this.params) {
      this.isWithFilter = false;
      this.filterKeysList.forEach((filterKey)=>{
        if (this.params[filterKey]) {
          this.isWithFilter = true;
        }
      })
      this.treeParameters = {filterParams: this.params};
      if (!this.params.sortField) {
        this.params.sortField = 'NICKNAME';
        this.params.sortDesc = 'ASC';
      }
      this.initialSortFieldName = this.params.sortField ? this.params.sortField : null;
      this.initialSortDirection = this.params.sortDesc ? this.params.sortDesc : null;
      this.searchTerm =  this.params.namingSearchTerm ? this.params.namingSearchTerm : '';
      if (withLoader) {
        this.showLoader(true);
        this.rootFoldersTitle = '';
      }
      this.updateFilters();
      let openNodesStr = '';
      if (this.currentData) {
        openNodesStr = this.currentData.items.filter(item => item.isOpen).map(item => item.id).join(',');
      }
      this.params.viewedRootFolders = openNodesStr;
      if (this.params[ScanPathParams.rfPage]) {
        this.params[ScanPathParams.page] = this.params[ScanPathParams.rfPage];
      }
      this.setBreadcrumbTitle(this.params);
      this.scanFavoriteService.getAllFavoriteIds().then((favIds)=>
      {
        if (this.params[ScanPathParams.onlyFavorite] && this.params[ScanPathParams.onlyFavorite]=='true') {
          this.params['rootfolderIds'] = favIds.join(',');
        }
        let queriesArray = [];
        queriesArray.push(this.scanService.getRootFoldersScanSummaryRequest(), this.scanService.getActiveScansTableRequest(this.params));
        this.addQuerySubscription(forkJoin(queriesArray).subscribe(([scanSummary, rootFolders]) => {
          this.cancelCallNotFinishNumber = 0 ;
          this.initFilters(this.isWithFilter ? rootFolders.stateSummaryInfo : null);
          this.setBreadcrumbTitle(this.params);
          if (this.metadataService.getKeyValueMetadataList(MetadataType.DEPARTMENT)) {
            this.filtersList[1].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), this.metadataService.getKeyValueMetadataList(MetadataType.DEPARTMENT), ScanPathParams.department);
            this.filtersList[2].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), this.metadataService.getKeyValueMetadataList(MetadataType.SCHEDULE_GROUP), ScanPathParams.scheduleGroup);
            this.filtersList[3].filterOptions = this.utilService.convertFilterOptionListIntoCheckArray(this.scanRouteManager.getLatestQueryParams(), this.metadataService.getKeyValueMetadataList(MetadataType.DATA_CANTER), ScanPathParams.dataCenter);
          }
          this.rootFolderSummaryObject = scanSummary;
          this.currentData = <TreeGridData<RootFolderRunSummaryInfo>>rootFolders;
          this.currentData.items.map((treeItem: TreeGridDataItem<any>) => {
            treeItem.id = this.getTreeIdFunction(treeItem);
            treeItem.isOpen = (treeItem.item.jobs && treeItem.item.jobs.length > 0);
          });
          if (this.currentData.items.length == 0 ) {
            this.emptyDataError = this.i18nService.translateId('SCAN.NOT_RESULT_MSG');
            if (this.params[ScanPathParams.runPhase] && this.params[ScanPathParams.runStatus] && this.params[ScanPathParams.runStatus].indexOf(ERunStatus.RUNNING) == -1 && this.params[ScanPathParams.runStatus].indexOf(ERunStatus.PAUSED) == -1 && this.params[ScanPathParams.runStatus].indexOf(ERunStatus.SUSPENDED) == -1 && this.params[ScanPathParams.runStatus].indexOf(ERunStatus.FAILED) == -1) {
              this.emptyDataError = this.i18nService.translateId('SCAN.INVALID_FILTER_MSG');
            }
          }
          this.totalRootFolderItems = rootFolders.totalElements;
          this.pageNumber = rootFolders.paging ? rootFolders.paging.currentPage : 1;
          this.showLoader(false);
          if (this.inAutomaticRefresh) {
            this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_PERIOD);
          }
        }));
      });
    };
  }



  onSetAutomaticRefresh(){
    this.switchAutomaticRefresh.emit();
  }

  private updateFilters() {
    if (this.filtersList && this.filtersList.length>0) {
      this.utilService.updateCheckArray(this.params,  ScanPathParams.onlyFavorite, this.filtersList[0].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.department, this.filtersList[1].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.scheduleGroup, this.filtersList[2].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.dataCenter, this.filtersList[3].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.runStatus, this.filtersList[4].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.runPhase, this.filtersList[5].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.mediaType, this.filtersList[6].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.accessibility, this.filtersList[7].filterOptions);
      this.utilService.updateCheckArray(this.params,  ScanPathParams.scanningOrder, this.filtersList[8].filterOptions);
      this.filtersList = _.cloneDeep(this.filtersList);
    }
  }

  private initFilters = (stateSummaryInfo = null)=> {
    let mediaTypeList = [
      {
        value: EMediaType.FILE_SHARE,
        displayText: 'FILE_SHARE',
      },
      {
        value: EMediaType.BOX,
        displayText: 'BOX',
      },
      {
        value: EMediaType.SHARE_POINT,
        displayText: 'SHARE_POINT',
      },
      {
        value: EMediaType.ONE_DRIVE,
        displayText: 'ONE_DRIVE',
      }
    ];
    if (CommonConfig['exchange-365.support-enabled'] === "true") {
      mediaTypeList.push({
        value: EMediaType.EXCHANGE_365,
        displayText: 'EXCHANGE',
      });
    }
    this.filtersList = [
      {
        type: FilterTypes.favoriteBox,
        filterOptions: [{
          value: true,
          selected: false,
          displayText : 'True'
        }],
        filterIcon: 'fa fa-star',
        filterTooltip: this.i18nService.translateId('SCAN.FILTER_STARRED_MSG'),
        filterField: ScanPathParams.onlyFavorite,
        filterTitle: this.i18nService.translateId('SCAN.FILTER_STARRED_FOLDER'),
        isCollapsed: false
      },
      {
        type: FilterTypes.selectBtn,
        filterOptions: null,
        filterTitle: this.appConfig.isInstallationModeLegal() ? this.i18nService.translateId('SCAN.MATTER') : this.i18nService.translateId('SCAN.DEPARTMENT'),
        filterField: ScanPathParams.department,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.department) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.department) : true
      },
      {
        type: FilterTypes.selectBtn,
        filterOptions: null,
        filterTitle: this.i18nService.translateId('SCAN.SCHEDULE_GROUP'),
        filterField: ScanPathParams.scheduleGroup,
        shortTitle: this.i18nService.translateId('SCAN.GROUP'),
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.scheduleGroup) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.scheduleGroup) : true
      },
      {
        type: FilterTypes.selectBtn,
        filterOptions: null,
        filterTitle:  this.i18nService.translateId('SCAN.DATA_CENTER'),
        filterField: ScanPathParams.dataCenter,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.dataCenter) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.dataCenter) : true
      },
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, [
            {
              value: null,
              displayText: this.i18nService.translateId('SCAN.ACTIVE'),
              children: [
                {
                  value: ERunStatus.RUNNING,
                  displayText: this.i18nService.translateId('SCAN.RUNNING') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.runningRootFoldersCount) + ')' : '')
                },
                {
                  value: ERunStatus.PAUSED,
                  displayText: this.i18nService.translateId('SCAN.PAUSED') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.pausedRootFoldersCount) + ')' : '')
                },
                {
                  value: ERunStatus.SUSPENDED,
                  displayText: this.i18nService.translateId('SCAN.SUSPENDED') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.suspendedRootFoldersCount) + ')' : '')
                }
              ]
            },
            {
              value: null,
              displayText: this.i18nService.translateId('SCAN.NOT_ACTIVE'),
              children: [
                {
                  value: ERunStatus.NEW,
                  displayText: this.i18nService.translateId('SCAN.NEW') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.newRootFoldersCount) + ')' : '')
                },
                {
                  value: ERunStatus.FINISHED_SUCCESSFULLY,
                  displayText: this.i18nService.translateId('SCAN.OK') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.scannedRootFoldersCount) + ')' : '')
                },
                {
                  value: ERunStatus.STOPPED,
                  displayText: this.i18nService.translateId('SCAN.STOPPED') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.stoppedRootFoldersCount) + ')' : '')
                },
                {
                  value: ERunStatus.FAILED,
                  displayText: this.i18nService.translateId('SCAN.FAILED') + (stateSummaryInfo ? ' (' + this.numSuffixPipe.transform(stateSummaryInfo.failedRootFoldersCount) + ')' : '')
                }]
            }
          ]
          , ScanPathParams.runStatus),
        filterTitle: this.i18nService.translateId('SCAN.STATUS'),
        filterField: ScanPathParams.runStatus,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.runStatus) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.runStatus) : true
      },
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, [
          {
            value: 'SCAN',
            displayText: 'SCAN.PHASE.SCAN'
          },
          {
            value: 'SCAN_FINALIZE',
            displayText: 'SCAN.PHASE.SCAN_FINALIZE'
          },
          {
            value: 'INGEST',
            displayText: 'SCAN.PHASE.INGEST'
          },
          {
            value: 'INGEST_FINALIZE',
            displayText: 'SCAN.PHASE.INGEST_FINALIZE'
          },
          {
            value: 'OPTIMIZE_INDEXES',
            displayText: 'SCAN.PHASE.OPTIMIZE_INDEXES'
          },
          {
            value: 'ANALYZE',
            displayText: 'SCAN.PHASE.ANALYZE'
          },
          {
            value: 'ANALYZE_FINALIZE',
            displayText: 'SCAN.PHASE.ANALYZE_FINALIZE'
          }], ScanPathParams.runPhase),
        filterTitle: this.i18nService.translateId('SCAN.ACTIVE_PHASE'),
        filterField: ScanPathParams.runPhase,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.runPhase) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.runPhase) : true
      },
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, mediaTypeList, ScanPathParams.mediaType),
        filterTitle: 'Media type',
        filterField: ScanPathParams.mediaType,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.mediaType) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.mediaType) : true
      },
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, [
          {
            value: '0',
            displayText: 'ACCESSIBLE',
          },
          {
            value: '1',
            displayText: 'NOT_ACCESSIBLE',
          },
          {
            value: '2',
            displayText: 'UNKNOWN',
          }
        ], ScanPathParams.accessibility),
        filterTitle: this.i18nService.translateId('SCAN.ACCESSIBILITY'),
        filterField: ScanPathParams.accessibility,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.accessibility) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.accessibility) : true
      },
      {
        type: FilterTypes.checkbox,
        filterOptions: this.utilService.convertFilterOptionListIntoCheckArray(this.params, [
          {
            value: 'FIRST_SCAN',
            displayText: this.i18nService.translateId('SCAN.FIRST_SCAN'),
          },
          {
            value: 'RE_SCAN',
            displayText: this.i18nService.translateId('SCAN.RESCAN'),
          }
        ], ScanPathParams.scanningOrder),
        filterTitle: this.i18nService.translateId('SCAN.FIRST_SCAN'),
        filterField: ScanPathParams.scanningOrder,
        isCollapsed: this.userSettingsService.hasLocaleStorage(this.storageKey + ScanPathParams.scanningOrder) ? this.userSettingsService.getFromLocaleStorage(this.storageKey + ScanPathParams.scanningOrder) : true
      }
    ];
    if (this.filtersList) {
      this.updateFilters();
    }
  };

  /*
  updateStatusAndPhaseFilter(){
    let queryParams = this.scanRouteManager.getLatestQueryParams();
    let runStatusValues = [];
    let runPhaseValues = [];
    if (queryParams && queryParams[ScanPathParams.runStatus]) {
      runStatusValues = queryParams[ScanPathParams.runStatus].split(',');
    }
    if (queryParams && queryParams[ScanPathParams.runPhase]) {
      runPhaseValues = queryParams[ScanPathParams.runPhase].split(',');
    }
    if (runStatusValues.indexOf(ERunStatus.FAILED)> -1 || runStatusValues.indexOf(ERunStatus.FINISHED_SUCCESSFULLY)> -1  || runStatusValues.indexOf(ERunStatus.NEW)> -1  || runStatusValues.indexOf(ERunStatus.STOPPED)> -1) {
      this.filtersList[3].isDisabled = true;
      this.filtersList[2].filterOptions[1].disabled = false;
    }
    else if (runPhaseValues.length > 0 ){
      this.filtersList[2].filterOptions[1].disabled = true;
      this.filtersList[3].isDisabled = false;
    }
    else {
      this.filtersList[3].isDisabled = false;
      this.filtersList[2].filterOptions[1].disabled = false;
    }
  }
  */

  setBreadcrumbTitle = (params)=> {
    this.rootFoldersTitle = this.i18nService.translateId('ROOTFOLDER.ALL_TITLE');
    if (params[ScanPathParams.scheduleGroup]) {
      let selectedGroups = params[ScanPathParams.scheduleGroup].split(',');
      if (selectedGroups.length == 1) {
        let sGroupItem = this.metadataService.getMetadataKeyValueItem(MetadataType.SCHEDULE_GROUP, selectedGroups[0]);
        this.rootFoldersTitle =  sGroupItem ? sGroupItem.displayText : '';
      }
      else if (selectedGroups.length > 1) {
        this.rootFoldersTitle = this.i18nService.translateId('ROOTFOLDER.BY_GROUP_TITLE');
      }
    }
  }


  onStatusFilterChanged = (newStatusFilterField)=>  {

    let keysArr= ['namingSearchTerm'];
    let valuesArr = [''];
    this.filtersList.forEach((fItem)=> {
      keysArr.push(fItem.filterField);
      if (fItem.filterField == ScanPathParams.runStatus) {
        valuesArr.push(newStatusFilterField);
      }
      else {
        valuesArr.push('');
      }
    });
    keysArr.push(ScanPathParams.rfPage);
    valuesArr.push('1');
    this.parametersChanged.emit(
      {
        keys: keysArr,
        values: valuesArr
      });
  };

  onFilterChanged(filterOutput: filterChangedOutput) {
    if (filterOutput) {
      let fieldsArr = [];
      let valuesArr = [];

      for (let count = 0 ; count < filterOutput.fields.length ; count++) {

        /*
        if (filterOutput.fields[count] == ScanPathParams.runPhase) {
          let phaseFilterStr = this.utilService.getFilterString(filterOutput.filterOptions[count]);
          if (phaseFilterStr.length > 0) {
            let queryParams = this.scanRouteManager.getLatestQueryParams();
            if (queryParams[ScanPathParams.runStatus] == null || queryParams[ScanPathParams.runStatus] == '') {
              fieldsArr.push(filterOutput.fields[count]);
              fieldsArr.push(ScanPathParams.runStatus);
              valuesArr.push(phaseFilterStr);
              valuesArr.push(ERunStatus.PENDING + ',' + ERunStatus.PAUSED + ',' + ERunStatus.RUNNING + ',' + ERunStatus.SUSPENDED);
            }
            else {
              fieldsArr.push(filterOutput.fields[count]);
              valuesArr.push(phaseFilterStr);
            }
          }
          else {
            fieldsArr.push(filterOutput.fields[count]);
            valuesArr.push(phaseFilterStr);
          }
        }
        */
        if (filterOutput.fields[count] == ScanPathParams.scheduleGroup){
          let phaseFilterStr = this.utilService.getFilterString(filterOutput.filterOptions[count]);
          let groupsArray = phaseFilterStr.split(',');
          fieldsArr.push(filterOutput.fields[count]);
          valuesArr.push(phaseFilterStr);
          fieldsArr.push(ScanPathParams.scheduleGroupName);
          if (phaseFilterStr != '' && groupsArray.length == 1) {
            valuesArr.push(this.metadataService.getMetadataKeyValueItem(MetadataType.SCHEDULE_GROUP, groupsArray[0]).displayText);
          }
          else {
            valuesArr.push('');
          }

        }
        else {
          fieldsArr.push(filterOutput.fields[count]);
          valuesArr.push(this.utilService.getFilterString(filterOutput.filterOptions[count]));
        }
      }
      fieldsArr.push(ScanPathParams.rfPage);
      valuesArr.push('1');
      this.parametersChanged.emit(
        {
          keys: fieldsArr,
          values: valuesArr
        });
    }
  }



  onSearchChange(searchText) {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.searchText,ScanPathParams.rfPage],
        values: [searchText,1]
      });
  }


  getTreeIdFunction(treeItem:any) {
    return treeItem.item.rootFolderDto.id;
  }

  private onSortByChanged = (sortObj: any) => {
    this.parametersChanged.emit(
      {
        keys: ['sortField','sortDesc', ScanQueryParams.PAGE_NUM],
        values: [sortObj.sortBy, sortObj.sortOrder,1]
      });
  };

  onCheckedItemsChanged = (items: any []) => {
    this.lastSelectedIds = items;
    setTimeout(()=> {
      let havePaused:boolean = false;
      let haveStopped:boolean = false;
      let haveRunning:boolean = false;
      let havedNew:boolean = false;
      this.allCanPlay = false;
      this.allCanMore = false;
      this.allCanPause = false;
      this.allCanStop = false;
      this.allCanUnmark = false;
      this.allCanMark = false;
      this.allCanStar = false;
      this.allCanUnstar = false;


      if (items.length > 0) {
        items.forEach((dataItem: TreeGridDataItem<any>) => {
          if (this.scanFavoriteService.isFavoriteRootFolderAsync(dataItem.item.rootFolderDto.id)) {
            this.allCanUnstar = true;
          }
          else {
            this.allCanStar = true;
          }
          if (dataItem.item.rootFolderDto.reingest) {
            this.allCanUnmark = true;
          }
          else {
            this.allCanMark = true;
          }
          if (!dataItem.item.crawlRunDetailsDto) {
            havedNew = true;
          }
          else if (dataItem.item.crawlRunDetailsDto.runOutcomeState == EJobState.RUNNING) {
            haveRunning = true;
          }
          else if (dataItem.item.crawlRunDetailsDto.runOutcomeState == EJobState.PAUSED) {
            havePaused = true;
          }
          else {
            haveStopped = true;
          }
        });
        if (havedNew || havePaused || haveStopped){
          this.allCanPlay = true;
        }
        if (haveRunning){
          this.allCanPause = true;
        }
        if (havePaused ){
          this.allCanStop = true;
        }
        if (haveStopped || havedNew ){
          this.allCanMore = true;
        }
      }
      this.refreshMainOptionMenu();
    });
  };

  unMarkSelected = ()=> {
    let rfIds = this.lastSelectedIds.map((dataItem) => {
      return dataItem.item.rootFolderDto.id;
    });
    this.scanService.markForFullScanMany(rfIds,false);
  };

  private refreshMainOptionMenu() {
    let actionArray = [];
    actionArray.push({
      title: this.i18nService.translateId('SCAN_OPTION.RUN_SCAN'),
      iconClass: "fa fa-play",
      actionFunc: this.runScanFolder,
      disabled: !this.allCanPlay
    });
    actionArray.push({
      title: this.i18nService.translateId('SCAN_OPTION.PAUSE_SCAN'),
      iconClass: "fa fa-pause",
      actionFunc: this.pauseScan,
      disabled: !this.allCanPause
    });
    actionArray.push({
      title: this.i18nService.translateId('SCAN_OPTION.STOP_SCAN'),
      iconClass: "fa fa-stop",
      actionFunc: this.stopScan,
      disabled : !this.allCanStop
     });
    actionArray.push({
      title: this.i18nService.translateId('SCAN_OPTION.ADVANCED_SCAN'),
      iconClass: "fa fa-graduation-cap",
      actionFunc: this.moreOptions,
      disabled : !this.allCanMore
    });

    let isThereItemChecked:boolean = this.lastSelectedIds.length>0;
    this.rootFolderDropDownItems = [
      {
        title: this.i18nService.translateId('SCAN_DD.ACTIONS'),
        children:actionArray
      },
      {
        title: this.i18nService.translateId('SCAN_DD.NAVIGATION'),
        disabled: isThereItemChecked,
        children: [
          {
            title: this.i18nService.translateId('SCAN_DD.RF_SETTINGS'),
            iconClass: "fa fa-pencil",
            href: AppUrls.ROOT_FOLDERS_SETTINGS,
            disabled: isThereItemChecked
          },
          {
            title: this.i18nService.translateId('SCAN_DD.VIEW_SG'),
            iconClass: "ion-clock",
            href: AppUrls.SCHEDULE_GROUPS_MONITORING,
            disabled: isThereItemChecked
          },
          {
            title: this.i18nService.translateId('SCAN_DD.GOTO_DISCOVER'),
            iconClass: "ion-android-exit",
            actionFunc: this.openInReportPage,
            disabled: isThereItemChecked
          },
          {
            title: this.i18nService.translateId('SCAN_DD.VIEW_INGEST_ERRORS'),
            iconClass: "fa fa-exclamation-circle",
            href: AppUrls.INGEST_ERRORS,
            disabled: isThereItemChecked
          },
          {
            title: this.i18nService.translateId('SCAN_DD.VIEW_SCAN_HISTORY'),
            iconClass: "fa fa-history",
            href: AppUrls.SCAN_HISTORY,
            disabled: isThereItemChecked
          }
        ]
      },
      {
        title: this.i18nService.translateId('SCAN_DD.MORE'),
        disabled: !this.allCanUnstar && !this.allCanStar,
        children: [
           {
            title: "Mark for full scan" ,
            iconClass: "fa fa-thumb-tack",
            actionFunc: this.markSelected,
            disabled: !this.allCanMark
          },
          {
            title: "Unmark for full scan" ,
            iconClass: "fa fa-thumb-tack",
            actionFunc: this.unMarkSelected,
            disabled: !this.allCanUnmark
          },
          {
            title: this.i18nService.translateId('SCAN_DD.ADD_STAR'),
            iconClass: "fa fa-star",
            actionFunc: this.addStarToSelected,
            disabled: !this.allCanStar
          },
          {
            title: this.i18nService.translateId('SCAN_DD.REMOVE_STAR'),
            iconClass: "fa fa-star-o",
            actionFunc: this.removeStarFromSelected,
            disabled: !this.allCanUnstar
          }]
      },
      {
        title: this.i18nService.translateId('SCAN_DD.COPY_PATH'),
        iconClass: "fa fa-files-o",
        actionFunc: null,
        disabled: true
      }];


    if (this.isTechSupport) {
      this.rootFolderDropDownItems.push({});
      this.rootFolderDropDownItems.push({
        title: this.i18nService.translateId('SCAN_DD.STRATEGY'),
        children: [
          {
            title: this.i18nService.translateId('SCAN_STRATEGY.NORMAL'),
            iconClass: this.scanStrategy == SCAN_STRATEGY.FAIR ? 'fa fa-check' : '',
            actionFunc: () => {
              this.updateScanStrategy('FAIR');
            }
          },
          {
            title: this.i18nService.translateId('SCAN_STRATEGY.AUTO'),
            iconClass: this.scanStrategy == SCAN_STRATEGY.AUTO_EXCLUSIVE ? 'fa fa-check' : '',
            actionFunc: () => {
              this.updateScanStrategy('AUTO_EXCLUSIVE');
            }
          },
          {
            title: this.i18nService.translateId('SCAN_STRATEGY.INGEST_ONLY'),
            iconClass: this.scanStrategy == SCAN_STRATEGY.INGEST_ONLY ? 'fa fa-check' : '',
            actionFunc: () => {
              this.updateScanStrategy('INGEST_ONLY');
            }
          },
          {
            title: this.i18nService.translateId('SCAN_STRATEGY.ANALYZE_ONLY'),
            iconClass: this.scanStrategy == SCAN_STRATEGY.ANALYZE_ONLY ? 'fa fa-check' : '',
            actionFunc: () => {
              this.updateScanStrategy('ANALYZE_ONLY');
            }
          }
        ]
      });
    }
  }


  addStarToSelected = ()=> {
    let rfIds = this.lastSelectedIds.map((dataItem) => {
      return dataItem.item.rootFolderDto.id;
    });
    this.scanFavoriteService.addFavoriteRootFolderArray(rfIds);
    this.loadData(true);
  };

  removeStarFromSelected = ()=> {
    let rfIds = this.lastSelectedIds.map((dataItem) => {
      return dataItem.item.rootFolderDto.id;
    });
    this.scanFavoriteService.removeFavoriteRootFolderArray(rfIds);
    this.loadData(true);
  };

  pauseScan = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'),this.i18nService.translateId('ROOTFOLDER.PAUSE_CONFIRMATION',{
      folders_num : this.lastSelectedIds.length,
      num_of_days : CommonConfig['handle-stuck-in-pause-runs.stop-threshold-days']
    })).then(result => {
      if (result) {
        this.scanService.pauseScan(this.lastSelectedIds.map(dataItem => dataItem.item.rootFolderDto.lastRunId));
      }
    });
  };

  stopScan = ()=> {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId('ROOTFOLDER.STOP_CONFIRMATION', {folders_num: this.lastSelectedIds.length })).then(result => {
      if (result) {
        this.scanService.stopScan(this.lastSelectedIds.map(dataItem => dataItem.item.rootFolderDto.lastRunId));
      }
    });
  };

  runScanFolder = ()=>  {
      var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId('ROOTFOLDER.RESUME_CONFIRMATION', {folders_num : this.lastSelectedIds.length })).then(result => {
        if (result) {
          let startList = [];
          let resumeList = [];
          this.lastSelectedIds.forEach((dataItem)=> {
            if (!dataItem.item.crawlRunDetailsDto) {
              startList.push(dataItem.item.rootFolderDto.id)
            }
            else if (dataItem.item.crawlRunDetailsDto.runOutcomeState == EJobState.PAUSED) {
              resumeList.push(dataItem.item.rootFolderDto.lastRunId)
            }
            else {
              startList.push(dataItem.item.rootFolderDto.id)
            }
          });
          if (resumeList.length > 0 ) {
            this.scanService.resumeScan(resumeList);
          }
          if (startList.length > 0 ) {
            this.scanService.startScanRootFolder(startList.join(','),SCAN_OPERATION.FULL);
          }
        }
      });
  };

  extractFolder = ()=>  {

  };

  moreOptions = ()=>  {
    let allNewItems = this.lastSelectedIds.filter(dataItem => !dataItem.item.crawlRunDetailsDto);
    this.dialogService.openModalPopup(RunScanDialogComponent,{data : {selectedName : this.lastSelectedIds.length + ' selected root folders', onlyMapMode : allNewItems.length ==  this.lastSelectedIds.length}}).result.then((result : SCAN_OPERATION) =>
    {
      if (result) {
        this.scanService.partialRun(this.lastSelectedIds.map(dataItem => dataItem.item.rootFolderDto.id).join(","), result);
      }
    }, (reason) => {
    });
  };

  changeToAllRootFolderMode() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.activeScanMode, ScanPathParams.scheduleGroup],
        values: [ActiveScanMode.ROOT_FOLDERS,'']
      });
  }

  updateScanStrategy = (newStrategy:string)=>  {
      this.scanService.changeScanStrategyMode(newStrategy).subscribe((sStrategy:SCAN_STRATEGY)=>{
        this.scanStrategy = sStrategy;
        this.refreshMainOptionMenu();
      }, (err)=>{
        this.dialogService.notifyError(err);
      });

  };

  onCollapseChanged(fItem) {
    this.userSettingsService.addToLocaleStorage(this.storageKey + fItem.filterField,fItem.isCollapsed);
  }
}

import * as _ from 'lodash';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PlatformLocation, DecimalPipe} from '@angular/common';
import {TreeGridInnerItemBaseComponent} from '../../../../../shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {
  EJobState,
  ERunStatus,
  EPauseReason,
  SCAN_OPERATION
} from "../../../../../common/scan/types/scanEnum.types";
import {DialogService} from "../../../../../shared-ui/components/modal-dialogs/dialog.service";
import {EditRootFolderScheduleDialog} from "../../../popups/editRootFolderScheduleDialog/edit-root-folder-schedule-dialog";
import {ScanService} from "../../../services/scan.service";
import {RootFolderSummaryInfo, ScheduleGroupDto} from "../../../../../common/scan/types/scan.dto";
import {RunScanDialogComponent} from "../../../popups/runScanDialog/runScanDialog.component";
import {I18nService} from "../../../../../common/translation/services/i18n-service";
import {Router} from '@angular/router';
import {EntityDisplayTypes} from "../../../../../core/types/discover.entity.types";
import { ClipboardService } from 'ngx-clipboard'
import {ScanPathParams} from "../../../types/scan-route-pathes";
import {MetadataService} from "../../../../../core/services/metadata.service";
import {MetadataType} from "../../../../../common/discover/types/common-enums";
import {RoutingService} from "../../../../../core/services/routing.service";
import {ActivatedRoute} from '@angular/router';
import {DirectoryExistStatus, EFileType, EJobType} from "@app/common/scan/types/scanEnum.types";
import {
  RunSummaryInfoView,
} from "@app/common/scan/types/scan.dto";
import {UtilService} from "@services/util.service";
import {ScanFavoriteService} from "@app/features/scan/services/scan-favorite.service";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import {CommonConfig} from "@app/common/configuration/common-config";
import {AppUrls} from "@core/types/app-urls";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";




@Component({
  selector: 'root-folder-item-grid',
  templateUrl: './root-folder-item-grid.component.html'
})
export class RootFolderItemGridComponent extends TreeGridInnerItemBaseComponent<RootFolderSummaryInfo> implements OnInit {


  @ViewChild('el') el: ElementRef;
  @ViewChild('runs') runs: ElementRef;

  SCAN_OPERATION = SCAN_OPERATION;
  EJobType = EJobType;
  EJobState = EJobState;
  folderNameDropDownItems:DropDownConfigDto[];
  additionalDropDownItems: DropDownConfigDto[];
  fileTypesMap = {};

  dcIsInFilter:boolean = false;
  sgIsInFilter: boolean = false;
  dcName:string = '';
  sgName:string  = '';
  runStatus:ERunStatus;
  isNotAccessible:boolean;
  isAccessibleUnknown:boolean;
  fileNumStr: string = null;
  lastRunElapsedTime: number = null;
  runSummaryObject:RunSummaryInfoView = null;
  phasePerecentageDone: string = null;
  pauseReason: EPauseReason ;
  ERunStatus = ERunStatus;
  EFileType = EFileType;
  isFavorite:boolean = false;

  CommonConfig = CommonConfig;


  constructor(private urlAuthorizationService: UrlAuthorizationService, private numSuffixPipe:NumSuffixPipe,private scanFavoriteService: ScanFavoriteService ,private routingService: RoutingService,private route: ActivatedRoute, private dialogService : DialogService, private scanService : ScanService, private i18nService: I18nService, private router: Router,private platformLocation:PlatformLocation,private clipboardService: ClipboardService, private metadataService : MetadataService, private utilService : UtilService, private decimalPipe:DecimalPipe) {
    super();
  }

 get isRunning():boolean{
    return  !!(this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING);
 }
 get isOk():boolean{
    return  !!(this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.FINISHED_SUCCESSFULLY);
 }
 get isPaused():boolean{
    return this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.PAUSED;
 }
  get isFailed():boolean{
    return  !!(this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.FAILED);
  }
  get isNew():boolean{
    return !this.dataItem.item.crawlRunDetailsDto;
}

  get isPending(){
   return this.dataItem.item.runningPhase && this.dataItem.item.runningPhase.jobState == EJobState.PENDING;
  }

  resumeScan = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.RESUME",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath )})).then(result => {
      if (result) {
        this.scanService.resumeScan([this.dataItem.item.rootFolderDto.lastRunId]);
      }
    });
  }


  stopScan = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.STOP",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath )})).then(result => {
      if (result) {
        this.scanService.stopScan([this.dataItem.item.rootFolderDto.lastRunId]);
      }
    });
  }


  forceStopScan = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.STOP",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath )})).then(result => {
      if (result) {
        this.scanService.forceStopScan([this.dataItem.item.rootFolderDto.lastRunId]);
      }
    });
  }


  pauseScan = ()=> {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.PAUSE",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath ), num_of_days : CommonConfig['handle-stuck-in-pause-runs.stop-threshold-days']})).then(result => {
      if (result) {
        this.scanService.pauseScan([this.dataItem.item.rootFolderDto.lastRunId]);
      }
    });
  }

  markFullScan = ()=> {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId('ROOTFOLDER.CONFIRM.MARK_FULL_SCAN')).then(result => {
      if (result) {
        this.scanService.markForFullScan(this.dataItem.item.rootFolderDto.id,true);
      }
    });
  }

  unMarkFullScan = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId('ROOTFOLDER.CONFIRM.UN_MARK_FULL_SCAN')).then(result => {
      if (result) {
        this.scanService.markForFullScan(this.dataItem.item.rootFolderDto.id,false);
      }
    });
  }

  moreScanOptions = ()=>{
    this.dialogService.openModalPopup(RunScanDialogComponent,{data : {onlyMapMode : this.dataItem.item.crawlRunDetailsDto == null || this.dataItem.item.crawlRunDetailsDto.totalProcessedFiles == 0 , selectedName : 'root folder: '+(this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath)}}).result.then((result : SCAN_OPERATION) =>
    {
      if (result) {
        this.scanService.partialRun(this.dataItem.item.rootFolderDto.id, result);
      }
    }, (reason) => {
    });
  }


  runScanFolder = ()=>  {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.START",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath )})).then(result => {
      if (result) {
        this.scanService.startScanRootFolder(this.dataItem.item.rootFolderDto.id,SCAN_OPERATION.FULL);
      }
    });
  }


  extractFolder(op : SCAN_OPERATION) {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("ROOTFOLDER.CONFIRM.EXTRACT",{root_folder_name : (this.dataItem.item.rootFolderDto.nickName ? this.dataItem.item.rootFolderDto.nickName : this.dataItem.item.rootFolderDto.realPath )})).then(result => {
      if (result) {
          this.scanService.startScanRootFolder(this.dataItem.item.rootFolderDto.id,op);
      }
    });
  }


  editGroup() {
    this.dialogService.openModalPopup(EditRootFolderScheduleDialog,{ data :
        {sGroupItem : this.dataItem.item  }
    }).result.then((result : ScheduleGroupDto) => {
      this.scanService.editScheduleGroup(result);
    }, (reason) => {
    });
  }


  copyFullPathToClipboard = ()=> {
    this.clipboardService.copyFromContent(this.dataItem.item.rootFolderDto.realPath)
  }

  /*
  viewScanHistory = ()=>{
    this.router.navigate(['/scan/monitor/history/scanHistory'],{
      queryParams : {
        selectedRunDetailId : this.dataItem.item.rootFolderDto.lastRunId,
        scanHistorySearchText : this.dataItem.item.rootFolderDto.realPath
      }
    });
  }*/


  viewDataCenter = ()=> {
    this.router.navigate(['/settings/customerDataCenters'],{
      queryParams : {
        selectedItem : this.dataItem.item.rootFolderDto.customerDataCenterDto.id
      }
    });
  }

  viewSGroup = ()=> {
    this.router.navigate(['/scanni/ScheduleGroup'],{
      queryParams : {
        selectedItem : this.dataItem.item.crawlRunDetailsDto.scheduleGroupId
      }
    });
  }


  changeDcFilter() {
    if (this.dcIsInFilter) {
      this.removeFilter(ScanPathParams.dataCenter, this.dataItem.item.rootFolderDto.customerDataCenterDto.id);
    }
    else {
      this.addFilter(ScanPathParams.dataCenter, this.dataItem.item.rootFolderDto.customerDataCenterDto.id);
    }
  }

  changeSgFilter() {
    if (this.sgIsInFilter) {
      this.removeFilter(ScanPathParams.scheduleGroup, this.dataItem.item.scheduleGroupDtos[0].id);
    }
    else {
      this.addFilter(ScanPathParams.scheduleGroup, this.dataItem.item.scheduleGroupDtos[0].id);
    }
  }

  addFilter = (filterField, filterId)=>{
    let filterStr:any   = this.hostComponentParams.filterParams[filterField] || '';
    if (filterStr.length > 0 ) {
      filterStr = filterStr + ','+ filterId;
    }
    else {
      filterStr = filterStr + filterId;
    }
    this.scanService.activeScanFilterChange(filterField,filterStr);
  }

  removeFilter = (filterField, filterId)=>{
    let filterStr:string = this.hostComponentParams.filterParams[filterField];
    let filterArray = filterStr.split(',');
    if (filterArray.indexOf(''+filterId) > -1) {
      filterArray.splice(filterArray.indexOf(''+filterId) ,1);
    }
    filterStr = filterArray.join(',');
    this.scanService.activeScanFilterChange(filterField,filterStr);
  }

  isInFilter = (filterType, filterId)=>{
    let filterStr:any   = this.hostComponentParams.filterParams[filterType];
    if (!filterStr || filterStr=='') {
      return false
    }
    let ans = (filterStr.split(',').indexOf(filterId+'')>-1);
    return ans;
  }

  /*
  getFilterDropDownItems(filterType, filterId) : any[]{
    let isInFilter = this.isInFilter(filterType,filterId);
    let ansDropDownItems = [{
      title: isInFilter ? "Remove from filter" : "Add to filter",
      iconClass : "fa fa-filter",
      chooseFunc: isInFilter  ? ()=> {
          this.removeFilter(filterType,filterId);
        }
        : ()=> {
          this.addFilter(filterType,filterId);
        }
    }];
    return ansDropDownItems;
  }
  */

  parametersChanged() {
    this.dcIsInFilter = this.isInFilter(ScanPathParams.dataCenter, this.dataItem.item.rootFolderDto.customerDataCenterDto.id);
    this.sgIsInFilter = this.isInFilter(ScanPathParams.scheduleGroup, this.dataItem.item.scheduleGroupDtos[0].id);
  }

  buildTooltip() {

    let isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    let actionArray: any[] = [];
    if (this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.PAUSED) {
      actionArray.push({
        title: this.i18nService.translateId("ROOTFOLDER.ITEM.START_SCAN"),
        iconClass: "fa fa-play",
        actionFunc: this.resumeScan,
        disabled: false
      })
    }
    else {
      actionArray.push({
        title: this.i18nService.translateId("ROOTFOLDER.ITEM.START_SCAN"),
        iconClass: "fa fa-play",
        actionFunc: this.runScanFolder,
        disabled: this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING
      })
    }

    actionArray.push({
      title: this.i18nService.translateId("ROOTFOLDER.ITEM.PAUSE"),
      iconClass: "fa fa-pause",
      actionFunc: this.pauseScan,
      disabled: !this.dataItem.item.crawlRunDetailsDto || this.dataItem.item.crawlRunDetailsDto.runOutcomeState != ERunStatus.RUNNING
    });

    actionArray.push({
      title: this.i18nService.translateId("ROOTFOLDER.ITEM.STOP"),
      iconClass: "fa fa-stop",
      actionFunc: this.stopScan,
      disabled: !this.dataItem.item.crawlRunDetailsDto || this.dataItem.item.crawlRunDetailsDto.runOutcomeState != ERunStatus.PAUSED
    });

    actionArray.push({
      title: this.i18nService.translateId("ROOTFOLDER.ITEM.ADVANCED"),
      iconClass: "fa fa-graduation-cap",
      actionFunc: this.moreScanOptions,
      disabled: (this.dataItem.item.crawlRunDetailsDto && (this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING || this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.PAUSED))
    });


    if (isTechSupport) {
      actionArray.push({
        title: this.i18nService.translateId("ROOTFOLDER.ITEM.FORCE_STOP"),
        iconClass: "fa fa-stop",
        actionFunc: this.forceStopScan,
        disabled: !this.dataItem.item.crawlRunDetailsDto || this.dataItem.item.crawlRunDetailsDto.runOutcomeState != ERunStatus.PAUSED
      });
    }


    this.additionalDropDownItems = [
      {
        title: this.i18nService.translateId("SCAN_DD.ACTIONS"),
        children: actionArray
      },
      {
        title: this.i18nService.translateId("SCAN_DD.NAVIGATION"),
        children: [
          {
            title: this.i18nService.translateId("SCAN_DD.RF_SETTINGS"),
            iconClass: "fa fa-pencil",
            href: AppUrls.ROOT_FOLDERS_SETTINGS,
            hrefParams: {
              selectedRootFolderId: this.dataItem.item.rootFolderDto.id,
              findId: this.dataItem.item.rootFolderDto.id
            }
          },
          {},
          {
            title: this.i18nService.translateId("SCAN_DD.VIEW_SG"),
            iconClass: "ion-clock",
            href: AppUrls.SCHEDULE_GROUPS_MONITORING,
            hrefParams: {
              selectedItem: this.dataItem.item.crawlRunDetailsDto ? this.dataItem.item.crawlRunDetailsDto.scheduleGroupId : null,
              [ScanPathParams.scheduleGroupIds]: this.dataItem.item.scheduleGroupDtos[0].id,
              [ScanPathParams.scheduleGroupName]: this.dataItem.item.scheduleGroupDtos[0].name
            }
          },
          {
            title: this.i18nService.translateId("SCAN_DD.GOTO_DISCOVER"),
            iconClass: "ion-android-exit",
            href: '/report/' + EntityDisplayTypes.folders+ '/' + EntityDisplayTypes.files,
            hrefParams: {
              activePan : 'left' ,
              includeSubEntityData : 'true',
              selectedItemLeft : encodeURIComponent(this.dataItem.item.rootFolderDto.realPath)
            },
            disabled: !this.dataItem.item.crawlRunDetailsDto
          },
          {
            title: this.i18nService.translateId("SCAN_DD.VIEW_INGEST_ERRORS"),
            iconClass: "fa fa-exclamation-circle",
            href: AppUrls.INGEST_ERRORS,
            hrefParams: {
              rootFolderId: this.dataItem.item.rootFolderDto.id,
              rootFolderName: this.dataItem.item.rootFolderDto.realPath
            },
            disabled: !this.dataItem.item.crawlRunDetailsDto
          },
          {
            title: this.i18nService.translateId("SCAN_DD.VIEW_MAP_ERRORS"),
            iconClass: "fa fa-exclamation-circle",
            href: AppUrls.SCAN_ERRORS,
            hrefParams: {
              rootFolderId: this.dataItem.item.rootFolderDto.id,
              rootFolderName: this.dataItem.item.rootFolderDto.realPath
            },
            disabled: !this.dataItem.item.crawlRunDetailsDto
          },
          {
            title: this.i18nService.translateId("SCAN_DD.VIEW_SCAN_HISTORY"),
            iconClass: "fa fa-history",
            href: AppUrls.SCAN_HISTORY,
            hrefParams: {
              rootFolderId: this.dataItem.item.rootFolderDto.id,
              rootFolderName: this.dataItem.item.rootFolderDto.nickName
            },
            disabled: !this.dataItem.item.crawlRunDetailsDto
          }
        ]
      },
      {
        title: this.i18nService.translateId("SCAN_DD.MORE"),
        disabled: !this.dataItem.item.crawlRunDetailsDto,
        children: [
          {
            title: !this.dataItem.item.rootFolderDto.reingest ? this.i18nService.translateId("SCAN_DD.MARK_FULL") : this.i18nService.translateId("SCAN_DD.UN_MARK_FULL"),
            iconClass: "fa fa-thumb-tack",
            actionFunc: !this.dataItem.item.rootFolderDto.reingest ? this.markFullScan : this.unMarkFullScan,
            disabled: !this.dataItem.item.crawlRunDetailsDto
          }]
      },
      {
        title: this.i18nService.translateId("SCAN_DD.COPY_PATH"),
        iconClass: "fa fa-files-o",
        actionFunc: this.copyFullPathToClipboard
      }]

  }


  clickOnChecknox = ()=> {
    this.dataItem.itemChecked = !this.dataItem.itemChecked;
  }

  ngOnInit(): void {
    this.scanFavoriteService.isFavoriteRootFolder(this.dataItem.item.rootFolderDto.id).then((isFavoriteRes) => {
      this.isFavorite = isFavoriteRes;
    });
    this.parseData();
    this.buildTooltip();
    this.dcName = this.metadataService.getMetadataKeyValueItem(MetadataType.DATA_CANTER, this.dataItem.item.rootFolderDto.customerDataCenterDto.id).displayText;
    this.sgName = this.metadataService.getMetadataKeyValueItem(MetadataType.SCHEDULE_GROUP, this.dataItem.item.scheduleGroupDtos[0].id).displayText;
    this.isNotAccessible = (this.dataItem.item.rootFolderDto.isAccessible === DirectoryExistStatus.NO);
    this.isAccessibleUnknown = (this.dataItem.item.rootFolderDto.isAccessible === DirectoryExistStatus.UNKNOWN);
    this.fileNumStr = null;
    if (this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.totalProcessedFiles) {
      this.fileNumStr = this.numSuffixPipe.transform(this.dataItem.item.crawlRunDetailsDto.totalProcessedFiles) + (this.dataItem.item.crawlRunDetailsDto.totalProcessedFiles == 1 ? ' file in ' : ' files in ') + this.numSuffixPipe.transform(this.dataItem.item.rootFolderDto.numberOfFoldersFound) + (this.dataItem.item.rootFolderDto.numberOfFoldersFound == 1 ? ' folder' : ' folders');
    }

    let endTime = null;
    if (this.dataItem.item.crawlRunDetailsDto && this.dataItem.item.crawlRunDetailsDto.runOutcomeState == ERunStatus.RUNNING) {
      endTime = Date.now();
    }
    else {
      endTime = this.dataItem.item.crawlRunDetailsDto ? this.dataItem.item.crawlRunDetailsDto.scanEndTime : null;
    }
    this.lastRunElapsedTime = endTime ? Math.floor(endTime - this.dataItem.item.crawlRunDetailsDto.scanStartTime) : null;
    this.runSummaryObject = this.scanService.getRunSummaryObject(this.dataItem.item);
    this.phasePerecentageDone = null;
    if (this.dataItem.item.runningPhase && this.dataItem.item.runningPhase.phaseMaxMark) {
      this.phasePerecentageDone = Math.ceil(100 * this.dataItem.item.runningPhase.phaseCounter / this.dataItem.item.runningPhase.phaseMaxMark) + "%";
    }
    this.pauseReason = this.dataItem.item.crawlRunDetailsDto ? this.dataItem.item.crawlRunDetailsDto.pauseReason : null;

    this.fileTypesMap = {};
    if (this.dataItem.item.rootFolderDto.fileTypes) {
      this.dataItem.item.rootFolderDto.fileTypes.forEach((fileType) => {
        this.fileTypesMap[fileType] = true;
      });
    }
  }

  private parseData() {
    if(_.isEmpty(this.dataItem.item.rootFolderDto.realPath)) {
      this.dataItem.item.rootFolderDto.realPath = this.dataItem.item.rootFolderDto.mailboxGroup || '';
    }
    if (this.dataItem.item.crawlRunDetailsDto) {
      this.runStatus = this.dataItem.item.crawlRunDetailsDto.runOutcomeState;
      if (this.runStatus == ERunStatus.RUNNING && this.dataItem.item.crawlRunDetailsDto.gracefulStop) {
        this.runStatus = ERunStatus.STOPPING;
      }
      if (this.runStatus == ERunStatus.PAUSED && this.dataItem.item.crawlRunDetailsDto.pauseReason != EPauseReason.USER_INITIATED) {
        this.runStatus = ERunStatus.SUSPENDED;
      }
    }
    else {
      this.runStatus = ERunStatus.NEW;
    }

      if( this.dataItem.item.runningPhase && this.dataItem.item.runningPhase.phaseStart){
        const n = new Date();
        this.dataItem.item.runningPhase.runningPhaseElapsedTime =  n.getTime() - this.dataItem.item.runningPhase.phaseStart;
      }


  }


  setRescan(newRescanState) {
      this.scanService.setRootFolderActiveState(this.dataItem.item.rootFolderDto.id,newRescanState);
  }


  changeFavorite(){
    if (this.isFavorite) {
      this.scanFavoriteService.removeFavoriteRootFolder(this.dataItem.item.rootFolderDto.id);
      this.isFavorite = false;
    }
    else {
      this.scanFavoriteService.addFavoriteRootFolder(this.dataItem.item.rootFolderDto.id);
      this.isFavorite = true;
    }
  }


  expandOrCollapseNode() {
    if (this.dataItem.isOpen || (this.dataItem.item.jobs != null && this.dataItem.item.jobs.length > 0 )) {
      this.dataItem.isOpen = !this.dataItem.isOpen;
      setTimeout(()=> {
        if (!this.utilService.isInViewport(this.runs.nativeElement)) {
          this.runs.nativeElement.scrollIntoView(false);
        }
      });
    }
    else {
      this.scanService.getRootFolderMoreDetails(this.dataItem.item.rootFolderDto.lastRunId, (jobs)=> {
        this.dataItem.item.jobs = jobs;
        this.dataItem.isOpen = true;
        setTimeout(()=> {
          if (!this.utilService.isInViewport(this.runs.nativeElement)) {
            this.runs.nativeElement.scrollIntoView(false);
          }
        });
      });
    }
  }
}


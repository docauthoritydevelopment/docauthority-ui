import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';
import {CommonModule, DecimalPipe, LOCATION_INITIALIZED} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {UtilService} from '@services/util.service';
import {scanRoutes} from "@app/features/scan/scan.routing";
import {ScanComponent} from "@app/features/scan/scan.component";
import {ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {RootFolderItemGridComponent} from "@app/features/scan/activeScan/root-folder/item-grid/root-folder-item-grid.component";
import {RootFolderItemGridHeaderComponent} from "@app/features/scan/activeScan/root-folder/grid-header/root-folder-item-grid-header.component";
import {ActiveScanComponent} from "@app/features/scan/activeScan/active-scan.component";
import {SharedUiModule} from "@shared-ui/shared-ui.module";
import {LastScanPhasesComponent} from "@app/features/scan/lastScanPhases/last-scan-phases.component";
import {EditRootFolderScheduleDialog} from "@app/features/scan/popups/editRootFolderScheduleDialog/edit-root-folder-schedule-dialog";
import {scheduleConfigDisplay} from "@app/features/scan/popups/scheduleConfigDisplay/schedule-config-display";
import {TranslationModule} from "@app/common/translation/translation.module";
import {ScanCenterComponent} from "@app/features/scan/scanCenter/scan-center.component";
import {CenterActiveScansComponent} from "@app/features/scan/scanCenter/centerActiveScans/center-active-scans.component";
import {CommonChartsModule} from "@app/common/charts/common-charts.module";
import { GridsterModule } from 'angular-gridster2';
import {FilterModule} from "@app/common/filters/filter.module";
import {RunScanDialogComponent} from "@app/features/scan/popups/runScanDialog/runScanDialog.component";
import {EditRootFolderDialog} from "@app/features/scan/popups/editRootFolderDialog/edit-root-folder-dialog";
import {InputParamComponent} from "@app/features/scan/components/inputParam/input-param.component";
import {ChooseDataCenterComponent} from "@app/features/scan/components/chooseDataCenter/choose-data-center.component";
import { ClipboardModule } from 'ngx-clipboard';
import {SystemComponentsComponent} from "@app/features/scan/systemComponents/system-components.component";
import {SystemComponentGridComponent} from "@app/features/scan/systemComponents/item-grid/system-component-grid.component";
import {ScheduleGroupItemGridComponent} from "@app/features/scan/activeScan/schedule-group/schedule-group-item-grid/schedule-group-item-grid.component";
import {ScheduleGroupGridHeaderComponent} from "@app/features/scan/activeScan/schedule-group/schedule-group-grid-header/schedule-group-grid-header.component";
import {ScheduleGroupComponent} from "@app/features/scan/activeScan/schedule-group/schedule-group.component";
import {RootFolderSummaryComponent} from "@app/features/scan/components/rootFolderSummary/root-folder-summary.component";
import {ScanHistoryComponent} from "@app/features/scan/activeScan/scan-history/scan-history.component";
import {ScanHistoryItemGridComponent} from "@app/features/scan/activeScan/scan-history/scan-history-item-grid/scan-history-item-grid.component";
import {ScanHistoryGridHeaderComponent} from "@app/features/scan/activeScan/scan-history/scan-history-grid-header/scan-history-grid-header.component";
import {FilterDiv} from "@app/features/scan/components/filterDiv/filter-div";
import {RootFolderComponent} from "@app/features/scan/activeScan/root-folder/root-folder.component";
import {BusinessListComponent} from "@app/features/scan/businessList/business-list.component";
import {BizListMonitorGridHeaderComponent} from "@app/features/scan/businessList/biz-list-monitor/biz-list-monitor-grid-header/biz-list-monitor-grid-header.component";
import {BizListMonitorItemGridComponent} from "@app/features/scan/businessList/biz-list-monitor/biz-list-monitor-item-grid/biz-list-monitor-item-grid.component";
import {BizListMonitorComponent} from "@app/features/scan/businessList/biz-list-monitor/biz-list-monitor.component";
import {BizListService} from "@app/features/scan/services/biz-list.service";
import {ScanHistoryScreenComponent} from "@app/features/scan/scanHistory/scan-history-screen.component";
import {ScheduleGroupScreenComponent} from "@app/features/scan/scheduleGroupScreen/schedule-group-screen.component";
import {ScanFavoriteService} from "@app/features/scan/services/scan-favorite.service";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import {StatusIconComponent} from "@app/features/scan/components/statusIcon/status-icon.component";
import {PeriodPipe} from "@shared-ui/pipes/periodPipe.pipe";
import {ServerComponentComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponent.component";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {ServerComponentItemGridComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentItemGrid/serverComponentItemGrid.component";
import {ServerComponentScreenComponent} from "@app/features/scan/serverComponentsScreen/serverComponentScreen.component";
import {BytesPipe} from "@shared-ui/pipes/bytes.pipe";
import {CheckNullPipe} from "@shared-ui/pipes/checkNull.pipe";
import {LimitBigPipe} from "@shared-ui/pipes/limitBig.pipe";
import {ServerComponentHistoryComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentHistory/serverComponentHistory.component";
import {ServerComponentHistoryItemGridComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentHistory/serverComponentHistoryItemGrid/serverComponentHistoryItemGrid.component";
import {ServerComponentHistoryGridHeaderComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentHistory/serverComponentHistoryGridHeader/serverComponentHistoryGridHeader.component";
import {ServerComponentBrokerTemplateComponent} from "@app/features/scan/components/serverComponentBrokerTemplate/serverComponentBrokerTemplate.component";
import {DialogsModule} from "@app/common/dialogs/dialogs.module";
import {ServerComponentHistoryDialogComponent} from "@app/features/scan/popups/serverComponentHistoryDialog/serverComponentHistoryDialog.component";
import {EditServerComponentDialogComponent} from "@app/features/scan/popups/editServerComponentDialog/editServerComponentDialog.component";
import {
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateLoader,
  TranslateModule,
  TranslateParser, TranslateService
} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslationGuard} from "@app/share-services/translation.guard";

/*
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/scan/', '.json');
}
*/

import {DaDraggableModule} from "@shared-ui/daDraggable.module";

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    LoaderModule,
    TreeGridModule,
    NgbModule,
    TranslationModule,
    RouterModule.forChild(scanRoutes),
    CommonChartsModule,
    GridsterModule,
    FilterModule,
    ClipboardModule,
    DialogsModule,
    TranslateModule.forChild(),
/*     TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: true
    }) */
    DaDraggableModule
  ],
  declarations: [
    RootFolderItemGridComponent,
    RootFolderItemGridHeaderComponent,
    ScanComponent,
    ActiveScanComponent,
    ScanCenterComponent,
    LastScanPhasesComponent,
    EditRootFolderScheduleDialog,
    EditRootFolderDialog,
    scheduleConfigDisplay,
    CenterActiveScansComponent,
    RunScanDialogComponent,
    InputParamComponent,
    ChooseDataCenterComponent,
    SystemComponentsComponent,
    SystemComponentGridComponent,
    ScheduleGroupItemGridComponent,
    ScheduleGroupGridHeaderComponent,
    ScheduleGroupComponent,
    RootFolderSummaryComponent,
    ScanHistoryComponent,
    ScanHistoryItemGridComponent,
    ScanHistoryGridHeaderComponent,
    FilterDiv,
    RootFolderComponent,
    BusinessListComponent,
    BizListMonitorComponent,
    BizListMonitorGridHeaderComponent,
    BizListMonitorItemGridComponent,
    ScanHistoryScreenComponent,
    ScheduleGroupScreenComponent,
    StatusIconComponent,
    ServerComponentComponent,
    ServerComponentItemGridComponent,
    ServerComponentScreenComponent,
    ServerComponentHistoryComponent,
    ServerComponentHistoryItemGridComponent,
    ServerComponentHistoryGridHeaderComponent,
    ServerComponentBrokerTemplateComponent,
    ServerComponentHistoryDialogComponent,
    EditServerComponentDialogComponent
  ],
  providers: [
    TranslationGuard,
    ScanRouteManager,
    UtilService,
    ScanService,
    ScanFavoriteService,
    BizListService,
    DecimalPipe,
    NumSuffixPipe,
    PeriodPipe,
    BytesPipe,
    LimitBigPipe,
    CheckNullPipe,
    ServerComponentService
  ],
  entryComponents: [
    RootFolderItemGridComponent,
    RootFolderItemGridHeaderComponent,
    EditRootFolderScheduleDialog,
    RunScanDialogComponent,
    EditRootFolderDialog,
    SystemComponentGridComponent,
    ScheduleGroupItemGridComponent,
    ScheduleGroupGridHeaderComponent,
    ScanHistoryComponent,
    ScanHistoryItemGridComponent,
    ScanHistoryGridHeaderComponent,
    BizListMonitorGridHeaderComponent,
    BizListMonitorItemGridComponent,
    ServerComponentItemGridComponent,
    ServerComponentHistoryDialogComponent,
    ServerComponentHistoryItemGridComponent,
    ServerComponentHistoryGridHeaderComponent,
    EditServerComponentDialogComponent
  ],

})
export class ScanModule {
  constructor() {
  }
}


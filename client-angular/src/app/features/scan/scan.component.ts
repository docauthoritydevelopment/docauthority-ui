import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {TranslateService} from "@ngx-translate/core";
import {scanTranslate} from "@app/translate/scan.translate";
import {commonTranslate} from "@app/translate/common.translate";
import {dashboardTranslate} from "@app/translate/dashboard.translate";


@Component({
  selector: 'scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScanComponent implements OnInit {

  constructor(private i18nService: I18nService, private translateService : TranslateService) {
    this.translateService.setTranslation('en', scanTranslate,false);
    this.translateService.setTranslation('en', i18nService.getCommonTranslate(),true);
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }

  ngOnInit() {
  }
}


import {Component, Input, OnInit, Output, EventEmitter , ViewEncapsulation, HostBinding} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {FilterTypes} from "@app/common/filters/types/filter-enums";
import {UtilService} from "@services/util.service";
import {UserSettingsService} from "@services/user.settings.service";
import {StorageKeys} from "@core/types/storage-keys";
import {FilterItem} from "@app/common/filters/types/filter-interfaces";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {LoginService} from "@services/login.service";

@Component({
  selector: 'filter-div',
  templateUrl: './filter-div.html',
  styleUrls: ['./filter-div.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterDiv extends BaseComponent {

  isOnFilter: boolean = false;
  filterDropDownItems:DropDownConfigDto[] = [];
  onlyFavorite:boolean = false;


  _filtersList:FilterItem[] = null;
  @Output()
  filterChanged = new EventEmitter();

  @Output()
  collapsedChanged = new EventEmitter();

  @Input()
  withFavoriteChoose:boolean = false;

  @Input()
  isVisible:boolean = true;

  @Input()
  set filtersList(newList:any[]) {
    this._filtersList = newList;
    this.checkOnFilter();
    this.filterDropDownItems =  [{
      title: "Clear all filters",
      iconClass : "fa fa-trash-o",
      disabled : !this.isOnFilter,
      actionFunc: ()=>{
        this.clearAllFilters();
      }
    }];
  }

  get filtersList(): any[] {
    return this._filtersList;
  }

  @HostBinding('class') mainClass: string = 'hide-filter';




  showFilter = false;
  FilterTypes =  FilterTypes;




  constructor(private utilService : UtilService,    private userSettingsService: UserSettingsService, private loginService: LoginService) {
    super();
  }

  getFilterStorageKey() {
    let currUser = this.loginService.getCurrentUser();
    return currUser.username + "_"+ StorageKeys.FILTER_DIV;
  }

  daOnInit(){
    let isOpenStr = localStorage.getItem(this.getFilterStorageKey());
    this.showFilter =  false;
    if (isOpenStr) {
      this.showFilter = JSON.parse(isOpenStr);
    }
    if (!this.showFilter) {
      this.mainClass = 'hide-filter';
    }
    else {
      this.mainClass = '';
    }
  }

  setShowFilter(newState:boolean) {
    this.showFilter = newState;
    localStorage.setItem(this.getFilterStorageKey(), JSON.stringify(newState));
    if (!this.showFilter) {
      this.mainClass = 'hide-filter';
    }
    else {
      this.mainClass = '';
    }
  }


  onFilterChanged(filterOption:any , filterField : string ) {
    this.filterChanged.emit(
      {
        fields: [filterField],
        filterOptions: [filterOption]
      });
  }


  checkOnFilter = ()=> {
    this.isOnFilter = false;
    if (this.filtersList) {
      this.filtersList.forEach((fItem) => {
        if (fItem.filterOptions) {
          fItem.filterOptions.forEach((fOption: any) => {
            if (fOption.selected) {
              this.isOnFilter = true;
            }
            if (fOption.children && !this.isOnFilter) {
              fOption.children.forEach((childOption: any) => {
                if (childOption.selected) {
                  this.isOnFilter = true;
                }
              });
            }
          });
        }
      });
    }
  }

  clearAllFilters = (event = null)=> {
    if (event != null) {
      event.stopImmediatePropagation();
    }
    let keysArr= [];
    let valuesArr = [];
    this.filtersList.forEach((fItem)=> {
      keysArr.push(fItem.filterField);
      fItem.filterOptions.forEach((fOption:any)=> {
        fOption.selected = false;
        if (fOption.children) {
          fOption.children.forEach((childOption:any)=> {
            childOption.selected = false;
          });
        }
      });
      valuesArr.push(fItem.filterOptions);
    });
    this.filterChanged.emit(
      {
        fields: keysArr,
        filterOptions: valuesArr
      });
  }


  onCollapseChanged(newValue,fItem) {
    fItem.isCollapsed = newValue;
    this.collapsedChanged.emit(fItem);
  }


  changeFilter(fItem) {
    fItem.filterOptions[0].selected = !fItem.filterOptions[0].selected;
    this.filterChanged.emit(
      {
        fields: [fItem.filterField],
        filterOptions: [fItem.filterOptions]
      });
  }



}


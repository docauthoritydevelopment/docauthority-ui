import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ClaComponentStatusDto} from "@app/common/scan/types/scan.dto";


@Component({
  selector: 'server-component-broker-template',
  templateUrl: './serverComponentBrokerTemplate.component.html',
  styleUrls: ['./serverComponentBrokerTemplate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ServerComponentBrokerTemplateComponent extends BaseComponent {

  @Input()
  brokerNameField: any;

  _item: ClaComponentStatusDto;

  @Input()
  set item (newItem: ClaComponentStatusDto) {
    if (!newItem[this.brokerNameField]) {
      newItem[this.brokerNameField] = {};
    }
    this._item = newItem;
  }

  get item(): ClaComponentStatusDto {
    return this._item;
  }

  constructor() {
    super();
  }

  daOnInit() {
  }

}


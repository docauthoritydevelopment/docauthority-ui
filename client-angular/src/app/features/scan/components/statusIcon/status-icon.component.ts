import {Component, Input, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ERunStatus, EJobState} from "../../../../common/scan/types/scanEnum.types";

@Component({
  selector: 'status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StatusIconComponent extends BaseComponent {

  @Input()
  status: any;

  ERunStatus = ERunStatus;
  EJobState = EJobState;

  constructor() {
    super();
  }

}


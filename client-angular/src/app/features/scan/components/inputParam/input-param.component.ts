import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {SCAN_CONFIG, SCAN_OPERATION} from "@app/common/scan/types/scanEnum.types";
import {BaseComponent} from "@core/base/baseComponent";
import {ParamActiveItem, ParamOption} from "@app/common/scan/types/scan.dto";
import {UtilService} from "@services/util.service";
import * as _ from 'lodash';

@Component({
  selector: 'input-param',
  templateUrl: './input-param.component.html',
  styleUrls: ['./input-param.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InputParamComponent extends BaseComponent{

  selectedItems=[];
  allSelectedItems=[];
  checkedSelectedItems=[];
  MAX_SELECTED_ITEMS = SCAN_CONFIG.max_data_records_in_dialog_to_scroll;
  DISPLAYED_ITMES_WHEN_MORE_THEN_MAX = SCAN_CONFIG.displayed_items_when_more_then_max;

  CREATE_NEW_OBJECT="CREATE_NEW_OBJECT";
  INPUT_EMPTY_OPTION="INPUT_EMPTY_OPTION";

  itemsOptions;
  savedItemExceededMax;
  totalItems;
  selectedItem;
  textEntered;
  _savedSelectedItems;


  @Input()
  onItemsSelected;

  @Input()
  actionName;

  @Input()
  removeAll  = () => {
    this.allSelectedItems.forEach(t => {
      if (this.onItemDeleted) {
        this.onItemDeleted(t, function () {
        }, function () {

        });
      }
    })
    this.allSelectedItems = [];
    this.updateSelectedItems();
  }

  @Input()
  loadOptionItems;

  @Input()
  refreshList = (items) =>  {
    this.itemsOptions = items;
  };

  @Input()
  reloadSavedValueParameters;

  @Input()
  createNewSelectedItemFromText;

  @Input()
  createNewOptionFromItem;

  @Input()
  importListFromEntityFilterDataFn;

  @Input()
  initiateViewAllDialogOpenState;

  @Input()
  importAutoEntity1;

  @Input()
  importAutoEntity2;

  @Input()
  mediaConnectionId;

  @Input()
  mediaType;

  @Input()
  customerDataCenterId;

  @Input()
  rootFolder;

  @Input()
  objectId;

  @Input()
  openNewObjectDialog;

  @Input()
  onNewItemAdded;

  @Input()
  onItemDeleted;

  @Input()
  onItemActiveStateChanged;

  @Input()
  options;

  @Input()
  classType;

  @Input()
  singleSelection;

  @Input()
  leaveTextAfterSelection;

  @Input()
  noCreateNewObjectButton;

  @Input()
  createEmptyItem;

  @Input()
  name;

  @Input()
  required;

  @Input()
  showCheckboxes;

  @Input()
  init;


  constructor(private utilService:UtilService) {
    super();
  }

  daOnInit()
  {
    this.loadItems();
  }

  @Input()
  set savedSelectedItems(newItems:any) {
    this._savedSelectedItems = newItems;
    this.afterChangeSavedSelectedItems();
  }

  get savedSelectedItems():any  {
    return this._savedSelectedItems;
  }



  afterChangeSavedSelectedItems() {
    if( this.savedSelectedItems&&this.savedSelectedItems[0]) {
      if(this.savedSelectedItems.length>this.MAX_SELECTED_ITEMS)
      {
        this.savedItemExceededMax = true;
        this.totalItems = this.savedSelectedItems.length;
        for(var i=0; i<this.DISPLAYED_ITMES_WHEN_MORE_THEN_MAX; i++){
          this.addSelectedItem(this.savedSelectedItems[i],this.savedSelectedItems[i].active);

        };
        if(this.initiateViewAllDialogOpenState)
        {
          this.openAllValueItemsInDialog();
          this.initiateViewAllDialogOpenState=false;
        }
      }
      else {
        this.savedSelectedItems.forEach(s=> {   //Ensure each is not double selected
          this.addSelectedItem(s,s.active);
        });
      }
    }
  };

  loadItems = () =>
  {
    this.itemsOptions = [];
    if(!this.noCreateNewObjectButton) {
      var newOption = {
        id :  this.CREATE_NEW_OBJECT,
        value :  this.CREATE_NEW_OBJECT,
        display : this.CREATE_NEW_OBJECT
    }
      this.itemsOptions.push(newOption);
    }
    if(this.createEmptyItem && this.singleSelection) {
      var emptyOption = {
        id: this.INPUT_EMPTY_OPTION,
        value: this.INPUT_EMPTY_OPTION,
        display: this.INPUT_EMPTY_OPTION
      }
      this.itemsOptions.push(emptyOption);
    }
    if(this.utilService.parseBoolean(this.options)) {
      if(_.isFunction(this.loadOptionItems)) {
        this.loadOptionItems( this.itemsOptions);
      }
    }
  }

  openBrowseServerFoldersDialog = () =>
  {
    /*
    var dlg = dialogs.create(
      'common/directives/dialogs/folderBrowserDialog.tpl.html',
      'folderBrowserDialogCtrl',
      {rootFolder:$scope.rootFolder,mediaType:$scope.mediaType,mediaConnectionId:$scope.mediaConnectionId,dataCenterId:$scope.customerDataCenterId,editRootFolderMode:false,title:"Select excluded folder"},
      {size: 'md',windowClass: 'offsetDialog'});

    dlg.result.then(function (newRootFolderPath:string) {
      $scope.addNewByUserText(newRootFolderPath);
    }, function () {

    });
    */
  }

  openAllValueItemsInDialog = ()=>  {
    /*
    var dlg = dialogs.create(
      'common/directives/dialogs/pageableCheckableItemsDialog.tpl.html',
      'pageableCheckableItemsDialogCtrl',
      {
        loadData:loadData ,
        reloadData:$scope.reloadData,
        title: "Manage action script parameters",
        objectId:$scope.objectId,
        classType:$scope.classType,loadOptionItems:$scope.loadOptionItems,options:$scope.options,createNewSelectedItemFromText:$scope.createNewSelectedItemFromText,actionName:$scope.actionName
      },
      {size: 'md', windowClass: 'offsetDialog'});

    dlg.result.then(function (itemsToUpdate:any[]) {
      //   $scope.initialObjectValues=itemsToUpdate;
    }, function () {

    });

    function loadData(gridOptions,$element)
    {
      $scope.loadAllData(gridOptions,$element, $scope.savedSelectedItems);
    }
    $scope.loadAllData=function(gridOptions,$element,savedSelectedItems)
    {
      var data= savedSelectedItems;
      setDataToGrid( data )
      function setDataToGrid(values)
      {
        var grid = $element;
        if (grid[0]&&  grid.data("kendoGrid")) {

          gridOptions.setData(grid, values);

        }
        else {
          gridOptions.gridSettings.dataSource.data = values;
          gridOptions.gridSettings.dataSource.transport = null;
          gridOptions.gridSettings.autoBind = true;
        }
      }
    }
    */
  }

  removeSelectedItem = (item) =>
  {
    var index =this.allSelectedItems.indexOf(item);
    if(index>-1)
    {
      if(this.onItemDeleted)
      {
        this.onItemDeleted(this.allSelectedItems[index],function()
        {},function()
        {

        });
      }
      this.allSelectedItems.splice(index,1);
      this.updateSelectedItems();
    }
  }

  refreshUserText = ($select)=>  {
    var text = $select.search;
    var FLAG = -1;
    if (text && this.createNewSelectedItemFromText) {
      var foundItem =  this.allSelectedItems ? this.allSelectedItems.filter(s=>s.item.id == FLAG)[0] : null;
      if (foundItem) {
        var foundIndex = this.allSelectedItems.indexOf(foundItem);
        this.allSelectedItems.splice(foundIndex, 1);
      }
      var foundInRecentItems = $select.items ? $select.items.filter(i=>i.id == FLAG)[0] : null;
      if (foundInRecentItems) { //if already exists remove it
        var foundIndex: number = $select.items.indexOf(foundInRecentItems);
        $select.items.splice(foundIndex, 1);
      }

      var newItem:ParamActiveItem<any> = this.createNewSelectedItemFromText(text);
      newItem.item.id = FLAG;
      $select.items.unshift(newItem.item)

      this.optionSelected(newItem.item, null, $select);
    }
  }

  addNewByUserText = (text:string) =>
  {
    if(text && text.trim()!='') {
      var newItem:ParamActiveItem<any>  =  this.createNewSelectedItemFromText(text);
      var selectedItem:ParamActiveItem<any>=  this.addSelectedItem(newItem);
      if(this.onNewItemAdded)
      {
        this.onNewItemAdded(selectedItem,function(){

        },function(){
          this.reloadData();
        });
      }
    }
  }


  optionSelected = (selectedItem:ParamOption<any>,$model,$select) =>
  {
    if(selectedItem.id==this.CREATE_NEW_OBJECT)
    {
      /*
      $scope.openNewObjectDialog(function(si){
        var newOption = $scope.createNewOptionFromItem(si);
        //     $select.selected=newOption;
        $scope.optionSelected(newOption , $model,$select );
        loadItems();
        $scope.reloadData();
      },function()
      {
        $model = null;
        $scope.selectedItem = null;
      });
      */

    } else if(selectedItem.id==this.INPUT_EMPTY_OPTION) {
      this.allSelectedItems=[];
      this.onItemsSelected(null);
    }
    else {
      var newOption:any = {};
      newOption.active=true;
      newOption.item=selectedItem;
      $select.selected = newOption.item;
      return this.addSelectedItem(newOption);
    }
    return null;
  }


  addSelectedItem = (selectedItem:ParamActiveItem<any>,isActive?:boolean)=>
  {
    var foundItem =selectedItem.item.id? this.allSelectedItems.filter(s=>s.item.id==selectedItem.item.id)[0]:this.allSelectedItems.filter(s=>s.item==selectedItem)[0];
    if(foundItem)
    {
      foundItem.active = true;
    }
    else {
      selectedItem.active=isActive!=null?isActive:true;
      if(this.singleSelection)
      {
        this.allSelectedItems=[selectedItem];
      }
      else {
        this.allSelectedItems.push(selectedItem);
      }
    }
    if(!this.leaveTextAfterSelection) {
      this.selectedItem = null;
      this.textEntered = Date.now(); //just for calling a change
    }
    else {
      this.selectedItem = selectedItem.item;
    }

    this.updateSelectedItems();
    return selectedItem;

  }

  reloadData= (selectedItem) =>
  {
    if(selectedItem) {
      this.addSelectedItem(selectedItem);
    }
    if( this.reloadSavedValueParameters) {
      this.reloadSavedValueParameters(this.allSelectedItems);
    }
  }

  checkedChange = (selectedItem) =>
  {
    if(!this.showCheckboxes) //label element triggers onClick event checkbox is not shown
    {
      return;
    }
    if(this.onItemActiveStateChanged)
    {
      this.onItemActiveStateChanged(selectedItem,function()
        { },
        function(){});
    }
    this.updateSelectedItems();
  }

  updateSelectedItems = ()=>
  {
    this.checkedSelectedItems=this.allSelectedItems.filter(i=>i.active).map(s=>s.item);
    this.onItemsSelected(this.singleSelection?this.allSelectedItems[0]: this.allSelectedItems);
  }

  changeCheckAll = (val:boolean)=>
  {
    this.allSelectedItems.forEach(s=>
    {
      (<any>s).active=val;
      if(this.onItemActiveStateChanged)
      {
        this.onItemActiveStateChanged(s);
      }
    });
    this.updateSelectedItems();
  }


  removeAllUnchecked = ()=>
  {
    this.allSelectedItems.filter(i=>!i.active).forEach(t=>
    {
      if(this.onItemDeleted)
      {
        this.onItemDeleted(t,function()
        {},function()
        {

        });
      }
    })
    this.allSelectedItems=this.allSelectedItems.filter(i=>i.active);
    this.updateSelectedItems();
  }



  getSelectedText = (text)=> {
    if(text == this.INPUT_EMPTY_OPTION) {
      return '';
    }
    return text;
  }


  exportToExcel = ()=>
  {

  }


  openUploadFromFileDialog = () =>
  {
    /*
    var dlg = dialogs.create(
      'common/directives/dialogs/uploadFileDialog.tpl.html',
      'uploadFileDialogCtrl',
      {title:'Upload '+$scope.actionName+' '+$scope.classType+' values from csv file'},
      {size:'md'});

    dlg.result.then(function(data:any[] ){
      if(data && data.length>0) {
        var header = data[0].split(',');
        var isFullObject = _.startsWith(header[0],'Active') ;
        if(isFullObject) {
          data.splice(0, 1); //remove header row
        }

        var importedItems=[];
        data.forEach(d=>
        {
          var properties = d.split(',');
          if(properties[0]) {
            if (isFullObject) {
              var newItem:ParamActiveItem<any> = $scope.createNewSelectedItemFromText(properties[1]);
              newItem.active = properties[0];
              importedItems.push(newItem);
            }
            if (properties.length == 1) {
              var newItem:ParamActiveItem<any> = $scope.createNewSelectedItemFromText(properties[0]);
              importedItems.push(newItem);
            }
          }
        });
        $scope.savedSelectedItems = importedItems;
      }
    },function(){

    });
    */
  }

  onError = ()=>
  {
  }



}

import {Component, Input, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import {ERunStatus, } from "@app/common/scan/types/scanEnum.types";
import {BaseComponent} from "@core/base/baseComponent";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";

@Component({
  selector: 'root-folder-summary',
  templateUrl: './root-folder-summary.component.html',
  styleUrls: ['./root-folder-summary.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RootFolderSummaryComponent extends BaseComponent {

  @Input()
  summaryObject: any;
  ERunStatus = ERunStatus;

  @Output()
  statusFilterChanged = new EventEmitter();




  constructor() {
    super();
  }

  openFilter(filterField:ERunStatus) {
    this.statusFilterChanged.emit(filterField);
  }

}


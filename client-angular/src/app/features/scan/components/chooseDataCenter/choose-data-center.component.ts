import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {SCAN_CONFIG, SCAN_OPERATION} from "@app/common/scan/types/scanEnum.types";
import {BaseComponent} from "@core/base/baseComponent";
import {CustomerDataCenterDto, ParamActiveItem, ParamOption} from "@app/common/scan/types/scan.dto";
import {UtilService} from "@services/util.service";
import * as _ from 'lodash';
import {ScanService} from "@app/features/scan/services/scan.service";

@Component({
  selector: 'choose-data-center',
  templateUrl: './choose-data-center.component.html',
  styleUrls: ['./choose-data-center.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChooseDataCenterComponent extends BaseComponent {

  @Input()
  selectedDataCenter: CustomerDataCenterDto;

  @Input()
  saveLastSelected = () =>
  {
    if (this.selectedDataCenter) {
      this.setLastSelectedFn(this.selectedDataCenter.id);
    }
  }

  @Input()
  getLastSelectedFn: (successFunction: (id: number) => void) => void;

  @Input()
  setLastSelectedFn: (id: number) => void;

  selectedDataCenterSubdisplay: string;
  dataCenterSavedSelectedItems: ParamActiveItem<CustomerDataCenterDto>[];

  constructor(private scanService: ScanService) {
    super();
  }

  daOnInit() {
    this.setSavedSelectedDataCenter(this.selectedDataCenter);
  }


  setSavedSelectedDataCenter = (dataCenter: CustomerDataCenterDto) => {

    if (dataCenter && dataCenter.id) {

      this.dataCenterSavedSelectedItems = [this.createDataCenterNewSelectedItem(dataCenter)];

    }
    else if (this.getLastSelectedFn) {
      this.do.apply(this, null);

    }
    else {
      this.dataCenterSavedSelectedItems = [];
    }
  }

  do() {
    this.getLastSelectedFn(
      function (centerId) {
        this.initSelectedDataCenterById(centerId);
      }.bind(this)
    );
  }

  createDataCenterNewSelectedItem(dataCenter: CustomerDataCenterDto): ParamActiveItem<CustomerDataCenterDto> {
    var newItem = this.createDataCenterParamOption(dataCenter);
    var paramActiveItem: ParamActiveItem<CustomerDataCenterDto> = {
      active: true,
      item: newItem
    }
    return paramActiveItem;
  }

  createDataCenterParamOption(dataCenter: CustomerDataCenterDto): ParamOption<CustomerDataCenterDto> {
    var descriptionPart = dataCenter.description ? dataCenter.description : ' ';
    var locationPart = dataCenter.location ? dataCenter.location : ' ';
    var newItem: ParamOption<CustomerDataCenterDto> = {
      id: dataCenter.id,
      display: dataCenter.name,
      subdisplay: locationPart + ' ' + descriptionPart,
      value: dataCenter
  }
    return newItem;
  }

  initSelectedDataCenterById = (centerId) => {
    if (centerId) {
      this.scanService.getDataCenterRequest(centerId).subscribe((dataCenter: CustomerDataCenterDto) => {
        if (dataCenter) {
          this.setSavedSelectedDataCenter(dataCenter);
        }
      })
    }
    else {
      this.scanService.getDataCentersRequest().subscribe((dataCenters: CustomerDataCenterDto[]) => {
          if (dataCenters && dataCenters.length > 0) {
            var defaultDataCenter = dataCenters.filter(d => d.defaultDataCenter);
            this.setSavedSelectedDataCenter(defaultDataCenter[0]);
          }
        }
        , function () {
        })
    }
  }

  loadDataCenterOptionItems = (itemsOptions: ParamOption<CustomerDataCenterDto>[]) => {
    let _local = this;
    this.scanService.getDataCentersRequest().subscribe((dataCenters: CustomerDataCenterDto[]) => {
        if (dataCenters) {
          dataCenters.forEach(dataCenters => {
            var newItem = this.createDataCenterParamOption(dataCenters);
            itemsOptions.push(newItem);
          })
        }
      }
      , this.onError)
  }

  onDataCenterItemSelected = (dataCenter: ParamActiveItem<CustomerDataCenterDto>) => {
    this.selectedDataCenter = dataCenter.item.value;
    //  (<Operations.RootFolderDto>$scope.rootFolderItem).customerDataCenterDto  =(<Operations.DtoCustomerDataCenter>dataCenter.item.value);
    this.selectedDataCenterSubdisplay = dataCenter.item.subdisplay;

  }

  openNewDataCenterDialog = (successFunction, cancelFunction) => {
    /*
    var dlg = this.dialogs.create(
      'common/directives/operations/dialogs/newDataCenterDialog.tpl.html',
      'newDataCenterItemDialogCtrl',
      {createNew: false},
      {size: 'md', windowClass: 'offsetDialog'});

    dlg.result.then(function (dataCenter:Operations.DtoCustomerDataCenter) {
      successFunction(dataCenter);
    }, function () {
      cancelFunction ? cancelFunction() : null;
    });
    */
  }



  onError() {
  }
}


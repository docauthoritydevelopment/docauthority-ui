import {
  ECrawlerPhaseBehavior, EDayOfTheWeek, EFileType,
  EJobCompletionStatus,
  EJobState,
  EJobType, EMediaType, ERunType, ERunStatus,
  EScheduleConfigType, EPauseReason, DirectoryExistStatus
} from "@app/common/scan/types/scanEnum.types";

export interface DtoPhaseDetails {
  name: string;
  jobType: EJobType;
  phaseCounter: number;
  phaseMaxMark: number;
  phaseStart: number;
  phaseLastStopTime: number;
  pauseDuration: number;
  phaseError: string;
  failedTasksCount: number;
  jobState: EJobState;
  stateDescription: string;
  jobId: number;
  tasksPerSecRunningRate: number;
  tasksPerSecRate: number;
  runningRateDurationInMsc: number;
  completionStatus: EJobCompletionStatus;
  taskRetriesCount: number;
  bizListId: number;
  runningPhaseElapsedTime:number; //Remove this, this is UI only

}


export interface DtoScheduleGroup {
  id: number;
  name: string;
  analyzePhaseBehavior: ECrawlerPhaseBehavior;
  rootFolderIds: number[];
  active: boolean;
  cronTriggerString: string;
  schedulingDescription: string;
  scheduleConfigDto: DtoScheduleConfig;
}



export interface   DtoScheduleConfig {
  scheduleType:EScheduleConfigType;
  hour:number;
  minutes:number;
  every:number;
  daysOfTheWeek:EDayOfTheWeek[];
  customCronConfig:string;
}

export interface RootFolderRunSummaryInfo{
  rootFolderSummaryInfo:DtoRootFolderSummaryInfo;
  jobs:DtoPhaseDetails[];
  currentJob: DtoPhaseDetails;
}

export interface  DtoRootFolder {
  id: number;
  lastRunId: number;
  path: string;
  realPath: string;
  docStoreId: number;
  numberOfFoldersFound: number;
  folderExcludeRulesCount: number;
  scannedFilesCountCap: number;
  scannedMeaningfulFilesCountCap: number;
  storeLocation: string;
  storePurpose: string;
  storeSecurity: string;
  extractBizLists: boolean;
  mediaType: EMediaType;
  rescanActive: boolean;
  description: string;
  fileTypes: EFileType[];
  mediaConnectionDetailsId: number;
  mediaConnectionName: string;
  nickName: string;
  customerDataCenterDto: DtoCustomerDataCenter;
  reingestTimeStampMs: number;
  reingest: boolean;
  firstScan:boolean;
  isAccessible:DirectoryExistStatus;
}

export interface DtoCustomerDataCenter {
  id: number;
  location: string;
  name: string;
  description: string;
  createdOnDB: number;
  defaultDataCenter: boolean;
}

export interface  ParamOption<T>
{
  id:any;
  value:T;
  display:any;
  subdisplay:any;
  title?:any;
}
export interface ParamActiveItem<T>
{
  active:boolean;
  item:ParamOption<T>;
}


export interface  MicrosoftConnectionDetailsDtoBase  extends DtoMediaTypeConnectionDetails {
  sharePointConnectionParametersDto:DtoSharePointConnectionParameters;
  type:string;
}

export interface  DtoSharePointConnectionDetails extends MicrosoftConnectionDetailsDtoBase  {

}
export interface DtoOneDriveConnectionDetails extends MicrosoftConnectionDetailsDtoBase  {

}

export interface DtoSharePointConnectionParameters {
  username:string;
  url:string;
  password:string;
  domain:string;
}

export interface DtoMediaTypeConnectionDetails {
  id:number;
  name:string;
  username:string;
  url:string;
  mediaType:EMediaType;
  connectionParametersJson: string;
}

export interface DtoBoxConnectionDetails  extends DtoMediaTypeConnectionDetails {
  jwt: string;
  type:string;
}

export class DtoRootFolderSummaryInfo {

  rootFolderDto:DtoRootFolder;
  crawlRunDetailsDto:DtoCrawlRunDetails;
  runStatus:ERunStatus;
  accessible:boolean;
  accessibleUnknown:boolean;
  customerDataCenterReady:boolean;
  scheduleGroupDtos:DtoScheduleGroup[];
  runningPhase:DtoPhaseDetails; //Remove this, this is UI only
  jobs:DtoPhaseDetails[]; //Remove this, this is UI only

}

export class DtoCrawlRunDetails {
  id: number;
  rootFolderDto: DtoRootFolder;
  scanStartTime: number;
  scanEndTime: number;
  runType: ERunType;
  runOutcomeState: ERunStatus;
  numberOfFoldersFound: number;
  totalProcessedFiles: number;
  processedPdfFiles: number;
  processedWordFiles: number;
  processedExcelFiles: number;
  processedOtherFiles: number;
  deletedFiles: number;
  newFiles: number;
  updatedContentFiles: number;
  updatedMetadataFiles: number;
  scanErrors: number;
  scanProcessingErrors: number;
  manual: boolean;
  accessible: boolean;
  ingestedRootFolders: number[];
  analyzedRootFolders: number[];
  scanFiles: boolean;
  ingestFiles: boolean;
  analyzeFiles: boolean;
  scheduleGroupId: number;
  scheduleGroupName: string;
  stateString: string;
  pauseReason:EPauseReason;
}

export interface DtoFolderExcludeRule {
  id?: number;
  matchString: string;
  rootFolderId: string;
  operator: string;
  disabled: boolean;
}

export interface filterChangedOutput{
  fields: string[],
  filterOptions: any[]
}


export interface breadcrumbItem{
  title : string,
  clickFunc? : any;
}


export interface DtoRunSummaryInfoView {
  id: any;
  elapsedTime:number;
  startTime:number;
  endTime:number;
  isRunning:boolean;
  nickName:string;
  jobName:string;
  jobType:string;
  runStatus:ERunStatus;
  isManualRun:boolean;
  rootFolderId:number;
  scheduleGroupName:string;
  scheduleGroupId:number;
  currentPhaseProgressCounter:number;
  currentPhaseProgressPercentage:number;
  currentPhaseProgressFinishMark:number;
  estimatedPhaseFinish:number;
  tasksPerSecRate:number;
  tasksPerSecRunningRate:number;
  tasksPerSecRateString:string;
  tasksPerSecRunningRateString:string;
}

export enum ScanRoutePaths {
  activeScans = 'ActiveScans',
  scanCenter = 'ScanCenter',
  bizList = 'BizList',
  scanHistory = 'ScanHistory'

}

export enum ScanPathParams {
  runStatus = 'runStatus',
  runPhase = 'runPhase',
  mediaType = 'mediaType',
  department = 'departmentId',
  scheduleGroupIds = 'scheduleGroupIds',
  scheduleGroup = 'scheduleGroup',
  scheduleGroupName = 'scheduleGroupName',
  rootFolderId = 'rootFolderId',
  rootFolderName = 'rootFolderName',
  dataCenter = 'dataCenterId',
  accessibility = 'accessibility',
  scanningOrder = 'scanningOrder',
  searchText = 'namingSearchTerm',
  pageSize = 'pageSize',
  activeScanMode = 'activeScanMode',
  page = 'page',
  take = 'take',
  sort = 'sort',
  selectedItemId = 'selectedItemId',
  rfPage = 'rfPage',
  sgPage = 'sgPage',
  shPage = 'shPage',
  scPage = 'scPage',
  shRunStatus = 'shRunStatus',
  shTextSearch = 'namingSearchTerm',
  sgTextSearch = 'groupNamingSearchTerm',
  withRootFolders = 'withRootFolders',
  onlyFavorite = 'onlyFavorite',
  timestamp = 'timestamp'

}

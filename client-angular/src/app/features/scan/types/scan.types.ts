export class  ServerComponentColumnObject {
  showTimestamp: boolean;
  showDiskSpace: boolean;
  showIngestTaskRequests: boolean;
  showAnalyzeTaskRequests: boolean;
  showAppServerIngestThroughput: boolean;
  showAppServerThroughput: boolean;
  showScanRequests: boolean;
  showScanResponses: boolean;
  showIngestResponses: boolean;
  showIngestRequests: boolean;
  showRealTimeRequests: boolean;
  showRealTimeResponses: boolean;
  showControlResponses: boolean;
  showContentCheckRequests: boolean;
  showThroughput: boolean;
  showStartScanTaskRequests: boolean;
  showPhysicalMemory: boolean;
  showSystemCpuLoad: boolean;
}


export class ServerComponentSummary {
  dbs: number;
  brokers: number;
  appServers: number;
  mpLive: number;
  mpFailed: number;
  mpDisabled : number;
  solrLive: number;
  solrFailed: number;
  brokerFailed:number;
}

import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SCAN_OPERATION} from "@app/common/scan/types/scanEnum.types";
import {UtilService} from "@services/util.service";
import {I18nService} from "@app/common/translation/services/i18n-service";

@Component({
  selector: 'ran-scan-dialog',
  templateUrl: './runScanDialog.component.html',
  styleUrls: ['./runScanDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RunScanDialogComponent implements OnInit {
  optionsList = [];
  data;
  selectOption = null;

  constructor(private activeModal: NgbActiveModal, private utilService: UtilService, private i18nService : I18nService) { }

  ngOnInit() {
    if (this.data && this.data.onlyMapMode == true)
    {
      this.optionsList = [
        {
          value: SCAN_OPERATION.ONLY_SCAN,
          displayText: 'Map',
          description: this.i18nService.translateId("DIALOGS.DESCRIPTIONS.FULL_SCAN")
        }];
      this.selectOption = SCAN_OPERATION.ONLY_SCAN;
    }
    else {
      this.optionsList = [
        {
          value: SCAN_OPERATION.ONLY_SCAN,
          displayText: 'Map',
          description: this.i18nService.translateId("DIALOGS.DESCRIPTIONS.FULL_SCAN")
        },
        {
          value: SCAN_OPERATION.ONLY_INGEST,
          displayText: 'Complete scan - Ingest',
          description: this.i18nService.translateId("DIALOGS.DESCRIPTIONS.INGEST_SCAN")
        },
        {
          value: SCAN_OPERATION.ONLY_ANALYZE,
          displayText: 'Complete scan - Analyze',
          description: this.i18nService.translateId("DIALOGS.DESCRIPTIONS.ANALYZE_SCAN")
        }];
    }
  }

  runScan() {
    this.activeModal.close(this.selectOption);
  }

  close() {
    this.activeModal.close(null);
  }

}

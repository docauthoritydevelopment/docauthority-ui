import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SCAN_OPERATION} from "@app/common/scan/types/scanEnum.types";
import {UtilService} from "@services/util.service";
import {ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";

@Component({
  selector: 'server-component-history-dialog',
  templateUrl: './serverComponentHistoryDialog.component.html',
  styleUrls: ['./serverComponentHistoryDialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ServerComponentHistoryDialogComponent  {

  constructor(private activeModal: NgbActiveModal, private utilService: UtilService) { }
  data: {openComponent : ClaComponentSummaryDto} = null;



  close() {
    this.activeModal.close(null);
  }

}

import {Component, OnInit, ViewEncapsulation, Input} from '@angular/core';
import {ScheduleConfigDto} from "@app/common/scan/types/scan.dto";
import {EDayOfTheWeek, EScheduleConfigType} from "@app/common/scan/types/scanEnum.types";


@Component({
  selector: 'schedule-config-display',
  templateUrl: './schedule-config-display.tpl.html',
  encapsulation: ViewEncapsulation.None
})
export class scheduleConfigDisplay implements OnInit {
  _scheduleConfig: ScheduleConfigDto = null;
  atHH;
  atMM;
  every;
  customCronConfig:string;

  @Input()
  readOnlyMode:boolean = false;


  @Input()
  set scheduleConfig(newConfig: ScheduleConfigDto) {
    this._scheduleConfig = newConfig;
    this.initSchedule();
  }

  get scheduleConfig(): ScheduleConfigDto {
    return this._scheduleConfig;
  }

  @Input()
  selectedScheduleType;


  startingTimeHoursRange = null;
  startingTimeHMinutesRange = null;

  scheduleTypeOptions = [
    EScheduleConfigType.DAILY,
    EScheduleConfigType.HOURLY
  ];

  dayNameOptions = [
    {value: EDayOfTheWeek.SUN, selected: false},
    {value: EDayOfTheWeek.MON, selected: false},
    {value: EDayOfTheWeek.TUE, selected: false},
    {value: EDayOfTheWeek.WED, selected: false},
    {value: EDayOfTheWeek.THU, selected: false},
    {value: EDayOfTheWeek.FRI, selected: false},
    {value: EDayOfTheWeek.SAT, selected: false},
  ];


  initTimes() {
    let timeHoursRange = [];
    let timeMinutesRange = [];
    for (let i = 0; i < 24; i++) {
      timeHoursRange.push(
        (i < 10) ? ('0' + i) : i + ''
      );
    }

    for (let i = 0; i < 60; i++) {
      timeMinutesRange.push(
        (i < 10) ? ('0' + i) : i + ''
      );
    }
    this.startingTimeHoursRange = timeHoursRange;
    this.startingTimeHMinutesRange = timeMinutesRange;
  }

  ngOnInit(): void {
    this.initTimes()
    this.initSchedule()
  }


  initSchedule() {

    if (this.scheduleConfig && this.startingTimeHoursRange) {
      this.atHH = this.scheduleConfig.hour != null ? this.startingTimeHoursRange.filter(h => h == (<ScheduleConfigDto>this.scheduleConfig).hour)[0] : null;
      this.atMM = this.scheduleConfig.minutes != null ? this.startingTimeHMinutesRange.filter(m => m == this.scheduleConfig.minutes)[0] : null;
      this.every = this.scheduleConfig.every;
      this.customCronConfig = this.scheduleConfig.customCronConfig;
      this.selectedScheduleType = this.scheduleConfig.scheduleType;
      if (this.scheduleConfig.scheduleType == EScheduleConfigType.WEEKLY && this.scheduleConfig.daysOfTheWeek) {
        this.dayNameOptions.forEach(f => (<any>f).selected = false);
        this.scheduleConfig.daysOfTheWeek.forEach(d => {
          this.dayNameOptions.filter(f => f.value == d)[0].selected = true;
        });
      }
    }
    else {
      this.every = 1;
      this.selectedScheduleType = EScheduleConfigType.DAILY;
    }
  }


  isWeekly = function () {
    return this.selectedScheduleType == EScheduleConfigType.WEEKLY;
  };

  isDaily = function () {
    return this.selectedScheduleType == EScheduleConfigType.DAILY;
  };

  isHourly = function () {
    return this.selectedScheduleType == EScheduleConfigType.HOURLY;
  };

  isCustom = function () {
    return this.selectedScheduleType == EScheduleConfigType.CUSTOM;
  };

  isValidEvery = function () {
    return this.every > 0;
  }

  isValidAtMM = function () {
    return this.atMM != null && this.atMM > -1 && this.atMM < 60;
  }

  isValidAtHH = function () {
    return this.isHourly() || (!this.isHourly() && this.atHH != null && this.atHH > -1 && this.atHH < 24);
  }

  isValidDayOfTheWeek = function () {
    return !this.isWeekly() || (this.isWeekly() && this.dayNameOptions.filter(f => f.selected)[0]);
  }

  isValidCustom = function () {
    // TODO: add cron expression validation check
    return (!this.isCustom()) || (this.customCronConfig && this.customCronConfig.trim().length > 0);
  }

  isValid = function () {
    if (!this.scheduleConfig ||  !this.startingTimeHoursRange) {
      return true;
    }
    return this.isValidEvery() && this.isValidAtMM() && this.isValidAtHH() && this.isValidDayOfTheWeek();
  }


  getUserScheduleData = function () {
    var schedule: ScheduleConfigDto;
    if (this.isWeekly()) {
      schedule = <ScheduleConfigDto>{};
      schedule.scheduleType = EScheduleConfigType.WEEKLY;
      schedule.daysOfTheWeek = this.dayNameOptions.filter(f => f.selected).map(d => d.value);
    }
    else if (this.isDaily()) {
      schedule = <ScheduleConfigDto>{};
      schedule.scheduleType = EScheduleConfigType.DAILY;
    }
    else if (this.isHourly()) {
      schedule = <ScheduleConfigDto>{};
      schedule.scheduleType = EScheduleConfigType.HOURLY;
    }
    schedule.hour = this.isHourly() ? null : this.atHH;
    schedule.minutes = this.atMM;
    schedule.every = this.every;

    if (this.isValid()) {
      return schedule;
    }
    return null;
  }
}

import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {DialogBaseComponent} from "@shared-ui/components/modal-dialogs/base/dialog-component.base";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {BaseComponent} from "@core/base/baseComponent";
import {DTOPagingResults} from "@core/types/query-paging-results.dto";

@Component({
  selector: 'edit-server-component-dialog',
  templateUrl: './editServerComponentDialog.component.html',
  styleUrls: ['./editServerComponentDialog.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EditServerComponentDialogComponent extends BaseComponent  {

  inputText;
  description;
  data = <any>{};
  dataCenterList: any[] = null;

  constructor(private activeModal: NgbActiveModal, private serverComponentService : ServerComponentService) {
    super();
  }


  ngOnInit() {
    this.serverComponentService.getDatacenterListRequest().subscribe((dcList: DTOPagingResults<any>)=> {
      this.dataCenterList = dcList.content;
    });
  }



  save(e){
    this.activeModal.close(this.data);
  };


  cancel(e){
    this.activeModal.close(null);
  };

}

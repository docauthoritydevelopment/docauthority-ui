import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {UtilService} from "@services/util.service";
import {PageTitleService} from "@services/page-title.service";
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ScanService} from "@app/features/scan/services/scan.service";
import {DaFilterService} from "@app/common/filters/services/da-filter.service";
import {MainLoaderService} from "@services/main-loader.service";
import {RoutingService} from "@services/routing.service";
import {MetadataService} from "@services/metadata.service";
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {BizListService} from "@app/features/scan/services/biz-list.service";


@Component({
  selector: 'business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BusinessListComponent extends BaseComponent{
  loading:boolean = false;
  bizListMonitorParams = null;

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);


  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private dialogService: DialogService,
              private daFilterServer:DaFilterService,
              private mainLoaderService:MainLoaderService,
              private metadataService: MetadataService,
              private pageTitleService: PageTitleService,
              private scanService: ScanService) {
    super();


    this.on(this.scanService.activeScanFilterChangeEvent$, ([filterField,filterStr])=> {
      this.routingService.updateParamList(this.route,
        [filterField], [filterStr]);
    })
  }

  daOnInit() {
    this.pageTitleService.setTitle('Business List');
    this.on(this.scanRouteManager.routeChangeEvent$, (eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    }));
    this.loadData();
  }

  loadData() {
    let  params = _.cloneDeep(this.scanRouteManager.getLatestQueryParams()) || {};
    this.bizListMonitorParams = params;
  }

  onBizListMonitorPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.page, pageNum);
  }

  onParametersChanged = (newParamsObject : {keys : string[] , values : string[]}) => {
    this.routingService.updateParamList(this.route, newParamsObject.keys, newParamsObject.values);
  }
}

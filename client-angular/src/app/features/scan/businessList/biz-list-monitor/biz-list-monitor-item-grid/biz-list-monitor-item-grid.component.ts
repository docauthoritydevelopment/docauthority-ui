import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {Router} from '@angular/router';
import {BizListService} from "@app/features/scan/services/biz-list.service";
import {BizListSummaryInfo} from "@app/common/scan/types/scan.dto";
import {EJobState} from "@app/common/scan/types/scanEnum.types";



@Component({
  selector: 'biz-list-monitor-item-grid',
  templateUrl: './biz-list-monitor-item-grid.component.html'
})
export class BizListMonitorItemGridComponent extends TreeGridInnerItemBaseComponent<BizListSummaryInfo> implements OnInit {

  @ViewChild('el') el: ElementRef;
  EJobState = EJobState;
  jobStateDescription = '';
  jobStateStr:string;
  totalExtractItems: number  ;
  currentExtractItems ;
  allBizListsRunning:boolean = false;

  constructor(private router: Router, private bizListService: BizListService) {
    super();
  }


  ngOnInit(): void {
    if (this.hostComponentParams) {
      this.allBizListsRunning = this.hostComponentParams.isRunning;
    }
    if (this.dataItem.item.runJob) {
      if (this.dataItem.item.runJob.stateDescription) {
        this.jobStateDescription = this.dataItem.item.runJob.stateDescription;
      }

      if (this.dataItem.item.runJob.jobState) {
        this.jobStateStr = this.bizListService.getJobState(this.dataItem.item.runJob);
      }
      if (this.dataItem.item.runJob.phaseMaxMark) {
        this.totalExtractItems = this.dataItem.item.runJob.phaseMaxMark;
      }
      if (this.dataItem.item.runJob.phaseCounter) {
        this.currentExtractItems = this.dataItem.item.runJob.phaseCounter;
      }
    }
  }

  viewBizList() {
    this.router.navigate(['/scan/configuration/bizLists'],{
      queryParams : {selectedListId : this.dataItem.item.bizListDto.id
      }
    });
  }

  setSingleBizlistActive(isActive:boolean) {
    this.bizListService.setSingleBizListActiveState(this.dataItem.item.bizListDto.id, isActive);
  }

}


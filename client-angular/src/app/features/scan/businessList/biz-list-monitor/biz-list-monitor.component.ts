import {Component,  ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridConfig,
  TreeGridData, TreeGridDataItem, TreeGridSelectionMode
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {BaseComponent} from "@core/base/baseComponent";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {EJobCompletionStatus, EJobState, EJobType, ERunStatus} from "@app/common/scan/types/scanEnum.types";
import {BizListMonitorItemGridComponent} from "@app/features/scan/businessList/biz-list-monitor/biz-list-monitor-item-grid/biz-list-monitor-item-grid.component";
import {BizListMonitorGridHeaderComponent} from "@app/features/scan/businessList/biz-list-monitor/biz-list-monitor-grid-header/biz-list-monitor-grid-header.component";
import {BizListService} from "@app/features/scan/services/biz-list.service";
import {Router} from '@angular/router';
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig, ExportType} from "@app/common/configuration/common-config";
import {BizListScheduleRunStatus, BizListSummaryInfo, CrawlRunDetailsDto} from "@app/common/scan/types/scan.dto";
import {AppUrls} from "@core/types/app-urls";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ServerExportService} from "@services/serverExport.service";

@Component({
  selector: 'biz-list-monitor',
  templateUrl: './biz-list-monitor.component.html',
  styleUrls: ['./biz-list-monitor.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BizListMonitorComponent extends BaseComponent{

  currentBLMonitorData: TreeGridData<BizListSummaryInfo>;
  totalBLMonitorItems: number = 0 ;
  loading: boolean = false;
  treeGridEvents: ITreeGridEvents<any>;
  _params: any = null;
  bizListSummary: BizListScheduleRunStatus = {};
  latestRun:CrawlRunDetailsDto;
  treeParameters:any  = null;
  exportDropDownItems:DropDownConfigDto[] = [];
  CommonConfig = CommonConfig;
  isTechSupport: boolean = false;

  isRunning:boolean = false;
  isFinished:boolean = false;
  isPaused: boolean = false;

  BizListMonitorItemGridComponent = BizListMonitorItemGridComponent;
  BizListMonitorGridHeaderComponent = BizListMonitorGridHeaderComponent;
  TreeGridSelectionMode = TreeGridSelectionMode;

  @Output() pageChanged = new EventEmitter();
  @Output() parametersChanged = new EventEmitter();

  @Input()
  set params(newParams:any) {
    this._params = newParams;
    this.loadData(true);
  }

  get params(): any {
    return this._params;
  }

  treeConfig = <TreeGridConfig>{
    disableSelection:true
  };

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);



  exportBLMonitorData = ()=> {
    this.serverExportService.prepareExportFile('Business_lists',ExportType.BIZ_LIST_MONITOR,this.params,{
      itemsNumber : this.totalBLMonitorItems
    },this.totalBLMonitorItems> CommonConfig.minimum_items_to_open_export_popup);
  }

  bLMonitorDropDownItems = [];

  constructor (private serverExportService : ServerExportService, private bizListService : BizListService, private dialogService: DialogService, private router:Router,private subHeaderService: SubHeaderService, private i18nService : I18nService,private urlAuthorizationService : UrlAuthorizationService) {
    super();
    this.on(this.bizListService.bizListChangedEvent$, () => {
      this.loadData(true);
    });

  }


  daOnInit() {
    this.isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    this.subHeaderService.setSubHeader(this.i18nService.translateId('BIZ_LIST_MON.SUB_HEADER'));
    this.exportDropDownItems = [{
      title: this.i18nService.translateId('EXPORT_TO_EXCEL'),
      iconClass: "fa fa-download",
      actionFunc: this.exportBLMonitorData
    }];
    this.treeGridEvents = {
      onPageChanged: this.onBLMonitorPageChanged
    };
  }


  private onBLMonitorPageChanged = (pageNum) => {
    this.pageChanged.emit(pageNum);
  }

  compStatusSeverityArr = [
    EJobCompletionStatus.OK,
    EJobCompletionStatus.CANCELLED,
    EJobCompletionStatus.STOPPED,
    EJobCompletionStatus.SKIPPED,
    EJobCompletionStatus.UNKNOWN,
    EJobCompletionStatus.TIMED_OUT,
    EJobCompletionStatus.DONE_WITH_ERRORS,
    EJobCompletionStatus.ERROR
  ];

  loadData = (withLoader:boolean = false)=>  {
    if (this.params != null) {
      if (withLoader) {
        this.showLoader(true);
      }
      let queriesArray = [];
      queriesArray.push(this.bizListService.getBizListMonitorTableRequest(this.params));
      queriesArray.push(this.bizListService.getBizListRunsRequest());
      queriesArray.push(this.bizListService.getBizListStateRequest());
      forkJoin(queriesArray).subscribe(([bLMonitorTable,runObj,blState]) => {
        if (runObj!= null ) {
          this.bizListSummary = blState;
          this.latestRun = runObj.lastRun;

          this.isRunning = (this.latestRun && this.latestRun.runOutcomeState == ERunStatus.RUNNING);
          this.treeParameters = {isRunning: this.isRunning};
          this.isFinished = (!this.latestRun || (this.latestRun && (this.latestRun.runOutcomeState == ERunStatus.FINISHED_SUCCESSFULLY || this.latestRun.runOutcomeState == ERunStatus.FAILED || this.latestRun.runOutcomeState == ERunStatus.NA || this.latestRun.runOutcomeState == ERunStatus.STOPPED )));
          this.isPaused = (this.latestRun && this.latestRun.runOutcomeState == ERunStatus.PAUSED);
          let jobsByBizListIdMap = {};
          if (runObj.jobList) {
            runObj.jobList.forEach((jobItem: any) => {
              let keepJob = jobItem;
              if (jobsByBizListIdMap[jobItem.bizListId]) {
                    let prevJobItem: any = jobsByBizListIdMap[jobItem.bizListId];
                    if (prevJobItem.jobState != jobItem.jobState ) {
                      if (prevJobItem.jobState == EJobState.DONE) {
                        keepJob = jobItem;
                      }
                      else {
                        keepJob = prevJobItem;
                      }
                    }
                    else {
                      if (prevJobItem.jobState != EJobState.DONE  || prevJobItem.completionStatus == jobItem.completionStatus) {
                        if (prevJobItem.jobType == EJobType.EXTRACT) {
                          keepJob = prevJobItem;
                        }
                        else {
                          keepJob = jobItem;
                        }
                      }
                      else {
                        if (this.compStatusSeverityArr.indexOf(prevJobItem.completionStatus) > this.compStatusSeverityArr.indexOf(jobItem.completionStatus)) {
                          keepJob = prevJobItem;
                        }
                        else {
                          keepJob = jobItem;
                        }
                      }
                    }
              }
              jobsByBizListIdMap[jobItem.bizListId] = keepJob;
            });
          }
          this.currentBLMonitorData = bLMonitorTable;
          this.totalBLMonitorItems = bLMonitorTable.totalElements;
          if (bLMonitorTable.items.length > 0) {
            bLMonitorTable.items.forEach((dataItem: TreeGridDataItem<BizListSummaryInfo>) => {
              if (jobsByBizListIdMap[dataItem.item.bizListDto.id]) {
                dataItem.item.runJob = jobsByBizListIdMap[dataItem.item.bizListDto.id];
              }
            });
          }
          this.showLoader(false);
          console.log('before refresh '+ (new Date()));
          this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_PERIOD)
        }
      });
    }
  }

  runAll = ()=> {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.isFinished ? this.i18nService.translateId("BIZ_LIST_MON.CONFIRM.START_EXTRACT") : this.i18nService.translateId("BIZ_LIST_MON.CONFIRM.RESUME_EXTRACT")).then(result => {
      if (result) {
        if (this.isFinished) {
          this.bizListService.runAllBizList();
        }
        else {
          this.bizListService.resumeAllBizList(this.latestRun.id);
        }
      }
    });
  }

  stopAll = ()=> {
    var dlg = this.dialogService.openConfirm('Confirmation', 'Do you want to stop extraction business lists? \n').then(result => {
      if (result) {
        this.bizListService.stopAllBizList(this.latestRun.id);
      }
    });
  }

  pauseAll = ()=> {
    var dlg = this.dialogService.openConfirm(this.i18nService.translateId('CONFIRMATION'), this.i18nService.translateId("BIZ_LIST_MON.CONFIRM.PAUSE_EXTRACT")).then(result => {
      if (result) {
        this.bizListService.pauseAllBizList(this.latestRun.id);
      }
    });
  }

  addBizList = ()=> {
    this.router.navigate([AppUrls.BIZ_LIST_SETTINGS],{
      queryParams : {
      }
    });
  }

  setBizListActive(isActive:boolean) {
    this.bizListService.setAllBizListActiveState(isActive);
  }

}

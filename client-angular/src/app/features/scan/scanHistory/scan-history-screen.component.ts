import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'
import {TreeGridSortOrder} from '@tree-grid/types/tree-grid.type';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {I18nService} from "@app/common/translation/services/i18n-service";


@Component({
  selector: 'scan-history-screen',
  templateUrl: './scan-history-screen.component.html',
  styleUrls: ['./scan-history-screen.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScanHistoryScreenComponent extends BaseComponent {

  initialSortDirection: TreeGridSortOrder = null;
  initialSortFieldName: string = null;

  showFilter: boolean = true;
  storageKey = 'ACTIVE_SCANS';
  loading: boolean = false;
  inAutomaticRefresh: boolean = true;
  sHistoryParams: any = null;
  pageBreadCrumbs:any[] = [{title: 'Scan history'}];

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private i18nService: I18nService,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();


    this.on(this.scanService.activeScanFilterChangeEvent$, ([filterField, filterStr]) => {
      this.routingService.updateParamList(this.route,[filterField], [filterStr]);
    })
  }


  daOnInit() {
    this.pageTitleService.setTitle('Scan history');
    this.on(this.scanRouteManager.routeChangeEvent$, (eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData(true);
      }
    }));
    const params: any = this.scanRouteManager.getLatestQueryParams();
    this.initialSortFieldName = params.sortField ? params.sortField : null;
    this.initialSortDirection = params.sortDesc ? params.sortDesc : null;
    this.loadData(true);
  }

  chooseRoot = ()=> {
    this.routingService.updateParamList(this.route, [ScanPathParams.rootFolderName,ScanPathParams.rootFolderId],['','']);
  }

  onSetAutomaticRefresh() {
    this.inAutomaticRefresh = !this.inAutomaticRefresh;
  }


  loadData(withLoader: boolean = false) {
    if (withLoader) {
      this.showLoader(true);
    }
    let params = _.cloneDeep(this.scanRouteManager.getLatestQueryParams()) || {};
    if (params.rootFolderName) {
      this.pageBreadCrumbs = [{title: this.i18nService.translateId('HISTORY.MAIN_TITLE')
        , clickFunc : this.chooseRoot},
        {title: params.rootFolderName}];
    }
    else {
      this.pageBreadCrumbs = [{title: this.i18nService.translateId('HISTORY.MAIN_TITLE')}];
    }
    if (params[ScanPathParams.shPage]) {
      params[ScanPathParams.page] = params[ScanPathParams.shPage];
    }
    this.sHistoryParams = params;
  }


  onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.shPage, pageNum);
  }


  onParametersChanged = (newParamsObject: { keys: string[], values: string[] }) => {
    this.routingService.updateParamList(this.route, newParamsObject.keys, newParamsObject.values);
  }
}

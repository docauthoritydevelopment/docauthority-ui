import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {UtilService} from '@services/util.service';
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {CenterActiveScansComponent} from "@app/features/scan/scanCenter/centerActiveScans/center-active-scans.component";
import {SystemComponentGridComponent} from "@app/features/scan/systemComponents/item-grid/system-component-grid.component";
import {forkJoin} from 'rxjs';
import {map, concatMap} from 'rxjs/operators';
import {TreeGridConfig} from "@tree-grid/types/tree-grid.type";


@Component({
  selector: 'system-components',
  templateUrl: './system-components.component.html',
  styleUrls: ['./system-components.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SystemComponentsComponent extends BaseComponent{

  @ViewChild(CenterActiveScansComponent)
  treeGridEvents = null;
  SystemComponentGridComponent= SystemComponentGridComponent;
  currentData =[];
  isExporting:boolean = false;
  loading:boolean = false;


  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();
  }

  treeConfig =  <TreeGridConfig> {
    enableSwitchSelectionMode:true,
    disableSelection:true
  };


  daOnInit() {
    this.treeGridEvents = {
    };

    this.on(this.scanRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData();
      }
    }));

    this.pageTitleService.setTitle('System Components');
    this.loadData();
  }


  loadData() {
    //this.mainLoaderService.setMainLoader(true);
    const params = this.scanRouteManager.getLatestQueryParams() || {};
    let queriesArray = [];
    queriesArray.push(this.scanService.getSystemComponentsRequest(params));
    forkJoin(queriesArray).pipe(map(([systemComponents]) => {
      this.currentData = systemComponents;
    })).subscribe();
  }

}

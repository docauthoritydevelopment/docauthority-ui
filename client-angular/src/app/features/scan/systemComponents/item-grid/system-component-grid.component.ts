import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';

@Component({
  selector: 'system-component-grid',
  templateUrl: './system-component-grid.component.html'
})
export class SystemComponentGridComponent extends TreeGridInnerItemBaseComponent<any> implements OnInit {

  @ViewChild('el') el: ElementRef;

  constructor() {
    super();
  }

  ngOnInit(): void {
  }
}


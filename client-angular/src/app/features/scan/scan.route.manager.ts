import {EventEmitter, Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';


declare var _: any;

export enum ScanRouteEvents {
  STATE_CHANGED
}

export enum ScanQueryParams {
  PAGE_NUM = 'page',
  BASE_FILTER_FIELD = 'baseFilterField',
  BASE_FILTER_VALUE = 'baseFilterValue',
  FILTER = 'filter'
}

@Injectable()
export class ScanRouteManager {
  latestQueryParams: any = null;
  private routeChangeEvent= new Subject<ScanRouteEvents>();
  routeChangeEvent$ = this.routeChangeEvent.asObservable();


  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.getRoute().subscribe((route) => {
      this.latestQueryParams = route.snapshot.queryParams;
      this.routeChangeEvent.next(ScanRouteEvents.STATE_CHANGED);
    });
  }


  getLatestQueryParams() {
    return this.latestQueryParams;
  }


  getRoute() {
    return this.router.events.pipe(
      filter(e => e instanceof NavigationEnd)
      , map(() => this.activatedRoute)
      , map((route: any ) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
  }
}


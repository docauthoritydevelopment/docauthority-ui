import {Component, Input, ViewEncapsulation} from '@angular/core';
import {EJobCompletionStatus, EJobState, ERunStatus} from "@app/common/scan/types/scanEnum.types";
import {PhaseDetailsDto} from "@app/common/scan/types/scan.dto";
import {environment} from "../../../../environments/environment";
import {ScanService} from "@app/features/scan/services/scan.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {I18nService} from "@app/common/translation/services/i18n-service";
import {UrlAuthorizationService} from "@services/url-authorization.service";
import {ESystemRoleName} from '@users/model/system-role-name';
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {ServerExportService} from "@services/serverExport.service";
import {ExportType} from "@app/common/configuration/common-config";



@Component({
  selector: 'last-scan-phases',
  templateUrl: './last-scan-phases.component.html',
  styleUrls: ['./last-scan-phases.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class LastScanPhasesComponent  {
  _jobList: PhaseDetailsDto[] = [];
  myEnvironment = environment;
  isExporting = false;
  exportDropDownItems: DropDownConfigDto[] = [];
  topOptionDropDownItems: DropDownConfigDto[] = null;
  additionalDropDownItems: DropDownConfigDto[] = [];
  isTechSupport:boolean = false;
  EJobState = EJobState;
  ERunStatus = ERunStatus;

  @Input()
  runId:number;


  @Input()
  runStatus:ERunStatus = null;


  @Input()
  exportFileName:string;


  @Input()
  set jobList(newList: any[]) {
    this._jobList = newList;
    this.updateJobListValues();
  }

  get jobList(): any[] {
    return this._jobList;
  }

  updateJobListValues() {
    if (this._jobList) {
      this._jobList.forEach(phase => {
        if (phase.phaseStart) {
          (<any>phase).phaseStart = new Date(phase.phaseStart);
        }
        if (phase.phaseLastStopTime) {
          (<any>phase).phaseEnd = new Date(phase.phaseLastStopTime);
        }
        this.scanService.updatePhaseStatus(phase);
        var endTime = (<any>phase).isRunning ? Date.now() : (<PhaseDetailsDto>phase).phaseLastStopTime;
        (<any>phase).lastRunElapsedTime = endTime ? Math.floor(endTime - (<PhaseDetailsDto>phase).phaseStart - phase.pauseDuration) : null;
        (<PhaseDetailsDto>phase).taskRetriesCount = (<PhaseDetailsDto>phase).taskRetriesCount == 0 ? null : (<PhaseDetailsDto>phase).taskRetriesCount;
        (<any>phase).lastRunElapsedTimeSpan = (<PhaseDetailsDto>phase).phaseStart && (<any>phase).lastRunElapsedTime ? (<any>phase).lastRunElapsedTime : '';
      });
    }
  }


  getDropDownItems = (job) => {
    let ans:any[] = [{
      title: "Force job enqueued ...",
      toolTip: "Force selected job enqueued tasks back to 'new state'",
      actionFunc : this.forceJobEnquedTasks
    }];


    if (job.jobState == EJobState.IN_PROGRESS || job.jobState == EJobState.READY) {
      ans.push({});
      ans.push({
        title: "Pause job",
        actionFunc : this.pauseJob
      });
    }
    else if (job.jobState == EJobState.PAUSED || job.jobState == EJobState.DONE) {
      ans.push({});
      ans.push({
        title: "Resume job",
        actionFunc : this.resumeJob
      });
    }
    return ans;
  };


  ngOnInit() {
    this.isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
    this.exportDropDownItems = [{
      title: "Export to Excel",
      iconClass : "fa fa-download",
      actionFunc : this.exportRunData
    }];
    if (this.isTechSupport) {
      this.topOptionDropDownItems = [];
      this.topOptionDropDownItems.push({
        title: "Force all enqueued tasks",
        actionFunc : this.forceAllEnquedTasks
      });
    }
  }


  constructor(private serverExportService : ServerExportService,private scanService : ScanService, private dialogService: DialogService, private i18nService : I18nService, private urlAuthorizationService: UrlAuthorizationService) {

  }

  exportRunData = ()=> {
    this.serverExportService.prepareExportFile('Run_jobs',ExportType.JOB_LIST,{runId : this.runId},{},false);
  }

  forceAllEnquedTasks = ()=> {
    var dlg = this.dialogService.openConfirm('Confirmation', 'Are you sure you want to force re-run enqueued tasks for: '+this.exportFileName).then(result => {
      if (result) {
        this.scanService.forceRerunRunEnqueuedTasks(this.runId);
      }
    });
  }

  forceJobEnquedTasks = ([jobId, jobName])=> {
    var dlg = this.dialogService.openConfirm('Confirmation', 'Are you sure you want to force re-run enqueued tasks for job: '+this.i18nService.translateId(jobName)).then(result => {
      if (result) {
        this.scanService.forceRerunJobEnqueuedTasks(jobId);
      }
    });
  }

  pauseJob = ([jobId, jobName])=> {
    this.scanService.pauseJob(jobId);
  }

  resumeJob = ([jobId, jobName])=> {
    this.scanService.resumeJob(jobId);
  }

}


import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash'
import {RoutingService} from '@services/routing.service';
import {PageTitleService} from '@services/page-title.service';
import {ScanRouteEvents, ScanRouteManager} from "@app/features/scan/scan.route.manager";
import {ScanService} from "@app/features/scan/services/scan.service";
import {BaseComponent} from "@core/base/baseComponent";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";



@Component({
  selector: 'server-component-screen',
  templateUrl: './serverComponentScreen.component.html',
  styleUrls: ['./serverComponentScreen.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ServerComponentScreenComponent extends BaseComponent{

  loading: boolean = false;
  breadCrumbslist:any[] = [];
  sComponentParams:any = null;
  inAutomaticRefresh: boolean = true;

  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);

  constructor(private scanRouteManager: ScanRouteManager,
              private routingService: RoutingService,
              private route: ActivatedRoute,
              private scanService: ScanService, private pageTitleService: PageTitleService) {
    super();


    this.on(this.scanService.activeScanFilterChangeEvent$, ([filterField,filterStr])=> {
      this.routingService.updateParamList(this.route,[filterField], [filterStr]);
    })
  }


  daOnInit() {
    this.pageTitleService.setTitle('Server components');
    this.on(this.scanRouteManager.routeChangeEvent$,(eventName => {
      if (eventName === ScanRouteEvents.STATE_CHANGED) {
        this.loadData(true);
      }
    }));


    const params: any = this.scanRouteManager.getLatestQueryParams();
    this.loadData(true);
  }



  loadData(withLoader:boolean = false) {
    if (withLoader){
      this.showLoader(true);
    }
    let  params = _.cloneDeep(this.scanRouteManager.getLatestQueryParams()) || {};
    this.breadCrumbslist = [
      {
        title : 'Server Components'
      }];
    if (params[ScanPathParams.scPage]) {
      params[ScanPathParams.page] = params[ScanPathParams.scPage];
    }
    this.sComponentParams = params;
  }

  onPageChanged = (pageNum) => {
    this.routingService.updateParam(this.route, ScanPathParams.scPage, pageNum);
  }


  onParametersChanged = (newParamsObject : {keys : string[] , values : string[]}) => {
    this.routingService.updateParamList(this.route, newParamsObject.keys, newParamsObject.values);
  }

  onSetAutomaticRefresh() {
    this.inAutomaticRefresh = !this.inAutomaticRefresh;
  }
}

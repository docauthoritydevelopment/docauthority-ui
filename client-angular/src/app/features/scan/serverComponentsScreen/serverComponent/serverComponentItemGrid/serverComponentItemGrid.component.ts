import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from '../../../../../shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ClaComponentStatusDto, ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";
import {ServerComponentHistoryDialogComponent} from "@app/features/scan/popups/serverComponentHistoryDialog/serverComponentHistoryDialog.component";
import {EditServerComponentDialogComponent} from "@app/features/scan/popups/editServerComponentDialog/editServerComponentDialog.component";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import * as _ from 'lodash';
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";


@Component({
  selector: 'server-component-item-grid',
  templateUrl: './serverComponentItemGrid.component.html'
})
export class ServerComponentItemGridComponent extends TreeGridInnerItemBaseComponent<ClaComponentSummaryDto> implements OnInit {

  @ViewChild('el') el: ElementRef;
  additionalDropDownItems:DropDownConfigDto[] = null;


  constructor(private dialogService: DialogService, private serverComponentService : ServerComponentService) {
    super();
  }

  ngOnInit(): void {
    if (this.dataItem.item.isAppServer) {
      this.additionalDropDownItems = [
        {
          title: "Get Logs",
          iconClass: "fa fa-wrench",
          actionFunc: this.getLogs
        },
        {
          title: "Get Error Logs",
          iconClass: "fa fa-wrench",
          actionFunc: this.getErrorLogs
        }]
    }
    else if (this.dataItem.item.isBrokerRC) {
      this.additionalDropDownItems = [
        {
          title: "Open ActiveMQ console",
          iconClass: "fa fa-envira",
          actionFunc: this.openActiveMq
        }]
    }
    else if (this.dataItem.item.isSolrNode) {
      this.additionalDropDownItems = [
        {
          title: "Get Logs",
          iconClass: "fa fa-wrench",
          actionFunc: this.getSolrNodeLogs
        }]
    }
    else if (this.dataItem.item.isMediaProcessor) {
      this.additionalDropDownItems = [
        {
          title: "Edit",
          iconClass: "fa fa-pencil",
          actionFunc: this.editMP
        },
        {
          title: "Get Logs",
          iconClass: "fa fa-wrench",
          actionFunc: this.getMPLogs
        }]
    }
  }

  editMP = ()=> {
    this.serverComponentService.getSercerComponentRequest(this.dataItem.item.elementId).subscribe((mpObject)=>
    {
      this.dialogService.openModalPopup(EditServerComponentDialogComponent, {data: _.cloneDeep(mpObject)}).result.then((result: ClaComponentStatusDto) => {
        if (result) {
          this.serverComponentService.editServerComponent(result);
        }
      }, (reason) => {
      });
    });
  }


  getMPLogs = ()=>  {
    document.location.href = '/api/sys/components/logs/fetchMP/'+ this.dataItem.item.claComponentDto.instanceId;
  }

  getSolrNodeLogs = ()=> {
    document.location.href = '/api/sys/components/logs/component/'+ this.dataItem.item.claComponentDto.id ;
  }


  openActiveMq = ()=>  {
    window.open('/active-mq-url','_blank');
  }

  getLogs = ()=>  {
    document.location.href = '/api/logs/fetch';
  }

  getErrorLogs = ()=>  {
    document.location.href = '/api/logs/fetchErrMon';
  }

  openMoreData = ()=> {
    this.dialogService.openModalPopup(ServerComponentHistoryDialogComponent,{data : {openComponent : this.dataItem.item}} ,{size : 'lg'});
  }
}

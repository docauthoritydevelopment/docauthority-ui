import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridConfig,
  TreeGridData,
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {BaseComponent} from "@core/base/baseComponent";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {ServerComponentItemGridComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentItemGrid/serverComponentItemGrid.component";
import {breadcrumbItem, ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";
import {ServerComponentSummary} from "@app/features/scan/types/scan.types";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {DateRangeDialog} from "@app/common/dialogs/dateRangeDialog/date-range-dialog";
import {ScanPathParams} from "@app/features/scan/types/scan-route-pathes";
import {DateDialog} from "@app/common/dialogs/dateDialog/date-dialog";
import enumerate = Reflect.enumerate;
import {SubHeaderService} from "@services/subHeader.service";
import {CommonConfig} from "@app/common/configuration/common-config";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";


@Component({
  selector: 'server-component',
  templateUrl: './serverComponent.component.html',
  styleUrls: ['./serverComponent.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ServerComponentComponent extends BaseComponent {

  currentScData: TreeGridData<ClaComponentSummaryDto>;
  serverComponentsSummary:ServerComponentSummary;
  totalSCompItems = 0;
  loading = false;
  isExporting = false;
  scTreeGridEvents: ITreeGridEvents<ClaComponentSummaryDto>;
  _params: any  = null;
  exportDropDownItems: DropDownConfigDto[] = null;
  ServerComponentItemGridComponent = ServerComponentItemGridComponent;
  CommonConfig = CommonConfig;

  treeConfig: any = <TreeGridConfig>{
    disableSelection:true
  }


  @Output() pageChanged = new EventEmitter();
  @Output() parametersChanged = new EventEmitter();
  @Output() switchAutomaticRefresh = new EventEmitter();

  @Input()
  filterBySearchDateTime: number = null;

  @Input()
  inAutomaticRefresh: boolean;

  @Input()
  breadCrumbs: breadcrumbItem[] = [];

  @Input()
  set params(newParams: any) {
    this._params = newParams;
    this.loadData(true);
  }

  get params(): any {
    return this._params;
  }


  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);


  exportSCompData = ()=> {
    this.isExporting = true;
    this.daSubscription(this.serverComponentService.exportServerComponentData(this.totalSCompItems,this.params).subscribe(() => {
      this.isExporting = false;
    }));
  }

  sCompDropDownItems: DropDownConfigDto[] = [];

  constructor (private serverComponentService: ServerComponentService, private dialogService: DialogService, private subHeaderService : SubHeaderService) {
    super();
  }


  private buildDropDown() {
    this.scTreeGridEvents = {
      onPageChanged: this.onScPageChanged
    };
  }

  daOnInit() {
    this.exportDropDownItems = [{
      title: "Export to Excel",
      iconClass: "fa fa-download",
      actionFunc: this.exportSCompData
    }];
    this.subHeaderService.setSubHeader("Scan > Server components");
    this.buildDropDown();
    this.loadData();
    this.on(this.serverComponentService.serverComponentChangedEvent$, () => {
      this.loadData(true);
    });
  }


  private onScPageChanged = (pageNum) => {
    this.pageChanged.emit(pageNum);
  }


  daOnDestroy() {
  }


  loadData =(withLoader:boolean = false) => {
    if (withLoader) {
      this.showLoader(true);
    }
    let queriesArray = [];
    this.filterBySearchDateTime = null;
    if (this.params[ScanPathParams.timestamp] != null) {
      this.filterBySearchDateTime = Number(this.params[ScanPathParams.timestamp]);
    }
    queriesArray.push(this.serverComponentService.getServerComponentTableRequest(this.params,this.filterBySearchDateTime));
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([[scTable,sComponentStatusSummary]]) => {
      this.serverComponentsSummary = sComponentStatusSummary;
      this.currentScData= scTable;
      this.totalSCompItems= scTable.totalElements;
      this.showLoader(false);
      if (this.inAutomaticRefresh) {
        this.startTimeOut(this.loadData,CommonConfig.SCREENS_REFRESH_PERIOD);
      }
    }));
  }


  clearDate() {
    this.parametersChanged.emit(
      {
        keys: [ScanPathParams.timestamp],
        values: ['']
      });
  }

  chooseDates() {
    this.dialogService.openModalPopup(DateDialog,{data : {date : this.filterBySearchDateTime}}, {size: 'sm'}).result.then((result : any) =>
    {
      if (result != null) {
        this.parametersChanged.emit(
          {
            keys: [ScanPathParams.timestamp],
            values: [result.date]
          });
      }
    }, (reason) => {
    });
  }

}

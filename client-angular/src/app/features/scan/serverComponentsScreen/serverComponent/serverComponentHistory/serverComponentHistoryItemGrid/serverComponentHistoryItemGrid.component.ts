import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {ClaComponentStatusDto, ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {ServerComponentColumnObject} from "@app/features/scan/types/scan.types";
import {CommonConfig} from "@app/common/configuration/common-config";

@Component({
  selector: 'server-component-history-item-grid',
  templateUrl: './serverComponentHistoryItemGrid.component.html'
})
export class ServerComponentHistoryItemGridComponent extends TreeGridInnerItemBaseComponent<ClaComponentStatusDto> implements OnInit {

  @ViewChild('el') el: ElementRef;
  componentColumn: ServerComponentColumnObject = null;
  CommonConfig = CommonConfig;



  constructor(private serverComponentService:ServerComponentService) {
    super();
  }

  ngOnInit(): void {
    this.componentColumn = this.serverComponentService.getServerComponentColumnObject(this.hostComponentParams.componentType);
  }

}

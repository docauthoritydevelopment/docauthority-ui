import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TreeGridHeaderBaseComponent, TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {ClaComponentStatusDto, ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {ServerComponentColumnObject} from "@app/features/scan/types/scan.types";

@Component({
  selector: 'server-component-history-grid-header',
  templateUrl: './serverComponentHistoryGridHeader.component.html'
})
export class ServerComponentHistoryGridHeaderComponent extends TreeGridHeaderBaseComponent implements OnInit {

  @ViewChild('el') el: ElementRef;
  componentColumn: ServerComponentColumnObject = null;



  constructor(private serverComponentService:ServerComponentService) {
    super();
  }

  ngOnInit(): void {
    this.componentColumn = this.serverComponentService.getServerComponentColumnObject(this.hostHeaderParams.componentType);
  }

}

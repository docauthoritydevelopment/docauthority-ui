import {Component, ViewEncapsulation, Input} from '@angular/core';
import * as _ from 'lodash'
import {
  ITreeGridEvents, TreeGridData,
} from '@tree-grid/types/tree-grid.type';
import {forkJoin} from 'rxjs';
import {BaseComponent} from "@core/base/baseComponent";
import {ServerComponentService} from "@app/features/scan/services/serverComponent.service";
import {ClaComponentStatusDto, ClaComponentSummaryDto} from "@app/common/scan/types/scan.dto";
import {ServerComponentHistoryItemGridComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentHistory/serverComponentHistoryItemGrid/serverComponentHistoryItemGrid.component";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ServerComponentHistoryGridHeaderComponent} from "@app/features/scan/serverComponentsScreen/serverComponent/serverComponentHistory/serverComponentHistoryGridHeader/serverComponentHistoryGridHeader.component";
import {ChartService} from "@app/common/charts/services/chart.service";
import {ChartTypes} from "@app/common/charts/types/common-charts-enums";
import {CommonConfig} from "@app/common/configuration/common-config";
import {PrometheusService} from "@services/prometheus.service";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";


@Component({
  selector: 'server-component-history',
  templateUrl: './serverComponentHistory.component.html',
  styleUrls: ['./serverComponentHistory.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ServerComponentHistoryComponent extends BaseComponent {

  @Input()
  openComponent:ClaComponentSummaryDto = null;

  loading = false;
  isExporting = false;
  totalSCHompItems: number;
  currentSCHData: TreeGridData<ClaComponentStatusDto>;
  currentSCHChartSeries:any;

  schTreeGridEvents: ITreeGridEvents<ClaComponentSummaryDto>;
  _params: any  = null;
  exportDropDownItems: DropDownConfigDto[] = null;
  ServerComponentHistoryItemGridComponent = ServerComponentHistoryItemGridComponent;
  ServerComponentHistoryGridHeaderComponent = ServerComponentHistoryGridHeaderComponent;
  tablePage = 1;
  showInGraphMode:boolean = false;
  ChartTypes =  ChartTypes;

  treeConfig: any = {
    disableSelection:true
  }



  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);



  constructor (private serverComponentService: ServerComponentService, private activeModal: NgbActiveModal, private chartService: ChartService, private prometheusService : PrometheusService) {
    super();
  }


  private buildDropDown() {
    this.schTreeGridEvents = {
      onPageChanged: this.onSCHPageChanged
    };
  }

  exportSCHData() {

  }


  daOnInit() {
    this.exportDropDownItems = [{
      title: "Export to Excel",
      iconClass: "fa fa-download",
      actionFunc: this.exportSCHData
    }];
    this.buildDropDown();
    this.tablePage = 1;
    this.loadData();
  }


  private onSCHPageChanged = (pageNum) => {
    this.tablePage = pageNum;
    this.loadData(true);
  }


  daOnDestroy() {
  }


  loadData(withLoader:boolean = false) {
    let queriesArray = [];
    queriesArray.push(this.serverComponentService.getServerComponentHistoryTableRequest({page : this.tablePage},this.openComponent.elementId, this.openComponent.claComponentDto && this.openComponent.claComponentDto.customerDataCenterDto ? this.openComponent.claComponentDto.customerDataCenterDto.id : null));
    let now = new Date();
    /*
    queriesArray.push(this.prometheusService.queryByIndicator('fc_cpu',now.getTime()-(1000*60*60),now.getTime(),10));
    queriesArray.push(this.prometheusService.queryByIndicator('fc_memory',now.getTime()-(1000*60*60),now.getTime(),10));
    */
    this.showLoader(true);
    this.addQuerySubscription(forkJoin(queriesArray).subscribe(([scTable]) => {
      this.currentSCHData= scTable;
      scTable.items.forEach((dataItem)=> {
        dataItem.item.systemCpuLoadPercentage = dataItem.item.systemCpuLoad * 100 ;
      });
      scTable.items.sort((dataItem1,dataItem2)=> {
        return (dataItem1.item.timestamp - dataItem2.item.timestamp);
      });


      this.currentSCHChartSeries = this.chartService.convertTreeGridDataToChartSeriesData(scTable,'timestamp',[{
        id : 'memoryPercentage',
        name : 'Memory percentage',
        color : CommonConfig.INITIAL_HIGHCHARTS_COLORS[0]
      },
      {id : 'systemCpuLoadPercentage',
      name : 'Cpu percentage',
      color : CommonConfig.INITIAL_HIGHCHARTS_COLORS[4]}]);

      // this.currentSCHChartSeries = this.prometheusService.convertPrometheusResultToChartSeriesData([cpuResult,memoryResult],['fc_cpu','fc_memory'],[CommonConfig.INITIAL_HIGHCHARTS_COLORS[0],CommonConfig.INITIAL_HIGHCHARTS_COLORS[4]]);
      this.totalSCHompItems= scTable.totalElements;
      this.showLoader(false);
    }));
  }

  updateGraphMode(newMode) {
    if (this.showInGraphMode != newMode) {
      this.showInGraphMode = newMode;
    }
  }



}

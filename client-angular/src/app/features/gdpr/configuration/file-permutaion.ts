import {GDPRPersonDetailsField} from '@app/features/gdpr/configuration/gdpr-person-details-field';
import {PersonDetailsDiscoverPermutation} from '@app/features/gdpr/types/person-details-discover-permutation';
import {Observable, of} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';

const PermutationConfig: PersonDetailsDiscoverPermutation[] = [
  {keys: [GDPRPersonDetailsField.email, GDPRPersonDetailsField.lastName]},
  {keys: [GDPRPersonDetailsField.firstName, GDPRPersonDetailsField.lastName]},
  {keys: [GDPRPersonDetailsField.email, GDPRPersonDetailsField.phone]},
  {keys: [GDPRPersonDetailsField.id]}

];

export function getPermutationConfig(): Observable<PersonDetailsDiscoverPermutation[]> {
  return of(PermutationConfig).pipe(map(permutations => {
    permutations.forEach(item => item.selected = item.selected || item.required );
    return permutations;
  }));
}



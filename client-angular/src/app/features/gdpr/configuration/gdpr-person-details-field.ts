export const GDPRPersonDetailsField = {
  email: 'email',
  lastName: 'lastName',
  firstName: 'firstName',
  phone: 'phone',
  id: 'idNumber'
}

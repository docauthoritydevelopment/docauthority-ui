export const GDPRApi =  {
  getWorkflows : '/api/workflow/list',
  getWorkflow : '/api/workflow/:id',
  getWorkflowHistory : '/api/workflow/:id/history',
  getWorkflowsCountByState : '/api/workflow/countByState',
  getWorkflowActionsFileCount : '/api/workflow/:id/fileCountByAction',
  getWorkflowStateFileCount : '/api/workflow/:id/fileCountByState',
  saveWorkflow : '/api/workflow',
  updateWorkflow : '/api/workflow/:id',
  deleteWorkflows : '/api/workflow/deleteByIds',
  addWorkflowFilePopulation : '/api/workflow/:id/discover/filter',
  deleteWorkflowFilePopulation : '/api/workflow/:id/discover/filter/:filterId',
  clearWorkflowFilePopulations : '/api/workflow/:id/discover/filter/clear',
  workflowIdFilterName : 'workflowId=:id',
  setActionByFilter : '/api/workflow/:id/action/filter', // Url: http://localhost:9000/api/workflow/{id}/action/filter?filter={json_filter_encoded}
}

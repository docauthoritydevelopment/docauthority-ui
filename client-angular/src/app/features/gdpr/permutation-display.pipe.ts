import { Pipe, PipeTransform } from '@angular/core';
import {PersonDetailsDiscoverPermutation} from '@app/features/gdpr/types/person-details-discover-permutation';

@Pipe({
  name: 'permutationDisplay'
})
export class PermutationDisplayPipe implements PipeTransform {

  transform(value: PersonDetailsDiscoverPermutation): string {
    return value.keys.join(' and ');
  }

}

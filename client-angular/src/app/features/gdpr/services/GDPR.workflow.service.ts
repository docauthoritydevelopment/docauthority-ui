import {Injectable} from '@angular/core';
import {
  GDPRWorkflowDto,
  WorkflowFileDataDto,
  WorkflowFilterDescriptorDto,
  WorkflowHistoryDto,
  WorkflowState
} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs/index';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import * as _ from 'lodash';
import {GDPRApi} from '@app/features/gdpr/configuration/gdpr-api';
import {PersonDetailsDiscoverPermutation} from '@app/features/gdpr/types/person-details-discover-permutation';
import {getPermutationConfig} from '@app/features/gdpr/configuration/file-permutaion';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {FilterFactory} from '@core/services/filter.factory';
import {EFileAction} from '@app/features/gdpr/types/file-action.enum';
import {EFileState} from '@app/features/gdpr/types/file-state';
import {FilterService} from '@services/filter.service';
import {UrlParamsService} from '@services/urlParams.service';
import {SingleCriteria} from 'app/core/services/filter-types/single-criteria';
import {ICriteriaComponent} from 'app/core/services/filter-data';
import {OrCriteria} from 'app/core/services/filter-types/or-criteria';
import {catchError, first, map, switchMap, tap} from 'rxjs/internal/operators';


@Injectable()
export class GDPRWorkflowService {

  constructor(
    private http: HttpClient,
    private urlParamsService: UrlParamsService,
    private dialogService: DialogService,
    private filterService: FilterService,
    private filterFactory: FilterFactory) {

  }

  setFilter(permutations: PersonDetailsDiscoverPermutation[], workflowItem: GDPRWorkflowDto) {
    this.filterService.clearPageRestrictionFilter();
    const filter = this.buildFilter(permutations, workflowItem);
    if (filter !== null) {
      this.filterService.addPageRestrictionFilter(filter);
      const thinFilterCopy = this.filterService.getThinFilterCopy();
      return thinFilterCopy;
    }
    return null;
  }

  buildFilter(permutations: PersonDetailsDiscoverPermutation[], workflowItem: GDPRWorkflowDto): ICriteriaComponent {
    const selectedPermutations = permutations.filter(permutation => permutation.selected);
    const permutationCriteria = selectedPermutations.map(p => this.getPermutationCriteria(p, workflowItem));
    if (permutationCriteria.length === 0) {
      return null;
    }
    if (permutationCriteria.length === 1) {
      return permutationCriteria[0];
    } else {
      const firstPermutationCriteria = permutationCriteria.splice(0, 1);
      return new OrCriteria(firstPermutationCriteria, permutationCriteria);
    }
  }

  getPermutationCriteria(permutation: PersonDetailsDiscoverPermutation, workflowItem: GDPRWorkflowDto): SingleCriteria {
    const str = permutation.keys.map(key => workflowItem.requestDetails[key]).join(' * ');
    return this.filterFactory.createContentSearch()(str);
    //  return new AndCriteria(permutation.keys.map(key => this.filterFactory.createContentSearch()(this.workflowItem.requestDetails[key]),
    //  ));
  }

  addFilesToWorkflow(workflowItem: GDPRWorkflowDto, permutations: PersonDetailsDiscoverPermutation[]): Observable<any> {
    this.setFilter(permutations, workflowItem);
    if (this.filterService.getFilterData().hasCriteria()) {
      const filterData = new WorkflowFilterDescriptorDto();
      filterData.filter = this.filterService.toKendoFilter();
      filterData.workflowFileData = new WorkflowFileDataDto();
      filterData.workflowFileData.fileAction = EFileAction.NEW;
      const result = this.addFilesToWorkflowItem(workflowItem.id, filterData).pipe(first(), tap(res => {
          workflowItem.filters.push(filterData.filter);
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
          return Observable.throw('Server error');
        }));
      return result;
    }
    return throwError('Server error');
  }

  prerareWorkflowToResolve(workflowItem: GDPRWorkflowDto, permutations: PersonDetailsDiscoverPermutation[], comment?: string) {
    workflowItem.state = WorkflowState.GDPR_DISCOVER;
    return this.addFilesToWorkflow(workflowItem, permutations).pipe(switchMap(() => {
      return this.saveWorkflow(workflowItem, comment)
    }));
  }

  loadWorkflows(params, states?: WorkflowState[]): Observable<DTOPagingResults<GDPRWorkflowDto>> {
    params = this.urlParamsService.buildPagedParams(params);
    if (states && states[0]) {
      params.states = states.join(',');
    }
    return this.http.get<DTOPagingResults<GDPRWorkflowDto>>(GDPRApi.getWorkflows, {params})
      .pipe(map(data => {
        data.content.forEach(workflow => {
          // parse data fro the sake of type safety
          if (!workflow.workflowSummary) {
            return;
          }
          const actionKeys = <EFileAction[]>(Object.keys(workflow.workflowSummary.fileActionCount));
          const actionMap = new Map<EFileAction, number>();
          actionKeys.forEach(key => actionMap.set(key, workflow.workflowSummary.fileActionCount[key]));
          workflow.workflowSummary.fileActionCount = actionMap;

          const stateKeys = <EFileState[]>(Object.keys(workflow.workflowSummary.fileStateCount));
          const stateMap = new Map<EFileState, number>();
          stateKeys.forEach(key => stateMap.set(key, workflow.workflowSummary.fileStateCount[key]));
          workflow.workflowSummary.fileStateCount = stateMap;
        });
        return data;
      }), catchError(err => {
        console.error(err);
        return Observable.throw(err)
      }));
  }

  getWorkflowsCountByState(): Observable<Map<WorkflowState, number>> {
    return this.http.get<Map<WorkflowState, number>>(GDPRApi.getWorkflowsCountByState)
      .pipe(map(data => {
        // parse data fro the sake of type safety
        const keys = <WorkflowState[]>(Object.keys(data));
        const workflowStateMap = new Map<WorkflowState, number>();
        keys.forEach(key => workflowStateMap.set(key, data[key]));
        return workflowStateMap;
      }));
  }

  loadWorkflowItem(id: number): Observable<GDPRWorkflowDto> {

    if (id) {
      return this.http.get<GDPRWorkflowDto>(GDPRApi.getWorkflow.replace(':id', id.toString()))
        .pipe(tap(data => {
        }))
    }
    return new Observable(null)
  }

  loadWorkflowItemHistory(id: number): Observable<DTOPagingResults<WorkflowHistoryDto>> {

    if (id) {
      return this.http.get<DTOPagingResults<WorkflowHistoryDto>>(GDPRApi.getWorkflowHistory.replace(':id', id.toString()))
    }
    return new Observable(null)
  }

  getWorkflowActionsFileCount(id: number): Observable<Map<EFileAction, number>> {
    return this.http.get<Map<EFileState, number>>(GDPRApi.getWorkflowActionsFileCount.replace(':id', id.toString()))
      .pipe(map(data => {
        // parse data fro the sake of type safety
        const keys = <EFileAction[]>(Object.keys(data));
        const filesMap = new Map<EFileAction, number>();
        keys.forEach(key => filesMap.set(key, data[key]));
        return filesMap;
      }));
  }

  getWorkflowStateFileCount(id: number): Observable<Map<EFileState, number>> {
    return this.http.get<Map<EFileState, number>>(GDPRApi.getWorkflowStateFileCount.replace(':id', id.toString()))
      .pipe(map(data => {
        // parse data fro the sake of type safety
        const keys = <EFileState[]>(Object.keys(data));
        const fileStateMap = new Map<EFileState, number>();
        keys.forEach(key => fileStateMap.set(key, data[key]));
        return fileStateMap;
      }));
  }

  addFilesToWorkflowItem(workFlowId: number, filterData: WorkflowFilterDescriptorDto): Observable<GDPRWorkflowDto> {

    return this.http.put<GDPRWorkflowDto>(GDPRApi.addWorkflowFilePopulation.replace(':id', workFlowId.toString()), filterData);


  }

  updateWorkflowFileStatus(workFlowId: number, fileAction: EFileAction, fileState: EFileState,
                           kendoFilter: any, additionalFilterParams: any, comment: string): Observable<boolean> {
    const fileData = new WorkflowFileDataDto();
    fileData.fileAction = fileAction;
    fileData.fileState = fileState;
    const w = new WorkflowFilterDescriptorDto();
    w.workflowFileData = fileData;
    w.filter = kendoFilter ? JSON.stringify(kendoFilter) : null;
    const params = comment ? {comment: comment} : {};
    if (additionalFilterParams) {
      _.extend(params, additionalFilterParams);
    }

    return this.http.post<boolean>(GDPRApi.setActionByFilter.replace(':id', workFlowId.toString()), w, {params});
  }


  deleteAllWorkflowFiles(workFlowId: number): Observable<GDPRWorkflowDto> {

    return this.http.delete<GDPRWorkflowDto>(GDPRApi.clearWorkflowFilePopulations.replace(':id', workFlowId.toString()));


  }

  buildPermutations(workflowItem: GDPRWorkflowDto): Observable<PersonDetailsDiscoverPermutation[]> {
    return getPermutationConfig().pipe(first(), map(permutations => {
      permutations.forEach(permutation => permutation.selected = this.hasValue(workflowItem, permutation))
      return permutations;
    }));
  }

  hasValue(workflowItem: GDPRWorkflowDto, permutation: PersonDetailsDiscoverPermutation): boolean {
    return !permutation.keys.find(key => !workflowItem.requestDetails[key]);
  }

  saveWorkflow(workflow: GDPRWorkflowDto, comment: string): Observable<GDPRWorkflowDto> {
    const params = comment ? {comment: comment} : {};
    if (!workflow.id) {
      return this.http.put<GDPRWorkflowDto>(GDPRApi.saveWorkflow, workflow, {params});
    }

    return this.http.post<GDPRWorkflowDto>(GDPRApi.updateWorkflow.replace(':id', workflow.id.toString()), workflow, {params});
  }

  deleteWorkflows(workflowIds: number[]) {
    const params = {
      workflowIds: workflowIds.join(',')
    };
    return this.http.delete(GDPRApi.deleteWorkflows, {params})
      .pipe(tap(data => {
      }));
  }

}

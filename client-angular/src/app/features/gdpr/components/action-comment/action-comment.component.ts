import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'da-action-comment',
  templateUrl: './action-comment.component.html',
  styleUrls: ['./action-comment.component.scss']
})
export class ActionCommentComponent implements OnInit {

  showDialog: boolean;
  @Input() comment: string;
  @Input() buttonText: string;

  @Output() closeButtonClicked = new EventEmitter<boolean>();

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
  }

  onCommentClicked() {
    this.showDialog = true;
    const initialTop = 52;
    this.comment = '';
    const nativeElement = this.el.nativeElement as HTMLElement;
    const dialogElement =  nativeElement.querySelector('.action-dialog') as HTMLElement;
    dialogElement.style.top = `${nativeElement.offsetTop + initialTop}px`;
  }

  onCloseClicked(approved: boolean) {
    const target = window.event.target;
    const identifierAttribute = (target as HTMLElement).attributes['data-dialogopen'];
    if (identifierAttribute && identifierAttribute.value === 'true') {
      return;
    }

    this.showDialog = false;
    this.closeButtonClicked.emit(approved);
  }

}

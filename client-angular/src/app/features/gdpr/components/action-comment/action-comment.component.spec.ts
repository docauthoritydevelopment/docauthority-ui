import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionCommentComponent } from './action-comment.component';
import {FormsModule} from '@angular/forms';

describe('ActionCommentComponent', () => {
  let component: ActionCommentComponent;
  let fixture: ComponentFixture<ActionCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ ActionCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

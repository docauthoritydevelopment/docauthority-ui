import * as _ from 'lodash';
import {Component, Input, OnChanges} from '@angular/core';
import {WorkflowHistoryDto} from 'app/features/gdpr/types/GDPR.workflow.dto';
import {GDPRWorkflowService} from 'app/features/gdpr/services/GDPR.workflow.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {catchError, map} from 'rxjs/internal/operators';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {GDPRWorkflowDtoWrapper} from '@app/features/gdpr/types/gdpr-workflow-dto-wrapper';
import {RoutingService} from '@services/routing.service';

declare let $: any;

@Component({
  selector: 'gdpr-history-workflow-item',
  templateUrl: './GDPR-item-history.component.html'
})

export class GDPRItemHistoryComponent implements OnChanges {
  @Input() workflow: GDPRWorkflowDtoWrapper;
  @Input() workflowId: number;
  private ps;
  private historyList;
  private activityList;
  private loading = false;
  private selectedItem = null;
  private showDetailsPanel = false;
  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 300);

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private workflowService: GDPRWorkflowService) {
  }

  ngOnChanges() {
    this.show();
    this.loadData();
  }

  private show() {
    $('gdpr-history-workflow-item').show();
    this.showLoader(true);
    this.closeDetailsPanel();
  }

  private loadData() {
    this.workflowService.loadWorkflowItemHistory(this.workflowId)
      .pipe(catchError((err) => this.dialogService.notifyError(err)),
        map(this.parseData))
      .subscribe((data) => {
        this.historyList = data;
        this.setActivities();
      });
  }

  private setActivities() {
    this.activityList = _.extend([], this.historyList.content);
  }

  private parseData(data: DTOPagingResults<WorkflowHistoryDto>) {
    return data;
  }

  private openDetailsPanel(item) {
    this.selectedItem = item;
    _.defer(() => {
      this.showDetailsPanel = true;
    })
  }

  private closeDetailsPanel() {
    this.showDetailsPanel = false;
  }


  private hide() {
    $('gdpr-history-workflow-item').hide();
  }
}


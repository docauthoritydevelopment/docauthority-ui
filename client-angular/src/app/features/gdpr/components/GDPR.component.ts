import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PageTitleService} from '@core/services/page-title.service';
import {TranslateService} from "@ngx-translate/core";
import {gdprTranslate} from "@app/features/gdpr/gdpr.translate";


@Component({
  selector: 'gdpr',
  templateUrl: './GDPR.component.html',
  styleUrls: ['./GDPR.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class GDPRComponent implements OnInit {

  constructor(private pageTitleService: PageTitleService, private translateService : TranslateService) {
    translateService.setTranslation('en', gdprTranslate);
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }

  ngOnInit() {
    this.pageTitleService.setTitle('GDPR')
  }


}


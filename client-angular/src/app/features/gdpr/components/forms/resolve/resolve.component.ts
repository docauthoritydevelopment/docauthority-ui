import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {EFileAction, EFileState, GDPRWorkflowDto, WorkflowState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import * as _ from 'lodash';
import {HttpErrorResponse} from '@angular/common/http';
import {V1RouteStateName} from '@app/doc-1/angular-1-routes/model/v1.route.state.name';
import {RouteUrls} from '@app/types/routing.urls';
import {BaseFilterField, EntityDisplayTypes} from '@core/types/discover.entity.types';
import {JSONUtilities} from '@utilities/json-tryparse';
import {ECriteriaOperators} from '@services/filter-types/criteria-operators';
import {ResolveBtnActions, ResolveTabActions} from './resolve-actions';
import {FileStatesBtn, FileStatesTabs} from '@app/features/gdpr/components/forms/resolve/file-states';
import {FilterService} from '@services/filter.service';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {FilterFactory} from '@core/services/filter.factory';
import {UrlUtility} from '@utilities/UrlUtility';
import {Subject, Subscription, throwError} from 'rxjs/index';
import {catchError, first, map, skipWhile, switchMap} from 'rxjs/internal/operators';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';


declare let $: any;

@Component({
  selector: 'gdpr-resolve',
  templateUrl: './resolve.component.html',
})
export class ResolveComponent implements OnInit, OnDestroy {
  selectedActionBtn = null;
  selectedFileStateBtn = null;
  EFileAction = EFileAction; // this is in order to use enum on html
  WorkflowState = WorkflowState;
  isWorkflowActivitiesVisible = false;
  workflowItemSet: Subject<boolean> = new Subject<boolean>();
  showClearAll: false;
  readyForSubmitReadyForApproval = false;
  approvalComment = null;
  pendingComment = null;
  actionsFileCount: Map<EFileAction, number>;
  statesFileCount: Map<EFileState, number>;
  currentFilterAction: EFileAction;
  activePan: string;
  selectedItemRightEntity: EntityDisplayTypes;
  selectedItemLeftEntity: EntityDisplayTypes;
  actions = ResolveBtnActions;
  tabActions = ResolveTabActions;
  fileStatesBtn: any[] = FileStatesBtn;
  fileStatesTabs: any[] = FileStatesTabs;
  totalFiles: number;
  private actionDialogLeftPos;
  private fileStateDialogLeftPos;
  private currentActionData = {};
  private currentFileStateData = {};
  private showApprovalDialog = false;
  private selectedItemLeftId: string;
  private selectedItemRightId: string;
  private selectedWorkflowId: number;
  private subscriptions: Subscription;
  private onWindowResize = _.debounce(() => {
    this.setFramePosition();
  }, 0);

  constructor(private v1Service: V1Service,
              private route: ActivatedRoute,
              private workflowService: GDPRWorkflowService,
              private filterService: FilterService,
              private filterFactory: FilterFactory,
              private router: Router,
              private baseFilterField: BaseFilterField,
              private cd: ChangeDetectorRef,
              private dialogService: DialogService) {
  }

  private _workflowItem: GDPRWorkflowDto;

  get workflowItem(): GDPRWorkflowDto {
    return this._workflowItem;
  }

  set workflowItem(value: GDPRWorkflowDto) {
    const workflowEmpty = !this._workflowItem;
    this._workflowItem = value;
    if (workflowEmpty) {
      this.workflowItemSet.next(!!value);
    }

  }

  ngOnInit() {
    this.filterService.setFilterData(null);
    this.v1Service.setOnWindowResize(() => this.onWindowResize());

    this.route.data.pipe(first()).subscribe(() => {
      this.workflowItemSet.pipe(skipWhile(value => !value), first()).subscribe(() => {
        setTimeout(() => {
            this.discover();
            this.v1Service.setVisibility(true);
            this.setFramePosition()
          }
        );
      })
      this.initWorkflowItem();
    });
  }

  goToGrid() {
    const params = {};
    params[RoutePathParams.findId] = this.workflowItem.id;
    if (this.workflowItem.state === WorkflowState.GDPR_DISCOVER || this.workflowItem.state === WorkflowState.GDPR_RESOLVE) {
      this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.openWorkflows], {queryParams: params});
    } else if (this.workflowItem.state === WorkflowState.GDPR_PENDING_APPROVAL) {
      this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.approvalWorkflows], {queryParams: params});
    } else if (this.workflowItem.state === WorkflowState.GDPR_PENDING_ACTION) {
      this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.pendingActionWorkflows], {queryParams: params});
    }
  }


  discover() {

    const v1Params = {
      isEmbedded: true,
      left: this.workflowItem &&
      this.workflowItem.state === WorkflowState.GDPR_PENDING_ACTION ? EntityDisplayTypes.folders : EntityDisplayTypes.groups,
      right: EntityDisplayTypes.files,
      workflowId: this.selectedWorkflowId,
      readOnly: true
    };
    this.v1Service.changeFrameRoute(V1RouteStateName.discover, v1Params);

  }

  filterByFileAction(action: EFileAction) {
    const activeFilterIdentifier = 'GDPRActiveFilter';
    this.currentFilterAction = action;
    const actionFilterBuilder = this.filterFactory.createWorkflowFileAction(activeFilterIdentifier);
    const existing = this.filterService.findPageRestictionFilter(activeFilterIdentifier);
    existing.forEach(e => this.filterService.removePageRestrictionFilter(e));

    if (action) {
      const tagFilterCriteria = actionFilterBuilder(action, this.selectedWorkflowId);
      this.filterService.addPageRestrictionFilter(tagFilterCriteria);
      this.v1Service.setVisibility(true);
    }

    this.isWorkflowActivitiesVisible = false;
    const thinFilterCopy = this.filterService.getThinFilterCopy();
    this.v1Service.changeParamsOfCurrentRoute({filter: thinFilterCopy, isEmbedded: true});
  }

  showWorkflowActivities() {
    this.v1Service.setVisibility(false);
    this.isWorkflowActivitiesVisible = true;
  }

  toggleFilterByFileState(fileStateConfig) {
    const stateFilterIdentifier = 'GDPRStateFilter';
    fileStateConfig.active = !fileStateConfig.active;

    const existing = this.filterService.findPageRestictionFilter(stateFilterIdentifier);
    existing.forEach(e => this.filterService.removePageRestrictionFilter(e));

    const fileStatesFilterBuilder = this.filterFactory.createWorkflowFileStatesFilter(stateFilterIdentifier);

    const activeFileStates = this.fileStatesTabs.filter(s => s.active).map(f => f.state);
    if (activeFileStates[0]) {
      const tagFilterCriteria = fileStatesFilterBuilder(activeFileStates, this.selectedWorkflowId, ECriteriaOperators.or);
      this.filterService.addPageRestrictionFilter(tagFilterCriteria);
    }

    const thinFilterCopy = this.filterService.getThinFilterCopy();
    this.v1Service.changeParamsOfCurrentRoute({filter: thinFilterCopy, isEmbedded: true});
  }

  clearAllActions(comment: string) {
    this.workflowService.updateWorkflowFileStatus(
      this.workflowItem.id, EFileAction.NEW, EFileState.NONE, null, null, comment).subscribe(() => {
        this.showClearAll = false;
        this.initActionsFileCount();
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);
      }
    )
  }

  setAction(action: EFileAction, comment: string) {
    let additionalFilterParams = null;
    let useFilter = false;
    if (this.currentActionData['applyTo'] === this.selectedItemLeftEntity) {
      additionalFilterParams = {
        'baseFilterField': this.baseFilterField.findBy(this.selectedItemLeftEntity),
        'baseFilterValue': this.selectedItemLeftId
      };
    } else if (this.currentActionData['applyTo'] === this.selectedItemRightEntity) {
      additionalFilterParams = {
        'baseFilterField': this.baseFilterField.findBy(this.selectedItemRightEntity),
        'baseFilterValue': this.selectedItemRightId
      };
    } else {
      useFilter = true;
    }
    // TODO add includeSubEntityData, rootfolder to params
    this.workflowService.updateWorkflowFileStatus(this.workflowItem.id, action, EFileState.ACTION_PENDING_APPROVAL,
      useFilter ? this.filterService.toKendoFilter() : null, additionalFilterParams, comment)
      .subscribe(res => {
          this.v1Service.reload();
          this.initActionsFileCount();
          if (this.workflowItem.state === WorkflowState.GDPR_DISCOVER) {
            this.workflowItem.state = WorkflowState.GDPR_RESOLVE;
            this.workflowService.saveWorkflow(this.workflowItem, '').subscribe(saveWorkflowResult => {
                this.workflowItem = saveWorkflowResult;
              },
              (err: HttpErrorResponse) => {
                console.error('Failed to update status to resolve')
              }
            )
          }
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )
  }

  setFileState(fileState: EFileState, comment: string) {
    let additionalFilterParams = null;
    let useFilter = false;
    if (this.currentFileStateData['applyTo'] === this.selectedItemLeftEntity) {
      additionalFilterParams = {
        'baseFilterField': this.baseFilterField.findBy(this.selectedItemLeftEntity),
        'baseFilterValue': this.selectedItemLeftId
      };
    } else if (this.currentFileStateData['applyTo'] === this.selectedItemRightEntity) {
      additionalFilterParams = {
        'baseFilterField': this.baseFilterField.findBy(this.selectedItemRightEntity),
        'baseFilterValue': this.selectedItemRightId
      };
    } else {
      useFilter = true;
    }
    // TODO add includeSubEntityData, rootfolder to params
    this.workflowService.updateWorkflowFileStatus(this.workflowItem.id, null, fileState,
      useFilter ? this.filterService.toKendoFilter() : null, additionalFilterParams, comment)
      .subscribe(() => {
          this.initFileStateFileCount();
          this.v1Service.reload();
          // refresh fileState progress
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )
  }

  setPendingApproveRequest(comment: string) {
    this.showApprovalDialog = false;
    const workflow = _.clone(this.workflowItem);
    workflow.state = WorkflowState.GDPR_PENDING_APPROVAL;
    this.workflowService.saveWorkflow(workflow, comment).subscribe(res => {
        const params = {};
        params[RoutePathParams.findId] = workflow.id;
        this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.approvalWorkflows], {queryParams: params});
      },
      (err: HttpErrorResponse) => {

        this.dialogService.notifyError(err);

      }
    )
  }

  setApproveRequest(comment: string) {
    this.showApprovalDialog = false;

    this.workflowItem.state = WorkflowState.GDPR_PENDING_ACTION;
    this.workflowService.updateWorkflowFileStatus(
      this.workflowItem.id, null, EFileState.APPROVED, this.filterService.toKendoFilter(), null, comment)
      .subscribe(res => {
          const workflow = _.clone(this.workflowItem);
          workflow.state = WorkflowState.GDPR_APPROVED;
          this.workflowService.saveWorkflow(workflow, comment).subscribe(() => {
              const params = {};
              params[RoutePathParams.findId] = workflow.id;
              this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.approvalWorkflows], {queryParams: params});
            },
            (err: HttpErrorResponse) => {

              this.dialogService.notifyError(err);

            }
          )
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )


  }

  setApproveRequestAndReadyForAction(comment: string) {
    this.showApprovalDialog = false;


    this.workflowService.updateWorkflowFileStatus(this.workflowItem.id, null,
      EFileState.APPROVED, this.filterService.toKendoFilter(), null, comment)
      .subscribe(res => {
          const workflow = _.clone(this.workflowItem);
          workflow.state = WorkflowState.GDPR_PENDING_ACTION;
          this.workflowService.saveWorkflow(workflow, comment).subscribe(() => {
              const params = {};
              params[RoutePathParams.findId] = workflow.id;
              this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.pendingActionWorkflows], {queryParams: params});
            },
            (err: HttpErrorResponse) => {

              this.dialogService.notifyError(err);

            }
          )
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )


  }

  setReopenApproveRequest(comment: string) {
    this.showApprovalDialog = false;

    this.workflowService.updateWorkflowFileStatus(
      this.workflowItem.id, null, EFileState.DISAPPROVED, this.filterService.toKendoFilter(), null, comment)
      .subscribe(res => {
          const workflow = _.clone(this.workflowItem);
          workflow.state = WorkflowState.GDPR_RESOLVE;
          this.workflowService.saveWorkflow(workflow, comment).subscribe(() => {
              this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.openWorkflows]);
            },
            (err: HttpErrorResponse) => {

              this.dialogService.notifyError(err);

            }
          )
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )

  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  onActionButtonClicked(e, selectedAction) {
    _.defer(() => {
      this.resetActionDialog(e);
      this.selectedActionBtn = selectedAction;
    });
  }

  onActionDialogClickedOutside(e) {
    this.selectedActionBtn = null;
  }

  resetActionDialog(e) {
    this.actionDialogLeftPos = $(e.target).position().left;
    this.currentActionData['comment'] = '';
  }

  onActionDialogCloseClick() {
    this.selectedActionBtn = null;
  }

  onApprovalButtonClicked() {
    _.defer(() => {
      this.approvalComment = '';
      this.showApprovalDialog = true;
    });
  }

  onApprovalDialogClickedOutside() {
    this.showApprovalDialog = false;
  }

  onApprovalDialogCloseClick() {
    this.showApprovalDialog = false;
  }

  onFileStateButtonClicked(e, selectedState) {
    _.defer(() => {
      this.resetFileStateDialog(e);
      this.selectedFileStateBtn = selectedState;
    });
  }

  onFileStateDialogClickedOutside(e) {
    this.selectedFileStateBtn = null;
  }

  resetFileStateDialog(e) {
    this.fileStateDialogLeftPos = $(e.target).position().left;
    this.currentFileStateData['comment'] = '';
  }

  onFileStateDialogCloseClick() {
    this.selectedFileStateBtn = null;
  }

  private initActionsFileCount() {

    this.workflowService.getWorkflowActionsFileCount(this.selectedWorkflowId)
      .subscribe(data => {
        this.actionsFileCount = data;
        let totalFiles = 0;
        for (const v of Array.from(data.values())) {
          totalFiles = totalFiles + v;
        }
        this.totalFiles = totalFiles;
        this.readyForSubmitReadyForApproval = !data.get(EFileAction.NEW) /*&&  this.totalFiles >0*/;

        //    this.cd.markForCheck();
      }, (err) => {
        console.error('failed getWorkflowActionsFile' + err);
      })

  }

  private initFileStateFileCount() {

    this.workflowService.getWorkflowStateFileCount(this.selectedWorkflowId)
      .subscribe(data => {
        this.statesFileCount = data;
      }, (err) => {
        console.error('failed getWorkflowStateFileCount' + err);
      })

  }

  private initV1DiscoverPage() {

    this.subscriptions =
      this.v1Service.v1StateChanged$.subscribe(url => {
        const urlObject = UrlUtility.parse(decodeURI(url), true);
        //  const parsedQueryString =  UrlUtility.parseUrlQueryString(decodeURI(data.url));
        const filterDataStr = urlObject.query[RoutePathParams.filter];
        if (filterDataStr) {
          JSONUtilities.tryParse(filterDataStr).then(
            filterData => this.filterService.setFilterData(filterData), error => console.error('Error parse JSON', error));
        }
        this.selectedItemLeftId = urlObject.query['selectedItemLeft'];
        this.selectedItemRightId = urlObject.query['selectedItemRight'];
        const pathParts = urlObject.pathname.split('/');  // "/v1/report/groups/files"
        this.selectedItemRightEntity = <EntityDisplayTypes>EntityDisplayTypes[pathParts[pathParts.length - 1]];
        this.selectedItemLeftEntity = <EntityDisplayTypes>EntityDisplayTypes[pathParts[pathParts.length - 2]];

        this.activePan = urlObject.query['activePan'];
        if (this.activePan && this.activePan === 'left') {
          this.currentActionData['applyTo'] = this.selectedItemLeftEntity;
          this.currentFileStateData['applyTo'] = this.selectedItemLeftEntity;
        } else if (this.activePan && this.activePan === 'right') {
          this.currentActionData['applyTo'] = this.selectedItemRightEntity;
          this.currentFileStateData['applyTo'] = this.selectedItemRightEntity;
        }
      });
  }

  private initWorkflowItem() {
    this.route.paramMap    // Flatten the Observable with the switchMap operator instead.
      .pipe(switchMap((params: ParamMap) => {
        // The switchMap operator also cancels previous
        // in-flight requests. If the user re-navigates to this route with a new id while the HeroService
        // is still retrieving the old id, switchMap discards that old request and returns the hero for the new id.
        this.selectedWorkflowId = parseInt(params.get('id'), 10);
        if (this.selectedWorkflowId) {
          this.initV1DiscoverPage();
          return this.workflowService.loadWorkflowItem(this.selectedWorkflowId)
            .pipe(catchError((err) => this.dialogService.notifyError(err))
            , map(this.parseData))
        }
        return throwError('Missing ID');
      }))
      .subscribe(data => {
        this.workflowItem = data;
        this.initActionsFileCount();
        if (this.workflowItem.state === WorkflowState.GDPR_PENDING_APPROVAL) {
          this.initFileStateFileCount();
        }
      });

  }

  private parseData(data: GDPRWorkflowDto) {

    data.filters = data.filters || [];
    return data;
  }


  private setFramePosition() {
    const offsetTop = 86;

    const element: HTMLElement = document.querySelector('gdpr-resolve .discover-iframe-wrapper')as HTMLElement;
    if (!element) {
      console.warn('element was not found');
      return;
    }
    const styleObj = {
      position: 'absolute',
      top: `${element.offsetTop + offsetTop}px`,
      left: '0',
      height: `${window.innerHeight }px`
    };
    this.v1Service.setFramePosition(styleObj);
  }
}

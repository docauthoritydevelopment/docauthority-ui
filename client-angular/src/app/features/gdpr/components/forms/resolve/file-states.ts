import {EFileAction, EFileState} from '@app/features/gdpr/types/GDPR.workflow.dto';


export const FileStatesBtn: any[] = [
  {
    title: 'Approve',
    btnCls: 'btn-success',
    iconCls: 'fa-file-o',
    state: EFileState.APPROVED,
    active: false
  },
  {
    title: 'Disapprove',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    state: EFileState.DISAPPROVED,
    active: false
  },
  {
    title: 'Remove',
    btnCls: 'btn-link',
    state: EFileState.ACTION_PENDING_APPROVAL,
    active: true
  }
];


export const FileStatesTabs: any[] = [
  {
    title: 'Pending approval',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    state: EFileState.ACTION_PENDING_APPROVAL,
    active: true
  },
  {
    title: 'Approve',
    btnCls: 'btn-success',
    iconCls: 'fa-file-o',
    state: EFileState.APPROVED,
    active: false
  },
  {
    title: 'Disapprove',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    state: EFileState.DISAPPROVED,
    active: false
  }
];


import {EFileAction} from '@app/features/gdpr/types/GDPR.workflow.dto';

export const ResolveBtnActions: any[] = [
  {
    title: 'Do Nothing',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    action: EFileAction.DO_NOTHING
  },
  {
    title: 'Delete',
    btnCls: 'btn-danger',
    iconCls: 'fa-trash',
    action: EFileAction.DELETE
  },
  {
    title: 'Archive',
    btnCls: 'btn-info',
    iconCls: 'fa-archive',
    action: EFileAction.ARCHIVE
  },
  {
    title: 'Irrelevant',
    btnCls: 'btn-success',
    iconCls: 'fa-cut',
    action: EFileAction.IRRELEVANT
  },
  {
    title: 'Remove',
    btnCls: 'btn-link',
    action: EFileAction.NEW
  }
];


export const ResolveTabActions: any[] = [
  {
    title: 'New',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    action: EFileAction.NEW
  },
  {
    title: 'Do Nothing',
    btnCls: 'btn-default',
    iconCls: 'fa-file-o',
    action: EFileAction.DO_NOTHING
  },
  {
    title: 'Delete',
    btnCls: 'btn-danger',
    iconCls: 'fa-trash',
    action: EFileAction.DELETE
  },
  {
    title: 'Archive',
    btnCls: 'btn-info',
    iconCls: 'fa-archive',
    action: EFileAction.ARCHIVE
  },
  {
    title: 'Irrelevant',
    btnCls: 'btn-success',
    iconCls: 'fa-cut',
    action: EFileAction.IRRELEVANT
  },
];

import * as _ from 'lodash';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {V1Service} from '@app/doc-1/v1-frame/v1.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {
  EFileAction,
  GDPRWorkflowDto,
  WorkflowFileDataDto,
  WorkflowFilterDescriptorDto
} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {HttpErrorResponse} from '@angular/common/http';
import {RouteUrls} from '@app/types/routing.urls';
import {EntityDisplayTypes} from '@core/types/discover.entity.types';
import {V1RouteStateName} from '@app/doc-1/angular-1-routes/model/v1.route.state.name';
import {getPermutationConfig} from '@app/features/gdpr/configuration/file-permutaion';
import {PersonDetailsDiscoverPermutation} from '@app/features/gdpr/types/person-details-discover-permutation';
import {ActionCommentComponent} from '@app/features/gdpr/components/action-comment/action-comment.component';
import {FilterService} from '@services/filter.service';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {OrCriteria} from '@services/filter-types/or-criteria';
import {SingleCriteria} from '@services/filter-types/single-criteria';
import {FilterFactory} from '@core/services/filter.factory';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {catchError, first, map, switchMap, tap} from 'rxjs/internal/operators';
import {Observable, Subscription, throwError} from 'rxjs/index';

@Component({
  selector: 'gdpr-discover',
  templateUrl: './GDPR-discover.component.html',
  styleUrls: ['./GDPR-discover.component.scss']
})

export class GDPRDiscoverComponent implements OnInit, OnDestroy {
  checkboxListOptions: any;
  includeText: string;
  comment: string;
  permutations: PersonDetailsDiscoverPermutation[];
  workflowItem: GDPRWorkflowDto;
  onWindowResize = _.debounce(() => {
    this.setFramePosition();
  }, 100);
  @ViewChild(ActionCommentComponent) actionCommentComponent;

  private selectedWorkflowId: number;
  private subscriptions: Subscription;

  constructor(
    private v1Service: V1Service,
    private route: ActivatedRoute,
    private workflowService: GDPRWorkflowService,
    private filterService: FilterService,
    private filterFactory: FilterFactory,
    private router: Router,
    private dialogService: DialogService
  ) {


  }

  onPermutationClick(permutation: PersonDetailsDiscoverPermutation) {
    if (permutation.required || !this.hasValue(permutation)) {
      return;
    }


    permutation.selected = !permutation.selected;

    this.v1Service.changeVisibility$.pipe(first()).subscribe(visible => {
      if (visible) {
        this.discover();
      }
    })
  }

  valuesOf(permutation): string {
    return permutation.keys.map(key => this.workflowItem.requestDetails[key]).filter(value => !!value).join(' + ');
  }

  hasValue(permutation): boolean {
    return !permutation.keys.find(key => !this.workflowItem.requestDetails[key]);
  }

  ngOnInit() {
    this.initV1DiscoverPage();
    this.initWorkflowItem();
    this.v1Service.setOnWindowResize(() => this.onWindowResize())


  }

  // TODO:  use it or loose it
  onFieldClicked(i) {
    this.checkboxListOptions[i].selected = !this.checkboxListOptions[i].selected;
  }

  buildFilter() {

    const selectedPermutations = this.permutations.filter(permutation => permutation.selected);
    const permutationCriteria = selectedPermutations.map(p => this.getPermutationCriteria(p));
    if (permutationCriteria.length === 0) {
      return null;
    }
    if (permutationCriteria.length === 1) {
      return permutationCriteria[0];
    } else {
      const firstCriteria = permutationCriteria.splice(0, 1);
      return new OrCriteria(firstCriteria, permutationCriteria);
    }

  }

  getPermutationCriteria(permutation: PersonDetailsDiscoverPermutation): SingleCriteria {
    const str = permutation.keys.map(key => this.workflowItem.requestDetails[key]).join(' * ');
    return this.filterFactory.createContentSearch()(str);
    //  return new AndCriteria(permutation.keys.map(key => this.filterFactory.createContentSearch()(this.workflowItem.requestDetails[key]),
    //  ));
  }

  onDialogClose(isApproved: boolean) {
    if (isApproved) {
      this.proceedToResolve(this.actionCommentComponent.comment)
    }
  }

  setFilter() {
    this.filterService.clearPageRestrictionFilter();
    const filter = this.buildFilter();
    if (filter != null) {
      this.filterService.addPageRestrictionFilter(filter);
      const thinFilterCopy = this.filterService.getThinFilterCopy();
      return thinFilterCopy;
    }
    return null;
  }

  discover() {

    const thinFilterCopy = this.setFilter();
    const v1Params = {
      left: EntityDisplayTypes.groups,
      right: EntityDisplayTypes.files,
      filter: thinFilterCopy,
      readOnly: true
    }

    this.v1Service.changeFrameRoute(V1RouteStateName.discover, v1Params);
    this.setFramePosition();
    this.v1Service.setVisibility(true);
  }

  hasCriteria() {
    return this.filterService.getFilterData().hasCriteria();
  }

  // TODO: Use it or loose it
  addFilesToWorkflow(): Observable<any> {
    this.setFilter();
    if (this.filterService.getFilterData().hasCriteria()) {
      const filterData = new WorkflowFilterDescriptorDto();
      filterData.filter = this.filterService.toKendoFilter();
      filterData.workflowFileData = new WorkflowFileDataDto();
      filterData.workflowFileData.fileAction = EFileAction.NEW;
      const result = this.workflowService.addFilesToWorkflowItem(this.selectedWorkflowId, filterData)
        .pipe(first(), tap(res => {
            this.workflowItem.filters.push(filterData.filter);
          },
          (err: HttpErrorResponse) => {
            this.dialogService.notifyError(err);
            return Observable.throw('Server error');
          }))
      return result;
    }
    return Observable.throw('Server error');
  }

  // TODO: Use it or loose it
  deleteAllWorkflowFiles() {
    this.dialogService.openConfirm('Confirm', 'Are you sure you want to remove associated file from workflow for ever?').then(ok => {
      if (ok) {
        this.workflowService.deleteAllWorkflowFiles(this.selectedWorkflowId).subscribe(res => {
            this.workflowItem = res;
            //  this.workflowItem.filters=[];
          },
          (err: HttpErrorResponse) => {

            this.dialogService.notifyError(err);

          }
        )
      }
    });
  }

  proceedToResolve(comment: string) {
    // this.workflowItem.state = WorkflowState.GDPR_DISCOVER;

    // this.addFilesToWorkflow().first().subscribe(() => {
    this.workflowService.prerareWorkflowToResolve(this.workflowItem, this.permutations, comment).subscribe(res => {
        this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.resolve.replace(':id', this.workflowItem.id.toString())]);
      },
      (err: HttpErrorResponse) => {

        this.dialogService.notifyError(err);

      });

  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  initV1DiscoverPage() {
    this.subscriptions =
      this.v1Service.v1StateChanged$.subscribe(url => {
        const filterDataStr = this.parseQuery(decodeURI(url))[RoutePathParams.filter];
        if (filterDataStr) {
          this.filterService.setFilterData(JSON.parse(filterDataStr));
        }

      });
  }

  initWorkflowItem() {
    this.route.paramMap    // Flatten the Observable with the switchMap operator instead.
      .pipe(switchMap((params: ParamMap) => {
        // The switchMap operator also cancels previous in-flight requests.
        // If the user re-navigates to this route with a new id while
        // the HeroService is still
        // retrieving the old id, switchMap discards that old request and returns the hero for the new id.
        this.selectedWorkflowId = parseInt(params.get('id'), 10);
        if (this.selectedWorkflowId) {
          return this.workflowService.loadWorkflowItem(this.selectedWorkflowId)
            .pipe(catchError((err) => this.dialogService.notifyError(err))
              , map(this.parseData)
              , tap((workflowItem) => {
                this.initFields(workflowItem);
              }));
        }
        return throwError('Missing ID');
      }))
      .subscribe((data: GDPRWorkflowDto) => this.workflowItem = data);
  }

  parseData(data: GDPRWorkflowDto) {

    if (typeof data !== 'object') {
      throw {message: 'Wrong object type', data};
    }
    data.filters = data.filters || [];
    return data;
  }

  parseQuery(str) {
    return (str).replace('?', '&').split('&').map(function (n) {
      return n = n.split('='), this[n[0]] = n[1], this
    }.bind({}))[0];
  }

  initFields(workflowItem) {
    getPermutationConfig().pipe(first()).subscribe(permutations => {
      this.workflowItem = workflowItem;
      this.permutations = permutations;
      this.permutations.forEach(permutation => permutation.selected = this.hasValue(permutation))
    });
  }

  setFramePosition() {
    const offsetTop = 50;
    const element = document.querySelector('gdpr-discover .discover-iframe-wrapper') as HTMLElement
    if (!element) {
      console.warn('Offset was not found');
      return;
    }
    this.v1Service.setFramePosition({
      position: 'absolute',
      top: `${element.offsetTop + offsetTop}px`,
      left: '0',
      height: `${window.innerHeight }px`
    });
  }
}


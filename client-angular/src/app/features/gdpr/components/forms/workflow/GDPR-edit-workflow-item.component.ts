import {AfterViewInit, Component, OnInit} from '@angular/core';
import {GDPRWorkflowDto, WorkflowState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {RoutingService} from '@services/routing.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {RouteUrls} from '@app/types/routing.urls';
import {GDPR_OPEN_WORKFLOW_REASON, GDPR_WORKFLOW_TYPE} from '../../../types/GDPR.workflow.dto';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {catchError, switchMap} from 'rxjs/operators';
import {RoutePathParams} from '@app/common/discover/types/route-path-params';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';
import {Observable, of} from 'rxjs/index';


@Component({
  selector: 'gdpr-edit-workflow-item',
  templateUrl: './GDPR-edit-workflow-item.component.html'
})

export class GDPREditWorkflowItemComponent implements OnInit, AfterViewInit {

  editMode: boolean;
  comment: string;
  worflowItem$: Observable<boolean | GDPRWorkflowDto>;
  GDPR_WORKFLOW_TYPE = GDPR_WORKFLOW_TYPE;
  GDPR_OPEN_WORKFLOW_REASON = GDPR_OPEN_WORKFLOW_REASON;
  private selectedWorkflowId: number;

  // you use the ActivatedRoute service to retrieve the parameters for the route
  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private workflowService: GDPRWorkflowService) {

    // this.registerEvents();
  }

  ngOnInit() {
    this.worflowItem$ = this.route.paramMap    // Flatten the Observable with the switchMap operator instead.
      .pipe(switchMap((params: ParamMap) => {
          // The switchMap operator also cancels previous in-flight requests.
          // If the user re-navigates to this route with a new id while the HeroService
          // is still retrieving the old id, switchMap discards that old request and returns the hero for the new id.
          this.selectedWorkflowId = parseInt(params.get('id'), 10);
          if (this.selectedWorkflowId) {
            //   this.editMode = true;
            return this.workflowService.loadWorkflowItem(this.selectedWorkflowId)
          }
          const defaultWorkflow = this.createDefaultItem();
          return of(defaultWorkflow);
        })
        , catchError((err) => this.dialogService.notifyError(err))
      )
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.editMode = this.selectedWorkflowId ? true : false;
    });
  }

  save(workflow: GDPRWorkflowDto, comment: string, isValid: boolean) {
    //  if (isValid) {
    const params = {};

    this.workflowService.saveWorkflow(workflow, comment).subscribe(
      (saved: GDPRWorkflowDto) => {
        params[RoutePathParams.findId] = saved.id;
        this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.openWorkflows], {queryParams: params});
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);

      }
    )
  }

  cancel() {
    const params = {};
    params[RoutePathParams.selectedItemId] = this.selectedWorkflowId;
    this.router.navigate([`/${RouteUrls.GDPR}/` + GDPRRoutePaths.openWorkflows], {queryParams: params});
  }

  private createDefaultItem() {
    const defaultWorkflow = new GDPRWorkflowDto();
    defaultWorkflow.workflowType = GDPR_WORKFLOW_TYPE.RIGHT_TO_BE_FORGOTTEN;
    defaultWorkflow.state = WorkflowState.NEW;
    defaultWorkflow.contactInfo = '{}';
    defaultWorkflow.requestDetails = {
      reason: GDPR_OPEN_WORKFLOW_REASON.LEGAL_OBLIGATION
    }
    return defaultWorkflow;
  }


}


import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {HttpErrorResponse} from '@angular/common/http';
import {RouteUrls} from '@app/types/routing.urls';
import {Router} from '@angular/router';
import {EFileAction, EFileState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {TreeGridInnerItemBaseComponent} from '@shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {TREE_GRID_ITEM_MORE_INFO} from '@shared-ui/components/tree-grid/types/tree-grid.more-info.interfaces';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';
import {GDPRWorkflowDtoWrapper} from '@app/features/gdpr/types/gdpr-workflow-dto-wrapper';

declare let $: any;

@Component({
  selector: 'gdpr-approved-item-grid',
  templateUrl: './approved-item-grid.component.html',

})
export class ApprovedItemGridComponent extends TreeGridInnerItemBaseComponent<GDPRWorkflowDtoWrapper> implements OnInit {
  EFileState = EFileState;
  @ViewChild('el') el: ElementRef;
  contextMenuAction = () => {
  };

  constructor(
    private dialogService: DialogService,
    private workflowService: GDPRWorkflowService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));
    this.buildContextMenu();
    this.buildMoreInfoPopupObj();
  }

  goToEdit() {
    this.router.navigate(
      [`/${RouteUrls.GDPR}/${GDPRRoutePaths.workflow.replace(':id', this.dataItem.item.id.toString())}/${GDPRRoutePaths.edit}`]);
  }

  buildContextMenu() {

  }

  buildMoreInfoPopupObj() {

    this.setMoreInfo(<TREE_GRID_ITEM_MORE_INFO>
      {
        title: ``,
        props: [
          {
            title: 'assignee',
            value: this.dataItem.item.internalDto.assignee
          }

        ]
      });
  }

  onItemSelected() {
  }


  private clearAllActionsFromFiles(comment: string) {
    this.workflowService.updateWorkflowFileStatus(this.dataItem.item.id, EFileAction.NEW,
      EFileState.NONE, null, null, comment).subscribe(res => {
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);
      }
    )
  }


  private deleteWorkflow() {
    this.workflowService.deleteWorkflows([this.dataItem.item.id]).subscribe((data) => {
      },
      (err) => {
        this.dialogService.notifyError(err);
      }
    );
  }
}

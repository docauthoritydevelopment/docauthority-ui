import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RouteUrls} from 'app/types/routing.urls';
import {
  approvalWorkflowsStates,
  closeWorkflowsStates,
  openWorkflowsStates,
  pendingActionWorkflowsStates
} from 'app/features/gdpr/types/GDPR.types';
import {GDPRWorkflowService} from 'app/features/gdpr/services/GDPR.workflow.service';
import {WorkflowState} from 'app/features/gdpr/types/GDPR.workflow.dto';

@Component({
  selector: 'gdpr-menu',
  templateUrl: './GDPR-menu.component.html',
  styleUrls: ['./_GDPR-menu.scss'],
  encapsulation: ViewEncapsulation.None
})

export class GDPRMenuComponent implements OnInit {
  RouteUrls = RouteUrls;
  WorkflowState = WorkflowState;
  openWorkflowsCounter;
  approvalWorkflowsCounter;
  pendingWorkflowsCounter;
  closedWorkflowsCounter;
  private countByState: Map<WorkflowState, number>;

  constructor(private workflowService: GDPRWorkflowService) {

  }

  ngOnInit() {
    this.workflowService.getWorkflowsCountByState()
      .subscribe(data => { //with no subscription
        this.countByState = data;
        this.openWorkflowsCounter = this.getCounter(openWorkflowsStates);
        this.approvalWorkflowsCounter = this.getCounter(approvalWorkflowsStates);
        this.pendingWorkflowsCounter = this.getCounter(pendingActionWorkflowsStates);
        this.closedWorkflowsCounter = this.getCounter(closeWorkflowsStates);
      });
  }

  private getCounter(states: WorkflowState[]) {
    if (this.countByState) {
      const total = states.map(s => this.countByState.get(s) || 0)
        .reduce((accumulator, currentValue) => accumulator + currentValue);
      return total;
    }
    return '?';
  }


}


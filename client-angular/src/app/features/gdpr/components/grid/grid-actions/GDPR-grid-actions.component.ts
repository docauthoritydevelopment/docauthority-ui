import {Component, EventEmitter, Input, Output} from '@angular/core';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RouteUrls} from '@app/types/routing.urls';
import {EFileAction, EFileState, WorkflowState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {HttpErrorResponse} from '@angular/common/http';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {first} from 'rxjs/internal/operators';
import * as _ from 'lodash';
import {GDPRWorkflowDtoWrapper} from '../../../types/gdpr-workflow-dto-wrapper';
import {GDPRRoutePaths} from '../../../types/gdpr-route-pathes';

@Component({
  selector: 'gdpr-grid-actions',
  templateUrl: './GDPR-grid-actions.component.html',
  styleUrls: ['./_GDPR-grid-actions.scss']
})
export class GDPRGridActionsComponent {
  @Input() markedItems: any [];
  @Input() selectedItem: GDPRWorkflowDtoWrapper;
  @Input() currentPath: GDPRRoutePaths;
  @Output() notifyWorkflowItemChange = new EventEmitter<string>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
    private workflowService: GDPRWorkflowService
  ) {
  }

  goToAdd() {
    this.router.navigate([`/${RouteUrls.GDPR}/${GDPRRoutePaths.add}`]);
  }

  goToEdit() {
    this.router.navigate(
      [`/${RouteUrls.GDPR}/${GDPRRoutePaths.workflow.replace(':id', this.selectedItem.id.toString())}/${GDPRRoutePaths.edit}`]);
  }

  goToOpen() {
    if (this.selectedItem.state === WorkflowState.NEW) {
      this.workflowService.buildPermutations(this.selectedItem.internalDto).pipe(first()).subscribe(permutations => {
        this.workflowService.prerareWorkflowToResolve(this.selectedItem.internalDto, permutations).pipe(first()).subscribe(() => this.router.navigate(
          [`/${RouteUrls.GDPR}/${GDPRRoutePaths.resolve.replace(':id', this.selectedItem.id.toString())}`]))
      })

    } else if (this.selectedItem.state === WorkflowState.GDPR_DISCOVER || this.selectedItem.state === WorkflowState.GDPR_RESOLVE) {
      this.router.navigate([`/${RouteUrls.GDPR}/${GDPRRoutePaths.resolve.replace(':id', this.selectedItem.id.toString())}`]);
    } else if (this.selectedItem.state === WorkflowState.GDPR_PENDING_APPROVAL ||
      this.selectedItem.state === WorkflowState.GDPR_PENDING_ACTION) {
      this.router.navigate([`/${RouteUrls.GDPR}/${GDPRRoutePaths.resolve.replace(':id', this.selectedItem.id.toString())}`]);
    }
  }

  goToDiscover() {
    this.router.navigate([`/${RouteUrls.GDPR}/${GDPRRoutePaths.discover.replace(':id', this.selectedItem.id.toString())}`]);
  }


  deleteWorkflow() {
    this.workflowService.deleteWorkflows([this.selectedItem.id]).subscribe((data) => {
        this.notifyWorkflowItemChange.emit('item deleted');
      },
      (err) => {
        this.dialogService.notifyError(err);
      }
    );
  }

  setAsInProgress() {
    const workflow = _.clone(this.selectedItem.internalDto);
    workflow.state = WorkflowState.GDPR_RESOLVE;
    this.workflowService.updateWorkflowFileStatus(workflow.id, null, EFileState.NONE, null, null, '')
      .subscribe(res => {
          this.workflowService.saveWorkflow(workflow, '').subscribe(() => {
              this.notifyWorkflowItemChange.emit('update workflow to in progress');
            },
            (err: HttpErrorResponse) => {
              this.dialogService.notifyError(err);
            }
          )
        },
        (err: HttpErrorResponse) => {
          this.dialogService.notifyError(err);
        }
      )

  }


  clearAllActionsFromFiles(comment: string) {
    this.workflowService.updateWorkflowFileStatus(
      this.selectedItem.id, EFileAction.NEW, EFileState.NONE, null, null, comment).subscribe(res => {
        this.notifyWorkflowItemChange.emit('clear all actions');
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);
      }
    )
  }

  clearDiscoveredFiles(comment: string) {
    this.workflowService.updateWorkflowFileStatus(
      this.selectedItem.id, EFileAction.NEW, EFileState.NONE, null, null, comment).subscribe(res => {
        this.notifyWorkflowItemChange.emit('remove all files');
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);
      }
    )
  }

}

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {HttpErrorResponse} from '@angular/common/http';
import {RouteUrls} from '@app/types/routing.urls';
import {Router} from '@angular/router';
import {EFileAction, EFileState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {GDPRWorkflowDtoWrapper} from '../../../types/gdpr-workflow-dto-wrapper';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';
import {TreeGridInnerItemBaseComponent} from '@app/shared-ui/components/tree-grid/base/tree-grid-inner-item.base';
import {DialogService} from '@app/shared-ui/components/modal-dialogs/dialog.service';
import {TREE_GRID_ITEM_MORE_INFO} from '@app/shared-ui/components/tree-grid/types/tree-grid.more-info.interfaces';

declare let $: any;

@Component({
  selector: 'gdpr-open-item-grid',
  templateUrl: './open-item-grid.component.html',

})
export class OpenItemGridComponent extends TreeGridInnerItemBaseComponent<GDPRWorkflowDtoWrapper> implements OnInit {
  EFileState = EFileState;
  @ViewChild('el') el: ElementRef;
  contextMenuAction = () => {
  };

  constructor(
    private dialogService: DialogService,
    private workflowService: GDPRWorkflowService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit(): void {
    this.setEl($(this.el.nativeElement));

  }

  goToEdit() {

    this.router.navigate(
      [`/${RouteUrls.GDPR}/${GDPRRoutePaths.workflow.replace(':id', this.dataItem.item.id.toString())}/${GDPRRoutePaths.edit}`]);
  }

  buildContextMenu() {

  }

  buildMoreInfoPopupObj() {

    this.setMoreInfo(<TREE_GRID_ITEM_MORE_INFO>      {
      title: ``,
      props: [
        {
          title: 'assignee',
          value: this.dataItem.item.internalDto.assignee
        }

      ]
    });
  }

  onItemSelected() {

  }

  deleteWorkflow() {
    this.workflowService.deleteWorkflows([this.dataItem.item.id]).subscribe((data) => {
      },
      (err) => {
        this.dialogService.notifyError(err);
      }
    );
  }

  clearAllActionsFromFiles(comment: string) {
    this.workflowService.updateWorkflowFileStatus(this.dataItem.item.id, EFileAction.NEW, EFileState.NONE, null,
      null, comment).subscribe(res => {
      },
      (err: HttpErrorResponse) => {
        this.dialogService.notifyError(err);
      }
    )
  }

}

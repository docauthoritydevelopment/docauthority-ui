import {Component, OnInit, ViewChild} from '@angular/core';
import {OpenItemGridHeaderComponent} from './open-item-grid/grid-header/open-item-grid-header.component';
import {GDPRWorkflowDto} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {ActivatedRoute} from '@angular/router';
import {DTOPagingResults} from '@core/types/query-paging-results.dto';
import {OpenItemGridComponent} from '@app/features/gdpr/components/grid/open-item-grid/open-item-grid.component';
import {ApprovedItemGridComponent} from '@app/features/gdpr/components/grid/approved-item-grid/approved-item-grid.component';
import {ApprovedItemGridHeaderComponent} from './approved-item-grid/grid-header/approved-item-grid-header.component';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {TreeGridComponent} from '@shared-ui/components/tree-grid/components/tree-grid.component';
import {
  ITreeGridEvents,
  TreeGridAdapterMapper,
  TreeGridConfig,
  TreeGridData,
  TreeGridSortingInfo
} from '@shared-ui/components/tree-grid/types/tree-grid.type';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {throwError} from 'rxjs/index';
import {catchError} from 'rxjs/internal/operators';
import {GDPRWorkflowDtoWrapper} from '@app/features/gdpr/types/gdpr-workflow-dto-wrapper';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';
import {RoutingService} from '@services/routing.service';
import {RoutePathParams} from '../../../../common/discover/types/route-path-params';


@Component({
  selector: 'gdpr-grid',
  templateUrl: './GDPR-grid.component.html',
})

export class GDPRGridComponent implements OnInit {

  @ViewChild(TreeGridComponent) treeGrid: TreeGridComponent<GDPRWorkflowDtoWrapper>;
  currentPath: GDPRRoutePaths;
  loading = false;
  selectedItemId: number;
  selectedItem: GDPRWorkflowDtoWrapper;
  markedItems: any [];
  message: any; // This is just to satisfy the compiler and to avoid build error
  currentData: TreeGridData<GDPRWorkflowDtoWrapper>;
  currentPage: number;
  sortInfo: TreeGridSortingInfo;
  treeGridEventHandlers: ITreeGridEvents<GDPRWorkflowDtoWrapper>;
  OpenItemGridComponent = OpenItemGridComponent;
  OpenItemGridHeaderComponent = OpenItemGridHeaderComponent;
  ApprovedItemGridComponent = ApprovedItemGridComponent;
  ApprovedItemGridHeaderComponent = ApprovedItemGridHeaderComponent;
  GDPRRoutePaths: any = typeof GDPRRoutePaths;
  treeGridConfig = new TreeGridConfig();
  private onPageChanged = (pageNum: number) => {
    this.routingService.updateParam(this.route, RoutePathParams.page, pageNum);
  };
  private onItemSelected = (itemId, item: GDPRWorkflowDtoWrapper) => {
    this.selectedItemId = itemId;
    this.selectedItem = item;

    this.routingService.updateQueryParamsWithoutReloading(
      this.route.snapshot.queryParams, RoutePathParams.selectedItemId, this.selectedItemId);
  };
  private onMarkItemsChanged = (items: any []) => {
    this.markedItems = items || [];
  };

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private gdprService: GDPRWorkflowService,
              private treeGridAdapter: TreeGridAdapterService,
  ) {

    this.treeGridEventHandlers = {
      onPageChanged: this.onPageChanged,
      onItemSelected: this.onItemSelected,
      onMarkedItemsChanged: this.onMarkItemsChanged
    }
  }

  ngOnInit() {
    this.route.data
      .subscribe((data) => {
        if (data) {
          this.setStateFromParams();
          this.createGrid(data.workflows);  // this from the resolve router
        }
      });
    this.route.url.subscribe(urlSegment => {
      this.currentPath = <GDPRRoutePaths>urlSegment[0].path;
    });
  }

  onNotifyWorkflowItemChange(message: string) {
    this.reloadGrid();
  }

  private createGrid(workflows: DTOPagingResults<GDPRWorkflowDto>) {
    const parsedWorkflows = this.parseData(workflows);
    this.currentData = this.treeGridAdapter.adapt(
      parsedWorkflows,
      this.selectedItemId,
      new TreeGridAdapterMapper());
  }

  private reloadGrid() {
    return this.gdprService.loadWorkflows(this.route.snapshot.queryParams, (<any>this.route.snapshot.data).states)
      .pipe(catchError(err => {
        this.dialogService.notifyError(err);
        return throwError(err);
      })).subscribe(data => {
        this.createGrid(data);
      });
  }

  private parseData(workflows: DTOPagingResults<GDPRWorkflowDto>) {
    const res = new DTOPagingResults<GDPRWorkflowDtoWrapper>();
    res.content = workflows.content.map(w => new GDPRWorkflowDtoWrapper(w));
    return {...workflows, ...res};
  }

  private setStateFromParams() {
    const queryParams = this.route.snapshot.queryParams;
    if (queryParams) {
      this.selectedItemId = parseInt(queryParams[RoutePathParams.findId] || queryParams[RoutePathParams.selectedItemId], 10);
    }
  }
}


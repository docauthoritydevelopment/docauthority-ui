import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {GDPRWorkflowDto} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {GDPRWorkflowService} from '@app/features/gdpr/services/GDPR.workflow.service';
import {Observable, throwError} from 'rxjs/index';
import {catchError} from 'rxjs/internal/operators';
import {DTOPagingResults} from '@app/core/types/query-paging-results.dto';
import {DialogService} from '@app/shared-ui/components/modal-dialogs/dialog.service';


@Injectable()
export class GDPRGridResolve implements Resolve<DTOPagingResults<GDPRWorkflowDto>> {

  constructor(private gdprService: GDPRWorkflowService, private dialogService: DialogService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DTOPagingResults<GDPRWorkflowDto>> {

    return this.gdprService.loadWorkflows(route.queryParams, route.data.states)
      .pipe(catchError(err => {
        this.dialogService.notifyError(err);
        return throwError('Server error');
      }));
  }
}

export interface PersonDetailsDiscoverPermutation {
  keys: string[];
  selected?: boolean;
  required?: boolean;
}

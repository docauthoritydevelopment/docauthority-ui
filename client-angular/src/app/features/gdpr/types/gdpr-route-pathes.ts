export enum GDPRRoutePaths {
  openWorkflows = 'open-workflows',
  approvalWorkflows = 'approval-workflows',
  pendingActionWorkflows = 'pending-workflows',
  closedWorkflows = 'closed-workflows',
  workflow = 'workflow/:id',
  discover = 'discover/:id',
  resolve = 'resolve/:id',
  edit = 'edit',
  add = 'add',

}

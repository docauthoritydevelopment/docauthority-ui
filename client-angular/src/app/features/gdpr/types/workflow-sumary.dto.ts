import {EFileAction} from './file-action.enum';
import {EFileState} from './file-state';

export class WorkflowSummaryDto {
  numOfFiles: number;
  fileActionCount: Map<EFileAction, number>;
  fileStateCount: Map<EFileState, number>;

}

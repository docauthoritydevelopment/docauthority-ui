import {WorkflowSummaryDto} from './workflow-sumary.dto';
import {GDPR_PRIORITY} from './gdpr-priority';
import {EFileState} from './file-state';
import {EFileAction} from './file-action.enum';

export * from './file-action.enum';
export * from './file-state';
export *  from './workflow-sumary.dto';

export class GDPRWorkflowDto {
  id: number;
  dateCreated: number | Date;
  dateModified: number | Date;
  workflowType: GDPR_WORKFLOW_TYPE;
  dateClosed: number;
  dueDate: number;
  currentStateDueDate: number | Date;
  dateLastStateChange: number | Date;
  priority: GDPR_PRIORITY;
  state: WorkflowState;
  assignee: string;
  requestDetails: { [key: string]: any };
  contactInfo: string;
  createdBy: string;
  filters: any;
  workflowSummary: WorkflowSummaryDto;

}


export class WorkflowFilterDescriptorDto {
  id: number;
  filter: any;
  workflowFileData: WorkflowFileDataDto;
  dateCreated: number | Date;
  dateModified: number | Date;
}

export class WorkflowFileDataDto {
  workflowId: number;
  fileAction: EFileAction;
  fileState: EFileState;
}

export class WorkflowHistoryDto {
  id: number;
  dateCreated: number | Date;
  dateModified: number | Date;
  actionType: EWorkflowHistoryActionType;
  fieldName: string;
  fieldType: string;
  valueBefore: string;
  valueAfter: string;
  comment: string;
  actor: string;
  workflowId: number
}

export enum EWorkflowHistoryActionType {
  COMMENT = 'COMMENT',
  STATE_CHANGE = 'STATE_CHANGE',
  CREATE = 'CREATE',
  UPDATE_FIELD = 'UPDATE_FIELD'
}

export enum WorkflowState {
  NEW = 'NEW',
  GDPR_DISCOVER = 'GDPR_DISCOVER',
  GDPR_RESOLVE = 'GDPR_RESOLVE',
  GDPR_PENDING_APPROVAL = 'GDPR_PENDING_APPROVAL',
  GDPR_APPROVED = 'GDPR_APPROVED',
  GDPR_PENDING_ACTION = 'GDPR_PENDING_ACTION',
  GDPR_ACTION_DONE = 'GDPR_ACTION_DONE',
  CLOSED = 'CLOSED'
}

export enum GDPR_WORKFLOW_TYPE {
  RIGHT_TO_BE_FORGOTTEN = 'RIGHT_TO_BE_FORGOTTEN',
  RECTIFICATION = 'RECTIFICATION',
  PORTABILITY = 'PORTABILITY'

}

export enum GDPR_OPEN_WORKFLOW_REASON {
  UNNECESSARY = 'UNNECESSARY',
  WITHDRAWS_CONSET = 'WITHDRAWS_CONSET',
  OBJECTS_TO_PROCESSING = 'OBJECTS_TO_PROCESSING',
  UNLAWFULLY_PROCESSED = 'UNLAWFULLY_PROCESSED',
  LEGAL_OBLIGATION = 'LEGAL_OBLIGATION',
  PERSONAL_DATA_OF_CHILD = 'PERSONAL_DATA_OF_CHILD',
  OTHER = 'OTHER'
}







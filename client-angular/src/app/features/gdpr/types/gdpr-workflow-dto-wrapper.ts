import {EFileState, GDPRWorkflowDto} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {EFileAction} from '@app/features/gdpr/types/file-action.enum';
import {DtoUIWrapper} from '@wrappers/server-object-wrapper';

export class GDPRWorkflowDtoWrapper extends DtoUIWrapper<GDPRWorkflowDto> {
  constructor(internalDto: GDPRWorkflowDto) {
    super(internalDto);

  }

  get id() {
    return this.internalDto.id;
  }

  get state() {
    return this.internalDto.state;
  }

  get actionFilesProgress() {
    const totalFiles = this.internalDto.workflowSummary.numOfFiles ? this.internalDto.workflowSummary.numOfFiles : 0;
    let tbdActionFileCount;
    if (this.internalDto.workflowSummary.fileActionCount) {
      if (this.internalDto.workflowSummary.fileActionCount.size === 0) {
        tbdActionFileCount = totalFiles;
      } else if (this.internalDto.workflowSummary.fileActionCount.get(EFileAction.NEW)) {
        tbdActionFileCount = this.internalDto.workflowSummary.fileActionCount.get(EFileAction.NEW);
      } else {
        tbdActionFileCount = 0; // no more in status new - all done
      }
    }

    return totalFiles - tbdActionFileCount;
  }

  get approvalFilesProgress() {
    const totalFiles = this.internalDto.workflowSummary.numOfFiles ? this.internalDto.workflowSummary.numOfFiles : 0;
    let appFileCount = 0, disAppFileCount = 0;
    if (this.internalDto.workflowSummary.fileStateCount) {
      if (this.internalDto.workflowSummary.fileStateCount.size === 0) {
        return totalFiles;
      }
      if (this.internalDto.workflowSummary.fileStateCount.get(EFileState.APPROVED)) {
        appFileCount = this.internalDto.workflowSummary.fileStateCount.get(EFileState.APPROVED);
      }
      if (this.internalDto.workflowSummary.fileStateCount.get(EFileState.DISAPPROVED)) {
        disAppFileCount = this.internalDto.workflowSummary.fileStateCount.get(EFileState.DISAPPROVED);
      }
      if (disAppFileCount === 0 && appFileCount === 0) {
        return 0;
      }
    }

    return totalFiles - appFileCount - disAppFileCount;
  }

  get actionFilesMaxProgress() {

    const totalFiles = this.internalDto.workflowSummary &&
    this.internalDto.workflowSummary.numOfFiles ? this.internalDto.workflowSummary.numOfFiles : 0;
    return totalFiles;
  }

}

import {WorkflowState} from '@app/features/gdpr/types/GDPR.workflow.dto';
export const openWorkflowsStates = [WorkflowState.NEW, WorkflowState.GDPR_DISCOVER, WorkflowState.GDPR_RESOLVE];
export const approvalWorkflowsStates = [WorkflowState.GDPR_PENDING_APPROVAL, WorkflowState.GDPR_APPROVED];
export const pendingActionWorkflowsStates = [WorkflowState.GDPR_PENDING_ACTION, WorkflowState.GDPR_ACTION_DONE];
export const closeWorkflowsStates = [WorkflowState.CLOSED];


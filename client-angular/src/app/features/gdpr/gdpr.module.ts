import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ClickOutsideModule} from 'ng-click-outside';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GDPRComponent} from './components/GDPR.component';
import {GDPRGridComponent} from './components/grid/GDPR-grid.component';
import {GDPRMenuComponent} from './components/grid/menu/GDPR-menu.component';
import {OpenItemGridHeaderComponent} from './components/grid/open-item-grid/grid-header/open-item-grid-header.component';
import {GDPRGridActionsComponent} from './components/grid/grid-actions/GDPR-grid-actions.component';
import {GDPREditWorkflowItemComponent} from './components/forms/workflow/GDPR-edit-workflow-item.component';
import {GDPRDiscoverComponent} from './components/forms/discover/GDPR-discover.component';
import {GDPRWorkflowService} from './services/GDPR.workflow.service';
import {V1Module} from '@app/doc-1/v1-frame/v1.module';
import {ResolveComponent} from './components/forms/resolve/resolve.component';
import {GDPRGridResolve} from '@app/features/gdpr/components/grid/grid.resolver';
import {GDPRItemHistoryComponent} from './components/history/GDPR-item-history.component';
import {OpenItemGridComponent} from '@app/features/gdpr/components/grid/open-item-grid/open-item-grid.component';
import {ApprovedItemGridComponent} from '@app/features/gdpr/components/grid/approved-item-grid/approved-item-grid.component';
import {ApprovedItemGridHeaderComponent} from './components/grid/approved-item-grid/grid-header/approved-item-grid-header.component';
import {PermutationDisplayPipe} from '@app/features/gdpr/permutation-display.pipe';
import {ActionCommentComponent} from './components/action-comment/action-comment.component';
import {FilterService} from '@services/filter.service';
import {LoaderModule} from '@app/shared-ui/components/loader/loader.module';
import {TreeGridModule} from '@app/shared-ui/components/tree-grid/tree-grid.module';
import {SharedUiModule} from '@app/shared-ui/shared-ui.module';
import {gdprRoutes} from '@app/features/gdpr/gdpr.routes';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    LoaderModule,
    TreeGridModule,
    V1Module,
    NgbModule,
    SharedUiModule,
    ClickOutsideModule,
    RouterModule.forChild(gdprRoutes),
    TranslateModule.forChild()
  ],
  declarations: [
     GDPRComponent,
     GDPRGridComponent,
     GDPRMenuComponent,
     OpenItemGridHeaderComponent,
     ApprovedItemGridHeaderComponent,
     OpenItemGridComponent,
     ApprovedItemGridComponent,
     GDPRGridActionsComponent,
     GDPREditWorkflowItemComponent,
     GDPRDiscoverComponent,
     ResolveComponent,
     GDPRItemHistoryComponent,
     PermutationDisplayPipe,
     ActionCommentComponent,
     OpenItemGridComponent

  ],
  providers: [
    GDPRWorkflowService,
    GDPRGridResolve,
    FilterService // New instance for GDPR is needed
  ],
  entryComponents: [
    OpenItemGridComponent,
    ApprovedItemGridComponent,
    OpenItemGridHeaderComponent,
    ApprovedItemGridHeaderComponent
  ],
})
export class GdprModule {

}


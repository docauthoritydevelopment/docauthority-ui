import {Routes} from '@angular/router';
import {GDPRComponent} from './components/GDPR.component';
import {GDPRGridComponent} from './components/grid/GDPR-grid.component';
import {GDPRDiscoverComponent} from './components/forms/discover/GDPR-discover.component';
import {GDPREditWorkflowItemComponent} from './components/forms/workflow/GDPR-edit-workflow-item.component';
import {
  approvalWorkflowsStates,
  closeWorkflowsStates,
  openWorkflowsStates,
  pendingActionWorkflowsStates
} from '@app/features/gdpr/types/GDPR.types';
import {ResolveComponent} from '@app/features/gdpr/components/forms/resolve/resolve.component';
import {GDPRGridResolve} from '@app/features/gdpr/components/grid/grid.resolver';
import {GDPRRoutePaths} from '@app/features/gdpr/types/gdpr-route-pathes';

export const gdprRoutes: Routes = [
  {
    path: '',
    component: GDPRComponent,
    children: [

      {
        path: GDPRRoutePaths.openWorkflows,
        component: GDPRGridComponent,
        resolve: {
          workflows: GDPRGridResolve
        },
        data: {
          states: openWorkflowsStates,
          breadcrumb: 'Open workflows'
        }
      },
      {
        path: GDPRRoutePaths.approvalWorkflows,
        component: GDPRGridComponent,
        resolve: {
          workflows: GDPRGridResolve
        },
        data: {
          states: approvalWorkflowsStates
        }
      },
      {
        path: GDPRRoutePaths.pendingActionWorkflows,
        component: GDPRGridComponent,
        resolve: {
          workflows: GDPRGridResolve
        },
        data: {
          states: pendingActionWorkflowsStates
        }
      },
      {
        path: GDPRRoutePaths.closedWorkflows,
        component: GDPRGridComponent,
        resolve: {
          workflows: GDPRGridResolve
        },
        data: {
          states: closeWorkflowsStates
        }
      },
      {
        path: GDPRRoutePaths.add,
        component: GDPREditWorkflowItemComponent,
      },
      {
        path: GDPRRoutePaths.workflow,
        component: GDPREditWorkflowItemComponent,
        children: [
          {
            path: GDPRRoutePaths.edit,
            component: GDPREditWorkflowItemComponent,
          },

        ]
      },
      {
        path: GDPRRoutePaths.discover,
        component: GDPRDiscoverComponent,
      },
      {
        path: GDPRRoutePaths.resolve,
        component: ResolveComponent,
        data: {preserveV1Route: true}
      },
      {
        path: '',
        redirectTo: GDPRRoutePaths.openWorkflows,
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: GDPRRoutePaths.openWorkflows,
        pathMatch: 'full'
      }
    ]
  }
];



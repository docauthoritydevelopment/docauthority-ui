export var gdprTranslate = {
  "STATUS" : {
    "GDPR_DISCOVER": "Discover",
    "NEW": "New",
    "GDPR_RESOLVE": "Resolve",
    "GDPR_PENDING_APPROVAL": "Pending Approval",
    "GDPR_APPROVED": "Approved",
    "GDPR_PENDING_ACTION": "Pending action",
    "GDPR_CLOSED": "Closed",
    "GDPR_ACTION_DONE": "Done"
  }
}

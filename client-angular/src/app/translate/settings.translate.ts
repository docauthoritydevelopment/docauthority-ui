export var settingTranslate = {
  "SUB_TITLE" : "Discover > Date Filters",
  "MAIN_TITLE" : "Date filters",
  "DIALOG" : {
    "TITLE" : {
      "GENERAL" : "General",
      "RANGE_ITEMS" : "Range items",
      "ADVANCED"  : "Advanced",
      "ADD_POINT_BELLOW" : "Add item below",
      "EDIT_TITLE" : "Edit title",
      "ADD_POINT" : "Add item",
      "START_EVER" : " Start from ever",
      "NOT_START_EVER" : "Do not start from ever",
      "TODAY_ON" : "Today - ON",
      "TODAY_OFF" : "Today - OFF",
    }
  },
  "DATE_SELECT" : {
    "ABSOLUTE" : "Absolute",
    "RELATIVE" : "Relative",
    "TITLE" : {
      "RESOLUTAION_STEP" : "Resolution step",
      "CANCEL_EDIT_TITLE" : "Cancel edit title",
      "SAVE_TITLE" : "Save title",
      "EDIT_TITLE" : "Edit title",
      "DELETE_ITEM" : "Delete this item"
    }
  },
  "LOG_LEVELS": {
    "TYPES" : {
      'OFF': 'Off',
      'ERROR': 'Error',
      'DEBUG':'Debug',
      'WARN': 'Warn',
      'INFO':'Info',
      'TRACE':'Trace',
      'FATAL':'Fatal'
    },
    "SEARCH_PLACE_HOLDER": "Search log level"
  },
  "DEPARTMENTS": {
    "BREADCRUMBS": 'Discover > Departments',
    "TITLE": 'Departments',
    "COMPONENT_MULTI": 'departments',
    "COMPONENT": 'department'
  },
  "MATTERS": {
    "BREADCRUMBS": 'Discover > Matters',
    "TITLE": 'Matters',
    "COMPONENT_MULTI": 'matters',
    "COMPONENT": 'matter'
  },
  "USER_VIEW_DATA": {
    "SAVED_FILTERS": {
      "BREADCRUMBS": 'Discover > Saved filters',
      "TITLE": 'Saved filters',
      "COMPONENT_MULTI": 'saved filters',
      "COMPONENT": 'saved filter',
      "COMPONENT_BASIC": 'filter'
    },
    "SAVED_CHARTS": {
      "BREADCRUMBS": 'Reports > Saved charts',
      "TITLE": 'Saved charts',
      "COMPONENT_MULTI": 'saved charts',
      "COMPONENT": 'saved chart',
      "COMPONENT_BASIC": 'chart'
    },
    "SAVED_WIDGETS": {
      "BREADCRUMBS": 'Dashboard > Widgets',
      "TITLE": 'Widgets',
      "COMPONENT_MULTI": 'widgets',
      "COMPONENT": 'widget',
      "COMPONENT_BASIC": 'widget'
    },
    "SAVED_VIEWS": {
      "BREADCRUMBS": 'Discover > Views',
      "TITLE": 'Views',
      "COMPONENT_MULTI": 'views',
      "COMPONENT": 'view',
      "COMPONENT_BASIC": 'view'
    },
    "SAVED_DASHBOARDS": {
      "BREADCRUMBS": 'Dashboard > Saved dashboards',
      "TITLE": 'Saved dashboards',
      "COMPONENT_MULTI": 'saved dashboards',
      "COMPONENT": 'saved dashboard',
      "COMPONENT_BASIC": 'dashboard'
    }
  }
};

export var systemTranslate = {
  "INGEST_ERR_TYPE" : {
    "ERR_NONE": "None",
    "ERR_UNKNOWN": "Unknown error",
    "ERR_ENCRYPTED": "Encryption error",
    "ERR_CORRUPTED": "Corrupted error",
    "ERR_OLD_FORMAT": "Old format error",
    "ERR_WRONG_EXT": "Wrong extension",
    "ERR_IO": "IO error",
    "STRICT_OPEN_XML": "Script open XML",
    "ERR_LIMIT_EXCEEDED": "Limited exceeded",
    "ERR_FILE_NOT_FOUND": "File not found",
    "PACKAGE_CRAWL_INTERNAL_ERROR": "Package crawl internal error",
    "PACKAGE_CRAWL_PASSWORD_PROTECTED": "Package crawl password protected",
    "ERR_OFFLINE_SKIPPED": "Offline skipped",
    "ERR_MEMORY_LIMIT_EXCEEDED": "Memory limit exceeded",
    "ERR_PARSING_FAILURE": "Parsing failure",
    "ERR_OFFICE_XML_ERROR": "Office XML error",
    "ERR_NETWORK": "Network error"
  }
};

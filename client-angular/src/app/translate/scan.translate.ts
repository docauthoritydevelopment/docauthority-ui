export var scanTranslate = {
  "DIALOGS" : {
    "DESCRIPTIONS" : {
      "FULL_SCAN" : "Runs full map scan",
      "INGEST_SCAN" : "Complete ingest processing in case of scan with incomplete Ingest phase.",
      "ANALYZE_SCAN" :  " Complete analysis in case of scan with incomplete Analysis phase.",
    }
  },
  "LAST_SCAN" : {
    "HEADER" : {
      "NAME" : "Name",
      "STATUS" : "Status",
      "DETAILS" : "Details",
      "START" : "Start time",
      "ELAPSED" : "Processing time",
      "PROGRESS" : "Progress",
      "RATE" : "Rate",
      "ERRORS" : "Errors"
    }
  },
  "BIZ_LIST_MON" : {
    "HEADER" : {
      "NAME" : "Name",
      "ACTIVE" : "Active",
      "TYPE" : "Type",
      "MEMBERS" : "Members",
      "STATUS" : "Status",
      "PROGRESS" : "Progress"
    },
    "MAIN_TITLE" : "Business lists",
    "SUB_HEADER" : "Scan > Business lists monitoring",
    "TITLE" : {
      "RUN_ALL" :"Run all",
      "RESUME_ALL" : "Resume all",
      "PAUSE_ALL"  : "Pause all"
    },
    "CONFIRM" : {
      "START_EXTRACT" : " Do you want to start business list extraction? ",
      "RESUME_EXTRACT" : " Do you want to resume business list extraction?",
      "PAUSE_EXTRACT" : "Do you want to pause business list extraction?"
    },
    "SCAN_EVERY_MSG" : "Scan every ",
    "LAST_SCAN_MSG" : "Last scan ",
    "NEXT_SCAN_MSG" : "Next scan "
  },
  "HISTORY" :{
    "HEADER" : {
      "NAME" : "Nick name",
      "PATH" : "Path",
      "STATE" : "State",
      "TRIGGER" : "Run trigger",
      "SG" : "Schedule group",
      "START" : "Start time",
      "END" : "End time"
    },
    "SEARCH_TITLE": "Search by nick name or path name",
    "SEARCH_PLACE_HOLDER" : "Search root folder",
    "SUB_HEADER" : "Scan > Scan history",
    "MAIN_TITLE" : "Scan history",
    "TITLE" : {
      "INACCESSIBILITY_INDICATOR" : " Root Folder access issue ",
      "HIDE_DETAILS" : "Hide scan phases details",
      "SHOW_DETAILS" : "Show scan phases details"
    }
  },
  "SGROUPS" :{
    "SUB_HEADER" : "Scan > Schedule groups monitoring",
    "SEARCH_TITLE": "Search by schedule group name",
    "SEARCH_PLACE_HOLDER" : "Search",
    "MAIN_TITLE" : "Schedule groups",
    "HEADER" : {
      "NAME" : "Name",
      "ROOT_FOLDERS" : "Root folders",
      "STATUS": "Status",
      "DESCRIPTION" : "Description",
      "NEXT_SCAN": "Next scan in",
      "RESCAN" : "Rescan",
      "LAST_SCAN" : "Last scan",
      "ACTIONS" : "Actions"
    },
    "TITLE" : {
      "RESUME_SELECTED" : "Resume scan for selected schedule groups",
      "RUN_SELECTED" : "Run all steps for selected schedule groups",
      "PAUSE_SELECTED" : "Pause scan for selected schedule groups",
      "STOP_SELECTED" : "Stop scan for selected schedule groups",
      "RUN" : "Run all steps for this scan",
      "RESUME" : "Resume this scan",
      "STOP" : "Stop this scan",
      "PAUSE" : "Pause this scan",
      "MORE" : "More options"
    },
    "SCAN_DD" : {
      "ACTIONS": "Actions",
      "NAVIGATION" : "Navigation",
      "SG_SETTING" : "Schedule group settings",
      "VIEW_RF" : "View root folders",
      "RUN" : "Run scan",
      "PAUSE" : "Pause scan",
      "STOP" : "Stop scan",
      "ADVANCED" : "Advanced scan options..."
    },
    "CONFIRM" : {
      "START_SELECTED" :"Are you sure you want to start processing {{num_of_sg}} selected schedule groups?",
      "PAUSE_SELECTED" : "Are you sure you want to pause processing {{num_of_sg}} selected schedule groups? <br>Note: Scans with a status of paused for more than {{num_of_days}} days are stopped!",
      "STOP_SELECTED" : "Are you sure you want to stop processing {{num_of_sg}} selected schedule groups? ",
      "START" : "Start processing schedule group \n{{sg_name}}?",
      "RESUME" : "Are you sure you want to continue processing group \n{{sg_name}}?",
      "PAUSE" : "Are you sure you want to pause processing group '{{sg_name}}'? <br>Note: Scans with a status of paused for more than {{num_of_days}} days are stopped!",
      "STOP" : "Are you sure you want to stop processing group \n{{sg_name}}?"
    }
  },
  "ROOTFOLDER": {
    "SEARCH_TITLE": "Search by root folder name or path",
    "SEARCH_PLACE_HOLDER" : "Search root folder",
    "SUB_HEADER" : "Root folders monitoring",
    "ALL_TITLE" : "All Root Folders",
    "MAIN_TITLE" : "Root Folders",
    "BY_GROUP_TITLE" : "Root Folders of selected schedule groups",
    "PAUSE_CONFIRMATION" :"Are you sure you want to pause processing {{folders_num}} selected root folders? <br>Note: Scans with a status of paused for more than {{num_of_days}} days are stopped!",
    "STOP_CONFIRMATION" : "Are you sure you want to stop processing {{folders_num}} selected root folders?",
    "RESUME_CONFIRMATION" : "Are you sure you want to start/resume scan for {{folders_num}} selected root folders?",
    "CONFIRM" : {
      "RESUME" : "Are you sure you want to continue processing: \n{{root_folder_name}}?",
      "STOP" : "Are you sure you want to stop processing: \n{{root_folder_name}}?",
      "PAUSE" : "Are you sure you want to pause processing: \n'{{root_folder_name}}'? <br>Note: Scans with a status of paused for more than {{num_of_days}} days are stopped!",
      "MARK_FULL_SCAN" : "Are you sure you want to mark for full scan?",
      "UN_MARK_FULL_SCAN" : "Are you sure you want to unmark for full scan?",
      "START" : "Start processing root folder \n{{root_folder_name}}?",
      "EXTRACT" : "Start extracting root folder \n{{root_folder_name}}?"
    },
    "HEADER" : {
      "NAME" : "Nick name/Path" ,
      "STATUS" : "Status",
      "PHASE" : "Phase",
      "PROGRESS" : "Progress",
      "RATE" : "Rate",
      "END_TIME" : "End time",
      "PROCESSED" : "Processed files",
      "LAST_SCANNED" : "Last scanned",
      "ACTIONS" : "Actions",
    },
    "ITEM" : {
      "START_SCAN" : "Run scan",
      "PAUSE" : "Pause scan",
      "RESUME" : "Resume scan",
      "STOP" : "Stop scan",
      "FORCE_STOP" : "Force stop scan",
      "RUN_ALL" : "Run all scan steps",
      "MORE" : "More options",
      "ADVANCED" : "Advanced scan options..."
    },
    "TITLE" : {
      "FIRST_SCAN_MSG" : "Folder scanned for first time",
      "ACCESS_MSG" : "Root folder not accessible",
      "UNKNOWN_ACCESS_MSG" : "Root folder got unknown accessible",
      "IS_MARK" : "Mark selected root folder for full scan (re-ingest) files on next scan",
      "HIDE_DETAILS" : "Hide last scan details",
      "SHOW_DETAILS" : "Show last scan details",
      "PHASE_RATE" : "Tasks per second since job started (Tasks per second running rate",
      "PHASE_TIME" : "Estimated phase end time",
      "WORD_FILES" : "MS-Word processed files",
      "EXCEL_FILES" : "MS-Excel processed files",
      "PDF_FILES" : "PDF processed files" ,
      "OTHER_FILES" : "Other processed files",
      "LAST_RUN_START" : "Last run start time",
      "LAST_RUN_ELAPSED" : "Last run processing time",
      "START_SELECTED" : "Start/resume scan for selected root folders",
      "PAUSE_SELECTED" : "Pause scan for selected root folders",
      "STOP_SELECTED" : "Stop scan for selected root folders",
      "END_TIME" : "Estimate phase end time"
    }
  },
  "SCAN_OPTION" : {
    "RUN_SCAN" : "Run scan",
    "PAUSE_SCAN" : "Pause scan",
    "STOP_SCAN" : "Stop scan",
    "ADVANCED_SCAN" : "Advanced scan options..."
  },
  "SCAN_DD" : {
    "ACTIONS" : "Actions",
    "NAVIGATION" : "Navigation",
    "RF_SETTINGS" : "Root folder settings",
    "VIEW_SG" : "View schedule group",
    "GOTO_DISCOVER" : "Go discover",
    "VIEW_INGEST_ERRORS" : "View ingest errors",
    "VIEW_MAP_ERRORS" : "View map errors",
    "VIEW_SCAN_HISTORY" : "View scan history",
    "MORE" : "More",
    "REMOVE_STAR" : "Remove star",
    "COPY_PATH" : "Copy full path",
    "STRATEGY" : "Scan strategy",
    "MARK_FULL" : "Mark for full scan",
    "UN_MARK_FULL" : "UnMark for full scan",
    "ADD_STAR" : "Add star"
  },
  "SCAN_STRATEGY" : {
    "NORMAL" : "Normal",
    "AUTO" : "Auto (exclusive)",
    "INGEST_ONLY" : "Ingest only",
    "ANALYZE_ONLY" : "Analyze only"
  },
  "SCAN" : {
    "NOT_RESULT_MSG" : "There are no results for this filter",
    "INVALID_FILTER_MSG" : "Invalid filter combination",
    "FILTER_STARRED_MSG" : "Show folders specifically marked by star",
    "FILTER_STARRED_FOLDER" : "Starred folders",
    "DEPARTMENT" : "Department",
    "MATTER" : "Matter",
    "SCHEDULE_GROUP" : "Schedule group",
    "GROUP" : "Group",
    "DATA_CENTER" : "Data center",
    "ACTIVE" : "Active",
    "RUNNING" : "Running",
    "PAUSED" : "Paused",
    "SUSPENDED" : "Suspended",
    "PAUSED/SUSPENDED" : "Paused/Suspended",
    "NOT_ACTIVE" : "Not active",
    "NEW" : "New",
    "OK" : "OK",
    "STOPPED" : "Stopped",
    "FAILED" : "Failed",
    "STATUS" : "Status",
    "ACTIVE_PHASE" : "Active phase",
    "MEDIA_TYPE" : "Media type",
    "ACCESSIBILITY" : "Accessibility",
    "FIRST_SCAN" : "First scan",
    "RESCAN" : "Rescan",
    "ERR" : {
      "FAIL_START_SCAN" : "Failed to start scan",
      "FAIL_START_SG" : "Failed to start schedule group scan" ,
      "FAIL_PAUSE_ALL" : "Failed to pause all" ,
      "FAIL_RESUME_ALL" : "Failed to resume all",
      "PAUSE" : "Failed to pause scan",
      "STOP" : "Failed to stop scan",
      "RESUME" : "Failed to resume scan",
      "PAUSE_JOB" : "Failed to pause job",
      "RESUME_JOB" : "Failed to resume job"
    },
    "PHASE" : {
      "SCAN" : "Map",
      "SCAN_FINALIZE" : "Map Finalize",
      "INGEST" : "Ingest",
      "INGEST_FINALIZE" : "Ingest finalize",
      "OPTIMIZE_INDEXES" : "Optimize indexes",
      "ANALYZE" : "Analyze",
      "ANALYZE_FINALIZE" : "Finalizing Analysis",
      "EXTRACT" : "Extract",
      "EXTRACT_FINALIZE" : "Finalizing Extract ",
      "LABELING" : "Labeling",
    }
  },
  "PAUSE_REASON" : {
    "USER_INITIATED" : "User initiated",
    "SOLR_DOWN" : "SOLR is down",
    "NO_AVAILABLE_MEDIA_PROCESSOR" : "Datacenter is down",
    "SYSTEM_INITIATED" : "System timeout",
    "LICENSE_EXPIRED" : "License expired",
    "SYSTEM_ERROR" : "System error"
  },
  "TRIGGER" : {
    "MANUAL" : "Manual",
    "SYSTEM" : "System"
  },
  "rootfolders": "root folders",
  "failedRootFoldersCount": "Failed",
  "newRootFoldersCount": "New",
  "pausedRootFoldersCount": "Paused",
  "runningRootFoldersCount": "Running",
  "scannedRootFoldersCount": "Scanned",
  "totalRootFoldersCount": "Total",
  'EXCHANGE':'MS-Exchange',
  'SCAN_ERR_PAUSE':'Failed to pause scan',
  'SCAN_ERR_FAIL_START_SCAN':'Failed to start scan',
  'SCAN_ERR_FAIL_START_SG':'Failed to start schedule group scan',
  'SCAN_ERR_FAIL_PAUSE_ALL':'Failed to pause all',
  'SCAN_ERR_FAIL_RESUME_ALL':'Failed to resume all',
  'SCAN_ERR_STOP':'Failed to stop scan',
  'SCAN_ERR_RESUME':'Failed to resume scan',
  'SCAN_ERR_PAUSE_JOB':'Failed to pause job',
  'SCAN_ERR_RESUME_JOB':'Failed to resume job',
  'SCAN_ERR_MARK':'Failed to mark for full scan',
  'SCAN_ERR_UNMARK':'Failed to un-mark for full scan'
}

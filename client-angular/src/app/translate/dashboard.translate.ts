export var dashboardTranslate = {
  "modificationDate:[NOW-1MONTH TO NOW]" : "last month",
  "modificationDate:[NOW-3MONTHS TO NOW-1MONTH]" : "1-3 months",
  "modificationDate:[NOW-6MONTHS TO NOW-3MONTH]" : "3-6 months",
  "modificationDate:[NOW-12MONTHS TO NOW-6MONTH]" : "6-12 months",
  "modificationDate:[NOW-5YEARS TO NOW-12MONTH]" : "1-5 years",
  "modificationDate:[* TO NOW-5YEARS]" : "Older",
  "modificationDate:[NOW/DAY-1MONTH TO NOW/DAY+1DAY]" : "Last month",
  "modificationDate:[NOW/DAY-3MONTHS TO NOW/DAY-1MONTH]" : "1-3 months",
  "modificationDate:[NOW/DAY-6MONTHS TO NOW/DAY-3MONTH]" : "3-6 months",
  "modificationDate:[NOW/DAY-12MONTHS TO NOW/DAY-6MONTH]" : "6-12 months",
  "modificationDate:[NOW/DAY-5YEARS TO NOW/DAY-12MONTH]" : "1-5 years",
  "modificationDate:[* TO NOW/DAY-5YEARS]" : "Older",
  "modificationDate:(-*)" : "Unprocessed",
  "entity management details":"Entity management details",
  'Scan configuration':'Scan configuration',
  'Scan result statistic':'Scan result statistic',
  'distinct owners':'Owners',
  'distinct acl read':'Read ACLs',
  'distinct acl write':'Write ACLs',
  'sensitive data groups':'Sensitive data groups',
  'sensitive data files':'Sensitive data files',
  'scanned files':'Scanned files',
  'scanned root folders':'Scanned root folders',
  'scan user account':'Scan user account',
  'Medium Groups' : 'Medium groups',
  'Small Groups' : 'Small groups'
}

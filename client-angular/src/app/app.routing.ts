import {Routes} from '@angular/router';
import {RouteUrls} from '@app/types/routing.urls';


import {AuthorizationGuard} from '@app/core/authorization.guard';

const featureModulesPrefix = 'app/features';


export const appRoutes: Routes = [

  {
    path: RouteUrls.SYSTEM,
    loadChildren: `${featureModulesPrefix}/system/system.module#SystemModule`,  // lazy load module
    canActivate: [AuthorizationGuard],
  //  data: {preload: true},  // load bundles in background
  },
  {
    path: RouteUrls.SETTINGS,
    loadChildren: `${featureModulesPrefix}/settings/settings.module#SettingsModule`,  // lazy load module
    canActivate: [AuthorizationGuard],
    data: { preload: true, delay: true },
  },
  {
    path: RouteUrls.SCAN,
    loadChildren: `${featureModulesPrefix}/scan/scan.module#ScanModule`,  // lazy load module
    canActivate: [AuthorizationGuard],
  //  data: {preload: true},  // load bundles in background

  },
  {
    path: RouteUrls.DISCOVER,
    loadChildren: `${featureModulesPrefix}/discover/discover.module#DiscoverModule` ,  // lazy load module
    canActivate: [AuthorizationGuard],
   // data: {preload: true},  // load bundles in background


  },
  {
    path: RouteUrls.DASHBOARD,
    loadChildren: `${featureModulesPrefix}/dashboard/dashboard.module#DashboardModule`,  // lazy load module
    canActivate: [AuthorizationGuard],
  },
  {
    path: RouteUrls.GDPR,
    loadChildren: `${featureModulesPrefix}/gdpr/gdpr.module#GdprModule`,
    canActivate: [AuthorizationGuard],
  //   data: {
  //     preload: true,
  // //    breadcrumb: 'GDPR'
  //   }
  },

//  { path: '**', redirectTo: 'dashboardForwarder' } // this needs to be after other routes
];


export abstract class AppInitializer {
  abstract initUserDetails(): void;
}

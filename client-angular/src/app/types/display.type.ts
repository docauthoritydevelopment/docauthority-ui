export enum DisplayType {
  CHART = 'CHART',
  DISCOVER = 'DISCOVER',
}

export enum ToolbarActionTypes {
  ADD      = 'add',
  EDIT     = 'edit',
  CLONE    = 'clone',
  DELETE   = 'delete',
  IMPORT   = 'import',
  EXPORT   = 'export',
  PLAY     = 'play',
  PAUSE    = 'pause',
  STOP     = 'stop',
  REFRESH  = 'refresh',
  MORE     = 'more',
  SCOPE    = 'scope'
}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UrlAuthorizationService} from '@core/services/url-authorization.service';
import {first} from "rxjs/internal/operators";
import {LoginService} from "@services/login.service";
import {LicenseService} from "@services/license.service";
import {V1RouteUrl} from "@angular-1-routes/model/v1.route.url";


@Injectable()
export class AuthorizationGuard implements CanActivate {

  constructor(private authorizationService: UrlAuthorizationService,private router:Router, private licenseService: LicenseService) {

  }

  /**
   * Gets the absolute URL out of the current route
   * @param {ActivatedRouteSnapshot} route
   * @returns {string} Absolute URL
   */



  canActivate(nextRoute: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Promise<boolean> {

    return new Promise((resolve) => {
      // if(this.licenseService.loginExpired){
      //  // window.location.href = V1RouteUrl.Unauthorized;
      //   resolve(false);
      // }
      let absoluteUrl = this.getAbsoluteUrl(nextRoute.root);
      absoluteUrl = absoluteUrl.replace('//','/');
      this.authorizationService.authorizeUrl(absoluteUrl).pipe(first()).subscribe(isAuthorized => {
        if (isAuthorized ) {
          resolve(true);
        }
        else { //navigate to default route if not authorized
          if(this.licenseService.loginExpired){
            window.location.href = '/'+V1RouteUrl.Settings;
            resolve(false);
            return false;
          }
         let rootRoute = this.router.config.filter( r=> r.path == "**").map(x => x.redirectTo)[0];
         if (rootRoute !== null && rootRoute !== undefined) {
           window.location.href = rootRoute; //need to check if default rout is authorized?
         }
          return false;
        }
      });
    });
  }

  private getAbsoluteUrl(route: ActivatedRouteSnapshot) {
    if (route.children.length !== 0) {
      if (!route.url[0]) {
        return `/${this.getAbsoluteUrl(route.children[0])}`
      }
      return `${route.url[0].path}/${this.getAbsoluteUrl(route.children[0])}`
    } else {
      return route.url[0] ? route.url[0].path : '';
    }
  }
}

import {ModuleWithProviders, NgModule, SkipSelf, Optional} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpAuthInterceptor} from './http/http-auth-interceptor';
import {RouterModule} from '@angular/router';
import {HttpTimeoutInterceptor} from './http/http-timeout-interceptor';
import {BaseFilterField} from './types/discover.entity.types';
import {UrlParamsService} from './services/urlParams.service';
import {TooltipService} from './services/tooltip.service';
import {RoutingService} from './services/routing.service';
import {FilterService} from './services/filter.service';
import {environment} from '../../environments/environment';
import {AuthorizationGuard} from '@core/authorization.guard';
import {ServerErrorsCountService} from '@core/server-errors-count.service';
import {SystemVersionService} from '@core/system-version.service';
import {UrlAuthorizationService} from '@core/services/url-authorization.service';
import {LoginService} from '@core/services/login.service';
import {LicenseService} from '@core/services/license.service';
import {FilterFactory} from '@core/services/filter.factory';
import {UiStateService} from '@core/services/ui-state.service';
import {ExcelService} from '@core/services/excel.service';
import {PageTitleService} from '@core/services/page-title.service';
import {UtilService} from './services/util.service';
import {PushClientModule} from '@app/core/push-client/push-client.module';
import {MetadataGuard} from "@core/metadata.guard";
import {MetadataService} from "@services/metadata.service";
import {UserSettingsService} from "@services/user.settings.service";
import {ServerOperations} from "@services/server-operations";
import {PrometheusService} from "@services/prometheus.service";
import {ServerExportService} from "@services/serverExport.service";
import {FormsModule} from '@angular/forms';
import {TranslationModule} from "@app/common/translation/translation.module";
import {DatePipe} from '@angular/common';
import {RdbService} from "@services/rdb.service";
import {TranslateModule} from "@ngx-translate/core";

import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {CommonExportModule} from "@app/common/export/common-export.module";
import {AdvancedExportDialogComponent} from "@app/common/export/popups/advancedExportDialog/advancedExportDialog.component";


const PROVIDERS = [{provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true},
  UrlAuthorizationService, AuthorizationGuard, MetadataGuard, ServerErrorsCountService, LoginService, LicenseService,PrometheusService,
  UtilService,UserSettingsService,
   SystemVersionService,ServerOperations,ServerExportService,DatePipe,RdbService,
  UrlParamsService, BaseFilterField, RoutingService, FilterService, FilterFactory, UiStateService, TooltipService, ExcelService, PageTitleService,MetadataService];

const PROVIDERS_WITH_TIMEOUT = [...PROVIDERS, {
  provide: HTTP_INTERCEPTORS,
  useClass: HttpTimeoutInterceptor,
  multi: true
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    PushClientModule,
    TranslationModule,
    TranslateModule,
    DaDraggableModule,
    CommonExportModule
  ],
  providers: [PROVIDERS],
  entryComponents: [
    AdvancedExportDialogComponent
  ]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }


  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: environment.useTimeout ? PROVIDERS_WITH_TIMEOUT : PROVIDERS
    }
  }


}

export interface MessageData {
  subject: string;
  data: any;
}

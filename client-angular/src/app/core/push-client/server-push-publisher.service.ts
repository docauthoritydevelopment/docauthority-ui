import {Injectable} from '@angular/core';
import * as SockJS from 'sockjs-client'
import {BehaviorSubject, Subject} from 'rxjs/index';
import {JSONUtilities} from '@app/common/utilities/json-tryparse';
import {MessageData} from './message-data.interface';
import {environment} from "../../../environments/environment";

// import SockJS = require('sockjs-client');

@Injectable()
export class ServerPushPublisherService {

  private socketClient: any;

  private messages: { [subjectName: string]: BehaviorSubject<MessageData> };

  constructor() {
    if (environment.useWebSocket) {
      this.messages = {};
      const sockjs_url = '/socket/server-push-notifications';
      this.socketClient = new SockJS(sockjs_url);
      // this.socketClient.onopen = () => console.log('Wh have a socket!!1');
      this.socketClient.onmessage = message => this.handleMessage(message)
    }
  }

  message(subject: string, ...args: any[] ) {
    this.socketClient.send(JSON.stringify( {subject, args}))
  }

  closeConnection() {
    if (environment.useWebSocket) {
      this.socketClient.close();
    }
  }

  getSubject(subjectName: string): Subject<any> {
    this.ensureSubjectExisting(subjectName);
    return this.messages[subjectName];
  }

  handleMessage(message: any) {
    JSONUtilities.tryParse(message.data).then(messageData =>
      this.getSubject(messageData.subject).next(messageData.result) )
  }

  private ensureSubjectExisting(subjectName: string) {
    if (!this.messages[subjectName]) {
      this.messages[subjectName] = new BehaviorSubject<MessageData>(null);
    }
  }




}

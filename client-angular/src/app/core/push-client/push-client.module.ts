import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ServerPushPublisherService} from '@app/core/push-client/server-push-publisher.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [ServerPushPublisherService],
  declarations: []
})
export class PushClientModule { }

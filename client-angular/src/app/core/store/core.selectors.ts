import {createFeatureSelector, createSelector} from '@ngrx/store';
import {CoreState} from './core.reducer';
import {ResetStatus} from '../types/apply-to-solr-polling-result';

export const getCoreState = createFeatureSelector<CoreState>('core');

export const getPageTitle = createSelector(getCoreState, state => state ? state.appTitle : '');

export const getNotificationCount = createSelector(getCoreState, state => state ? state.notifications : null);

export const isDialogOpenSelector = createSelector(getCoreState, state => state ? state.dialogOpen : false);

export const applyToSolrStatusSelector = createSelector(getCoreState, state => state ? state.taskStatus ||
  ResetStatus : ResetStatus)

export const getPollersSelector = createSelector(getCoreState, state => state.pollers);

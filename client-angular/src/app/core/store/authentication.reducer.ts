import {Action} from '@ngrx/store';
import {
  GET_USER_INFO_SUCCESS,
  GetUserInfoSuccessAction,
  LOGOUT
} from './authentication.actions';
import {DtoUserWrapper} from '@users/model/user.model';
import {ESystemRoleName} from '@users/model/system-role-name';
import {DtoDocauthorityLicenseWrapper} from '../types/dto-docauthority-license';

export interface AuthenticationState {
  currentUser: DtoUserWrapper;
  loginLicense: DtoDocauthorityLicenseWrapper

}

const initialState: AuthenticationState = {
  currentUser: null,
  loginLicense: null
}

export function authenticationReducer(state: AuthenticationState = initialState, action: Action) {
  switch (action.type) {
    // case GET_USER_INFO_SUCCESS: {
    //   const user: DtoUserWrapper = (<GetUserInfoSuccessAction>action).userInfo;
    //   if (user && !user.bizRolesMap) {
    //     user.bizRolesMap = {};
    //   }
    //   // Add 'None' dummy role to user
    //   user.bizRolesMap[ESystemRoleName.None] = {
    //     name: ESystemRoleName.None,
    //     id: -1,
    //     authorities: [],
    //     description: '',
    //     displayName: 'None'
    //   };
    //   return {...state, currentUser: (<GetUserInfoSuccessAction>action).userInfo}
    //}
    // case GET_LOGIN_LICENSE_SUCCESS: {
    //   return {...state, loginLicense: (action as GetLoginLicenseSuccess).result}
    // }
    case LOGOUT: {
      return {...state, currentUser: null}
    }
    default: {
      return state;
    }
  }
}

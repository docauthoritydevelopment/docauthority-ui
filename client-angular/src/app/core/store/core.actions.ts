import {Action} from '@ngrx/store';
import {ApiCallMetadata} from '../api-call-metadata';
import {PollingMetadata} from '../polling-metadata';
import {ApplyToSolrPollingResult} from '../types/apply-to-solr-polling-result';
import {applyToSolrStatusSelector} from './core.selectors';


export const APP_PREFIX = `[DocAuthority UI]`;

export const GET_LOG_ERRORS_NOTIFICATIONS_SUCCESS = `${APP_PREFIX} Get Log Errors Notifications Success`;
export const GET_LOG_FATAL_NOTIFICATION_SUCCESS = `${APP_PREFIX} Get Log Fatals Notifications Success`;

export const API_CALL_ACTION = `${APP_PREFIX} API Call Action`;
export const SET_TITLE_ACTION = `${APP_PREFIX} Set title`;
export const SET_DIALOG_STATE = `${APP_PREFIX} Set Dialog State`;
export const CLEAR_FATAL_RUN_ERRORS_SUCCESS = `${APP_PREFIX} Clear fatal run errors success`;

export const NOTIFY_ERROR_ACTION = `${APP_PREFIX} Notify Error`;
export const GET_NOTIFICATIONS_FAILED = `${APP_PREFIX} Get Notification Failed`
export const GET_NOTIFICATIONS = `${APP_PREFIX} Get Notifications`
export const CLEAR_ERRORS_SUCCESS = `${APP_PREFIX} Clear Errors Success`
export const POLLING_ACTION = `${APP_PREFIX} Polling Action`
export const APPLY_TO_SOLR_POLLING_SUCCESS = `${APP_PREFIX} Apply to solr polling success`
export const CLEAR_APPLY_TO_SOLR_STATUS = `${APP_PREFIX} Clear Apply to solr status`
export const CLEAR_POLLER = `${APP_PREFIX} Clear poller`;

/**
 * When inherited, holds data regarding API calls in it's metadata
 */
export abstract class ApiCallAction implements Action {
  readonly type = API_CALL_ACTION;

  private subscribers: ((response: any) => void)[];

  protected constructor(public metadata: ApiCallMetadata, public data?: any) {
    this.subscribers = [];
  }

  subscribe(callback: (response: any) => void) {
    this.subscribers.push(callback);
  }

  runSubscribers(response) {
    this.subscribers.forEach(sub => sub(response));
  }
}

export abstract class PollingAction implements Action {
  readonly type = POLLING_ACTION;
  /**
   * An identifier for the polling action.
   */
  pollingType: string;
  guid: string;

  protected constructor(public apiCallAction: ApiCallAction, public pollingMetaData: PollingMetadata,
                        public replaceAction: boolean = true) {
    this.guid = this.getGuid();
  }

  private getGuid(): string {
    return `${this.randomStringComponent()}${this.randomStringComponent()}-${this.randomStringComponent()}-
    ${this.randomStringComponent()}-${this.randomStringComponent()}-${this.randomStringComponent()}${this.randomStringComponent()}`
  }

  private randomStringComponent(): string {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

}


export class NotifyErrorAction implements Action {
  readonly type = NOTIFY_ERROR_ACTION;

  constructor(public title, public message) {

  }
}

export class SetTitle implements Action {
  readonly type = SET_TITLE_ACTION;

  constructor(public title: string) {

  }
}

export class Empty implements Action {
  readonly type = 'This is an empty action';
}

export class SetDialogState implements Action {
  readonly type = SET_DIALOG_STATE;

  constructor(public dialogOpened: boolean) {

  }
}

export class GetNotificationsfailed implements Action {
  readonly type = GET_NOTIFICATIONS_FAILED;
}

export class GetServerErrors extends ApiCallAction {
  constructor() {
    super({
      url: '/api/run/logmon/error',
      method: 'get',
      errorAction: () => new GetNotificationsfailed(),
      successSideActions: [() => new GetLogFatals()],
      successAction: result => new GetLogErrorsSuccess(result)
    })
  }
}

export class GetNotifications implements Action {
  readonly type = GET_NOTIFICATIONS;
}

export class GetLogFatals extends ApiCallAction {
  constructor() {
    super({
      url: '/api/run/logmon/fatal',
      method: 'get',
      successAction: result => new GetLogFatalsSuccess(result)
    })
  }
}

export class GetLogFatalsSuccess implements Action {
  readonly type = GET_LOG_FATAL_NOTIFICATION_SUCCESS;

  constructor(public notifications: number) {

  }
}

export class GetLogErrorsSuccess implements Action {
  readonly type = GET_LOG_ERRORS_NOTIFICATIONS_SUCCESS;

  constructor(public notifications: number) {

  }
}

export class ClearFatalErrorsSuccess implements Action {
  readonly type = CLEAR_FATAL_RUN_ERRORS_SUCCESS;
}

export class ClearErrorsSuccess implements Action {
  readonly type = CLEAR_ERRORS_SUCCESS;
}

export class ClearFatalRunErrors extends ApiCallAction {
  constructor() {
    super({
      url: '/api/run/logmon/resetFatal',
      method: 'get',
      successAction: () => new ClearFatalErrorsSuccess()
    })
  }
}

export class ClearPoller implements Action {
  readonly type = CLEAR_POLLER;

  constructor(public pollingAction: PollingAction) {

  }
}

export class ClearRunErrors extends ApiCallAction {
  constructor() {
    super({
      url: '/api/run/logmon/reset',
      method: 'get',
      successAction: () => new ClearErrorSuccess()
    })
  }
}

export class StartPollApplyToSolr extends PollingAction {
  readonly pollingType = 'Apply to solr polling';

  constructor(pollingInterval = 25000) {
    super(new PollApplyToSolr(), {
      pollingInterval: pollingInterval,
      pollingEndingCondition: store => {
        return applyToSolrStatusSelector(store).applyToSolrStatus === 'DONE';
      },
      pollingStateClearingAction: new ClearApplyToSolrStatus()
    })
  }
}

export class ClearApplyToSolrStatus implements Action {
  readonly type = CLEAR_APPLY_TO_SOLR_STATUS;
}

export class PollApplyToSolrSuccess implements Action {
  readonly type = APPLY_TO_SOLR_POLLING_SUCCESS;

  constructor(public result: ApplyToSolrPollingResult) {

  }
}

export class StopApplyToSolr extends ApiCallAction {
  constructor() {
    super({
      url: '/api/filetag/apply/solr',
      method: 'delete',
      successAction: () => new Empty()
    })
  }
}

export class PollApplyToSolr extends ApiCallAction {
  constructor() {
    super({
      url: 'api/filetag/apply/solr',
      method: 'get',
      successAction: (result )=> {
        return(new PollApplyToSolrSuccess(result))
      }
    })
  }
}

export class ApplySystemUpdates extends ApiCallAction {
  constructor() {
    super({
      url: '/api/filetag/apply/solr',
      method: 'post',
      successAction: () => new Empty()
    })
  }

}

export class ClearErrorSuccess implements Action {
  readonly type = CLEAR_ERRORS_SUCCESS;
}


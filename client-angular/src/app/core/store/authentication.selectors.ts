import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AuthenticationState} from './authentication.reducer';

export const authenticationStateSelector = createFeatureSelector<AuthenticationState>('authentication');

// export const getCurrentUserSelector = createSelector(authenticationStateSelector, state => {
//   return state ? state.currentUser : null;
// });
//
// export const isLoginExpiredSelector = createSelector(authenticationStateSelector,
//   state =>  {
//   return !!(state && state.currentUser)
//   })

//export const getLoginLicenseSelector = createSelector(authenticationStateSelector, state => state ? state.loginLicense : null);

//export const getLoginLicenseApplyDate = createSelector(getLoginLicenseSelector, state => state.applyDate)
//export const loginLicenseExpiredSelector = createSelector(getLoginLicenseSelector,
//  state => !state ? undefined : state.internalDto.loginExpired);

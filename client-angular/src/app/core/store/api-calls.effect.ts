import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Action, Store} from '@ngrx/store';
import {catchError, map, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {API_CALL_ACTION, ApiCallAction, Empty} from './core.actions';
import {Actions, Effect} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {Observable, of, TimeoutError} from 'rxjs/index';


@Injectable()

export class ApiCallsEffect {

  apiQueue: any[] = [];

  dispatchApiCalls() {
    const action: ApiCallAction = this.apiQueue.pop();
    if (!action) {
      return;
    }
    of(action).pipe(tap((actionToProcess: ApiCallAction) => {
        (actionToProcess as any).dispatchTime = new Date();
        if (actionToProcess.metadata.preprocessActions) {
          this.store.dispatch(actionToProcess.metadata.preprocessActions())
        }
        (actionToProcess as any).type += `${actionToProcess.metadata.method} ${actionToProcess.metadata.url}`;
    }),
      withLatestFrom(this.store),
      switchMap((actionState: any) => {
          const  state = actionState[1];
          const result = this.fetchData(action, state);
          return result;
        }
      ), map((actionResponse: any) => {
        const {response} = actionResponse;
        // Check for error response
        if (response instanceof HttpErrorResponse || response instanceof TimeoutError) {
          if (!action.metadata.errorAction) {
            return new Empty();
          }
          return action.metadata.errorAction(response);
        }
        if (action.metadata.successSideActions) {
          setTimeout(() =>
            this.runSideActions(action.metadata.successSideActions, response), 100);
        }
        return action.metadata.successAction(response);

      })).subscribe(returnedAction => this.store.next(returnedAction));


  }

  @Effect() apiCallEffect: Observable<Action> = this.actions$
    .ofType(API_CALL_ACTION).pipe(
      tap((action: ApiCallAction) => {
        this.apiQueue.push(action);

        setTimeout(() => this.dispatchApiCalls());

      }),
      map(() => new Empty())
     )


  constructor(private store: Store<any>, private actions$: Actions, private http: HttpClient) {
  }


  fetchData(action, state): Observable<any> {
    const cacheExpression = action.metadata.cacheExpression;

    if (cacheExpression) {
      const cacheResult = cacheExpression(state);

      if (cacheResult !== null && cacheResult !== undefined) {
        return of({action, cacheResult});
      }
    }
    const result = this.processRequest(action)
    return result;
  }

  processRequest(action: ApiCallAction): Observable<any> {
    const body = action.metadata.requestMapper ? action.metadata.requestMapper(action.data) : action.data;
    action.metadata.method = action.metadata.method || 'get';
    const result = this.http.request(action.metadata.method, action.metadata.url, {
      body: body,
      observe: 'body',
      reportProgress: true
    }).pipe(
      map(response => {
         if (action.metadata.responseMapper) {
          response = action.metadata.responseMapper(response);
        }
        return {action, response};
      }),
      catchError(error => {
         if (!action.metadata.responseMapper && error.status === 200) {
          return of({action, response: null});
        }
        return of({action, response: error})
      }));

    return result;
  }

  private runSideActions(actions: (((result: any) => Action))[], response: any) {
    actions.forEach(sideAction =>
      setTimeout(() =>
        this.store.next(sideAction(response))), 100);
  }
}

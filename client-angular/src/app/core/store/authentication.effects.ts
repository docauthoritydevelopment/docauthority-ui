import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {LOGOUT} from './authentication.actions';
import {Empty} from '@core/store/core.actions';
import {map} from 'rxjs/internal/operators';
import {Observable} from 'rxjs/index';
import {ServerPushPublisherService} from '@app/core/push-client/server-push-publisher.service';
import {Router} from "@angular/router";
import {RoutingService} from "@services/routing.service";

@Injectable()
export class AuthenticationEffects {
  @Effect() redirectAfterLogout: Observable<Action> = this.actions$
    .ofType(LOGOUT).pipe(map(() => {
      this.serverPushClient.closeConnection();
      window.location.href = '/login.html'+'?_u='+encodeURIComponent(this.routingService.getCurrentUrl());
      return new Empty()
    }));

  constructor(private actions$: Actions,private  serverPushClient: ServerPushPublisherService,private routingService : RoutingService) {

  }

}

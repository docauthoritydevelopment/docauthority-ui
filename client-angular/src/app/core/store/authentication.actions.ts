
import {Action} from '@ngrx/store';
import {ApiCallAction, APP_PREFIX} from '@core/store/core.actions';
import {parseUserInfoFromResponse} from '../mappers/user-info-mappers';
import {licenseResponseMapper} from '../mappers/license-response-mapper';
import {DtoDocauthorityLicenseWrapper} from '../types/dto-docauthority-license';
import {ApiCallMetadata} from "@core/api-call-metadata";
import {Router} from "@angular/router";



const prefix = `${APP_PREFIX} Authentication `;

export const GET_USER_INFO_SUCCESS = `${prefix} Get user info success`;
export const LOGOUT = `${prefix} Logout`;
export const GET_LOGIN_LICENSE_SUCCESS = `${prefix} GET Login licence success`;

export class GetUserDetailsAttemptAction extends ApiCallAction {
  constructor(returnUrl: string) {

    super(<ApiCallMetadata>{
      url: '/userinfo?tmpTime'+(new Date()).getTime(),
      method: 'get',
      responseMapper: response => parseUserInfoFromResponse(response),
      successAction: result => result ? new GetUserInfoSuccessAction(result) : new LogoutAction(returnUrl),
      errorAction: () => new LogoutAction(returnUrl)

    })
  }
}


// export class GetLoginLicence extends ApiCallAction {
//   constructor() {
//     super({
//       url: 'api/license/last',
//       successAction: result => new GetLoginLicenseSuccess(result),
//       responseMapper: result => licenseResponseMapper(result)
//     });
//   }
// }
//
// export class GetLoginLicenseSuccess implements Action {
//   readonly type = GET_LOGIN_LICENSE_SUCCESS
//   constructor(public result: DtoDocauthorityLicenseWrapper) {
//
//   }
// }

export class GetUserInfoSuccessAction implements Action {
  readonly type = GET_USER_INFO_SUCCESS;

  constructor(public userInfo: any) {

  }
}

export class LogoutSuccessAction implements Action {
  readonly type = LOGOUT;

}

export class LogoutAction extends ApiCallAction {

  constructor(private currentUrl : string) {
    super(<ApiCallMetadata>{
      method: 'post',
      url: '/logout'+'?_u='+encodeURIComponent(currentUrl),
      successAction: () => new LogoutSuccessAction(),
      errorAction: () => new LogoutSuccessAction()
    })
  }

}

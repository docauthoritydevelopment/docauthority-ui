import {Actions, Effect} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {HttpClient} from '@angular/common/http';
import {ApiCallAction, ClearPoller, Empty, POLLING_ACTION, PollingAction} from './core.actions';
import {Injectable} from '@angular/core';
import {getPollersSelector} from './core.selectors';
import {map, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs/index';


@Injectable()
export class PollingEffects {

  @Effect() pollingEffect: Observable<Action> = this.actions$.ofType(POLLING_ACTION)
    .pipe(withLatestFrom(this.store)
      , tap(([action, state]) => {
          const pollingAction = action as PollingAction;
          const currentPoller = getPollersSelector(state)[pollingAction.pollingType];
          if (pollingAction.pollingMetaData.pollingStateClearingAction) {
            this.store.next(pollingAction.pollingMetaData.pollingStateClearingAction);
            // Set the clearing action to null to avoid rerunning it on the second poll
            pollingAction.pollingMetaData.pollingStateClearingAction = null;
            // If the clearing action is null, and the poller does not exist, don't poll again
          } else if (!currentPoller || currentPoller.guid !== pollingAction.guid) {
            return;
          }
          if (!pollingAction.pollingMetaData.pollingEndingCondition(state)) {
            const newAction = {...pollingAction.apiCallAction}
            this.store.next(newAction as ApiCallAction);
            setTimeout(() => this.store.dispatch(pollingAction), pollingAction.pollingMetaData.pollingInterval)
          } else {
            this.store.dispatch(new ClearPoller(pollingAction))
          }
        }
      ), map(() => new Empty()));

  constructor(private actions$: Actions, private http: HttpClient, private store: Store<any>) {
  }
}

import {Action} from '@ngrx/store';
import {
  APPLY_TO_SOLR_POLLING_SUCCESS, CLEAR_APPLY_TO_SOLR_STATUS,
  CLEAR_ERRORS_SUCCESS,
  CLEAR_FATAL_RUN_ERRORS_SUCCESS, CLEAR_POLLER, ClearPoller,
  GET_LOG_ERRORS_NOTIFICATIONS_SUCCESS,
  GET_LOG_FATAL_NOTIFICATION_SUCCESS,
  GET_NOTIFICATIONS_FAILED,
  GetLogErrorsSuccess, PollApplyToSolrSuccess, POLLING_ACTION, PollingAction,
  SET_DIALOG_STATE,
  SET_TITLE_ACTION,
  SetDialogState,
  SetTitle
} from './core.actions';
import {NotificationState} from '../types/notification-state';
import {ApplyToSolrPollingResult, ResetStatus} from '../types/apply-to-solr-polling-result';


export interface CoreState {
  appTitle: string;
  notifications: NotificationState;
  dialogOpen: boolean;
  taskStatus?: ApplyToSolrPollingResult;
  pollers: {[key: string] : PollingAction};
}


const initialState: CoreState = {
  appTitle: 'DocAuthority UI',
  notifications: {fatal: 0, error: 0},
  dialogOpen: false,
  pollers: {}

}

export function coreReducer(state: CoreState = initialState, action: Action): CoreState {
  switch (action.type) {
    case POLLING_ACTION: {
      const pollers = {...state.pollers};
      const pollingAction = action as PollingAction;
      if (!pollers[pollingAction.pollingType] ||
        (pollers[pollingAction.pollingType].guid !== pollingAction.guid && pollingAction.replaceAction)) {
        pollers[pollingAction.pollingType] = pollingAction;
      }
      return {...state, pollers }
    }
    case CLEAR_POLLER: {
      const clearAction = action as ClearPoller;
      const pollers = {...state.pollers};
      delete pollers[clearAction.pollingAction.pollingType];
      return {...state, pollers};
    }
    case SET_TITLE_ACTION: {
      return {...state, appTitle: (<SetTitle>action).title}
    }
    case CLEAR_APPLY_TO_SOLR_STATUS: {
      return {...state, taskStatus:  ResetStatus}
    }
    case APPLY_TO_SOLR_POLLING_SUCCESS: {
      return {...state, taskStatus: (action as PollApplyToSolrSuccess).result}
    }
    case SET_DIALOG_STATE: {
      return {...state, dialogOpen: (<SetDialogState>action).dialogOpened}
    }
    case GET_LOG_ERRORS_NOTIFICATIONS_SUCCESS: {
      return {
        ...state,
        notifications: {error: (<GetLogErrorsSuccess>action).notifications, fatal: state.notifications.fatal}
      }
    }
    case GET_LOG_FATAL_NOTIFICATION_SUCCESS : {
      return {
        ...state,
        notifications: {fatal: (<GetLogErrorsSuccess>action).notifications, error: state.notifications.error}
      }

    }
    case GET_NOTIFICATIONS_FAILED: {
      return {...state, notifications: {...state.notifications, lastCallError: true}}
    }
    case CLEAR_FATAL_RUN_ERRORS_SUCCESS: {
      return {
        ...state, notifications: {...state.notifications, fatal: 0}
      }
    }
    case CLEAR_ERRORS_SUCCESS: {
      return {...state, notifications: {...state.notifications, error: 0}}
    }
    default: {
      return state;
    }
  }
}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {UrlAuthorizationService} from '@core/services/url-authorization.service';
import {first} from "rxjs/internal/operators";
import {MetadataService} from "@services/metadata.service";


@Injectable()
export class MetadataGuard implements CanActivate {

  constructor(private metadataService:MetadataService) {

  }



  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {

    return this.metadataService.checkMetaDataLoaded();
  }
}

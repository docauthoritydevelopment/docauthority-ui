import {Observable} from 'rxjs/index';

export abstract class SystemVersion {
  abstract getVersion(): Observable<string>
}

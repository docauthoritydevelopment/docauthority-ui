import { Injectable } from '@angular/core';
import {Store} from '@ngrx/store';
import {forkJoin, Observable} from 'rxjs/index';
import {NotificationState} from '@app/core/types/notification-state';
import {getNotificationCount} from '@app/core/store/core.selectors';
import {ClearFatalRunErrors, ClearRunErrors} from '@app/core/store/core.actions';
import {GetLogErrorsSuccess, GetLogFatalsSuccess} from '@core/store/core.actions';
import {ServerPushPublisherService} from '@core/push-client/server-push-publisher.service';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {CommonConfig, ServerUrls} from "@app/common/configuration/common-config";


@Injectable()
export class ServerErrorsCountService {

  constructor(private store: Store<any>, private pushClient: ServerPushPublisherService, private http: HttpClient) {

  }

  public start(){
    if (!environment.useWebSocket) {
      this.pollServerCount();
    } else {
      this.subscribeToServercountPushMessages();
    }
  }

  private pollServerCount() {
    forkJoin( this.http.get('/api/run/logmon/error'), this.http.get('/api/run/logmon/fatal'))
         .subscribe((values: any[]) => {
           const [errors, fatals] = values;
           this.store.dispatch(new GetLogFatalsSuccess(fatals))
           this.store.dispatch(new GetLogErrorsSuccess(errors))
           setTimeout(() => this.pollServerCount(), CommonConfig.SERVER_ERROR_POLLING_MSEC)

         } )
  }

  private subscribeToServercountPushMessages() {
    this.pushClient.getSubject('fatals').subscribe(message => {
      this.store.dispatch(new GetLogFatalsSuccess(+message))
    })

    this.pushClient.getSubject('errors').subscribe(message => {
      // console.log('fatals', message)
      this.store.dispatch(new GetLogErrorsSuccess(+message))
    })
  }

  get serverErrorCounts$(): Observable<NotificationState> {
    return this.store.select(getNotificationCount)
  }

  clearFatals() {
    this.store.dispatch(new ClearFatalRunErrors())
  }

  clearErrors() {
    this.store.dispatch(new ClearRunErrors());
  }

}

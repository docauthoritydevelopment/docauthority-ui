import * as _ from 'lodash';
import {ReplaySubject} from 'rxjs/index';

interface Message {
  channel: string;
  data: any;
}

export class Radio {
  public channels: { name: string, subject: ReplaySubject<Message> }[];

  constructor() {
    this.channels = [];
  }


  public trigger(channelName, data) {
    const channel = this.getChannel(channelName);

    if (channel) {
      channel.subject.next(data);
    }
  }


  public listenTo(channelNames, listener) {
    channelNames = !_.isArray(channelNames) ? [channelNames] : channelNames;

    _.each(channelNames, (channelName) => {
      let channel = this.getChannel(channelName);

      if (channel == null) {
        channel = {name: channelName, subject: new ReplaySubject(1)};
        this.channels.push(channel);
      }
      channel.subject.subscribe(listener);
    })
  }



  public dispose(channelNames){
    channelNames = !_.isArray(channelNames) ? [channelNames] : channelNames;

    _.each(channelNames, (channelName) => {
      const channel = this.getChannel(channelName);

      if (channel) {
        channel.subject.unsubscribe();
      }
    })
  }


  private getChannel(channelName: string) {
    return this.channels.find(channel => channel.name === channelName);
  }
}



/**
 * This class wraps DTOs from the server.
 * Extending this class enables you to add fields
 * you need for client logic / view without loosing server data
 */
export abstract class DtoUIWrapper<T> {
  public constructor(public internalDto: T) {
    if (!internalDto) {
      throw {message: 'No internal DTO'}
    }
  //  Object.assign(this, internalDto);
  }
}

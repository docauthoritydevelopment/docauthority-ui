import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class SystemVersionService{

  constructor(private http: HttpClient) {
  }

  getVersion(): Observable<string> {
    return this.http.get('/getVersion')
      .pipe(map((data: any) => {return  data.version;
      }));
  }

}

import {SavedFilterDto, UserViewDataDto} from "@app/common/saved-user-view/types/saved-filters-dto";
import { Input, Output, EventEmitter} from "@angular/core";
import {DashboardService} from "@app/common/dashboard/services/dashboard.service";
import {ChartInfo, DashboardChartDataDto} from "@app/features/dashboard/types/dashboard-interfaces";
import {ChartType} from "@app/features/dashboard/types/dashboard-enums";
import * as _ from 'lodash';
interface EditWidgetData {
  userView: UserViewDataDto;
  filterList: SavedFilterDto[];
  containerId?: number;
  globalFilter?: boolean;
  discoverEntityTypeId? : number;
  discoverName?: string;
  discoverRightName?: string;
  filter?: SavedFilterDto;
  datePartitionId?: string;
}

export class BaseEditWidgetComponent {
  private serverCallCache:any  = {};
  public userViewDataDtoItem: UserViewDataDto = null;
  _data:EditWidgetData;

  @Input()
  set data(newData: EditWidgetData) {
    this._data = newData;
    if (newData != null) {
      this.setInitData();
    }
  }



  @Output()
  onUpdatePreview = new EventEmitter<{chartData: DashboardChartDataDto, chartInfo : ChartInfo<any,any>, userView : UserViewDataDto}>();

  @Output()
  onSetLoader = new EventEmitter<boolean>();

  get data():EditWidgetData {
    return this._data;
  }

  constructor(public baseDashboardService: DashboardService){
  }

  public isValid(onlyForPreview:boolean = false):boolean {
    return false;
  }

  public updateCurrUserViewDataItem = () => {};

  protected setInitData(){}

  private getCacheString(chartType  : ChartType, params : any):string {
    return chartType.toString() + "_" + JSON.stringify(params);
  }

  private handlePreviewHttpResponse = (dashboardChartDataDto: DashboardChartDataDto, chartInfo: ChartInfo<any,any>)=>  {
    this.onSetLoader.emit(false);
    this.onUpdatePreview.emit({
      chartData: dashboardChartDataDto,
      chartInfo: chartInfo,
      userView: this.isValid() ? this.userViewDataDtoItem : null
    })
  }

  public updatePreview = (chartInfo: ChartInfo<any,any>, onlyForUpdateSaveBtn:boolean = false)=>  {
    if (onlyForUpdateSaveBtn && this.isValid())
    {
      this.onUpdatePreview.emit({
        chartData : null,
        chartInfo : null ,
        userView : this.userViewDataDtoItem
      });
    }
    else if (this.isValid(true)) {
      this.onSetLoader.emit(true);
      this.updateCurrUserViewDataItem();
      let dataParams = this.baseDashboardService.getWidgetChartDataQueryParams(chartInfo.additionalQueryParams, chartInfo.beforeTransformQueryParams);
      dataParams.page = 1;
      let cacheStr = this.getCacheString(chartInfo.chartType, dataParams);
      if (this.serverCallCache[cacheStr])
      {
        this.handlePreviewHttpResponse(_.cloneDeep(this.serverCallCache[cacheStr]), chartInfo);
      }
      else {
        this.baseDashboardService.getDashboardWidgetChartData(chartInfo.chartType, dataParams, null).subscribe((dashboardChartDataDto: DashboardChartDataDto) => {
          if (Object.keys(this.serverCallCache).length  > 20) {
            this.serverCallCache = {};
          }
          this.serverCallCache[cacheStr] = _.cloneDeep(dashboardChartDataDto);
          this.handlePreviewHttpResponse(dashboardChartDataDto, chartInfo);
        });
      }
    }
    else {
      this.onUpdatePreview.emit(null);
    }
  }
}

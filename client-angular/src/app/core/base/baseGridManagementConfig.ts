import {Observable} from "rxjs";
import {TreeGridSortOrder} from "@tree-grid/types/tree-grid.type";


export class BaseGridManagementConfig {
  initialSortField:string;
  initialSortDir:TreeGridSortOrder;
  searchBoxPlaceHolder:string;
  entityName:string;

}

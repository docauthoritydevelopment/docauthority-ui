import {Observable, Subject} from "rxjs";
import {DTOPagingResults} from "@core/types/query-paging-results.dto";

export interface IDataService<T,S> {
  getData:(params:any) => Observable<DTOPagingResults<T>>;
  exportData:(totalItemsNum:number,params )=>void;
  newDataEvent$:Observable<S>;
  updatedDataEvent$:Observable<S>;
  deleteDataEvent$:Observable<S>;
}

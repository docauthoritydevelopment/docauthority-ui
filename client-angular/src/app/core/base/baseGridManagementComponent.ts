import {ActivatedRoute, Router} from "@angular/router";

import {BaseComponent} from "@core/base/baseComponent";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {ToolbarActionTypes} from "@app/types/display.type";
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {
  ITreeGridEvents,
  TreeGridAdapterMapper,
  TreeGridData,
  TreeGridSortingInfo, TreeGridSortOrder
} from "@tree-grid/types/tree-grid.type";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import * as _ from "lodash";
import {SettingPathParams} from "@app/features/settings/types/settings-route-pathes";
import {RoutingService} from "@services/routing.service";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";
import {Observable, of} from "rxjs";
import {UtilService} from "@services/util.service";
import {TreeGridAdapterService} from "@tree-grid/services/tree-grid-adapter.service";
import {switchMap} from "rxjs/operators";
import {IDataService} from "@core/base/IDataService";
import {BaseGridManagementConfig} from "./baseGridManagementConfig";

export abstract  class BaseGridManagementComponent<T,S> extends BaseComponent {

  public loading: boolean;
  public isExporting: boolean;
  public settingsActionItems: ButtonConfigDto[];
  public exportActionItem: ButtonConfigDto;
  public initialSortFieldName:string = null;
  public initialSortDirection: TreeGridSortOrder = null;

  public treeGridEvents: ITreeGridEvents<T>;
  public selectedItemId: number;
  public selectedItem:T;
  public totalElements: number = 0;
  public searchBoxInput: SearchBoxConfigDto;

  public treeGridData:TreeGridData<T>;
  private _baseGridManagementConfig:BaseGridManagementConfig;

  set baseGridManagementConfig(val:BaseGridManagementConfig){
    this._baseGridManagementConfig = val;

  }

  private _dataService:IDataService<T,S>;
  set dataService(val:IDataService<T,S>){
    this._dataService = val;
  }

  constructor(myRouter: Router, myActivatedRoute: ActivatedRoute,protected routingService: RoutingService,
              protected utilService: UtilService,protected activatedRoute: ActivatedRoute, protected treeGridAdapter: TreeGridAdapterService){
    super(myRouter, myActivatedRoute);

  }

  daOnInit() {
    this.initializeRoles();
    this.setActionItems();
    this.setTreeGridEvent();
    this.registerDataServiceEvents();
  }
 //action items are change onInit and when selectedItem is changed
  protected setActionItems() {
    this.settingsActionItems = [
      {
        type: ToolbarActionTypes.ADD,
        title: `Add ${this._baseGridManagementConfig.entityName}`,
        name: 'Add',
        actionFunc: this.openAddNewDialog,
        disabled: false,
        href: ToolbarActionTypes.ADD,
        order: 0,
        btnType: 'flat'
      },
      {
        type: ToolbarActionTypes.EDIT,
        title: `Edit ${this._baseGridManagementConfig.entityName}`,
        name: 'Edit',
        actionFunc: this.openEditDialog,
        disabled: !this.selectedItem ,
        href: ToolbarActionTypes.EDIT,
        order: 1,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.DELETE,
        title: `Delete ${this._baseGridManagementConfig.entityName}`,
        name: 'Delete',
        actionFunc: this.openDeleteDialog,
        disabled: !this.selectedItem,
        href: ToolbarActionTypes.DELETE,
        order: 2,
        btnType: 'link'
      },
      {
        type: ToolbarActionTypes.IMPORT,
        title: `Import ${this._baseGridManagementConfig.entityName} from csv`,
        name: 'Import',
        actionFunc: this.openUploadItemsDialog,
        disabled: false,
        href: ToolbarActionTypes.IMPORT,
        order: 3,
        btnType: 'flat'
      }
    ];

    let exportDropDownItems:DropDownConfigDto[] = [{
      title: "As Excel",
      iconClass: "fa fa-file-excel-o",
      actionFunc: this.exportData
    }];

    this.exportActionItem = {
      type: ToolbarActionTypes.EXPORT,
      title: "Export options",
      disabled: this.isExporting,
      children: exportDropDownItems,
      href: ToolbarActionTypes.EXPORT
    }
  }

  protected registerDataServiceEvents(){
    this.on(this._dataService.deleteDataEvent$, () => { //TODO: want to pass the dataItem
      this.loadData(true);
    });
    this.on(this._dataService.updatedDataEvent$, (updateItem:S) => {
      this.onAfterDataUpdated();
      this.routingService.updateParamList(this.activatedRoute,
        [SettingPathParams.selectedItemId,SettingPathParams.findId], [updateItem['id'],updateItem['id']]);
    });
    this.on(this._dataService.newDataEvent$, (newItem:S) => {
      this.onAfterDataAdded(newItem);
      this.routingService.updateParamList(this.activatedRoute,
        [SettingPathParams.selectedItemId,SettingPathParams.findId], [newItem['id'],newItem['id']]);
    });
  }

  protected onAfterDataAdded(newItem:S){

  }


  protected onAfterDataUpdated(){

  }
  protected initializeRoles(){

  }
  protected routeChanged(){
    let params = _.cloneDeep(this.getQueryParams()) || {};
    if (params) {
      this.selectedItemId = parseInt(params[RoutePathParams.findId] || params[RoutePathParams.selectedItemId], 10);
    }
    this.setInitialSort(params);
    this.initSearchBox(params);
    this.loadData(true);

  }

  onSearchChanged = (newText) => {
    this.routingService.updateParamList(this.activatedRoute,
      [SettingPathParams.searchText,SettingPathParams.page], [newText,1]);
  };


  protected loadData = (withLoader: boolean = false, findId?:number) => { //TODO: support findId ewhen supported in server, inorder to jump to the newly created item in the right page and when filter is cleared
    if (withLoader) {
      this.showLoader(true);
    }

    this.addQuerySubscription(
      this.getData().subscribe(gridData => {

      this.treeGridData = gridData;
        this.showLoader(false);
      this.totalElements = gridData.totalElements;
        this.turnOffFindIdMode();
        this.onAfterLoadData();
    }));
  };

  protected onAfterLoadData(){

  }
  private getData():Observable<TreeGridData<T>>{
    let params = _.cloneDeep(this.getQueryParams()) || {};
    this.setInitialSort(params);
    return this._dataService.getData(params).pipe(
      switchMap(data => {
          let parsedData = this.parseData(data.content);
          data.content = parsedData;
          if (data.pageable) {
            let currParams = this.getQueryParams();
            if (currParams[SettingPathParams.page] != data.pageable.pageNumber + 1) {
              this.routingService.updateQueryParamsWithoutReloading(currParams,
                SettingPathParams.page, data.pageable.pageNumber + 1);
              this.updateLatestQueryParams(SettingPathParams.page, data.pageable.pageNumber + 1);
            }
          }
          let gridData: TreeGridData<T> = this.treeGridAdapter.adapt(
            data,
            this.selectedItemId,
            new TreeGridAdapterMapper()
          );
          return of(gridData);
        })
      );
  }
  protected parseData(data:T[]){
    return data;
  }

  openUploadItemsDialog = () => {

  };

  openDeleteDialog = ()=> {

  }
  openAddNewDialog = ()=> {

  }
  openEditDialog = ()=> {

  }

  private turnOffFindIdMode() {
  if (this.getQueryParams() && this.getQueryParams()[RoutePathParams.findId]) {
    this.routingService.updateQueryParamsWithoutReloading(this.getQueryParams(),
      SettingPathParams.findId, null);
    this.updateLatestQueryParams(SettingPathParams.findId, null);

  }
}
  private setInitialSort(params:any) {
    if (!params.sort) {
      params.sort = JSON.stringify([{dir: this._baseGridManagementConfig.initialSortDir, field: this._baseGridManagementConfig.initialSortField}]);
    }
    let sortObject=JSON.parse(params.sort);
    this.initialSortFieldName = sortObject[0].field;
    this.initialSortDirection = sortObject[0].dir;
  }

  private exportData = ()=>  {
    let params = _.cloneDeep(this.getQueryParams()) || {};
     this.setInitialSort(params);
      this._dataService.exportData(this.totalElements,params);
  }


  private setTreeGridEvent() {
    this.treeGridEvents = <ITreeGridEvents<T>>{
      onPageChanged: this.onPageChanged,
      onSortByChanged: this.onSortByChanged,
      onItemSelected: this.onItemSelected
    };
  }


  private onPageChanged = (pageNum) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.page],[pageNum]);
  };


  private onSortByChanged = (sortObj: TreeGridSortingInfo) => {
    this.routingService.updateParamList(this.activatedRoute,[SettingPathParams.sort,SettingPathParams.page],
      [JSON.stringify([{"field": sortObj.sortBy, "dir": (<string>sortObj.sortOrder)}]),1]);
  };

  private onItemSelected = (itemId, item) => { //TODO: this items are entered to the back history and they should not
    if (!this.selectedItem || this.selectedItemId != itemId) {
      this.selectedItemId = itemId;
      this.routingService.updateQueryParamsWithoutReloading(this.getQueryParams(),
        RoutePathParams.selectedItemId, this.selectedItemId);
      this.updateLatestQueryParams(RoutePathParams.selectedItemId, this.selectedItemId);
    }
    this.selectedItem = item;
    this.setActionItems();
  };
  private onCheckedItemsChanged = (items: any []) => {
    //this.selectedItems = items;

    // this.selectedItemId = items[0].item.scheduleGroupDto.id;
    // this.selectedItem = items[0].item;
    // this.routingService.updateQueryParamsWithoutReloading(this.route.snapshot.queryParams,RoutePathParams.selectedItemId, items[0].item.scheduleGroupDto.id);
    //
    // this.updateActionItems('selection');
  };

  private initSearchBox(params:any = null){
    this.searchBoxInput  = {
      searchTerm: params && params[SettingPathParams.searchText]? params[SettingPathParams.searchText]:  '' ,
      title: '',
      placeholder: `Search ${this._baseGridManagementConfig.searchBoxPlaceHolder}`
    };
  }


  private showLoader = _.debounce((show: boolean) => {
    if (this.showLoader) {
      this.showLoader.cancel();
    }
    this.loading = show;
  }, 100);
}

import {OnDestroy, OnInit} from '@angular/core';
import {Subscription, Observable} from 'rxjs';
import {DaDropDownService} from "@shared-ui/components/da-drop-down/services/da-drop-down.service";
import {AppInjector} from "@app/app.component";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter, map} from "rxjs/internal/operators";
import * as _ from "lodash";


export class BaseComponent implements  OnDestroy , OnInit{
  listeners: Subscription[] = [];
  interval = null;
  public daDropDownService: DaDropDownService = null;
  timeOutListeners = {};
  timeOutDefaultKey = 'DEFAULT';
  queriesSubsciptions: any = {};
  myLatestQueryParams: any = null;
  private  defaultQuerySubscriptionKey = 'DEFAULT';
  private routeSubscribe:Subscription = null;



  constructor(private myRouter: Router = null, private myActivatedRoute: ActivatedRoute = null){
    if (AppInjector) {
      this.daDropDownService = AppInjector.get(DaDropDownService);
    }
  }

  protected routeChanged(){
  }



  updateLatestQueryParams(key,value) {
    this.myLatestQueryParams[key] = value;
  }

  getQueryParams() {
    return this.myLatestQueryParams;
  }




  startTimeOut(handler: any, timeout: any, key = this.timeOutDefaultKey) {
    this.stopTimeOut(key);
    this.timeOutListeners[key] = window.setTimeout(handler, timeout);
  }

  stopTimeOut(key= this.timeOutDefaultKey) {
    if (this.timeOutListeners[key]) {
      window.clearTimeout(this.timeOutListeners[key]);
      delete this.timeOutListeners[key]
    }
  }

  isUsingTimer(): boolean {
    return (this.interval != null);
  }

  startTimer(theFunc,period) {
    this.interval = setInterval(theFunc,period)
  }

  pauseTimer() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  ngOnInit(): void {
    if (!this.daDropDownService && AppInjector) {
      this.daDropDownService = AppInjector.get(DaDropDownService);
    }
    this.daOnInit();
    if (this.myRouter != null) {
      this.routeSubscribe = this.myRouter.events.pipe(
        filter(event => event instanceof NavigationEnd)
      ).subscribe((event: NavigationEnd) => {
        this.myLatestQueryParams = _.cloneDeep(this.myActivatedRoute.snapshot.queryParams);
        this.routeChanged();
      });
      this.myLatestQueryParams = _.cloneDeep(this.myActivatedRoute.snapshot.queryParams);
      this.routeChanged();
    }
  }

  protected daOnInit(): void {}

  isDropDownOpen() {
    return this.daDropDownService.isDaDropDownOpen();
  }

  closeAllDaDropDown(){
    this.daDropDownService.closeAllDropDowns();
  }

  ngOnDestroy(): void {
    this.cleanup();
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.daDropDownService.clearDropDownCount();
    this.clearAllQuerySubscriptions();
    if (this.routeSubscribe) {
      this.routeSubscribe.unsubscribe();
    }
    this.daOnDestroy();
  }

  daOnDestroy(): void {}

  on(invoker:Observable<any>, targetFunc: (value: any) => void): void {
    this.listeners.push(invoker.subscribe(targetFunc));
    //should add here handle error cause when subscription fails it cannot be tried again
    //also the data that is sent is not passed to the invoker
  }

  daSubscription(sub:Subscription) {
    this.listeners.push(sub);
  }

  cleanup(): void {
    this.listeners.forEach((subscript: Subscription) => {
      subscript.unsubscribe();
    });

    Object.keys(this.timeOutListeners).forEach((timeOutKey)=> {
      window.clearTimeout(this.timeOutListeners[timeOutKey]);
    });
    this.timeOutListeners = {};
    this.listeners = [];
  }

  clearAllQuerySubscriptions(){
    let qSubscriptionKeys: string[] = Object.keys(this.queriesSubsciptions);
    qSubscriptionKeys.forEach((qSubKey:string)=> {
      this.queriesSubsciptions[qSubKey].unsubscribe();
    });
  }


  clearQuerySubscription(key: string = this.defaultQuerySubscriptionKey) {
    if (this.queriesSubsciptions[key]) {
      this.queriesSubsciptions[key].unsubscribe();
    }
  }

  addQuerySubscription(querySubscription:Subscription, key: string = this.defaultQuerySubscriptionKey) {
    this.clearQuerySubscription(key);
    this.queriesSubsciptions[key] = querySubscription;
  }

}

export enum ServerExportsTypes
{
  ROOT_FOLDERS= "rootfolders",
  FILES = "files",
  FILES_TO = "files_to"
}


export enum ServerExportStatus
{
  DONE= "DONE",
  FAILED = "FAILED",
  IN_PROGRESS = "IN_PROGRESS",
  NEW = "NEW"
}



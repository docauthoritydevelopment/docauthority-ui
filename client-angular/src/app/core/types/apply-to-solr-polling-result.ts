export interface ApplyToSolrPollingResult {
  applyCounter: number;
  applyCounterMaxMark: number;
  applyToSolrStatus: 'DONE' | 'IN_PROGRESS' | 'NONE' | 'NOT_STARTED';
}


export const ResetStatus: ApplyToSolrPollingResult = {
  applyCounter: 0,
 applyCounterMaxMark: 0,
 applyToSolrStatus: 'DONE'
}

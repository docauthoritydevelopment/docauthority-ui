import {ErrorCodes} from "@core/types/error.codes";

export interface BadHttpRequestException {
  type : ErrorCodes;
  message : string;
  url:string;
}


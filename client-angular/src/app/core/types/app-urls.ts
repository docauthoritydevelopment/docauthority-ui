export enum AppUrls {
  SCHEDULE_GROUPS_SETTING = '/settings2/ScheduleGroups',
  ACTIVE_SCAN_ROOT_FOLDERS  = '/scanni/ActiveScans',
  ROOT_FOLDERS_SETTINGS = '/scan/manage/rootfolders',
  SCHEDULE_GROUPS_MONITORING = '/scanni/ScheduleGroup',
  INGEST_ERRORS = '/system/IngestError',
  SCAN_ERRORS = '/system/ScanError',
  SCAN_HISTORY = '/scanni/ScanHistory',
  BIZ_LIST_SETTINGS = '/scan/configuration/bizLists'
}

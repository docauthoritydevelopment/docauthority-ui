export class DTOPagingResults<T> {
  content: T[];
  totalPages: number;
  totalElements: number;
  size: number;
  last: boolean;
  first: boolean;
  sort: any;
  numberOfElements: number;
  number:number;
  pageable? : any;
}

export class AggregationCountItemDTO<T> {
  item:T;
  count : number;
  count2: number;
}

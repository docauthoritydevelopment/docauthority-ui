export class SystemSettingsDto {
  id : number;
  name : string;
  value : string;
  createdTimeStampMs : number;
  modifiedTimeStampMs : number;
}

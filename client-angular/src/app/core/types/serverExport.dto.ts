import {ServerExportStatus} from "@core/types/serverExport.types";
import {FileTypes} from "@core/types/file.types";


export interface ExportUserParams {
  fileName : string;
  fileType : FileTypes;
  itemsNumber : number;
  uniqueId: number;
  downloadWhenReady:string;
}


export interface ServerExportFile {
  createdBy?: string;
  createdDate?: number;
  id?: string;
  lastUpdateDate? : number;
  currentTimeStamp? : number;
  statusChangedDate ? : number;
  progress ?: number;
  status: ServerExportStatus;
  userParams: ExportUserParams
  timeLeftStr ?: string;  //client side attribute
  changedLately ? :boolean; //client side attribute
  wasDeleted? :boolean; //client side attribute
}

export class FileExportRequestDto {
  fileNamePrefix : string;
  exportParams : any ;
  userParams : any;
}

export interface NotificationState {
  fatal: number;
  error: number;
  lastCallError?: boolean;
}

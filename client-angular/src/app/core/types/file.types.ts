export enum FileTypes {
  msWord= "WORD",
  pdf= "PDF",
  text= "TEXT",
  msExcel= "EXCEL",
  csv= "CSV",
  other= "OTHER",
  scannedPDF= "SCANNED_PDF",
  package= "PACKAGE",
  zip = "ZIP"
}

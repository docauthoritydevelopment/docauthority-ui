import {Injectable} from '@angular/core';

export enum EntityDisplayTypes {
  acl_reads = 'acl_reads',
  acl_writes = 'acl_writes',
  groups = 'groups',
  folders = 'folders',
  folders_flat = 'folders_flat',
  files = 'files',
  owners = 'owners',
  contents = 'contents',
  functional_roles = 'functional_roles',
  subtrees = 'subtrees',
  tags = 'tags',
  tag_types = 'tag_types',
  root_folders = 'root_folders',
  doc_types = 'doc_types',
  bizlistItems_clients = 'bizlist_items_clients',
  bizlistItem_clients_extractions = 'bizlist_item_clients_extractions',
  bizlistItem_consumers_extraction_rules = 'bizlist_item_consumers_extraction_rules',
  file_created_dates = 'file_created_dates',
  file_modified_dates = 'file_modified_dates',
  file_accessed_dates = 'file_accessed_dates',
  departments = 'departments',
}

export enum EFilterContextNames {
  acl_read = 'aclRead',
  acl_write = 'aclWrite',
  group = 'group',
  folder = 'folder',
  file = 'file',
  owner = 'owner',
  content = 'content',
  subtree = 'subTree',
  tag = 'tag',
  functional_role = 'functionalRole',
  tag_type = 'tagType',
  root_folder = 'rootFolder',
  doc_type = 'docType',
  bizList_item_clients = 'bizListItemClients',
  bizList_item_clients_extraction = 'bizListItemClientsExtraction',
  bizList_item_consumers_extraction_rule = 'bizListItemConsumersExtractionRule',
  file_created_date = 'fileCreatedDate',
  file_modified_date = 'fileModifiedDate',
  file_accessed_date = 'fileAccessedDate',
  department = 'department',
}

@Injectable()
export class BaseFilterField {
  private config = {
    files_entityNameId : 'fileId',
    folders_entityNameId: 'folderId',
    folders_flat_entityNameId: 'folderId',
    groups_entityNameId: 'groupId',
    subfolders_entityNameId: 'subfolderId',
    subdoc_types_entityNameId: 'subDocTypeId',
    doc_types_entityNameId: 'docTypeId',
    functional_roles_entityNameId: 'functionalRoleId',
    owners_entityNameId: 'ownerId',
    content_entityNameId: 'contentId',
    extensions_entityNameId: 'extensionId',
    subtrees_entityNameId: 'subTreeId',
    acl_reads_entityNameId: 'aclReadId',
    acl_writes_entityNameId: 'aclWriteId',
    file_created_dates_entityNameId: 'creationDate',
    file_modified_dates_entityNameId: 'lastModifiedDate',
    file_accessed_dates_entityNameId: 'fsLastAccess',
    tags_entityNameId: 'file.tags',
    tag_types_entityNameId: 'file.tagType',
    bizlist_items_clients_entityNameId: 'bizListItemAssociationId',
    bizlist_item_clients_extractions_entityNameId: 'bizListItemExtractionId',
    bizlist_item_consumers_extraction_rules_entityNameId: 'extractionRule',
    departments_entityNameId: 'departmentId',
    subdepartments_entityNameId: 'subDepartmentId',
    matters_entityNameId: 'departmentId',
    submatters_entityNameId: 'subDepartmentId',
  };

  findBy(entityDisplayTypes: EntityDisplayTypes): string {
    return this.config[entityDisplayTypes + '_entityNameId'];
  }
}

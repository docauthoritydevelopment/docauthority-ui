import {DtoUIWrapper} from '@wrappers/server-object-wrapper';

export class DtoDocAuthorityLicense {
  id: number;
  creationDate: number;
  applyDate: number;
  registeredTo: string;
  issuer: string;
  restrictions: LicenseRestrictions;
  serverId: string;
  loginExpired: boolean;
}

export interface LicenseRestrictions {
  maxRootFolders: string;
  crawlerExpiry: string;
  serverId: string;
  loginExpiry: string;
  maxFiles: string;
}

export class DtoDocauthorityLicenseWrapper extends DtoUIWrapper<DtoDocAuthorityLicense> {
  constructor(internalDto: DtoDocAuthorityLicense) {
    super(internalDto);

  }

  get creationDate() {
    return new Date(this.internalDto.creationDate);
  }

  get applyDate() {
    return new Date(this.internalDto.applyDate);
  }

  get loginExpireDate(): Date {
    return new Date(parseInt(this.internalDto.restrictions.loginExpiry, 10))
  }

  get scanExpire(): Date {
    return new Date(parseInt(this.internalDto.restrictions.crawlerExpiry, 10))
  }

  get loginExpired(): boolean {
    return this.internalDto.loginExpired;
  }
}

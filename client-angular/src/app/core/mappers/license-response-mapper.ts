import {
  DtoDocAuthorityLicense,
  DtoDocauthorityLicenseWrapper
} from '../types/dto-docauthority-license';

export function licenseResponseMapper(license: DtoDocAuthorityLicense): DtoDocauthorityLicenseWrapper {
  const result =  new DtoDocauthorityLicenseWrapper(license);
  return result;
}

import {DtoUser, DtoUserWrapper} from '@users/model/user.model';
import {DtoSystemRole} from '@users/model/system-role.dto';
import {ITemplateRoleContainer} from '@users/model/template-role-container';

export function parseUserInfoFromResponse(dtoUser: DtoUser): DtoUserWrapper {
  const result: DtoUserWrapper = new DtoUserWrapper(dtoUser);

  result.bizRolesMap = getSystemRoles(dtoUser.bizRoleDtos);
  result.funcRoleMap = getSystemRoles(dtoUser.funcRoleDtos);

  return result;

}

function getSystemRoles(roleContainer: ITemplateRoleContainer[]): { [name: string]: DtoSystemRole } {
  const result: { [name: string]: DtoSystemRole } = {};

  roleContainer.forEach(role => {
    role.template.systemRoleDtos.forEach(sysRole => {
      result[sysRole.name.toString()] = sysRole;
    });
  })
  return result;
}

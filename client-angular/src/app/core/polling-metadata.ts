import {Action} from '@ngrx/store';

export interface PollingMetadata {

  /**
   * Polling interval in ms
   */
  pollingInterval: number;


  /**
   * An expression to be checked every polling, if it is true, the polling should stop
   * @param args
   * @returns {boolean}
   */
  pollingEndingCondition: (state: any) => boolean;

  /**
   * Action to dispatch that will reset the state before polling
   */
  pollingStateClearingAction: Action;
  }

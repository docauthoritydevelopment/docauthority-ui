import { TestBed, inject } from '@angular/core/testing';

import { SystemVersionService } from './system-version.service';

describe('SystemVersionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SystemVersionService]
    });
  });

  it('should be created', inject([SystemVersionService], (service: SystemVersionService) => {
    expect(service).toBeTruthy();
  }));
});

import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';
import {timeout} from 'rxjs/internal/operators';

@Injectable()
export class HttpTimeoutInterceptor implements HttpInterceptor {
  private defaultTimeout: number;

  constructor() {
    this.defaultTimeout = environment.defaultTimeout || 6000;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const timeoutLimit = Number(req.headers.get('timeout')) || this.defaultTimeout;
    return next.handle(req).pipe(timeout(timeoutLimit));
  }

}

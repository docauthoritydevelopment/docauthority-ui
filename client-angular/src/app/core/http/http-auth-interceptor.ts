import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';
import {catchError, map} from 'rxjs/internal/operators';
import {Router} from "@angular/router";
import {RoutingService} from "@services/routing.service";


export const LOGIN_PAGE = '/login.html';

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

  constructor(private routingService : RoutingService) {
  }

  intercept(req: HttpRequest<any>, handler: HttpHandler): Observable<HttpEvent<any>> {
    return handler.handle(req).pipe(map((event: HttpEvent<any>) => {
        return event;
      }), catchError((err: any) => {
        if (err instanceof HttpErrorResponse && err.status === 401) {

          window.location.href = LOGIN_PAGE+'?_u='+encodeURIComponent(this.routingService.getCurrentUrl());
          // this.store.dispatch(new LogoutAction());

        }
        throw(err);

      })
    )    as  Observable<HttpEvent<any>>;
  }

}

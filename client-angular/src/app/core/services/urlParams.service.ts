import * as _ from 'lodash';
import {TreeGridSortingInfo} from '@tree-grid/types/tree-grid.type';
import {Injectable} from '@angular/core';


export interface DefaultPagedReqParams {
  pageSize?: number;
  take?: number;
  page?: number;
  filter?: string;
  selectedItemId?: any;
  sort?: TreeGridSortingInfo | string;
  findId?: any;
}

@Injectable()
export class UrlParamsService {

  private readonly defaultPagedParams = <DefaultPagedReqParams>{
    pageSize: 30,
    page: 1
  };


  public fixedPagedParams(params: DefaultPagedReqParams, pageSize:number = null) {
    const ans: any = _.cloneDeep(params);
    ans.page = params.page && this.isInteger(params.page) ? parseInt(<any>params.page) : this.defaultPagedParams.page;
    ans.pageSize = params.pageSize && this.isInteger(params.pageSize) ? parseInt(<any>params.pageSize) : (pageSize ? pageSize: this.defaultPagedParams.pageSize);
    ans.take = params.take && this.isInteger(params.take) ? parseInt(<any>params.take) : ans.pageSize;
    ans.selectedItemId = params.findId?params.findId:null;
    if(params.findId){
      ans.selectedItemId = params.findId;
      delete ans.findId;
    }
    else{   ////findId should be sent only in special case when try to look for the exact page
      delete ans.selectedItemId;
    }
    return ans;
  }


  public buildPagedParams(params: DefaultPagedReqParams) {
    const reqParams = <DefaultPagedReqParams>{};
    reqParams.page = params.page && this.isInteger(params.page) ? parseInt(<any>params.page, 10) : this.defaultPagedParams.page;
    reqParams.pageSize = params.pageSize && this.isInteger(params.pageSize)
      ? parseInt(<any>params.pageSize, 10) : this.defaultPagedParams.pageSize;
    reqParams.take = reqParams.pageSize;
    if (params.filter) {
      reqParams.filter = params.filter;
    }
    if (params.findId) {
      reqParams.selectedItemId = params.findId;
    }
    if (params.sort) { //TODO: parse more accurate
      reqParams.sort = JSON.stringify([{
        dir: (<TreeGridSortingInfo>params.sort).sortOrder,
        field: (<TreeGridSortingInfo>params.sort).sortBy
      }]);
    }
    return reqParams;
  }


  isInteger(value) {
    if (isNaN(parseInt(value, 10))) {
      return false;
    }
    return true;
  }

  isBoolean(value) {
    if (value.trim().toLowerCase() === 'true' || value.trim().toLowerCase() === 'false') {
      return true;
    }

    return false;
  }

  parseBoolean(value) {
    if (value.trim().toLowerCase() === 'true') {
      return true;
    } else if (value.trim().toLowerCase() === 'false') {
      return false;
    }

    return null;
  }
}

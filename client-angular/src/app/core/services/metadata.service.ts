import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BaseDiscoverWrapper} from '../../common/discover/types/base-discover-wrapper';
import {forkJoin, Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {BaseDiscoverDTO} from "@app/common/discover/types/discover-interfaces";
import { of } from 'rxjs';
import {MetadataType} from "@app/common/discover/types/common-enums";


@Injectable()
export class MetadataService {

  initialSubscriber: Observable<any> = null;
  metadataLoaded: boolean = false;
  metadata: any = {};
  hashData: any = {};
  recentlyUsedMap: any = {};

  metadataLoadList: any[] = [{
    url: '/api/functionalrole/filecount',
    type: MetadataType.DATA_ROLE
  },
    {
      url: 'api/daterange/partitions/1/filecount/created',
      type: MetadataType.CREATED_DATE
    },
    {
      url: '/api/filetag/filecount',
      type: MetadataType.TAG
    },
    {
      url: '/api/doctype/filecount',
      type: MetadataType.DOC_TYPE
    }];

  recentlyUsedList: any = [
    {
      url: '/api/user/saveddata/recentlyUsedDocTypesItemKey',
      type: MetadataType.DOC_TYPE
    },
    {
      url: '/api/user/saveddata/recentlyUsedTagItemsItemKey',
      type: MetadataType.TAG
    },
    {
      url: '/api/user/saveddata/recentlyUsedDepartmentsItemsItemKey',
      type: MetadataType.DEPARTMENT
    },
    {
      url: '/api/user/saveddata/recentlyUsedDepartmentsItemsItemKey',
      type: MetadataType.MATTER
    }];

  constructor(private http: HttpClient) {
  }

  addMetadataList(wrapperList: any, mType: MetadataType) {
    const newList: any[] = [];
    const hashObj: any = {};

    for (let count = 0; count < wrapperList.content.length; count++) {
      newList.push(wrapperList.content[count]);
      hashObj[wrapperList.content[count].item.id] = wrapperList.content[count];
    }
    this.metadata[mType] = newList;
    this.hashData[mType] = hashObj
  }

  addKeyValueMetadataList(keyValueList: any, mType: MetadataType, idField='value') {
    const newList: any[] = [];
    const hashObj: any = {};

    for (let count = 0; count < keyValueList.length; count++) {
      newList.push(keyValueList[count]);
      hashObj[keyValueList[count][idField]] = keyValueList[count];
    }
    this.metadata[mType] = newList;
    this.hashData[mType] = hashObj
  }

  getKeyValueMetadataList(mType: MetadataType){
    return this.metadata[mType];
  }

  getMetadataList(mType: MetadataType): BaseDiscoverWrapper<BaseDiscoverDTO>[] {
    return this.metadata[mType];
  }

  getMetadataItem(mType: MetadataType, id: any): BaseDiscoverWrapper<BaseDiscoverDTO> {
    if (!this.hashData[mType]) {
      let ans:any  = {};
      return ans;
    }
    return this.hashData[mType][id];
  }

  getMetadataKeyValueItem(mType: MetadataType, id: any): any {
    if (!this.hashData[mType]) {
      let ans:any  = {};
      return ans;
    }
    return this.hashData[mType][id];
  }


  checkMetaDataLoaded(): boolean {
    return this.metadataLoaded;
  }


  waitToMetadataLoaded(): Observable<any> {
    if (this.metadataLoaded) {
      return of(true);
    }
    else if (this.initialSubscriber != null ) {
      return this.initialSubscriber;
    }
    else {
      this.loadInitialData();
      return this.initialSubscriber;
    }
  }


  loadInitialData() {
    const queriesArray: any[] = [];
    for (let count = 0; count < this.metadataLoadList.length; count++) {
      const params: any = {
        pageSize: environment.maximumItemsOnServerCallForExcel,
        page: 1,
        skip: 0,
        take: environment.maximumItemsOnServerCallForExcel
      };
      queriesArray.push(this.http.get(this.metadataLoadList[count].url, <any>{params}));
    }
    this.initialSubscriber = forkJoin(queriesArray).pipe(map(results => {
      for (let count = 0; count < this.metadataLoadList.length; count++) {
        this.addMetadataList(results[count], this.metadataLoadList[count].type);
      }
      this.metadataLoaded = true;
    }))
    this.initialSubscriber.subscribe();

    const recentlyUsedQueries: any[] = [];
    for (let count = 0; count < this.recentlyUsedList.length; count++) {
      recentlyUsedQueries.push(this.http.get(this.recentlyUsedList[count].url));
    }

    forkJoin(recentlyUsedQueries).pipe( map(results => {
      for (let count = 0; count < this.recentlyUsedList.length; count++) {
        this.addRecentlyList(results[count].data, this.recentlyUsedList[count].type);
      }
    })).subscribe();
  }

  addRecentlyList(recentlyData:string, mType: MetadataType) {
    let recentList: number[] = [];
    if (mType== MetadataType.TAG) {
      let tagsMap: any = JSON.parse(JSON.parse(recentlyData));
      Object.keys(tagsMap).forEach((tagsTypeId) => {
        recentList.push(...tagsMap[tagsTypeId])
      });
    }
    else {
      let usedList: number[] = JSON.parse(JSON.parse(recentlyData));
      recentList.push(...usedList)
    }
    this.recentlyUsedMap[mType] = recentList;
  }


  getRecentlyUsedList(mType: MetadataType): number[] {
    return (this.recentlyUsedMap[mType] ? this.recentlyUsedMap[mType] : []);
  }
}

import {Injectable} from '@angular/core';
import {FileTypes} from '../types/file.types';
import {FilterOption} from "@app/common/filters/types/filter-interfaces";
import * as _ from 'lodash';
import {I18nService} from "@app/common/translation/services/i18n-service";
import {CommonConfig} from "@app/common/configuration/common-config";
import {RoutePathParams} from "@app/common/discover/types/route-path-params";



@Injectable()
export class UtilService {


  constructor(private i18nSevice: I18nService) {
  }

  capitalize(str:string)
  {
    return str && str[0].toUpperCase() + str.toLowerCase().slice(1);
  }

  flattenTree =  (tree:any[])=> {
    return tree.reduce((acc, value)=> {
      acc.push(value);
      if (value.children) {
        acc = acc.concat(this.flattenTree(value.children));
      }
      return acc;
    }, []);
  };


  commonTrans(text: string): string {
    const commonPref = 'fa fa-file-';
    const commonSuffix = '-o';
    if (text === '') {
      return `${commonPref}o`;
    }
    return `${commonPref}${text}${commonSuffix}`;
  };

  getFilterString = (filterOptionsList: any[]): string => {
    const selectedState = this.flattenTree(filterOptionsList).filter(function (s) {
      return s.selected && s.value;
    }).map(function (d) {
        return d.value;
    });
    return selectedState.join(',');
  }

  getFilterDisplayString = (filterOptionsList: any[]): string => {
    const selectedState = this.flattenTree(filterOptionsList).filter(function (s) {
      return s.selected && s.value;
    }).map(function (d) {
      return (d.displayText ? d.displayText : d.value);
    });
    return selectedState.join(',');
  }

  updateCheckArray(queryParams,  queryParamName: string,filterOptions:any[]) {
    let allSelected: boolean = true;
    if (filterOptions && filterOptions.length > 0) {
      const filtersMap = {};
      if (queryParams && queryParams[queryParamName]) {
        const allFilters = queryParams[queryParamName].split(',');
        for (let count = 0; count < allFilters.length; count++) {
          filtersMap[allFilters[count]] = true;
        }
      }
      for (let count: number = 0; count < filterOptions.length; count++) {
        if (filtersMap[filterOptions[count].value]) {
          filterOptions[count].selected = true;
        }
        else if (filterOptions[count].children && filterOptions[count].children.length > 0) {
          let childAns = this.updateCheckArray(queryParams, queryParamName, filterOptions[count].children);
          if (childAns) {
            filterOptions[count].selected = true;
          }
          else {
            filterOptions[count].selected = false;
            allSelected = false;
          }
        }

        else {
          filterOptions[count].selected = false;
          allSelected = false;
        }
      }
    }
    return allSelected;
  }

  convertIntoCheckArray(queryParams, sourceArr: string[], queryParamName: string, improveDisplayText:boolean = false, translationPrefix:string = ''): any {
    const filtersMap = {};
    if (queryParams && queryParams[queryParamName]) {
      const allFilters = queryParams[queryParamName].split(',');
      for (let count = 0; count < allFilters.length; count++) {
        filtersMap[allFilters[count]] = true;
      }
    }
    const ans: any[] = [];
    for (let count = 0; count < sourceArr.length; count++) {
      let value = sourceArr[count];
      let displayText = sourceArr[count];
      if (sourceArr[count].indexOf(':')>-1) {
        value = sourceArr[count].substring(0,sourceArr[count].indexOf(':'));
        displayText = sourceArr[count].substring(sourceArr[count].indexOf(':')+1);
      }
      if (improveDisplayText) {
        let translateText = this.i18nSevice.translateId(translationPrefix + displayText);
        if (translateText == (translationPrefix + displayText)) {
          displayText = _.capitalize(displayText.replace(new RegExp('_', 'g'), ' ').toLowerCase());
        }
        else {
          displayText = translateText;
        }
      }
      ans.push({
        value: sourceArr[count],
        selected: filtersMap[value] ? true : false,
        displayText :displayText
      });
    }
    return ans;
  }

  convertMapIntoCheckArray(queryParams, sourceMap: any, queryParamName: string): any {
    const filtersMap = {};
    if (queryParams && queryParams[queryParamName]) {
      const allFilters = queryParams[queryParamName].split(',');
      for (let count = 0; count < allFilters.length; count++) {
        filtersMap[allFilters[count]] = true;
      }
    }
    const ans: any[] = [];
    let mapKeys:string[] = Object.keys(sourceMap);
    for (let count = 0; count < mapKeys.length; count++) {
      let key:string = mapKeys[count];
      let displayText:string = sourceMap[key];
      ans.push({
        value: key,
        selected: filtersMap[key] ? true : false,
        displayText :displayText
      });
    }
    return ans;
  }


  convertFilterOptionListIntoCheckArray = (queryParams, sourceArr: FilterOption[], queryParamName: string): any =>  {
    const filtersMap = {};
    if (queryParams && queryParams[queryParamName]) {
      const allFilters = queryParams[queryParamName].split(',');
      for (let count = 0; count < allFilters.length; count++) {
        filtersMap[allFilters[count]] = true;
      }
    }
    const ans: any[] = [];
    for (let count = 0; count < sourceArr.length; count++) {
      let newFilterOption:FilterOption = _.clone(sourceArr[count]);
      newFilterOption.selected = filtersMap[newFilterOption.value] ? true : false;
      ans.push(newFilterOption);
      if (sourceArr[count].children) {
        newFilterOption.children = this.convertFilterOptionListIntoCheckArray(queryParams,sourceArr[count].children,queryParamName);
        let foundSelected:boolean = false;
        let foundNotSelected:boolean = false;
        newFilterOption.children.map((fItem:FilterOption)=> {
          fItem.parent = newFilterOption;
          if (fItem.selected) {
            foundSelected = true;
          }
          else {
            foundNotSelected = true;
          }
        });
        if (foundSelected && !foundNotSelected) {
          newFilterOption.selected = true;
        }
        else if (!foundSelected && foundNotSelected) {
          newFilterOption.selected = false;
        }
      }
    }
    return ans;
  }

  getCheckArrayOfMediaType(queryParams:any) {
    let mediaTypeMap: any = {
      ["FILE_SHARE"]: 'FILE_SHARE',
      ["BOX"]: 'BOX',
      ["SHARE_POINT"]: 'SHARE_POINT',
      ["ONE_DRIVE"]: 'ONE_DRIVE'
    }
    if (CommonConfig['exchange-365.support-enabled'] === "true") {
      mediaTypeMap["EXCHANGE_365"] = 'EXCHANGE_365';
    }
    return this.convertMapIntoCheckArray(queryParams, mediaTypeMap, RoutePathParams.mediaType);
  }

  getBytesDisplayString(bytes, precision = 1) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
      return '-';
    }
    if (bytes === 0) {
      return '0';
    }
    const units = [' bytes', 'KB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + units[number];
  };


  getClassByFileType(fileType): string {
    if (fileType === FileTypes.text) {
      return this.commonTrans('text');
    }
    if (fileType === FileTypes.other) {
      return this.commonTrans('');
    }
    if (fileType === FileTypes.pdf) {
      return this.commonTrans('pdf');
    }
    if (fileType === FileTypes.scannedPDF) {
      return this.commonTrans('pdf') + ' scanned-pdf';
    }
    if (fileType === FileTypes.msExcel) {
      return this.commonTrans('excel');
    }
    if (fileType === FileTypes.msWord) {
      return this.commonTrans('word');
    }
    return this.commonTrans('code');   // unknown
  }

  parseBoolean(value) {
    if (value.trim().toLowerCase() == "true") {
      return true;
    }
    else if (value.trim().toLowerCase() == "false") {
      return false;
    }

    return null;
  }

  getPeriodString(dateTimeSpanInMillisec: number): string {
    if (dateTimeSpanInMillisec == null) {
      return '';
    }
    dateTimeSpanInMillisec<0?dateTimeSpanInMillisec=0:null;
    dateTimeSpanInMillisec>0&&dateTimeSpanInMillisec<1000 ? dateTimeSpanInMillisec=1000:null;
    let neededPeriodInMiliSec = dateTimeSpanInMillisec;
    let days = Math.floor(neededPeriodInMiliSec / (24*60*60*1000));
    let daysms=neededPeriodInMiliSec % (24*60*60*1000);
    let hours = Math.floor((daysms)/(60*60*1000));
    let hoursms=neededPeriodInMiliSec % (60*60*1000);
    let minutes = Math.floor((hoursms)/(60*1000));
    let minutesms=neededPeriodInMiliSec % (60*1000);
    let sec = Math.floor((minutesms)/(1000));
    let ans:string = null;
    let pad2 = function(number) {
      return (number < 10 ? '0' : '') + number
    }
    if (dateTimeSpanInMillisec < 24*3600*1000) {
      ans =pad2(hours) +":"+pad2(minutes)+":"+pad2(sec)
    }
    else {
      if (days == 1) {
        ans = '1 Day ';
      }
      else {
        ans = days + ' Days ';
      }
      if (hours > 0 || minutes > 0 || sec > 0 ) {
        ans = ans + pad2(hours) + ":" + pad2(minutes) + ":" + pad2(sec)
      }
    }
    return ans;
  }


  isInViewport = function(element) {
    var rect = element.getBoundingClientRect();
    var html = document.documentElement;
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    );
  }



}

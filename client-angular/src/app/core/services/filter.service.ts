import {Injectable} from '@angular/core';
import {ICriteriaComponent} from './filter-types/criteria-component.interface';
import {FilterData} from '@services/filter-data';
import {CriteriaOperation} from '@services/filter-types/criteria-operation';
import {SingleCriteria} from '@services/filter-types/single-criteria';
import {EFilterOperators} from '@services/filter-types/filter-operators';
import {OrCriteria} from '@services/filter-types/or-criteria';
import {AndCriteria} from '@services/filter-types/and-criteria';


@Injectable()
export class FilterService {
  clearPageRestrictionFilter = function () {
    this.currentFilterData.pageRestrictionFilter = null
  };
  clearPageFilter = function () {
    this.currentFilterData.pageFilter = null;

  };
  getFilterData = function () {
    return this.currentFilterData;
  };
  clearPageUserSearchFilters = function () {
    this.currentFilterData.pageUserSearchFilters = null;
  };
  clearUserSearchFilter = function () {
    this.currentFilterData.userSearchFilter = null;
  };
  removePageFilter = function (criteria: SingleCriteria) {
    this.currentFilterData.pageFilter = this.removeFilter(this.currentFilterData.pageFilter, criteria);


  };
  removePageRestrictionFilter = function (criteria: ICriteriaComponent) {
    this.currentFilterData.pageRestrictionFilter = this.removeFilter(this.currentFilterData.pageRestrictionFilter, criteria)

  };
  removePageUserFilter = function (criteria: ICriteriaComponent) {
    this.currentFilterData.pageUserSearchFilters = this.removeFilter(this.currentFilterData.pageUserSearchFilters, criteria);
  };
  private currentFilterData: FilterData = new FilterData();
  private removeFilter = function (criteriaOperation: CriteriaOperation, criteriaItemToRemove: ICriteriaComponent) {
    if (criteriaOperation == criteriaItemToRemove) {
      return null;
    }
    if (criteriaOperation) {
      criteriaOperation.removeCriteria(criteriaItemToRemove);
    }
    return criteriaOperation.criterias.length > 0 ? criteriaOperation : null;
  };

  constructor() {
    // this.setFilterData(filterData);
  }

  // removePageUserFilter(criteria: ICriteriaComponent) {
  //   this.currentFilterData.pageUserSearchFilters = this.removeFilter(this.currentFilterData.pageUserSearchFilters, criteria);
  // };

  // removePageRestrictionFilter(criteria: ICriteriaComponent) {
  //   this.currentFilterData.pageRestrictionFilter = this.removeFilter(this.currentFilterData.pageRestrictionFilter, criteria)
  //
  // };

  // removePageFilter(criteria: SingleCriteria) {
  //   this.currentFilterData.pageFilter = this.removeFilter(this.currentFilterData.pageFilter, criteria);
  //
  //
  // };

  // clearPageRestrictionFilter() {
  //   this.currentFilterData.pageRestrictionFilter = null
  // }

  // clearPageFilter() {
  //   this.currentFilterData.pageFilter = null;
  //
  // }

  // getFilterData() {
  //   return this.currentFilterData;
  // }

  // clearPageUserSearchFilters() {
  //   this.currentFilterData.pageUserSearchFilters = null;
  // };

  // clearUserSearchFilter() {
  //   this.currentFilterData.userSearchFilter = null;
  // };

  public setFilterData(filterData: FilterData) {
    var _this = this;
    this.currentFilterData = new FilterData();
    if (filterData) {
      restoreFilter(filterData.pageRestrictionFilter, this.addPageRestrictionFilter);
      restoreFilter(filterData.userSearchFilter, this.addUserSearchFilter);
      restoreFilter(filterData.pageUserSearchFilters, this.addPageUserSearchFilter);
      restoreFilter(filterData.pageFilter, this.addPageFilter);
    }

    function restoreFilter(filterField, addFunc) {
      if (filterField) {
        const criteria = restoreFilterRecursive(filterField);
        addFunc.call(_this, criteria);
      }
    }

    function restoreFilterRecursive(filterField) {
      if (!filterField) {
        return null;
      }
      if (filterField.objType === 'SingleCriteria') {
        (<SingleCriteria>filterField).contextName = filterField.contextName ? filterField.contextName.value : null;
        (<SingleCriteria>filterField).operator = filterField.operator ? EFilterOperators.parse(filterField.operator.value) : null;
        return new SingleCriteria((<SingleCriteria>filterField).contextName, (<SingleCriteria>filterField).fieldName, (<SingleCriteria>filterField).value, (<SingleCriteria>filterField).operator, (<SingleCriteria>filterField).displayedText, (<SingleCriteria>filterField).displayedFilterType, (<SingleCriteria>filterField).originalCriteriaOperationId);
      }
      // single item compound criteria - return the single leaf only
      if (filterField.criterias.length == 1) {
        return restoreFilterRecursive(filterField.criterias[0]);
      }
      // Multiple items handling ...
      const criterias = [];
      let result;

      filterField.criterias.forEach(criteria => {
        const parts = restoreFilterRecursive(criteria);
        criterias.push(parts);
      });
      if (filterField.objType === 'AndCriteria') {
        result = new AndCriteria(criterias);
      } else if (filterField.objType === 'OrCriteria') {
        result = new OrCriteria(criterias);
      }
      return result;

    }


  };

  public toKendoFilter() {
    return this.currentFilterData.toKendoFilter();
  };

  getThinFilterCopy() {
    const thinFilterCopy = JSON.stringify(this.getFilterData(), function (key, value) {
      if (value == null) {
        return undefined;
      }
      return value;
    });
    return thinFilterCopy;
  }

  public addPageFilter(newCriteria: ICriteriaComponent) {
    this.currentFilterData.setPageFilters(newCriteria);

  };

  public addPageRestrictionFilter(newCriteria: ICriteriaComponent) {
    this.currentFilterData.setPageRestrictionFilters(newCriteria);

  };

  public addUserSearchFilter(newCriteria: ICriteriaComponent) {
    this.currentFilterData.setUserSearchFilter(newCriteria);

  };

  public addPageUserSearchFilter(newCriteria: ICriteriaComponent) {
    this.currentFilterData.setPageUserSearchFilters(newCriteria);

  };

  public findPageRestictionFilter(filterIdentifier) :ICriteriaComponent[]{
    var pageResFilters = this.getFilterData().getPageRestrictionFilter();
    if(pageResFilters&& pageResFilters.hasCriteria()) {
      return pageResFilters.getAllCriteriaLeafs().filter(f=>f.originalCriteriaOperationId == filterIdentifier);

    }
    return [];
  };

  // private removeFilter(criteriaOperation: CriteriaOperation, criteriaItemToRemove: ICriteriaComponent) {
  //   if (criteriaOperation === criteriaItemToRemove) {
  //     return null;
  //   }
  //   if (criteriaOperation) {
  //     criteriaOperation.removeCriteria(criteriaItemToRemove);
  //   }
  //   return criteriaOperation.criterias.length > 0 ? criteriaOperation : null;
  // };

}



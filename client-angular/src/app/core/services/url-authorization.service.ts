import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {authorizationConfig} from '@app/navigation/config/autorization-config';
import {DtoUserWrapper} from '@users/model/user.model';
import {ESystemRoleName} from '@users/model/system-role-name';
import {Observable, of} from 'rxjs/index';
import {first, map, skipWhile} from 'rxjs/operators';
import {LoginService} from '@services/login.service';
import {UrlRoles} from "@app/navigation/config/url-roles";
import {LicenseService} from "@services/license.service";



@Injectable()
export class UrlAuthorizationService {

  constructor(private loginService: LoginService, private router: Router, private licenseService: LicenseService) {
  }

  // addDashboardAuthorization = function():void  {
  //   this.loginService.currentUser$.pipe( //cannot be sure it happens before first menu set
  //     skipWhile(user => !user),
  //     first()
  //     , map((currentUser:DtoUserWrapper ) => {
  //
  //       this.router.config.forEach(route => {
  //         if (route.url === V1RouteUrl.DashboardSystem) {
  //           route.roles = [currentUser.internalDto.dashboards.AdminSysRole];
  //         }
  //         else if (route.url === V1RouteUrl.DashboardIT) {
  //           route.roles = [currentUser.internalDto.dashboards.ITSysRole];
  //         }
  //         else if (route.url === V1RouteUrl.DashboardUser) {
  //           route.roles = [ESystemRoleName.Not,currentUser.internalDto.dashboards.ITSysRole,currentUser.internalDto.dashboards.AdminSysRole];
  //         }
  //       });
  //
  //       authorizationConfig.forEach(config => {
  //         if (config.url === V1RouteUrl.DashboardSystem) {
  //           config.roles = [currentUser.internalDto.dashboards.AdminSysRole];
  //         }
  //         else if (config.url === V1RouteUrl.DashboardIT) {
  //           config.roles = [currentUser.internalDto.dashboards.ITSysRole];
  //         }
  //         else if (config.url === V1RouteUrl.DashboardUser) {
  //           config.roles = [ESystemRoleName.Not,currentUser.internalDto.dashboards.ITSysRole,currentUser.internalDto.dashboards.AdminSysRole];
  //         }
  //       });
  //       return true;
  //     })
  //   ).subscribe();
  // }

  authorizeUrl(absoluteUrl: string): Observable<boolean> {
    return this.loginService.currentUser$.pipe(
      skipWhile(user => !user),
      first(),
      map((currentUser) => {
        const result = this.authorizeUrlForUser(absoluteUrl, currentUser);
        return result;
      })
    )
  }

  isSupportRoles(authorizedSystemRoles: ESystemRoleName[]){
    return(this.isAuthorized(this.loginService.getCurrentUser(),authorizedSystemRoles));
  }

  authorizeRolesForCurrentUser(authorizedSystemRoles: ESystemRoleName[]): Observable<boolean> {
    return this.loginService.currentUser$.pipe(
      skipWhile(user => !user),
      first(),
      map((currentUser) => {
        const result = this.isAuthorized(currentUser,authorizedSystemRoles);
        return result;
      })
    )
  }


  private authorizeUrlForUser(url: string, user: DtoUserWrapper): boolean {
    if (!user) {
      return false;
    }
    // return true;

    if (!url.startsWith('/')) {
      url = `/${url}`;
    }

    authorizationConfig.forEach(config => {
      if (!config.url.startsWith('/')) {
        config.url = `/${config.url}`;
      }
    });

    const candidateRoles: UrlRoles [] = authorizationConfig.filter(config => url.toLowerCase().startsWith(config.url.toLowerCase()));


    if (!candidateRoles || candidateRoles.length == 0 ) {
      console.warn(`No roles found for ${url}`);
      return true;
    }
    let bestUrlRole:UrlRoles = null;

    //find the longest role that matches the current url.
    candidateRoles.forEach(goUrlRole=> {
      if (!bestUrlRole || bestUrlRole.url.length < goUrlRole.url.length) {
        bestUrlRole = goUrlRole;
      }
    });
    if((this.licenseService.loginExpired || user.internalDto.loginLicenseExpired) && !bestUrlRole.showOnLoginLicenseExpired){
      return false;
    }
    const result = this.isAuthorized(user, bestUrlRole.roles);
    return result;
  }

   private isAuthorized(userData: DtoUserWrapper, authorizedSystemRoles: ESystemRoleName[], withFunctionalRoleIds ?: number[], withAllFuncRoles ?: boolean): boolean {

    if (userData) {

      if (authorizedSystemRoles.indexOf(ESystemRoleName.None)>-1){
        return true;
      }

      let inNotSearch:boolean = false;
      if (authorizedSystemRoles.indexOf(ESystemRoleName.Not)>-1){
        inNotSearch = true;
      }

      let foundBizRelatedRole: boolean = !!authorizedSystemRoles.find(role => !!userData.bizRolesMap[role]);

      if (inNotSearch && foundBizRelatedRole) {
        return false;
      }

      if (!foundBizRelatedRole) {
        if (inNotSearch) {
          return true;
        }
        if (withAllFuncRoles) {
          withFunctionalRoleIds = userData.internalDto.funcRoleDtos.map(functionalRole => functionalRole.id);
        }
        if (withFunctionalRoleIds && withFunctionalRoleIds[0]) {
          const userFuncRolesIds = withFunctionalRoleIds.filter(a => !!userData.funcRoleMap[a]);
          if (userFuncRolesIds.length > 0) {
            let i = 0;
            while (!foundBizRelatedRole && i < userFuncRolesIds.length) {
              const userFuncRolesId = userFuncRolesIds[i];
              i++;
              const sysRoleByNameMap = userData.funcRoleMap[userFuncRolesId];
              const foundSysRole = !!authorizedSystemRoles.find(a => !!sysRoleByNameMap[a]); // At least one was found
              if (foundSysRole) {
                foundBizRelatedRole = true;
              }

            }
          }
        }
      }
      return foundBizRelatedRole;
    }
    return false;

  }


}

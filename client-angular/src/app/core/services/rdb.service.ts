import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {LogoutAction} from '@app/core/store/authentication.actions';
import {DtoUserWrapper} from '@app/common/users/model/user.model';
import {delay, first, map, publishReplay, refCount, shareReplay, skipWhile, tap} from "rxjs/internal/operators";
import {DtoDocAuthorityLicense, DtoDocauthorityLicenseWrapper} from "@core/types/dto-docauthority-license";
import {ITemplateRoleContainer} from "@users/model/template-role-container";
import {DtoUser} from "@users/model/user.model";
import {DtoSystemRole} from "@users/model/system-role.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {Router} from "@angular/router";
import {RoutingService} from "@services/routing.service";
import {AppConfig} from "@services/appConfig.service";
import {CommonConfig, ServerUrls} from "@app/common/configuration/common-config";

@Injectable()
export class RdbService {

  constructor() {
  }

  parseServerURL(fullUrl, paramObject = null) {
    let ans =  fullUrl;
    if (ans != null && paramObject) {
      let allKeys = Object.keys(paramObject);
      allKeys.forEach((key)=> {
        ans = this.replaceAll(ans,"{"+key+"}",paramObject[key]);
      });
    }
    return ans;
  }

  getQueryForAllItems(fullUrl) {
    return fullUrl +"?take="+ CommonConfig.MAX_FIRST_LEVEL_ITEMS_IN_TREE +"&pageSize=" + CommonConfig.MAX_FIRST_LEVEL_ITEMS_IN_TREE;
  }

  replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

}



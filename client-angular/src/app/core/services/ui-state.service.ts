import { Injectable } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/index';
import {isDialogOpenSelector} from '@app/core/store/core.selectors';

@Injectable()
export class UiStateService {

  constructor(private store: Store<any> ) { }

  isDialogOpen$(): Observable<boolean> {
    return  this.store.select(isDialogOpenSelector)
  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, concatMap} from 'rxjs/operators';
import {ChartSeries, SingleChartSeriesData} from "@app/common/charts/types/common-charts-interfaces";

@Injectable()
export class PrometheusService {

  constructor(private http: HttpClient) {
  }


  getUnixTimestamp(theDateInMilliSec:number):number {
    let ts = Math.round(theDateInMilliSec / 1000);
    return ts;
  }

  queryByIndicator(indicator, startTimeInMilliSec,endTimeInMilliSec,stepInSec) {
    return this.http.get('http://localhost:9090/api/v1/query_range?query='+indicator+'&start='+this.getUnixTimestamp(startTimeInMilliSec)+'&end='+this.getUnixTimestamp(endTimeInMilliSec)+'&step='+stepInSec);
  }


  convertPrometheusResultToChartSeriesData(proDataArray: any[], valueFields: string[], colors: string[]): ChartSeries {
    let ans: any[] = [];
    for (let count = 0 ; count < proDataArray.length ; count++) {
      let dataArr = [];
      proDataArray[count].data.result[0].values.forEach(([time, data]) => {
        dataArr.push([(Number(time) * 1000), Number(data)]);
      });
      ans.push({
        name: valueFields[count],
        data: dataArr,
        color: colors[count]
      })
    }
    return ans;
  }

}



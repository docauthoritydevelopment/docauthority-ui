import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SystemSettingsDto} from "@core/types/systemSettings.dto";
import {CommonConfig} from "@app/common/configuration/common-config";
@Injectable()
export class AppConfig {
  constructor(private http: HttpClient) {}

  updateConfigurationBySystemSettingsDto(responseList : SystemSettingsDto[]) {
    if (Array.isArray(responseList)) {
      responseList.forEach((resItem: SystemSettingsDto) => {
        let fieldName = resItem.name;
        CommonConfig[fieldName] = resItem.value;
      });
    }
  }

  isInstallationModeStandard():boolean{
    return CommonConfig['installation-mode'] == 'standard';
  }

  isInstallationModeLegal():boolean{
    return CommonConfig['installation-mode'] == 'legal';
  }

  isLabelingEnabled():boolean{
    return CommonConfig['labeling-enabled'] == 'true';
  }
  isMetadataEnabled():boolean{
    return CommonConfig['metadata-enabled'] == 'true';
  }


  /* load() {
    const serverRequest = 'api/system/settings/prefix?prefix='
    return new Promise<void>((resolve, reject) => {
      this.http.get(serverRequest).toPromise().then((responseList : SystemSettingsDto[]) => {
        responseList.forEach((resItem:SystemSettingsDto)=> {
          let fieldName = resItem.name;
          CommonConfig[fieldName] = resItem.value;
        });
        resolve();
      }).catch((response: any) => {
        reject(`Could not perform request '${serverRequest}': ${JSON.stringify(response)}`);
      });
    });
  }  */
}

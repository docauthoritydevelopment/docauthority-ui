import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {BehaviorSubject, Observable, Subject} from 'rxjs/index';
import {LogoutAction} from '@app/core/store/authentication.actions';
import {DtoUserWrapper} from '@app/common/users/model/user.model';
import {delay, first, map, publishReplay, refCount, shareReplay, skipWhile, tap} from "rxjs/internal/operators";
import {DtoDocAuthorityLicense, DtoDocauthorityLicenseWrapper} from "@core/types/dto-docauthority-license";
import {ITemplateRoleContainer} from "@users/model/template-role-container";
import {DtoUser} from "@users/model/user.model";
import {DtoSystemRole} from "@users/model/system-role.dto";
import {ESystemRoleName} from "@users/model/system-role-name";
import {Router} from "@angular/router";
import {RoutingService} from "@services/routing.service";
import {AppConfig} from "@services/appConfig.service";

@Injectable()
export class LoginService {

  // private CACHE_SIZE = 1;
  // private cache$:Observable<DtoUserWrapper>;
  private currentUserSubject$ = new BehaviorSubject<DtoUserWrapper>(null);

  constructor(private http: HttpClient, private store: Store<any>,private routingService : RoutingService, private appConfig : AppConfig) {
    this.init();
  }


  private init() {

      this.http.get<DtoUser>( '/userinfo?tmpTimeAng2'+(new Date()).getTime()).subscribe(
        userInfo=>{
          const dtoUserWrapper = this.parseData(userInfo);
          this.currentUserSubject$.next(dtoUserWrapper);
        },
        error => console.error(error)
      );
  }

  login(username: string, password: string ) {
    this.http.post('/login',
      {
        username: username,
        password: password
      })
  }

  getCurrentUser() {
    return this.currentUserSubject$.getValue();
  }

  logout() {
     let currUrl = this.routingService.getCurrentUrl();
    // this.routingService.clearAllParametersWithoutReload();
    this.store.dispatch(new LogoutAction(currUrl));
   }

  get currentUser$(): Observable<DtoUserWrapper> {
  return this.currentUserSubject$.asObservable();
}
  //
  // getCurrentUser$(): Observable<DtoUserWrapper> {
  //   return this.store.select(getCurrentUserSelector)
  // }

  private parseData(dtoUser: DtoUser): DtoUserWrapper {
    const result: DtoUserWrapper = new DtoUserWrapper(dtoUser);

    result.bizRolesMap = this.getSystemRoles(dtoUser.bizRoleDtos);
    result.funcRoleMap = this.getSystemRoles(dtoUser.funcRoleDtos);
    this.appConfig.updateConfigurationBySystemSettingsDto(dtoUser.configurationList);

    return result;

  }

  private getSystemRoles(roleContainer: ITemplateRoleContainer[]): { [name: string]: DtoSystemRole } {
    const result: { [name: string]: DtoSystemRole } = {};

    roleContainer.forEach(role => {
      role.template.systemRoleDtos.forEach(sysRole => {
        result[sysRole.name.toString()] = sysRole;
      });
    })
    return result;
  }

}



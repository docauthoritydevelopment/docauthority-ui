import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of, Subject} from "rxjs";
import {DTOPagingResults} from "../types/query-paging-results.dto";
import {catchError, map, tap} from "rxjs/operators";
import {ScheduledOperationMetadataDto} from "../../common/discover/types/scheduledOperationMetadata-dto";
import {ServerUrls} from "../../common/configuration/common-config";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {ServerExportFile} from "@core/types/serverExport.dto";
import {RdbService} from "@services/rdb.service";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";



@Injectable({
  providedIn: 'root',
})
export class ScheduledOperationsService {
  private changeEvent = new Subject<number>();
  changeEvent$ = this.changeEvent.asObservable();

  constructor(private http: HttpClient, private rdbService:RdbService, private dialogService: DialogService) {
  }

  listOperations(): Observable<ScheduledOperationMetadataDto[]> {

    return this.http.get<ScheduledOperationMetadataDto[]>(ServerUrls.DISCOVER_ASYNC_USER_OPERATIONS, {})
      .pipe(
        tap((data)=> {
          return data;
        })
        , catchError(err => {
            console.error(err);
            return Observable.throw(err)
        }));
  }

  updateOperations(){
       this.changeEvent.next();
  }

  deleteOperation(process:ScheduledOperationMetadataDto) {
    this.http.delete(this.rdbService.parseServerURL(ServerUrls.SCHEDULE_OPERATIONS_DELETE, {'id' : +process.id})).subscribe((ans: any) => {
      this.updateOperations();
    }, (err)=>{
      this.dialogService.showAlert("Error in delete", 'Error in delete process : '+process.description, AlertType.Error  , err);
    });
  }
}

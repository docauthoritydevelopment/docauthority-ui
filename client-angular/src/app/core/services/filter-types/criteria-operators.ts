export class ECriteriaOperators {
  // values
  static and = new ECriteriaOperators('AND');
  static or = new ECriteriaOperators('OR');

  constructor(public value: string) {
  }

  toString() {
    return this.value;
  }

}

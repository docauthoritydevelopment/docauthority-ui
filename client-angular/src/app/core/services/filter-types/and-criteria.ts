import {ICriteriaComponent} from './criteria-component.interface';
import {CriteriaOperation} from './criteria-operation';

export class AndCriteria extends CriteriaOperation {
  public objType = 'AndCriteria'; // for serialization
  constructor(criteriaOperation: ICriteriaComponent[], otherCriteriaOperation?: ICriteriaComponent[]) {
    super('and', criteriaOperation, otherCriteriaOperation);
  }


}

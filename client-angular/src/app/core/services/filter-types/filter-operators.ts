export class EFilterOperators {
  // values
  static equals = new EFilterOperators('eq');
  static notEquals = new EFilterOperators('neq');
  static contains = new EFilterOperators('contains');
  static doesNotContain = new EFilterOperators('doesnotcontain');
  static greaterThen = new EFilterOperators('ge');
  static lessThen = new EFilterOperators('lt');

  static operators = [EFilterOperators.equals, EFilterOperators.notEquals, EFilterOperators.contains,
    EFilterOperators.doesNotContain, EFilterOperators.greaterThen, EFilterOperators.lessThen];

  constructor(public value: string) {
  }

  static parse(str: string) {
    return EFilterOperators.operators.find(operator => str === operator.toString());
  }


  toString() {
    return this.value;
  }
}

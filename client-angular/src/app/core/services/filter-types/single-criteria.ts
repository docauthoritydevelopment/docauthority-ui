import {EFilterOperators} from './filter-operators';
import {ICriteriaComponent} from './criteria-component.interface';

export class SingleCriteria implements ICriteriaComponent {
  public objType = 'SingleCriteria'; // for serialization

  constructor(public contextName: string, public fieldName: string, public value: any, public operator: EFilterOperators,
              public displayedText?: string, public displayedFilterType?: string, public originalCriteriaOperationId?: any) {

  }

  getAllCriteriaLeafs() {
    return [this];
  };

  getAllUniqueCriteriaOperationLeafs(propertiesUtils) {
    return [this];
  };

  getType() {
    return 'SingleCriteria'
  };

  removeCriteria(criteria) {
    return false;
  };

  toKendoFilter() {
    return {
      field: this.contextName ? `${this.contextName}.${this.fieldName}` : this.fieldName,
      operator: this.operator.toString(),
      value: this.value
    };
  };

  hasCriteria() {
    return this.fieldName !== null && this.value !== null;
  };


}

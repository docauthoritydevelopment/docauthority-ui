import {SingleCriteria} from './single-criteria';

export interface ICriteriaComponent {
  toKendoFilter(): any;

  hasCriteria(): boolean;

  getType(): string;

  removeCriteria(criteria: ICriteriaComponent): boolean;

  // removeCriteriaOperation(criteriaToRemove:CriteriaOperation):void;
  getAllCriteriaLeafs(): SingleCriteria[];

  getAllUniqueCriteriaOperationLeafs(propertiesUtils): SingleCriteria[];
}

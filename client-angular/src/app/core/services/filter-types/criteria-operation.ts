import {ICriteriaComponent} from './criteria-component.interface';
import {SingleCriteria} from './single-criteria';

export class CriteriaOperation implements ICriteriaComponent {
  criterias: ICriteriaComponent[];
  operator: string;
  getType = function () {
    return 'CriteriaOperation'
  };



  removeCriteria=function(criteria:ICriteriaComponent)
  {
    var found = -1;

    for (var i=0;i<this.criterias.length;++i) {
      if (criteria === this.criterias[i]) {
        found = i;
        break;
      }
    }
    if (found>=0) {
      this.criterias.splice(found, 1);

      return true;
    }
    for (let i = 0; i < this.criterias.length; i++)
      {
      var c = this.criterias[i];
      if (c.removeCriteria(criteria)) {
        if (c.criterias.length == 1)
        {  this.criterias[i] = c.criterias[0]; // cascade empty criterias
        } else if (c.criterias.length == 0)
        {  this.criterias.splice(i, 1);// c ascade empty criterias
        }
        return true;
      }
    }

    return false;
  };

  constructor(operator: string, criteriaOperation?: ICriteriaComponent[], otherCriteriaOperation?: ICriteriaComponent[]) {

    this.criterias = [];
    if (criteriaOperation) {
      this.criterias = criteriaOperation;
    }
    this.operator = operator;

    if (otherCriteriaOperation) {
      this.criterias = this.criterias.concat(otherCriteriaOperation);
    }
  }

  toKendoFilter() {
    if (!this.hasCriteria()) {
      return null;
    }
    // single item compound criteria - return the single leaf only
    if (this.criterias.length === 1) {
      return this.criterias[0].toKendoFilter();
    }
    // Multiple items handling ...
    const result = {
      logic: this.operator,
      filters: []
    };

    this.criterias.forEach(c => {
      const parts = c.toKendoFilter();
      result.filters.push(parts);
    });
    return result;
  };

  hasCriteria() {
    return (this.criterias.length > 0);
  };

  getAllCriteriaLeafs() {
    const result: SingleCriteria[] = [];
    this.criterias.forEach(c => {
      const parts = c.getAllCriteriaLeafs();
      result.push(...parts);
    });
    return result;
  };


  getAllUniqueCriteriaOperationLeafs(propertiesUtils) {
    const allLeafs = this.getAllCriteriaLeafs();
    if (allLeafs && allLeafs.length > 0) {
      return propertiesUtils.unique(allLeafs, function (criteria: SingleCriteria) {
        return criteria.originalCriteriaOperationId;
      });
    }
    return [];
  };


}

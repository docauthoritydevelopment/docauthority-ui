import {CriteriaOperation} from 'app/core/services/filter-types/criteria-operation';
import {ICriteriaComponent} from 'app/core/services/filter-data';

export class OrCriteria extends CriteriaOperation {
  public objType = 'OrCriteria'; // for serialization
  constructor(criteriaOperation: ICriteriaComponent[], otherCriteriaOperation?: ICriteriaComponent[]) {
    super('or', criteriaOperation, otherCriteriaOperation);
  }
}

export interface IFilterData {
  hasCriteria(): boolean;

  toKendoFilter(): any;
}

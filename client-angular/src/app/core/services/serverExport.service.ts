import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {CommonConfig} from "@app/common/configuration/common-config";
import * as _ from 'lodash';
import {FileTypes} from "@core/types/file.types";
import {ExportUserParams, FileExportRequestDto, ServerExportFile} from "@core/types/serverExport.dto";
import {ServerExportStatus} from "@core/types/serverExport.types";
import {map, concatMap} from 'rxjs/operators';
import {Observable, forkJoin} from 'rxjs/index';
import {DatePipe} from "@angular/common";
import {AdvancedExportDialogComponent} from "@app/common/export/popups/advancedExportDialog/advancedExportDialog.component";

@Injectable()
export class ServerExportService implements OnDestroy{

  private addExportFileEvent= new Subject<ServerExportFile>();
  private refreshExportFileListEvent= new Subject<string>();
  addExportFileEvent$ = this.addExportFileEvent.asObservable();
  refreshExportFileListEvent$ = this.refreshExportFileListEvent.asObservable();
  progressIdsMap = {};

  exportList: ServerExportFile[] = [];
  timeOutListener = null;
  uniqueId: number = null;
  deletedFilesMap = {};

  constructor(private http: HttpClient, private dialogService: DialogService, private datePipe: DatePipe,) {
    let now = new Date();
    this.uniqueId = now.getTime();
    setTimeout(()=>{
      this.loadExportList();
    },CommonConfig.TIME_IN_MILI_SEC_BEFORE_GET_EXPORT_LIST)
  }

  stopRefreshList(){
    if (this.timeOutListener) {
      window.clearTimeout(this.timeOutListener);
      this.timeOutListener = null;
    }
  }

  getExportUniqueId():number {
    return this.uniqueId;
  }

  checkFileIdExist(fileId): Observable<boolean> {
    return this.http.get('/api/exporter2/list').pipe(map((exList: ServerExportFile[])=> {
      return exList.filter(exItem => exItem.id == fileId).length > 0;
    }));
  }

  loadExportList = ()=> {
    this.stopRefreshList();
    this.http.get('/api/exporter2/list').subscribe((exList: any[])=> {
      this.exportList = exList;
      let newProgressIdsMap = {};
      let newDeletedMap = {};
      this.exportList.forEach((exItem: ServerExportFile)=> {
        if (exItem.status == ServerExportStatus.IN_PROGRESS ||  exItem.status == ServerExportStatus.NEW) {
          newProgressIdsMap[exItem.id] = true;
        }
        else if (this.progressIdsMap[exItem.id] && (exItem.status == ServerExportStatus.FAILED ||  exItem.status == ServerExportStatus.DONE)) {
          exItem.changedLately = true;
        }
        if (this.deletedFilesMap[exItem.id] != null && this.deletedFilesMap[exItem.id]<CommonConfig.export_delete_wait_cycles) {
          exItem.wasDeleted = true;
          newDeletedMap[exItem.id] = this.deletedFilesMap[exItem.id]+1;
        }
      });
      this.deletedFilesMap = newDeletedMap;
      this.progressIdsMap = newProgressIdsMap;
      this.refreshExportFileListEvent.next();
      this.timeOutListener = window.setTimeout(this.loadExportList, this.progressIdsMap && Object.keys(this.progressIdsMap).length > 0 ?  CommonConfig.EXPORT_LIST_REFRESH_PERIOD_WHEN_PROCESSING :  CommonConfig.EXPORT_LIST_REFRESH_PERIOD);
    });
  };

  ngOnDestroy() {
    this.stopRefreshList();
  }

  markAsDeleted(file:ServerExportFile) {
    file.wasDeleted = true;
    this.deletedFilesMap[file.id] = 1;
  }

  deleteExportFile(file:ServerExportFile) {
    this.http.delete('/api/exporter2/export/'+file.id).subscribe((ans: any) => {
      this.loadExportList();
    }, (err)=>{
      this.dialogService.showAlert("Error in export", 'Error in delete file: '+file.userParams.fileName+', file already deleted.', AlertType.Error  , err);
    });
  }

  updateNewExportFile(fileName, type, exportParams, userParams) {
    let paramsObject:FileExportRequestDto  = {
      fileNamePrefix : fileName,
      exportParams : exportParams,
      userParams : userParams
    };

    let sExServerExportFile:ServerExportFile = {
      status : ServerExportStatus.NEW,
      progress : 0 ,
      userParams : userParams
    };
    this.exportList.splice(0,0,sExServerExportFile);
    this.addExportFileEvent.next(sExServerExportFile);
    this.http.post('/api/exporter2/export/'+type,paramsObject).subscribe((ans: any) => {
      this.progressIdsMap[ans.id] = true;
      this.loadExportList();
    }, (err)=>{
      this.dialogService.showAlert("Error in export", 'Error in export : '+fileName, AlertType.Error , err);
      this.loadExportList();
    });
  }

  prepareExportFile = (fileNamePrefix, type, exportParams, userParams,useAdvancedMode:boolean = false) => {
    this.addExportFile(fileNamePrefix + '_'+this.datePipe.transform((new Date()).getTime(),CommonConfig.DATE_FORMAT),type,exportParams,userParams,useAdvancedMode);
  };

  addExportFile = (fileName, type, exportParams, userParams,useAdvancedMode:boolean = false, suggestItemNumber: number = null)=>  {
    let updatedUserParams:ExportUserParams = _.cloneDeep(userParams);
    updatedUserParams.fileName = fileName;
    updatedUserParams.fileType = FileTypes.msExcel;
    let updatedExportParams  = _.cloneDeep(exportParams);
    if (!updatedExportParams.page) {
      updatedExportParams.page = 1;
    }
    if (!updatedExportParams.skip ) {
      updatedExportParams.skip = 0 ;
    }
    if (!updatedExportParams.pageSize) {
      updatedExportParams.pageSize = userParams.itemsNumber ? userParams.itemsNumber : CommonConfig.MAX_EXCEL_RECORDS_IN_FILE;
    }

    if (!updatedExportParams.take) {
      updatedExportParams.take = userParams.itemsNumber ? userParams.itemsNumber : CommonConfig.MAX_EXCEL_RECORDS_IN_FILE;
    }

    if (useAdvancedMode) {
      this.dialogService.openModalPopup(AdvancedExportDialogComponent,{data : {type: type , exportParams : updatedExportParams, userParams : updatedUserParams, suggestItemNumber : suggestItemNumber}}).result.then((updatedObject : {exportParams : any ,userParams : any}) =>
      {
        if (updatedObject != null) {
          updatedObject.userParams.uniqueId = this.getExportUniqueId();
          this.updateNewExportFile(updatedObject.userParams.fileName, type, updatedObject.exportParams, updatedObject.userParams)
        }
      }, (reason) => {
      });
    }
    else {
      updatedUserParams.downloadWhenReady = "true";
      updatedUserParams.uniqueId = this.getExportUniqueId();
      this.updateNewExportFile(fileName, type, updatedExportParams, updatedUserParams)
    }
  };

  getExportList() {
    return this.exportList;
  }
}

import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, forkJoin, Subject} from 'rxjs/index';

@Injectable()
export class ServerOperations {
  runningQueriesMap = {};

  constructor( private http: HttpClient)
  {
  }


  getData(url:string, params:any={}, urlParams:any={}): Observable<any> {
    let urlParamsKeys = Object.keys(urlParams);
    urlParamsKeys.forEach((urlParam)=> {
      url.replace(':'+urlParam, urlParamsKeys[urlParam])
    });
    return this.http.get(url, params);
  }


}

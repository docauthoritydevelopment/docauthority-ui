import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subject, timer} from 'rxjs/index';
import {DtoDocauthorityLicenseWrapper} from '@app/core/types/dto-docauthority-license';
import {
  delay, first,
  map,
  publishReplay,
  refCount,
  share,
  shareReplay,
  skipWhile,
  startWith,
  tap
} from "rxjs/internal/operators";
import {HttpClient} from "@angular/common/http";
import {DtoDocAuthorityLicense} from "@core/types/dto-docauthority-license";
import {RoutingService} from "@services/routing.service";
import {V1RouteUrl} from "@angular-1-routes/model/v1.route.url";
import {LoginService} from "@services/login.service";



@Injectable({
  providedIn: 'root',
})
export class LicenseService {

  // private CACHE_SIZE = 1;
  // private cache$:Observable<DtoDocauthorityLicenseWrapper>;
  private loginExpiredSubject$ = new BehaviorSubject(false);
  private licenseSubject$ = new BehaviorSubject<DtoDocauthorityLicenseWrapper>(null);





  constructor( private routingService:RoutingService,private loginService:LoginService,
              private http: HttpClient,)
  {
    console.log('LicenseService constructor');
    this.init();
  }

  private init() {
    this.loginService.currentUser$.pipe(
      skipWhile(user => !user), //first subscription user is null
      first()
    ).subscribe(user=>{
      if (this.loginExpiredSubject$.getValue()!=user.internalDto.loginLicenseExpired) {
        this.loginExpiredSubject$.next(user.internalDto.loginLicenseExpired);
      }
    });
    setTimeout(() => {
      this.getLicense().subscribe(
        lic=>{
          this.licenseSubject$.next(lic);

          if (this.loginExpiredSubject$.getValue()!=lic.loginExpired) {

            this.loginExpiredSubject$.next(lic.loginExpired)
          } //fire this event only if there is a change from initial value
        },
        error => console.error(error)
      );
    }, 20000); //postpone loading after app loaded

  }

  get loginExpired$():  Observable<boolean>{
    return this.loginExpiredSubject$.asObservable();  //asObservable is needed that when someone gets the observable result  they can't next values
  }
  get loginExpired():boolean{
    return this.loginExpiredSubject$.getValue();
  }
  get license$(): Observable<DtoDocauthorityLicenseWrapper> {
  return this.licenseSubject$.asObservable();
  }
  private getLicense(): Observable<DtoDocauthorityLicenseWrapper>{
   return this.http.get<DtoDocAuthorityLicense>('/api/license/last?3232323').pipe(
     map(data => new DtoDocauthorityLicenseWrapper(data)),

   );
  }



}

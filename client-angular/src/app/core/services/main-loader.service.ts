import { Injectable } from '@angular/core';
import {Observable, forkJoin, Subject} from 'rxjs/index';



@Injectable()
export class MainLoaderService {

  private loaderChangeEvent = new Subject<boolean>();
  loaderChangeEvent$ = this.loaderChangeEvent.asObservable();

  constructor() {
  }

  setMainLoader(doShow:boolean) {
    this.loaderChangeEvent.next(doShow);
  }
}

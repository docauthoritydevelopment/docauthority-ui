import {AndCriteria} from './filter-types/and-criteria';
import {EFilterOperators} from './filter-types/filter-operators';
import {CriteriaOperation} from './filter-types/criteria-operation';

export interface IFilterData {
  hasCriteria(): boolean;

  toKendoFilter(): any;
}

export class FilterData implements IFilterData {
  pageRestrictionFilter: CriteriaOperation; // from prev page selection
  userSearchFilter: ICriteriaComponent;
  pageUserSearchFilters: CriteriaOperation;
  pageFilter: CriteriaOperation; // column filter
  hasCriteria = function () {
    return ((this.pageRestrictionFilter ? this.pageRestrictionFilter.hasCriteria() : false) ||
      (this.pageFilter ? this.pageFilter.hasCriteria() : false) ||
      (this.userSearchFilter ? this.userSearchFilter.hasCriteria() : false) ||
      (this.pageUserSearchFilters ? this.pageUserSearchFilters.hasCriteria() : false));

  };
  getPageRestrictionFilter = function () {
    return this.pageRestrictionFilter;
  };
  getPageFilter = function () {
    return this.pageFilter;
  };
  getUserSearchFilter = function () {
    return this.userSearchFilter;
  };
  getPageUserSearchFilters = function () {
    return this.pageUserSearchFilters;
  };

  getSingleCriteriaFromPageFilter = function (fieldName: string, value: any) {
    if (this.pageFilter) {
      const allLeafts: SingleCriteria[] = this.pageFilter.getAllCriteriaLeafs();
      for (let count = 0; count < allLeafts.length; count++) {
        if (allLeafts[count].fieldName === fieldName && allLeafts[count].value === value) {
          return allLeafts[count];
        }
      }
      return null;
    }
    return null;
  }

  removeCriteriaFromPageFilter = function (criteriaItemToRemove: SingleCriteria) {
    const allCriteriaLeaf = this.pageFilter.getAllCriteriaLeafs();
    let foundCreiteria: SingleCriteria = null;
    for (let count = 0; count < allCriteriaLeaf.length; count++) {
      if (allCriteriaLeaf[count].fieldName === criteriaItemToRemove.fieldName &&
        allCriteriaLeaf[count].value === criteriaItemToRemove.value && allCriteriaLeaf[count].operator === criteriaItemToRemove.operator) {
        foundCreiteria = allCriteriaLeaf[count];
        break;
      }
    }
    if (!foundCreiteria) {
      return null;
    }

    if (foundCreiteria === this.pageFilter) {
      this.pageFilter = null;
      return null;
    }
    ;

    if (this.pageFilter) {
      this.pageFilter.removeCriteria(foundCreiteria);
    }
    if (this.pageFilter.criterias.length == 0) {
      this.pageFilter = null;
    }
  };
  setPageRestrictionFilters = function (pageRestrictionFilter: ICriteriaComponent) {
    if (this.pageRestrictionFilter) {
      this.pageRestrictionFilter = new AndCriteria([pageRestrictionFilter], this.pageRestrictionFilter);
    } else {
      this.pageRestrictionFilter = pageRestrictionFilter;
    }
  };
  setPageUserSearchFilters = function (pageUserSearchFilters: ICriteriaComponent) {

    if (this.pageUserSearchFilters) {
      this.pageUserSearchFilters = new AndCriteria([pageUserSearchFilters], this.pageUserSearchFilters);
    } else {
      this.pageUserSearchFilters = pageUserSearchFilters;
    }

  };
  setUserSearchFilter = function (userSearchFilter: ICriteriaComponent) {
    this.userSearchFilter = userSearchFilter;
  };
  setPageFilters = function (pageFilter: ICriteriaComponent) {
    if (this.pageFilter) {
      this.pageFilter = new AndCriteria([pageFilter], this.pageFilter);
    } else {
      this.pageFilter = pageFilter;
    }
  };
  toKendoFilter = function () {
    const allFilters: CriteriaOperation[] = [];
    if (this.userSearchFilter) {
      allFilters.push(this.userSearchFilter);
    }
    if (this.pageUserSearchFilters) {
      allFilters.push(this.pageUserSearchFilters);
    }
    if (this.pageRestrictionFilter) {
      allFilters.push(this.pageRestrictionFilter);
    }
    if (this.pageFilter) {
      allFilters.push(this.pageFilter);
    }
    let result = null;
    if (allFilters.length === 1) {
      result = allFilters[0].toKendoFilter();
    } else if (allFilters.length > 1) {
      const first = allFilters[0];
      allFilters.splice(0, 1);
      const theFullCriteria = new AndCriteria([first], allFilters);
      result = theFullCriteria.toKendoFilter();
    }
    return result;
  };

  constructor() {

  }

}

export interface ICriteriaComponent {
  toKendoFilter(): any;

  hasCriteria(): boolean;

  getType(): string;

  removeCriteria(criteria: ICriteriaComponent): boolean;

  // removeCriteriaOperation(criteriaToRemove:CriteriaOperation):void;
  getAllCriteriaLeafs(): SingleCriteria[];

  getAllUniqueCriteriaOperationLeafs(propertiesUtils): SingleCriteria[];
}


export class SingleCriteria implements ICriteriaComponent {
  public objType = 'SingleCriteria'; //for serialization
  toKendoFilter = function () {
    return {
      field: this.contextName ? this.contextName.toString() + '.' + this.fieldName : this.fieldName,
      operator: this.operator.toString(),
      value: this.value
    };
  };
  hasCriteria = function () {
    return this.fieldName != null && this.value != null;
  };
  getAllCriteriaLeafs = function () {
    return [this];
  };
  getAllUniqueCriteriaOperationLeafs = function (propertiesUtils) {
    return [this];
  };
  getType = function () {
    return 'SingleCriteria'
  };
  removeCriteria = function (criteria) {
    return false;
  };

  constructor(public contextName: string, public fieldName: string, public value: any, public operator: EFilterOperators, public displayedText?: string, public displayedFilterType?: string, public originalCriteriaOperationId?: any) {

  }

  //removeCriteriaOperation(criteriaToRemove:CriteriaOperation)
  //{
  //  return null; //na
  //}

}


/*
export class CriteriaOperation implements ICriteriaComponent {
  criterias: ICriteriaComponent[];
  operator:string;
  constructor(operator:string, criteriaOperation?:ICriteriaComponent[],otherCriteriaOperation?:ICriteriaComponent[])
  {

    this.criterias=[];
    if(criteriaOperation) {
      this.criterias=criteriaOperation;
    }
    this.operator=operator;

    if(otherCriteriaOperation) {
      this.criterias= this.criterias.concat(otherCriteriaOperation);
    }
  }
  getType =function()
  {
    return "CriteriaOperation"
  };
  hasCriteria=function()
  {
    return (this.criterias.length>0 );
  };
  getAllCriteriaLeafs = function(): SingleCriteria[]
  {
    var result:SingleCriteria[] =[];
    this.criterias.forEach(c=> {
      var parts = c.getAllCriteriaLeafs();
      result = result.concat(parts);
    });
    return result;
  };

  getAllUniqueCriteriaOperationLeafs = function(propertiesUtils)
  {
    var allLeafs = this.getAllCriteriaLeafs();
    if(allLeafs&&allLeafs.length>0) {
      return propertiesUtils.unique(allLeafs, function (criteria:SingleCriteria) {
        return criteria.originalCriteriaOperationId;
      });
    }
    return [];
  };



  removeCriteria=function(criteria:ICriteriaComponent)
  {
    var found = -1;

    for (var i=0;i<this.criterias.length;++i) {
      if (criteria === this.criterias[i]) {
        found = i;
        break;
      }
    }
    if (found>=0) {
      this.criterias.splice(found, 1);

      return true;
    }
    for(var i=0; i<this.criterias.length; i++)
    {
      var c= this.criterias[i];
      if (c.removeCriteria(criteria)) {
        if(c.criterias.length==1 )
        {
          this.criterias[i]= c.criterias[0]; //cascade empty criterias
        }
        else if(c.criterias.length==0 )
        {
          this.criterias.splice(i, 1);//cascade empty criterias
        }
        return true;
      }
    }

    return false;
  };

  toKendoFilter=function()
  {
    if(!this.hasCriteria())
    {
      return null;
    }
    // single item compound criteria - return the single leaf only
    if (this.criterias.length==1) {
      return this.criterias[0].toKendoFilter();
    }
    // Multiple items handling ...
    var result = {
      logic: this.operator,
      filters:  []
    };

    this.criterias.forEach(c=> {
      var parts = c.toKendoFilter();
      result.filters.push(parts);
    });
    return result;
  };
  //{
//  logic: "or",
//    filters: [
//  { field: "fieldA", operator: "eq", value: 100 },
//  {
//    logic: "and",
//    filters: [
//      { field: "fieldA", operator: "lt", value: 100 },
//      { field: "fieldB", operator: "eq", value: true }
//    ]
//  }
//]
//}

}
*/

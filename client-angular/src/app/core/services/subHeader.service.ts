import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/index';


@Injectable({
  providedIn: 'root'
})
export class SubHeaderService{

  private subHeaderChangedEvent = new Subject<string>();
  subHeaderChangedEvent$ = this.subHeaderChangedEvent.asObservable();

  setSubHeader(innerHTML:string){
    this.subHeaderChangedEvent.next(innerHTML);
  }

  constructor() {
  }

}

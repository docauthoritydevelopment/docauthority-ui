import * as _ from 'lodash';
import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRoute, ActivationStart, NavigationEnd, Router, RoutesRecognized} from '@angular/router';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';
import {SetInitialQueryParams} from '@app/store/app.actions';
import {BehaviorSubject, Subscription} from 'rxjs/index';
import {getLastRouteQueryparams} from '@app/store/router.selectors';
import {Data} from "@angular/router/src/config";
import {filter} from "rxjs/operators";
import {V1RouteData} from "@angular-1-routes/model/v1.route.data";
import {Observable} from "rxjs/internal/Observable";
import {UtilService} from "@services/util.service";
import {Ng1RoutePrefix} from "@app/doc-1/v1-frame/v1.service";

export const V1RouteVersion = 'v1';

@Injectable()
export class RoutingService  implements OnDestroy {
  private history = [];
  private _subscriptions: Subscription;
  private _isV1Route$: BehaviorSubject<boolean>;


  constructor(
    private router: Router,    private utilService: UtilService,  private route: ActivatedRoute,  private location: Location,  private store: Store<any>) {

        this._isV1Route$ = new BehaviorSubject<boolean>(false);

        this._subscriptions = this.router.events.pipe(
          filter(event => event instanceof NavigationEnd))
          .subscribe((event: NavigationEnd) => {
            this.history = [...this.history, event.urlAfterRedirects];

          });

      this._subscriptions.add(this.router.events.pipe(
         filter(event => event instanceof ActivationStart)) //using NavigationEnd since ActivationEnd is called multiple times per route segment
          .subscribe((event: ActivationStart) => {
               const routeData = <V1RouteData>event.snapshot.data;
                if(event.snapshot.children.length==0) { //do it only for leaf url segment
                if (routeData && routeData.version == V1RouteVersion) {
                  this._isV1Route$.next(true);
                }
                else {
                  this._isV1Route$.next(false);
                }
              }
      }));



    // Parent:  about
  //  this.route.parent.url.subscribe(url => console.log(url[0].path));

    // Current Path:  company
 //   this.route.url.subscribe(url => console.log(url[0].path));
  }

  getLastRouteData$():Observable<Data> {
    //return this.store.select(getLastRouteData)
    return this.route.data;
  }

  getRouteQueryParams$() {
    return this.store.select(getLastRouteQueryparams)
  }

  getCurrentUrl() {
    return this.location.path();
  }

  clearAllParametersWithoutReload() {
    this.location.replaceState("");
  }


  get isV1Route$():Observable<boolean>{
    return this._isV1Route$.asObservable();
  }

  public getHistory(): string[] {
    return this.history;
  }

  setInitialQueryParams() {
    this.store.next(new SetInitialQueryParams());
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/index';
  }

  public updateQueryParamsWithoutReloading(queryParams, paramKey, paramVal) {

    const qParams = _.extend({}, queryParams);
    qParams[paramKey] = paramVal;

    const urlTree = this.router.createUrlTree([], {
      queryParams: qParams,
      queryParamsHandling: 'merge',
      preserveFragment: true
    });

    this.location.go(urlTree.toString());
  }

  public updateQueryParams(queryParams, paramKey, paramVal) {

    const qParams = _.extend({}, queryParams);
    qParams[paramKey] = paramVal;

    const urlTree = this.router.createUrlTree([], {
      queryParams: qParams,
      queryParamsHandling: 'merge',
      preserveFragment: true
    });

    this.router.navigate([this.router.url], {queryParams: qParams});
  }

  public replaceQueryParamsWithoutReloading(queryParams, paramKey, paramVal) {

    const qParams = _.extend({}, queryParams);
    qParams[paramKey] = paramVal;

    const urlTree = this.router.createUrlTree([], {
      queryParams: qParams,
      queryParamsHandling: 'merge',
      preserveFragment: true
    });

    this.location.replaceState(urlTree.toString());
  }

  public updateParamsWithoutReloading(route, paramKey, paramVal) {

    const params = _.extend({}, route.snapshot.params);
    params[paramKey] = paramVal;

    const path = route.routeConfig.path;
    const urlTree = this.router.createUrlTree([], {}).toString();
    const urlTreeArr = urlTree.split('/');

    _.each(urlTreeArr, (item, index) => {
      if (item.indexOf(path) === 0) {
        const str = JSON.stringify(params);
        const formattedParams = str.substring(1, str.length - 1).replace(/"/g, '').replace(/:/g, '=');
        urlTreeArr[index] = `${path};${formattedParams}`
      }
    });

    this.location.go(urlTreeArr.join('/'));
  }

  public clearAdditionalParams(activeRoute, localStorageKey = null, filterKeysList:string[]) {
    const url = this.location.path().split('?')[0];
    const qParams = _.extend({}, activeRoute.snapshot.queryParams);
    let allKeys = Object.keys(qParams);
    allKeys.forEach((attrName)=> {
      if (filterKeysList.indexOf(attrName) > -1) {
        delete qParams[attrName];
      }
    });
    if (localStorageKey != null) {
 //     this.utilService.addToLocalStorageUser(localStorageKey, qParams).subscribe();
    }
    this.router.navigate([url], {queryParams: qParams});
  }

  public updateUrlNoReload(url:string){
    this.location.replaceState(url); //updates history as well
  }

  public updateParam(activeRoute, paramKey, paramVal) {

    const url = this.location.path().split('?')[0];

    const qParams = _.extend({}, activeRoute.snapshot.queryParams);
    qParams[paramKey] = paramVal;

    this.router.navigate([url], {queryParams: qParams});
  }

  public updateAllParam(activeRoute, qParams) {

    const url = this.location.path().split('?')[0];
    this.router.navigate([url], {queryParams: qParams});
  }


  private getUrlParamAsObject = (searchParams :string) => {
    const result = {};
    //in case the queryString is empty
    if (searchParams!==undefined) {
      const paramParts = searchParams.split('&');
      for(let part of paramParts) {
        let paramValuePair = part.split('=');
        //exclude the case when the param has no value
        if(paramValuePair.length===2) {
          result[paramValuePair[0]] = decodeURIComponent(paramValuePair[1]);
        }
      }

    }
    return result;
  }


  public updateParamList(activeRoute, paramKeyList, paramValList) {

    let pathArr:string[] = this.location.path().split('?');
    const url = pathArr[0];

    let qParams = {};
    if (pathArr.length == 2 && pathArr[1].length > 0 ) {
      qParams = _.extend({}, this.getUrlParamAsObject(pathArr[1]));
    }
    for (let count = 0; count < paramKeyList.length; count++) {
      if (paramValList[count] != '') {
        qParams[paramKeyList[count]] = paramValList[count];
      }
      else if (qParams[paramKeyList[count]]) {
        delete qParams[paramKeyList[count]];
      }
    }
    this.router.navigate([url], {queryParams: qParams});
  }

  public navigate(url, queryParams) {
    this.router.navigate([url], {queryParams: queryParams});
  }

  public navigateToPrevUrl() {
    this.router.navigate([this.getPreviousUrl()]);
  }

  ngOnDestroy() {
    if (this._subscriptions) {
      this._subscriptions.unsubscribe();
    }
  }
}

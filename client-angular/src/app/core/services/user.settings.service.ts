import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {LoginService} from "@services/login.service";
import {map, skipWhile, first} from 'rxjs/internal/operators';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class UserSettingsService {


  constructor(private loginService:LoginService, private http: HttpClient) {
  }


  hasSessionStorage(key) {
    return sessionStorage.getItem(key)!== null;
  }


  addToSessionStorage(key,value) {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  getFromSessionStorage(key) {
    if (!sessionStorage.getItem(key)) {
      return null;
    }
    return JSON.parse(sessionStorage.getItem(key));
  }

  hasLocaleStorage(key) {
    return localStorage.getItem(key)!== null;
  }

  addToLocaleStorage(key,value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getFromLocaleStorage(key) {
    if (!localStorage.getItem(key)) {
      return null;
    }
    return JSON.parse(localStorage.getItem(key));
  }


  addToRemoteUserStorage(key, value) {
    this.http.put('/api/user/saveddata/'+key,JSON.stringify(value)).subscribe(()=>{
    });
  }

  getFromRemoteUserStorage(key):any  {
    return this.http.get('/api/user/saveddata/'+key).pipe(map((ansObj:any)=>{
      if (ansObj== null) {
        return null;
      }
      return JSON.parse(ansObj.data);
    }));
  }

  addToLocalStorageUser(key,value) {
    return this.loginService.currentUser$.pipe(
      skipWhile(user => !user), //first subscription user is null
      first()
      , map((currentUser:any ) => {
        localStorage.setItem(key+'_' + currentUser.username, JSON.stringify(value));
      }));
  }

  getFromLocalStorageUser(key) {
    return this.loginService.currentUser$.pipe(
      skipWhile(user => !user),
      first()
      , map((currentUser:any ) => {
        return JSON.parse(localStorage.getItem(key+'_' + currentUser.username));
      }));
  }


}

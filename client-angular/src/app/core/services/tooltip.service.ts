import * as _ from 'lodash';
import { Injectable } from '@angular/core';

declare var $: any;

@Injectable()
export class TooltipService {
  private history = [];

  constructor() {

  }

  init() {

    let selector = `
    a[title], 
    a[tooltip], 
    a[bubble],
    h3[title], 
    h3[tooltip], 
    h3[bubble],
    span[title], 
    span[tooltip], 
    span[bubble],
    div[title],
    div[tooltip],
    div[bubble], 
    button[title],
    button[tooltip],
    button[bubble]`;

    $(document).on("mouseenter", selector, (e) =>{
      if (e.currentTarget && e.currentTarget.className && e.currentTarget.className.indexOf('no-special-tooltip')>-1) {
        return;
      }
      this.removeTooltip();
      this.showTooltip(e);
    });
  };

  private showTooltip (e) {

    let elm = $(e.target);
    let title = elm.attr("title") || elm.attr("tooltip") || elm.attr("bubble");

    if(!title) {
      elm = elm.closest("[title], [tooltip], [bubble]");
      title = elm.attr("title") || elm.attr("tooltip") || elm.attr("bubble");
    }
    if (title) {
      this.createTooltip(e, elm, title,  elm && elm.length > 0 && elm[0].className && elm[0].className.indexOf("mouse-tooltip")>-1);

      elm.removeAttr('ng-attr-title');
      elm.removeAttr('title').removeAttr('tooltip');
      elm.attr({'bubble' : title});

      elm.on("mouseleave click", () =>{
        this.removeTooltip();
      });
    }
  };

  private removeTooltip(){
    if(this.createTooltip) {
      this.createTooltip.cancel();
    }
    $(document).find('body').find('.da-tooltip').remove();
  };

  private createTooltip = _.debounce( (e, elm, title, mouseLocationTooltip:boolean = false) => {

    let tooltipElement = $('<div>').addClass('da-tooltip da-tooltip-fade-in');

    $(document).find('body').append(tooltipElement);
    tooltipElement.html(title);

    let pos = this.calculatePosition(e, elm, tooltipElement, this.getDirection(elm), mouseLocationTooltip);
    tooltipElement.addClass('da-tooltip-' + pos.dir).css({
      top: pos.top,
      left: pos.left
    });
  },400);

  private getDirection(elm) {
    return elm.attr('tooltip-direction') || elm.attr('title-direction') || 'top';
  };

  private calculatePosition(e, elm, tooltip, direction, mouseLocationTooltip) {

    let tooltipBounding = tooltip[0].getBoundingClientRect();
    let elBounding = elm[0].getBoundingClientRect();
    let scrollLeft = window.scrollX || document.documentElement.scrollLeft;
    let scrollTop = window.scrollY || document.documentElement.scrollTop;
    let arrow_padding = 12;
    let pos = {left: 0, top: 0};
    let newDirection = null;

    // calculate the left position
    if (mouseLocationTooltip) {
      pos.left = e.pageX - tooltipBounding.width/2 ;
    }
    else if (this.stringStartsWith(direction, 'left')) {
      pos.left = elBounding.left - tooltipBounding.width - (arrow_padding / 2) + scrollLeft;
    } else if (this.stringStartsWith(direction, 'right')) {
      pos.left = elBounding.left + elBounding.width + (arrow_padding / 2) + scrollLeft;
    } else if (this.stringContains(direction, 'left')) {
      pos.left = elBounding.left - tooltipBounding.width + arrow_padding + scrollLeft;
    } else if (this.stringContains(direction, 'right')) {
      pos.left = elBounding.left + elBounding.width - arrow_padding + scrollLeft;
    } else {
      pos.left = elBounding.left + (elBounding.width / 2) - (tooltipBounding.width / 2) + scrollLeft;
    }

    // calculate the top position
    if (mouseLocationTooltip) {
      pos.top = e.pageY - 45;
    }
    else if (this.stringStartsWith(direction, 'top')) {
      pos.top = elBounding.top - tooltipBounding.height - (arrow_padding / 2) + scrollTop;
    } else if (this.stringStartsWith(direction, 'bottom')) {
      pos.top = elBounding.top + elBounding.height + (arrow_padding / 2) + scrollTop;
    } else if (this.stringContains(direction, 'top')) {
      pos.top = elBounding.top - tooltipBounding.height + arrow_padding + scrollTop;
    } else if (this.stringContains(direction, 'bottom')) {
      pos.top = elBounding.top + elBounding.height - arrow_padding + scrollTop;
    } else {
      pos.top = elBounding.top + (elBounding.height / 2) - (tooltipBounding.height / 2) + scrollTop;
    }

    if (pos.left < scrollLeft) {
      newDirection = direction.replace('left', 'right');
    } else if ((pos.left + tooltipBounding.width) > (window.innerWidth + scrollLeft)) {
      newDirection = direction.replace('right', 'left');
    }

    if (pos.top < scrollTop) {
      newDirection = direction.replace('top', 'bottom');
    } else if ((pos.top + tooltipBounding.height) > (window.innerHeight + scrollTop)) {
      newDirection = direction.replace('bottom', 'top');
    }

    if (newDirection) {
      //return calculatePosition(e, elm, tooltip, newDirection);
    }

    return {
      top: pos.top,
      left: pos.left,
      dir: direction
    };
  };

  private stringStartsWith(searchString, findString) {
    return searchString.substr(0, findString.length) === findString;
  };

  private stringContains(searchString, findString) {
    return searchString.indexOf(findString) !== -1;
  };
}

import {Injectable} from '@angular/core';
import * as XLSX from 'xlsx';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs/index';
import {environment} from '../../../environments/environment';
import {DefaultPagedReqParams, UrlParamsService} from './urlParams.service';
import {flatMap} from '@utilities/array.utils';
import {map, timeout} from 'rxjs/internal/operators';
import {TreeGridSortingInfo} from "@tree-grid/types/tree-grid.type";
import {DatePipe} from '@angular/common';

const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor(private http: HttpClient,
              private datePipe: DatePipe,
              private urlParamsService: UrlParamsService) {
  }

  public exportAsExcelFile(json: any[], excelFileName: string, header = []): void {

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json, {header:header});
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};

    XLSX.writeFile(workbook, `${excelFileName}_${this.datePipe.transform((new Date()).getTime(),'dd-MM-yyyy_HH-mm')}${EXCEL_EXTENSION}`);
  }


  public exportData(totalItemsNumber, urlSuffix, params:any  , extractSingleDataFunc, excelTitle: string, sortOption: any = null, getListFromResultFunc = null): Observable<void> {
    let mainThis = this;
    return Observable.create(function(observer) {
      let goItemNum = 0;
      let goPage = 1;
      let queriesArray: any[] = [];


      while (goItemNum < totalItemsNumber && goItemNum < environment.maximumItemsShowOnExcel) {
        params.pageSize = environment.maximumItemsOnServerCallForExcel;
        params.page = goPage;
        params.take = environment.maximumItemsOnServerCallForExcel;
        if (goPage == 1) {
          params.export=true;
        }
        else {
          if (params.export) {
            delete params.export;
          }
        }
        if (sortOption != null) {
          params.sort = JSON.stringify([{
            dir: (<TreeGridSortingInfo>sortOption).sortOrder,
            field: (<TreeGridSortingInfo>sortOption).sortBy
          }]);
        }
        params.skip = 0;
        params.pageSizeReduced = true;
        queriesArray.push(mainThis.http.get(urlSuffix, <any>{params}).pipe(timeout(150000)));
        goItemNum = goItemNum + environment.maximumItemsOnServerCallForExcel;
        goPage = goPage + 1;
      }

      let listQueriesArray = [];
      while (queriesArray.length > 0) {
        let tmpQueryArr = [];
        if (queriesArray.length > environment.maximumParallelServerCalls) {
          tmpQueryArr = queriesArray.slice(0, environment.maximumParallelServerCalls);
          queriesArray = queriesArray.slice(environment.maximumParallelServerCalls);
        }
        else {
          tmpQueryArr = queriesArray;
          queriesArray = [];
        }
        listQueriesArray.push(tmpQueryArr);
      }
      let loadQueriesList = function(listQueriesArray, resultDataArray,extractSingleDataFunc, excelTitle) {
        forkJoin(listQueriesArray.shift()).subscribe((results) => {
          let excelItemsList:any[] = [];
          for (let count: number = 0 ; count < results.length ; count++ ) {
            let queryItemList:any[];
            if (getListFromResultFunc == null) {
              queryItemList = results[count]['content'];
            }
            else {
              queryItemList = getListFromResultFunc(results[count]);
            }
            for (let grow: number = 0 ; grow < queryItemList.length ; grow++) {
              resultDataArray.push(extractSingleDataFunc(queryItemList[grow]));
            }
          }
          if (listQueriesArray.length > 0 ) {
            loadQueriesList(listQueriesArray, resultDataArray,extractSingleDataFunc, excelTitle);
          }
          else {
            mainThis.exportAsExcelFile(resultDataArray, excelTitle);
            observer.next();
          }
        });
      };

      if (listQueriesArray.length > 0 ) {
        loadQueriesList(listQueriesArray, [], extractSingleDataFunc, excelTitle);
      }
      else {
        mainThis.exportAsExcelFile([], excelTitle);
        observer.next();
      }
    });
  }

  public exportSelectedData(itemsList, extractSingleDataFunc, excelTitle: string): Observable<void> {
    let mainThis = this;
    return Observable.create(function(observer) {
      let resultDataArray = [];
      for (let grow: number = 0 ; grow < itemsList.length ; grow++) {
        resultDataArray.push(extractSingleDataFunc(itemsList[grow]));
      }
      mainThis.exportAsExcelFile(resultDataArray, excelTitle);
      observer.next();
    });
  }
}

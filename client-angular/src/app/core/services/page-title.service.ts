import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {SetTitle} from '@core/store/core.actions';
import {getPageTitle} from '@core/store/core.selectors';

@Injectable()
export class PageTitleService {

  constructor(private store: Store<any>) {
  }

  setTitle(title: string) {

    this.store.dispatch(new SetTitle(title));


  }

  getTitile$() {
    return this.store.select(getPageTitle);
  }

}

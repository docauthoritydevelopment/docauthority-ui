import {Injectable} from '@angular/core';
import {EFilterOperators} from '@services/filter-types/filter-operators';
import {SingleCriteria} from '@services/filter-types/single-criteria';
import {EFileAction, EFileState} from '@app/features/gdpr/types/GDPR.workflow.dto';
import {ECriteriaOperators} from '@services/filter-types/criteria-operators';
import {ICriteriaComponent} from '@services/filter-types/criteria-component.interface';
import * as _ from 'lodash';
import {AndCriteria} from '@services/filter-types/and-criteria';
import {OrCriteria} from '@services/filter-types/or-criteria';

@Injectable()
export class FilterFactory {
  createContentSearch(): (value: string) => SingleCriteria {

    return function (value: string) {
      const originalCriteriaOperationId = '123';
      return new SingleCriteria(null, filterFieldNames.contentFilter, value,
        EFilterOperators.contains, value, 'GDPR name', originalCriteriaOperationId);
    }
  }

  createSubFolderFilter =function(){
    var displayedFilterType='Subfolder';
    return function(id,name=null):ICriteriaComponent {
      if(id ) {
        return new SingleCriteria(null, filterFieldNames.subFolders, id, EFilterOperators.equals, name,displayedFilterType);
      }
    }
  }

  createWorkflowFileAction(filterIdentifier = '121'): (action: EFileAction, workflowId: number) => SingleCriteria {

    return function (action: EFileAction, workflowId: number) {
      return new SingleCriteria(null, filterFieldNames.workflowData,
        `${workflowId}.*.${action}.*`, EFilterOperators.equals, action.toString(), 'GDPR action', filterIdentifier);
    }
  }

  // TODO: Use it or loose it
  createWorkflowFileActionAndState(filterIdentifier = '123'): (action: EFileAction, fileState: EFileState, workflowId: number) => SingleCriteria {

    return function (action: EFileAction, fileState: EFileState, workflowId: number) {
      return new SingleCriteria(null, filterFieldNames.workflowData,
        `${workflowId}.*.${action}.${fileState}`, EFilterOperators.equals,
        `${action.toString()} ${fileState.toString()}`, 'GDPR', filterIdentifier);
    }
  }

  createWorkflowFileStatesFilter(filterIdentifier = '124'): (fileStates: EFileState[], workflowId: number, criteriaOperation: ECriteriaOperators) => ICriteriaComponent {
    return function (fileStates: EFileState[], workflowId: number, criteriaOperation: ECriteriaOperators): ICriteriaComponent {
      if (fileStates && fileStates.length > 0) {
        fileStates = _.clone(fileStates);
        const first = new SingleCriteria(
          null, filterFieldNames.workflowData, `${workflowId}.*.*.${fileStates[0]}`,
          EFilterOperators.equals, fileStates[0].toString(), 'GDPR state', filterIdentifier);
        fileStates.splice(0, 1);
        const criterias = [];
        fileStates.forEach(t =>
          criterias.push(new SingleCriteria(null, filterFieldNames.workflowData, `${workflowId}.*.*.${t}`, EFilterOperators.equals, t.toString(),
            'GDPR state', filterIdentifier)));
        if (criterias.length === 0) {
          return first;
        }
        if (criteriaOperation === ECriteriaOperators.and) {
          return new AndCriteria([first], criterias);
        }
        ;
        return new OrCriteria([first], criterias);
      }
    }
  }

  // ToDo: Use it or loose it
  createWorkflowById() {

    return function (value: number) {
      return new SingleCriteria(null, filterFieldNames.workflowId, value, EFilterOperators.equals, value.toString(), 'workflow');
    }
  }

}

enum filterFieldNames {
  contentFilter = 'contentFilter',
  workflowId = 'workflowId',
  workflowData = 'workflowData',
  subFolders = 'folder.subfolders'
}

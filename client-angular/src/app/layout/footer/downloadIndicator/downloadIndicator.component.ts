import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation,ViewChild} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ServerExportService} from "@services/serverExport.service";
import {NgbTooltip,NgbPopover} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from "@services/util.service";
import {ServerExportStatus} from "@core/types/serverExport.types";
import {ServerExportFile} from "@core/types/serverExport.dto";
import {CommonConfig} from "@app/common/configuration/common-config";
import {FileTypes} from "@core/types/file.types";
import {AlertType} from "@shared-ui/components/modal-dialogs/types/alert-type.enum";
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";


@Component({
  selector: 'download-indicator',
  templateUrl: './downloadIndicator.component.html',
  styleUrls: ['./downloadIndicator.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DownloadIndicatorComponent extends BaseComponent {
  @ViewChild('tooltip') public tooltip: NgbTooltip;
  @ViewChild('popOver') public popover: NgbPopover;
  tooltipText = '';
  exportList:ServerExportFile[] = [];
  processingCount = 0 ;
  downloadedCount = 0 ;
  failedCount = 0 ;
  ServerExportStatus = ServerExportStatus;
  statusListIsOpen:boolean = false;
  showChangeIndication: boolean = false;


  constructor(private serverExportService:ServerExportService, private utilService: UtilService, private dialogService : DialogService) {
    super();
  }

  daOnInit() {
    this.on(this.serverExportService.addExportFileEvent$, (sExServerExportFile:ServerExportFile) => {
      this.updateExportList(true);
      setTimeout(()=> {
        if (!this.statusListIsOpen) {
          this.showTooltipMsg(3000);
        }
      });
    });

    this.on(this.serverExportService.refreshExportFileListEvent$, () => {
      this.updateExportList();
    });
    this.updateExportList();
  }


  getFileTypeClassName(fileType): string {
    if (fileType === FileTypes.msExcel) {
      return 'export-excel-icon';
    }
    else if (fileType === FileTypes.zip) {
      return 'export-zip-icon';
    }
  }


  updateExportList = (withoutChangeIndication:boolean = false)=>  {
    this.exportList = this.serverExportService.getExportList();
    let beforeProcessing = this.processingCount;
    this.showChangeIndication = false;

    let thereWasChange:boolean = false;
    this.exportList.forEach((item:ServerExportFile)=> {
      if (!item.wasDeleted) {
        /*
        if (item.status == ServerExportStatus.IN_PROGRESS && item.progress != null && item.progress < 100 && item.progress != 0 ) {
          let actualTime = item.currentTimeStamp - item.createdDate
          let timeLeft = actualTime*100/item.progress - actualTime;
          if (timeLeft < 1000) {
            item.timeLeftStr = '1 sec left';
          }
          else if (timeLeft < 60000) {
            item.timeLeftStr = Math.floor(timeLeft/1000)+ ' sec left';
          }
          else if (timeLeft < 3600000) {
            item.timeLeftStr = Math.round(timeLeft/60000)+ ' min left';
          }
          else {
            item.timeLeftStr = Math.round(timeLeft/3600000)+ ' hour left';
          }
        }
        */


        if (item.status == ServerExportStatus.DONE && item.changedLately) {
          if (item.userParams.downloadWhenReady == "true" && (item.userParams.uniqueId == this.serverExportService.getExportUniqueId())) {
            this.openFile(item);
          }
          else if (!withoutChangeIndication){
            this.showChangeIndication = true;
          }
        }
        if (item.status == ServerExportStatus.FAILED && item.changedLately && !withoutChangeIndication) {
          this.showChangeIndication = true;
        }
        if (item.changedLately) {
          thereWasChange = true;
        }
        if (this.showChangeIndication) {
          setTimeout(()=> {
            this.showChangeIndication = false;
          },5000);
        }
      }
    });

    this.downloadedCount = 0 ;
    this.processingCount = 0 ;
    this.failedCount = 0 ;
    this.exportList.forEach((item:ServerExportFile)=> {
      if (!item.wasDeleted) {
        if (item.status == ServerExportStatus.DONE) {
          this.downloadedCount++;
        }
        else if (item.status == ServerExportStatus.FAILED) {
          this.failedCount++;
        }
        else {
          this.processingCount++;
        }
      }
    });

    if (this.downloadedCount ==0 && this.processingCount == 0 && this.failedCount == 0 ) {
      this.statusListIsOpen = false;
    }

    this.exportList.sort((exItem1:any , exItem2:any)=> {
      return exItem2.createdDate - exItem1.createdDate;
    });
    if (beforeProcessing != this.processingCount || thereWasChange) {
      setTimeout(() => {
        this.closeToolTip();
      });
    }
  }

  closeList() {
    this.statusListIsOpen = false;
    this.showChangeIndication = false;
    this.closeToolTip();
  }


  openFile(file: ServerExportFile, withCheck : boolean = false) {
    if (!file.wasDeleted) {
      if (file.status == ServerExportStatus.DONE) {
        if (withCheck) {
          this.serverExportService.checkFileIdExist(file.id).subscribe((fileExisit: boolean) => {
            this.serverExportService.markAsDeleted(file);
            if (fileExisit) {
              window.location.href = '/api/exporter2/download/' + file.id;
              this.updateExportList(true);
            }
            else {
              this.dialogService.showAlert("Download file", 'Cannot download file , file already deleted.', AlertType.Error);
            }
          });
        }
        else {
          this.serverExportService.markAsDeleted(file);
          setTimeout(()=> {
            window.location.href = '/api/exporter2/download/' + file.id;
          },100);
        }
      }
    }
  }

  deleteFile(file: ServerExportFile) {
    if (!file.wasDeleted) {
      this.serverExportService.markAsDeleted(file);
      this.serverExportService.deleteExportFile(file);
      this.updateExportList(true);
    }
  }



  openList() {
    this.closeToolTip();
    this.statusListIsOpen = true;
    this.showChangeIndication = false;
  }

  closeToolTip = ()=> {
    if (this.tooltip && this.tooltip.isOpen()) {
      this.tooltip.close();
    }
  }

  showTooltipMsg(period) {
    this.closeToolTip();
    this.tooltip.open();
    this.startTimeOut(() => {
      this.tooltip.close();
    }, period);
  }

}

import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {CommonConfig} from "@app/common/configuration/common-config";
import {
  ScheduledOperationMetadataDto,
  ScheduledOperationState
} from "@app/common/discover/types/scheduledOperationMetadata-dto";
import {ScheduledOperationsService} from "@services/scheduledOperations.service";


@Component({
  selector: 'da-running-task-status',
  templateUrl: './running-task-status.component.html',
  styleUrls: ['./running-task-status.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RunningTaskStatusComponent  extends BaseComponent  {
  ASSOCIATION_FOR_FILES_BY_FILTER = "ASSOCIATION_FOR_FILES_BY_FILTER";
  statusListIsOpen:boolean
  progressCounter: number;

   phaseName: string;
 // @Output() requestStopApplyToSolr = new EventEmitter();
  isRunning: boolean;
  total: number;
  operations:ScheduledOperationMetadataDto[];
  ScheduledOperationState = ScheduledOperationState;

  constructor(private scheduledOperationsService:ScheduledOperationsService) {
    super();
  }

  get isAsyncOperationRunning(){
   return this.operations && this.operations.length>0;
  };

  get toolTip(): string {
    if (this.total !== 0) {
      return `${this.progressCounter}  applying: ${this.progressCounter} / ${this.total}`;
    }
    return 'Starting...';
  }

  get progressPercentage(): number {
    if (this.total === 0) {
      return 0;
    }
    return this.progressCounter / this.total * 100;
  }

  get progressPercentageStr(): string {
    return `${Math.round(this.progressPercentage)}%`;
  }

  daOnInit(): void {
    this.on(this.scheduledOperationsService.changeEvent$, () => {
      this.loadData();
    });

    setTimeout(()=> {
      this.loadData();
    },7000)
  }

  loadData=()=>{
    this.addQuerySubscription(
      this.scheduledOperationsService.listOperations().subscribe(
      (list:ScheduledOperationMetadataDto[]) => {
           this.operations = list.sort(function(a1,a2){ return a2.createTime - a1.createTime ; });
           this.startTimeOut(this.loadData,this.operations && this.operations.length>0?
             CommonConfig.SCREENS_REFRESH_CHECK_PERIOD:CommonConfig.SCREENS_REFRESH_PERIOD);
    } , (error) => {
          console.log(error); //report logger error
          this.startTimeOut(this.loadData, this.operations && this.operations.length > 0 ?
            CommonConfig.SCREENS_REFRESH_CHECK_PERIOD : CommonConfig.SCREENS_REFRESH_PERIOD)
        })
    );
  }

  deleteProcess(p:ScheduledOperationMetadataDto) {
    this.scheduledOperationsService.deleteOperation(p);
  }

  // stopApplyTagsToSolr() {
  //   this.requestStopApplyToSolr.emit();
  // }


}

import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {ApplyToSolrPollingResult} from '@core/types/apply-to-solr-polling-result';
import {Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';
import {SystemVersionService} from "@core/system-version.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

  version$: Observable<string>;

  constructor(private versionService: SystemVersionService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.getVersion();
    }, 4000); //postpone loading of charts menu items till app lis loaded





  }



  getVersion() {
    this.version$ = this.versionService.getVersion();
  };

}

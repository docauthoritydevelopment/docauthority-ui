import * as _ from 'lodash';
import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BreadCrumb } from './breadcrumb';
import {distinctUntilChanged, filter, map} from 'rxjs/internal/operators';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: [ './breadcrumb.component.scss' ],
    encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent {


    breadcrumbs$ = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd),
        distinctUntilChanged(),
        map(event => this.buildBreadCrumb(this.activatedRoute.root)));

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    buildBreadCrumb(route: ActivatedRoute, url: string = '',
                    breadcrumbs: Array<BreadCrumb> = []): Array<BreadCrumb> {

      let path = '';
      let label = '';
      let nextUrl = url;
      let newBreadcrumb = null;
      let newBreadcrumbs = breadcrumbs;

      const isValidRoute = route.routeConfig &&
        !_.isEmpty(route.routeConfig.path) && _.isObject(route.routeConfig.data) && !_.isEmpty(route.routeConfig.data['breadcrumb']);

      if (isValidRoute) {
        label = route.routeConfig.data['breadcrumb'];
        path = route.routeConfig.path;
        nextUrl = `${url}${path}/`;

        newBreadcrumb = {
          label: label,
          url: nextUrl,
          params: route.snapshot.params
        };
      }
      if (newBreadcrumb) {
        newBreadcrumbs = [...breadcrumbs, newBreadcrumb];
      }
      if (route.firstChild) {
          return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
      }
      return newBreadcrumbs;
    }
}

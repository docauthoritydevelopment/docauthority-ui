import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {MetadataService} from "@services/metadata.service";
import {SubHeaderService} from "@services/subHeader.service";

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent extends BaseComponent{
  displayInnerHtml = '';

  constructor(private metadataService : MetadataService, private subHeaderService: SubHeaderService) {
    super();
  }

  daOnInit(){
    this.on(this.subHeaderService.subHeaderChangedEvent$, (newInnerHtml) => {
      this.displayInnerHtml = newInnerHtml;
    });
  }

}

// import {Injectable} from '@angular/core';
// import {Actions, Effect} from '@ngrx/effects';
// import {Router} from '@angular/router';
// import {Empty} from '@core/store/core.actions';
// import {map, tap} from 'rxjs/internal/operators';
// import {V1RouteStateName} from "@app/doc-1/angular-1-routes/model/v1.route.state.name";
//
// @Injectable()
// export class RouterEffects {
//
//   @Effect() nateToDashboard = this.actions$.ofType('ROUTER_CANCEL')
//     .pipe(tap(() => {
//         this.router.navigate(['/'+V1RouteStateName.dashboardIT])
//       }),
//       map(() => new Empty()))
//
//   constructor(private actions$: Actions, private router: Router) {
//   }
// }

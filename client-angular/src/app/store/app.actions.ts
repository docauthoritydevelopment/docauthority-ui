import {APP_PREFIX} from '@core/store/core.actions';
import {Action} from '@ngrx/store';

export const SET_INITIAL_QUERY_PARAMS = `${APP_PREFIX} Set initial query params`;

/**
 * This action, when dispatched should casue the router state
 * to have initial qeury params
 */
export class SetInitialQueryParams implements Action {
  readonly type = SET_INITIAL_QUERY_PARAMS;
}

import {Action, ActionReducerMap} from '@ngrx/store';
import * as fromAuthentication from '@core/store/authentication.reducer';
import {RouterState} from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import {RouterAction, RouterReducerState} from '@ngrx/router-store';
import * as fromdoc1Router from '../doc-1/angular-1-routes/store/doc-1-routes.reducer';
import * as fromCore from '@core/store/core.reducer';
import * as fromv1 from '../doc-1/v1-frame/store/v1.reducer';
import {SET_INITIAL_QUERY_PARAMS} from './app.actions';


export interface AppState {
  authentication: fromAuthentication.AuthenticationState;
  router: RouterReducerState<RouterState>;
  doc1Router: fromdoc1Router.V1RouteState;
  core: fromCore.CoreState;
  v1State: fromv1.V1State;
}

export const combinedReducers: ActionReducerMap<AppState> = {
  authentication: fromAuthentication.authenticationReducer,
  router: routerReducer,
  doc1Router: fromdoc1Router.reducer,
  core: fromCore.coreReducer,
  v1State: fromv1.v1Reducer
}


function getParamsObject() {
  const searchString = window.location.search;
  if (!searchString) {
    return null;
  }
  const searchArray = searchString.substring(1).split('&');
  const result = {};
  searchArray.forEach(item => {
    const [key, value] = item.split('=');
    result[key] = value;
  });

  return result;

}

export function routerReducer(state: RouterReducerState<RouterState>, action: Action) {

  switch (action.type) {
    case SET_INITIAL_QUERY_PARAMS: {
      const newState = {...state, queryParams: getParamsObject()};
      return newState;
    }
    default:
      return fromRouter.routerReducer(state, action as  RouterAction<any, RouterState>);

  }
}

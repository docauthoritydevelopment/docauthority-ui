import {RouterStateSerializer} from '@ngrx/router-store';
import {Params, RouterStateSnapshot} from '@angular/router';


/**
 * This interface represents the router state in the store
 */
export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  data: any;
}

export interface RouterState {
  state: RouterStateUrl;
}

function getData(state) {
  if (state && state.children && state.children.length !== 0) {

    return getData(state.children[0]);
  }

  if (!state || ! state.data) {
  return null;
  }

   const stateParams = state.data.stateParams || {};

  return {...state.data, stateParams: {...stateParams, ...state.params}};
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {

    const { url } = routerState;
    const queryParams = routerState.root.queryParams;
    const data = getData(routerState.root);
  //  data.stateParams = {...data.stateParams, routerState.params}

    return { url, queryParams, data };
  }

}

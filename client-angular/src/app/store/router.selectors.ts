import {RouterReducerState} from '@ngrx/router-store';
import {RouterState} from '@angular/router';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {V1RouteStateName} from 'app/doc-1/angular-1-routes/model/v1.route.state.name';

export const routerStateSelector = createFeatureSelector<RouterReducerState<RouterState>>('router');

// export const getCurrentUrl = createSelector(routerStateSelector, routerState => {
//   return routerState && routerState.state ? routerState.state['url'] : ''
// });

export interface RouteStateData {
  stateName: V1RouteStateName;
  withoutAuth: boolean;
  version: string
  stateParams: { [key: string]: any; };
}

export const getLastRouteData = createSelector(routerStateSelector, (routerState: any) => {
  return routerState && routerState.state ? <RouteStateData> (routerState.state.data) : null;
});

export const getLastRouteQueryparams = createSelector(routerStateSelector, (routerState: any) => {
  return routerState && routerState.state ? routerState.state.queryParams : null
});

export const isNg2Route = createSelector(getLastRouteData, data => {
  return data && data.version !== 'v1';

});

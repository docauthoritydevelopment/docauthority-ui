import {Directive, ElementRef, Input, OnDestroy, OnInit, Renderer2, NgZone} from '@angular/core';

@Directive({
  selector: '[daDraggable]'
})
export class DraggableDirective implements OnInit {

  @Input('daDraggable') draggable;
  /**
   * Selector for selecting the draggable element
   */
  @Input() draggableSelector: string;
  private newXPosition;
  private yPosition;
  private newYPosition;
  private xPosition
  private onDragStart: Function;
  private dragElem: HTMLElement;
  private isDraging:boolean = false;

  constructor(private _ngZone: NgZone, private elementRef: ElementRef
    , private renderer: Renderer2) {
  }

  ngOnDestroy() {
    // Remove events
    if (this.onDragStart) {
      this.onDragStart();
    }
  }

  ngOnInit() {
    this.draggableSelector = this.draggableSelector || '.modal-content';
    this.addDragEvents();
  }

  mouseMove(event) {
    event.preventDefault();
    if (this.isDraging) {
      this.newXPosition = this.xPosition - event.clientX;
      this.newYPosition = this.yPosition - event.clientY;
      this.xPosition = event.clientX;
      this.yPosition = event.clientY;
      this.setNewPosition();
    }
  }

  mouseUp(event) {
    event.preventDefault();
    if (this.isDraging) {
      this.isDraging = false;
      window.document.removeEventListener('mousemove', this.mouseMove);
      window.document.removeEventListener('mouseleave', this.mouseLeave);
      window.document.removeEventListener('mouseup', this.mouseUp);
    }
  }

  mouseLeave(event) {
    event.preventDefault();
    if (this.isDraging) {
      this.isDraging = false;
      window.document.removeEventListener('mousemove', this.mouseMove);
      window.document.removeEventListener('mouseleave', this.mouseLeave);
      window.document.removeEventListener('mouseup', this.mouseUp);
    }
  }



  addDrogMoveEvent = ()=> {
    this._ngZone.runOutsideAngular(()=> {
      window.document.addEventListener('mousemove', this.mouseMove.bind(this));
      window.document.addEventListener('mouseleave', this.mouseLeave.bind(this));
      window.document.addEventListener('mouseup', this.mouseUp.bind(this));
    });
  }

  private addDragEvents(): void {
    this.dragElem = this.elementRef.nativeElement.parentElement.parentElement.parentElement.querySelector(this.draggableSelector) as HTMLElement;
    if (!this.dragElem) {
      this.dragElem = this.elementRef.nativeElement.parentElement.parentElement.parentElement;
    }
    this.dragElem.style.position = 'absolute';
    let isMsie11 = (<any>document).documentMode && (<any>document).documentMode<=11 ;
    if (isMsie11) {
      this.dragElem.style.top = (window.innerHeight - this.dragElem.clientHeight)/2 + "px";
    }
    this.onDragStart = this.renderer.listen(
      this.elementRef.nativeElement
      , 'mousedown'
      , (event: DragEvent): void => {
        if (!this.isDraging) {
          this.xPosition = event.clientX;
          this.yPosition = event.clientY;
          this.isDraging = true;
          this.addDrogMoveEvent();
        }
      });
  }


  setNewPosition() {
    this.dragElem.style.top = `${(this.dragElem.offsetTop - this.newYPosition)}px`;
    this.dragElem.style.left = `${(this.dragElem.offsetLeft - this.newXPosition)}px`;
  }

}

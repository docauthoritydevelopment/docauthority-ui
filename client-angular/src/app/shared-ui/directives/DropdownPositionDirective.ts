import {Directive, ContentChild, AfterContentInit, ElementRef, OnDestroy, Input, Inject, forwardRef} from '@angular/core';
import { NgbDropdownMenu, NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import {positionElements} from "@shared-ui/directives/positioning";
import { Subscription } from 'rxjs';

@Directive({
  selector: '[ngbDropdown][ngbDropdownReposition]',
})
export class DropdownPositionDirective implements  OnDestroy {

  @Input() fixPos: string;

  @ContentChild(NgbDropdownMenu) private menu: NgbDropdownMenu;
  @ContentChild(NgbDropdownMenu, {read: ElementRef}) private menuRef: ElementRef;

  private readonly onChangeSubscription: Subscription;
  private oldParent: HTMLElement | null;
  private menuWrapper: HTMLElement;
  lastTop = null;
  triggerEl = null;
  placement = null;

  lastTriggerX = null;
  lastTriggerY = null;

  constructor(
    @Inject(forwardRef(() => NgbDropdown)) private dropdown: NgbDropdown,
    private elementRef: ElementRef,
  ) {
    this.onChangeSubscription = this.dropdown.openChange.subscribe((open: boolean) => {
      if (!open) {
        setTimeout(() => this.removeMenuFromBody(), 0);
      }
      else {
        this.oldParent = (<HTMLElement> this.menuRef.nativeElement).parentElement;
        this.createWrapper();
        setTimeout(() => {
          if (this.triggerEl == null) {
            this.triggerEl = this.elementRef.nativeElement.children[0];
          }
          if (this.placement == null)  {
            this.placement = this.dropdown.placement;
          }
          //this.menuWrapper.classList.remove("hide");
          this.setWrapperWidth();
          if (!this.isInBody()) {
            this.appendMenuToBody();
          }
          let popupIsOnTop: boolean = false;
          if (this.fixPos) {
            let scrollTop = window.scrollY || document.documentElement.scrollTop;
            let triggerElBoundries = this.triggerEl.getBoundingClientRect();
            let triggerBtnHeight = triggerElBoundries.height;
            let top = triggerElBoundries.top + this.menuWrapper.offsetHeight;
            let winHeight = scrollTop + window.innerHeight;
            let dropdownHeight = this.menuRef.nativeElement.clientHeight;
            if (top + dropdownHeight + triggerBtnHeight >= winHeight) {
              popupIsOnTop = true;
              if (this.placement.indexOf('bottom') > -1) {
                this.placement = this.placement.replace('bottom', 'top');
                this.menuWrapper.classList.add("top-drop-down-wrapper");
              }
            }
            else {
              if (this.placement.indexOf('top') > -1) {
                this.placement = this.placement.replace('top', 'bottom');
              }
            }
          }

          if (this.placement.indexOf("left") > -1) {
            this.menuWrapper.classList.add("left-drop-down-wrapper");
          }
          positionElements(this.triggerEl, this.menuWrapper, this.placement, true);
          this.menu.applyPlacement(positionElements(this.triggerEl, this.menuRef.nativeElement, this.placement));
          if (!popupIsOnTop && this.fixPos) {
            this.menuRef.nativeElement.style.top = '0';
          }

          let currBoundries = this.triggerEl.getBoundingClientRect();
          this.lastTriggerX = currBoundries.x;
          this.lastTriggerY = currBoundries.y;
        });
      }
    });
  }


  /*
  ngAfterContentInit() {
    this.menu.position = (triggerEl: HTMLElement, placement: string) => {
      this.triggerEl = triggerEl;
      this.placement = placement;
      if (this.lastTriggerX != null) {
        let currBoundries = this.triggerEl.getBoundingClientRect();
        if (this.lastTriggerX != currBoundries.x || this.lastTriggerY != currBoundries.y){
          this.lastTriggerX = null;
          this.lastTriggerY = null;
          this.dropdown.close();
        }
      }
    }
  }
  */

  ngOnDestroy() {
    this.removeMenuFromBody();
    if (this.onChangeSubscription) {
      this.onChangeSubscription.unsubscribe();
    }
    this.lastTriggerX = null;
    this.lastTriggerY = null;
  }

  private isInBody() {
    return this.menuWrapper && this.menuWrapper.parentNode === document.body;
  }

  private removeMenuFromBody = ()=>  {
    if (this.isInBody()) {
      if (this.oldParent) {
        this.menuWrapper.style.display = 'none';
        this.oldParent.appendChild(this.menuWrapper);
        delete this.menuWrapper;
      }
      this.triggerEl = null;
      this.placement = null;
    }
  }

  private appendMenuToBody() {
    this.menuWrapper.style.display = 'inline-block';
    window.document.body.appendChild(this.menuWrapper);
  }

  private createWrapper() {

    this.menuWrapper = document.createElement('div');
    this.menuWrapper.style.position = 'absolute';
    this.menuWrapper.style.zIndex = '1111';
    this.menuWrapper.classList.add("drop-down-position")

    /* this.menuWrapper.addEventListener('keyup', (event: KeyboardEvent) => {
      if (event.keyCode === 27) {
        this.dropdown.closeFromOutsideEsc();
      }
    });*/
    this.menuWrapper.appendChild(this.menuRef.nativeElement);
  }

  private setWrapperWidth() {
    const parentEl = <HTMLElement> this.elementRef.nativeElement;
    this.menuWrapper.style.width = parentEl.clientWidth + 'px';
  }
}

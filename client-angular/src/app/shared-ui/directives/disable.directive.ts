import {Directive, ElementRef, HostListener, Input} from '@angular/core';

const shadowSelector = '#modal-shadow';

@Directive({
  selector: '[daDisable]'
})
export class DisableDirective {

  @Input() noShadow: boolean;

  constructor(private el: ElementRef) {
  }

  private _disabled: boolean;

  get disabled(): boolean {
    return this._disabled;
  }

  @Input('daDisable') set disabled(value: boolean) {
    this._disabled = value;
    const element = this.el.nativeElement;
    if (value) {
      if (!this.noShadow) {
        this.appendShadow(element);
      }
    } else {
      const shadows = element.parentElement.querySelectorAll(shadowSelector);
      if (shadows) {
        this.removeShadows(shadows);
        // shadows.remove();
      } else {
        console.warn('Np shadow');
      }
      element.style.cursor = '';
      element.style.textDecoration = '';
    }
    this.el.nativeElement.attributes['disabled'] = value;
  }

  private appendShadow(element) {
    element.style.position = element.style.position || 'relative';
    const shadow = element.parentElement.querySelector(shadowSelector);
    if (!shadow) {
      element.parentElement.appendChild(this.createDropShadow());
      element.style.cursor = 'default';
      element.style.textDecoration = 'none';
    }
  }

  removeShadows(shadows) {
    for (let index = 0; index < shadows.length; index++) {
     // console.log('About to remove shadow');
      let el =  shadows[index];
      el.parentNode && el.parentNode.removeChild(el);
     // shadows[index].remove();
    }
  }

  @HostListener('click')
  disabledClick() {
    if (this.disabled) {
      window.event.stopPropagation();
      window.event.preventDefault();
      return false;
    }
    return !this.disabled;
  }

  @HostListener('hover') hover() {
    return !this.disabled;
  }

  createDropShadow() {
    const shadow = document.createElement('div');
    shadow.style.backgroundColor = 'transperant';
    shadow.style.position = 'absolute';
    shadow.style.zIndex = '100';
    shadow.style.top = '0';
    shadow.style.left = '0';
    shadow.style.bottom = '0';
    shadow.style.right = '0';
    shadow.id = 'modal-shadow';
    shadow.style.height = `${this.el.nativeElement.offsetHeight + 10}px`;
    return shadow;
  }

}

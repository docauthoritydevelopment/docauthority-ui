import {Directive, HostListener, OnInit, Renderer2, Input, ElementRef} from '@angular/core';

@Directive({
  selector: '[daDropdownMenu]'
})
export class DropdownMenuDirective implements OnInit {

  constructor(private renderer: Renderer2, private el: ElementRef) {

  }
  @Input() daDropdownMenu: any;

  ngOnInit() {
    const caretElement = document.createElement('span');
    caretElement.classList.add('caret');
    this.el.nativeElement.appendChild(caretElement);

  }

  @HostListener('click')
    openCloseMenu() {
     this.el.nativeElement.classList.toggle('open');
  }


}

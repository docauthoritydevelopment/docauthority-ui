import { Pipe, PipeTransform } from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Pipe({
  name: 'checkNull'
})
export class CheckNullPipe implements PipeTransform {

  constructor() {

  }


  transform(value: any, args?: any): any {
    if(value !== null) {
      return value;
    }
    return '';
  }

}

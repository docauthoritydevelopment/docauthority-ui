import { Pipe, PipeTransform } from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Pipe({
  name: 'limitBig'
})
export class LimitBigPipe implements PipeTransform {

  constructor() {

  }


  transform(value: any, args?: any): any {
    if (typeof args === 'undefined') args = 7;
    if ((value+'').length <= (args+2)) {
      return value;
    }
    return parseFloat(value).toPrecision(args);
  }

}

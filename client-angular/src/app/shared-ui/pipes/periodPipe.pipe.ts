import {Pipe} from '@angular/core';
import {UtilService} from "@services/util.service";

@Pipe({name: 'period'})
export class PeriodPipe {

  constructor(private utilService: UtilService) {

  }

  transform(dateTimeSpanInMillisec): any {
    return this.utilService.getPeriodString(dateTimeSpanInMillisec);
  }
}


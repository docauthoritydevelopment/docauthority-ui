import { Pipe, PipeTransform } from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Pipe({
  name: 'dadate'
})
export class DaDatePipe implements PipeTransform {

  constructor() {

  }


  transform(value: any, args?: any): any {
    if (value == null) {
      return '';
    }
    let ans =  (new Date(value)).toDateString();
    return ans;
  }

}

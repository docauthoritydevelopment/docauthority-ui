import { Pipe, PipeTransform } from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Pipe({
  name: 'bytes'
})
export class BytesPipe implements PipeTransform {

  constructor() {

  }


  transform(value: any, args?: any): any {
    if (isNaN(parseFloat(value)) || !isFinite(value)) return '-';
    if(value==0) return '0';
    if (typeof args === 'undefined') args = 1;
    var units = [' bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
      number = Math.floor(Math.log(value) / Math.log(1024));
    return (value / Math.pow(1024, Math.floor(number))).toFixed(args) +  ' ' +  units[number];
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import {I18nService} from "@app/common/translation/services/i18n-service";

@Pipe({
  name: 'numSuffix'
})
export class NumSuffixPipe implements PipeTransform {

  constructor() {

  }


  transform(value: any, args?: any): any {
    if (value == null) {
      return '0';
    }
    let isNegative = false;
    let formattedNumber = null;
    if (value < 0) {
      isNegative = true
    }
    value = Math.abs(value)
    if (value >= 1000000000) {
      formattedNumber = (value / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    } else if (value >= 1000000) {
      formattedNumber =  (value / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    } else  if (value >= 1000) {
      formattedNumber =  (value / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    } else {
      formattedNumber = value;
    }
    if(isNegative) { formattedNumber = '-' + formattedNumber }
    return formattedNumber;
  }

}

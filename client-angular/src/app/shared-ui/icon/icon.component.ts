import {Component, Input, OnInit} from '@angular/core';
import {IconsConfig} from "@app/common/configuration/common-config";

@Component({
  selector: 'da-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {

  @Input()
  set href(newHref: string) {
    this._href = newHref;
    this.updateParams();
  }

  get href(): string {
    return this._href;
  }


  @Input()
  set iconClass(newIconClass: string) {
    this._iconClass = newIconClass;
    this.updateParams();
  }

  get iconClass(): string {
    return this._iconClass;
  }

  _href = null;
  _iconClass = null;
  iconName = null;
  isPngIcon  = null;
  isSpriteSvg: boolean = false;


  updateParams() {
    this.iconName = (IconsConfig[this.href] ? IconsConfig[this.href] : (this.href ? this.href : null) );
    this.isPngIcon = this.iconName && this.iconName.toLowerCase().endsWith('.png');
    this.isSpriteSvg = this.iconName && this.iconName.startsWith('__');
  }

  ngOnInit() {
  }

}

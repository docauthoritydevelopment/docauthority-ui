import { NgModule } from '@angular/core';
import {DraggableDirective} from '@shared-ui/directives/draggable.directive';


@NgModule({
  imports: [],
  providers: [],
  declarations: [DraggableDirective],
  exports: [DraggableDirective],
  entryComponents: []
})
export class DaDraggableModule { }

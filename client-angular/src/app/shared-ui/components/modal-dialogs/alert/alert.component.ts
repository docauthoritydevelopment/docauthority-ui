import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AlertType} from '@shared-ui/components/modal-dialogs/types/alert-type.enum';
import {HttpErrorResponse} from '@angular/common/http';
import {ESystemRoleName} from "@users/model/system-role-name";
import {UrlAuthorizationService} from "@services/url-authorization.service";



@Component({
  selector: 'da-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AlertComponent implements OnInit {
  showMore = false;

  /**
   * Alert title
   */
  public title: string;
  /**
   * Alert message
   */
  public message: string;

  /**
   * Text on close button
   */
  public closeButtonText: string;

  /**
   * Alert Type
   */
  public alertType: AlertType;

  public  AlertTypeEnum = AlertType;

  public err:HttpErrorResponse;
  isTechSupport:boolean = false;

  constructor(private activeModal: NgbActiveModal, private urlAuthorizationService: UrlAuthorizationService) { }

  ngOnInit() {
    this.isTechSupport = this.urlAuthorizationService.isSupportRoles([ESystemRoleName.TechSupport]);
  }

  close(result: boolean) {
    this.activeModal.close(result);
  }



}

import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Dialog} from '../types/dialog.interface';

@Component({
  selector: 'da-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmComponent implements OnInit, Dialog {

  @Input() title: string;
  @Input() message: string;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  close(result: boolean) {
    this.activeModal.close(result);
  }

}

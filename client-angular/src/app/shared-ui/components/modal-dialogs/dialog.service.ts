import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Dialog} from './types/dialog.interface';
import {AlertType} from './types/alert-type.enum';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import {HttpErrorResponse} from '@angular/common/http';
import {ConfirmComponent} from '@shared-ui/components/modal-dialogs/confirm/confirm.component';
import {DialogWrapperComponent} from '@shared-ui/components/modal-dialogs/dialog-wrapper/dialog-wrapper.component';
import {AlertComponent} from '@shared-ui/components/modal-dialogs/alert/alert.component';
import {Store} from '@ngrx/store';
import {SetDialogState} from '@core/store/core.actions';
import {Observable, of} from 'rxjs/index';
import {DialogWrapper, DialogWrapperOptions} from "@shared-ui/components/modal-dialogs/types/dialog.interface";
import {BadHttpRequestException} from "@core/types/error.dto";
import {I18nService} from "@app/common/translation/services/i18n-service";


@Injectable()
export class DialogService {

  constructor(private modalService: NgbModal, private store: Store<any>, private i18nService:I18nService) {
  }

  /**
   * Notify error message
   * @param {HttpErrorResponse} error
   * @returns {Observable<boolean>}
   */

  notifyError(error: HttpErrorResponse): Observable<boolean> {
    this.showAlert('Error', 'Sorry, an error occurred', AlertType.Error);
    return of(true);

  };

  /**
   * Opens a confirmation dialog box
   * @param {string} title Confirmation title
   * @param {string} message
   * @returns {Promise<any>}
   */
  openConfirm(title: string, message: string): Promise<boolean> {
    const dialog = this.openDialog(ConfirmComponent, {size: 'sm'});

    this.setDialogProperties(dialog, title, message);

    return dialog.result;

  }

  /**
   * Opens modal popup of a given instance type
   * @param {T} popupComponentType Component type
   * @param paramsObject modal parameters
   * @returns {NgbModalRef} Modal reference
   */
  openModalPopup<T>(popupComponentType: T, paramsObject: any = {},popupOptions: any = {}): NgbModalRef {
    const ans: NgbModalRef = this.modalService.open(popupComponentType, {centered: true, beforeDismiss: () => false,  ...popupOptions});
    Object.keys(paramsObject).forEach(paramName => {
      ans.componentInstance[paramName] = paramsObject[paramName];
    })
    return ans;
  }

  /***
   * Shows alert modal
   * @param {string} title
   * @param {string} message
   * @param {AlertType} alertType
   */
  showAlert(title: string, message: string, alertType: AlertType, err:HttpErrorResponse = null) {
    const dialog = this.openDialog(AlertComponent, {size: 'sm'});
    this.setDialogProperties(dialog, title, message);
    const alertComponent = dialog.componentInstance as AlertComponent;
    alertComponent.closeButtonText = 'Close';
    alertComponent.err = err;
    alertComponent.alertType = alertType;
  }

  private setDialogProperties(dialog, title: string, message: string): void {
    const confirm: Dialog = dialog.componentInstance;
    confirm.title = title;
    confirm.message = message;
  }

  private openDialog(component: any, options?: any): NgbModalRef {
    const dialog = this.modalService.open(component, {centered: true, beforeDismiss: () => false, ...options});
    dialog.result.then(() => this.store.dispatch(new SetDialogState(false))
      , () => this.store.dispatch(new SetDialogState(false)));
    return dialog;
  }

  openDialog1(options?: DialogWrapperOptions): NgbModalRef {

    let modelOptions:any  = {
      centered: true,
      beforeDismiss: () => false
    };

    if(!_.isEmpty(options.windowClass)) {
      modelOptions.windowClass = options.windowClass;
    }

    const dialog = this.modalService.open(DialogWrapperComponent, modelOptions);
    const wrapperDialog: DialogWrapper = dialog.componentInstance;
    wrapperDialog.set(options);

    return dialog;
  }

  showCrudOperationsError = function(errObj: BadHttpRequestException) {
    if (errObj.message) {
      this.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG', {message: errObj.message}), AlertType.Error, null);
    }
    else {
      this.showAlert(this.i18nService.translateId('ERROR'), this.i18nService.translateId('MESSAGES.ERROR_MSG_NO_MSG', {}), AlertType.Error, null);
    }
  }

}

import * as _ from 'lodash';
import {Component, ComponentFactoryResolver, OnInit, ViewChild, ElementRef, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DialogWrapperOptions} from "@shared-ui/components/modal-dialogs/types/dialog.interface";
import {TreeGridInnerItemBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";

declare let $: any;

@Component({
  selector: 'dialog-wrapper',
  templateUrl: './dialog-wrapper.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DialogWrapperComponent implements OnInit {

  close;
  dismiss;
  title;

  @ViewChild('viewContainer', {read: ViewContainerRef}) viewContainer: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private activeModal: NgbActiveModal) { }

  ngOnInit() {

  }

  //-------------------------------------------

  private set(options: DialogWrapperOptions) {

    this.title = options.title;

    const factory = this.componentFactoryResolver.resolveComponentFactory(options.innerComponent);
    const ref = this.viewContainer.createComponent(factory);

    this.setClose(options);
    this.setDismiss(options);


    (ref.instance)['data'] = options.data;
    (ref.instance)['wrapper'] = this;

    ref.changeDetectorRef.detectChanges();
  }

  //-------------------------------------------

  setClose(options) {

    this.close = (data) => {
      if(_.isFunction(options.close)) {
        options.close(data);
      } else {
        this.closeDialog(true);
      }
    };
  }

  //-------------------------------------------


  setDismiss(options) {

    this.dismiss = (e) => {
      if(_.isFunction(options.dismiss)) {
        options.dismiss();
      } else {
        this.closeDialog(true);
      }
      this.closeDialog(true);
    };
  }

  //-------------------------------------------

  closeDialog(result: boolean) {
    this.activeModal.close(result);
  }
}

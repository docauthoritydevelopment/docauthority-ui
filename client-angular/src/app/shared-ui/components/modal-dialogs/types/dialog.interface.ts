export interface Dialog {
  title: string;
  message: string;
}


export interface DialogWrapper {
  set: (options: DialogWrapperOptions) => void;
}

export interface DialogWrapperOptions {
  data:any;
  title: string;
  innerComponent: any;
  close: any;
  dismiss?: any;
  windowClass?: string;
}

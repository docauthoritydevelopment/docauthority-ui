export enum AlertType {
  Error="Error",
  Info="Info",
  Debug="Debug",
  Fatal="Fatal"
}

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";

@Component({
  selector: 'toolbar-actions',
  templateUrl: './toolbarActions.component.html',
  styleUrls: ['../toolbar.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarActionsComponent extends BaseComponent {

  //----------------------------------------------
  // input
  //----------------------------------------------
  @Input() actionItems:ButtonConfigDto[];

  //----------------------------------------------
  // daOnInit
  //----------------------------------------------
  daOnInit() {
    if (this.actionItems) {
      this.actionItems.sort((a, b) => (a.order >= b.order) ? 1 : -1);
    }
  }

  //----------------------------------------------

  onClickAction = (action:MenuItemConfigDto) => {
    if(action.actionFunc && !action.disabled) {
      action.actionFunc();
    }
  };
}

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";

@Component({
  selector: 'toolbar-title',
  templateUrl: './toolbarTitle.component.html',
  styleUrls: ['../toolbar.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarTitleComponent extends BaseComponent {

  //----------------------------------------------
  // input
  //----------------------------------------------
  @Input() title:string;
  @Input() totalElements:number;
  @Input() totalFilteredElements:number;
  @Input() refreshActionItem:ButtonConfigDto;

  //----------------------------------------------
  // daOnInit
  //----------------------------------------------
  daOnInit() {}

  //----------------------------------------------

  onClickAction = (action:MenuItemConfigDto) => {
    if(action.actionFunc && !action.disabled) {
      action.actionFunc();
    }
  };
}

import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";
import {SearchBoxConfigDto} from "@app/common/types/searchBoxConfig.dto";
import {MenuItemConfigDto} from "@app/common/types/menuItemConfig.dto";

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarComponent extends BaseComponent {

  //----------------------------------------------
  // input
  //----------------------------------------------
  @Input() title:string;
  @Input() totalElements:number;
  @Input() refreshActionItem:ButtonConfigDto;
  @Input() settingsActionItems:ButtonConfigDto[];
  @Input() monitorActionItems:ButtonConfigDto[];
  @Input() searchBox:SearchBoxConfigDto;
  @Input() exportActionItem:ButtonConfigDto;
  @Input() sideMenuActionItems:ButtonConfigDto;
  @Input() totalFilteredElements:number = null;


  //----------------------------------------------
  // daOnInit
  //----------------------------------------------
  daOnInit() {}

  //----------------------------------------------

  onClickAction = (action:MenuItemConfigDto) => {
    if(action.actionFunc && !action.disabled) {
      action.actionFunc();
    }
  };

  //----------------------------------------------

  onSearchChanged = (newText:string) => {
    this.searchBox.searchFunc(newText);
  };
}

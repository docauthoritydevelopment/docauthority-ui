import {Component, Input, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from "@core/base/baseComponent";
import {ButtonConfigDto} from "@app/common/types/buttonConfig.dto";

@Component({
  selector: 'toolbar-dropdown-actions',
  templateUrl: './toolbarDropdownActions.component.html',
  styleUrls: ['../toolbar.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarDropdownActionsComponent extends BaseComponent {

  //----------------------------------------------
  // input
  //----------------------------------------------
  @Input() actionItem:ButtonConfigDto;

  //----------------------------------------------
  // daOnInit
  //----------------------------------------------
  daOnInit() {}
}

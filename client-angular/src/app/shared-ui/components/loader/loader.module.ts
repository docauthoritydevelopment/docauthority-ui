import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from './loader.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoaderComponent,
  ],
  exports: [
    LoaderComponent
  ],
  providers: [
  ]
})
export class LoaderModule { }

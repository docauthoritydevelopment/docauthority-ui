import {Component, OnInit, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader.template.html',
  styleUrls: ['./_loader.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoaderComponent implements OnInit {

  @Input() show;

  constructor() {
  }

  ngOnInit(): void {
  }
}

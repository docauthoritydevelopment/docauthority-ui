import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'progressBar',
  templateUrl: './progressBar.template.html',
  styleUrls: ['./progressBar.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ProgressBarComponent {

  @Input() value;
  @Input() maxValue;
  @Input() displayText;
  @Input() color = '#6094c4';
  @Input() leftWithNoRadius:boolean = false;
  @Input() rightWithNoRadius:boolean = false;
  @Input() showPercentage:boolean = false;
  @Input() progressTitle:string = null;
  percentageText:string='';


  constructor() {
  }

  ngOnInit() {
    if (this.maxValue == 0 && this.value> 0) {
      this.percentageText  = this.value+ ' files';
    }
    else if (this.maxValue == 0 && this.value==0) {
      this.percentageText  = '0%';
    }
    else {
      this.percentageText = Math.floor(this.value * 100 / this.maxValue) + '%';
    }
  }
}

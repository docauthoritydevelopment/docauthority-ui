import { Component, ViewChild, Input, ElementRef } from '@angular/core';

declare var $: any;
declare var _: any;

export interface DropdownItem {
  key: any;
  val: any;
  icon?: string;
}

@Component({
  selector: 'dropdown',
  templateUrl: './dropdown.template.html'
})

export class DropdownComponent {

  @Input() items: DropdownItem[];
  @Input() initialKey;
  @Input() direction;
  @Input() captionMode;
  @Input() onItemChanged;

  @ViewChild('el') el:ElementRef;

  private elm;
  initialVal: string;
  isDropdownOpen: boolean;

  constructor() {
  }

  //--------------------------------------------------

  ngOnInit(): void {
    this.setInitialValue();
    this.elm = $(this.el.nativeElement);
  }

  //--------------------------------------------------

  setInitialValue() {
    if(_.isEmpty(this.initialKey)) {
      this.initialKey = !_.isEmpty(this.items) ? this.items[0].key : null;
    }
    _.some(this.items, (item) => {
      if(item.key === this.initialKey){
        this.initialVal = item.val;
        return true;
      }
    });
  }

  //--------------------------------------------------

  showDropdown(): void {
    this.isDropdownOpen = true;
    //this.perfectScrollbarService.adjustScrollbar(this.elm.find('.inner-list'));
  }

  //--------------------------------------------------

  closeDropdown(e: Event): void {
    let target = $(event.target);
    if(_.isEmpty(target.closest('.input'))){
      this.isDropdownOpen = false;
    }
  }

  //--------------------------------------------------

  onItemClick(item) {
    this.elm.find('.caption').html(item.val);
    this.isDropdownOpen = false;

    if(_.isFunction(this.onItemChanged)) {
      this.onItemChanged(item.key);
    }
  }
}

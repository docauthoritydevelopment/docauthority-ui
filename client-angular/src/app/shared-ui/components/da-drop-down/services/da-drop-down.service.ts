import {Injectable} from '@angular/core';
import {Subject} from "rxjs/index";



@Injectable()
export class DaDropDownService {

  daDropDownOpenCount:number = 0;

  private closeDaDropDownEvent = new Subject<number>();
  closeDaDropDownEvent$ = this.closeDaDropDownEvent.asObservable();

  constructor()
  {
  }

  onDaDropDownOpen() {
    this.daDropDownOpenCount++;
  }

  onDaDropDownClose() {
    this.daDropDownOpenCount = 0 ;
  }

  clearDropDownCount() {
    this.daDropDownOpenCount = 0 ;
  }


  isDaDropDownOpen():boolean {
    return (this.daDropDownOpenCount != 0);
  }

  public closeAllDropDowns() {
    this.closeDaDropDownEvent.next();
  }

}

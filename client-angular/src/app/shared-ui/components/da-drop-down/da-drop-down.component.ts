import {Component, Input, ViewEncapsulation, ViewChild, ElementRef} from '@angular/core';
import {NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import {DropDownConfigDto} from "@app/common/types/dropDownConfig.dto";
import {BaseComponent} from "@core/base/baseComponent";

@Component({
  selector: 'da-drop-down',
  templateUrl: './da-drop-down.template.html',
  styleUrls: ['./da-drop-down.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DaDropDownComponent extends  BaseComponent{
  @ViewChild('myDrop') myDrop: NgbDropdown;

  @ViewChild('openBtn') openBtn:ElementRef;

  @Input()
  openIfOnlyOne: boolean = false;


  @Input()
  dropDownItems: DropDownConfigDto[];

  @Input()
  dropDownItemsFunc: any = null;

  @Input()
  dropDownWidth: string = null;

  @Input()
  showItem: any;

  @Input()
  additionalParams:any = null;

  @Input()
  enabled: boolean = false;

  @Input()
  openOnLeft: boolean = false;

  @Input()
  openSubMenuOnRight: boolean = false;

  @Input()
  withArrow: boolean = false;

  isInBottom:boolean = false;

  @Input()
  alsoOpenInTop: boolean = false;

  @Input()
  withChangedPosition: boolean = false;

  itemClick(selectItem:DropDownConfigDto) {
    if (selectItem.actionFunc) {
      if (this.additionalParams) {
        selectItem.actionFunc(this.additionalParams);
      }
      else {
        selectItem.actionFunc();
      }
    }
  }

  constructor() {
    super();
  }

  daOnInit(){
    if (this.dropDownItemsFunc) {
      this.dropDownItems = this.dropDownItemsFunc(this.showItem);
    }
    this.on(this.daDropDownService.closeDaDropDownEvent$, () => {
      this.closeDropDown();
    });
  }

  onDropDownOpenChanged(isOpen) {
    if (isOpen) {
      this.daDropDownService.onDaDropDownOpen();
    }
    else {
      this.daDropDownService.onDaDropDownClose();
    }

  }

  private closeDropDown() {
    if (this.myDrop.isOpen()) {
      this.myDrop.close();
    }
  }


  changeDropDown(event) {
    if (this.openIfOnlyOne  && this.dropDownItems && this.dropDownItems.length == 1 && this.dropDownItems[0].actionFunc) {
      this.dropDownItems[0].actionFunc();
    }
    else {
      if (this.myDrop.isOpen()) {
        this.myDrop.close();
      }
      else {
        if (this.alsoOpenInTop) {
          var rect = this.openBtn.nativeElement.getBoundingClientRect();
          var html = document.documentElement;
          this.isInBottom = ((rect.bottom + this.dropDownItems.length * 35 + 150) > (window.innerHeight || html.clientHeight));
        }
        this.myDrop.open();
      }
    }
  }
}

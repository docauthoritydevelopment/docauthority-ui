import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Subject} from "rxjs/internal/Subject";
import {debounceTime} from "rxjs/operators";
import {Subscription} from "rxjs/internal/Subscription";

@Component({
  selector: 'search-box',
  templateUrl: './searchBox.template.html',
  styleUrls: ['./searchBox.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SearchBoxComponent {

  _searchText='';
  textChanged:boolean = false;
  debouncer = new Subject<string>();
  debouncerSubscription: Subscription = null;
  showClearBtn = false;

  @Input() boxTitle;
  @Input() boxPlaceholder;
  @Input() notifyOnKeyup;
  @Input() debounceTime: number = 50;
  @Output() searchChanged = new EventEmitter();

  @Input()
  set searchTerm(newValue){
    this.searchText = newValue;
    this.showClearBtn = (this.searchText != null && this.searchText.length > 0 );
  }

  get searchTerm() {
    return this.searchText;
  }

  set searchText(newText:string){
    this._searchText = newText;
    this.textChanged = true;
  }

  get searchText():string {
    return this._searchText;
  }


  constructor() {
    this.debouncerSubscription = this.debouncer.pipe(debounceTime(this.debounceTime))
      .subscribe(
      (value: string) => { this.doSearch(value) },
      (error) => { console.log(error); }
    );
  }

  onKeyup() {
    if(this.notifyOnKeyup) {
      this.showClearBtn =  (this.searchText && this.searchText.length > 0);
      this.debouncer.next(this.searchText);
    }
  }

  doSearch(textToSearch:string = null) {
    if (this.textChanged  || textToSearch) {
      let theNewText = textToSearch ? textToSearch : this.searchText;
      this.showClearBtn = (theNewText  != null && theNewText.length > 0 );
      this.searchChanged.emit(theNewText);
      this.textChanged = false;
    }
  }

  clearSearch(){
      this._searchText = '';
      this.searchChanged.emit(this.searchText);
    this.showClearBtn = false;
      this.textChanged = false;
  }

  ngOnDestroy() {
    this.debouncerSubscription.unsubscribe();
  }

}

import * as _ from 'lodash';
import {OnDestroy} from '@angular/core';
import {TREE_GRID_ITEM_MORE_INFO} from '../types/tree-grid.more-info.interfaces';
import {TreeGridDataItem} from '../types/tree-grid.type';
import {logging} from "selenium-webdriver";
import getLevel = logging.getLevel;

export class TreeGridInnerItemBaseComponent<T> implements OnDestroy {

  public $el;
  public moreInfo: TREE_GRID_ITEM_MORE_INFO;
  private contextMenuSelector = _.uniqueId('tree-grid-item-context-menu');

  constructor() {
  }

  public hostComponent = null;
  public _hostComponentParams = null;
  public _dataItem: TreeGridDataItem<T>;
  public itemCheckedNotifyFunc = null;

  public hostComponentParamsChanged() {};


  detectIE11() {
    return (<any>document).documentMode && (<any>document).documentMode<=11 ;
  }

  getLeftPaddingWidth() {
    if (this.detectIE11()) {
      if (this.dataItem.level == 0) {
        return 0;
      }
      else if (this.dataItem.level == 1) {
        return 6;
      }
      else {
        return 6 + (this.dataItem.level - 1) * 25;
      }
    }
    else {
      if (this.dataItem.level == 0) {
        return 0;
      }
      else if (this.dataItem.level == 1) {
        return 15;
      }
      else {
        return 15 + (this.dataItem.level - 1) * 25;
      }
    }
  }

  get dataItem(): TreeGridDataItem<T> {
    return this._dataItem;
  }

  set dataItem(data: TreeGridDataItem<T>) {
    this._dataItem = data;
  }

  set hostComponentParams(newParams: any) {
    this._hostComponentParams = newParams;
    this.hostComponentParamsChanged();
  }

  get hostComponentParams():any  {
    return this._hostComponentParams;
  }

  notifyCheckChange() {
    if (this.itemCheckedNotifyFunc) {
      this.itemCheckedNotifyFunc();
    }
  }

  setEl(el) {
    this.$el = el;
  }

  setMoreInfo(moreInfo: TREE_GRID_ITEM_MORE_INFO) {
    this.moreInfo = moreInfo;
  }


  ngOnDestroy(): void {
  }
}


export class TreeGridHeaderBaseComponent  {

  constructor() {
  }

  public hostHeaderParams = null;

}


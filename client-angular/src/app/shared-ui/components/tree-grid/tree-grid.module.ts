import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClickOutsideModule} from 'ng-click-outside';
import {ContextMenuModule} from "ngx-contextmenu";

import {TreeGridComponent} from './components/tree-grid.component';
import {TreeGridHeaderWrapperComponent} from './components/tree-grid-header/tree-grid-header.component';
import {TreeGridPainterComponent} from './components/tree-grid-painter/tree-grid-painter.component';
import {TreeGridPagerComponent} from './components/tree-grid-pager/tree-grid-pager.component';
import {TreeGridItemComponent} from './components/tree-grid-item/tree-grid-item.component';
import {TreeGridAdapterService} from '@shared-ui/components/tree-grid/services/tree-grid-adapter.service';
import {UtilitiesModule} from '@app/common/utilities/utilities.module';
import {TreeGridDataService} from "@tree-grid/services/tree-grid.data.service";
import {TreeGridMarkingService} from "@tree-grid/services/tree-grid-marking.service";
import {TreeGridItemsService} from "@tree-grid/services/tree-grid-items.service";
import {TreeGridSelectionSwitcherComponent} from "@tree-grid/components/selection-switcher/tree-grid-selection-switcher.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    UtilitiesModule,
    ContextMenuModule.forRoot({useBootstrap4: true})
  ],
  declarations: [
    TreeGridComponent,
    TreeGridPainterComponent,
    TreeGridItemComponent,
    TreeGridPagerComponent,
    TreeGridHeaderWrapperComponent,
    TreeGridSelectionSwitcherComponent
  ],
  providers: [
    TreeGridItemsService,
    TreeGridMarkingService,
    TreeGridDataService,
    TreeGridAdapterService
  ],
  exports: [
    TreeGridComponent,
    TreeGridSelectionSwitcherComponent
  ]
})
export class TreeGridModule {
}

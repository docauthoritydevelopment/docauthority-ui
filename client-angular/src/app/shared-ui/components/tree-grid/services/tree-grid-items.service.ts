import * as _ from 'lodash';
import {TreeGridItemComponent} from '../components/tree-grid-item/tree-grid-item.component';
import {Injectable} from "@angular/core";

@Injectable()
export class TreeGridItemsService {

  public itemListComponents: { [id: number]: TreeGridItemComponent<any> } = {};

  constructor() {
  }

  //----------------------------------------------

  public add(id, itemComponent: TreeGridItemComponent<any>){
    this.itemListComponents[itemComponent.dataItem.id] = itemComponent;
  }

  //----------------------------------------------

  public remove(id: number){
    delete this.itemListComponents[id];
  }

  //----------------------------------------------

  public getItem(id):TreeGridItemComponent<any>  {
    return this.itemListComponents[id];
  }

  //----------------------------------------------

  public getItems(ids:number[]) {
    return _.filter(this.itemListComponents, (item, itemId) => {
      return _.includes(ids, _.toInteger(itemId));
    });
  }

  //----------------------------------------------

  public getDataItems(ids:number[]) {

    let items = this.getItems(ids);

    return _.map(items, (item) => {
      return item.dataItem;
    });
  }

  //----------------------------------------------

  public getItemsId(): number[]  {
    return _.map(_.keys(this.itemListComponents), Number);
  }
}



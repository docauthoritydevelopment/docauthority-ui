import {MarkType} from '../types/tree-grid.type';
import {Injectable} from "@angular/core";
import {TreeGridExcludeItemType} from "@tree-grid/types/tree-grid.type";

declare let _: any;

@Injectable()
export class TreeGridMarkingService {

  private markedItems : number [] = [];
  private partialMarkedItems = {};

  //----------------------------------------

   updateMarking(item, children, parents, isMarked: boolean): void{
    isMarked = !!isMarked;

    let items = _.union(item, children);

     if(isMarked) {
        this.addMarkedItem(items);
        this.updatePartialMarks(parents, item);
     } else {
       this.removeMarkedItem(items);
       this.clearPartialMarks(items, parents, item)
     }
  }

  //---------------------------------------

  excludeItems(item, children, excludeType) {
    let items = excludeType === TreeGridExcludeItemType.item ? item : children;
    this.removeMarkedItem(items);
  }

  //----------------------------------------

   updateMarkingListByNewItem(newIds: any[]) {
    let updatedItems = [];
    this.markedItems.forEach((mItem:number) => {
      if (newIds.indexOf(mItem)>-1) {
        updatedItems.push(mItem);
      }
    });
    this.markedItems = updatedItems;
  }

  //----------------------------------------

   clearAllMarking() {
    this.markedItems = [];
  }

  //----------------------------------------

   getMarkedItems(): number[] {
      return this.markedItems;
  }

  //---------------------------------------

   markAll(items, isMark) {
    if(isMark) {
      this.markedItems = items;
    } else {
      this.markedItems = [];
    }
  }
  //----------------------------------------------

  getMarkType = (itemId) => {
    if(this.isMarked(itemId)) {
      return MarkType.all;
    }
    if(!_.isEmpty(this.partialMarkedItems[itemId])) {
      return MarkType.partial;
    }
    return null;
  };

  //----------------------------------------

  public isMarked(item:number): boolean {
    return _.includes(this.markedItems, item);
  }

  //----------------------------------------

  private addMarkedItem(itemsToAdd : number[]) {
    this.markedItems = _.union(this.markedItems, itemsToAdd);
  }

  //----------------------------------------

  private removeMarkedItem(itemsToRemove : number[]) {
    this.markedItems = _.difference(this.markedItems, itemsToRemove);
  }

  //---------------------------------------

  private updatePartialMarks(parents, item) {
    _.forEach(parents, (parent) => {
      this.partialMarkedItems[parent] = _.union(this.partialMarkedItems[parent] || [], item);
    });
  }

  //---------------------------------------

  private clearPartialMarks(items, parents, item) {
    _.forEach(items, (item) => {
      this.partialMarkedItems[item] = {};
    });
    this.partialMarkedItems[item] = {};
    _.forEach(parents, (parent) => {
      this.partialMarkedItems[parent] = _.difference(this.partialMarkedItems[parent] || [], item);
    });
  }
}



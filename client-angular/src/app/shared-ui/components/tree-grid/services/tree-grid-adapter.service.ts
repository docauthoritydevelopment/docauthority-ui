import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {TreeGridAdapterMapper, TreeGridData, TreeGridDataItem, TreeGridPagingInfo} from '../types/tree-grid.type';

declare var property: any;

@Injectable()
export class TreeGridAdapterService {

  public adapt(data: {}, selectedItem: any, mapper: TreeGridAdapterMapper, hasChildren?: (dataItem) => any): TreeGridData<any> {

    const treeGridData = new TreeGridData();

    treeGridData.totalElements = data['totalElements'];
    treeGridData.items = this.createTree(data, mapper, hasChildren);
    treeGridData.paging = this.getPagingInfo(data, mapper);
    treeGridData.selectedItem = this.getSelectedItem(treeGridData, mapper, selectedItem);
    treeGridData.selectedItemParents = this.getSelectedItemParents(treeGridData, mapper, treeGridData.selectedItem);

    return treeGridData;
  }

  //---------------------------------------------

  private createTree(data, mapper, hasChildren) {

    const ROOT_ID = '-1';

    const groupedNodes = this.groupNodes(mapper.contentProperty?data[mapper.contentProperty]:data, mapper, hasChildren);
    const tree = this.organizeNodesHierarchically(groupedNodes, groupedNodes[ROOT_ID], mapper.idProperty, 0);

    return tree;
  }

  //---------------------------------------------

  private groupNodes(content: any[], mapper, hasChildren): {} {

    return content.reduce((prev, item) => {

      const treeNode = new TreeGridDataItem<any>(null);

      treeNode.item = item;
      treeNode.id = this.getNestedProperty(item, mapper.idProperty);
      treeNode.parentId = this.getNestedProperty(item, mapper.parentIdProperty) || -1;
      treeNode.hasMoreChildren = _.isFunction(hasChildren) ? hasChildren(item) : false;

      if (_.has(prev, treeNode.parentId) && treeNode.parentId) {
        prev[treeNode.parentId].push(treeNode);
        return prev;
      }
      prev[treeNode.parentId] = [treeNode];
      return prev;
    }, {});
  }

  //---------------------------------------------

  private getRootId(groupedNodes, mapper, data) {

    const groupedArrayByID = _.keys(groupedNodes);

    const arrayByID = _.map(mapper.contentProperty ? data[mapper.contentProperty] : data, (item) => {
      return this.getNestedProperty(item, mapper.idProperty) + '';
    });
    return _.pullAll(groupedArrayByID, arrayByID);
  }

  //---------------------------------------------

  private organizeNodesHierarchically(groupedNodes, nodeChildren, customID, level) {
    const tree = [];

    const childrenIds = _.map(nodeChildren, (node) => {
      return node.id;
    });

    _.each(nodeChildren, (node) => {
      if (node) {
        node.level = level;
        node.siblings = childrenIds;
        node.children = this.organizeNodesHierarchically(groupedNodes, groupedNodes[node.id], customID,level+1);
      }
      tree.push(node);
    });
    return tree;
  }

  //---------------------------------------------

  private getPagingInfo(data, mapper) {
    const pagingInfo = new TreeGridPagingInfo();

    let pageNum = this.getNestedProperty(data, mapper.pageNumberProperty) || 0;
    pagingInfo.currentPage = pageNum + 1;

    let totalPages = this.getNestedProperty(data, mapper.totalPagesProperty) || 1;
    pagingInfo.totalPages = totalPages;

    return pagingInfo;
  }

  //---------------------------------------------

  private getSelectedItem(treeGridData, mapper, selectedItem) {

    if (!_.isEmpty(treeGridData.items)) {
      const item = this.findItem(treeGridData.items, selectedItem);
      if (item) {
        return selectedItem;
      }
      return treeGridData.items[0].id;
    }
    return null;
  }

  //---------------------------------------------

  private getSelectedItemParents(treeGridData, mapper, selectedItem) {

    let parents = [];

    const setParent = (_item) => {
      const item = this.findItem(treeGridData.items, _item);

      if(item.parentId > -1) {
        parents.push(item.parentId);
        setParent(item.parentId);
      }
    };
    if(!_.isNull(selectedItem)) {
      setParent(selectedItem);
    }
    return parents;
  }

  //---------------------------------------------

  private getNestedProperty(object, nestedProperty) {
    if (_.isObject(object)) {
      const split = nestedProperty.split('.');
      return split.reduce(function (obj, prop) {
        return obj && obj[prop];
      }, object);
    }
    return null;
  }

  //---------------------------------------------

  private findItem(list, itemId) {
    let itemList = null;
    const find = (items) => {
      _.each(items, (item) => {
        if (item.id === itemId) {
          itemList = item;
        }
        if (item.children) {
          find(item.children);
        }
      });
    };
    find(list);
    return itemList;
  }
}


import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { TreeGridData, TreeGridDataItem} from '@tree-grid/types/tree-grid.type';


@Injectable()
export class TreeGridDataService {


  getParentsIds(treeGridData, itemId) {
    let item = this.findItem(treeGridData.items, itemId);

    if(item) {
      let parents = item.item['parents'] || [];
      return _.difference(parents, itemId);
    }
    return [];
  }


  //--------------------------------------------------

  public getItemChildren(data:TreeGridData<any>, itemId) {

    let childList = [];

    let countChildren = (item) => {
      if (item && item.children) {
        _.forEach(item.children, (child) => {
          childList.push(child.id);
          countChildren(child);
        });
      }
    };

    let item = this.findItem(data.items, itemId);
    countChildren(item);
    return childList;
  }

  //--------------------------------------------------

  public getItemParent(data:TreeGridData<any>, itemId) {

    let item = this.findItem(data.items, itemId);

    if(item.parentId == -1) {
      return data.items[0];
    }

    return this.findItem(data.items, item.parentId);
  }

  //---------------------------------------------

  updateOpenNodes(newData, treeGridData)  {
    let openMap: any  = {};
    if (treeGridData) {
      this.getOpenNodes(openMap, treeGridData.items);
    }
    this.getParentNodes(openMap,newData.items, newData.selectedItemParents);
    this.setOpenNodes(openMap,newData.items);
  }

  //----------------------------------------------

  getOpenNodes(openMap, nodesList: any[]) {
    for (let count = 0 ; count < nodesList.length ; count++) {
      if (nodesList[count].isOpen) {
        openMap[nodesList[count].id] = true;
        if (nodesList[count].children ) {
          this.getOpenNodes(openMap, nodesList[count].children);
        }
      }
    }
  }

  //----------------------------------------------

  getParentNodes(openMap, items, selectedItemParents) {
    _.forEach(items, (node) => {
      if(_.includes(selectedItemParents, node.id)) {
        openMap[node.id] = true;
      }
      if (node.children ) {
        this.getParentNodes(openMap, node.children, selectedItemParents);
      }
    })
  }

  //----------------------------------------------

  setOpenNodes(openMap, nodesList: any[]) {
    for (let count = 0 ; count < nodesList.length ; count++) {
      let nodeIsOpen: boolean = false;
      nodeIsOpen =  (openMap[nodesList[count].id] === true);

      if (nodeIsOpen) {
        if (nodesList[count].children &&  nodesList[count].children.length>0) {
          nodesList[count].isOpen =  true;
        }
        if (nodesList[count].children) {
          this.setOpenNodes(openMap, nodesList[count].children);
        }
      }
    }
  }

  //--------------------------------------------------

  filterData(data, originalData, filterBy) {
    if(_.isFunction(filterBy)) {
      data.items = this.filterRecursiveData(originalData.items, filterBy);
    }
    else {
      data.items = originalData.items;
    }
  }

  getDisplayedItemsNum(data) {
    return this.findRecursiveItemNum(data.items);
  }

  findRecursiveItemNum(items: TreeGridDataItem<any>[]):number {
    let ans = 0 ;
    items.forEach((dataItem : TreeGridDataItem<any>)=> {
      if (dataItem.children) {
        ans = ans +  this.findRecursiveItemNum(dataItem.children);
      }
      ans++;
    });
    return ans;
  }

  filterRecursiveData(items: TreeGridDataItem<any>[],filterBy:any): TreeGridDataItem<any>[]  {
    let ans : TreeGridDataItem<any>[] = [];
    items.forEach((dataItem : TreeGridDataItem<any>)=>{
      if (dataItem.children) {
        let childAns : TreeGridDataItem<any>[] = this.filterRecursiveData(dataItem.children, filterBy);
        let isItemFiltered: boolean = filterBy(dataItem.item);
        if (childAns.length > 0 || isItemFiltered){
          let ansItem = _.cloneDeep(dataItem);
          if (!isItemFiltered) {
            ansItem.children = childAns;
            //ansItem.isOpen = true;
          }
          ansItem.isOpen = ( childAns.length > 0 ); //openMap[ansItem.id] != null;
          ans.push(ansItem);
        }
      }
      else {
        if (filterBy(dataItem.item)) {
          let ansItem = _.cloneDeep(dataItem);
          ansItem.isOpen = false; //openMap[ansItem.id] != null;
          ans.push(ansItem);
        }
      }
    });
    return ans;
  }


  /*
  closeRecursiveData(items: TreeGridDataItem<any>[]): TreeGridDataItem<any>[]  {
    let ans : TreeGridDataItem<any>[] = [];
    items.forEach((dataItem : TreeGridDataItem<any>)=>{
      if (dataItem.children) {
        let childAns : TreeGridDataItem<any>[] = this.closeRecursiveData(dataItem.children);
        let ansItem = _.cloneDeep(dataItem);
        ansItem.children = childAns;
        ansItem.isOpen = false;
        ans.push(ansItem);
      }
      else {
        let ansItem = _.cloneDeep(dataItem);
        ansItem.isOpen = false; //openMap[ansItem.id] != null;
        ans.push(ansItem);
      }
    });
    return ans;
  }
*/

  //--------------------------------------------------

  sortData(data, originalData, sortObj) {
    if(data) {
      let order = (<string>sortObj.sortOrder).toLowerCase();
      data.items = _.orderBy(data.items, [dataItem => dataItem.item[sortObj.sortBy].toLowerCase()] , [order]);
    }
  }

  //--------------------------------------------------

  public findItem<T>(list, itemId): TreeGridDataItem<any> {
    let itemList = null;
    let find = (items) => {
      _.each(items, (item) => {
        if (item.id == itemId) {
          itemList = item;
        }
        if (item.children) {
          find(item.children);
        }
      });
    };
    find(list);
    return itemList;
  }



  public findItemsByIds<T>(gridData : TreeGridData<T>, idList: number[]): TreeGridDataItem<T>[] {
    let itemList:TreeGridDataItem<T>[] = [];
    let idsMap={};
    idList.forEach((id) => {
      idsMap[id]  = true;
    });
    let find = (items:TreeGridDataItem<T>[]) => {
      _.each(items, (dataItem:TreeGridDataItem<T>) => {
        if (idsMap[dataItem.id]) {
          itemList.push(dataItem);
        }
        if (dataItem.children) {
          find(dataItem.children);
        }
      });
    };
    find(gridData.items);
    return itemList;
  }
}

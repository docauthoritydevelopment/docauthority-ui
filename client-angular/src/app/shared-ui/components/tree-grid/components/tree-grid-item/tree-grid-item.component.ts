import * as _ from 'lodash';
import { Component, OnInit, OnChanges, ElementRef, ViewEncapsulation } from '@angular/core';
import { OnDestroy, ComponentFactoryResolver, ViewContainerRef, ViewChild, Input } from '@angular/core';
import { TreeGridInnerItemBaseComponent } from '@tree-grid/base/tree-grid-inner-item.base';
import { ContextMenuService } from 'ngx-contextmenu';
import {TreeGridDataItem, MarkType, TreeGridConfig} from '@tree-grid/types/tree-grid.type';
import { TREE_GRID_ITEM_MORE_INFO } from "@tree-grid/types/tree-grid.more-info.interfaces";
import { TreeGridSelectionMode, TreeGridExcludeItemType} from "@tree-grid/types/tree-grid.type";
import { ContextMenuComponent } from 'ngx-contextmenu';

declare let $: any;

@Component({
  selector: 'tree-grid-item',
  templateUrl: './tree-grid-item.template.html',
  styleUrls: ['./_tree-grid-item.scss'],
  encapsulation: ViewEncapsulation.None

})

export class TreeGridItemComponent<T> implements OnInit, OnChanges, OnDestroy {

  @Input() parent;
  @Input() treeGridItemComponent;
  @Input() treeGridContextMenuComponent;
  @Input() treeGridConfig:TreeGridConfig;
  @Input() dataItem: TreeGridDataItem<T>;
  @Input() inTreeMode:boolean = false;
  @Input() hostComponent;
  @Input() hostComponentParams;
  @Input() selectionMode:TreeGridSelectionMode;


  @ViewChild('el') el: ElementRef;
  @ViewChild('viewContainer', {read: ViewContainerRef}) viewContainer: ViewContainerRef;
  @ViewChild(ContextMenuComponent) public markChkboxMenu: ContextMenuComponent;

  _elm;
  showPlusIcon;
  isItemMarked;
  isItemSelected;
  contextMenuRef;
  innerComponentRef;
  isItemPartialMarked;
  inLoadingSubTree:boolean = false;

  moreInfo: TREE_GRID_ITEM_MORE_INFO;
  TreeGridSelectionMode = TreeGridSelectionMode;
  TreeGridExcludeItemType = TreeGridExcludeItemType;

  //-------------------------------------------

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private contextMenuService: ContextMenuService) {
  }

  //-------------------------------------------

  ngOnInit(): void {
    this._elm = $(this.el.nativeElement);
    this.setInnerComponent();
  }

  //-------------------------------------------

  ngOnChanges(changes: any): void {
    if(changes.hostComponentParams) {
      if(this.innerComponentRef) {
        (<TreeGridInnerItemBaseComponent<T>>(this.innerComponentRef.instance)).hostComponentParams = this.hostComponentParams;
      }
    }
  }

  //-------------------------------------------

  showItemContextMenu($event: MouseEvent): void {
    if(this.treeGridContextMenuComponent && $($event.target).closest('.ignore-context-menu').length === 0) {

      $event.preventDefault();
      $event.stopPropagation();

      const factory = this.componentFactoryResolver.resolveComponentFactory(this.treeGridContextMenuComponent);
      this.contextMenuRef = this.viewContainer.createComponent(factory);
      this.updateComponent(this.contextMenuRef);

      this.contextMenuService.show.next({
        contextMenu: this.contextMenuRef.instance['contextmenu'],
        event: $event,
        item: {},
      });
    }
  }

  //-------------------------------------------

  private setInnerComponent() {

    const factory = this.componentFactoryResolver.resolveComponentFactory(this.treeGridItemComponent);
    this.innerComponentRef = this.viewContainer.createComponent(factory);
    this.updateComponent(this.innerComponentRef);

    this.isItemMarked = this.parent.isItemMarked() || (this.dataItem.id && this.parent.markingManager.isMarked(this.dataItem.id));
    this.showPlusIcon = this.dataItem.hasMoreChildren || !_.isEmpty(this.dataItem.children) || false;

    this.moreInfo = (<TreeGridInnerItemBaseComponent<T>>this.innerComponentRef.instance).moreInfo;
    this.parent.onItemAdd(this.dataItem.id, this);
  }

  //-------------------------------------------

  private updateComponent(componentRef) {

    (<TreeGridInnerItemBaseComponent<T>>(componentRef.instance)).dataItem = this.dataItem;
    (<TreeGridInnerItemBaseComponent<T>>(componentRef.instance)).hostComponent = this.hostComponent;
    (<TreeGridInnerItemBaseComponent<T>>(componentRef.instance)).hostComponentParams = this.hostComponentParams;

    componentRef.changeDetectorRef.detectChanges();
  }

  //-------------------------------------------

  onItemSelected(e) {

    if(this.selectionMode === this.TreeGridSelectionMode.single) {
      this.parent.onItemSelected(_.isNumber(this.dataItem.id) ? this.dataItem.id :  ( this.dataItem.additional ? this.dataItem.additional : null ), this.dataItem.item ? this.dataItem.item : this.dataItem.additional);
    }
  }

  //-------------------------------------------

  onPlusClicked($event, node) {
    if($event) {
      $event.stopImmediatePropagation();
    }

    if (this.inLoadingSubTree) {
      return;
    }

    let toggleOpening = () => {
      node.isOpen = !node.isOpen;
    };

    if (_.isEmpty(this.dataItem.children)) {
      this.inLoadingSubTree = true;
      this.parent.loadSubData(this.dataItem, () => {
        toggleOpening();
        this.inLoadingSubTree = false;
      });
    } else {
      toggleOpening();
    }
  }


  //-------------------------------------------

  toggleIconPlusClass() {

    this._elm.find('.k-icon').toggleClass('open');
    this._elm.closest('li').toggleClass('open');
  }

  //-------------------------------------------

  onMarkChanged(e) {
    if(e) {
      e.stopPropagation();
    }
    this.isItemMarked = !this.isItemMarked;
    this.parent.onItemMarkChanged([this.dataItem.id], this.isItemMarked);
  }

  //------------------------------------------

  onCheckboxOptionClicked(excludeType: TreeGridExcludeItemType) {
    this.parent.excludeMarkedItems([this.dataItem.id], excludeType);
  }


  //-------------------------------------------
  // called form parent
  //-------------------------------------------


  public updateMarking(markType: MarkType ) {
    this.isItemMarked = markType === MarkType.all;
    this.isItemPartialMarked = markType === MarkType.partial;
  }

  //-------------------------------------------

  public updateSelection(isSelected: boolean) {
    this.isItemSelected = isSelected;
  }

  //-------------------------------------------

  public open() {
    this.onPlusClicked(null, this.dataItem);
  }

  //-------------------------------------------

  public scrollTo() {
    if (!this.isElementInViewport(this._elm[0])) {
      this._elm[0].scrollIntoView(false);
    }
  }

  isElementInViewport = function(el) {

    var rect = el.getBoundingClientRect();

    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
      rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
  }

  //-------------------------------------------

  ngOnDestroy() {
    this.parent.onItemDestroy(this.dataItem.id);
  }
}

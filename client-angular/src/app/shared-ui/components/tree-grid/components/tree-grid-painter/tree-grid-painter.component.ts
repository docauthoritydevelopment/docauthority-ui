import {Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import {TreeGridSelectionMode} from "@tree-grid/types/tree-grid.type";

@Component({
  selector: 'tree-grid-painter',
  templateUrl: './tree-grid-painter.template.html',
  styleUrls: ['./_tree-grid-painter.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeGridPainterComponent implements OnInit {

  @Input() treeGridItemComponent;
  @Input() treeGridContextMenuComponent;
  @Input() treeGridConfig;
  @Input() treeGridItems;
  @Input() parent;
  @Input() hostComponent;
  @Input() hostComponentParams;
  @Input() treeGridItemParentComponentRef;
  @Input() inTreeMode:boolean = false;
  @Input() selectionMode:TreeGridSelectionMode;

  ngOnInit(): void {
    this.treeGridItems = this.treeGridItems || [];
  }
}

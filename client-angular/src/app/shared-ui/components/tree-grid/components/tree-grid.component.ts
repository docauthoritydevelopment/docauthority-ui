import * as _ from 'lodash'
import {
  Component,
  ElementRef, EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit, Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {TreeGridMarkingService} from '../services/tree-grid-marking.service';
import {TreeGridItemsService} from '../services/tree-grid-items.service';
import {ITreeGridEvents, TreeGridConfig, TreeGridData, TreeGridDataItem, TreeGridSortOrder} from '../types/tree-grid.type';
import {TreeGridItemComponent} from './tree-grid-item/tree-grid-item.component';
import {TreeGridDataService} from "@tree-grid/services/tree-grid.data.service";
import {TreeGridHeaderWrapperComponent} from "@tree-grid/components/tree-grid-header/tree-grid-header.component";
import {TreeGridSelectionMode} from "@tree-grid/types/tree-grid.type";

declare let $: any;

@Component({
  selector: 'tree-grid',
  templateUrl: './tree-grid.template.html',
  styleUrls: ['./_tree-grid.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TreeGridComponent<T> implements OnInit, OnChanges {
  elm;
  selectedItem: boolean;
  lastSortObject:any;


  _treeGridData: TreeGridData<T>;
  _originalTreeGridData: TreeGridData<T>;
  _localFilterFunction: (item: T) => boolean  = null;

  @ViewChild('el') el: ElementRef;
  @ViewChild(TreeGridHeaderWrapperComponent) headerWrapperComponent:TreeGridHeaderWrapperComponent;

  @Input() selectionMode:TreeGridSelectionMode = TreeGridSelectionMode.single;
  @Input() hostComponent;
  @Input() hostComponentParams;
  @Input() hostHeaderParams;
  @Input() inTreeMode:boolean = false;
  @Input() keepOpenNodes:boolean = false;
  @Input() getIdFunction:any  = null;
  @Input() initialSortDirection: TreeGridSortOrder;
  @Input() initialSortFieldName: string;
  @Input() selectFirstItem:boolean = true;
  @Input() scrollIntoSelected:boolean = false;

  @Input() treeGridConfig: TreeGridConfig;
  @Input() treeGridEvents: ITreeGridEvents<T>;
  @Input() treeGridItemComponent;
  @Input() treeGridHeaderComponent;
  @Input() treeGridContextMenuComponent;
  @Input() keepScrollLocation:boolean = false;
  @Output() onFilteredCountChanged = new EventEmitter<number>();

  @Input()
  set localFilterFunction(newFunc:(dataItem: T) => boolean) {
    this._localFilterFunction = newFunc;
    if (this._treeGridData) {
      this.filterDisplayedData(this._localFilterFunction)
    }
  }

  get localFilterFunction(): (dataItem: T) => boolean {
    return this._localFilterFunction;
  }

  //----------------------------------------------

  @Input()
  set treeGridData(newData: TreeGridData<T>) {
    this.setTreeGridData(newData);
  }
  get treeGridData(): TreeGridData<T> {
    return this._treeGridData
  }

  //----------------------------------------------

  constructor(
    private itemsManager: TreeGridItemsService,
    private markingManager:  TreeGridMarkingService,
    private dataService: TreeGridDataService) {

    this.treeGridEvents = this.treeGridEvents || {};
    this.treeGridConfig = this.treeGridConfig || new TreeGridConfig();
  }

  //----------------------------------------------

  ngOnInit(): void {
    this.elm = $(this.el.nativeElement);
    this.adjustGrid();
  }

  //----------------------------------------------

  ngOnChanges(): void {
    this.adjustGrid();
  }

  //----------------------------------------------

  markAllItems = (isMarked: boolean) => {

    this.markingManager.markAll(this.itemsManager.getItemsId(), isMarked);
    this.markComponents();
    this.notifyMarkChanged();
  };

  //----------------------------------------------

  loadSubData = (dataItem, callback) => {

    if (_.isFunction(this.treeGridEvents.onLoadSubTree)) {
      this.treeGridEvents.onLoadSubTree(dataItem).subscribe((resList : TreeGridDataItem<any>[]) => {
        dataItem.children = resList;
        if (_.isFunction(callback)) {
          callback();
        }
      });
    }
  };

  //----------------------------------------------

  onItemSelected = (itemId, item, silence?:boolean) => {
    if (this.treeGridConfig.disableSelection) {
      return;
    }
    this.updateSelection(false);
    this.treeGridData.selectedItem = itemId;
    this.updateSelection(true);

    if (!silence && _.isFunction(this.treeGridEvents.onItemSelected)) {
      this.treeGridEvents.onItemSelected(itemId, item);
    }
  };

  //----------------------------------------------

  onItemAdd = (id, itemComponent: TreeGridItemComponent<any>) => {
    this.itemsManager.add(id, itemComponent);
  };

  //----------------------------------------------

  onItemDestroy = (id: number) => {
    this.itemsManager.remove(id);
  };

  //----------------------------------------------

  private adjustGrid = _.debounce(() => {
    if (this.elm && this.treeGridData) {
      if (!this.treeGridConfig.disableSelection) {
        if (this.treeGridData.selectedItem && this.itemsManager.getItem(this.treeGridData.selectedItem)!= null ) {
          this.onItemSelected(this.treeGridData.selectedItem, this.itemsManager.getItem(this.treeGridData.selectedItem).dataItem.item);
          this.scrollTo(this.treeGridData.selectedItem);
        }
        else if (this.selectFirstItem && this.treeGridData.items.length > 0) {
          this.onItemSelected(this.treeGridData.items[0].id, this.itemsManager.getItem(this.treeGridData.items[0].id).dataItem.item);
          this.scrollTo(this.treeGridData.items[0].id);
        }
      }
    }
  }, 50);

  //----------------------------------------------

  moveToPage(pageNum) {
    if (this.treeGridEvents) {
      this.treeGridEvents.onPageChanged(pageNum);
    }
    this.markingManager.clearAllMarking();
  }

  //----------------------------------------------

  private setTreeGridData(data:TreeGridData<T>) {

    if (data != null) {
      let scrollTop: number = this.el.nativeElement.scrollTop;
      if (this.keepOpenNodes) {
        this.dataService.updateOpenNodes(data, this.treeGridData);
      }
      this._treeGridData = data;
      this._originalTreeGridData = _.extend({}, data); //this is a shallow copy

      this.markingManager.updateMarkingListByNewItem(this._treeGridData.items.map(item => item.id));
      if (this._localFilterFunction) {
        this.filterDisplayedData(this._localFilterFunction)
      }
      this.notifyMarkChanged();
      if(this.treeGridConfig.doLocalSorting) {
        let sortObj={
          sortBy : this.initialSortFieldName,
          sortOrder : this.initialSortDirection || TreeGridSortOrder.ASC
        }
        this.lastSortObject = sortObj;
        this.dataService.sortData(this._treeGridData, this._originalTreeGridData, sortObj);
      }
      if (this.keepScrollLocation) {
        setTimeout(() => {
          this.el.nativeElement.scrollTop = scrollTop;
        });
      }
    }
  }

  //---------------------------------------------

  markComponents() {

    _.forEach(this.itemsManager.itemListComponents, (itemComponent, itemId) => {
      if (itemComponent) {
        itemComponent.updateMarking(this.markingManager.getMarkType(_.toInteger(itemId)));
      }
    });
  }

  //----------------------------------------------

  notifyMarkChanged() {

    let markedItems = this.dataService.findItemsByIds(this._treeGridData , this.markingManager.getMarkedItems());

    if (this.treeGridEvents.onMarkedItemsChanged) {
      this.treeGridEvents.onMarkedItemsChanged(markedItems);
    }
    if(this.treeGridData && this.headerWrapperComponent) {
      this.headerWrapperComponent.onMarkedItemsChanged(this.treeGridData.items.length, markedItems.length);
    }
  }


  //----------------------------------------------
  // Called form treeGridItem
  //----------------------------------------------

  sortBy(sortObject: any) {
    this.lastSortObject = sortObject;
    if(this.treeGridConfig.doLocalSorting) {
      this.dataService.sortData(this._treeGridData, this._originalTreeGridData, sortObject);
    }
    else if (this.treeGridEvents) {
      this.treeGridEvents.onSortByChanged(sortObject);
    }
  }

  //----------------------------------------------

  onItemMarkChanged(itemId, isMarked) {

    let children = this.getItemChildren(itemId);
    let parents = this.dataService.getParentsIds(this.treeGridData, itemId);

    this.markingManager.updateMarking(itemId, children, parents, isMarked);
    this.markComponents();
    this.notifyMarkChanged();
  }

  //----------------------------------------------

  excludeMarkedItems = (itemId, excludeType) => {
    let children = this.getItemChildren(itemId);

    this.markingManager.excludeItems(itemId, children, excludeType);
    this.markComponents();
    this.notifyMarkChanged();
  };

  //----------------------------------------------

  updateSelection(isSelected) {
    const itemComponent = <TreeGridItemComponent<any>>(this.itemsManager.getItem(this.treeGridData.selectedItem));
    if (itemComponent) {
      itemComponent.updateSelection(isSelected);
    }
    else if (!this.treeGridConfig.disableSelection && this.selectFirstItem && this.treeGridData.items.length > 0) {
      const itemComponent = <TreeGridItemComponent<any>>(this.itemsManager.getItem(this.treeGridData.items[0].id));
      if (itemComponent) {
        itemComponent.updateSelection(isSelected);
      }
    }
  }

  //----------------------------------------------

  isItemMarked = (itemId): boolean => {
    return this.markingManager.isMarked(itemId);
  };


  //----------------------------------------------
  // API (External use)
  //----------------------------------------------

  scrollTo(selectedItem) {
    const itemComponent = <TreeGridItemComponent<any>>(this.itemsManager.getItem(selectedItem));
    if (itemComponent) {
      itemComponent.scrollTo();
    }
  }

  //----------------------------------------------

  openSelectedNode() {
    const itemComponent = <TreeGridItemComponent<any>>(this.itemsManager.getItem(this.treeGridData.selectedItem));
    if (itemComponent) {
      itemComponent.open();
    }
  }

  //----------------------------------------------

  getItemParent(itemId) {
    return this.dataService.getItemParent(this.treeGridData, itemId);
  }

  //----------------------------------------------

  getItemChildren(itemId) {
    return this.dataService.getItemChildren(this.treeGridData, itemId);
  }

  //----------------------------------------------

  getMarkedItems() {
    return this.markingManager.getMarkedItems();
  }

  //----------------------------------------------

  private filterDisplayedData(filterBy) {
    this.dataService.filterData(this._treeGridData, this._originalTreeGridData, filterBy);
    setTimeout(()=> {
      this.onFilteredCountChanged.emit(this.getDisplayedItemsCount());
    });
  }

  getDisplayedItemsCount():number {
    return this.dataService.getDisplayedItemsNum(this._treeGridData);
  }

}

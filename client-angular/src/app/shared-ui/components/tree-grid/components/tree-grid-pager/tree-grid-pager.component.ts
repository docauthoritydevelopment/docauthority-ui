import {Component, Input, OnChanges, ViewEncapsulation} from '@angular/core';
import {TreeGridPagingInfo} from '../../types/tree-grid.type';

declare var $: any;
declare var _: any;

@Component({
  selector: 'tree-grid-pager',
  templateUrl: './tree-grid-pager.template.html',
  styleUrls: ['./_tree-grid-pager.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TreeGridPagerComponent implements OnChanges {

  _paging: TreeGridPagingInfo;

  @Input() parent;

  @Input()
  set paging(newPaging: TreeGridPagingInfo) {
    this._paging = newPaging;
    this._displayPageNumber = this.paging.currentPage;
  }

  get paging():TreeGridPagingInfo {
    return this._paging;
  }

  _displayPageNumber:number = null;
  showPager: boolean = false;


  get displayPageNumber() {
    return this._displayPageNumber;
  }

  set displayPageNumber(newPageNum:number) {
    this._displayPageNumber = newPageNum;
  }

  ngOnChanges(): void {
    if (this.paging) {
      this.showPager = this.paging.totalPages > 1 || this.paging.currentPage>1;
    }
  }

  verifyKey(event):boolean {
    if (event.key === "Enter") {
      if (this.displayPageNumber === null || this.displayPageNumber+'' === '' || isNaN(this.displayPageNumber)) {
        this._displayPageNumber = this.paging.currentPage;
      }
      else if (Number(this.displayPageNumber) > this.paging.totalPages)
      {
        this.parent.moveToPage(this.paging.totalPages);
        return true;
      }
      else {
        this.parent.moveToPage(this.displayPageNumber);
        return true;
      }
    }
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  moveToPage(option) {
    switch (option) {
      case 'first': {
        this.parent.moveToPage(1);
        break
      }
      case 'prev': {
        this.parent.moveToPage(Math.max(this.paging.currentPage - 1, 1));
        break
      }
      case 'next': {
        this.parent.moveToPage(Math.min(this.paging.currentPage + 1, this.paging.totalPages));
        break
      }
      case 'last': {
        this.parent.moveToPage(this.paging.totalPages);
        break
      }
    }
  }
}

import {Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import {TreeGridSelectionMode} from "@tree-grid/types/tree-grid.type";

declare let $: any;
declare let _: any;

@Component({
  selector: 'tree-grid-selection-switcher',
  templateUrl: './tree-grid-selection-switcher.template.html',
  styleUrls: ['./_tree-grid-selection-switcher.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeGridSelectionSwitcherComponent implements OnInit {

  @Input() hostComponent;

  mode = TreeGridSelectionMode.single;

  //-------------------------------------------

  ngOnInit(): void {

  }

  //-------------------------------------------

  toggleCheckbox() {
    if(this.hostComponent && this.hostComponent.treeGrid) {
      this.mode = this.mode === TreeGridSelectionMode.single ? TreeGridSelectionMode.multi: TreeGridSelectionMode.single;
      this.hostComponent.treeGrid.selectionMode =this.mode;
    }
  }
}

import {Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild, Input, ElementRef, ViewEncapsulation} from '@angular/core';
import {TreeGridHeaderBaseComponent} from "@tree-grid/base/tree-grid-inner-item.base";
import {MarkType, TreeGridConfig} from '@tree-grid/types/tree-grid.type';
import {TreeGridSortOrder, TreeGridSelectionMode} from "@tree-grid/types/tree-grid.type"

declare let $: any;
declare let _: any;

@Component({
  selector: 'tree-grid-header',
  templateUrl: './tree-grid-header.template.html',
  styleUrls: ['./_tree-grid-header.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeGridHeaderWrapperComponent implements OnInit {

  _initialSortDirection: TreeGridSortOrder;
  _initialSortFieldName: string;


  @Input() parent;
  @Input() treeGridConfig:TreeGridConfig;
  @Input() treeGridHeaderComponent;

  @Input()
  set initialSortDirection(newDirection: TreeGridSortOrder) {
   this._initialSortDirection = newDirection;
   this.updateSortInitialParams();
  }

  get initialSortDirection() {
    return this._initialSortDirection;
  }

  @Input()
  set initialSortFieldName(newFieldName: string) {
    this._initialSortFieldName = newFieldName;
    this.updateSortInitialParams();
  }

  get initialSortFieldName() {
    return this._initialSortFieldName;
  }

  @Input() hostHeaderParams;

  @ViewChild('viewContainer', {read: ViewContainerRef}) viewContainer: ViewContainerRef;

  private outerElm;
  private innerElm;

  checkTypes = MarkType;
  checkType  = MarkType.none;
  TreeGridSelectionMode = TreeGridSelectionMode;

  constructor(
    private el : ElementRef,
    private componentFactoryResolver: ComponentFactoryResolver) {
  }

  //---------------------------------------------

  ngOnInit(): void {

    const factory = this.componentFactoryResolver.resolveComponentFactory(this.treeGridHeaderComponent);
    const ref = this.viewContainer.createComponent(factory);
    (<TreeGridHeaderBaseComponent>(ref.instance)).hostHeaderParams = this.hostHeaderParams;

    this.outerElm = $(this.el.nativeElement).closest('.header-panel');
    this.innerElm = $(ref['_elDef'].element.name);
    this.fixHeaderPos();

    _.defer(() => {
      this.handleSort();
    }, 100);
  }

  //--------------------------------------------

   fixHeaderPos() {
    this.outerElm.closest('.scrollable-panel').scroll((e) => {
      this.outerElm.css({
        display: 'block',
        top: e.currentTarget.scrollTop
      })
    });
  }

  //--------------------------------------------

  handleSort() {
    let sortColumns = this.innerElm.find('*[data-col-sortable="true"]');
    sortColumns.addClass('sortable');

    sortColumns.click((e) => {
      let target = $(e.target);
      let isAsc = target.hasClass('desc');

      sortColumns.removeClass('desc asc');
      target.addClass(isAsc ? 'asc' : 'desc');

      this.parent.sortBy({
        sortBy: target.attr('data-col-name'),
        sortOrder: isAsc ? TreeGridSortOrder.ASC : TreeGridSortOrder.DESC
      });
    });
    this.updateSortInitialParams();
  }


  updateSortInitialParams() {
    if (this.innerElm) {
      let sortColumns = this.innerElm.find('*[data-col-sortable="true"]');
      if (sortColumns && this.initialSortFieldName != null) {
        for (let count = 0; count < sortColumns.length; count++) {
          $(sortColumns[count]).removeClass(TreeGridSortOrder.ASC.toLowerCase());
          $(sortColumns[count]).removeClass(TreeGridSortOrder.DESC.toLowerCase());
          if (sortColumns[count].getAttribute('data-col-name') == this.initialSortFieldName) {
            $(sortColumns[count]).addClass((this.initialSortDirection || TreeGridSortOrder.ASC).toLowerCase());
          }
        }
      }
    }
  }

  //-------------------------------------------

  onMarkedItemsChanged(totalItems, markedItems) {

    this.checkType = MarkType.none;

    if(markedItems > 0) {
      this.checkType = markedItems < totalItems ? MarkType.partial : MarkType.all;
    }
  }

  //--------------------------------------------

  onCheckboxClicked(e){

    let isChecked = (this.checkType !== MarkType.none);

    this.checkType = isChecked ? MarkType.none : MarkType.all;
    this.parent.markAllItems(!isChecked);
  }

  onSwitchSelectionModeClicked(e){
    this.parent.selectionMode = TreeGridSelectionMode.multi
  }

  get selectionMode():TreeGridSelectionMode{
    return this.parent.selectionMode;
  }
  //--------------------------------------------

  onBtnActionsClicked(e) {

  }
}

import {Observable} from 'rxjs';
import {EGroupType} from "@app/common/discover/types/common-enums";

export class TreeGridDataItem<T> {
  id: number;
  hasMoreChildren: any;
  siblings: any;
  parentId: any;
  children: any;
  level? : number;
  additional?: any;
  type? : any;
  itemChecked?:boolean;
  isExpand:boolean = false;
  isOpen?:boolean;

  constructor(public item: T) {

  }
}

//-----------------------------------------------

export class TreeGridPagingInfo {
  currentPage: number;
  totalPages: number;
}

//------------------------------------------------

export class TreeGridSortingInfo {
  constructor(
    public  sortBy: string,
    public sortOrder: TreeGridSortOrder
  ) {
  };
}

//-----------------------------------------------

export class TreeGridColumn {
  name: string;
  title: string;
  width?: string;
  sortable: boolean
}

//------------------------------------------------

export class TableColumn {
  name: string;
  title: string;
  width?: string;
  sortable: boolean
}

//------------------------------------------------

export enum TreeGridSortOrder {
  ASC = 'ASC' ,
  DESC = 'DESC'
}

//------------------------------------------------

export class TreeGridConfig {

  showMoreInfoBtn: boolean= false;
  showMoreActionsBtn: boolean= false;
  doLocalSorting: boolean = false;
  enableSwitchSelectionMode:boolean = false;
  disableSelection:boolean = false;
}

//------------------------------------------------

export class TreeGridData<T> {
  common? : {};
  items: TreeGridDataItem<T>[];
  totalElements: number;
  selectedItem ? : T;
  selectedItemParents? : T[];
  paging? : TreeGridPagingInfo;
  sorting? : TreeGridSortingInfo;
}

//------------------------------------------------

export class TreeGridContextMenuOptions {
  el: any;
  items: {};
  callback: () => any;
  selector: string;
}

//------------------------------------------------

export class TreeGridAdapterMapper {

  parentIdProperty: string;
  contentProperty: string;
  idProperty: string;
  pageSizeProperty: string;
  pageNumberProperty: string;
  totalElementsProperty: string;
  totalPagesProperty: string;
  numberOfElementsProperty: string;

  constructor({
                parentIdProperty = 'parentId',
                contentProperty = 'content',
                idProperty = 'id',
                pageSizeProperty = 'size',
                pageNumberProperty = 'number',
                totalElementsProperty = 'totalElements',
                totalPagesProperty = 'totalPages',
                numberOfElementsProperty = 'numberOfElements'
              }: {
    parentIdProperty?: string,
    contentProperty?: string,
    idProperty?: string,
    pageSizeProperty?: string,
    pageNumberProperty?: string,
    totalElementsProperty?: string,
    totalPagesProperty?: string,
    numberOfElementsProperty?: string
  } = {}) {
    this.parentIdProperty = parentIdProperty;
    this.contentProperty = contentProperty;
    this.idProperty = idProperty;
    this.pageSizeProperty = pageSizeProperty;
    this.pageNumberProperty = pageNumberProperty;
    this.totalElementsProperty = totalElementsProperty;
    this.totalPagesProperty = totalPagesProperty;
    this.numberOfElementsProperty = numberOfElementsProperty;
  }
}

//------------------------------------------------

export enum MarkType {
  none = 'none',
  all = 'all',
  partial = 'partial'
}

//------------------------------------------------

export enum TreeGridSelectionMode {
  single = 'single',
  multi = 'multi'
}

//-----------------------------------------------

export enum TreeGridExcludeItemType {
  item = 'single',
  children = 'children'
}

//------------------------------------------------

export interface ITreeGridEvents<T> {
  onItemSelected?(itemId: any, item: T): void;

  onMarkedItemsChanged?(items: any []);

  onLoadSubTree?(dataItem: any): Observable<{}>;

  onPageChanged?(flag: any): void;

  onSortByChanged?(colName: any): void;

  onCheckItemsChanged?(items:any[]): void;
}


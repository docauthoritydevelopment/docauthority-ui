interface MORE_INFO_PROP {
  title: string,
  value: string
}

export interface TREE_GRID_ITEM_MORE_INFO {
  title: string;
  props: MORE_INFO_PROP[]
}

import * as _ from 'lodash';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {DialogService} from "@shared-ui/components/modal-dialogs/dialog.service";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'da-select-input',
  templateUrl: './daSelectInput.template.html',
  styleUrls: ['./daSelectInput.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DaSelectInputComponent {
  @ViewChild('myDrop') myDrop: NgbDropdown;

  _options: any[]  =[];
  origOptions: any[] = [];
  availableOptions:any[] = [];
  _searchText: string = '';
  _selectedName:string;
  _withNoneValue: boolean = false;
  inUpdateMode:boolean;
  lastSelectedText:string = '';

  static NONE_VALUE_ITEM:any = {
    name : "None",
    id : null
  };

  @ViewChild("selectInputBox")
  selectInputBox:ElementRef;

  @Output() itemSelected = new EventEmitter();

  @Input()
  canChooseNotOnlyFromList:boolean = false;

  @Input()
  idField:string = 'id';

  @Input()
  displayedTextField:string = 'name';

  @Input()
  displayedSubTextField:string = 'description';

  @Input()
  newPopupComponent:any = null;

  @Input()
  newPopupData:any = {};

  @Input()
  disabled:boolean = false;

  @Input()
  withoutAutoComplete:boolean = false;

  @Input()
  set withNoneValue(newBoolValue:boolean) {
    if (this._withNoneValue != newBoolValue) {
      this._withNoneValue = newBoolValue;
      this.options = this.origOptions;
    }
  }

  get withNoneValue():boolean {
    return this._withNoneValue;
  }

  @Input()
  emptyValueMsg:string = '';

  @Input()
  openInTop:boolean = false;

  @Input()
  onlyForDisplay:boolean = false;

  @Input()
  set selectedName(newName:string) {
    this._selectedName = newName;
    this._searchText = '';
  }

  get selectedName(){
    return this._selectedName;
  }


  @Input()
  set options(newOptions: any[]){
    this.origOptions = newOptions;
    this._options = _.cloneDeep(newOptions);
    if (this.withNoneValue) {
      this._options.unshift(DaSelectInputComponent.NONE_VALUE_ITEM);
    }
    this.availableOptions = this._options;
  }

  onBlur = ()=>  {
    this.myDrop.close();
    /*
    if (this.canChooseNotOnlyFromList) {
      this.itemSelected.emit(this.lastSelectedText);
      this.lastSelectedText="";
    }*/
    this.searchText = '';
    this.availableOptions = this._options;
    setTimeout(()=>{
      this.inUpdateMode = false;
    });
  }

  cancelEvent(event) {
    event.stopImmediatePropagation();
  }

  set searchText(newText: string) {
    this._searchText = newText;
    this.lastSelectedText = newText;
    if (this._searchText=="") {
      this.availableOptions = this._options;
    }
    else {
      this.availableOptions = this._options.filter((optionItem)=> {
        return (optionItem[this.displayedTextField].toLowerCase().indexOf(this._searchText.toLowerCase()) > -1);
      });
    }
  }

  onEnterClick(){
    if (this.canChooseNotOnlyFromList) {
      this.lastSelectedText = this._searchText;
      this.onBlur();
    }
  }

  changeToUpdateMode() {
    if (!this.onlyForDisplay) {
      if (!this.withoutAutoComplete) {
        this.inUpdateMode = true;
        setTimeout(() => {
          this.selectInputBox.nativeElement.focus();
          this.myDrop.open();
        });
      }
      else {
        this.myDrop.open();
      }
    }
  }

  doSelectItem = (aOption)=>  {
    if (!this.canChooseNotOnlyFromList) {
      this.itemSelected.emit(aOption);
    }
    else {
      this.lastSelectedText = aOption.id;
    }
    this.onBlur();
  }

  onDropDownOpenChanged(isOpen) {
    if (!isOpen) {
      this._searchText = '';
      this.availableOptions = this._options;
      this.inUpdateMode = false;
      if (this.canChooseNotOnlyFromList) {
        this.itemSelected.emit(this.lastSelectedText);
      }
    }
  }

  openNewPopup() {
    this.myDrop.close();
    let modelParams = {
      windowClass: 'modal-md-window'
    };
    this.dialogService.openModalPopup(this.newPopupComponent, {data : this.newPopupData}, modelParams).result.then((data : any) => {
      if(data) {
        this._options.push(data);
        this.selectedName = data[this.displayedTextField];
        this.itemSelected.emit(data);
        this.onBlur();
      }
    }, (reason) => {
    });
  }

  ddMenuMouseDown(event) {
    event.stopImmediatePropagation();
  }

  constructor(private dialogService: DialogService, private elementRef: ElementRef) {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconComponent } from './icon/icon.component';
import { DropdownMenuDirective } from './directives/dropdown-menu.directive';
import {DisableDirective} from '@shared-ui/directives/disable.directive';
import {TreeGridAdapterService} from '@tree-grid/services/tree-grid-adapter.service';
import {ConfirmComponent} from '@shared-ui/components/modal-dialogs/confirm/confirm.component';
import {DialogWrapperComponent} from '@shared-ui/components/modal-dialogs/dialog-wrapper/dialog-wrapper.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {DialogService} from '@shared-ui/components/modal-dialogs/dialog.service';
import {AlertComponent} from '@shared-ui/components/modal-dialogs/alert/alert.component';
import {KeysPipe} from '@shared-ui/pipes/keysPipe.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProgressBarComponent} from "@shared-ui/components/progrtessBar/progressBar.component";
import {DaDropDownComponent} from "@shared-ui/components/da-drop-down/da-drop-down.component";
import {PeriodPipe} from "@shared-ui/pipes/periodPipe.pipe";
import {TranslationModule} from "@app/common/translation/translation.module";
import {DaDatePipe} from "@shared-ui/pipes/da-date.pipe";
import {DaDropDownService} from "@shared-ui/components/da-drop-down/services/da-drop-down.service";
import {BytesPipe} from "@shared-ui/pipes/bytes.pipe";
import {NumSuffixPipe} from "@shared-ui/pipes/numSuffix.pipe";
import {SearchBoxComponent} from "@shared-ui/components/searchBox/searchBox.component";
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DropdownPositionDirective} from "@shared-ui/directives/DropdownPositionDirective";
import {CheckNullPipe} from "@shared-ui/pipes/checkNull.pipe";
import {LimitBigPipe} from "@shared-ui/pipes/limitBig.pipe";
import {DaDraggableModule} from "@shared-ui/daDraggable.module";
import {ToolbarComponent} from "@shared-ui/components/toolbar/toolbar.component";
import {DaSelectInputComponent} from "@shared-ui/components/da-select-input/daSelectInput.component";
import {ToolbarActionsComponent} from "@shared-ui/components/toolbar/toolbarActions/toolbarActions.component";
import {ToolbarExportActionComponent} from "@shared-ui/components/toolbar/toolbarExportAction/toolbarExportAction.component";
import {ToolbarDropdownActionsComponent} from "@shared-ui/components/toolbar/toolbarDropdownActions/toolbarDropdownActions.component";
import {ToolbarTitleComponent} from "@shared-ui/components/toolbar/toolbarTitle/toolbarTitle.component";
import {SearchPipe} from "@shared-ui/pipes/searchPipe.pipe";


const SHARED_UI_COMPONENTS = [IconComponent, DropdownMenuDirective, DisableDirective, DaDatePipe, DialogWrapperComponent,CheckNullPipe,LimitBigPipe,
  ConfirmComponent, AlertComponent, KeysPipe, ProgressBarComponent, DaDropDownComponent, PeriodPipe,BytesPipe,NumSuffixPipe,SearchBoxComponent, DropdownPositionDirective,
  SearchPipe,DaSelectInputComponent, ToolbarComponent, ToolbarActionsComponent, ToolbarExportActionComponent, ToolbarDropdownActionsComponent, ToolbarTitleComponent];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModalModule,
    NgbModule,
    TranslationModule,
    RouterModule,
    DaDraggableModule
  ],
  providers: [TreeGridAdapterService, DialogService, DaDropDownService],
  declarations: SHARED_UI_COMPONENTS,
  exports: SHARED_UI_COMPONENTS,
  entryComponents: [ConfirmComponent, DialogWrapperComponent, AlertComponent]
})
export class SharedUiModule { }

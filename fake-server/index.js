const express = require('express');

const app = express();

app.get('/api/user/role', (req, res) => {
  const sysRoles = require('./data/sys-roles');

  res.send(sysRoles);

})


app.get('/api/*', (req, res) => {
  res.send([]);
})

app.get('/getVersion', (req, res) => {
  res.send({version: 'Fake-version'})
})

app.get('/login.html', (req, res) => {
  res.sendFile('../client-angularjs/login.html')
})

app.post('/login', (req, res) => {
  res.send({user: {}});
})

app.get('/userInfo', (req, res) => {
  const userInfo = require('./data/user-info');
  res.send(userInfo);

})

function getPort() {
  let port = 3000;

  const arguments = process.argv;

  const portArgumentIndex = arguments.findIndex(arg => arg.startsWith('--port'))

  if (portArgumentIndex < 0) {
    console.warn(`No port specified, will listen on ${port}`)
  } else {
    try {
      port = parseInt(arguments[portArgumentIndex + 1]);
    }
    catch(err) {
      console.error('Illegal port number, Useage: --port <port number>');
    }
  }

  return port;
}

const port = getPort();

const server = app.listen(port, () => {
 console.log(`Fake backend server listens on port ${port}`)
})

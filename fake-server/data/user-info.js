module.exports = {
  "session": "4CA3E16A473AE04FDA2BF1ABC442CC32",
  "id": 33,
  "name": "Administrator",
  "username": "admin",
  "bizRoleDtos": [{
    "id": 1,
    "name": "admin",
    "description": "Administrator",
    "displayName": "Administrator",
    "template": {
      "id": 1,
      "name": "admin",
      "description": "Administrator",
      "displayName": "Administrator",
      "templateType": "BIZROLE",
      "systemRoleDtos": [{
        "id": 8,
        "name": "GeneralAdmin",
        "displayName": "General administration",
        "description": "General system administration",
        "authorities": ["GeneralAdmin"]
      }, {
        "id": 15,
        "name": "MngDocTypeConfig",
        "displayName": "Configure document types",
        "description": "Configure DocTypes",
        "authorities": ["MngDocTypeConfig"]
      }, {
        "id": 1,
        "name": "MngScanConfig",
        "displayName": "Manage scan configuration",
        "description": "Manage root folders and scan configuration",
        "authorities": ["MngScanConfig"]
      }, {
        "id": 30,
        "name": "ViewReports",
        "displayName": "View reports",
        "description": "View saved reports",
        "authorities": ["ViewReports"]
      }, {
        "id": 11,
        "name": "AssignSecurityTag",
        "displayName": "Assign security tags",
        "description": "Assign security tags",
        "authorities": ["AssignSecurityTag"]
      }, {
        "id": 19,
        "name": "AssignDataRole",
        "displayName": "Assign Data Role",
        "description": "Assign Data Role",
        "authorities": ["AssignDataRole"]
      }, {
        "id": 25,
        "name": "ViewPermittedContent",
        "displayName": "View permitted content",
        "description": "View permitted content",
        "authorities": ["ViewPermittedContent"]
      }, {
        "id": 14,
        "name": "ViewSecurityTagAssociation",
        "displayName": "View security tags",
        "description": "View security tag associations",
        "authorities": ["ViewSecurityTagAssociation"]
      }, {
        "id": 4,
        "name": "MngUsers",
        "displayName": "Manage users",
        "description": "Manage system users",
        "authorities": ["MngUsers"]
      }, {
        "id": 24,
        "name": "ViewFiles",
        "displayName": "View files",
        "description": "View all files",
        "authorities": ["ViewFiles"]
      }, {
        "id": 17,
        "name": "ViewDocTypeTree",
        "displayName": "View DocTypes tree",
        "description": "View DocTypes hierarchy",
        "authorities": ["ViewDocTypeTree"]
      }, {
        "id": 31,
        "name": "MngReports",
        "displayName": "Manage reports",
        "description": "Manage saved reports",
        "authorities": ["MngReports"]
      }, {
        "id": 7,
        "name": "ViewAuditTrail",
        "displayName": "View audit",
        "description": "View audit trail",
        "authorities": ["ViewAuditTrail"]
      }, {
        "id": 3,
        "name": "MngRoles",
        "displayName": "Manage roles",
        "description": "Manage system roles",
        "authorities": ["MngRoles"]
      }, {
        "id": 20,
        "name": "ViewDataRoleAssociation",
        "displayName": "View assigned Data Role",
        "description": "View Data Role associations",
        "authorities": ["ViewDataRoleAssociation"]
      }, {
        "id": 16,
        "name": "AssignDocType",
        "displayName": "Assign DocTypes",
        "description": "Assign DocTypes",
        "authorities": ["AssignDocType"]
      }, {
        "id": 18,
        "name": "ViewDocTypeAssociation",
        "displayName": "View assigned DocTypes",
        "description": "View DocTypes associations",
        "authorities": ["ViewDocTypeAssociation"]
      }, {
        "id": 21,
        "name": "MngBizLists",
        "displayName": "Manage business lists",
        "description": "Manage business lists",
        "authorities": ["MngBizLists"]
      }, {
        "id": 9,
        "name": "MngTagConfig",
        "displayName": "Manage tags",
        "description": "Manage tags configuration",
        "authorities": ["MngTagConfig"]
      }, {
        "id": 29,
        "name": "MngGroups",
        "displayName": "Manage groups",
        "description": "Manage group names and content",
        "authorities": ["MngGroups"]
      }, {
        "id": 12,
        "name": "ViewTagTypes",
        "displayName": "View tag types",
        "description": "View tag types",
        "authorities": ["ViewTagTypes"]
      }, {
        "id": 2,
        "name": "RunScans",
        "displayName": "Run scans",
        "description": "Run scans",
        "authorities": ["RunScans"]
      }, {
        "id": 13,
        "name": "ViewTagAssociation",
        "displayName": "View assigned tags",
        "description": "View tag associations",
        "authorities": ["ViewTagAssociation"]
      }, {
        "id": 26,
        "name": "ViewPermittedFiles",
        "displayName": "View permitted files",
        "description": "View permitted files",
        "authorities": ["ViewPermittedFiles"]
      }, {
        "id": 27,
        "name": "ViewBizListAssociation",
        "displayName": "View assigned business list items",
        "description": "View business list associations",
        "authorities": ["ViewBizListAssociation"]
      }, {
        "id": 23,
        "name": "ViewContent",
        "displayName": "View files content",
        "description": "View all files content",
        "authorities": ["ViewContent"]
      }, {
        "id": 6,
        "name": "MngUserRoles",
        "displayName": "Assign roles",
        "description": "Assign roles to users",
        "authorities": ["MngUserRoles"]
      }, {
        "id": 22,
        "name": "RunActions",
        "displayName": "Run actions",
        "description": "Run actions",
        "authorities": ["RunActions"]
      }, {
        "id": 5,
        "name": "ViewUsers",
        "displayName": "View users",
        "description": "View users configuration",
        "authorities": ["ViewUsers"]
      }, {
        "id": 28,
        "name": "AssignBizList",
        "displayName": "Assign business list item",
        "description": "Assign business list item",
        "authorities": ["AssignBizList"]
      }, {
        "id": 10,
        "name": "AssignTag",
        "displayName": "Assign tags",
        "description": "Assign tags",
        "authorities": ["AssignTag"]
      }]
    }
  }],
  "funcRoleDtos": [],
  "passwordMustBeChanged": false,
  "systemRoleDtos": [],
  "userType": "INTERNAL",
  "highlightFileContentTextSupport": true,
  "loginTime": "2018-05-31T09:14:07.308Z"
}

REM @echo off

echo === Compile v2
cd  client-angular
call ng build

echo === Compile v1
cd  ../client-angularjs
call grunt buildDev

echo === Server
cd  ..
call node server/app.js


